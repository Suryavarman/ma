<div style="text-align: center;">
<h1>Maçon de l'espace</h1>
</div>

![Interface graphique de Ma](/code/applications/Editeur/images/gui_02.png)
[![License](https://img.shields.io/badge/license-MIT-green)](https://framagit.org/Suryavarman/ma/-/blob/master/LICENSE)
[![pipeline status](https://framagit.org/Suryavarman/ma/badges/master/pipeline.svg)](https://framagit.org/Suryavarman/ma/-/commits/master)

## Présentation

Maçon de l'espace alias [Ma](https://framagit.org/Suryavarman/ma) est un projet écrit en c++17 et python3.

L'objectif est de créer un éditeur d'applications 3D simple qui permet d'interfacer différents moteurs 3D tel que [Ogre3D](https://www.ogre3d.org/)
, [Panda3D](https://www.panda3d.org/) et [Cycles](https://wiki.blender.org/wiki/Source/Render/Cycles/Standalone).

La version du projet en est encore au stade d'alpha.

## La subdivision du projet

- [api](/code/api/ma/)
- [modules](/code/modules/)
- [applications](/code/applications/)

## Liens

Documentation de l'api de ma : https://suryavarman.fr/stockage/ma/doc/

Git :  [https://framagit.org/Suryavarman/ma](https://framagit.org/Suryavarman/ma)

Extraits de code : https://framagit.org/Suryavarman/ma/-/snippets

Actualité : [Mastodon](https://mamot.fr/@suryavarman)

Wiki : [wikis/home](https://framagit.org/Suryavarman/ma/-/wikis/home)
