/// \file Platform.h
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

/*
 *-----------------------------------------------------------------------------
 *This source file was part of OGRE
 *    (Object-oriented Graphics Rendering Engine)
 *For the latest info, see http://www.ogre3d.org/
 *
 *Original version Copyright (C) 2000-2009 Torus Knot Software Ltd
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *THE SOFTWARE.
 *-----------------------------------------------------------------------------
 */

#pragma once

// Initial platform/compiler-related stuff to set.
#define MA_PLATFORM_WIN32 1
#define MA_PLATFORM_LINUX 2
#define MA_PLATFORM_APPLE 3
#define MA_PLATFORM_SYMBIAN 4
#define MA_PLATFORM_IPHONE 5

#define MA_COMPILER_MSVC 1
#define MA_COMPILER_GNUC 2
#define MA_COMPILER_BORL 3
#define MA_COMPILER_WINSCW 4
#define MA_COMPILER_GCCE 5

#define MA_COMPILER_CLANG 6 // Ça peut aider... : https://clang.llvm.org/docs/DiagnosticsReference.html

#define MA_ENDIAN_LITTLE 1
#define MA_ENDIAN_BIG 2

#define MA_ARCHITECTURE_32 1
#define MA_ARCHITECTURE_64 2

// Finds the compiler type and version.
#if defined(__GCCE__)
    #define MA_COMPILER MA_COMPILER_GCCE
    #define MA_COMP_VER _MSC_VER

#elif defined(__WINSCW__)
    #define MA_COMPILER MA_COMPILER_WINSCW
    #define MA_COMP_VER _MSC_VER

#elif defined(_MSC_VER)
    #define MA_COMPILER MA_COMPILER_MSVC
    #define MA_COMP_VER _MSC_VER

#elif defined(__clang__)
    #define MA_COMPILER MA_COMPILER_CLANG
    #define MA_COMP_VER (((__clang_major__) * 100) + (__clang_minor__ * 10) + __clang_patchlevel__)

#elif defined(__GNUC__)
    #define MA_COMPILER MA_COMPILER_GNUC
    #define MA_COMP_VER (((__GNUC__) * 100) + (__GNUC_MINOR__ * 10) + __GNUC_PATCHLEVEL__)

#elif defined(__BORLANDC__)
    #define MA_COMPILER MA_COMPILER_BORL
    #define MA_COMP_VER __BCPLUSPLUS__
    #define __FUNCTION__ __FUNC__
#else
    #pragma error "Compilateur incconnu. Abandon de la compilation."

#endif

/* Finds the current platform */
#if defined(__WIN32__) || defined(_WIN32)
    #define MA_PLATFORM MA_PLATFORM_WIN32
#elif defined(__APPLE_CC__)
    // Device                                                     Simulator
    // Both requiring OS version 4.0 or greater
    #if __ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__ >= 40000 || __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
        #define MA_PLATFORM MA_PLATFORM_APPLE_IOS
    #else
        #define MA_PLATFORM MA_PLATFORM_APPLE
    #endif
#else
    #define MA_PLATFORM MA_PLATFORM_LINUX
#endif

/* Find the arch type */
#if defined(__x86_64__) || defined(_M_X64) || defined(__powerpc64__) || defined(__alpha__) || defined(__ia64__) ||     \
    defined(__s390__) || defined(__s390x__)
    #define MA_ARCH_TYPE MA_ARCHITECTURE_64
#else
    #define MA_ARCH_TYPE MA_ARCHITECTURE_32
#endif
