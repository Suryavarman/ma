/// \file wx/Utility.tpp
/// \author Pontier Pierre
/// \date 2020-05-24
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Error.h"
#include "ma/Error.h"
namespace ma::wx
{
    template<typename T>
    ObjectKey<T>::ObjectKey()
    {}

    template<typename T>
    ObjectKey<T>::ObjectKey(const T &key)
    {
        auto data = new KeyRefData<T>();
        m_refData = data;
        data->m_Key = key;
    }

    template<typename T>
    ObjectKey<T>::~ObjectKey()
    {}

    template<typename T>
    wxObjectRefData *ObjectKey<T>::CreateRefData() const
    {
        return new KeyRefData<T>();
    }

    template<typename T>
    wxObjectRefData *ObjectKey<T>::CloneRefData(const wxObjectRefData *data) const
    {
        auto data_key = dynamic_cast<const KeyRefData<T> *>(data);

        MA_ASSERT(data_key, L"data_key est nulle", std::invalid_argument);

        return new KeyRefData<T>(*data_key);
    }

    template<typename T>
    bool ObjectKey<T>::operator==(const ObjectKey &obj) const
    {
        if(m_refData == obj.m_refData)
            return true;

        if(!m_refData || !obj.m_refData)
            return false;

        auto data_key = dynamic_cast<KeyRefData<T> *>(m_refData);
        auto obj_data_key = dynamic_cast<KeyRefData<T> *>(obj.m_refData);

        // here we use the wxObjectRefData::operator==() function.
        // Note however that this comparison may be very slow if the
        // reference data contains a lot of data to be compared.
        return *data_key == *obj_data_key;
    }

    template<typename T>
    void ObjectKey<T>::SetKey(const T &key)
    {
        // since this function modifies one of the ma::wxObjectKey internal property,
        // we need to be sure that the other ma::wxObjectKey instances which share the
        // same MyCarRefData instance are not affected by this call.
        // I.e. it's very important to call UnShare() in all setters of
        // refcounted classes!
        UnShare();

        auto data_key = dynamic_cast<KeyRefData<T> *>(m_refData);

        MA_ASSERT(data_key, L"data_key est nulle", std::runtime_error);

        data_key->m_Key = key;
    }

    template<typename T>
    T ObjectKey<T>::GetKey() const
    {
        wxCHECK_MSG(IsOk(), T(), L"invalid key");

        auto data_key = dynamic_cast<KeyRefData<T> *>(m_refData);

        MA_ASSERT(data_key, L"data_key est nulle", std::runtime_error);

        return data_key->m_Key;
    }
} // namespace ma::wx