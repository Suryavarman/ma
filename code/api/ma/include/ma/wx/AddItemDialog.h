/// \file wx/AddItemDialog.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#pragma once

#include "ma/wx/wx.h"
#include "ma/Dll.h"
#include "ma/Sort.h"

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/checklst.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/html/htmlwin.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

namespace ma::wx
{
    /// Affiche une fenêtre de dialogue.
    /// Elle permet de choisir le type de l'item à créer et à ajouter dans l'item parent passé en paramètre.
    /// \exception std::invalid_argument La clef de l'item parent n'est associé à aucun item.
    class M_DLL_EXPORT AddItemDialog: public wxDialog
    {
        protected:
            wxListBox *m_TypesListBox;
            wxHtmlWindow *m_HTMLWin;
            wxButton *m_ValidateButton;
            wxButton *m_CancelButton;

            ma::ItemPtr m_ItemParent;

            // Virtual event handlers, override them in your derived class
            virtual void OnAdd(wxCommandEvent &event);
            virtual void OnCancel(wxCommandEvent &event);
            virtual void OnSelected(wxCommandEvent &event);

            virtual wxArrayString GetList();
            virtual void SetHtml();

        public:
            AddItemDialog(const ma::Item::key_type &key_parent,
                          wxWindow *parent,
                          wxWindowID id = wxID_ANY,
                          const wxString &title = wxEmptyString,
                          const wxPoint &pos = wxDefaultPosition,
                          const wxSize &size = wxSize(758, 523),
                          long style = wxDEFAULT_DIALOG_STYLE);
            ~AddItemDialog() override;
    };

} // namespace ma::wx
