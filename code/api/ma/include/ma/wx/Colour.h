/// \file wx/Colour.h
/// \author Pontier Pierre
/// \date 2020-10-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"
#include "ma/Dll.h"
#include "ma/Colour.h"
#include "ma/wx/Variable.h"
#include <wx/clrpicker.h>
#include <regex>

namespace ma::wx
{
    class M_DLL_EXPORT ColourPanel: public Var<wxColourVar>
    {
        protected:
            /// Permet de ne pas créer une boucle infinie de mise à jour.
            /// Si cette variable a pour valeur « vrai » alors les fonctions comme OnColourPicker, ne tiendrons pas
            /// compte de l'évènement.
            bool m_ObservationUpdatePhase;

            /// Ça serait peut-être mieux d'utiliser https://docs.wxwidgets.org/trunk/classwx_colour_dialog.html et
            /// de refaire la gestion du bouton, avec la gestion de la transparence.
            wxColourPickerCtrl *m_ColourPickerCtrl;

            virtual void OnColourPickerChanged(wxColourPickerEvent &event);

            /// \remarks Cet évènement existe depuis la version 3.1.3 de wxWidgets.
            /// S'il n'existe pas, la fonction ne sera pas utilisée.
            virtual void OnColourPickerCurrentChanged(wxColourPickerEvent &event);

            /// \remarks Cet évènement existe depuis la version 3.1.3 de wxWidgets.
            /// S'il n'existe pas la fonction ne sera pas utilisée.
            virtual void OnColourPickerDialogCancelled(wxColourPickerEvent &event);

        public:
            ColourPanel(ma::wxColourVarPtr var,
                        wxWindow *parent,
                        wxWindowID win_id = wxID_ANY,
                        const wxPoint &pos = wxDefaultPosition,
                        const wxSize &size = wxDefaultSize,
                        long style = wxTAB_TRAVERSAL);

            ~ColourPanel() override;

            void SetVar(const ma::ObservablePtr &observable, bool end_observation = false) override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ColourPanel, wx)
    };
    typedef ma::wxObserverVar<ColourPanel> ColourPanelVar;
    typedef ColourPanelVar *ColourPanelVarPtr;

    M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(ColourPanel, wx)

} // namespace ma::wx
