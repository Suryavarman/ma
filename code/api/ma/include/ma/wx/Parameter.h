/// \file wx/Parameter.h
/// \author Pontier Pierre
/// \date 2020-09-01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"

#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>

#include "ma/Dll.h"
#include "ma/Observer.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/Colour.h"
#include "ma/RTTI.h" // Pour que la macro M_HEADER_MAKERVAR_TYPE fonctionne.

namespace ma::wx
{
    /// Pour modifier et visualiser les paramètre d'affichage d'une interface wxWidgets, il faut un item qui
    /// effectue le lien entre le système d'affichage des « ma::Item » et l'interface graphique.
    /// Ils servent principalement de structure contenant des variables.
    class Parameter: public ma::Item, public ma::Observer
    {
        private:
            /// Permet de stocker une fois pour toute le class info associée à m_wxClassName.
            const wxClassInfo *m_ClassInfo;

        protected:
            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            /// \brief Surcharger cette fonction pour ne pas avoir à refaire les tests en surchargeant
            /// UpdateObservation.\n
            /// Cela vous permet de n'avoir qu'à tester la clef de var pour savoir quoi faire avec les mises à jour
            /// de variables.
            /// \example
            /// \code {.cpp}
            ///     if(m_Color->GetKey() == var->GetKey())
            ///         UpdateColor(std::dynamic_pointer_cast<ma::ColorVar>(var));
            ///
            ///     if(m_Name == var)
            ///         UpdateName();
            /// \endcode
            virtual void UpdateVar(ma::VarPtr var, const ma::Observable::Data &data, wxWindow *window);

            void OnEnable() override;
            void OnDisable() override;

        public:
            struct Key
            {
                    struct Var
                    {
                            static const ma::Var::key_type &GetwxClassName();
                    };
            };
            /// Le nom de la classe de l'élément de la gui.
            /// Pour wxWidgets cela correspond au nom retourné par wxClassInfo::GetClassName
            /// \see https://docs.wxwidgets.org/latest/classwx_class_info.html
            ma::StringVarPtr m_wxClassName;

            /// \brief Le constructeur.
            /// \param in_params Les paramètres de l'item.
            /// \param class_name Le nom de la classe.
            /// \exception std::invalid argument La fenêtre assignée à nul.
            explicit Parameter(const ma::Item::ConstructorParameters &in_params, const std::wstring &class_name);
            Parameter() = delete;
            ~Parameter() override;

            /// Pour pouvoir commencer les observations, il faut attendre que l'item eu été construit.
            void EndConstruction() override;
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Parameter, wx)
    };
    typedef std::shared_ptr<Parameter> ParameterPtr;

    class M_DLL_EXPORT ParameterManager: public ma::Item
    {
        public:
            /// Valeurs de wxSYS_COLOUR_SCROLLBAR
            ma::wxColourVarPtr m_SysColourScrollBar;

            /// Valeurs de wxSYS_COLOUR_DESKTOP
            ma::wxColourVarPtr m_SysColourDesktop;

            /// Valeurs de wxSYS_COLOUR_ACTIVECAPTION
            ma::wxColourVarPtr m_SysColourActiveCaption;

            /// Valeurs wxSYS_COLOUR_INACTIVECAPTION
            ma::wxColourVarPtr m_SysColourInactiveCaption;

            /// Valeurs wxSYS_COLOUR_MENU
            ma::wxColourVarPtr m_SysColourMenu;

            /// Valeurs wxSYS_COLOUR_WINDOW
            ma::wxColourVarPtr m_SysColourWindow;

            /// Valeurs wxSYS_COLOUR_WINDOWFRAME
            ma::wxColourVarPtr m_SysColourWindowFrame;

            /// Valeurs wxSYS_COLOUR_MENUTEXT
            ma::wxColourVarPtr m_SysColourMenuText;

            /// Valeurs wxSYS_COLOUR_WINDOWTEXT
            ma::wxColourVarPtr m_SysColourWindowMenu;

            /// Valeurs wxSYS_COLOUR_CAPTIONTEXT
            ma::wxColourVarPtr m_SysColourCaptionText;

            /// Valeurs wxSYS_COLOUR_ACTIVEBORDER
            ma::wxColourVarPtr m_SysColourActiveBorder;

            /// Valeurs wxSYS_COLOUR_INACTIVEBORDER
            ma::wxColourVarPtr m_SysColourInactiveBorder;

            /// Valeurs wxSYS_COLOUR_APPWORKSPACE
            ma::wxColourVarPtr m_SysColourAppWorkspace;

            /// Valeurs wxSYS_COLOUR_HIGHLIGHT
            ma::wxColourVarPtr m_SysColourHighLight;

            /// Valeurs wxSYS_COLOUR_HIGHLIGHTTEXT
            ma::wxColourVarPtr m_SysColourHighLightText;

            /// Valeurs wxSYS_COLOUR_BTNFACE
            ma::wxColourVarPtr m_SysColourBoutonFace;

            /// Valeurs wxSYS_COLOUR_BTNSHADOW
            ma::wxColourVarPtr m_SysColourBoutonShadow;

            /// Valeurs wxSYS_COLOUR_GRAYTEXT
            ma::wxColourVarPtr m_SysColourGrayText;

            /// Valeurs wxSYS_COLOUR_BTNTEXT
            ma::wxColourVarPtr m_SysColourBoutonText;

            /// Valeurs wxSYS_COLOUR_INACTIVECAPTIONTEXT
            ma::wxColourVarPtr m_SysColourInactiveCaptionText;

            /// Valeurs wxSYS_COLOUR_BTNHIGHLIGHT
            ma::wxColourVarPtr m_SysColourBoutonHighLight;

            /// Valeurs wxSYS_COLOUR_3DDKSHADOW
            ma::wxColourVarPtr m_SysColour3DDarkShadow;

            /// Valeurs wxSYS_COLOUR_3DLIGHT
            ma::wxColourVarPtr m_SysColour3DLight;

            void InitSystemColours();

            explicit ParameterManager(const ma::Item::ConstructorParameters &in_params);
            ParameterManager() = delete;
            ~ParameterManager() override;

            template<typename T_Parameter>
            std::shared_ptr<T_Parameter> GetParameter(const std::wstring &class_name);

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ParameterManager, wx)
    };
    typedef std::shared_ptr<ParameterManager> ParameterManagerPtr;

    class M_DLL_EXPORT WindowParameter: public Parameter
    {
        protected:
            void UpdateVar(ma::VarPtr var, const ma::Observable::Data &data, wxWindow *window) override;

        public:
            struct Key
            {
                    struct Var
                    {
                            static const ma::Var::key_type ms_Id;
                            static const ma::Var::key_type ms_Label;
                    };
            };

            explicit WindowParameter(const ma::Item::ConstructorParameters &in_params);
            WindowParameter() = delete;
            ~WindowParameter() override;

            /// Pour pouvoir début« les observations, il faut attendre que l'item eu été construit.
            void EndConstruction() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(WindowParameter, wx)
    };
    typedef std::shared_ptr<WindowParameter> WindowParameterPtr;

} // namespace ma::wx

#include "ma/wx/Parameter.tpp"

// M_HEADER_MAKERITEM_TYPE(WindowParameter)
