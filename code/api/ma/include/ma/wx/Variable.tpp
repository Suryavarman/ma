/// \file wx/Variable.h
/// \author Pontier Pierre
/// \date 2020-02-06
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Error.h"
#include "ma/Translation.h"
#include "ma/Item.h"
#include "ma/wx/Variable.h"
#include "ma/ObservationManager.h"
#include "ma/Root.h"
#include <wx/mimetype.h>
#include <sstream>

namespace ma::wx
{
    template<typename T>
    Var<T>::Var(typename Var<T>::ptr_type var,
                wxWindow *parent,
                bool add_observation,
                wxWindowID id,
                const wxPoint &pos,
                const wxSize &size,
                long style):
    wxPanel(parent, id, pos, size, style, var ? wxString(var->toString()) : wxPanelNameStr),
    ma::Observer(),
    m_Var(var),
    m_IInfos(nullptr)
    {
        MA_ASSERT(m_Var, L"var est nulle.", std::invalid_argument);

        m_VerticalSizer = new wxBoxSizer(wxVERTICAL);

        m_TopHorizontalSizer = new wxBoxSizer(wxHORIZONTAL);

        m_Name = new wxStaticText(this, wxID_ANY, M_Tr(L"Name"), wxDefaultPosition, wxDefaultSize, 0);
        m_Name->Wrap(-1);
        m_Name->SetToolTip(M_Tr(L"Information sur la variable"));

        m_TopHorizontalSizer->Add(m_Name, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_Warning = new wxStaticBitmap(
            this, wxID_ANY, wxArtProvider::GetBitmap(wxART_ERROR, wxART_TOOLBAR), wxDefaultPosition, wxDefaultSize, 0);
        m_TopHorizontalSizer->Add(m_Warning, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_ResetButton =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_ResetButton->SetBitmap(wxArtProvider::GetBitmap(wxART_UNDO, wxART_BUTTON));
        m_ResetButton->SetFont(wxFont(wxNORMAL_FONT->GetPointSize(),
                                      wxFONTFAMILY_DEFAULT,
                                      wxFONTSTYLE_NORMAL,
                                      wxFONTWEIGHT_NORMAL,
                                      false,
                                      wxEmptyString));
        m_ResetButton->SetToolTip(M_Tr(L"Remet la valeur par défaut."));

        m_TopHorizontalSizer->Add(m_ResetButton, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_DeleteButton =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_DeleteButton->SetBitmap(wxArtProvider::GetBitmap(wxART_DELETE, wxART_BUTTON));
        m_DeleteButton->SetBitmapPosition(wxBOTTOM);
        m_DeleteButton->SetFont(wxFont(wxNORMAL_FONT->GetPointSize(),
                                       wxFONTFAMILY_DEFAULT,
                                       wxFONTSTYLE_NORMAL,
                                       wxFONTWEIGHT_NORMAL,
                                       false,
                                       wxEmptyString));
        m_DeleteButton->SetToolTip(M_Tr(L"Supprime la variable de l'item."));

        m_TopHorizontalSizer->Add(m_DeleteButton, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_DisplayInstanceInfosButton =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_DisplayInstanceInfosButton->SetBitmap(wxArtProvider::GetBitmap(wxART_LIST_VIEW, wxART_BUTTON));
        m_DisplayInstanceInfosButton->SetToolTip(
            M_Tr(L"LAffiche ou cache la grille d'édition des informations de la variable."));

        m_TopHorizontalSizer->Add(m_DisplayInstanceInfosButton, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_VerticalSizer->Add(m_TopHorizontalSizer, 0, wxEXPAND, 5);

        m_BottomHorizontalSizer = new wxBoxSizer(wxVERTICAL);

        auto options = wxHSCROLL | wxTE_AUTO_URL | wxTE_PROCESS_ENTER | wxTE_PROCESS_TAB;

        if(m_Var->m_ReadOnly)
            options |= wxTE_READONLY;

        m_TextCtrlVar = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, options);
        m_TextCtrlVar->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));
        m_TextCtrlVar->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
        m_TextCtrlVar->SetToolTip(M_Tr(L"Valeur de la chaîne de caractères."));

        m_BottomHorizontalSizer->Add(m_TextCtrlVar, 1, wxALL | wxEXPAND, 5);

        m_VerticalSizer->Add(m_BottomHorizontalSizer, 1, wxEXPAND, 5);

        m_InstanceInfosVerticalSizer = new wxBoxSizer(wxVERTICAL);

        m_VerticalSizer->Add(m_InstanceInfosVerticalSizer, 0, wxEXPAND, 5);

        this->SetSizer(m_VerticalSizer);
        this->Layout();

        m_Warning->Hide();
        // m_TextCtrlVar->Enable(!m_Var->m_ReadOnly);
        // m_TextCtrlVar->ReadOnly(!m_Var->m_ReadOnly);
        m_ResetButton->Show(!m_Var->m_ReadOnly);
        m_DeleteButton->Show(m_Var->m_Dynamic);

        if(m_Var->IsObservable())
        {
            if(ma::Item::HasItem(ma::Item::Key::GetObservationManager()) && add_observation)
            {
                auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
                obs_manager->AddObservation<wxObserverVar<typename ma::wx::Var<T>>, ma::VarVar>(this, m_Var);
            }
        }
        else
        {
            m_Warning->Show(true);
            m_Warning->SetToolTip(
                M_Tr(L"Au moment de la création de cette instance «" + m_Var->GetKey() + L"» n'était pas observable."));
            SetVar(m_Var);
        }

        m_Name->Bind(wxEVT_RIGHT_UP, &Var<T>::OnCopyVarKey, this);
        m_ResetButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &Var<T>::OnReset, this);
        m_DeleteButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &Var<T>::OnDelete, this);
        m_DisplayInstanceInfosButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &Var<T>::OnDisplayInstanceInfos, this);
        m_TextCtrlVar->Bind(wxEVT_COMMAND_TEXT_UPDATED, &Var<T>::OnTextChange, this);
        m_TextCtrlVar->Bind(wxEVT_COMMAND_TEXT_ENTER, &Var<T>::OnTextEnter, this);
        m_TextCtrlVar->Bind(wxEVT_COMMAND_TEXT_MAXLEN, &Var<T>::OnTextMaxLen, this);
        m_TextCtrlVar->Bind(wxEVT_COMMAND_TEXT_URL, &Var<T>::OnTextURL, this);
    }

    template<typename T>
    Var<T>::~Var()
    {}

    template<typename T>
    void Var<T>::OnCopyVarKey(wxMouseEvent &event)
    {
        ma::root()->m_wxGuiAccess->SendTextToClipboard(m_Var->GetKey());
        event.Skip();
    }

    template<typename T>
    void Var<T>::OnDisplayInstanceInfos(wxCommandEvent &event)
    {
        if(!m_IInfos)
        {
            m_IInfos = new VarIInfo2(m_Var, this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
            m_InstanceInfosVerticalSizer->Add(m_IInfos, 1, wxALL | wxEXPAND, 5);
        }
        else
            m_IInfos->Show(!m_IInfos->IsShown());

        m_IInfos->Layout();
        this->Layout();

        auto *parent = this->GetParent();
        if(parent)
            parent->Layout();

        event.Skip();
    }

    template<typename T>
    void Var<T>::OnTextEnter(wxCommandEvent &event)
    {
        const std::wstring text = m_TextCtrlVar->GetValue().ToStdWstring();

        if(m_Var->toString() != text)
        {
            if(m_Var->IsValidString(text))
            {
                m_Var->fromString(text);
                m_TextCtrlVar->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));
            }
            else
                m_TextCtrlVar->SetValue(m_Var->toString());
        }

        event.Skip();
    }

    template<typename T>
    void Var<T>::OnTextChange(wxCommandEvent &event)
    {
        const std::wstring text = m_TextCtrlVar->GetValue().ToStdWstring();

        if(m_Var->toString() != text)
        {
            if(m_Var->IsValidString(text))
                m_TextCtrlVar->SetForegroundColour(*wxBLUE);
            else
                m_TextCtrlVar->SetForegroundColour(*wxRED);
        }
        else
        {
            m_TextCtrlVar->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));
        }

        event.Skip();
    }

    template<typename T>
    void Var<T>::OnTextMaxLen(wxCommandEvent &event)
    {
        wxMessageOutputMessageBox().Printf("Vous avez atteint la limite du nombre de caractères maximum autorisée.");
        event.Skip();
    }

    template<typename T>
    void Var<T>::OnTextURL(wxTextUrlEvent &event)
    {
        const std::wstring text = m_TextCtrlVar->GetValue().ToStdWstring();
        const std::wstring url = text.substr(event.GetURLStart(), event.GetURLEnd());

        wxMimeTypesManager manager;
        wxFileType *filetype = manager.GetFileTypeFromExtension("html");
        wxString command = filetype->GetOpenCommand(url);
        wxExecute(command);

        event.Skip();
    }

    template<typename T>
    void Var<T>::OnReset(wxCommandEvent &event)
    {
        m_Var->Reset();
        event.Skip();
    }

    template<typename T>
    void Var<T>::OnDelete(wxCommandEvent &event)
    {
        auto item_key = m_Var->GetItemKey();
        auto item = ma::Item::GetItem(item_key);
        item->EraseVar(m_Var->GetKey());
    }

    template<typename T>
    void Var<T>::SetVar(const ma::ObservablePtr &observable, bool end_observation)
    {
        typename Var<T>::ptr_type var = std::dynamic_pointer_cast<typename ma::wx::Var<T>::value_type>(observable);

        MA_ASSERT(var, L"Impossible de convertir l'observable en ptr_type, car var est nulle", std::invalid_argument);

        MA_ASSERT(
            m_Var == var,
            L"L'observable est différent de l'instance m_Var qui est supposée être observée. wxVarString observe une "
            L"instance à la fois.",
            std::invalid_argument);

        if(end_observation)
        {
            m_Name->SetLabel("");
            m_Name->SetToolTip("");
            m_TextCtrlVar->Clear();
        }
        else
        {
            auto value = m_Var->toString();
            auto type = m_Var->GetTypeInfo();
            m_Name->SetLabel(m_Var->GetKey());

            std::wstringstream desc;
            desc << L"Classe: " << type.m_ClassName << std::endl;
            desc << L"Type: " << type.m_Description << std::endl;

            if(m_Var->HasIInfoKey(ma::Var::Key::IInfo::ms_Doc))
                desc << L"Utilisation: " << m_Var->GetIInfoValue(ma::Var::Key::IInfo::ms_Doc) << std::endl;

            m_Name->SetToolTip(desc.str());

            if(m_TextCtrlVar->GetValue().ToStdWstring() != value && m_Var->IsValidString(value))
                m_TextCtrlVar->SetValue(value);
        }
        this->Layout();
    }

    template<typename T>
    void Var<T>::BeginObservation(const ma::ObservablePtr &observable)
    {
        SetVar(observable);
    }

    template<typename T>
    void Var<T>::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(data.HasActions(ma::Var::Key::Obs::ms_EndSetValue))
        {
            SetVar(observable);
        }

        // Pour veiller à ce que tout soit bien aux bonnes dimensions
        // Car l'ajout ou la suppressions d'informations d'instance peut faire
        // varier la hauteur du panneau m_IInfos et du coup de tout le reste.
        // \todo Voir si il n'y a pas une meilleur manière de faire
        if(m_IInfos && m_IInfos->IsShown() &&
           (data.HasActions(ma::Var::Key::Obs::ms_EndSetIInfo) ||
            data.HasActions(ma::Var::Key::Obs::ms_EndSetIInfoValue) ||
            data.HasActions(ma::Var::Key::Obs::ms_EndAddIInfo) ||
            data.HasActions(ma::Var::Key::Obs::ms_EndRemoveIInfo)))
        {
            m_IInfos->Layout();
            this->Layout();

            auto *parent = this->GetParent();
            if(parent)
                parent->Layout();
        }
    }

    template<typename T>
    void Var<T>::EndObservation(const ma::ObservablePtr &observable)
    {
        SetVar(observable, true);
    }

    template<typename T>
    VarBaseItem<T>::VarBaseItem(VarBaseItem<T>::ptr_type var,
                                wxWindow *parent,
                                wxWindowID id,
                                const wxPoint &pos,
                                const wxSize &size,
                                long style):
    wxPanel(parent, id, pos, size, style, var ? wxString(var->GetKey()) : wxPanelNameStr),
    m_Var(var)
    {
        MA_ASSERT(m_Var, L"var est nulle.", std::invalid_argument);

        wxBoxSizer *horizontal_sizer;
        horizontal_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_Name = new wxStaticText(this, wxID_ANY, M_Tr(L"Nom"), wxDefaultPosition, wxDefaultSize, 0);
        m_Name->Wrap(-1);
        m_Name->SetToolTip(M_Tr(L"Information sur la variable"));

        horizontal_sizer->Add(m_Name, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_staticline = new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL);
        horizontal_sizer->Add(m_staticline, 0, wxEXPAND | wxALL, 5);

        m_bpButton1 =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_bpButton1->SetBitmap(wxNullBitmap);
        m_bpButton1->SetToolTip(M_Tr(L"Image, logo, aperçu, ... de l'item sélectionné."));

        horizontal_sizer->Add(m_bpButton1, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        wxBoxSizer *vertical_sizer = new wxBoxSizer(wxVERTICAL);

        m_TextCtrlVar = new wxTextCtrl(this, wxID_ANY, M_Tr(L"Aucun"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY);
        m_TextCtrlVar->Enable(false);
        m_TextCtrlVar->SetToolTip(M_Tr(L"L'item sélectionné"));

        vertical_sizer->Add(m_TextCtrlVar, 0, wxALL | wxEXPAND, 5);

        wxBoxSizer *horizontal2_sizer;
        horizontal2_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_ButtonSetItemCourant =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_ButtonSetItemCourant->SetBitmap(wxArtProvider::GetBitmap(wxART_GO_BACK, wxART_MENU));
        m_ButtonSetItemCourant->SetToolTip(M_Tr(L"Assigner l'item courant."));

        horizontal2_sizer->Add(m_ButtonSetItemCourant, 0, wxALL, 5);

        m_SearchItem =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_SearchItem->SetBitmap(wxArtProvider::GetBitmap(wxART_FIND, wxART_BUTTON));
        m_SearchItem->SetToolTip(M_Tr(L"Rechercher un item à affecter."));

        horizontal2_sizer->Add(m_SearchItem, 0, wxALL, 5);

        m_ResetButton =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_ResetButton->SetBitmap(wxArtProvider::GetBitmap(wxART_UNDO, wxART_BUTTON));
        m_ResetButton->SetToolTip(M_Tr(L"Remet la valeur par défaut."));

        horizontal2_sizer->Add(m_ResetButton, 0, wxALL, 5);
        vertical_sizer->Add(horizontal2_sizer, 1, wxEXPAND, 5);
        horizontal_sizer->Add(vertical_sizer, 1, wxEXPAND, 5);

        this->SetSizer(horizontal_sizer);
        this->Layout();

        if(m_Var->IsObservable() && ma::Item::HasItem(ma::Item::Key::GetObservationManager()))
        {
            auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
            obs_manager->AddObservation<ma::wxObserverVar<VarBaseItem<T>>, ma::VarVar>(this, m_Var);
        }
    }

    template<typename T>
    VarBaseItem<T>::~VarBaseItem()
    {}

    template<typename T>
    void VarBaseItem<T>::SetVar(const ma::ObservablePtr &observable)
    {
        VarBaseItem<T>::ptr_type var = std::dynamic_pointer_cast<VarBaseItem<T>::value_type>(observable);

        MA_ASSERT(var, L"Impossible de convertir l'observable en ptr_type, car var est nulle", std::invalid_argument);

        MA_ASSERT(
            m_Var != var,
            L"L'observable est différent de l'instance m_Var qui est supposée être observée. wxVarBaseItem observe "
            L"une instance à la fois.",
            std::invalid_argument);

        auto item = m_Var->GetItem();
        SetLabels(item);

        this->Layout();
    }

    template<typename T>
    void VarBaseItem<T>::BeginObservation(const ma::ObservablePtr &observable)
    {
        SetVar(observable);
    }

    template<typename T>
    void VarBaseItem<T>::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(data.HasActions(ma::Var::Key::Obs::ms_EndSetValue))
            SetVar(observable);
    }

    template<typename T>
    void VarBaseItem<T>::EndObservation(const ma::ObservablePtr &observable)
    {
        // static const std::wstring function_name = "void ma::wxVarBaseItem::EndObservation(const ma::ObservablePtr&
        // observable)";
        SetLabels(nullptr);
    }

    template<typename T>
    void VarBaseItem<T>::SetLabels(const ma::ItemPtr &item)
    {
        if(item)
        {
            auto type = item->GetTypeInfo();
            m_Name->SetLabel(m_Var->GetKey());
            std::wstringstream desc;
            desc << L"Classe: " << type.m_ClassName << std::endl;
            desc << L"Type: " << type.m_Description << std::endl;

            if(m_Var->HasIInfoKey(ma::Var::Key::IInfo::ms_Doc))
                desc << L"Utilisation: " << m_Var->GetIInfoValue(ma::Var::Key::IInfo::ms_Doc) << std::endl;

            m_Name->SetToolTip(desc.str());
            m_TextCtrlVar->Clear();
            m_TextCtrlVar->AppendText(item->GetKey());
        }
        else
        {
            m_Name->SetLabel("");
            m_Name->SetToolTip("");
            m_TextCtrlVar->Clear();
        }
    }
} // namespace ma::wx