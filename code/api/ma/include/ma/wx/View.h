/// \file wx/View.h
/// \author Pontier Pierre
/// \date 2024-01-01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/RTTI.h"

/// Doit être contenu en-dehors de tout espace de nom.
#define MA_HEADER_DEF_TAG(in_M_name, in_M_Tag_Heritage)                                                                \
    namespace ma::wx                                                                                                   \
    {                                                                                                                  \
        class in_M_name: public in_M_Tag_Heritage                                                                      \
        {                                                                                                              \
            public:                                                                                                    \
                explicit in_M_name(const ma::Item::ConstructorParameters &in_params);                                  \
                in_M_name() = delete;                                                                                  \
                ~in_M_name() override;                                                                                 \
                M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_name, wx)                                                  \
        };                                                                                                             \
        typedef std::shared_ptr<in_M_name> in_M_name##Ptr;                                                             \
    }                                                                                                                  \
                                                                                                                       \
    M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(in_M_name, wx)

#define MA_CPP_DEF_TAG(in_M_name, in_M_Tag_Heritage, in_M_Description)                                                 \
    namespace ma::wx                                                                                                   \
    {                                                                                                                  \
        in_M_name::in_M_name(const Item::ConstructorParameters &in_params): in_M_Tag_Heritage(in_params)               \
        {}                                                                                                             \
                                                                                                                       \
        in_M_name::~in_M_name() = default;                                                                             \
                                                                                                                       \
        const TypeInfo &in_M_name::Getwx##in_M_name##TypeInfo()                                                        \
        {                                                                                                              \
            const auto tag_typeinfo = in_M_Tag_Heritage::Getwx##in_M_Tag_Heritage##TypeInfo();                         \
                                                                                                                       \
            static const TypeInfo info = {                                                                             \
                Getwx##in_M_name##ClassName(), in_M_Description, tag_typeinfo.MergeDatas({})};                         \
                                                                                                                       \
            return info;                                                                                               \
        }                                                                                                              \
        M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(in_M_name,                                                                \
                                             wx,                                                                       \
                                             ma::wx::in_M_Tag_Heritage::Getwx##in_M_Tag_Heritage##ClassHierarchy())    \
    }                                                                                                                  \
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(in_M_name, wx)

namespace ma
{
    namespace wx
    {
        /// Une étiquette n'est défini que par son type héritant de « Tag » et se différenciant des autres étiquettes.
        class Tag: public Item
        {
            public:
                explicit Tag(const Item::ConstructorParameters &in_params);
                Tag() = delete;
                ~Tag() override;
                M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Tag, wx)
        };
        typedef std::shared_ptr<Tag> TagPtr;

    } // namespace wx

    M_HEADER_ITEM_TYPE_VAR_WITH_NAMESPACE(Tag, wx)
} // namespace ma

MA_HEADER_DEF_TAG(Position, Tag)
MA_HEADER_DEF_TAG(Bottom, Position)
MA_HEADER_DEF_TAG(Top, Position)
MA_HEADER_DEF_TAG(Right, Position)
MA_HEADER_DEF_TAG(Left, Position)
MA_HEADER_DEF_TAG(Center, Position)

MA_HEADER_DEF_TAG(Axis, Tag)
MA_HEADER_DEF_TAG(X, Axis)
MA_HEADER_DEF_TAG(Y, Axis)
MA_HEADER_DEF_TAG(XY, Axis)

MA_HEADER_DEF_TAG(Type, Tag)
MA_HEADER_DEF_TAG(Text, Type)
MA_HEADER_DEF_TAG(Widgets, Type)
MA_HEADER_DEF_TAG(Rendering, Type)
MA_HEADER_DEF_TAG(RealTime, Rendering)
MA_HEADER_DEF_TAG(Precompute, Rendering)

namespace ma::wx
{
    class View: public Item, public Observer
    {
        protected:
            wxWindowID m_WindowID;

            virtual wxWindow *BuildWindow(wxWindow *parent) = 0;

            void OnEnable() override;
            void OnDisable() override;

        public:
            typedef std::set<decltype(TypeInfo::m_ClassName)> tags_type;

            ma::BoolVarPtr m_Display;
            ma::StringVarPtr m_Name;

            explicit View(const Item::ConstructorParameters &in_params, const ma::Var::key_type &name);
            View() = delete;
            ~View() override;
            void EndConstruction() override;

            wxWindow *CreateView(wxWindow *parent);

            /// Permet de notifier de la destruction de la fenêtre de la vue.
            /// \todo utiliser  Bind(wxEVT_DESTROY, &View::ViewDestroyed, this);
            virtual void ViewDestroyed();

            // /// La fenêtre se ferme il faut supprimer le wxParameter
            // void OnClose(wxWindowDestroyEvent &event);

            /// \return Renvoie la fenêtre associée à cette vue. Peut être nul si aucune fenêtre ne lui est associées.
            virtual wxWindow *GetView() const;

            /// \return Renvoie la liste des étiquettes associées à cette item.
            virtual tags_type GetTags() const;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(View, wx)
    };
    typedef std::shared_ptr<View> ViewPtr;

    /// Conteneur de vues.
    class Views: public Item
    {
        public:
            typedef std::deque<ma::wx::ViewPtr> views_type;
            typedef View::tags_type tags_type;
            struct Key
            {
                    /// Clef unique associé à « ma::wx::Views »
                    static const ma::Item::key_type &Get();
            };

            explicit Views(const Item::ConstructorParameters &in_params);
            Views() = delete;
            ~Views() override;
            void EndConstruction() override;

            /// \return La liste des vues instanciées.
            /// \remarks Les actives comme les désactives sont renvoyées.
            views_type GetViews() const;

            /// \return La liste les étiquettes disponibles.
            tags_type GetTags() const;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Views, wx)
    };
    typedef std::shared_ptr<Views> ViewsPtr;
} // namespace ma::wx
