/// \file wx/GuiAccess.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"

#include <wx/aui/aui.h>

#include <iostream>

#include "ma/Prerequisites.h"
#include "ma/wx/Context.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/wx/Parameter.h"
#include "ma/Dll.h"

namespace ma::wx
{
    /// \class GuiAccess
    /// \brief Permet de communiquer entre la GUI et les scripts.
    ///
    /// wxGuiAccess défini une interface unique.
    /// Elle permet d'attribuer et d'accéder aux instances de certains éléments de l'application wxWidgets.
    /// Pour y accéder il vous faudra utiliser ma::Root. ex: root.wx.GetMenuBar()
    ///
    class M_DLL_EXPORT GuiAccess: public ma::Item
    {
        protected:
            using ma::Item::Item;

            /// Instance du menu de l'application.
            wxMenuBar *m_MenuBar;

            /// Instance de la barre d'outils de l'application.
            wxAuiToolBar *m_ToolBar;

            /// Instance du feuillet placé au bas de l'application. Typiquement les onglets de logs.
            wxAuiNotebook *m_BottomNotebook;

            /// Instance du feuillet placé à gauche de l'application. Typiquement les explorateurs d'arborescences.
            wxAuiNotebook *m_LeftNotebook;

            /// Instance du feuillet placé à droite de l'application. Typiquement les explorateurs d'arborescences.
            wxAuiNotebook *m_RightNotebook;

            /// Instance du feuillet centrale de l'application. Typiquement l'édition de fichiers, scènes 3D,
            /// images...
            wxAuiNotebook *m_CenterNotebook;

            /// La fenêtre principale de l'application. Elle contient le menu, la barre à outils et les trois
            /// feuillets.
            wxFrame *m_MotherFrame;

        public:
            /// Le gestionnaire d'apparence des fenêtres wxWidgets.
            ParameterManagerPtr m_ParameterManager;

            /// Définition du contexte pour wxWidgets. Il permet de générer des objets wxWidgets représentant des
            /// ressources.
            ma::wx::ContextPtr m_wxContext;

            explicit GuiAccess(const ConstructorParameters &params);
            GuiAccess() = delete;

            /// Destructeur.
            ~GuiAccess() override;

            /// Accès par pointeur à l'instance de la barre de menu de l'application.
            wxMenuBar *GetMenuBar() const;

            /// Accès par pointeur à l'instance de la barre d'outils de l'application.
            wxAuiToolBar *GetToolBar() const;

            /// Accès par pointeur à l'instance du feuillet placé au bas de l'application.
            wxAuiNotebook *GetBottomNotebook() const;

            /// Accès par pointeur à l'instance du feuillet placé à gauche de l'application.
            wxAuiNotebook *GetLeftNoteBook() const;

            /// Accès par pointeur à l'instance du feuillet placé à droite de l'application.
            wxAuiNotebook *GetRightNoteBook() const;

            /// Accès par pointeur à l'instance du feuillet placé au centre de l'application.
            wxAuiNotebook *GetCenterNoteBook() const;

            /// Accès à par pointeur à l'instance de la fenêtre principale de l'application.
            wxFrame *GetMotherFrame() const;

            /// Affecter l'instance de la barre de menu de l'application.
            void SetMenuBar(wxMenuBar *inMenuBar);

            /// Affecter l'instance de la barre d'outils de l'application.
            void SetToolBar(wxAuiToolBar *inAuiToolBar);

            /// Affecter l'instance du feuillet placé au bas de l'application.
            void SetBottomNotebook(wxAuiNotebook *inNoteBook);

            /// Affecter l'instance du feuillet placé à gauche de l'application.
            void SetLeftNoteBook(wxAuiNotebook *inNoteBook);

            /// Affecter l'instance du feuillet placé à droite de l'application.
            void SetRightNoteBook(wxAuiNotebook *inNoteBook);

            /// Affecter l'instance du feuillet placé au centre de l'application.
            void SetCenterNoteBook(wxAuiNotebook *inNoteBook);

            /// Affecter l'instance de la fenêtre principale de l'application.
            void SetMotherFrame(wxFrame *inFrame);

            /// Permet de copier dans le presse-papier un texte et de notifier la copie du texte dans la barre
            /// d'état.
            bool SendTextToClipboard(const std::wstring &);

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(GuiAccess, wx)
    };
    typedef std::shared_ptr<GuiAccess> GuiAccessPtr;
} // namespace ma::wx
