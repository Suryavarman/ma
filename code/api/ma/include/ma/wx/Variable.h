/// \file wx/Variable.h
/// \author Pontier Pierre
/// \date 2020-02-05
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/statline.h>
#include <wx/bmpbuttn.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/valgen.h>
// #include <wx/stc/stc.h>
// #include <wx/richtext/richtextctrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/grid.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/tglbtn.h>
#include <wx/dnd.h>

#include "ma/Dll.h"
#include "ma/Observer.h"
#include "ma/Variable.h"
#include "ma/RTTI.h"
#include "ma/Translation.h"

namespace ma::wx
{
    /// Affiche les informations de l'instance d'une variable. Les informations sont de type ma::InstanceInfo.
    class VarIInfo: public wxPanel, public ma::Observer
    {
        protected:
            ma::VarPtr m_Var;

            wxBoxSizer *m_VerticalSizer;
            wxBoxSizer *m_GridSizer;
            wxGrid *m_Grid;
            wxBoxSizer *m_HorizontalSizer;
            wxBitmapButton *m_Add;
            wxBitmapButton *m_Remove;

            // Virtual event handlers, override them in your derived class
            virtual void OnSetKeyValue(wxGridEvent &event);
            virtual void OnAddRow(wxCommandEvent &event);
            virtual void OnRemoveSelected(wxCommandEvent &event);

            virtual void SetVar(const ma::ObservablePtr &observable, bool end_observation = false);

            virtual void BeginObservation(const ma::ObservablePtr &observable) override;
            virtual void UpdateObservation(const ma::ObservablePtr &observable,
                                           const ma::Observable::Data &data) override;
            virtual void EndObservation(const ma::ObservablePtr &observable) override;

        public:
            VarIInfo(ma::VarPtr var,
                     wxWindow *parent,
                     wxWindowID id = wxID_ANY,
                     const wxPoint &pos = wxDefaultPosition,
                     const wxSize &size = wxSize(500, -1),
                     long style = wxTAB_TRAVERSAL,
                     const wxString &name = wxEmptyString);

            VarIInfo() = delete;

            virtual ~VarIInfo();

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(VarIInfo, wx)
    };
    typedef ma::wxObserverVar<VarIInfo> VarIInfoVar;
    typedef VarIInfoVar *VarIInfoPtr;

    class VarIInfo2: public wxPanel, public ma::Observer
    {
        protected:
            ma::VarPtr m_Var;

            wxPropertyGrid *m_PropertyGrid;
            wxBitmapButton *m_Append;
            wxBitmapButton *m_Remove;

            virtual void OnSetKeyValue(wxPropertyGridEvent &event);
            virtual void OnAppendProperty(wxCommandEvent &event);
            virtual void OnRemoveProperty(wxCommandEvent &event);

            virtual void SetVar(const ma::ObservablePtr &observable, bool end_observation = false);

            virtual void BeginObservation(const ma::ObservablePtr &observable) override;
            virtual void UpdateObservation(const ma::ObservablePtr &observable,
                                           const ma::Observable::Data &data) override;
            virtual void EndObservation(const ma::ObservablePtr &observable) override;

        public:
            VarIInfo2(ma::VarPtr var,
                      wxWindow *parent,
                      wxWindowID id = wxID_ANY,
                      const wxPoint &pos = wxDefaultPosition,
                      const wxSize &size = wxSize(500, 300),
                      long style = wxTAB_TRAVERSAL,
                      const wxString &name = wxEmptyString);
            virtual ~VarIInfo2();

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(VarIInfo2, wx)
    };
    typedef ma::wxObserverVar<VarIInfo2> VarIInfoVar2;
    typedef VarIInfoVar2 *VarIInfoVar2Ptr;

    class AddIInfo: public wxDialog
    {
        protected:
            ma::VarPtr m_Var;

            wxStaticText *m_KeyStaticText;
            wxTextCtrl *m_KeyTxtCtrl;
            wxButton *m_Append;
            wxButton *m_Cancel;

            virtual void OnText(wxCommandEvent &event);
            virtual void OnTextEnter(wxCommandEvent &event);
            virtual void OnAppend(wxCommandEvent &event);
            virtual void OnCancel(wxCommandEvent &event);

        public:
            AddIInfo(ma::VarPtr var,
                     wxWindow *parent,
                     wxWindowID id = wxID_ANY,
                     const wxString &title = M_Tr(L"Ajouter une variable d'information d'instance"),
                     const wxPoint &pos = wxDefaultPosition,
                     const wxSize &size = wxSize(600, -1),
                     long style = wxDEFAULT_DIALOG_STYLE);
            virtual ~AddIInfo();
            M_HEADER_CLASSHIERARCHY_NO_OVERRIDE_WITH_NAMESPACE(AddIInfo, wx)
    };

    /// Ce template permet d'afficher n'importe quelle variable.
    /// Il est tout de même un template pour permettre de dériver et spécialiser ma::wx::Var pour un type de
    /// variable donnée.
    template<typename T_Var>
    class Var: public wxPanel, public ma::Observer
    {
        public:
            typedef T_Var value_type;
            typedef std::shared_ptr<T_Var> ptr_type;

        protected:
            ptr_type m_Var;

            wxBoxSizer *m_VerticalSizer;
            wxBoxSizer *m_TopHorizontalSizer;
            wxStaticText *m_Name;
            wxStaticBitmap *m_Warning;
            wxBitmapButton *m_ResetButton;
            wxBitmapButton *m_DeleteButton;
            wxBitmapButton *m_DisplayInstanceInfosButton;
            wxBoxSizer *m_BottomHorizontalSizer;
            wxTextCtrl *m_TextCtrlVar;
            wxBoxSizer *m_InstanceInfosVerticalSizer;

            VarIInfo2 *m_IInfos;

            /// À l'aide du clic droit sur le nom d'une variable, vous pouvez copier dans le presse-papier le nom de
            /// la variable.
            virtual void OnCopyVarKey(wxMouseEvent &event);

            virtual void OnReset(wxCommandEvent &event);
            virtual void OnDelete(wxCommandEvent &event);
            virtual void OnDisplayInstanceInfos(wxCommandEvent &event);
            virtual void OnTextChange(wxCommandEvent &event);
            virtual void OnTextEnter(wxCommandEvent &event);
            virtual void OnTextMaxLen(wxCommandEvent &event);
            virtual void OnTextURL(wxTextUrlEvent &event);

            virtual void SetVar(const ma::ObservablePtr &observable, bool end_observation = false);

            virtual void BeginObservation(const ma::ObservablePtr &observable) override;
            virtual void UpdateObservation(const ma::ObservablePtr &observable,
                                           const ma::Observable::Data &data) override;
            virtual void EndObservation(const ma::ObservablePtr &observable) override;

        public:
            /// Constructor - Un nouveau wxVar doit recevoir un parent pour être attachée à une fenêtre.
            /// \param var La variable qui référence une chaîne de caractères.
            ///        Seulement « fromString » et « toString » seront utilisés.
            ///        Ce qui veut dire que « wxVar » peut observer n'importe quelles variables.
            /// \param parent Pointe sur le parent de la fenêtre.
            /// \param add_observation mettez à faux pour que la classe parente puisse appeler elle-même
            ///        « obs_manager->AddObservation<ma::wxObserverVar<typename ma::wxVar<T>>, ma::VarVar>
            ///        (this, m_Var) ».
            ///        Ce qui permet de s'assurer que « ma::wx::Var::BeginObservation » soit appelé au bon moment.
            /// \param winid pointer to a parent window.
            /// \param pos Window position. wxDefaultPosition indicates that
            ///        wxWidgets should generate a default position for the
            ///        window. If using the wxWindow class directly, supply
            ///        an actual position.
            /// \param size Window size. wxDefaultSize indicates that
            ///        wxWidgets should generate a default size for the
            ///        window. If no suitable size can be found, the window
            ///        will be sized to 20x20 pixels so that the window is
            ///        visible but obviously not correctly sized.
            /// \param style Window style. For generic window styles, please
            ///        see wxWindow.
            /// \remarks Le nom du panel sera celui de la variable, ce qui aidera à le retrouver.
            /// \exception std::invalid_argument Le paramètre « var » est nul.
            Var(ptr_type var,
                wxWindow *parent,
                bool add_observation = true,
                wxWindowID winid = wxID_ANY,
                const wxPoint &pos = wxDefaultPosition,
                const wxSize &size = wxDefaultSize,
                long style = wxTAB_TRAVERSAL);

            Var() = delete;

            virtual ~Var();
    };

    /// Classe générique qui permet d'afficher n'importe quelle variable
    class M_DLL_EXPORT VarString: public Var<ma::Var>
    {
        public:
            using Var<ma::Var>::Var;
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(VarString, wx)
    };
    typedef ma::wxObserverVar<VarString> VarStringVar;
    typedef VarStringVar *VarStringVarPtr;

    class M_DLL_EXPORT VarDirectory: public Var<ma::DirectoryVar>
    {
        protected:
            wxButton *m_BrowserButton;

            virtual void OnOpenExplorer(wxCommandEvent &);

        public:
            VarDirectory(ma::DirectoryVarPtr var,
                         wxWindow *parent,
                         wxWindowID winid = wxID_ANY,
                         const wxPoint &pos = wxDefaultPosition,
                         const wxSize &size = wxDefaultSize,
                         long style = wxTAB_TRAVERSAL);

            virtual ~VarDirectory();
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(VarDirectory, wx)
    };
    typedef ma::wxObserverVar<VarDirectory> VarDirectoryVar;
    typedef VarDirectoryVar *VarDirectoryVarPtr;

    class M_DLL_EXPORT VarFileName: public Var<ma::FilePathVar>
    {
        protected:
            wxButton *m_BrowserButton;

            virtual void OnOpenExplorer(wxCommandEvent &);

        public:
            VarFileName(ma::FilePathVarPtr var,
                        wxWindow *parent,
                        wxWindowID winid = wxID_ANY,
                        const wxPoint &pos = wxDefaultPosition,
                        const wxSize &size = wxDefaultSize,
                        long style = wxTAB_TRAVERSAL);

            virtual ~VarFileName();
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(VarFileName, wx)
    };
    typedef ma::wxObserverVar<VarFileName> VarFileNameVar;
    typedef VarFileNameVar *VarFileNameVarPtr;

    /// Affiche l'item pointer par une ma::BaseItemVar
    /// \tparam T un type qui implémente BaseItemVar \example ItemVar
    /// \todo corriger wxVarItem::SetVar m_Var != var
    template<typename T>
    class M_DLL_EXPORT VarBaseItem: public wxPanel, public ma::Observer
    {
        public:
            typedef T value_type;
            typedef std::shared_ptr<T> ptr_type;

        protected:
            ptr_type m_Var;

            wxStaticText *m_Name;
            wxStaticLine *m_staticline;
            wxBitmapButton *m_bpButton1;
            wxTextCtrl *m_TextCtrlVar;
            wxBitmapButton *m_ButtonSetItemCourant;
            wxBitmapButton *m_SearchItem;
            wxBitmapButton *m_ResetButton;

            void SetVar(const ma::ObservablePtr &observable);
            void SetLabels(const ma::ItemPtr &);

            void BeginObservation(const ma::ObservablePtr &observable) override;
            void UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ma::ObservablePtr &observable) override;

        public:
            /// Constructor - Un nouveau wxVarBaseItem doit recevoir un parent pour être attachée à une fenêtre.
            /// \param var La variable qui référence un item.
            /// \param parent pointer to a parent window.
            /// \param win_id pointer to a parent window.
            /// \param pos Window position. wxDefaultPosition indicates that
            ///        wxWidgets should generate a default position for the
            ///        window. If using the wxWindow class directly, supply
            ///        an actual position.
            /// \param size Window size. wxDefaultSize indicates that
            ///        wxWidgets should generate a default size for the
            ///        window. If no suitable size can be found, the window
            ///        will be sized to 20x20 pixels so that the window is
            ///        visible but obviously not correctly sized.
            /// \param style Window style. For generic window styles, please see
            ///        wxWindow.
            /// \remarks Le nom du panel sera celui de la variable, ce qui aidera à le retrouver.
            /// \exception std::invalid_argument Le paramètre « var » est nulle.
            VarBaseItem(ptr_type var,
                        wxWindow *parent,
                        wxWindowID win_id = wxID_ANY,
                        const wxPoint &pos = wxDefaultPosition,
                        const wxSize &size = wxDefaultSize,
                        long style = wxTAB_TRAVERSAL);

            VarBaseItem() = delete;

            virtual ~VarBaseItem();
    };

    struct M_DLL_EXPORT VarItem: public VarBaseItem<ItemVar>
    {
            using VarBaseItem<ItemVar>::VarBaseItem;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(VarItem, wx)
    };

    class M_DLL_EXPORT ItemListDropTarget: public wxTextDropTarget
    {
        public:
            ma::ItemHashSetVarPtr m_Var;

            ItemListDropTarget(ma::ItemHashSetVarPtr var);
            virtual ~ItemListDropTarget();

            virtual bool OnDropText(wxCoord x, wxCoord y, const wxString &text);
            M_HEADER_CLASSHIERARCHY_NO_OVERRIDE_WITH_NAMESPACE(ItemListDropTarget, wx)
    };

    class M_DLL_EXPORT ItemList: public Var<ma::ItemHashSetVar>
    {
        protected:
            wxBoxSizer *m_OptionsHorizontalSizer;
            wxBitmapButton *m_RemoveButton;
            wxButton *m_InsertionOrder;
            wxButton *m_AlphabeticalOrder;
            wxBitmapButton *m_AscendingOrder;
            wxBitmapButton *m_DescendingOrder;
            wxTextCtrl *m_KeyOrderTextControl;
            wxListBox *m_ItemsList;

            typedef std::unordered_map<ma::Item::key_type, std::pair<std::wstring, size_t>> key_str_index_map;
            /// Permet d'associer la clef de l'item à la chaîne de caractères utilisée pour l'identifier dans
            /// m_ItemsList.
            /// Ce qui permet de retrouver un item associé.
            /// Index sert à stocker l'index de l'itérateur dans le conteneur.
            /// Tous les index sont mis à jour à chaque insertion et suppression.
            key_str_index_map m_KeyStrMap;

            typedef std::map<std::wstring, std::pair<ma::Item::key_type, size_t>, std::less<std::wstring>>
                str_key_index_map;

            /// Permet d'associer la chaîne de caractères d'un item contenu dans « m_ItemsList » à la clef de
            /// l'item.
            /// Ce qui permet de retrouver un item associé.
            /// Index sert à stocker l'index de l'itérateur dans le conteneur.
            /// Tous les index sont mis à jour à chaque insertion et suppression.
            str_key_index_map m_StrKeyMap;

            enum Sort
            {
                Insertion_Ascending, // default
                Insertion_Descending,
                Alphabetical_Ascending,
                Alphabetical_Descending
            };

            /// L'ordre courant de la liste.
            /// \remarks Par défaut la valeur est « Sort::Insertion_Ascending ».
            Sort m_CurrentOrder;

            /// Reconstruit m_ItemsList en fonction de l'ordre choisi. Et met à jour m_CurrentOrder.
            /// \param sort_type L'ordre choisi
            /// \param force_update Si vrais la liste sera reconstruite même si « sort_type » est égale à
            /// « m_CurrentOrder ».
            virtual void SetupOrder(Sort sort_type, bool force_update = false);

            virtual void OnInsertOrder(wxCommandEvent &event);
            virtual void OnAlphabeticalOrder(wxCommandEvent &event);
            virtual void OnAscendingOrder(wxCommandEvent &event);
            virtual void OnDescendingOrder(wxCommandEvent &event);
            virtual void OnTextSearch(wxCommandEvent &event);
            virtual void OnTextEnterSearch(wxCommandEvent &event);
            virtual void OnItemsSelect(wxCommandEvent &event);
            virtual void OnItemsDClick(wxCommandEvent &event);
            virtual void OnDelete(wxCommandEvent &event) override;

            virtual void BeginObservation(const ma::ObservablePtr &observable) override;
            virtual void UpdateObservation(const ma::ObservablePtr &observable,
                                           const ma::Observable::Data &data) override;
            virtual void EndObservation(const ma::ObservablePtr &observable) override;

            /// Ajoute un item à m_ItemsList
            virtual void Append(const ma::ItemPtr &item);

            /// Quitte un item à m_ItemsList
            virtual void Remove(const ma::ItemPtr &item);

            /// Reconstruit entièrement la liste affichée.
            virtual void RebuildList();

            /// Met à jour les valeurs d'index de m_KeyStrMap et m_StrKeyMap.
            virtual void RebuildIndex();

            /// Retourne le nom qui sera afficher dans la liste pour représenter cet item.
            virtual std::wstring GetDisplayName(const ma::ItemPtr &item);

        public:
            using Var<ma::ItemHashSetVar>::Var;

            ItemList(ma::ItemHashSetVarPtr var,
                     wxWindow *parent,
                     wxWindowID win_id = wxID_ANY,
                     const wxPoint &pos = wxDefaultPosition,
                     const wxSize &in_size = wxSize(332, -1),
                     long style = wxTAB_TRAVERSAL,
                     const wxString &name = wxEmptyString);
            virtual ~ItemList();

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ItemList, wx)
    };
    typedef wxObserverVar<ItemList> ItemListVar;
    typedef ItemListVar *ItemListVarPtr;

    M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarItem, wx)
    M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarString, wx)
    M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarDirectory, wx)
    M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarFileName, wx)
    M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(ItemList, wx)

} // namespace ma::wx

#include "ma/wx/Variable.tpp"
