/// \file wx/AddVarDialog.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"
#include "ma/Dll.h"
#include "ma/Sort.h"

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/checklst.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/html/htmlwin.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

namespace ma::wx
{
    /// Affiche une fenêtre de dialogue.
    /// Elle permet de choisir le type de la variable à créer et à ajouter dans l'item passé en paramètre.
    /// \exception std::invalid_argument La clef de l'item n'est associé à aucun item.
    class M_DLL_EXPORT AddVarDialog: public wxDialog
    {
        private:
            /// Ajoute la variable.
            void Add();

        protected:
            wxStaticText *m_KeyStaticText;
            wxTextCtrl *m_KeyTextCtrl;
            wxListBox *m_TypesListBox;
            wxHtmlWindow *m_HTMLWin;
            wxButton *m_ValidateButton;
            wxButton *m_CancelButton;

            ma::ItemPtr m_Item;

            // Virtual event handlers, override them in your derived class
            virtual void OnText(wxCommandEvent &);
            virtual void OnTextEnter(wxCommandEvent &);
            virtual void OnAdd(wxCommandEvent &);
            virtual void OnCancel(wxCommandEvent &);
            virtual void OnSelected(wxCommandEvent &event);

            virtual wxArrayString GetList();
            virtual void SetHtml();

        public:
            AddVarDialog(const ma::Item::key_type &key,
                         wxWindow *parent,
                         wxWindowID id = wxID_ANY,
                         const wxString &title = wxEmptyString,
                         const wxPoint &pos = wxDefaultPosition,
                         const wxSize &size = wxSize(758, 523),
                         long style = wxDEFAULT_DIALOG_STYLE);

            ~AddVarDialog() override;
    };

} // namespace ma::wx
