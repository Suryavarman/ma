/// \file wx/Parameter.cpp
/// \author Pontier Pierre
/// \date 2020-09-01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Error.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/ObservationManager.h"
#include "ma/ClassInfo.h"

namespace ma::wx
{
    template<typename T_Parameter>
    std::shared_ptr<T_Parameter> ParameterManager::GetParameter(const std::wstring &class_name)
    {
        typedef MatchVar<StringVar, T_Parameter> Match;
        auto deque = ma::Find<Match>::GetDeque(GetPtr(), Match(), ma::Item::SearchMod::LOCAL);
        return deque.empty() ? std::shared_ptr<T_Parameter>{} : *deque.begin();
    }

} // namespace ma::wx