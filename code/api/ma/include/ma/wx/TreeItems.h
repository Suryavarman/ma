/// \file wx/TreeItems.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/bmpbuttn.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/treectrl.h>
#include <wx/panel.h>

#include "ma/Dll.h"
#include "ma/Id.h"

namespace ma::wx
{
    /// Permet de faire le lien entre les « ma::Item » et « wxTreeItemId ».
    /// \remarks Pour récupérer le wxTreeItemId il vous faudra utiliser la fonction «wxTreeItemData::GetId()» qui
    /// retourne un «wxTreeItemId».
    /// \see <a href="https://docs.wxwidgets.org/trunk/classwx_tree_item_data.html" rel="noopener noreferrer"
    /// target="_blank">wxTreeItemData</a>
    class M_DLL_EXPORT TreeItemData: public ::wxTreeItemData
    {
        public:
            const ma::Item::key_type m_Key;

            explicit TreeItemData(ma::Item::key_type);
            virtual ~TreeItemData();
    };

    typedef std::unordered_map<ma::Item::key_type, wxTreeItemId> wxTreeKeyIdMap;

    class M_DLL_EXPORT TreeItems: public wxPanel, public ma::Observer
    {
        protected:
            wxBitmapButton *m_Add;
            wxBitmapButton *m_Remove;
            wxTextCtrl *m_Search;
            wxBitmapButton *m_ExecuteSearch;
            /// \see https://docs.wxwidgets.org/3.0/overview_treectrl.html
            wxTreeCtrl *m_TreeCtrl;

            wxTreeKeyIdMap m_KeyIdMap;

            /// Le menu qui s'affiche lors du clic droit sur un item de l'arbre.
            wxMenu m_ContextMenu;

            /// Liste des items qui ne peuvent être supprimer via wxTreeItems
            std::unordered_set<ma::Item::key_type> m_ItemsNotRemovable;

            virtual void OnAdd(wxCommandEvent &event);
            virtual void OnRemove(wxCommandEvent &event);
            virtual void OnSearch(wxCommandEvent &event);
            virtual void OnTreeRemove(wxTreeEvent &event);
            virtual void OnActivated(wxTreeEvent &event);
            virtual void OnSelect(wxTreeEvent &event);
            virtual void OnTreeItemMenu(wxTreeEvent &event);
            virtual void OnBeginDrag(wxTreeEvent &event);
            virtual void OnEndDrag(wxTreeEvent &event);
            virtual void OnMenu(wxTreeEvent &event);

            /// \return La clef de l'item de ms_Map associée à l'item de
            ///         l'arbre.
            /// \exception std::runtime_error Si la clef associée à l'item de
            ///            labre n'est pas présente dans ma::Item::ms_Map.
            /// \exception std::runtime_error Si la récupération du
            ///            ma::wxTreeItemData a échouée.
            /// \exception std::invalid_argument Si la wxTreeItemId n'est pas
            ///            valide.
            virtual ma::Item::key_type GetKey(const wxTreeItemId &) const;

            virtual wxTreeItemId GetId(const ma::Item::key_type &) const;
            bool HasId(const ma::Item::key_type &) const;

            /// \param var La variable observée qui sert à définir un label en
            /// plus du nom de la classe.
            /// \param end_observation Si vrais alors la variable à notifiée la
            /// fin de son observation et le label devra donc être vide.
            virtual void SetItemName(const ma::VarPtr &var, bool end_observation = false);

            virtual void Remove();

            void BeginObservation(const ma::ObservablePtr &observable) override;
            void UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ma::ObservablePtr &observable) override;

        public:
            TreeItems() = delete;

            /// Constructor - Un nouveau wxTreeItems doit recevoir un parent
            ///               pour être attachée à une fenêtre.
            ///    \param in_parent pointer to a parent window.
            ///    \param in_id identifiant de cette nouvelle fenêtre.
            ///    \param in_pos La position du panel. wxDefaultPosition est la
            ///           postion par défaut du panel.
            ///    \param in_size La taille du panel. wxDefaultSize est la
            ///           taille par défaut du panel. Si aucune taille
            ///           convenable n'est fournises la taille du panel sera de
            ///           20x20 pixels.
            ///    \param in_style Window style. Pour les styles génériques
            ///           \see wxWindow.
            ///    \param name Le nom du panel.
            explicit TreeItems(wxWindow *in_parent,
                               wxWindowID in_id = wxID_ANY,
                               const wxPoint &in_pos = wxDefaultPosition,
                               const wxSize &in_size = wxDefaultSize,
                               long in_style = wxTAB_TRAVERSAL,
                               const wxString &in_name = wxPanelNameStr);

            virtual ~TreeItems();

        private:
            /// Si vrais alors il ne faut pas ajouter les observables qui ne sont
            /// pas observables. Cela se fera lors d'une deuxième passe.
            bool m_InitializationState;

            /// Fonction qui détermine si on peut observer les variables d'un
            /// item et si oui ajoute celles que wxTreeItems observent.
            void AddVarObservations(const ma::ItemPtr &);

            void ShowTreeTooltip(wxTreeEvent &event);

        public:
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(TreeItems, wx)
    };

    typedef ma::wxObserverVar<TreeItems> TreeItemsVar;
    typedef TreeItemsVar *TreeItemsVarPtr;

} // namespace ma::wx
