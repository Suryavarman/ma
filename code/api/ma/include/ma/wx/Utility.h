/// \file wx/Utility.h
/// \author Pontier Pierre
/// \date 2020-05-24
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/Item.h"
#include "ma/wx/wx.h"

#include <wx/object.h>

namespace ma::wx
{
    /// Permet de lier une clef à un wxObject
    /// \see https://docs.wxwidgets.org/trunk/classwx_object_ref_data.html
    template<typename T>
    class ObjectKey: public wxObject
    {
        public:
            ObjectKey();
            ObjectKey(const T &key);
            virtual ~ObjectKey();

            bool IsOk() const
            {
                return m_refData != nullptr;
            }

            bool operator==(const ObjectKey &obj) const;
            bool operator!=(const ObjectKey &obj) const
            {
                return !(*this == obj);
            }

            void SetKey(const T &key);
            T GetKey() const;

        protected:
            virtual wxObjectRefData *CreateRefData() const;
            virtual wxObjectRefData *CloneRefData(const wxObjectRefData *data) const;
    };

    class M_DLL_EXPORT ObjectItemKey: public ObjectKey<ma::Item::key_type>
    {
        public:
            using ObjectKey<ma::Item::key_type>::ObjectKey;

        protected:
            wxDECLARE_DYNAMIC_CLASS(ObjectItemKey);
    };

    class M_DLL_EXPORT ObjectVarKey: public ObjectKey<ma::Var::key_type>
    {
        public:
            using ObjectKey<ma::Var::key_type>::ObjectKey;

        protected:
            wxDECLARE_DYNAMIC_CLASS(ObjectVarKey);
    };

    template<typename T>
    class KeyRefData: public wxObjectRefData
    {
        public:
            KeyRefData(): wxObjectRefData(), m_Key()
            {}

            KeyRefData(const KeyRefData &data): wxObjectRefData(), m_Key(data.m_Key)
            {
                // copy refcounted data; this is usually a time- and memory-consuming operation
                // and is only done when two (or more) wxObjectKey instances need to unshare a
                // common instance of wxKeyRefData
                m_Key = data.m_Key;
            }

            bool operator==(const KeyRefData &data) const
            {
                if(&data != this)
                    return m_Key == data.m_Key;
                else
                    return m_Key;
            }
            // private:
            // in real world, reference counting is usually used only when
            // the wxObjectRefData-derived class holds data very memory-consuming;
            // in this example the various wxKeyRefData instances may share a wxKeyRefData
            // instance which however only takes some bytes for this key!
            T m_Key;
    };
} // namespace ma::wx

#include "ma/wx/Utility.tpp"
