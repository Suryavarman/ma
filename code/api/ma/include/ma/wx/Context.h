/// \file wx/Context.h
/// \author Pontier Pierre
/// \date 2020-03-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Context.h"
#include "ma/VariableEnum.h"
#include "ma/Dll.h"

namespace ma::wx
{
    // class Context;
    // typedef std::shared_ptr<ma::wx::Context> ContextPtr;

    /// Énumération représentant wxBitmapType.
    /// Sa représentation en chaîne de caractères se fait ainsi :
    /// \code
    /// R"#({{"wxBITMAP_TYPE_INVALID ", wxBITMAP_TYPE_INVALID }, {"wxBITMAP_TYPE_BMP", wxBITMAP_TYPE_BMP}, …})#"
    /// \endcode
    class M_DLL_EXPORT BitmapTypesVar: public T_EnumerationVar<std::wstring, unsigned int>
    {
        public:
            using T_EnumerationVar<std::wstring, unsigned int>::T_EnumerationVar;
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(BitmapTypesVar, wx)
    };
    typedef std::shared_ptr<BitmapTypesVar> BitmapTypesVarPtr;

    struct BitmapTypes;

#define M_BitmapTypeChoiceVarBase                                                                                      \
    ma::T_StaticEnumVar<std::wstring, unsigned int, ma::wx::BitmapTypes, std::less<std::wstring>>
    /// Choix d'une clef provenant d'une énumération représentant
    /// wxBitmapType.
    /// Sa représentation en chaîne de caractères se fait ainsi :
    /// \code
    /// R"#({"wxBITMAP_TYPE_INVALID", {"2798396d-d211-40b9-8b8f-55a631d74032", "m_BitmapTypes"}})#"
    /// //  {choix, {clef de l'item, clef de la variable}}
    /// \endcode
    class M_DLL_EXPORT BitmapTypeChoiceVar: public M_BitmapTypeChoiceVarBase
    {
        public:
            using M_BitmapTypeChoiceVarBase::T_StaticEnumVar;
            M_HEADER_CLASSHIERARCHY_NO_OVERRIDE_WITH_NAMESPACE(BitmapTypeChoiceVar, wx)
    };
    typedef std::shared_ptr<ma::wx::BitmapTypeChoiceVar> BitmapTypeChoiceVarPtr;
#undef M_BitmapTypeChoiceVarBase

    /// Un foncteur pour instancier et retourner la référence de l'énumération
    struct M_DLL_EXPORT BitmapTypes
    {
            static ma::wx::BitmapTypeChoiceVar::name_value_type &Get();
    };

    class M_DLL_EXPORT ExtensionsVar: public T_Var<std::set<std::wstring>>
    {
        public:
            using T_Var<std::set<std::wstring>>::T_Var;
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ExtensionsVar, wx)
    };
    typedef std::shared_ptr<ExtensionsVar> ExtensionsVarPtr;

    class M_DLL_EXPORT ImageHandler: public ma::Item
    {
        private:
            const wxImageHandler &m_Handler;

        public:
            /// Nom du Handler.
            ma::StringVarPtr m_Name;

            /// Les extensions associées à ce format d'image.
            /// \remarks Les chaînes de caractères sont garanties d'être des minuscules.
            ma::wx::ExtensionsVarPtr m_Extensions;

            /// Le type de l'image géré.
            ma::wx::BitmapTypeChoiceVarPtr m_BitmapType;

            ImageHandler() = delete;
            explicit ImageHandler(const ma::Item::ConstructorParameters &in_params, const wxImageHandler &handler);
            ~ImageHandler() override;

            std::shared_ptr<ImageHandler> Clone(const Item::key_type &key) const;

            /// \return Le gestionnaire de wxWidgets pour charger et sauvegarder ce type d'image.
            const wxImageHandler &GetHandler();

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ImageHandler, wx)
    };
    typedef std::shared_ptr<ImageHandler> ImageHandlerPtr;

    class M_DLL_EXPORT Context: public ma::Context
    {
        private:
            void Init();

        public:
            struct Key
            {
                    static const ma::Var::key_type &GetBitmapTypes();
                    static const ma::Item::key_type &GetContex();
            };

            /// Représentation de wxBitmapType
            ma::wx::BitmapTypesVarPtr m_BitmapTypes;

            /// Liste des extensions des fichiers images qui sont gérés par wxWidgets.
            /// \remarks Les chaînes de caractères sont garanties d'être des minuscules.
            ma::wx::ExtensionsVarPtr m_BitmapExtensions;

            explicit Context(const ma::Item::ConstructorParameters &in_params, const std::wstring &name);
            Context() = delete;
            ~Context() override;

            ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry) override;

            /// \return Vrais si la ressource représente un format d'image géré par la classe ma::wx::Image.
            /// \remark Le résultat dépend du context. Elle n'est donc pas statique.
            bool IsImage(ma::ResourcePtr resource);

            /// \return La ressource est chargée et convertie en wxImage.
            /// \exception std::invalid_argument La ressource n'est pas image ou si elle l'est le format de celle-ci
            /// n'est pas géré.
            // ma::wx::ImagePtr Get(ma::ResourcePtr resource);

            /// Retourne la liste des formats d'images gérés. La clef est le nom du format.
            /// Les clefs sont triées par ordre alphabétique.
            /// \remarks Les ma::wx::ImageHandlerPtr sont enfants du context.
            std::unordered_map<std::wstring, ma::wx::ImageHandlerPtr> GetImageHandlers() const;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Context, wx)
    };
    typedef std::shared_ptr<ma::wx::Context> ContextPtr;

    /// Implémenter Data pour charger un type d'item dans un contexte donné
    class M_DLL_EXPORT Image: public ma::Data
    {
        public:
            /// Le contexte dans lequel la ressource sera lue.
            ma::wx::ContextPtr m_wxContext;

            /// La ressource représentant (peut-être) une image.
            ma::ResourcePtr m_Resource;

            /// Si la ressource représente une image elle sera charger dans celle-ci.
            wxImage m_Image;

            explicit Image(const Item::ConstructorParameters &in_params,
                           const ContextLinkPtr &link,
                           ma::ItemPtr client);
            Image() = delete;
            ~Image() override;

            /// Charge la donnée dans le contexte.
            virtual bool Load() override;

            /// Décharge la donnée du contexte.
            virtual bool UnLoad() override;

            /// Par défaut le comportement est d'abord d'appeler Unload et si
            /// celui-ci renvoie vrais
            virtual bool Reload() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Image, wx)
    };
    typedef std::shared_ptr<Image> ImagePtr;
} // namespace ma::wx

namespace ma
{
    template M_DLL_EXPORT ma::wx::BitmapTypesVarPtr
    ma::Item::AddVar<ma::wx::BitmapTypesVar>(const ma::Var::key_type &key,
                                             const ma::wx::BitmapTypesVar::value_type &value);
    template M_DLL_EXPORT ma::wx::BitmapTypeChoiceVarPtr
    ma::Item::AddVar<ma::wx::BitmapTypeChoiceVar>(const ma::Var::key_type &key,
                                                  const ma::wx::BitmapTypeChoiceVar::value_type &value);
} // namespace ma

// M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Context, wx)
