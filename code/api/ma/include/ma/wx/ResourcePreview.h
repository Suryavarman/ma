/// \file wx/ResourcePreview.h
/// \author Pontier Pierre
/// \date 2021-06-22
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Variable.h"
#include "ma/Resource.h"
#include "ma/wx/Context.h"
#include "ma/Dll.h"

namespace ma::wx
{
    class M_DLL_EXPORT ResourcePreviewer: public ma::Item
    {
        protected:
            /// Génère le rendu de l'aperçu dans la variable « image ».
            /// \param res La ressource dont on souhaite générer l'aperçu.
            /// \param out_image L'image qui sera remplie par les données de l'aperçu.
            /// \return Vrais si la prévisualisation, à la résolution demandée, a peu avoir été faite et copiée dans
            /// « out_image ».
            /// \remarks Est appelé par Get après que la ressource et l'image ont été testées.
            virtual bool FillImage(ma::ResourcePtr res, wxImage &out_image) = 0;

        public:
            using ma::Item::Item;

            /// Permet de savoir si cette visionneuse peut être utilisé.
            /// Exemple Si le contexte utilisé par la visionneuse n'est pas présent « Usable » retournera « false ».
            virtual bool Usable() const = 0;

            /// Génère le rendu de l'aperçu dans « image ».
            /// \param res La ressource dont on souhaite générer l'aperçu.
            /// \param out_image L'image qui sera remplie par les données de
            /// l'aperçu.
            /// \return Vrais si la prévisualisation, à la résolution demandée, a peu avoir été faite et copiée dans
            /// « out_image ».
            /// \exception std::invalid_argument la ressource est un aperçu.
            /// Ou l'image possède une largeur et/ou hauteur de 0.
            /// \remarks Cette fonction appel FillImage après avoir testé la ressource et l'image.
            virtual bool Get(ma::ResourcePtr res, wxImage &out_image);
    };
    typedef std::shared_ptr<ma::wx::ResourcePreviewer> ResourcePreviewertPtr;

    /// \see https://docs.wxwidgets.org/trunk/artprov_8h.html#af7d82bed13ba95dd8ba357f0cb4bdfbd
    class M_DLL_EXPORT ArtProviderPR: public ResourcePreviewer
    {
        protected:
            virtual bool FillImage(ma::ResourcePtr res, wxImage &image);

        public:
            using ma::wx::ResourcePreviewer::ResourcePreviewer;

            /// Permet de savoir si cette visionneuse peut être utilisée.
            /// Exemple si le contexte qui sera utilisé n'est pas présent.
            virtual bool Usable() const;
    };
    typedef std::shared_ptr<ma::wx::ResourcePreviewer> ResourcePreviewertPtr;

    /// Le plus grand aura la priorité.
    /// \see ma::wx::ResourcePreviewMap
    struct ResourcePreviewPriorityCmp
    {
            /// int = priorité,
            /// std::set<std::wstring> {extensions sans le «.»},
            /// ma::wx::ResourcePreviewerPtr la visionneuse
            typedef std::tuple<int, std::set<std::wstring>, ma::wx::ResourcePreviewertPtr> value_type;

            constexpr bool operator()(const value_type &lhs, const value_type &rhs) const
            {
                return std::get<0>(lhs) > std::get<0>(rhs);
            }
    };

    /// \code {.cpp}
    /// //  priorité, {extensions}, la visionneuse
    /// {
    ///     {10, {L"obj", L"fbx"}, CycleRP},
    ///     { 1, {L"jpg", L"bmp", L"png"}, IconeRP},
    ///     { 2, {L"jpg"}, ImageRP},
    ///     { 2, {L"tiff"}, ImageRP}
    ///     { 0, {}, ArtProviderPR} // <--- Ce dernier gère tout les types
    ///                                     de ressources.
    /// }
    /// \endcode
    /// Pour une ressource donnée la visionneuse sélectionnée sera
    /// celle qui aura la priorité la plus élevée.
    /// Dans l'exemple ci-dessus la visionneuse sélectionnée pour un
    /// fichier:
    /// - jpg sera ImageRP.
    /// - obj sera CycleRP
    /// - txt sera ArtProvielerPR
    class M_DLL_EXPORT ResourcePreviewMap:
    public ma::T_Var<ma::priority_queue<ma::wx::ResourcePreviewPriorityCmp::value_type,
                                        std::vector<ma::wx::ResourcePreviewPriorityCmp::value_type>,
                                        ma::wx::ResourcePreviewPriorityCmp>>,
        public ma::Observer
    {
        protected:
            virtual void SetFromItem(const std::wstring &value) override;
            virtual ma::VarPtr Copy() const override;

            typedef std::map<std::wstring, ma::wx::ResourcePreviewertPtr> AccessMap;
            /// Permet de récupérer facilement la visionneuse associée à un
            /// type de ressource.
            /// \remarks Est mis à jour à chaque modification de m_Value
            AccessMap m_AccessMap;

        public:
            using ma::T_Var<ma::priority_queue<ma::wx::ResourcePreviewPriorityCmp::value_type,
                                               std::vector<ma::wx::ResourcePreviewPriorityCmp::value_type>,
                                               ma::wx::ResourcePreviewPriorityCmp>>::T_Var;

            /// \param resource La ressource qui utilisée pour chercher si
            /// il y a une visionneuse pour la visualiser.
            /// \return Vrais si il y une visionneuse qui gère cette
            /// ressource.
            bool HasPreviewer(const ma::ResourcePtr &res) const;

            /// \return La visionneuse qui doit rendre un aperçu de la
            /// ressource «res».
            /// \exception std::invalid_argument La ressource ne peut être
            /// afficher par aucune des visionneuses présentes.
            ma::wx::ResourcePreviewertPtr GetPreviewer(ma::ResourcePtr res);

            /// Concrétisation de ma::Observer
            void BeginObservation(const ObservablePtr &) override;

            /// Concrétisation de ma::Observer
            void UpdateObservation(const ObservablePtr &, const ma::Observable::Data &) override;

            /// Concrétisation de ma::Observer
            void EndObservation(const ObservablePtr &) override;
    };
    typedef std::shared_ptr<ma::wx::ResourcePreviewMap> ResourcePreviewMapPtr;

    /// Permet de générer un visuel d'une ressource. Ce visuel pourra en
    /// fonction des règles définies être un icône, un rendu 3D ou une
    /// miniature d'une image.
    /// \todo Voir si il faut utiliser :
    /// https://docs.wxwidgets.org/trunk/classwx_bitmap_bundle.html
    class M_DLL_EXPORT ResourcePreviewManager: public ma::Item
    {
        private:
            void Init();

        protected:
            /// \param resource La ressource dont ont recherche le dossier
            /// où est stocké les aperçus de celle-ci.
            /// \return Retourne le dossier où est stocké l'aperçu
            /// de la ressource. Si le dossier n'existe pas il sera créé.
            /// \remarks Le dossier sera nommé «.ma_preview» et sera
            /// adjacent au fichier ressource.
            ma::FolderPtr GetPreviewFolder(ma::ResourcePtr resource);

        public:
            /// \return la clef de la variable m_RPs
            static const ma::Var::key_type &GetRPsKey();

            /// Liste des visionneuses de ressources.
            ResourcePreviewMapPtr m_RPs;

#if(MA_PLATFORM == MA_PLATFORM_WIN32)
            ResourcePreviewManager() = delete;
#endif
            explicit ResourcePreviewManager(const ma::Item::ConstructorParameters &in_params);
            virtual ~ResourcePreviewManager();

            /// \param resource La ressource qui sera chargée puis dont
            /// l'aperçu sera généré.
            /// \param width La largeur de l'image exprimée en nombre de
            /// pixels.
            /// \param height La hauteur de l'image exprimée en nombre de
            /// pixels.
            /// \return L'aperçu de la ressource.
            /// \exception std::invalid_argument La ressource n'est pas
            /// gérée par l'ensemble des ResourcePreviewer contenu dans
            /// m_RPs.
            /// \remarks le fichier de l'aperçu se nommera :
            /// resource_mame_width_height. Il sera placé dans le dossier
            /// .ma_preview adjacent au fichier ressource.
            ma::wx::ImagePtr Get(ma::ResourcePtr resource, unsigned int width, unsigned int height);

            /// Renvoie vrais si cette ressource a un fichier image la
            /// représentant de largeur «width» et de hauteur «height».
            /// \param resource La ressource dont on cherche à savoir si
            /// elle a un aperçu de largeur «width» et de hauteur «hegiht».
            /// \param width La largeur de l'image exprimée en nombre de
            /// pixels.
            /// \param height La hauteur de l'image exprimée en nombre de
            /// pixels.
            bool HasPreview(ma::ResourcePtr resource, unsigned int width, unsigned int height) const;

            /// \return Vrais si le dossier est un dossier utilisé pour
            /// stocker des aperçus.
            static bool IsPreviewFolder(ma::FolderPtr folder);

            /// \return Vrais si la ressource représente un aperçu.
            static bool IsPreviewResource(ma::ResourcePtr res);

            /// \return Vrais si la ressource représente un aperçu.
            static bool IsPreviewResource(const ma::Item::key_type &key);

            /// Retourne la liste des formats d'images gérés. La clef est le
            /// nom du format. Les clef sont triées par ordre alphabétique.
            /// \remarks Les ma::wx::ImageHandlerPtr sont enfants du
            /// context.
            std::unordered_map<std::wstring, ma::wx::ImageHandlerPtr> GetImageHandlers() const;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ResourcePreviewManager, wx)
    };
    typedef std::shared_ptr<ma::wx::ResourcePreviewManager> ResourcePreviewManagertPtr;
} // namespace ma::wx
