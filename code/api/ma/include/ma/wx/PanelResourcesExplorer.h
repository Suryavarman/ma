/// \file wx/PanelResourcesExplorer.h
/// \author Pontier Pierre
/// \date 2020-04-04
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/button.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#include <wx/bmpbuttn.h>
#include <wx/scrolwin.h>
#include <wx/panel.h>

#include "ma/Dll.h"
#include "ma/Observer.h"
#include "ma/Variable.h"
#include "ma/Resource.h"
#include "ma/wx/Parameter.h"
#include "ma/Colour.h"
#include "ma/wx/View.h"

namespace ma::wx
{
    class PanelResourcesExplorer;

    class M_DLL_EXPORT PanelResourcesExplorerParameter: public Parameter
    {
        protected:
            void UpdateVar(ma::VarPtr var, const ma::Observable::Data &data, wxWindow *window) override;

        public:
            struct Key
            {
                    static const ma::Item::key_type &Get();
            };

            /// Le nombre de dossiers affichés dans l'en tête des ressources.
            /// \see ma::wxPanelResourcesExplorer
            ma::UVarPtr m_CountDisplayResPath;

            /// La définition en nombre de pixels de la largeur d'un panneau représentant un fichier ou un dossier.
            ma::UVarPtr m_PanelItemWidth;

            /// La définition en nombre de pixels de la hauteur d'un panneau représentant un fichier ou un dossier.
            ma::UVarPtr m_PanelItemHeight;

            /// Couleur de fond des éléments représentant les fichiers et les
            /// dossiers.
            ma::wxColourVarPtr m_ItemsBackgroundColour;

            /// Couleur de devant des éléments représentant les fichiers et les
            /// dossiers.
            ma::wxColourVarPtr m_ItemsForegroundColour;

            /// Couleur d'arrière-plan pour signifier le passage de la souris sur un des éléments représentant les
            /// fichiers et les dossiers.
            ma::wxColourVarPtr m_ItemsHoverBackgroundColour;

            /// Couleur de premier plan pour signifier le passage de la souris sur un des éléments représentant les
            /// fichiers et les dossiers.
            ma::wxColourVarPtr m_ItemsHoverForegroundColour;

            /// Couleur d'arrière-plan pour signifier la sélection des éléments représentant les fichiers et les
            /// dossiers.
            ma::wxColourVarPtr m_ItemsSelectedBackgroundColour;

            /// Couleur de premier plan pour signifier la sélection des éléments représentant les fichiers et les
            /// dossiers.
            ma::wxColourVarPtr m_ItemsSelectedForegroundColour;

            /// L'item qui a été choisi pour construire la vue.
            /// \remarks Si m_CurrentItemVar->GetItem() est un fichier, alors « m_ObserverdItem » sera le parent de
            /// cette ressource.
            /// Si m_CurrentItemVar->GetItem() ne contient ni dossier ni ressource alors
            /// m_ObserverdItem ne sera pas modifier.
            /// Si m_CurrentItemVar->GetItem() contient un ou plusieurs dossiers et/ou ressources alors
            /// m_ObserverdItem sera m_CurrentItemVar->GetItem().
            /// \see UpdateObservedItem()
            ma::ItemVarPtr m_ObservedItem;

            explicit PanelResourcesExplorerParameter(const ConstructorParameters &in_params);
            PanelResourcesExplorerParameter() = delete;
            ~PanelResourcesExplorerParameter() override;

            /// Pour pouvoir débuter les observations, il faut attendre que l'item eu été construit.
            void EndConstruction() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(PanelResourcesExplorerParameter, wx);
    };
    typedef std::shared_ptr<PanelResourcesExplorerParameter> PanelResourcesExplorerParameterPtr;

    class M_DLL_EXPORT PanelResourceItem: public wxPanel, public ma::Observer
    {
            friend PanelResourcesExplorerParameter;

        protected:
            /// Identifiant de l'évènement qui lancera la copie de la clef de l'item de type ressource dans le
            /// presse-papier.
            static const int ms_ID_CopyKey;

            /// Identifiant de l'évènement qui lancera la copie du chemin du fichier.
            static const int ms_ID_CopyPath;

            /// Ouvre l'explorateur de fichiers de l'os à l'adresse du fichier.
            static const int ms_ID_OpenFileExplorer;

            /// Ouvre l'éditeur associé au type de fichier.
            static const int ms_ID_OpenFileEditor;

            /// Image de la ressource.
            wxStaticBitmap *m_FileBitmap;

            /// Le nom de la ressource.
            wxStaticText *m_FileNameStaticText;

            /// L'item de type ressource qui est observé.
            ma::ResourcePtr m_Resource;

            /// Le menu contextuel qui s'affiche lors d'un clic droit.
            wxMenu m_PopupMenu;

            /// Référence vers la variable qui contient l'item observé par l'explorateur de ressources.
            ma::ItemVarPtr m_ObservedItem;

            /// Instance de l'item possédant les paramètres d'affichage personnalisable.
            PanelResourcesExplorerParameterPtr m_Parameter;

            /// Permet de savoir si la sourie survole cette fenêtre.
            bool m_Hover;

            /// Permet de savoir l'opération de glisser déposé à débuter.
            bool m_DragBegin;

            void BeginObservation(const ma::ObservablePtr &observable) override;
            void UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ma::ObservablePtr &observable) override;

            virtual void OnCopyKey(wxCommandEvent &event);
            virtual void OnCopyPath(wxCommandEvent &event);

            /// Ouvre le dossier de la ressource dans l'explorateur de dossier du système d'exploitation.
            /// \exception std::logic_error La ressource a pour parent un item qui n'est pas du type « ma::Folder ».
            virtual void OnOpenFileExplorer(wxCommandEvent &event);

            /// Ouvre la ressource dans l'éditeur associé par défaut à ce type de ressource par le système
            /// d'exploitation.
            virtual void OnOpenFileEditor(wxCommandEvent &event);

            virtual void OnLeftDown(wxMouseEvent &event);
            virtual void OnLeftUp(wxMouseEvent &event);
            virtual void OnLeftDClick(wxMouseEvent &event);
            virtual void OnRightClick(wxMouseEvent &event);
            virtual void OnEnterWindow(wxMouseEvent &event);
            virtual void OnLeaveWindow(wxMouseEvent &event);
            virtual void OnMouseMove(wxMouseEvent &event);

            /// Met à jour le rendu de la sélection ou de la non-sélection de la ressource.
            virtual void UpdateSelectionRendering();

        public:
            PanelResourceItem() = delete;
            PanelResourceItem(const ma::ResourcePtr &resource,
                              PanelResourcesExplorerParameterPtr parameter,
                              wxWindow *parent,
                              wxWindowID id = wxID_ANY,
                              const wxPoint &pos = wxDefaultPosition,
                              const wxSize &size = wxSize(100, 100),
                              long style = wxTAB_TRAVERSAL,
                              const wxString &name = wxEmptyString);
            ~PanelResourceItem() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(PanelResourceItem, wx);
    };
    typedef ma::wxObserverVar<PanelResourceItem> PanelResourceItemVar;
    typedef PanelResourceItemVar *PanelResourceItemVarPtr;

    class M_DLL_EXPORT PanelFolderItem: public wxPanel, public ma::Observer
    {
            friend PanelResourcesExplorerParameter;

        protected:
            /// Identifiant de l'évènement qui lancera la copie de la clef de l'item de type dossier dans le
            /// presse-papier.
            static const int ms_ID_CopyKey;

            /// Identifiant de l'évènement qui lancera la copie du chemin du dossier.
            static const int ms_ID_CopyPath;

            /// Ouvre l'explorateur de fichiers de l'os à l'adresse du dossier.
            static const int ms_ID_OpenFileExplorer;

            /// Image représentant le dossier.
            wxStaticBitmap *m_FileBitmap;

            /// Nom du dossier.
            wxStaticText *m_FileNameStaticText;

            /// L'item de type dossier qui est observé.
            ma::FolderPtr m_Folder;

            /// Le menu contextuel qui s'affiche lors d'un clic droit.
            wxMenu m_PopupMenu;

            /// Référence vers la variable qui contient l'item observé par l'explorateur de ressources.
            ma::ItemVarPtr m_ObservedItem;

            /// Instance de l'item possédant les paramètres d'affichage
            /// personnalisable.
            PanelResourcesExplorerParameterPtr m_Parameter;

            /// Permet de savoir si la sourie survole cette fenêtre.
            bool m_Hover;

            void BeginObservation(const ma::ObservablePtr &observable) override;
            void UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ma::ObservablePtr &observable) override;

            virtual void OnCopyKey(wxCommandEvent &event);
            virtual void OnCopyPath(wxCommandEvent &event);
            virtual void OnOpenFileExplorer(wxCommandEvent &event);
            virtual void OnBeginDrag(wxMouseEvent &event);

            virtual void OnLeftClick(wxMouseEvent &event);
            virtual void OnRightClick(wxMouseEvent &event);

            virtual void OnEnterWindow(wxMouseEvent &event);
            virtual void OnLeaveWindow(wxMouseEvent &event);

            /// Met à jour le rendu de la sélection ou de la non sélection de la dossier.
            virtual void UpdateSelectionRendering();

        public:
            PanelFolderItem(const ma::FolderPtr &folder,
                            PanelResourcesExplorerParameterPtr parameter,
                            wxWindow *parent,
                            wxWindowID id = wxID_ANY,
                            const wxPoint &pos = wxDefaultPosition,
                            const wxSize &size = wxSize(100, 100),
                            long style = wxTAB_TRAVERSAL,
                            const wxString &name = wxEmptyString);
            ~PanelFolderItem() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(PanelFolderItem, wx);
    };
    typedef ma::wxObserverVar<PanelFolderItem> PanelFolderItemVar;
    typedef PanelFolderItemVar *PanelFolderItemVarPtr;

    class M_DLL_EXPORT PanelResourcesExplorer: public wxPanel, public ma::Observer
    {
            friend PanelResourcesExplorerParameter;

        private:
            /// Permet de stopper la mise à jour infinie de l'item courant.
            /// \todo Réfléchir à une solution plus générique et/ou une solution qui évite ce problème.
            bool m_EndObservationBegin;

            /// Permet de savoir que la variable courante est en train de changer de valeur.
            /// Cela permet d'optimiser la génération de l'affichage.
            bool m_SetCurrentItem;

            /// Permet d'indiquer de ne pas appeler la fonction EndObservation.
            bool m_NoEndObservation;

            /// Redimensionne la grille, en ajustant le nombre de colonnes et de lignes.
            void ResizeGrid();

            /// Supprime l'observation de l'ancien item et commence l'observation du nouvel item.
            /// \return vrais si le nouvel item est observé par this.
            bool SetNewItem(ma::ItemPtr old_item, ma::ItemPtr new_item);

        protected:
            typedef std::vector<wxButton *> Buttons;
            typedef std::vector<wxStaticText *> Separators;
            typedef std::unordered_map<ma::Item::key_type, wxPanel *> wxPanelItems;

            /// Les boutons de l'en-tête, représentant la hiérarchie des dossiers parents du dossier courant.
            /// \remarks Si l'item courant n'est pas un dossier, mais contient des dossiers ou des ressources, il
            /// peut être affiché par wxPanelResourcesExplorer et par conséquent m_DirButtons sera vide.
            Buttons m_DirButtons;

            /// Les séparateurs de l'en-tête, représentant la hiérarchie des dossiers parents du dossier courant.
            /// \remarks si l'item courant n'est pas un dossier, mais contient des dossiers ou des ressources, il
            /// peut être affiché par wxPanelResourcesExplorer et par conséquent m_DirSeparators sera vide.
            Separators m_DirSeparators;

            wxScrolledWindow *m_ScrolledWindow;
            wxGridSizer *m_GridSizer;

            wxBoxSizer *m_HeaderHorizontalSizer;

            /// Les panels qui ont été créés pour afficher les dossiers et les
            /// fichiers.
            wxPanelItems m_PanelsItems;

            /// L'item courant. C'est un raccourci.
            /// Si l'item courant est un dossier alors l'explorateur affichera
            /// ce dossier.
            /// Si l'item courant est une ressource alors l'explorateur affichera le dossier contenant la ressource
            /// et sélectionnera le fichier lié à la ressource. \see ma::Root::current
            ma::ItemVarPtr m_CurrentItemVar;

            /// Permet de définir une interface qui manipulera les paramètres graphiques de l'explorateur de
            /// ressources.
            PanelResourcesExplorerParameterPtr m_Parameters;

            void BeginObservation(const ma::ObservablePtr &observable) override;
            void UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ma::ObservablePtr &observable) override;

            virtual void OnSize(wxSizeEvent &event);

            /// À partir du chemin met en place l'en-tête.
            virtual void SetupDir();

            /// Supprime tous les panels représentants des ressources et des dossiers.
            virtual void Clear();

            /// Permet de construire la grille de ressources et de dossiers.
            virtual void BuildGrid();

            /// Permet de mettre à jour le choix de l'item à observer.
            /// \see m_ObserverdItem
            virtual void UpdateObservedItem(ma::ItemVarPtr var);

        public:
            /// Juste pour être compatible avec la RTTI de wxWidgets
            PanelResourcesExplorer();

            explicit PanelResourcesExplorer(wxWindow *parent,
                                            const wxString &name = wxEmptyString,
                                            wxWindowID id = wxID_ANY,
                                            const wxPoint &pos = wxDefaultPosition,
                                            const wxSize &size = wxSize(500, 300),
                                            long style = wxTAB_TRAVERSAL);
            ~PanelResourcesExplorer() override;

            /// Ma RTTI
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(PanelResourcesExplorer, wx);

        private:
            /// wxWidgets RTTI
            /// Pour être utilisable par : « wx::Parameter »
            wxDECLARE_DYNAMIC_CLASS_NO_COPY(PanelResourcesExplorer);
    };
    typedef ma::wxObserverVar<PanelResourcesExplorer> PanelResourcesExplorerVar;
    typedef PanelResourcesExplorerVar *PanelResourcesExplorerVarPtr;

    //         auto* resource_explorer = new ma::wx::PanelResourcesExplorer(m_AuiNotebookBottom, _(L"Explorateur de
    //         ressources"));
    //        title = _(L"Explorateur de ressources.");
    //        m_AuiNotebookBottom->AddPage(resource_explorer,
    //                                     title,
    //                                     false,
    //                                     wxArtProvider::GetBitmap(wxART_FOLDER, wxART_FRAME_ICON));
    class PanelResourcesExplorerView: public View
    {
        protected:
            wxWindow *BuildWindow(wxWindow *parent) override;

        public:
            struct Key
            {
                    static const ma::Item::key_type &Get();
            };

            /// \remarks Params ne sera pas utilisé.
            explicit PanelResourcesExplorerView(const ma::Item::ConstructorParameters &params = {});
            PanelResourcesExplorerView() = delete;
            ~PanelResourcesExplorerView() override;
            void EndConstruction() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(PanelResourcesExplorerView, wx);
    };
    typedef std::shared_ptr<PanelResourcesExplorerView> PanelResourcesExplorerViewPtr;

} // namespace ma::wx

M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(PanelResourcesExplorerView, wx)
M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(PanelResourcesExplorerParameter, wx)
