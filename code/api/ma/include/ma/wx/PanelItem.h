/// \file wx/PanelItem.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"

#include <wx/spinctrl.h>
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
// #include <wx/propgrid/propgrid.h>
// #include <wx/propgrid/advprops.h>
#include <wx/panel.h>

#include "ma/Dll.h"
#include "ma/Observer.h"
#include "ma/Variable.h"

namespace ma::wx
{
    /// Affiche l'item courant \see ma::Root::current, c'est-à-dire le premier de la liste.
    class M_DLL_EXPORT PanelItem: public wxPanel, public ma::Observer
    {
        protected:
            /// Le pointeur sur la variable qui stocke un item.
            ma::ItemVarPtr m_ItemVar;

            wxBoxSizer *m_VerticalSizer;
            wxStaticBitmap *m_Logo;
            wxStaticText *m_ClassName;
            wxStaticText *m_CountChild;
            wxBitmapButton *m_Add;
            wxStaticText *m_TextKey;
            wxStaticText *m_Key;
            wxScrolledWindow *m_ScrolledWindow;

            /// Stock les panneaux liés aux variables.
            std::unordered_map<ma::Var::key_type, wxWindow *> m_Panels;

            void SetLabels();
            void SetPanels();

            /// \remarks N'oubliez pas d'appeler «this->Layout();» à la suite des appels à AddPanel.
            void AddPanel(const ma::Var::key_type &key);

            /// \remarks N'oubliez pas d'appeler «this->Layout();» à la suite des appels à RemovePanel.
            void RemovePanel(const ma::Var::key_type &key);
            void ClearPanels();

            void BeginObservation(const ma::ObservablePtr &observable) override;
            void UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ma::ObservablePtr &observable) override;

            /// Un clic droit sur le nom de la classe copiera le nom de la classe dans le presse-papier.
            virtual void OnCopyClassName(wxMouseEvent &);

            /// Un clic droit sur l'identifiant de l'item copiera l'identifiant de l'item dans le presse-papier.
            virtual void OnCopyId(wxMouseEvent &);

            /// Ouvre une fenêtre de dialogue qui permettra de choisir le type de variable que vous souhaitez
            /// ajouter.
            virtual void OnAddVar(wxCommandEvent &);

        public:
            PanelItem() = delete;

            /// Constructeur - Un nouveau wxPanelItem doit recevoir un parent
            /// pour être attachée à une fenêtre.
            ///    \param parent pointer to a parent window.
            ///    \param win_id pointer to a parent window.
            ///    \param pos Window position. wxDefaultPosition indicates that wxWidgets should generate a default
            ///           position for the window. If using the wxWindow class directly, supply an actual position.
            ///    \param size Window size. wxDefaultSize indicates that wxWidgets should generate a default size
            ///           for the  window. If no suitable size can be found, the window will be sized to 20x20
            ///           pixels so that the window is visible but obviously not correctly sized.
            ///    \param style Window style. For generic window styles, please see wxWindow.
            ///    \param name wxVarBaseItem name.
            explicit PanelItem(wxWindow *parent,
                               wxWindowID win_id = wxID_ANY,
                               const wxPoint &pos = wxDefaultPosition,
                               const wxSize &size = wxDefaultSize,
                               long style = wxTAB_TRAVERSAL,
                               const wxString &name = wxPanelNameStr);

            ~PanelItem() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(PanelItem, wx);
    };

    typedef ma::wxObserverVar<PanelItem> PanelItemVar;
    typedef PanelItemVar *PanelItemVarPtr;
} // namespace ma::wx
