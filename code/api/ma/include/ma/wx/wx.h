/// \file wx/wx.h
/// \author Pontier Pierre
/// \date 2020-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
///  Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Prerequisites.h"
#include "ma/String.h"

#if defined(M_NOGUI) && defined(wxUSE_GUI)
    #undef wxUSE_GUI
#endif

#if MA_COMPILER == MA_COMPILER_CLANG
    // https://clang.llvm.org/docs/UsersManual.html#controlling-diagnostics-via-pragmas
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Winconsistent-missing-override"
#endif // MA_COMPILER_CLANG

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#if MA_COMPILER == MA_COMPILER_CLANG
    #pragma clang diagnostic pop
#endif // MA_COMPILER_CLANG

#if !defined(M_NOGUI)
    #include <wx/textctrl.h>
namespace ma::wx
{
    /// A appeler pour l'utiliser:
    /// \code{.cpp}wxSetAssertHandler(ma::wx::TheAssertHandler);\endcode
    /// \see src/common/appbase.cpp et include/wx/debug.h
    M_DLL_EXPORT void
    AssertHandler(const wxString &file, int line, const wxString &func, const wxString &cond, const wxString &msg);

    /// Spécifier ici le wxTextCtrl où seront notifier les avertissements et les erreurs.
    /// \see https://docs.wxwidgets.org/stable/classwx_stream_to_text_redirector.html
    extern wxTextCtrl *g_TextCtrl;

    extern const wxColour g_TextErrorBackgroundColour;
    extern const wxColour g_TextWarningBackgroundColour;
    extern const wxColour g_TextDefaultBackgroundColour;
    extern const wxColour g_TextErrorColour;
    extern const wxColour g_TextWarningColour;
    extern const wxColour g_TextDefaultColour;
    extern const wxFont g_TextFont;

    /// Attribut du texte représentent les erreurs.
    extern const wxTextAttr g_TextErrorAttr;

    /// Attribut du texte représentent les avertissements.
    extern const wxTextAttr g_TextWarningAttr;

    /// Attribut du texte représentent les avertissements.
    extern const wxTextAttr g_TextDefaultAttr;
} // namespace ma::wx

#endif

// M_DLL_EXPORT std::wstring operator = (const wxString &);
