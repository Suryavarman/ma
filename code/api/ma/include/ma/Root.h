/// \file Root.h
/// \author Pontier Pierre
/// \date 2019-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Prerequisites.h"
#include "ma/Item.h"
#include "ma/RTTI.h"
#include "ma/ClassInfo.h"
#include "ma/ObservationManager.h"
#include "ma/Module.h"

#ifndef M_NOGUI
    #include "ma/wx/GuiAccess.h"
    #include "ma/wx/View.h"
#endif

#include "ma/Resource.h"
#include "ma/Context.h"
#include "ma/Garbage.h"
#include "ma/Config.h"
#include "ma/Dll.h"

/// Raccourcis pour accéder à la référence du singleton.
/// \example
/// \code{.cpp}
/// auto item = MA_ROOT->GetItem(key);
/// \endcode
#define MA_ROOT ma::Item::GetRoot<ma::Root>()

namespace ma
{
    class Root;

    typedef std::shared_ptr<Root> RootPtr;

    class M_DLL_EXPORT Root: public Item
    {
        public:
            static const ma::Var::key_type &GetKeyCurrentVarProperty();

            explicit Root(const Item::ConstructorParameters &in_params,
                          const std::filesystem::path &in_config_path = std::filesystem::current_path() /
                                                                        L"config.cfg");
            Root() = delete;
            ~Root() override;

            void EndConstruction() override;

            /// La poubelle d'items.
            /// Un item une fois sorti de « ma::Item::ms_Map », sera placé dans cette poubelle.
            /// Si seul la poubelle possède une référence de cet item, il sera détruit au premier appel de
            /// « m_Garbage->CleanTrashes() ».
            GarbagePtr m_Garbage;

            // Début de la définition des gestionnaires
            // ————————————————————————————————————————————————————————————————————

            /// Permet de stocker toutes les hiérarchies des classes de l'API.
            ClassInfoManagerPtr m_ClassInfoManager;

            /// Permet d'enregistrer les usines à item (Maker).
            /// Elles fabriqueront des items en fonction du nom de la classe donné en paramètre.
            RTTIPtr m_RttiManager;

            /// Permet de gérer les liens d'observation entre les observables et les observateurs.
            ObservationManagerPtr m_ObservationManager;

            /// Gestion des ressources.
            ResourceManagerPtr m_ResourceManager;

            /// Gestion des contextes
            ContextManagerPtr m_ContextsManager;

            // Fin de la définition des gestionnaires

            /// Le fichier de configuration de l'application. Il ne sert pas à sauvegarder le projet.
            /// Il sert à sauvegarder/ des éléments de configuration de cette application.
            ConfigItemPtr m_Config;

            /// L'item courant.
            /// \todo Utiliser une liste d'items
            ItemVarPtr m_Current;

#ifndef M_NOGUI
            /// Permet d'accéder aux éléments de la « GUI » de l'application, mis à disposition par celle-ci.
            /// \remarks N'est compilé que si wxUSE_GUI est défini.
            wx::GuiAccessPtr m_wxGuiAccess;

            /// Contient les vues qui seront affichées et masquée par le gestionnaire de vue de l'application.
            /// C'est à ce gestionnaire qu'il est conféré la tâche de placer et afficher les vues. Le modules n'ont pas
            /// connaissance du fonctionnement de l'application. Il doivent donc enregistrer leurs vues ici.
            /// \remarks Chaque vue est associé à une liste d'étiquettes qui aideront l'application à déterminer la
            /// place de ces vues.
            wx::ViewsPtr m_Views;
#endif

            /// Le gestionnaire de modules.
            /// Il contient la liste des fabrique de modules et les modules qui ont été chargés.
            ModuleManagerPtr m_ModulesManager;

            /// Permet de s'assurer que le chemin de la racine soit correctement initialisé.
            /// \param project_path Le chemin du projet. Le parent du dossier où la racine sera sauvegardé.
            /// \example
            /// %user%/.ma/gui/projects/project_0
            void SetSavePath(const std::filesystem::path &project_path);

            M_HEADER_CLASSHIERARCHY(Root)
    };

    /// Raccourcis pour accéder au root.
    /// \example
    /// \code{.cpp}
    /// auto root = ma::root();
    /// menubar = root()->wx.GetMenuBar();
    /// \endcode
    /// \see (https://www.fluentcpp.com/2017/10/27/function-aliases-cpp/)
    const auto root = ma::Item::GetRoot<ma::Root>;
} // namespace ma
