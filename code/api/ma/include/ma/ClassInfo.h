/// \file ClassInfo.h
/// \author Pontier Pierre
/// \date 2020-02-11
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/String.h"
#include "ma/TypeInfo.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/Dll.h"

namespace ma
{
    class M_DLL_EXPORT ClassInfo: public Item
    {
        public:
            /// Clef de la variable m_Description.
            static const ma::Var::key_type ms_KeyDescriptionVar;

            /// Clef de la variable m_ParentClasses
            static const ma::Var::key_type ms_KeyParentClassesVar;

            explicit ClassInfo(const Item::ConstructorParameters &in_params, const ma::ClassHierarchy &hierarchy = {});
            ClassInfo() = delete;
            ~ClassInfo() override; ///< Destructeur

            /// Le nom de la classe.
            ma::StringVarPtr m_Name;

            /// La description de la classe
            ma::StringVarPtr m_Description;

            /// La liste par ordre alphabétique des parents de classe.
            /// \remarks Permet de tester rapidement si une classe est parente de celle-ci.
            ma::StringSetVarPtr m_ParentClasses;

            //            SetClassInfoVarPtr m_ChildrenClasses;
            //
            //            ma::ClassHierarchy GetClassHierarchy();
            //            std::unordered_set<std::wstring> GetChildrenClass();
            //            virtual ma::TypeInfo GetClassTypeInfo() const;

            M_HEADER_CLASSHIERARCHY(ClassInfo)
    };
    typedef std::shared_ptr<ClassInfo> ClassInfoPtr;

    class M_DLL_EXPORT ClassInfoManager: public Item
    {
        protected:
            /// Si l'instance de ClassInfoManager existe alors IsInherit appellera cette fonction.
            /// Cette fonction doit tester l'héritage de l'observable en testant les hiérarchies de classes définies
            /// statiquement et dynamiquement, d'où les deux dernières lettre DS à la fin du nom de la fonction.
            /// \return Vrais si «child_class_name» hérite de la classe nommée
            /// «parent_class_name» ou si il est égale à «parent_class_name».
            /// \param child_class_name Nom de classe qui sera tester sur son
            /// héritage à parent_class_name.
            /// \param parent_class_name Le nom de classe parente à tester.
            /// \todo A faire/A implémenter
            virtual bool IsInheritDS(const std::wstring &child_class_name, const std::wstring &parent_class_name);

            /// Si l'instance de ClassInfoManager n'existe pas alors IsInherit appellera cette fonction.
            /// Cette fonction doit tester l'héritage de l'observable en testant les hiérarchies de classes définies
            /// statiquement, d'où la dernière lettre S  à la fin du nom de la fonction.
            /// \return Vrais si «child_class_name» hérite de la classe nommée
            /// «parent_class_name» ou si il est égale à «parent_class_name».
            /// \param child_class_name Nom de classe qui sera tester sur son
            /// héritage à parent_class_name.
            /// \param parent_class_name Le nom de classe parente à tester.
            static bool IsInheritS(const std::wstring &child_class_name, const std::wstring &parent_class_name);

        public:
            explicit ClassInfoManager(const Item::ConstructorParameters &in_params);
            ClassInfoManager() = delete;
            ~ClassInfoManager() override; ///< Destructeur

            /// Test si l'instance de ClassInfoManager existe:
            /// \li si oui le test d'héritage se fera avec la définition des
            /// classes ajoutées dynamiquement et statiquement. \todo le faire.
            /// \li si non le test d'héritage se fera seulement avec la
            /// définition des classes définies statiquement.
            static bool IsInherit(const ma::ObservablePtr &obs, const std::wstring &parent_class_name);

            /// Test si l'instance de ClassInfoManager existe:
            /// \li si oui le test d'héritage se fera avec la définition des classes ajoutées dynamiquement et
            /// statiquement. \todo le faire.
            /// \li si non le test d'héritage se fera seulement avec la définition des classes définies statiquement.
            static bool IsInherit(const std::wstring &child_class_name, const std::wstring &parent_class_name);

            /// Test si l'instance de ClassInfoManager existe:
            /// \li si oui l'ensemble des hiérarchies définies dynamiquement et statiquement sera envoyé. \todo à faire.
            /// \li si non l'ensemble des hiérarchies définies statiquement sera envoyé.
            static ma::TypeInfo::ClassHierarchies GetClassHierarchies();

            void BuildHierarchies();

            M_HEADER_CLASSHIERARCHY(ClassInfoManager)
    };
    typedef std::shared_ptr<ClassInfoManager> ClassInfoManagerPtr;
} // namespace ma
