/// \file Algorithm.h
/// \author Pontier Pierre
/// \date 2020-03-22
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/String.h"
#include <vector>
#include <string_view>

namespace ma
{
    M_DLL_EXPORT std::vector<std::string> Split(const std::string &s, char delimiter);
    M_DLL_EXPORT std::vector<std::wstring> Split(const std::wstring &s, wchar_t delimiter);

    /// Quitte tous les espaces.
    /// \param str La chaîne de caractères à traiter.
    /// \see https://en.cppreference.com/w/cpp/string/wide/iswspace
    M_DLL_EXPORT std::wstring Trim(const std::wstring &str, bool only_blank = false);

    M_DLL_EXPORT std::wstring TrimStart(const std::wstring &str, bool only_blank = false);
    M_DLL_EXPORT std::wstring TrimEnd(const std::wstring &str, bool only_blank = false);
    M_DLL_EXPORT std::wstring TrimStartEnd(const std::wstring &str, bool only_blank = false);

    M_DLL_EXPORT std::wstring_view TrimStart(const std::wstring_view &str, bool only_blank = false);
    M_DLL_EXPORT std::wstring_view TrimEnd(const std::wstring_view &str, bool only_blank = false);
    M_DLL_EXPORT std::wstring_view TrimStartEnd(const std::wstring_view &str, bool only_blank = false);

    /// Dans inout remplace toutes les itération de « what » par « with ».
    M_DLL_EXPORT std::size_t ReplaceAll(std::wstring &inout, std::wstring_view what, std::wstring_view with);

    /// Supprime de « inout » toutes les itérations de « what ».
    M_DLL_EXPORT std::size_t RemoveAll(std::wstring &inout, std::wstring_view what);
} // namespace ma
