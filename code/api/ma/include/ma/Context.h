/// \file Context.h
/// \author Pontier Pierre
/// \date 2020-03-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/Resource.h"
#include "ma/RTTI.h"
#include "ma/Dll.h"

#include <optional>

#define M_HEADER_MAKERDATA_TYPE(in_M_type)                                                                             \
                                                                                                                       \
    class M_DLL_EXPORT in_M_type##DataMaker: public ma::DataMaker                                                      \
    {                                                                                                                  \
        public:                                                                                                        \
            typedef in_M_type value_type;                                                                              \
            explicit in_M_type##DataMaker(const ConstructorParameters &in_params);                                     \
            in_M_type##DataMaker() = delete;                                                                           \
            ~in_M_type##DataMaker() override;                                                                          \
            DataPtr CreateData(ContextLinkPtr &link,                                                                   \
                               ma::ItemPtr client,                                                                     \
                               const CreateParameters &params = CreateParameters()) const override;                    \
            const TypeInfo &GetFactoryTypeInfo() const override;                                                       \
            static std::wstring GetDataClassName();                                                                    \
            static std::wstring GetClientClassName();                                                                  \
                                                                                                                       \
            DataMakerPtr Clone(const Item::key_type &clone_key) const override;                                        \
                                                                                                                       \
            M_HEADER_CLASSHIERARCHY(in_M_type##DataMaker)                                                              \
    };                                                                                                                 \
    typedef std::shared_ptr<in_M_type##DataMaker> in_M_type##DataMakerPtr;

#define M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(in_M_type, in_M_namespace)                                              \
                                                                                                                       \
    class M_DLL_EXPORT in_M_type##DataMaker: public ma::DataMaker                                                      \
    {                                                                                                                  \
        public:                                                                                                        \
            typedef in_M_type value_type;                                                                              \
            explicit in_M_type##DataMaker(const ConstructorParameters &in_params);                                     \
            in_M_type##DataMaker() = delete;                                                                           \
            ~in_M_type##DataMaker() override;                                                                          \
            DataPtr CreateData(ContextLinkPtr &link,                                                                   \
                               ma::ItemPtr client,                                                                     \
                               const CreateParameters &params = CreateParameters()) const override;                    \
            const TypeInfo &GetFactoryTypeInfo() const override;                                                       \
            static std::wstring GetDataClassName();                                                                    \
            static std::wstring GetClientClassName();                                                                  \
                                                                                                                       \
            DataMakerPtr Clone(const Item::key_type &clone_key) const override;                                        \
                                                                                                                       \
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##DataMaker, in_M_namespace)                               \
    };                                                                                                                 \
    typedef std::shared_ptr<in_M_type##DataMaker> in_M_type##DataMakerPtr;

#define M_CPP_MAKERDATA_TYPE(in_M_type, in_M_client_type)                                                              \
    ma::in_M_type##DataMaker::in_M_type##DataMaker(const ConstructorParameters &in_params):                            \
    ma::DataMaker(in_params, in_M_type##DataMaker::GetDataClassName(), in_M_type##DataMaker::GetClientClassName())     \
    {}                                                                                                                 \
                                                                                                                       \
    ma::in_M_type##DataMaker::~in_M_type##DataMaker()                                                                  \
    {}                                                                                                                 \
                                                                                                                       \
    ma::DataMakerPtr ma::in_M_type##DataMaker::Clone(const Item::key_type &clone_key) const                            \
    {                                                                                                                  \
        return ma::Item::CreateItem<ma::in_M_type##DataMaker>({GetParentKey(), true, m_CanBeDeleteOnLoad, clone_key}); \
    }                                                                                                                  \
                                                                                                                       \
    ma::DataPtr ma::in_M_type##DataMaker::CreateData(                                                                  \
        ContextLinkPtr &link, ma::ItemPtr client, const ma::Item::CreateParameters &params) const                      \
    {                                                                                                                  \
        auto client_data = std::dynamic_pointer_cast<in_M_client_type>(client);                                        \
                                                                                                                       \
        MA_ASSERT(client_data, L"client_data est nul.", std::invalid_argument);                                        \
        return ma::Item::CreateItem<ma::in_M_type>(params, link, client_data);                                         \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_type##DataMaker::GetFactoryTypeInfo() const                                           \
    {                                                                                                                  \
        return ma::in_M_type::Get##in_M_type##TypeInfo();                                                              \
    }                                                                                                                  \
                                                                                                                       \
    std::wstring ma::in_M_type##DataMaker::GetDataClassName()                                                          \
    {                                                                                                                  \
        return ma::to_wstring(#in_M_type);                                                                             \
    }                                                                                                                  \
                                                                                                                       \
    std::wstring ma::in_M_type##DataMaker::GetClientClassName()                                                        \
    {                                                                                                                  \
        return ma::to_wstring(#in_M_client_type);                                                                      \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_type##DataMaker::Get##in_M_type##DataMakerTypeInfo()                                  \
    {                                                                                                                  \
        static const ma::TypeInfo type_info = {ma::to_wstring(#in_M_type) + L"DataMaker",                              \
                                               L"A partir de la chaîne de caractère «" + ma::to_wstring(#in_M_type) +  \
                                                   L"» cette fabrique conçoit des items de type " +                    \
                                                   ma::to_wstring(#in_M_type),                                         \
                                               {}};                                                                    \
        return type_info;                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY(in_M_type##DataMaker, DataMaker)

#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(in_M_type, in_M_namespace, in_M_client_type)                           \
        ma::in_M_namespace::in_M_type##DataMaker::in_M_type##DataMaker(const ConstructorParameters &in_params):        \
        ma::DataMaker(in_params,                                                                                       \
                      ma::in_M_namespace::in_M_type##DataMaker::GetDataClassName(),                                    \
                      ma::in_M_namespace::in_M_type##DataMaker::GetClientClassName())                                  \
        {}                                                                                                             \
                                                                                                                       \
        ma::in_M_namespace::in_M_type##DataMaker::~in_M_type##DataMaker()                                              \
        {}                                                                                                             \
                                                                                                                       \
        ma::DataMakerPtr ma::in_M_namespace::in_M_type##DataMaker::Clone(const Item::key_type &clone_key) const        \
        {                                                                                                              \
            return ma::Item::CreateItem<ma::in_M_namespace::in_M_type##DataMaker>(                                     \
                {GetParentKey(), true, m_CanBeDeleteOnLoad, clone_key});                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::DataPtr ma::in_M_namespace::in_M_type##DataMaker::CreateData(                                              \
            ContextLinkPtr &link, ma::ItemPtr client, const ma::Item::CreateParameters &params) const                  \
        {                                                                                                              \
            auto client_data = std::dynamic_pointer_cast<in_M_client_type>(client);                                    \
                                                                                                                       \
            MA_ASSERT(client_data, L"client_data est nul.", std::invalid_argument);                                    \
                                                                                                                       \
            return ma::Item::CreateItem<ma::in_M_namespace::in_M_type>(params, link, client_data);                     \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_namespace::in_M_type##DataMaker::GetFactoryTypeInfo() const                       \
        {                                                                                                              \
            return ma::in_M_namespace::in_M_type::Get##in_M_namespace##in_M_type##TypeInfo();                          \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_namespace::in_M_type##DataMaker::GetDataClassName()                                      \
        {                                                                                                              \
            return ma::to_wstring(#in_M_namespace "::" #in_M_type);                                                    \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_namespace::in_M_type##DataMaker::GetClientClassName()                                    \
        {                                                                                                              \
            auto client_class_name = ma::to_wstring(#in_M_client_type);                                                \
            ma::RemoveAll(client_class_name, L"ma::");                                                                 \
            return client_class_name;                                                                                  \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo                                                                                             \
            &ma::in_M_namespace::in_M_type##DataMaker::Get##in_M_namespace##in_M_type##DataMakerTypeInfo()             \
        {                                                                                                              \
            static const ma::TypeInfo type_info = {                                                                    \
                ma::to_wstring(#in_M_namespace "::" #in_M_type) + L"DataMaker"s,                                       \
                L"A partir de la chaîne de caractère «"s + ma::to_wstring(#in_M_namespace "::" #in_M_type) +           \
                    L"» cette fabrique conçoit des items de type "s + ma::to_wstring(#in_M_namespace "::" #in_M_type), \
                {}};                                                                                                   \
            return type_info;                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##DataMaker, in_M_namespace, DataMaker)
#else
    #define M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(in_M_type, in_M_namespace, in_M_client_type)                           \
        ma::in_M_namespace::in_M_type##DataMaker::in_M_type##DataMaker(const ConstructorParameters &in_params):        \
        ma::DataMaker(in_params,                                                                                       \
                      ma::in_M_namespace::in_M_type##DataMaker::GetDataClassName(),                                    \
                      ma::in_M_namespace::in_M_type##DataMaker::GetClientClassName())                                  \
        {}                                                                                                             \
                                                                                                                       \
        ma::in_M_namespace::in_M_type##DataMaker::~in_M_type##DataMaker()                                              \
        {}                                                                                                             \
                                                                                                                       \
        ma::DataMakerPtr ma::in_M_namespace::in_M_type##DataMaker::Clone(const Item::key_type &clone_key) const        \
        {                                                                                                              \
            return ma::Item::CreateItem<ma::in_M_namespace::in_M_type##DataMaker>(                                     \
                {GetParentKey(), true, m_CanBeDeleteOnLoad, clone_key});                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::DataPtr ma::in_M_namespace::in_M_type##DataMaker::CreateData(                                              \
            ContextLinkPtr &link, ma::ItemPtr client, const ma::Item::CreateParameters &params) const                  \
        {                                                                                                              \
            auto client_data = std::dynamic_pointer_cast<in_M_client_type>(client);                                    \
                                                                                                                       \
            MA_ASSERT(client_data, L"client_data est nul.", std::invalid_argument);                                    \
            return ma::Item::CreateItem<ma::in_M_namespace::in_M_type>(params, link, client);                          \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_namespace::in_M_type##DataMaker::GetFactoryTypeInfo() const                       \
        {                                                                                                              \
            return ma::in_M_namespace::in_M_type::Get##in_M_namespace####in_M_type##TypeInfo();                        \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_namespace::in_M_type##DataMaker::GetDataClassName()                                      \
        {                                                                                                              \
            return ma::to_wstring(#in_M_namespace "::" #in_M_type);                                                    \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_namespace::in_M_type##DataMaker::GetClientClassName()                                    \
        {                                                                                                              \
            auto client_class_name = ma::to_wstring(#in_M_client_type);                                                \
            ma::RemoveAll(client_class_name, L"ma::");                                                                 \
            return client_class_name;                                                                                  \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo                                                                                             \
            &ma::in_M_namespace::in_M_type##DataMaker::Get##in_M_namespace####in_M_type##DataMakerTypeInfo()           \
        {                                                                                                              \
            static const ma::TypeInfo type_info = {                                                                    \
                ma::to_wstring(#in_M_namespace "::" #in_M_type) + L"DataMaker"s,                                       \
                L"A partir de la chaîne de caractère «"s + ma::to_wstring(#in_M_namespace "::" #in_M_type) +           \
                    L"» cette fabrique conçoit des items de type "s + ma::to_wstring(#in_M_namespace "::" #in_M_type), \
                {}};                                                                                                   \
            return type_info;                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##DataMaker, in_M_namespace, DataMaker)
#endif // MA_COMPILER_CLANG

namespace ma
{
    class ContextLink;
    class DataMaker;
    M_HEADER_ITEM_TYPE_VAR(ContextLink)

    class M_DLL_EXPORT ContextManager: public ma::Item
    {
        public:
            /// \tparam T_Context Hérite de « ma::Context ».
            /// \param name Le nom du contexte recherché.
            /// \return Le premier « Item » de type « T_Context » portant le nom égale à la valeur de « name ».
            template<typename T_Context>
            std::shared_ptr<T_Context> Get(const ma::Var::key_type &name);

            /// \tparam T_Context Hérite de « ma::Context ».
            /// \param name Le nom du contexte recherché.
            /// \return Vrais s'il y a un « Item » de type « T_Context » portant le nom égale à la valeur de « name ».
            template<typename T_Context>
            bool Has(const ma::Var::key_type &name);

            explicit ContextManager(const Item::ConstructorParameters &in_params);
            ~ContextManager() override;
            M_HEADER_CLASSHIERARCHY(ContextManager)
    };
    typedef std::shared_ptr<ContextManager> ContextManagerPtr;

    /// Implémenter Data pour charger un type d'item dans un contexte donné
    class M_DLL_EXPORT Data: public ma::Item
    {
        protected:
            /// La clef de l'item de type ContextLink.
            /// Lors du déchargement de la donnée, cette méthode permet de décharger les pointeurs partagés sans perdre
            /// leur identifiant. S'il faut recharger la donnée, la clef permettra de retrouver le lien.
            const Item::key_type m_KeyLink;

            friend class ContextLink;
            void OnEnable() override;

            friend class ContextLink;
            void OnDisable() override;

        public:
            /// Si vrais la donnée est chargée dans le contexte si faux la donnée n'est pas chargée dans le contexte.
            ma::BoolVarPtr m_IsLoad;

            /// Description de l'état de la donnée.
            ma::StringVarPtr m_State;

            /// Le lien vers le client et le contexte.
            ContextLinkVarPtr m_Link;

            /// Pointe sur le client que représente cette donnée.
            ma::ItemVarPtr m_Client;

            static const ma::Item::key_type &GetIsLoadVarId();
            static const ma::Item::key_type &GetSateVarId();
            static const ma::Item::key_type &GetLinkVarId();
            static const ma::Item::key_type &GetClientVarId();

            explicit Data(const Item::ConstructorParameters &in_params,
                          const std::shared_ptr<ContextLink> &link,
                          const ma::ItemPtr &client);
            ~Data() override;

            /// Charge la donnée dans le contexte.
            virtual bool Load();

            /// Décharge la donnée du contexte.
            virtual bool UnLoad();

            /// Par défaut le comportement est d'abord d'appeler Unload et si celui-ci renvoie vrai.
            virtual bool Reload();

            /// Permet d'indiquer à la donnée que la fabrique qui l'a créée va être détruite et qu'elle sera la
            /// remplacente.
            /// \param old_maker
            /// \param new_maker
            virtual void MakerReplacement(std::shared_ptr<DataMaker> old_maker, std::shared_ptr<DataMaker> new_maker);

            M_HEADER_CLASSHIERARCHY(Data)
    };
    typedef std::shared_ptr<Data> DataPtr;

    class M_DLL_EXPORT DataMaker: public ma::Item
    {
        public:
            /// Type pour lister les fabricateurs de données
            typedef std::unordered_map<std::wstring, ma::DataMaker> DataMakerMap;

            static const Var::key_type ms_Key_DataClassName;

            static const Var::key_type ms_Key_ClientClassName;

        protected:
            explicit DataMaker(const Item::ConstructorParameters &in_params,
                               const std::wstring &data_class_name,
                               const std::wstring &client_class_name);

        public:
            /// Variable contenant le nom de la classe. La variable est identifiée par ms_KeyClassNameProperty.
            const ma::StringVarPtr m_DataClassName;

            /// Variable contenant le nom de la classe. La variable est identifiée par ms_KeyClassNameProperty.
            const ma::StringVarPtr m_ClientClassName;

            virtual ma::DataPtr CreateData(std::shared_ptr<ContextLink> &link,
                                           ma::ItemPtr client,
                                           const CreateParameters &params = ma::Item::CreateParameters()) const = 0;

            /// Les fabriques n'ont pas de clef unique, pour pouvoir les chargé il faut les retrouver par leur type.
            /// Mais il faut que la clef de celui-ci soit celle du dossier de sauvegarde.
            /// La RTTI peut permettre de gérer ça, mais cela veut dire que pour chaque fabrique il faut une fabrique
            /// associée dans la RTTI.
            virtual std::shared_ptr<DataMaker> Clone(const Item::key_type &clone_key) const = 0;

            explicit DataMaker() = delete;
            ~DataMaker() override;

            /// Permet d'informer le type de l'élément qui sera construit par cette fabrique.
            virtual const ma::TypeInfo &GetFactoryTypeInfo() const = 0;

            /// Lite de fabricateurs de données pour le type de contexte donné.
            DataMakerMap GetDataMakerMap(const std::wstring &context_class_name);

            /// Formalise l'ajout d'un fabricateur de donnée pour un contexte donné.
            template<typename T_DataMaker, typename... Args>
            std::shared_ptr<T_DataMaker> AddDataMaker(const std::wstring &context_class_name, const Args &...args);

            M_HEADER_CLASSHIERARCHY(DataMaker)
    };
    typedef std::shared_ptr<DataMaker> DataMakerPtr;

    /// Les enfants de ContextDataMaker sont des DataMaker associé au type de contexte représenté par la variable
    /// « m_ContextName ».
    /// Le gestionnaire de contextes créé ses ContextDataMaker pour permettre aux contextes de savoir comment générer un
    /// type de donnée.
    class M_DLL_EXPORT ContextDataMaker: public ma::Item
    {
        public:
            /// Variable contenant le nom de la classe du contexte auquel sont associés les fabricateurs qu'hébergera
            /// cet « Item ». \remarks La clef de cette variable est
            const ma::StringVarPtr m_ContextName;

            explicit ContextDataMaker(const Item::ConstructorParameters &in_params, const std::wstring &context_name);
            explicit ContextDataMaker() = delete;
            ~ContextDataMaker() override;

            ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry) override;

            M_HEADER_CLASSHIERARCHY(ContextDataMaker)
    };
    typedef std::shared_ptr<ContextDataMaker> ContextDataMakerPtr;

    /// Un contexte interprète une représentation (la donnée) d'un item
    /// (le client).
    /// Pour générer la représentation d'un item :
    /// 1) créer un lien (ContextLink) dans le contexte désiré.
    /// 2) Dans le lien sélectionné l'item (le client) à représenter dans le
    /// contexte.
    ///
    /// Un contexte possède une liste d'enfants de type « ContextLink »
    class M_DLL_EXPORT Context: public ma::Item
    {
        private:
            /// Stocke la clef du DataMakers le temps de le créer.
            const ma::Item::key_type m_DataMakersKey;

            /// L'item qui contient des DataMakers
            ma::ContextDataMakerPtr m_DataMakers;

        protected:
            void OnEnable() override;
            void OnDisable() override;

            /// Si l'élément enfant de ce contexte n'existe pas il sera créé.
            /// \exception std::logic_error Il y a plusieurs ContextDataMaker enfant de ce contexte.
            /// Il ne peut y en avoir qu'un seul.
            ContextDataMakerPtr GetContextDataMaker();

            ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry) override;

        public:
            /// Variable contenant le nom du contexte.
            const ma::StringVarPtr m_Name;

            /// Constructeur d'un contexte.
            /// \param in_params Les paramètres de construction.
            /// \param name Le nom du contexte.
            /// \param data_maker_key Si non vide, défini la clef de l'élément contenant les data_makers.
            explicit Context(const Item::ConstructorParameters &in_params,
                             const std::wstring &name,
                             ma::Item::key_type data_maker_key = {});

            explicit Context() = delete;
            ~Context() override;
            void EndConstruction() override;

            /// \return Une liste de fabrique.
            template<typename T_DataMaker>
            std::deque<std::shared_ptr<T_DataMaker>> GetDataMakers() const;

            /// \return Une liste de fabrique.
            std::deque<DataMakerPtr> GetDataMakers() const;

            /// \tparam T_ItemContext Hérite de ItemContext et est la représentation d'un item dans ce contexte
            /// \tparam T_Item Hérite de la classe « Item ».
            /// \return Retourne une représentation de l'item adapté à ce contexte.
            // template <typename T_ItemContext, typename T_Item>
            // std::shared_ptr<T_ItemContext> Get(std::shared_ptr<T_Item> item);

            /// Permet d'accéder à la liste des fabricateurs de données.
            /// \todo Implémenter son utilisation en rendant BuildData non pure.
            // const DataMaker::DataMakerMap &GetRegisteredMakerData() const;

            /// Lorsqu'un ContextLink se voit attribué un item, il va appeler cette fonction de son Context parent.
            /// \param link Le lien vers le contexte
            /// \param parent La donnée parente de la nouvelle donnée à créer.
            /// Si nul alors le parent est ce lien.
            /// \param client L'item qui sera représenté par cette donnée.
            /// \return La donnée créée.
            /// \remarks La donnée sera enfant du lien.
            virtual ma::DataPtr BuildData(std::shared_ptr<ContextLink> link, ma::DataPtr parent, ma::ItemPtr client);

            /// Permet de savoir si un client a un lien dans ce contexte.
            std::optional<std::shared_ptr<ContextLink>> HasLink(ma::ItemPtr client) const;

            /// Permet de savoir si un client a un lien dans ce contexte et si la donnée n'est pas nulle.
            std::optional<DataPtr> HasData(ma::ItemPtr client) const;

            /// Permet de retrouver la donnée associée à un item.
            /// \exception std::invalid_argument HasLink ou HasData renvoient faux.
            template<typename T_Data>
            std::shared_ptr<T_Data> GetData(ma::ItemPtr client) const;

            /// Permet de créer le fabricateur qui gérera les clients de type T_DataMaken
            /// \tparam T_DataMaker
            /// \tparam Args
            /// \param args Les arguments pour construire de fabricateur.
            /// \return
            template<typename T_DataMaker, typename... Args>
            std::shared_ptr<T_DataMaker> AddDataMaker(const Args &...args);

            template<typename T_DataMaker>
            void RemoveDataMaker();

            M_HEADER_CLASSHIERARCHY(Context)
    };
    typedef std::shared_ptr<Context> ContextPtr;

    M_HEADER_ITEM_TYPE_VAR(Data)
    M_HEADER_ITEM_TYPE_VAR(Context)

    /// Le parent de cette classe est un contexte.
    /// \todo Cette classe devrait-elle être un « template » ?
    class M_DLL_EXPORT ContextLink: public ma::Item, public ma::Observer
    {
        private:
            /// Permet d'avoir un accès simplifié au gestionnaire d'observations.
            ma::ObservationManagerPtr m_ObserverManager;

            /// Factorisation. Parcours les données et arrête d'observer les clients associés.
            /// Ce qui appellera EndObservation pour chaque client et supprimera la donnée.
            void RemoveDatas();

            /// Met à jour le nom du contexte.
            void UpdateName();

            /// À chaque fois que le client est modifié il faut
            void UpdateDependencies();

        protected:
            void OnEnable() override;
            void OnDisable() override;

            ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry) override;

        public:
            typedef std::unordered_map<ma::Item::key_type, ma::DataPtr> client_datas;

            /// Pour un contexte donné, « m_Client » et ses enfants auront chacun un « ma::Data » associé à ce lien.
            /// Cette table permet de rapidement d'accéder à cette association.
            client_datas m_ClientDatas;

            /// Variable pointant vers l'item qui sera représenté par cette instance.
            ma::ItemVarPtr m_Client;

            /// Nom du client. Permet de faciliter l'identifications visuelle des liens.
            ma::StringVarPtr m_Name;

            /// Renvoie la clef de la variable m_Client
            static const ma::Item::key_type &GetClientVarKey();

            explicit ContextLink(const Item::ConstructorParameters &in_params, ma::ItemPtr client = {});
            explicit ContextLink() = delete;
            ~ContextLink() override;

            /// Permet d'attendre que les éléments soient observables pour débuter leur observation.
            void EndConstruction() override;

            /// Renvoi le parent qui est le contexte
            virtual ma::ContextPtr GetContext() const;

            /// Permet de commencer l'observation de m_Data, m_Client et m_Context.
            void BeginObservation(const ObservablePtr &observable) override;

            /// \brief L'observable a été modifié, cette fonction est donc appelée avec les informations caractérisant
            /// la ou les modifications appliquées à l'observable.
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;

            /// \brief L'observable vient de se détruire, il faut implémenter la réaction de l'observateur.
            void EndObservation(const ObservablePtr &) override;

            M_HEADER_CLASSHIERARCHY(ContextLink)
    };
    typedef std::shared_ptr<ContextLink> ContextLinkPtr;

} // namespace ma

M_HEADER_MAKERITEM_TYPE(ContextLink)

#include "ma/Context.tpp"
