/// \file Dll.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)

    #ifndef _BUILD_STATIC_LIB
        #ifdef _BUILD_DLL
            #define M_DLL_EXPORT __declspec(dllexport)
        #else
            #define M_DLL_EXPORT __declspec(dllimport)
        #endif
    #else
        #define M_DLL_EXPORT
    #endif

#else // linux && darwin

    #ifndef _BUILD_STATIC_LIB
        #ifdef _BUILD_DLL
            // https://gcc.gnu.org/onlinedocs/gcc-4.0.0/gcc/Function-Attributes.html
            #define M_DLL_EXPORT __attribute__((__visibility__("default")))
        #else
            #define M_DLL_EXPORT
        #endif
    #else
        #define M_DLL_EXPORT
    #endif

#endif
