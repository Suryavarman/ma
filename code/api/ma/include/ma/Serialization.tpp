/// \file Serialization.tpp
/// \author Pontier Pierre
/// \date 2021-16-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Serialization.h"
#include <iostream>

template<typename T>
T ma::Serialization::Parser::GetValue() const
{
    return ma::FromNode<T>(m_Root);
}

template<typename T>
bool ma::Serialization::Parser::IsValid() const
{
    return ma::IsValidNode<T>(m_Root);
}

template<typename T>
ma::Serialization::Errors ma::Serialization::Parser::GetErrors() const
{
    return ma::GetErrors<T>(m_Root);
}

// Par défaut //————————————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T>
bool ma::IsRepresentedByString()
{
    return false;
}

// Type Nombre//————————————————————————————————————————————————————————————————————————————————————————————————————————

template<typename T_NUMBER>
T_NUMBER ma::FromString(const MA_SFINAE_STRING_NUMBER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_NUMBER>();
}

template<typename T_NUMBER>
bool ma::IsValidString(const MA_SFINAE_STRING_NUMBER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_NUMBER>(parser.GetRoot());
}

template<typename T_NUMBER>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_NUMBER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_NUMBER>(parser.GetRoot());
}

// template <typename T_NUMBER>
// MA_SFINAE_BOOL_NUMBER ma::IsRepresentedByString()
//{
//    return false;
//}

template<typename T_NUMBER>
T_NUMBER ma::FromNode(const MA_SFINAE_NODE_NUMBER &node)
{
    MA_ASSERT(node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

    MA_ASSERT(node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

    MA_ASSERT(node->m_Nodes.size() == 0,
              L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
              std::invalid_argument);

    return std::stoi(std::wstring(node->m_String));
}

template<typename T_NUMBER>
bool ma::IsValidNode(const MA_SFINAE_NODE_NUMBER &node)
{
    if(node->m_Type == ma::Serialization::Node::Value && node->m_Nodes.size() == 0)
    {
        const std::wstring str{node->m_String};

        // https://stackoverflow.com/questions/4654636/how-to-determine-if-a-string-is-a-number-with-c
        // https://ideone.com/lyDtfi
        bool result = false;
        if(std::regex_match(str, ma::g_IntRegType))
        {
            try
            {
                std::stoi(str);
                result = true;
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }

        return result;
    }
    else
    {
        return false;
    }
}

template<typename T_NUMBER>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_NUMBER &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_NUMBER>() + L"]"s;

    if(node->m_Nodes.size() != 0)
        errors.push_back(
            {node->m_Index, type_name + L"(node->m_Nodes.size() "s + ma::toString(node->m_Nodes.size()) + L" != 0"s});

    if(node->m_Type != ma::Serialization::Node::Value)
    {
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: " + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value"});
    }
    // Si nous voyons que c'est une structure au lieu d'être une valeur, il n'est alors pas nécessaire de se donner
    // la peine de l'indiquer une deuxième fois.
    else
    {
        const std::wstring str{node->m_String};

        if(!std::regex_match(str, ma::g_IntRegType))
        {
            errors.push_back({node->m_Index, type_name + L"std::regex_match(str, ma::g_IntRegType) == false \n"s});
        }
        else
        {
            try
            {
                std::stoi(str);
            }
            catch(const std::exception &e)
            {
                errors.push_back(
                    {node->m_Index, type_name + L"std::stoi("s + ma::Quoted(str) + L") => "s + ma::to_wstring(e.what())});
            }
        }
    }

    return errors;
}

// Type std::pair //————————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T1, typename T2>
std::wstring ma::toString(const std::pair<T1, T2> &value)
{
    std::wstringstream ss;

    auto t1_str = ma::toString(value.first);
    if(ma::IsRepresentedByString<T1>())
        t1_str = ma::Quoted(t1_str);

    auto t2_str = ma::toString(value.second);
    if(ma::IsRepresentedByString<T2>())
        t2_str = ma::Quoted(t2_str);

    ss << g_BeginKeyWord << t1_str << g_SeparatorKeyWord << L" " << t2_str << g_EndKeyWord;
    return ss.str();
}

template<typename T1, typename T2>
std::pair<T1, T2> FromString(const std::wstring &str)
{
    auto parser = ma::Serialization::Parser{str};
    return parser.GetValue<std::pair<T1, T2>>();
}

template<typename T_Pair>
T_Pair ma::FromString(const MA_SFINAE_STRING_PAIR &str)
{
    auto parser = ma::Serialization::Parser{str};
    return parser.GetValue<T_Pair>();
}

template<typename T_Pair>
bool ma::IsValidString(const MA_SFINAE_STRING_PAIR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_Pair>(parser.GetRoot());
}

template<typename T_Pair>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_PAIR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_Pair>(parser.GetRoot());
}

// template <typename T_Pair>
// MA_SFINAE_BOOL_PAIR ma::IsRepresentedByString()
//{
//    return false;
//}

template<typename T_Pair>
T_Pair ma::FromNode(const MA_SFINAE_NODE_PAIR &node)
{
    MA_ASSERT(
        node->m_Type == ma::Serialization::Node::Struct, L"Le nœud n'est pas une structure.", std::invalid_argument);

    MA_ASSERT(node->m_Nodes.size() == 2u, L"Le nœud ne possède pas deux enfants.", std::invalid_argument);

    return {ma::FromNode<typename T_Pair::first_type>(node->m_Nodes[0]),
            ma::FromNode<typename T_Pair::second_type>(node->m_Nodes[1u])};
}

template<typename T_Pair>
bool ma::IsValidNode(const MA_SFINAE_NODE_PAIR &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Struct && node->m_Nodes.size() == 2u &&
                  ma::IsValidNode<typename T_Pair::first_type>(node->m_Nodes[0]) &&
                  ma::IsValidNode<typename T_Pair::second_type>(node->m_Nodes[1u]);

    return result;
}

template<typename T1, typename T2>
bool ma::IsValidNode(const MA_SFINAE_NODE_PAIR2 &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Struct && node->m_Nodes.size() == 2u &&
                  ma::IsValidNode<T1>(node->m_Nodes[0]) && ma::IsValidNode<T2>(node->m_Nodes[1u]);

    return result;
}

template<typename T_Pair>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_PAIR &node)
{
    const std::wstring type_name = L"["s + ma::TypeNameW<T_Pair>() + L"]"s;

    ma::Serialization::Errors errors;

    if(node->m_Type != ma::Serialization::Node::Struct)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Struct"s});

    if(node->m_Nodes.size() != 2u)
        errors.push_back({node->m_Index, type_name + L"node->m_Nodes.size() != 2u"s});
    else
    {
        const auto first = node->m_Nodes[0];
        const auto second = node->m_Nodes[1u];
        if(ma::IsValidNode<typename T_Pair::first_type>(first))
        {
            auto errors_first = ma::GetErrors<typename T_Pair::first_type>(first);
            errors.insert(errors.end(), errors_first.begin(), errors_first.end());
        }

        if(ma::IsValidNode<typename T_Pair::second_type>(second))
        {
            auto errors_second = ma::GetErrors<typename T_Pair::second_type>(second);
            errors.insert(errors.end(), errors_second.begin(), errors_second.end());
        }
    }

    return errors;
}

// Type container //————————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Container>
MA_SFINAE_STRING_CONTAINER ma::toString(const T_Container &value)
{
    std::wstringstream ss;
    ss << g_BeginKeyWord;

    size_t i = 0;
    for(auto it : value)
    {
        auto it_str = ma::toString(it);
        if(ma::IsRepresentedByString<decltype(it)>())
            it_str = ma::Quoted(it_str);

        if(i < value.size() - 1)
            ss << it_str << g_SeparatorKeyWord << L" ";
        else
            ss << it_str;
        i++;
    }
    ss << g_EndKeyWord;
    return ss.str();
}

template<typename T_Container>
T_Container ma::FromString(const MA_SFINAE_STRING_CONTAINER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_Container>();
}

template<typename T_Container>
bool ma::IsValidString(const MA_SFINAE_STRING_CONTAINER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_Container>(parser.GetRoot());
}

template<typename T_Container>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_CONTAINER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_Container>(parser.GetRoot());
}

// template <typename T_Container>
// MA_SFINAE_BOOL_CONTAINER ma::IsRepresentedByString()
//{
//    return false;
//}

template<typename T_Container>
T_Container ma::FromNode(const MA_SFINAE_NODE_CONTAINER &node)
{
    MA_ASSERT(
        node->m_Type == ma::Serialization::Node::Struct, L"Le nœud n'est pas une structure.", std::invalid_argument);

    T_Container result;

    // La fonction « insert » est la plus commune à tous les conteneurs de la librairie standard.
    for(auto child : node->m_Nodes)
        result.insert(result.end(), ma::FromNode<typename T_Container::value_type>(child));

    return result;
}

template<typename T_Container>
bool ma::IsValidNode(const MA_SFINAE_NODE_CONTAINER &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Struct;

    if(result)
        for(auto it = node->m_Nodes.begin(); result && it != node->m_Nodes.end(); ++it)
            result = result && ma::IsValidNode<typename T_Container::value_type>(*it);

    return result;
}

template<typename T_Container>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_CONTAINER &node)
{
    ma::Serialization::Errors errors;

    if(node->m_Type != ma::Serialization::Node::Struct)
    {
        const std::wstring type_name = L"["s + ma::TypeNameW<T_Container>() + L"]"s;
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Struct"s});
    }
    else
    {
        for(auto it = node->m_Nodes.begin(); it != node->m_Nodes.end(); ++it)
        {
            auto errors_it = ma::GetErrors<typename T_Container::value_type>(*it);
            errors.insert(errors.end(), errors_it.begin(), errors_it.end());
        }
    }

    return errors;
}

// Type push container //———————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Container>
MA_SFINAE_STRING_PUSH_CONTAINER ma::toString(const T_Container &value)
{
    std::wstringstream ss;
    ss << g_BeginKeyWord;

    const iterable_push_container<T_Container> iterable_value(value);

    size_t i = 0;
    for(auto it : iterable_value)
    {
        auto it_str = ma::toString(it);
        if(ma::IsRepresentedByString<decltype(it)>())
            it_str = ma::Quoted(it_str);

        if(i < value.size() - 1)
            ss << it_str << g_SeparatorKeyWord << L" ";
        else
            ss << it_str;
        i++;
    }
    ss << g_EndKeyWord;
    return ss.str();
}

template<typename T_Container>
T_Container ma::FromString(const MA_SFINAE_STRING_PUSH_CONTAINER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_Container>();
}

template<typename T_Container>
bool ma::IsValidString(const MA_SFINAE_STRING_PUSH_CONTAINER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_Container>(parser.GetRoot());
}

template<typename T_Container>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_PUSH_CONTAINER &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_Container>(parser.GetRoot());
}

// template <typename T_Container>
// MA_SFINAE_BOOL_PUSH_CONTAINER ma::IsRepresentedByString()
//{
//    return false;
//}

template<typename T_Container>
T_Container ma::FromNode(const MA_SFINAE_NODE_PUSH_CONTAINER &node)
{
    MA_ASSERT(
        node->m_Type == ma::Serialization::Node::Struct, L"Le nœud n'est pas une structure.", std::invalid_argument);

    T_Container result;

    for(auto child : node->m_Nodes)
        result.push(ma::FromNode<typename T_Container::value_type>(child));

    return result;
}

template<typename T_Container>
bool ma::IsValidNode(const MA_SFINAE_NODE_PUSH_CONTAINER &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Struct;

    if(result)
        for(auto it = node->m_Nodes.begin(); result && it != node->m_Nodes.end(); ++it)
            result = result && ma::IsValidNode<typename T_Container::value_type>(*it);

    return result;
}

template<typename T_Container>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_PUSH_CONTAINER &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_Container>() + L"]"s;

    if(node->m_Type != ma::Serialization::Node::Struct)
    {
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Struct"s});
    }
    else
    {
        for(auto it = node->m_Nodes.begin(); it != node->m_Nodes.end(); ++it)
        {
            auto errors_it = ma::GetErrors<typename T_Container::value_type>(*it);
            errors.insert(errors.end(), errors_it.begin(), errors_it.end());
        }
    }

    return errors;
}
/*
template <typename T>
std::queue<T> ma::FromNode(const typename nodeptr_<typename std::queue<T>::const_iterator>::type& node)
{
    MA_ASSERT(node->m_Type == ma::Serialization::Node::Struct,
              L"Le nœud n'est pas une structure.",
              std::invalid_argument);

    std::queue<T> result;

    for(auto child : node->m_Nodes)
        result.push(ma::FromNode<std::queue<T>::value_type>(child));

    return result;
}

template <typename T>
std::stack<T> ma::FromNode(const typename nodeptr_<typename std::stack<T>::const_iterator>::type& node)
{
    MA_ASSERT(node->m_Type == ma::Serialization::Node::Struct,
              L"Le nœud n'est pas une structure.",
              std::invalid_argument);

    std::stack<T> result;

    for(auto child : node->m_Nodes)
        result.push(ma::FromNode<std::stack<T>::value_type>(child));

    return result;
}


template <typename T>
std::forward_list<T> ma::FromNode(const typename nodeptr_<typename std::forward_list<T>::const_iterator>::type& node)
{
    MA_ASSERT(node->m_Type == ma::Serialization::Node::Struct,
              L"Le nœud n'est pas une structure.",
              std::invalid_argument);

    std::forward_list<T> result;

    for(auto it = node->m_Nodes.rbegin(); it != node->m_Nodes.rend(); ++it)
        result.push_front(ma::FromNode<std::forward_list<T>::value_type>(*it));

    return result;
}
*/

// Type std::array //———————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Array>
MA_SFINAE_STRING_ARRAY ma::toString(const T_Array &value)
{
    std::wstringstream ss;
    ss << g_BeginKeyWord;

    size_t i = 0;
    for(auto it : value)
    {
        auto it_str = ma::toString(it);
        if(ma::IsRepresentedByString<decltype(it)>())
            it_str = ma::Quoted(it_str);

        if(i < value.size() - 1)
            ss << it_str << g_SeparatorKeyWord << L" ";
        else
            ss << it_str;
        i++;
    }
    ss << g_EndKeyWord;
    return ss.str();
}

template<typename T_Array>
T_Array ma::FromString(const MA_SFINAE_STRING_ARRAY &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_Array>();
}

template<typename T_Array>
bool ma::IsValidString(const MA_SFINAE_STRING_ARRAY &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_Array>(parser.GetRoot());
}

template<typename T_Array>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_ARRAY &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_Array>(parser.GetRoot());
}

// template <typename T_Array>
// MA_SFINAE_BOOL_ARRAY ma::IsRepresentedByString()
//{
//    return false;
//}

template<class T_Array>
T_Array ma::FromNode(const MA_SFINAE_NODE_ARRAY &node)
{
    MA_ASSERT(
        node->m_Type == ma::Serialization::Node::Struct, L"Le nœud n'est pas une structure.", std::invalid_argument);

    T_Array result;

    MA_ASSERT(node->m_Nodes.size() <= result.size(),
              L"Le nœud a plus d'enfants que ne le permet le tableau: " + ma::toString(result.size()),
              std::invalid_argument);

    size_t index = 0;
    for(auto child : node->m_Nodes)
    {
        result[index] = ma::FromNode<typename T_Array::value_type>(child);
        index++;
    }

    return result;
}

template<class T_Array>
bool ma::IsValidNode(const MA_SFINAE_NODE_ARRAY &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Struct && node->m_Nodes.size() <= T_Array::size_type;

    if(result)
        for(auto child : node->m_Nodes)
            result = result && ma::IsValidNode<typename T_Array::value_type>(child);

    return result;
}

template<class T_Array>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_ARRAY &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_Array>() + L"]"s;

    if(node->m_Type != ma::Serialization::Node::Struct)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Struct"s});

    if(node->m_Nodes.size() > T_Array::size_type)
    {
        errors.push_back({node->m_Index,
                          type_name + L"(node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) +
                              L") > (T_Array::size_type : "s + ma::toString(T_Array::size_type) + L")"s});
    }
    else if(node->m_Type == ma::Serialization::Node::Struct && node->m_Nodes.size() == T_Array::size_type)
    {
        for(auto it = node->m_Nodes.begin(); it != node->m_Nodes.end(); ++it)
        {
            auto errors_it = ma::GetErrors<typename T_Array::value_type>(*it);
            errors.insert(errors.end(), errors_it.begin(), errors_it.end());
        }
    }

    return errors;
}

// Type Eigen::Matrix //————————————————————————————————————————————————————————————————————————————————————————————————
template<class T_EigenMatrix,
         std::enable_if_t<T_EigenMatrix::RowsAtCompileTime != 1 && T_EigenMatrix::ColsAtCompileTime != 1, bool>>
MA_SFINAE_STRING_EIGEN_MATRIX ma::toString(const T_EigenMatrix &value)
{
    std::wstringstream ss;

    ss << g_BeginKeyWord;

    // Les vecteurs seront écris ainsi:
    // {0, 1, 2, 3, 4}
    // les matrices ainsi
    // {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}}
    typename T_EigenMatrix::Index index_row = 0;
    for(const auto &row : value.rowwise())
    {
        ss << g_BeginKeyWord;

        typename T_EigenMatrix::Index index_col = 0;
        for(const auto &element : row)
        {
            auto element_str = ma::toString(element);
            if(ma::IsRepresentedByString<decltype(element)>())
                element_str = ma::Quoted(element_str);

            if(index_col < row.size() - 1)
                ss << element_str << g_SeparatorKeyWord << L" ";
            else
                ss << element_str;
            index_col++;
        }

        ss << g_EndKeyWord;

        if(index_row < T_EigenMatrix::RowsAtCompileTime - 1)
            ss << g_SeparatorKeyWord << L" ";

        index_row++;
    }

    ss << g_EndKeyWord;
    return ss.str();
}

template<class T_EigenMatrix,
         std::enable_if_t<T_EigenMatrix::RowsAtCompileTime == 1 || T_EigenMatrix::ColsAtCompileTime == 1, bool>>
MA_SFINAE_STRING_EIGEN_MATRIX ma::toString(const T_EigenMatrix &value)
{
    std::wstringstream ss;

    ss << g_BeginKeyWord;

    // Les vecteurs seront écris ainsi:
    // {0, 1, 2, 3, 4}

    // https://eigen.tuxfamily.org/dox/classEigen_1_1Matrix.html
    // Hélas je n'ai pas trouvé comment avoir les itérateurs.
    for(typename T_EigenMatrix::Index index = 0; index < value.size(); index++)
    {
        const auto element = value[index];
        auto element_str = ma::toString(element);
        if(ma::IsRepresentedByString<decltype(element)>())
            element_str = ma::Quoted(element_str);

        if(index < value.size() - 1)
            ss << element_str << g_SeparatorKeyWord << L" ";
        else
            ss << element_str;
    }

    ss << g_EndKeyWord;
    return ss.str();
}

template<typename T_EigenMatrix>
T_EigenMatrix ma::FromString(const MA_SFINAE_STRING_EIGEN_MATRIX &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_EigenMatrix>();
}

template<typename T_EigenMatrix>
bool ma::IsValidString(const MA_SFINAE_STRING_EIGEN_MATRIX &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_EigenMatrix>(parser.GetRoot());
}

template<typename T_EigenMatrix>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_EIGEN_MATRIX &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_EigenMatrix>(parser.GetRoot());
}

// template <typename T_EigenMatrix>
// MA_SFINAE_BOOL_EIGEN_MATRIX ma::IsRepresentedByString()
//{
//    return false;
//}

template<class T_EigenMatrix>
T_EigenMatrix ma::FromNode(const MA_SFINAE_NODE_EIGEN_MATRIX &node)
{
    MA_ASSERT(
        node->m_Type == ma::Serialization::Node::Struct, L"Le nœud n'est pas une structure.", std::invalid_argument);

    const auto nodes_size = static_cast<typename T_EigenMatrix::Index>(node->m_Nodes.size());

    T_EigenMatrix result;

    // Nous commençons par les matrices à une dimension
    // Ex: {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
    if(T_EigenMatrix::SizeAtCompileTime == nodes_size)
    {
        typename T_EigenMatrix::Index index_row = 0;
        typename T_EigenMatrix::Index index_col = 0;

        for(auto child : node->m_Nodes)
        {
            const auto value = ma::FromNode<typename T_EigenMatrix::Scalar>(child);

            result(index_row, index_col) = value;

            index_col++;
            if(index_col >= T_EigenMatrix::ColsAtCompileTime)
            {
                index_col = 0;

                index_row++;
                if(index_row >= T_EigenMatrix::RowsAtCompileTime)
                    index_row = 0;
            }
        }
    }
    // Ex: {{0, 1, 2}, {3, 4, 6}, {7, 8, 9}}
    else if(T_EigenMatrix::ColsAtCompileTime == nodes_size)
    {
        typename T_EigenMatrix::Index index_row = 0;
        typename T_EigenMatrix::Index index_col = 0;

        for(auto row : node->m_Nodes)
        {
            const auto row_nodes_size = static_cast<typename T_EigenMatrix::Index>(row->m_Nodes.size());

            MA_ASSERT(row_nodes_size == T_EigenMatrix::RowsAtCompileTime,
                      L"Le nœud n'a pas le nombre d'enfants requis pour représenter ce type de matrice" +
                          ma::toString(T_EigenMatrix::RowsAtCompileTime) + L"x" +
                          ma::toString(T_EigenMatrix::ColsAtCompileTime) + L": " + ma::toString(result.size()),
                      std::invalid_argument);
            for(auto col : row->m_Nodes)
            {
                const auto value = ma::FromNode<typename T_EigenMatrix::Scalar>(col);
                result(index_row, index_col) = value;
                index_col++;
            }
            index_col = 0;
            index_row++;
        }
    }
    else
    {
        MA_ASSERT(false,
                  L"Le nœud n'a pas le nombre d'enfants requis pour représenter ce type de matrice" +
                      ma::toString(T_EigenMatrix::RowsAtCompileTime) + L"x" +
                      ma::toString(T_EigenMatrix::ColsAtCompileTime) + L": " + ma::toString(result.size()),
                  std::invalid_argument);
    }

    return result;
}

template<class T_EigenMatrix>
bool ma::IsValidNode(const MA_SFINAE_NODE_EIGEN_MATRIX &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Struct;

    if(result)
    {
        const auto nodes_size = static_cast<typename T_EigenMatrix::Index>(node->m_Nodes.size());

        // Ex: {{0, 1, 2}, {3, 4, 6}, {7, 8, 9}}
        if(T_EigenMatrix::RowsAtCompileTime == nodes_size)
        {
            // typename T_EigenMatrix::Index index_row = 0;
            // typename T_EigenMatrix::Index index_col = 0;

            for(auto row : node->m_Nodes)
            {
                if(result)
                {
                    const auto row_nodes_size = static_cast<typename T_EigenMatrix::Index>(row->m_Nodes.size());

                    result = result && row_nodes_size == T_EigenMatrix::ColsAtCompileTime;

                    if(result)
                    {
                        for(auto col : row->m_Nodes)
                            result = result && ma::IsValidNode<typename T_EigenMatrix::Scalar>(col);
                    }
                }
            }
        }
        // Ex: {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
        else if(T_EigenMatrix::SizeAtCompileTime == nodes_size)
        {
            for(auto child : node->m_Nodes)
                result = result && ma::IsValidNode<typename T_EigenMatrix::Scalar>(child);

            result = result && true;
        }
        else
        {
            result = false;
        }
    }

    return result;
}

template<class T_EigenMatrix>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_EIGEN_MATRIX &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_EigenMatrix>() + L"]"s;

    if(node->m_Type != ma::Serialization::Node::Struct)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Struct"s});

    const auto nodes_size = static_cast<typename T_EigenMatrix::Index>(node->m_Nodes.size());

    // Ex: {{0, 1, 2}, {3, 4, 6}, {7, 8, 9}}
    if(node->m_Type == ma::Serialization::Node::Struct && nodes_size == T_EigenMatrix::RowsAtCompileTime)
    {
        for(auto it = node->m_Nodes.begin(); it != node->m_Nodes.end(); ++it)
        {
            auto row = *it;
            if(row->m_Type != ma::Serialization::Node::Struct)
            {
                errors.push_back({row->m_Index,
                                  type_name + L"(row->m_Type: "s + GetTypeStr(row->m_Type) +
                                      L") != ma::Serialization::Node::Struct"s});
            }
            else
            {
                const auto row_nodes_size = static_cast<typename T_EigenMatrix::Index>(row->m_Nodes.size());

                if(nodes_size == T_EigenMatrix::ColsAtCompileTime)
                {
                    for(auto col = row->m_Nodes.begin(); col != row->m_Nodes.end(); ++col)
                    {
                        auto errors_it = ma::GetErrors<typename T_EigenMatrix::Scalar>(*col);
                        errors.insert(errors.end(), errors_it.begin(), errors_it.end());
                    }
                }
                else
                {
                    errors.push_back({row->m_Index,
                                      type_name + L"(rrow->m_Nodes.size(): "s + ma::toString(row_nodes_size) +
                                          L") != (T_EigenMatrix::ColsAtCompileTime: "s +
                                          ma::toString(T_EigenMatrix::ColsAtCompileTime) + L")"s});
                }
            }
        }
    }
    // Ex: {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
    else if(node->m_Type == ma::Serialization::Node::Struct && nodes_size == T_EigenMatrix::SizeAtCompileTime)
    {
        for(auto it = node->m_Nodes.begin(); it != node->m_Nodes.end(); ++it)
        {
            auto errors_it = ma::GetErrors<typename T_EigenMatrix::Scalar>(*it);
            errors.insert(errors.end(), errors_it.begin(), errors_it.end());
        }
    }
    else
    {
        const auto message = L"Le nœud n'a pas le nombre d'enfants requis pour représenter ce type de matrice" +
                             ma::toString(T_EigenMatrix::ColsAtCompileTime) + L"x" +
                             ma::toString(T_EigenMatrix::RowsAtCompileTime);

        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Nodes.size(): "s + ma::toString(nodes_size) + L") > "s + message + L")"s});
    }

    return errors;
}

// Type Eigen::Quaternion //————————————————————————————————————————————————————————————————————————————————————————————
template<class T_EigenQuaternion>
MA_SFINAE_STRING_EIGEN_QUATERNION ma::toString(const T_EigenQuaternion &value)
{
    std::wstringstream ss;
    ss << g_BeginKeyWord;

    auto to_str = [&](const typename T_EigenQuaternion::Scalar &scalar, bool last_scalar = false)
    {
        auto scalar_str = ma::toString(scalar);
        if(ma::IsRepresentedByString<decltype(scalar)>())
            scalar_str = ma::Quoted(scalar_str);

        if(last_scalar)
            ss << scalar_str;
        else
            ss << scalar_str << g_SeparatorKeyWord << L" ";
    };

    to_str(value.w());
    to_str(value.x());
    to_str(value.y());
    to_str(value.z(), true);

    ss << g_EndKeyWord;
    return ss.str();
}

template<typename T_EigenQuaternion>
T_EigenQuaternion ma::FromString(const MA_SFINAE_STRING_EIGEN_QUATERNION &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_EigenQuaternion>();
}

template<typename T_EigenQuaternion>
bool ma::IsValidString(const MA_SFINAE_STRING_EIGEN_QUATERNION &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_EigenQuaternion>(parser.GetRoot());
}

template<typename T_EigenQuaternion>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_EIGEN_QUATERNION &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_EigenQuaternion>(parser.GetRoot());
}

// template <typename T_EigenQuaternion>
// MA_SFINAE_BOOL_EIGEN_QUATERNION ma::IsRepresentedByString()
//{
//    return false;
//}

template<class T_EigenQuaternion>
T_EigenQuaternion ma::FromNode(const MA_SFINAE_NODE_EIGEN_QUATERNION &node)
{
    MA_ASSERT(
        node->m_Type == ma::Serialization::Node::Struct, L"Le nœud n'est pas une structure.", std::invalid_argument);

    const auto nodes_size = node->m_Nodes.size();
    MA_ASSERT(nodes_size == 4u,
              L"Le nœud a « " + ma::toString(nodes_size) + L" » enfants au lieu de quatre.",
              std::invalid_argument);

    return {ma::FromNode<typename T_EigenQuaternion::Scalar>(node->m_Nodes[0]),
            ma::FromNode<typename T_EigenQuaternion::Scalar>(node->m_Nodes[1u]),
            ma::FromNode<typename T_EigenQuaternion::Scalar>(node->m_Nodes[2u]),
            ma::FromNode<typename T_EigenQuaternion::Scalar>(node->m_Nodes[3u])};
}

template<class T_EigenQuaternion>
bool ma::IsValidNode(const MA_SFINAE_NODE_EIGEN_QUATERNION &node)
{
    const auto nodes_size = node->m_Nodes.size();
    bool result = node->m_Type == ma::Serialization::Node::Struct && nodes_size == 4u;

    if(result)
        for(auto child : node->m_Nodes)
            result = result && ma::IsValidNode<typename T_EigenQuaternion::Scalar>(child);

    return result;
}

template<class T_EigenQuaternion>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_EIGEN_QUATERNION &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_EigenQuaternion>() + L"]"s;

    if(node->m_Type != ma::Serialization::Node::Struct)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Struct"s});

    const auto nodes_size = node->m_Nodes.size();
    if(nodes_size != 4u)
    {
        errors.push_back({node->m_Index,
                          type_name + L"(node->m_Nodes.size(): "s + ma::toString(nodes_size) +
                              L") > (T_EigenQuaternion::size_type : 4)"s});
    }
    else if(node->m_Type == ma::Serialization::Node::Struct && nodes_size == 4u)
    {
        for(auto it = node->m_Nodes.begin(); it != node->m_Nodes.end(); ++it)
        {
            auto errors_it = ma::GetErrors<typename T_EigenQuaternion::Scalar>(*it);
            errors.insert(errors.end(), errors_it.begin(), errors_it.end());
        }
    }

    return errors;
}

// Type Eigen::Transform //—————————————————————————————————————————————————————————————————————————————————————————————
template<class T_EigenTransform>
MA_SFINAE_STRING_EIGEN_TRANSFORM ma::toString(const T_EigenTransform &value)
{
    return ma::toString(value.matrix());
}

template<typename T_EigenTransform>
T_EigenTransform ma::FromString(const MA_SFINAE_STRING_EIGEN_TRANSFORM &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_EigenTransform>();
}

template<typename T_EigenTransform>
bool ma::IsValidString(const MA_SFINAE_STRING_EIGEN_TRANSFORM &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_EigenTransform>(parser.GetRoot());
}

template<typename T_EigenTransform>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_EIGEN_TRANSFORM &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_EigenTransform>(parser.GetRoot());
}

// template <typename T_EigenTransform>
// MA_SFINAE_BOOL_EIGEN_TRANSFORM ma::IsRepresentedByString()
//{
//    return false;
//}

template<class T_EigenTransform>
T_EigenTransform ma::FromNode(const MA_SFINAE_NODE_EIGEN_TRANSFORM &node)
{
    T_EigenTransform tr{ma::FromNode<typename T_EigenTransform::MatrixType>(node)};
    return tr;
}

template<class T_EigenTransform>
bool ma::IsValidNode(const MA_SFINAE_NODE_EIGEN_TRANSFORM &node)
{
    return ma::IsValidNode<typename T_EigenTransform::MatrixType>(node);
}

template<class T_EigenTransform>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_EIGEN_TRANSFORM &node)
{
    return ma::GetErrors<typename T_EigenTransform::MatrixType>(node);
}

// Type std::tuple //———————————————————————————————————————————————————————————————————————————————————————————————————
template<typename... Ts>
std::wstring ma::Tuple::toString(std::tuple<Ts...> const &value)
{
    std::wstring str = g_BeginKeyWord;

    std::apply(
        [&str](auto &&...tuple_args)
        {
            std::size_t n{0};
            // expression repliée
            ((str +=
              (ma::IsRepresentedByString<decltype(tuple_args)>() ? ma::Quoted(ma::toString(tuple_args)) :
                                                                   ma::toString(tuple_args)) +
              (++n != sizeof...(Ts) ? std::wstring{g_SeparatorKeyWord + L" "s} : std::wstring{L""s})),
             ...);
        },
        value);

    str += g_EndKeyWord;
    return str;
}

template<typename T_Tuple, std::size_t index>
void ma::Tuple::FromNode(T_Tuple &in_tuple,
                         const ma::Serialization::Node::Nodes::const_iterator &begin,
                         const ma::Serialization::Node::Nodes::const_iterator &end)
{
    constexpr size_t tuple_size{std::tuple_size_v<T_Tuple>};

    if constexpr(index < tuple_size)
    {
        MA_ASSERT(begin != end, L"begin == end"s, std::invalid_argument);

        std::get<index>(in_tuple) = ma::FromNode<typename std::tuple_element<index, T_Tuple>::type>(*begin);

        if constexpr(index + 1u < tuple_size)
            ma::Tuple::FromNode<T_Tuple, index + 1u>(in_tuple, begin + 1u, end);
    }
}

template<typename T_Tuple, std::size_t index>
bool ma::Tuple::IsValidNode(const ma::Serialization::Node::Nodes::const_iterator &begin,
                            const ma::Serialization::Node::Nodes::const_iterator &end)
{
    constexpr size_t tuple_size{std::tuple_size_v<T_Tuple>};

    if constexpr(index < tuple_size)
    {
        if(begin == end)
            return false;

        bool result = true;

        if constexpr(index + 1u < tuple_size)
            result = ma::Tuple::IsValidNode<T_Tuple, index + 1u>(begin + 1u, end);

        return result && ma::IsValidNode<typename std::tuple_element<index, T_Tuple>::type>(*begin);
    }
    else
        return true;
}

template<typename T_Tuple, std::size_t index>
ma::Serialization::Errors ma::Tuple::GetErrors(const ma::Serialization::Node::Nodes::const_iterator &begin,
                                               const ma::Serialization::Node::Nodes::const_iterator &end)
{
    constexpr size_t tuple_size{std::tuple_size_v<T_Tuple>};

    ma::Serialization::Errors result;

    if constexpr(index < tuple_size)
    {
        if(begin == end)
        {
            auto node = *begin;
            result.push_back({node->m_Index, L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") begin == end."s});
        }

        if constexpr(index + 1u < tuple_size)
        {
            auto errors = ma::Tuple::GetErrors<T_Tuple, index + 1u>(begin + 1u, end);
            result.insert(result.end(), errors.begin(), errors.end());
        }

        auto errors = ma::GetErrors<typename std::tuple_element<index, T_Tuple>::type>(*begin);
        result.insert(result.end(), errors.begin(), errors.end());
    }

    return result;
}

template<class... Ts>
std::wstring ma::toString(const std::tuple<Ts...> &value)
{
    return ma::Tuple::toString(value);
}

template<class T_Tuple>
T_Tuple ma::FromString(const MA_SFINAE_STRING_TUPLE &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_Tuple>();
}

template<typename T_Tuple>
bool ma::IsValidString(const MA_SFINAE_STRING_TUPLE &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_Tuple>(parser.GetRoot());
}

template<typename T_Tuple>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_TUPLE &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_Tuple>(parser.GetRoot());
}

// template <typename T_Tuple>
// MA_SFINAE_BOOL_TUPLE ma::IsRepresentedByString()
//{
//    return false;
//}

template<class T_Tuple>
T_Tuple ma::FromNode(const MA_SFINAE_NODE_TUPLE &node)
{
    MA_ASSERT(
        node->m_Type == ma::Serialization::Node::Struct, L"Le nœud n'est pas une structure.", std::invalid_argument);

    T_Tuple result{};

    constexpr size_t tuple_size{std::tuple_size_v<T_Tuple>};

    MA_ASSERT(node->m_Nodes.size() <= tuple_size,
              L"Le nœud a plus d'enfants que ne le permet le tuple: " + ma::toString(tuple_size),
              std::invalid_argument);

    MA_ASSERT(tuple_size > 0, L"Le tuple doit avoir au moins un paramètre.", std::invalid_argument);

    ma::Tuple::FromNode<T_Tuple, 0>(result, node->m_Nodes.begin(), node->m_Nodes.end());

    return result;
}

template<class T_Tuple>
bool ma::IsValidNode(const MA_SFINAE_NODE_TUPLE &node)
{
    constexpr size_t tuple_size{std::tuple_size_v<T_Tuple>};

    bool result =
        node->m_Type == ma::Serialization::Node::Struct && node->m_Nodes.size() <= tuple_size && tuple_size > 0;

    if(result)
        return ma::Tuple::IsValidNode<T_Tuple, 0>(node->m_Nodes.begin(), node->m_Nodes.end());
    else
        return false;
}

template<class T_Tuple>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_TUPLE &node)
{
    constexpr size_t tuple_size{std::tuple_size_v<T_Tuple>};

    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_Tuple>() + L"]"s;

    if(node->m_Type != ma::Serialization::Node::Struct)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Struct"s});

    if(node->m_Nodes.size() > tuple_size)
        errors.push_back({node->m_Index,
                          type_name + L"(node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) +
                              L") > (tuple_size : "s + ma::toString(tuple_size) + L")"s});

    if(node->m_Type == ma::Serialization::Node::Struct && node->m_Nodes.size() == tuple_size)
    {
        auto errors_2 = ma::Tuple::GetErrors<T_Tuple, 0>(node->m_Nodes.begin(), node->m_Nodes.end());
        errors.insert(errors.end(), errors_2.begin(), errors_2.end());
    }

    return errors;
}

// Type std::basic_string//—————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_String>
MA_SFINAE_TEMPLATE_STRING_STRING ma::toString(const T_String &value)
{
    return value;
}

template<typename T_String>
T_String ma::FromString(const MA_SFINAE_TEMPLATE_STRING_STRING &str)
{
    return str;
}

template<typename T_String>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_TEMPLATE_STRING_STRING &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_String>(parser.GetRoot());
}

// template <typename T_String>
// MA_SFINAE_TEMPLATE_BOOL_STRING ma::IsRepresentedByString()
//{
//    return true;
//}

template<typename T_String>
bool ma::IsValidString(const MA_SFINAE_TEMPLATE_STRING_STRING &str)
{
    return true;
}

template<typename T_String>
bool ma::IsValidNode(const MA_SFINAE_TEMPLATE_NODE_STRING &node)
{
    // bool result = node->m_Type == ma::Serialization::Node::Value && node->m_Nodes.size() == 0;

    return true;
}

template<typename T_String>
T_String ma::FromNode(const MA_SFINAE_TEMPLATE_NODE_STRING &node)
{
    // todo Je ne comprends pas ces erreurs peu importe le type du nœud, nous devrions renvoyer la chaîne de caractères?
    //
    //    MA_ASSERT(node->m_Type == ma::Serialization::Node::Value,
    //              L"Le nœud n'est pas une valeur: " + std::wstring(node->m_String),
    //              std::invalid_argument);
    //
    //    MA_ASSERT(node->m_Nodes.size() == 0,
    //              L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.: "
    //              +
    //                  std::wstring(node->m_String),
    //              std::invalid_argument);

    return std::wstring{node->m_String};
}

template<typename T_String>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_TEMPLATE_NODE_STRING &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name{L"["s + ma::TypeNameW<T_String>() + L"]"s};

    if(node->m_Type != ma::Serialization::Node::Value)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value"s});

    if(node->m_Nodes.size() != 0)
        errors.push_back(
            {node->m_Index, type_name + L"(node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L" != 0"s});

    return ma::Serialization::Errors{};
}

// Type std::filesystem::path //————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Path>
MA_SFINAE_TEMPLATE_STRING_PATH ma::toString(const T_Path &value)
{
    return value.wstring();
}

template<typename T_Path>
T_Path ma::FromString(const MA_SFINAE_TEMPLATE_STRING_PATH &str)
{
    return std::filesystem::path{str};
}

template<typename T_Path>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_TEMPLATE_STRING_PATH &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_Path>(parser.GetRoot());
}

template<typename T_Path>
bool ma::IsValidString(const MA_SFINAE_TEMPLATE_STRING_PATH &str)
{
    return true;
}

template<typename T_Path>
bool ma::IsValidNode(const MA_SFINAE_TEMPLATE_NODE_PATH &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Value && node->m_Nodes.size() == 0;

    return result;
}

template<typename T_Path>
T_Path ma::FromNode(const MA_SFINAE_TEMPLATE_NODE_PATH &node)
{
    MA_ASSERT(node->m_Type == ma::Serialization::Node::Value,
              L"Le nœud n'est pas une valeur: " + std::wstring(node->m_String),
              std::invalid_argument);

    MA_ASSERT(node->m_Nodes.size() == 0,
              L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.: " +
                  std::wstring(node->m_String),
              std::invalid_argument);

    return std::filesystem::path{node->m_String};
}

template<typename T_Path>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_TEMPLATE_NODE_PATH &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name{L"["s + ma::TypeNameW<T_Path>() + L"]"s};

    if(node->m_Type != ma::Serialization::Node::Value)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value"s});

    if(node->m_Nodes.size() != 0)
        errors.push_back(
            {node->m_Index, type_name + L"(node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L" != 0"s});

    return ma::Serialization::Errors{};
}

// Type ma::Item //————————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Item>
MA_SFINAE_STRING_ITEM ma::toString(const std::shared_ptr<T_Item> &value)
{
    if(value)
        return value->GetKey();
    else
        return L"";
}

template<typename T_ItemPtr>
MA_SFINAE_STRING_ITEM_PTR ma::toString(const T_ItemPtr &value)
{
    if(value)
        return value->GetKey();
    else
        return L"";
}

template<typename T_Item>
std::shared_ptr<T_Item> ma::FromString(const MA_SFINAE_STRING_ITEM &str)
{
    if(str.empty())
        return std::shared_ptr<T_Item>{};
    else
        return ma::Item::GetItem<T_Item>(str);
}

template<typename T_ItemPtr>
T_ItemPtr ma::FromString(const MA_SFINAE_STRING_ITEM_PTR &str)
{
    if(str.empty())
        return T_ItemPtr{};
    else
        return ma::Item::GetItem<typename T_ItemPtr::element_type>(str);
}

template<typename T_Item>
bool ma::IsValidString(const MA_SFINAE_STRING_ITEM &str)
{
    return str.empty() || ma::Item::HasItem(str);
}

template<typename T_ItemPtr>
bool ma::IsValidString(const MA_SFINAE_STRING_ITEM_PTR &str)
{
    return str.empty() || ma::Item::HasItem(str);
}

template<typename T_Item>
std::shared_ptr<T_Item> ma::FromNode(const MA_SFINAE_NODE_ITEM &node)
{
    MA_ASSERT(node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

    MA_ASSERT(node->m_Nodes.size() == 0,
              L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
              std::invalid_argument);

    return FromString<T_Item>(std::wstring{node->m_String});
}

template<typename T_ItemPtr>
T_ItemPtr ma::FromNode(const MA_SFINAE_NODE_ITEM_PTR &node)
{
    MA_ASSERT(node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

    MA_ASSERT(node->m_Nodes.size() == 0,
              L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
              std::invalid_argument);

    MA_ASSERT(node->m_String.empty() || ma::Id::IsValid(node->m_String),
              L"La chaîne de caractère du nœud ne représente par un identifiant. L'identifiant doit être un GUID ou "
              L"une chaîne de caractères vide. Pour de plus amples informations veuillez regardez la classe API:Id",
              std::invalid_argument);

    MA_ASSERT(node->m_String.empty() || ma::Item::HasItem(std::wstring{node->m_String}),
              L"La chaîne de caractère du nœud ne représente par un item existant. La chaîne de caractères doit un "
              L"identifiant valide ou vide.",
              std::invalid_argument);

    std::wstring key{node->m_String};

    return FromString<T_ItemPtr>(key);
}

template<typename T_Item>
bool ma::IsValidNode(const MA_SFINAE_NODE_ITEM &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Value && node->m_Nodes.size() == 0;

    return result && (node->m_String.empty() || ma::Item::HasItem(std::wstring{node->m_String}));
}

template<typename T_ItemPtr>
bool ma::IsValidNode(const MA_SFINAE_NODE_ITEM_PTR &node)
{
    bool result = node->m_Type == ma::Serialization::Node::Value && node->m_Nodes.size() == 0;

    return result && (node->m_String.empty() || ma::Item::HasItem(std::wstring{node->m_String}));
}

template<typename T_Item>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_ITEM &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_Item>() + L"]"s;

    if(node->m_Type != ma::Serialization::Node::Value)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: " + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value"});

    if(node->m_Nodes.size() != 0)
        errors.push_back(
            {node->m_Index, type_name + L"node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L" != 0"});

    if(!node->m_String.empty() && !ma::Id::IsValid(node->m_String))
        errors.push_back({node->m_Index, type_name + L"ma::Id::IsValid(node->m_String) == false"});

    if(!node->m_String.empty() && !ma::Item::HasItem(std::wstring{node->m_String}))
        errors.push_back({node->m_Index, type_name + L"ma::Item::HasItem(std::wstring{node->m_String}) == false"});

    return errors;
}

template<typename T_ItemPtr>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_ITEM_PTR &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_ItemPtr>() + L"]"s;

    if(node->m_Type != ma::Serialization::Node::Value)
        errors.push_back(
            {node->m_Index,
             type_name + L"(node->m_Type: "s + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value"s});

    if(node->m_Nodes.size() != 0)
        errors.push_back(
            {node->m_Index, type_name + L"node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L" != 0"});

    if(!node->m_String.empty() && !ma::Id::IsValid(node->m_String))
        errors.push_back({node->m_Index, type_name + L"ma::Id::IsValid(node->m_String) == false"});

    if(!node->m_String.empty() && !ma::Item::HasItem(std::wstring{node->m_String}))
        errors.push_back({node->m_Index, type_name + L"ma::Item::HasItem(std::wstring{node->m_String}) == false"});

    return errors;
}

// Type ma::Var //—————————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Variable>
MA_SFINAE_STRING_VAR ma::toString(const std::shared_ptr<T_Variable> &value)
{
    if(value)
        return L"{" + ma::Quoted(value->GetItemKey()) + L", " + ma::Quoted(value->GetKey()) + L"}";
    else
        return L"";
}

template<typename T_VariablePtr>
MA_SFINAE_STRING_VAR_PTR ma::toString(const T_VariablePtr &value)
{
    if(value)
        return L"{" + ma::Quoted(value->GetItemKey()) + L", " + ma::Quoted(value->GetKey()) + L"}";
    else
        return L"";
}

template<typename T_Variable>
std::shared_ptr<T_Variable> ma::FromString(const MA_SFINAE_STRING_VAR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_Variable>();
}

template<typename T_VariablePtr>
T_VariablePtr ma::FromString(const MA_SFINAE_STRING_VAR_PTR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return parser.GetValue<T_VariablePtr>();
}

template<typename T_Variable>
bool ma::IsValidString(const MA_SFINAE_STRING_VAR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_Variable>(parser.GetRoot());
}

template<typename T_VariablePtr>
bool ma::IsValidString(const MA_SFINAE_STRING_VAR_PTR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::IsValidNode<T_VariablePtr>(parser.GetRoot());
}

template<typename T_Variable>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_VAR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_Variable>(parser.GetRoot());
}

template<typename T_VariablePtr>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_STRING_VAR_PTR &str)
{
    auto parser = ma::Serialization::Parser{str};

    return ma::GetErrors<T_VariablePtr>(parser.GetRoot());
}

template<typename T_Variable>
std::shared_ptr<T_Variable> ma::FromNode(const MA_SFINAE_NODE_VAR &node)
{
    const auto item_var = ma::FromNode<std::pair<ma::Item::key_type, ma::Var::key_type>>(node);

    // return ma::Item::GetItem(item_var.first)->GetVar<T_Variable>(item_var.second); ne peut être compiler...
    // Serialization.tpp:902:65: erreur: expected primary-expression before « > » token
    auto var_ptr = std::dynamic_pointer_cast<T_Variable>(ma::Item::GetItem(item_var.first)->GetVar(item_var.second));

    MA_ASSERT(var_ptr, L"Impossible de convertir dynamiquement la variable.", std::runtime_error);

    return var_ptr;
}

template<typename T_VariablePtr>
T_VariablePtr ma::FromNode(const MA_SFINAE_NODE_VAR_PTR &node)
{
    const auto item_var = ma::FromNode<std::pair<ma::Item::key_type, ma::Var::key_type>>(node);

    // return ma::Item::GetItem(item_var.first)->GetVar<typename T_VariablePtr::element_type>(item_var.second); ne peut
    // être compiler... Serialization.tpp:902:65: erreur: expected primary-expression before « > » token
    auto var_ptr = std::dynamic_pointer_cast<typename T_VariablePtr::element_type>(
        ma::Item::GetItem(item_var.first)->GetVar(item_var.second));

    MA_ASSERT(var_ptr, L"Impossible de convertir dynamiquement la variable.", std::runtime_error);

    return var_ptr;
}

template<typename T_Variable>
bool ma::IsValidNode(const MA_SFINAE_NODE_VAR &node)
{
    bool result = ma::IsValidNode<std::pair<ma::Item::key_type, ma::Var::key_type>>(node);

    std::wstring item_key = node->m_Nodes.size() >= 2u ? std::wstring{node->m_Nodes[0]->m_String} : L"";

    return result &&
           (node->m_String.empty() || ((node->m_Nodes.size() >= 2u) && ma::Item::HasItem(item_key) &&
                                       ma::Item::GetItem(item_key)->HasVar(std::wstring{node->m_Nodes[1u]->m_String})));
}

template<typename T_VariablePtr>
bool ma::IsValidNode(const MA_SFINAE_NODE_VAR_PTR &node)
{
    bool result = ma::IsValidNode<std::pair<ma::Item::key_type, ma::Var::key_type>>(node);

    std::wstring item_key = node->m_Nodes.size() >= 2u ? std::wstring{node->m_Nodes[0]->m_String} : L"";

    return result &&
           (node->m_String.empty() || ((node->m_Nodes.size() >= 2u) && ma::Item::HasItem(item_key) &&
                                       ma::Item::GetItem(item_key)->HasVar(std::wstring{node->m_Nodes[1u]->m_String})));
}

template<typename T_Variable>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_VAR &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_Variable>() + L"]"s;

    auto errors_pair = ma::GetErrors<std::pair<ma::Item::key_type, ma::Var::key_type>>(node);
    errors.insert(errors.end(), errors_pair.begin(), errors_pair.end());

    if(node->m_Nodes.size() >= 2u)
    {
        const auto node_item = node->m_Nodes[0];
        const std::wstring item_key = std::wstring{node_item->m_String};

        if(!item_key.empty() && !ma::Id::IsValid(item_key))
            errors.push_back({node_item->m_Index, type_name + L"ma::Id::IsValid(item_key) == false"});

        if(!item_key.empty() && !ma::Item::HasItem(item_key))
            errors.push_back({node_item->m_Index, type_name + L"ma::Item::HasItem(item_key) == false"});

        if(ma::Item::HasItem(item_key))
        {
            const auto node_var = node->m_Nodes[1u];
            const std::wstring var_key = std::wstring{node_var->m_String};
            const auto item = ma::Item::GetItem(item_key);

            if(!var_key.empty() && !item->HasVar(var_key))
                errors.push_back({node_var->m_Index, type_name + L"item->HasVar(var_key) == false"});
        }
    }

    return errors;
}

template<typename T_VariablePtr>
ma::Serialization::Errors ma::GetErrors(const MA_SFINAE_NODE_VAR_PTR &node)
{
    ma::Serialization::Errors errors;

    const std::wstring type_name = L"["s + ma::TypeNameW<T_VariablePtr>() + L"]"s;

    auto errors_pair = ma::GetErrors<std::pair<ma::Item::key_type, ma::Var::key_type>>(node);
    errors.insert(errors.end(), errors_pair.begin(), errors_pair.end());

    if(node->m_Nodes.size() >= 2u)
    {
        const auto node_item = node->m_Nodes[0];
        const std::wstring item_key = std::wstring{node_item->m_String};

        if(!item_key.empty() && !ma::Id::IsValid(item_key))
            errors.push_back({node_item->m_Index, type_name + L"ma::Id::IsValid(item_key) == false"});

        if(!item_key.empty() && !ma::Item::HasItem(item_key))
            errors.push_back({node_item->m_Index, type_name + L"ma::Item::HasItem(item_key) == false"});

        if(ma::Item::HasItem(item_key))
        {
            const auto node_var = node->m_Nodes[1u];
            const std::wstring var_key = std::wstring{node_var->m_String};
            const auto item = ma::Item::GetItem(item_key);

            if(!var_key.empty() && !item->HasVar(var_key))
                errors.push_back({node_var->m_Index, type_name + L"item->HasVar(var_key) == false"});
        }
    }

    return errors;
}
