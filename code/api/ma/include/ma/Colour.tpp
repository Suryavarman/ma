/// \file Colour.tpp
/// \author Pontier Pierre
/// \date 2020-10-25
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <limits>

namespace ma
{
    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    const size_t VarColour<T, T_ComponentCount, T_ComponentType>::m_ComponentCount = T_ComponentCount;

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    template<typename... Args>
    VarColour<T, T_ComponentCount, T_ComponentType>::VarColour(const ma::Var::ConstructorParameters &params,
                                                               const Args &...args):
    Var(params),
    m_Value(args...),
    m_Previous(),
    m_Default(args...)
    {}

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    VarColour<T, T_ComponentCount, T_ComponentType>::VarColour(const ma::Var::ConstructorParameters &params):
    Var(params),
    m_Value(),
    m_Previous(),
    m_Default()
    {}

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    VarColour<T, T_ComponentCount, T_ComponentType>::~VarColour()
    {}

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    void VarColour<T, T_ComponentCount, T_ComponentType>::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Previous = m_Value;
        m_Value = m_Default;
        UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    std::wstring VarColour<T, T_ComponentCount, T_ComponentType>::toString() const
    {
        MA_ASSERT(false, L"Cette fonction est a implémentée.", std::logic_error);

        return L"";
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    std::wstring VarColour<T, T_ComponentCount, T_ComponentType>::GetKeyValueToString() const
    {
        MA_ASSERT(false, L"Cette fonction est a implémentée.", std::logic_error);

        return ma::toString(std::make_pair(m_Key, L""s));
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    void VarColour<T, T_ComponentCount, T_ComponentType>::fromString(const std::wstring &str)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(str);
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    bool VarColour<T, T_ComponentCount, T_ComponentType>::IsValidString(const std::wstring &str) const
    {
        MA_ASSERT(false, L"N'est pas encore implémentée.", std::logic_error);

        return true;
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    void VarColour<T, T_ComponentCount, T_ComponentType>::Set(const T &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    T VarColour<T, T_ComponentCount, T_ComponentType>::Get() const
    {
        return m_Value;
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    T VarColour<T, T_ComponentCount, T_ComponentType>::GetPrevious() const
    {
        return m_Previous;
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    void VarColour<T, T_ComponentCount, T_ComponentType>::SetFromItem(const std::wstring &str)
    {
        MA_ASSERT(false, L"N'est pas encore implémenté.", std::logic_error);
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    void VarColour<T, T_ComponentCount, T_ComponentType>::SetFromItem(const T &value)
    {
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    VarPtr VarColour<T, T_ComponentCount, T_ComponentType>::Copy() const
    {
        auto ptr = std::shared_ptr<VarColour<T, T_ComponentCount, T_ComponentType>>(
            new VarColour<T, T_ComponentCount, T_ComponentType>(GetConstructorParameters(), m_Value));

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    VarColour<T, T_ComponentCount, T_ComponentType> &VarColour<T, T_ComponentCount, T_ComponentType>::operator=(
        const VarColour<T, T_ComponentCount, T_ComponentType>::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }
} // namespace ma
