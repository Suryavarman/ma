/// \file Variable.h
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"
#include "ma/Dll.h"
#include "ma/Item.h" // La déclaration de la classe Variable est dans Item.h
#include "ma/Id.h"
#include "ma/Serialization.h"

#include <typeinfo>
#include <regex>

#include <wx/filename.h>

namespace ma
{
    template<typename T>
    class T_Var: public Var
    {
        public:
            typedef T value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut
            value_type m_Default;

            void SetFromItem(const std::wstring &) override;

            template<typename T_Var, typename... Args>
            friend void Item::SetVarFromValue(const VarsMap::key_type &key, const Args &...args);
            virtual void SetFromItem(const value_type &value);

            ma::VarPtr Copy() const override;

        public:
            explicit T_Var(const ma::Var::ConstructorParameters &params, const value_type &value);
            explicit T_Var(const ma::Var::ConstructorParameters &params);
            T_Var() = delete;
            T_Var(const T_Var &) = delete;
            ~T_Var() override;

            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;
            void fromString(const std::wstring &) override;

            bool IsValidString(const std::wstring &value) const override;
            void Reset() override;

            virtual void Set(const value_type &value);
            const value_type &Get() const;
            const value_type &GetPrevious() const;

            virtual T_Var<T> &operator=(const value_type &value);
    };

    template<>
    class M_DLL_EXPORT T_Var<std::wstring>: public Var
    {
        public:
            typedef std::wstring value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut
            value_type m_Default;

            void SetFromItem(const std::wstring &) override;

            ma::VarPtr Copy() const override;

        public:
            explicit T_Var(const ma::Var::ConstructorParameters &params, const value_type &value);
            explicit T_Var(const ma::Var::ConstructorParameters &params);
            T_Var() = delete;
            T_Var(const T_Var &) = delete;
            ~T_Var() override;

            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;
            void fromString(const std::wstring &) override;

            bool IsValidString(const std::wstring &value) const override;
            void Reset() override;

            virtual void Set(const value_type &value);
            const value_type &Get() const;
            const value_type &GetPrevious() const;

            virtual T_Var<std::wstring> &operator=(const value_type &value);
    };

    class M_DLL_EXPORT IntVar: public T_Var<int>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<int>::T_Var;

            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(IntVar)
    };

    class M_DLL_EXPORT LongVar: public T_Var<long>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<long>::T_Var;
            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(LongVar)
    };

    class M_DLL_EXPORT LongLongVar: public T_Var<long long>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<long long>::T_Var;
            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(LongLongVar)
    };

    /// \remarks Le type « unsigned » est le même que « unsigned int ».
    class M_DLL_EXPORT UVar: public T_Var<unsigned>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<unsigned>::T_Var;
            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(UVar)
    };

    class M_DLL_EXPORT ULongVar: public T_Var<unsigned long>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<unsigned long>::T_Var;
            bool IsValidString(const std::wstring &value) const override;

            M_HEADER_CLASSHIERARCHY(ULongVar)
    };

    class M_DLL_EXPORT ULongLongVar: public T_Var<unsigned long long>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<unsigned long long>::T_Var;
            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(ULongLongVar)
    };

    //    /// Existe pour être sur de garder la bonne précision proposée par
    //    /// wxULongLong.
    //    /// \see https://docs.wxwidgets.org/trunk/classwx_u_long_long.html#a9bab432053f76690f22fe68da7888b09
    //    class M_DLL_EXPORT wxULongLongVar : public T_Var<wxULongLong>
    //    {
    //        protected:
    //            void SetFromItem(const std::wstring& value) override;
    //            ma::VarPtr Copy() const override;
    //
    //        public:
    //            using T_Var<wxULongLong>::T_Var;
    //            std::wstring toString() const override;
    //            bool IsValidString(const std::wstring& value) const override;
    //            M_HEADER_CLASSHIERARCHY(wxULongLongVar)
    //    };
    //    typedef std::shared_ptr<ma::wxULongLongVar> wxULongLongVarPtr;
    //    template M_DLL_EXPORT wxULongLongVarPtr ma::Item::AddVar<ma::wxULongLongVar>(const ma::Var::key_type& key,
    //    const unsigned long long& value); template M_DLL_EXPORT wxULongLongVarPtr
    //    ma::Item::AddVar<ma::wxULongLongVar>(const ma::Var::key_type& key);

    class M_DLL_EXPORT FloatVar: public T_Var<float>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<float>::T_Var;
            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(FloatVar)
    };

    class M_DLL_EXPORT DoubleVar: public T_Var<double>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<double>::T_Var;
            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(DoubleVar)
    };

    class M_DLL_EXPORT LongDoubleVar: public T_Var<long double>
    {
        protected:
            void SetFromItem(const std::wstring &value) override;
            ma::VarPtr Copy() const override;

        public:
            using T_Var<long double>::T_Var;
            bool IsValidString(const std::wstring &value) const override;
            M_HEADER_CLASSHIERARCHY(LongDoubleVar)
    };

    typedef std::shared_ptr<ma::IntVar> IntVarPtr;
    typedef std::shared_ptr<ma::LongVar> LongVarPtr;
    typedef std::shared_ptr<ma::LongLongVar> LongLongVarPtr;
    typedef std::shared_ptr<ma::UVar> UVarPtr;
    typedef std::shared_ptr<ma::ULongVar> ULongVarPtr;
    typedef std::shared_ptr<ma::ULongLongVar> ULongLongVarPtr;
    typedef std::shared_ptr<ma::FloatVar> FloatVarPtr;
    typedef std::shared_ptr<ma::DoubleVar> DoubleVarPtr;
    typedef std::shared_ptr<ma::LongDoubleVar> LongDoubleVarPtr;

    template M_DLL_EXPORT IntVarPtr ma::Item::AddVar<ma::IntVar>(const ma::Var::key_type &key, const int &value);
    template M_DLL_EXPORT IntVarPtr ma::Item::AddVar<ma::IntVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT LongVarPtr ma::Item::AddVar<ma::LongVar>(const ma::Var::key_type &key, const long &value);
    template M_DLL_EXPORT LongVarPtr ma::Item::AddVar<ma::LongVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT LongLongVarPtr ma::Item::AddVar<ma::LongLongVar>(const ma::Var::key_type &key,
                                                                           const long long &value);
    template M_DLL_EXPORT LongLongVarPtr ma::Item::AddVar<ma::LongLongVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT UVarPtr ma::Item::AddVar<ma::UVar>(const ma::Var::key_type &key, const unsigned &value);
    template M_DLL_EXPORT UVarPtr ma::Item::AddVar<ma::UVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT ULongVarPtr ma::Item::AddVar<ma::ULongVar>(const ma::Var::key_type &key,
                                                                     const unsigned long &value);
    template M_DLL_EXPORT ULongVarPtr ma::Item::AddVar<ma::ULongVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT ULongLongVarPtr ma::Item::AddVar<ma::ULongLongVar>(const ma::Var::key_type &key,
                                                                             const unsigned long long &value);
    template M_DLL_EXPORT ULongLongVarPtr ma::Item::AddVar<ma::ULongLongVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT FloatVarPtr ma::Item::AddVar<ma::FloatVar>(const ma::Var::key_type &key, const float &value);
    template M_DLL_EXPORT FloatVarPtr ma::Item::AddVar<ma::FloatVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT DoubleVarPtr ma::Item::AddVar<ma::DoubleVar>(const ma::Var::key_type &key,
                                                                       const double &value);
    template M_DLL_EXPORT DoubleVarPtr ma::Item::AddVar<ma::DoubleVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT LongDoubleVarPtr ma::Item::AddVar<ma::LongDoubleVar>(const ma::Var::key_type &key,
                                                                               const long double &value);
    template M_DLL_EXPORT LongDoubleVarPtr ma::Item::AddVar<ma::LongDoubleVar>(const ma::Var::key_type &key);

    class M_DLL_EXPORT StringVar: virtual public Var
    {
        public:
            typedef std::wstring value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut.
            value_type m_Default;

            friend Item;
            void SetFromItem(const std::wstring &) override;
            ma::VarPtr Copy() const override;

        public:
            explicit StringVar(const ma::Var::ConstructorParameters &params, const value_type &value);
            explicit StringVar(const ma::Var::ConstructorParameters &params, const value_type::value_type *value);
            explicit StringVar(const ma::Var::ConstructorParameters &params, const wxString &value);
            explicit StringVar(const ma::Var::ConstructorParameters &params);
            StringVar() = delete;
            StringVar(const StringVar &) = delete;
            ~StringVar() override;

            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;
            void fromString(const std::wstring &) override;

            /// \return true
            bool IsValidString(const std::wstring &value) const override;
            void Reset() override;

            void Set(const value_type &value);
            const value_type &Get() const;
            const value_type &GetPrevious() const;

            virtual StringVar &operator=(const ma::StringVar::value_type &value);
            virtual bool operator!=(const ma::StringVar::value_type &value);
            virtual bool operator==(const ma::StringVar::value_type &value);

            virtual StringVar &operator=(const wxString &value);
            virtual bool operator!=(const wxString &value);
            virtual bool operator==(const wxString &value);

            M_HEADER_CLASSHIERARCHY(StringVar)
    };
    typedef std::shared_ptr<ma::StringVar> StringVarPtr;

    template M_DLL_EXPORT StringVarPtr ma::Item::AddVar<ma::StringVar>(const ma::Var::key_type &key,
                                                                       const StringVar::value_type &value);

    template M_DLL_EXPORT StringVarPtr ma::Item::AddVar<ma::StringVar>(const ma::Var::key_type &key);

    /*
    /// Un enum sera décris de cette manière:
    /// R"#({value: {value_1: "name_1", value_2: "name_2"}})#"
    /// value : la valeur sélectionnée
    /// {value_1: "name_1", value_2: "name_2"}: la description des valeurs
    /// possibles et leur intitulé respectif.
    class M_DLL_EXPORT EnumVar: virtual public Var
    {
        public:
            /// Type de la valeur.
            typedef unsigned int value_type;

            /// Type du nom représentant la valeur choisie.
            typedef std::wstring name_type;

            /// Type du conteneur servant à stocker les valeurs possibles.
            typedef std::unordered_map<value_type, name_type> enum_type;

        protected:
            value_type m_Value,
                       m_Previous, ///< L'ancienne valeur.
                       m_Default; ///< La valeur par défaut.

            const enum_type m_Enumration, /// Énumération actuelle
                      m_PreviousEnumration, ///< L'ancienne énumération
                      m_DefaultEnumration; ///< L'énumération par défaut.

            void SetFromItem(const std::wstring&) override;
            ma::VarPtr Copy() const override;

        public:
            explicit EnumVar(const ma::Var::key_type& key, bool dynamic, bool read_only, const value_type& value, const
    enum_type& enumeration); explicit EnumVar(const ma::Var::key_type& key, bool dynamic, bool read_only, const
    enum_type& enumeration); explicit EnumVar(const ma::Var::key_type& key, bool dynamic, bool read_only); EnumVar() =
    delete; EnumVar(const EnumVar&)=delete;
    ~EnumVar() override;

            std::wstring toString() const override;
            void fromString(const std::wstring&) override;

            /// \return true
            bool IsValidString(const std::wstring& value) const override;
            void Reset() override;

            void Set(const value_type& value);
            const value_type& Get() const;
            const value_type& GetPrevious() const;

            void Set(const enum_type& enumeration);
            const enum_type& Get() const;
            const enum_type& GetPrevious() const;

            M_HEADER_CLASSHIERARCHY(EnumVar)
    };
    typedef std::shared_ptr<ma::EnumVar> EnumVarPtr;

    template
    M_DLL_EXPORT EnumVarPtr ma::Item::AddVar<ma::EnumVar>(const ma::Var::key_type& key, const value_type& value,
    const enum_type& enumeration);

    template
    M_DLL_EXPORT EnumVarPtr ma::Item::AddVar<ma::EnumVar>(const ma::Var::key_type& key, const enum_type&
    enumeration);

    template
    M_DLL_EXPORT EnumVarPtr ma::Item::AddVar<ma::EnumVar>(const ma::Var::key_type& key, const EnumVar::value_type&
    value);

    template
    M_DLL_EXPORT EnumVarPtr ma::Item::AddVar<ma::EnumVar>(const ma::Var::key_type& key);
    */

    /// \remarks utilise PathVar::toPath
    M_DLL_EXPORT std::filesystem::path Path(const std::wstring &value);

    /// Gestion des chemins représentant un fichier ou un dossier.
    class M_DLL_EXPORT PathVar: public Var
    {
        public:
            typedef std::filesystem::path value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut.
            value_type m_Default;

            void SetFromItem(const std::wstring &) override;

            /// SetFromItem est ami d'Item. L'idéal aurait été seulement d'être ami de la fonction.
            /// friend void Item::SetVarFromValue(const VarsMap::key_type &key, const value_type& value);
            /// template<typename T_Var, typename... Args>
            /// friend void Item::SetVarFromValue(const VarsMap::key_type &key, const Args &...args);
            friend Item;
            virtual void SetFromItem(const value_type &value);

            ma::VarPtr Copy() const override;

        public:
            /// Clef pour retrouver si le chemin doit exister ou pas.
            static const std::wstring ms_KeyMustExist;

            PathVar(const ma::Var::ConstructorParameters &params, const value_type &value);
            PathVar(const ma::Var::ConstructorParameters &params, const std::wstring &value);
            // explicit PathVar(const ma::Var::ConstructorParameters &params, const std::filesystem::path &value);
            explicit PathVar(const ma::Var::ConstructorParameters &params);
            PathVar() = delete;
            PathVar(const PathVar &) = delete;
            ~PathVar() override;

            /// Par défaut retourne faux.
            bool MustExist() const;

            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;
            void fromString(const std::wstring &) override;

            /// \return true
            bool IsValidString(const std::wstring &value) const override;
            virtual bool IsValidValue(const value_type &value) const;
            void Reset() override;

            virtual void Set(const value_type &value);
            const value_type &Get() const;
            const value_type &GetPrevious() const;

            static value_type toPath(const std::wstring &path);

            virtual PathVar &operator=(const ma::PathVar::value_type &value);
            virtual PathVar &operator=(const std::wstring &value);
            virtual bool operator!=(const ma::PathVar::value_type &value);
            virtual bool operator==(const ma::PathVar::value_type &value);
            virtual bool operator!=(const std::wstring &value);
            virtual bool operator==(const std::wstring &value);

            M_HEADER_CLASSHIERARCHY(PathVar)
    };
    typedef std::shared_ptr<ma::PathVar> PathVarPtr;

    template M_DLL_EXPORT PathVarPtr ma::Item::AddVar<ma::PathVar>(const ma::Var::key_type &key,
                                                                   const ma::PathVar::value_type &value);

    template M_DLL_EXPORT PathVarPtr ma::Item::AddVar<ma::PathVar>(const ma::Var::key_type &key);

    /// Gestion des chemins représentant un dossier.
    class M_DLL_EXPORT DirectoryVar: public ma::PathVar
    {
        protected:
            ma::VarPtr Copy() const override;

        public:
            using ma::PathVar::PathVar;

            /// Le chemin doit exister.
            bool IsValidString(const std::wstring &value) const override;

            /// Le répertoire doit exister.
            bool IsValidValue(const value_type &value) const override;

            virtual std::filesystem::directory_entry toEntry() const;

            M_HEADER_CLASSHIERARCHY(DirectoryVar)
    };
    typedef std::shared_ptr<ma::DirectoryVar> DirectoryVarPtr;

    template M_DLL_EXPORT DirectoryVarPtr ma::Item::AddVar<ma::DirectoryVar>(const ma::Var::key_type &key,
                                                                             const DirectoryVar::value_type &value);

    template M_DLL_EXPORT DirectoryVarPtr ma::Item::AddVar<ma::DirectoryVar>(const ma::Var::key_type &key);

    /// Gestion des chemins représentant un fichier.
    class M_DLL_EXPORT FilePathVar: public ma::PathVar
    {
        protected:
            ma::VarPtr Copy() const override;

        public:
            typedef std::unordered_set<std::wstring> Extensions;
            /// Clef pour retrouver dans les informations de la variable les extensions acceptées par cette variable.
            /// \code {.cpp}
            /// {ms_KeyExtension, "bmp, jpg, png"}
            /// \endcode
            static const std::wstring ms_KeyExtension;

            /// Les extensions seront délimitées ainsi :
            /// \code {.cpp}
            /// {ms_KeyExtension, "bmp, jpg, png"}
            /// \endcode
            static const char ms_ExtensionDelimiter;

            using ma::PathVar::PathVar;

            bool IsValidString(const std::wstring &value) const override;
            bool IsValidValue(const value_type &value) const override;

            /// \return L'extension du fichier.
            std::wstring GetExt() const;

            /// \return Les extensions acceptées par cette variable.
            virtual Extensions GetExtensions() const;

            /// Indique si le chemin existe.
            virtual bool Exist() const;

            M_HEADER_CLASSHIERARCHY(FilePathVar)
    };
    typedef std::shared_ptr<ma::FilePathVar> FilePathVarPtr;

    template M_DLL_EXPORT FilePathVarPtr ma::Item::AddVar<ma::FilePathVar>(const ma::Var::key_type &key,
                                                                           const ma::FilePathVar::value_type &value);

    template M_DLL_EXPORT FilePathVarPtr ma::Item::AddVar<ma::FilePathVar>(const ma::Var::key_type &key);

    class M_DLL_EXPORT DefineVar: virtual public Var
    {
        protected:
            void SetFromItem(const std::wstring &) override;
            ma::VarPtr Copy() const override;

        public:
            explicit DefineVar(const ma::Var::ConstructorParameters &params);
            DefineVar() = delete;
            DefineVar(const DefineVar &) = delete;
            ~DefineVar() override;

            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;
            void fromString(const std::wstring &) override;

            void Reset() override;

            /// \return true
            bool IsValidString(const std::wstring &value) const override;

            M_HEADER_CLASSHIERARCHY(DefineVar)
    };
    typedef std::shared_ptr<ma::DefineVar> DefineVarPtr;

    template M_DLL_EXPORT DefineVarPtr ma::Item::AddVar<ma::DefineVar>(const ma::Var::key_type &key);

    class M_DLL_EXPORT BoolVar: virtual public Var
    {
        public:
            typedef bool value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut.
            value_type m_Default;

            void SetFromItem(const std::wstring &) override;

            /// SetFromItem est ami d'Item. L'idéal aurait été seulement d'être ami de la fonction
            /// « void Item::SetVarFromValue<BoolVar>(const VarsMap::key_type &key, const value_type &value); »
            friend Item;
            virtual void SetFromItem(const value_type &value);

            ma::VarPtr Copy() const override;

        public:
            BoolVar(const ma::Var::ConstructorParameters &params, value_type var);
            explicit BoolVar(const ma::Var::ConstructorParameters &params);
            BoolVar() = delete;
            BoolVar(const BoolVar &) = delete;
            ~BoolVar() override;

            std::wstring toString() const override;
            std::wstring GetKeyValueToString() const override;
            void fromString(const std::wstring &) override;
            bool IsValidString(const std::wstring &) const override;
            void Reset() override;

            void Set(value_type value);
            value_type Get() const;
            value_type GetPrevious() const;

            /// Permet d'utiliser le même test que IsValidString
            static bool IsBool(const std::wstring &);

            /// Permet d'utiliser la même conversion fromString
            static bool ToBool(const std::wstring &);

            /// Permet d'utiliser la même conversion toString
            static std::wstring ToStr(bool value);

            virtual BoolVar &operator=(ma::BoolVar::value_type value);

            M_HEADER_CLASSHIERARCHY(BoolVar)
    };
    typedef std::shared_ptr<ma::BoolVar> BoolVarPtr;

    template M_DLL_EXPORT BoolVarPtr ma::Item::AddVar<ma::BoolVar>(const ma::Var::key_type &key,
                                                                   const BoolVar::value_type &value);

    template M_DLL_EXPORT BoolVarPtr ma::Item::AddVar<ma::BoolVar>(const ma::Var::key_type &key);

    class SetVar: public Var
    {
        public:
            struct Key
            {
                    struct Obs
                    {
                            /// Permet de notifier le début de l'insertion d'une nouvelle valeur.
                            static const ma::Observable::Data::key_type ms_BeginInsertValue;

                            /// Permet de notifier la fin de l'insertion d'une nouvelle valeur.
                            static const ma::Observable::Data::key_type ms_EndInsertValue;

                            /// Permet de notifier le début de la suppression d'une valeur.
                            static const ma::Observable::Data::key_type ms_BeginEraseValue;

                            /// Permet de notifier la fin de la suppression d'une valeur.
                            static const ma::Observable::Data::key_type ms_EndEraseValue;
                    };
            };

            explicit SetVar(const ma::Var::ConstructorParameters &params);
            ~SetVar() override;

            M_HEADER_CLASSHIERARCHY(SetVar)
    };

    class VectorVar: public Var
    {
        public:
            struct Key
            {
                    struct Obs
                    {
                            /// Permet de notifier le début de l'ajout d'une nouvelle valeur à la fin vu tableau.
                            static const ma::Observable::Data::key_type ms_BeginPushBackValue;

                            /// Permet de notifier la fin de l'ajout d'une nouvelle valeur à la fin vu tableau.
                            static const ma::Observable::Data::key_type ms_EndPushBackValue;

                            /// Permet de notifier le début de l'insertion d'une nouvelle valeur
                            static const ma::Observable::Data::key_type ms_BeginInsertValue;

                            /// Permet de notifier la fin de l'insertion d'une nouvelle valeur
                            static const ma::Observable::Data::key_type ms_EndInsertValue;

                            /// Permet de notifier le début de la suppression d'une valeur.
                            static const ma::Observable::Data::key_type ms_BeginEraseValue;

                            /// Permet de notifier la fin de la suppression d'une valeur
                            static const ma::Observable::Data::key_type ms_EndEraseValue;
                    };
            };

            explicit VectorVar(const ma::Var::ConstructorParameters &params);
            ~VectorVar() override;

            M_HEADER_CLASSHIERARCHY(VectorVar)
    };

    class HashSetVar: public Var
    {
        public:
            struct Key
            {
                    struct Obs
                    {
                            /// Permet de notifier le début de l'insertion d'une nouvelle valeur
                            static const ma::Observable::Data::key_type ms_BeginInsertValue;

                            /// Permet de notifier la fin de l'insertion d'une nouvelle valeur
                            static const ma::Observable::Data::key_type ms_EndInsertValue;

                            /// Permet de notifier le début de la suppression d'une valeur.
                            static const ma::Observable::Data::key_type ms_BeginEraseValue;

                            /// Permet de notifier la fin de la suppression d'une valeur
                            static const ma::Observable::Data::key_type ms_EndEraseValue;
                    };
            };

            explicit HashSetVar(const ma::Var::ConstructorParameters &params);
            ~HashSetVar() override;

            M_HEADER_CLASSHIERARCHY(HashSetVar)
    };

    class M_DLL_EXPORT StringHashSetVar: public HashSetVar
    {
        public:
            typedef std::unordered_set<std::wstring> value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut.
            value_type m_Default;

            void SetFromItem(const std::wstring &) override;

            /// SetFromItem est ami d'Item. L'idéal aurait été seulement d'être ami de la fonction
            /// « void Item::SetVarFromValue<StringHashSetVar>(const VarsMap::key_type &key, const value_type &value); »
            friend Item;
            virtual void SetFromItem(const value_type &value);
            ma::VarPtr Copy() const override;

        public:
            StringHashSetVar(const ma::Var::ConstructorParameters &params, const value_type &value);
            explicit StringHashSetVar(const ma::Var::ConstructorParameters &params);
            StringHashSetVar() = delete;
            StringHashSetVar(const StringHashSetVar &) = delete;
            ~StringHashSetVar() override;

            std::wstring toString() const override;
            std::wstring GetKeyValueToString() const override;

            /// Examples de paramètre:
            /// \code {.cpp}
            /// R"#({"12345", "TOTO1", "TOTO2", "TOTO3"})#"
            /// R"#({"TOTO0", "TOTO1", "TOTO2", "TOTO3"})#"
            /// R"#({"TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })#"
            /// R"#({   "TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })#"
            /// R"#({"TOTO"  })#"
            /// R"#({"TOTO"})#"
            /// R"#(          {"TOTO"}                    )#"
            /// R"#({"TOTO"}     )#"
            /// R"#({""})#"
            /// R"#({""}   )#"
            /// R"#({" "})#"
            /// R"#({" TOTO"})#"
            /// R"#({"TOTO "})#"
            /// R"#({" TOTO "})#"
            /// R"#({ " TOTO " , " TOTO2" })#"
            /// R"#({" TOTO ","TOTO2"})#"
            /// R"#({""TRUC1", "LOLO1"})#"
            /// R"#({"}"TRUC2", "LOLO2"})#"
            /// R"#({"},",{"", "LOLO2"})#"
            /// \endcode
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/wNG3QM/4
            void fromString(const std::wstring &) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement
            /// correct.
            /// \remarks à utiliser avant d'appeler fromString.
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/vYZWdG/12
            bool IsValidString(const std::wstring &) const override;
            void Reset() override;

            void Set(const value_type &value);
            const value_type &Get() const;
            const value_type &GetPrevious() const;

            virtual StringHashSetVar &operator=(const value_type &value);

            M_HEADER_CLASSHIERARCHY(StringHashSetVar)
    };
    typedef std::shared_ptr<ma::StringHashSetVar> StringHashSetVarPtr;

    template M_DLL_EXPORT StringHashSetVarPtr
    ma::Item::AddVar<ma::StringHashSetVar>(const ma::Var::key_type &key, const StringHashSetVar::value_type &value);

    template M_DLL_EXPORT StringHashSetVarPtr ma::Item::AddVar<ma::StringHashSetVar>(const ma::Var::key_type &key);

    class M_DLL_EXPORT StringSetVar: public SetVar
    {
        public:
            typedef std::set<std::wstring> value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut.
            value_type m_Default;

            void SetFromItem(const std::wstring &) override;

            /// SetFromItem est ami d'Item. L'idéal aurait été seulement d'être ami de la fonction
            /// « void Item::SetVarFromValue<StringSetVar>(const VarsMap::key_type &key, const value_type &value); »
            friend Item;
            virtual void SetFromItem(const value_type &value);
            ma::VarPtr Copy() const override;

        public:
            StringSetVar(const ma::Var::ConstructorParameters &params, const value_type &value);
            explicit StringSetVar(const ma::Var::ConstructorParameters &params);
            StringSetVar() = delete;
            StringSetVar(const StringSetVar &) = delete;
            ~StringSetVar() override;

            std::wstring toString() const override;
            std::wstring GetKeyValueToString() const override;

            /// Examples de paramètre:
            /// \code {.cpp}
            /// R"#({"12345", "TOTO1", "TOTO2", "TOTO3"})#"
            /// R"#({"TOTO0", "TOTO1", "TOTO2", "TOTO3"})#"
            /// R"#({"TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })#"
            /// R"#({   "TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })#"
            /// R"#({"TOTO"  })#"
            /// R"#({"TOTO"})#"
            /// R"#(          {"TOTO"}                    )#"
            /// R"#({"TOTO"}     )#"
            /// R"#({""})#"
            /// R"#({""}   )#"
            /// R"#({" "})#"
            /// R"#({" TOTO"})#"
            /// R"#({"TOTO "})#"
            /// R"#({" TOTO "})#"
            /// R"#({ " TOTO " , " TOTO2" })#"
            /// R"#({" TOTO ","TOTO2"})#"
            /// R"#({""TRUC1", "LOLO1"})#"
            /// R"#({"}"TRUC2", "LOLO2"})#"
            /// R"#({"},",{"", "LOLO2"})#"
            /// \endcode
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/wNG3QM/4
            void fromString(const std::wstring &) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement
            /// correct.
            /// \remarks à utiliser avant d'appeler fromString.
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/vYZWdG/12
            bool IsValidString(const std::wstring &) const override;
            void Reset() override;

            void Set(const value_type &value);
            const value_type &Get() const;
            const value_type &GetPrevious() const;

            virtual StringSetVar &operator=(const value_type &value);

            M_HEADER_CLASSHIERARCHY(StringSetVar)
    };
    typedef std::shared_ptr<ma::StringSetVar> StringSetVarPtr;

    template M_DLL_EXPORT StringSetVarPtr ma::Item::AddVar<ma::StringSetVar>(const ma::Var::key_type &key,
                                                                             const StringSetVar::value_type &value);

    template M_DLL_EXPORT StringSetVarPtr ma::Item::AddVar<ma::StringSetVar>(const ma::Var::key_type &key);

    class M_DLL_EXPORT StringVectorVar: public VectorVar
    {
        public:
            typedef std::vector<std::wstring> value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            /// L'ancienne valeur.
            value_type m_Previous;

            /// La valeur par défaut.
            value_type m_Default;

            void SetFromItem(const std::wstring &) override;

            /// SetFromItem est ami d'Item. L'idéal aurait été seulement d'être ami de la fonction
            /// « void Item::SetVarFromValue<StringSetVar>(const VarsMap::key_type &key, const value_type &value); »
            friend Item;
            virtual void SetFromItem(const value_type &value);
            ma::VarPtr Copy() const override;

        public:
            StringVectorVar(const ma::Var::ConstructorParameters &params, const value_type &value);
            explicit StringVectorVar(const ma::Var::ConstructorParameters &params);
            StringVectorVar() = delete;
            StringVectorVar(const StringSetVar &) = delete;
            ~StringVectorVar() override;

            std::wstring toString() const override;
            std::wstring GetKeyValueToString() const override;

            /// Examples de paramètre:
            /// \code {.cpp}
            /// R"#({"12345", "TOTO1", "TOTO2", "TOTO3"})#"
            /// R"#({"TOTO0", "TOTO1", "TOTO2", "TOTO3"})#"
            /// R"#({"TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })#"
            /// R"#({   "TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })#"
            /// R"#({"TOTO"  })#"
            /// R"#({"TOTO"})#"
            /// R"#(          {"TOTO"}                    )#"
            /// R"#({"TOTO"}     )#"
            /// R"#({""})#"
            /// R"#({""}   )#"
            /// R"#({" "})#"
            /// R"#({" TOTO"})#"
            /// R"#({"TOTO "})#"
            /// R"#({" TOTO "})#"
            /// R"#({ " TOTO " , " TOTO2" })#"
            /// R"#({" TOTO ","TOTO2"})#"
            /// R"#({""TRUC1", "LOLO1"})#"
            /// R"#({"}"TRUC2", "LOLO2"})#"
            /// R"#({"},",{"", "LOLO2"})#"
            /// \endcode
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/wNG3QM/4
            void fromString(const std::wstring &) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement
            /// correct.
            /// \remarks à utiliser avant d'appeler fromString.
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/vYZWdG/12
            bool IsValidString(const std::wstring &) const override;
            void Reset() override;

            void Set(const value_type &value);
            const value_type &Get() const;
            const value_type &GetPrevious() const;

            virtual StringVectorVar &operator=(const value_type &value);

            M_HEADER_CLASSHIERARCHY(StringVectorVar)
    };
    typedef std::shared_ptr<ma::StringVectorVar> StringVectorVarPtr;

    template M_DLL_EXPORT StringVectorVarPtr
    ma::Item::AddVar<ma::StringVectorVar>(const ma::Var::key_type &key, const StringVectorVar::value_type &value);

    template M_DLL_EXPORT StringVectorVarPtr ma::Item::AddVar<ma::StringVectorVar>(const ma::Var::key_type &key);

    template<typename T_Item>
    class T_ItemHashSetVar: public HashSetVar
    {
        public:
            typedef T_Item item_type;
            typedef std::shared_ptr<item_type> item_ptr;
            typedef std::unordered_set<item_ptr> value_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            void SetFromItem(const std::wstring &) override;

            /// SetFromItem est ami d'Item. L'idéal aurait été seulement d'être ami de la fonction
            /// « void Item::SetVarFromValue<T_ItemHashSetVar<T>>(const VarsMap::key_type &key, const value_type
            /// &value); »
            friend Item;
            virtual void SetFromItem(const value_type &value);

            ma::VarPtr Copy() const override;

        public:
            T_ItemHashSetVar(const ma::Var::ConstructorParameters &params, value_type value);
            explicit T_ItemHashSetVar(const ma::Var::ConstructorParameters &params);
            T_ItemHashSetVar() = delete;
            T_ItemHashSetVar(const T_ItemHashSetVar &) = delete;
            ~T_ItemHashSetVar() override;

            std::wstring toString() const override;
            std::wstring GetKeyValueToString() const override;

            /// Examples de paramètre:
            /// \code {.cpp}
            /// R"#({"00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000003",
            /// "00000000-0000-0000-0000-000000000004"})#" R"#({"00000000-0000-0000-0000-000000000002",
            /// "00000000-0000-0000-0000-000000000003",    "00000000-0000-0000-0000-000000000004"})#"
            /// R"#({"00000000-0000-0000-0000-000000000002"  })#"
            /// R"#({"00000000-0000-0000-0000-000000000002"})#"
            /// R"#(          {"00000000-0000-0000-0000-000000000002"}                    )#"
            /// R"#({"00000000-0000-0000-0000-000000000002"}     )#"
            /// \endcode
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/wNG3QM/4
            void fromString(const std::wstring &) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement
            /// correct.
            /// \remarks à utiliser avant d'appeler fromString.
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/vYZWdG/12
            bool IsValidString(const std::wstring &) const override;
            void Reset() override;

            /// \remarks Génère l'évènement ms_KeyObsBeginSetValue et
            /// ms_KeyObsEndSetValue
            void Set(const value_type &value);

            const value_type &Get() const;

            /// \remarks Génère l'évènement ms_KeyObsBeginInsertValue et
            /// ms_KeyObsEndInsertValue
            virtual void Insert(item_ptr item);

            /// \remarks Génère l'évènement ms_KeyObsBeginInsertValue et
            /// ms_KeyObsEndInsertValue
            virtual void Insert(const ma::Item::key_type &key);

            /// \remarks Génère l'évènement ms_KeyObsBeginEraseValue et
            /// ms_KeyObsEndEraseValue
            /// \exception std::invalid_argument L'item n'est pas présent dans la liste.
            /// Vous pouvez tester la présence d'un item via la fonction « HasItem ».
            virtual void Erase(item_ptr item);

            /// \remarks Génère l'évènement ms_KeyObsBeginEraseValue et
            /// ms_KeyObsEndEraseValue
            /// \exception std::invalid_argument L'item n'est pas présent dans la liste.
            /// Vous pouvez tester la présence d'un item via la fonction « HasItem ».
            virtual void Erase(const ma::Item::key_type &key);

            /// \return Vrais si l'item est présent dans le conteneur, sinon faux.
            virtual bool HasItem(item_ptr item);

            /// \return Vrais si l'item est présent dans le conteneur, sinon faux.
            virtual bool HasItem(const ma::Item::key_type &key);

            virtual T_ItemHashSetVar<T_Item> &operator=(const value_type &value);
    };

    class M_DLL_EXPORT ItemHashSetVar: public ma::T_ItemHashSetVar<ma::Item>
    {
        public:
            using ma::T_ItemHashSetVar<ma::Item>::T_ItemHashSetVar;
            M_HEADER_CLASSHIERARCHY(ItemHashSetVar)
    };
    typedef std::shared_ptr<ItemHashSetVar> ItemHashSetVarPtr;

    template M_DLL_EXPORT ItemHashSetVarPtr ma::Item::AddVar<ItemHashSetVar>(const ma::Var::key_type &key,
                                                                             const ItemHashSetVar::value_type &value);
    template M_DLL_EXPORT ItemHashSetVarPtr ma::Item::AddVar<ItemHashSetVar>(const ma::Var::key_type &key);

    template<typename T_Item>
    class T_ItemVectorVar: public VectorVar
    {
        public:
            typedef T_Item item_type;
            typedef std::shared_ptr<item_type> item_ptr;
            typedef std::vector<item_ptr> value_type;
            typedef std::vector<typename Item::key_type> keys_type;

        protected:
            /// La valeur de la variable.
            value_type m_Value;

            void SetFromItem(const std::wstring &) final;

            /// SetFromItem est ami d'Item. L'idéal aurait été seulement d'être ami de la fonction
            /// « void Item::SetVarFromValue<T_ItemHashSetVar<T>>(const VarsMap::key_type &key, const value_type
            /// &value); »
            friend Item;
            void SetFromItem(const value_type &value);

            ma::VarPtr Copy() const final;

        public:
            T_ItemVectorVar(const ma::Var::ConstructorParameters &params, value_type value);
            explicit T_ItemVectorVar(const ma::Var::ConstructorParameters &params);
            T_ItemVectorVar() = delete;
            T_ItemVectorVar(const T_ItemVectorVar &) = delete;
            ~T_ItemVectorVar() override;

            std::wstring toString() const final;
            std::wstring GetKeyValueToString() const final;

            /// Examples de paramètre:
            /// \code {.cpp}
            /// R"#({"00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000003",
            /// "00000000-0000-0000-0000-000000000004"})#" R"#({"00000000-0000-0000-0000-000000000002",
            /// "00000000-0000-0000-0000-000000000003",    "00000000-0000-0000-0000-000000000004"})#"
            /// R"#({"00000000-0000-0000-0000-000000000002"  })#"
            /// R"#({"00000000-0000-0000-0000-000000000002"})#"
            /// R"#(          {"00000000-0000-0000-0000-000000000002"}                    )#"
            /// R"#({"00000000-0000-0000-0000-000000000002"}     )#"
            /// \endcode
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/wNG3QM/4
            void fromString(const std::wstring &) final;

            /// Permet de tester si la chaîne de caractère est syntaxiquement
            /// correct.
            /// \remarks à utiliser avant d'appeler fromString.
            /// \see https://coliru.stacked-crooked.com/a/0f4990bc9a507635
            /// \see https://regex101.com/r/vYZWdG/12
            bool IsValidString(const std::wstring &) const final;
            void Reset() final;

            /// \remarks Génère l'évènement ms_KeyObsBeginSetValue et
            /// ms_KeyObsEndSetValue
            void Set(const value_type &value);

            const value_type &Get() const;

            /// \return Un vecteur des clefs des éléments.
            keys_type GetKeys() const;

            /// \remarks Génère l'évènement ms_KeyObsBeginInsertValue et ms_KeyObsEndInsertValue.
            /// \exception std::invalid_argument L'index n'est pas sur l'interval [0, m_Value.size()].
            void Insert(typename value_type::size_type index, item_ptr item);

            /// \remarks Génère l'évènement ms_KeyObsBeginInsertValue et ms_KeyObsEndInsertValue.
            /// \exception std::invalid_argument L'index n'est pas sur l'interval [0, m_Value.size()].
            void Insert(typename value_type::size_type index, const ma::Item::key_type &key);

            /// \return Le nombre d'éléments contenus dans m_Value.
            typename value_type::size_type Size() const;

            /// \return Vrais si m_Value ne contient aucun éléments.
            bool Empty() const;

            /// \remarks Génère l'évènement ms_KeyObsBeginPushBackValue et ms_KeyObsEndPushBackValue.
            void PushBack(item_ptr item);

            /// \remarks Génère l'évènement ms_KeyObsBeginPushBackValue et ms_KeyObsEndPushBackValue.
            void PushBack(const ma::Item::key_type &key);

            /// \remarks Génère l'évènement ms_KeyObsBeginEraseValue et ms_KeyObsEndEraseValue
            /// \exception std::invalid_argument L'item n'est pas présent dans la liste.
            /// Vous pouvez tester la présence d'un item via la fonction « HasItem ».
            void Erase(item_ptr item);

            /// \remarks Génère l'évènement ms_KeyObsBeginEraseValue et
            /// ms_KeyObsEndEraseValue
            /// \exception std::invalid_argument L'item n'est pas présent dans la liste.
            /// Vous pouvez tester la présence d'un item via la fonction « HasItem ».
            void Erase(const ma::Item::key_type &key);

            /// \return Vrais si l'item est présent dans le conteneur, sinon faux.
            bool HasItem(item_ptr item);

            /// \return Vrais si l'item est présent dans le conteneur, sinon faux.
            bool HasItem(const ma::Item::key_type &key);

            virtual T_ItemVectorVar<T_Item> &operator=(const value_type &value);
    };

    class M_DLL_EXPORT ItemVectorVar: public ma::T_ItemVectorVar<ma::Item>
    {
        public:
            using ma::T_ItemVectorVar<ma::Item>::T_ItemVectorVar;
            M_HEADER_CLASSHIERARCHY(ItemVectorVar)
    };
    typedef std::shared_ptr<ItemVectorVar> ItemVectorVarPtr;

    template M_DLL_EXPORT ItemVectorVarPtr ma::Item::AddVar<ItemVectorVar>(const ma::Var::key_type &key,
                                                                           const ItemVectorVar::value_type &value);
    template M_DLL_EXPORT ItemVectorVarPtr ma::Item::AddVar<ItemVectorVar>(const ma::Var::key_type &key);

    /// Définit une variable pointant sur un observable.
    class M_DLL_EXPORT ObservableVar: public Var
    {
        public:
            explicit ObservableVar(const ma::Var::ConstructorParameters &params);
            ObservableVar() = delete;
            ObservableVar(const ObservableVar &) = delete;
            ~ObservableVar() override;

            virtual void Set(const ObservablePtr &) = 0;
            virtual ObservablePtr Get() const = 0;
            virtual ObservableVar &operator=(const ObservablePtr &) = 0;

            M_HEADER_CLASSHIERARCHY(ObservableVar)
    };
    typedef std::shared_ptr<ObservableVar> ObservableVarPtr;

    /// Permet de faire référence à un item.
    /// La variable est observable.
    /// Cela permet de connaître ses changements de valeurs.
    /// \remarks Pour observer une variable pointant sur un item voici un code servant d'exemple :
    /// \code{.cpp}
    /// root->obs->AddObservation<ma::wxTreeItemsVar, ma::VarVar>(this, root->current);
    /// \endcode
    template<typename T>
    class BaseItemVar: public ma::ObservableVar
    {
        public:
            typedef T value_type;
            typedef std::shared_ptr<T> ptr_type;

        protected:
            ptr_type m_Item;

            /// Clef de l'ancien item pointé
            Item::key_type m_PreviousKey;

            /// Clef de l'item pointé par défaut. Si à la construction d'item est nulle alors la clef sera vide.
            Item::key_type m_Default;

            /// Que ça soit la fonction SetItem, SetFromItem, fromString ou Reset, elles appelleront toutes cette
            /// fonction. template<typename... Args> friend void Item::SetVarFromValue<ma::BaseItemVar<T>,
            /// Args...>(const VarsMap::key_type &key, const Args
            /// &...args);
            friend Item;
            virtual void SetItemFromItem(const ma::BaseItemVar<T>::ptr_type &item);

            void SetFromItem(const std::wstring &) override;

            template<typename T_Var, typename... Args>
            friend void Item::SetVarFromValue(const VarsMap::key_type &key, const Args &...args);
            virtual void SetFromItem(const ma::ObservablePtr &observable);

            ma::VarPtr Copy() const override;

        public:
            BaseItemVar(const ma::Var::ConstructorParameters &params, const ObservablePtr &item);
            explicit BaseItemVar(const ma::Var::ConstructorParameters &params);
            BaseItemVar() = delete;
            BaseItemVar(const BaseItemVar &) = delete;
            ~BaseItemVar() override;

            /// \return Clef de l'item, si l'item est nul alors une chaîne de caractère vide est renvoyée.
            /// \example "00000000-0000-0000-0000-000000000002"
            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;

            /// À partir de la clef de l'item, il retrouve la référence à celui-ci.
            /// Si la clef est vide alors m_Observable sera nulle.
            void fromString(const std::wstring &) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
            /// \remarks Si la chaîne est vide la fonction renvoie vrai.
            bool IsValidString(const std::wstring &) const override;

            void Reset() override;

            void Set(const ObservablePtr &) override;
            ObservablePtr Get() const override;

            /// \brief Permet d'affecter un item à la variable.
            ///
            /// \exception std::invalid_argument si l'item auquel est associé la variable est dans un état différent que
            /// celui passé en paramètre.
            /// Si l'item propriétaire de la variable est actif alors l'item passé en paramètre doit l'être aussi et
            /// inversement.
            /// Pour connaître la valeur
            /// \code {.cpp}
            /// bool item_state = ma::Item::HasItem(item->GetKey());
            /// \endcode
            virtual void SetItem(const ptr_type &);
            virtual const ptr_type &GetItem() const;
            ptr_type GetPrevious() const;
            Item::key_type GetPreviousKey() const;

            virtual BaseItemVar<T> &operator=(const ptr_type &item);
            BaseItemVar<T> &operator=(const ObservablePtr &observable) override;
    };

#define M_HEADER_ITEM_TYPE_VAR(in_M_item_type)                                                                         \
    class M_DLL_EXPORT in_M_item_type##Var: public ma::BaseItemVar<ma::in_M_item_type>                                 \
    {                                                                                                                  \
        protected:                                                                                                     \
            using ma::BaseItemVar<ma::in_M_item_type>::Set;                                                            \
                                                                                                                       \
        public:                                                                                                        \
            using ma::BaseItemVar<ma::in_M_item_type>::BaseItemVar;                                                    \
                                                                                                                       \
            M_HEADER_CLASSHIERARCHY(in_M_item_type##Var)                                                               \
    };                                                                                                                 \
    typedef std::shared_ptr<ma::in_M_item_type##Var> in_M_item_type##VarPtr;                                           \
                                                                                                                       \
    template M_DLL_EXPORT ma::in_M_item_type##VarPtr ma::Item::AddVar<in_M_item_type##Var>(                            \
        const ma::Var::key_type &key, const ma::in_M_item_type##Var::ptr_type &value);                                 \
                                                                                                                       \
    template M_DLL_EXPORT ma::in_M_item_type##VarPtr ma::Item::AddVar<in_M_item_type##Var>(                            \
        const ma::Var::key_type &key);

#define M_HEADER_ITEM_TYPE_VAR_WITH_NAMESPACE(in_M_item_type, in_M_NameSpace)                                          \
    namespace in_M_NameSpace                                                                                           \
    {                                                                                                                  \
        class M_DLL_EXPORT in_M_item_type##Var: public ma::BaseItemVar<in_M_NameSpace::in_M_item_type>                 \
        {                                                                                                              \
            protected:                                                                                                 \
                using ma::BaseItemVar<in_M_NameSpace::in_M_item_type>::Set;                                            \
                                                                                                                       \
            public:                                                                                                    \
                using ma::BaseItemVar<in_M_NameSpace::in_M_item_type>::BaseItemVar;                                    \
                                                                                                                       \
                M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_item_type##Var, in_M_NameSpace)                            \
        };                                                                                                             \
        typedef std::shared_ptr<in_M_item_type##Var> in_M_item_type##VarPtr;                                           \
    }                                                                                                                  \
    template M_DLL_EXPORT in_M_NameSpace::in_M_item_type##VarPtr                                                       \
    ma::Item::AddVar<in_M_NameSpace::in_M_item_type##Var>(const ma::Var::key_type &key,                                \
                                                          const in_M_NameSpace::in_M_item_type##Var::ptr_type &value); \
                                                                                                                       \
    template M_DLL_EXPORT in_M_NameSpace::in_M_item_type##VarPtr                                                       \
    ma::Item::AddVar<in_M_NameSpace::in_M_item_type##Var>(const ma::Var::key_type &key);

#define M_CPP_ITEM_TYPE_VAR(in_M_item_type)                                                                            \
                                                                                                                       \
    const TypeInfo &in_M_item_type##Var::Get##in_M_item_type##VarTypeInfo()                                            \
    {                                                                                                                  \
        static const TypeInfo info = {                                                                                 \
            Get##in_M_item_type##VarClassName(),                                                                       \
            L"Permet de faire référence à un autre item qui ne soit pas enfant de l'item possédant une variable " +    \
                ma::to_wstring(#in_M_item_type) + L".",                                                                \
            {}};                                                                                                       \
                                                                                                                       \
        return info;                                                                                                   \
    }                                                                                                                  \
    M_CPP_CLASSHIERARCHY(in_M_item_type##Var, ObservableVar)

#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_ITEM_TYPE_VAR_WITH_NAMESPACE(in_M_item_type, in_M_NameSpace)                                         \
                                                                                                                       \
        const TypeInfo &in_M_NameSpace::in_M_item_type##Var::Get##in_M_NameSpace##in_M_item_type##VarTypeInfo()        \
        {                                                                                                              \
            static const TypeInfo info = {Get##in_M_NameSpace##in_M_item_type##VarClassName(),                         \
                                          L"Permet de faire référence à un autre item qui ne soit pas enfant de "   \
                                          L"l'item possédant une variable " +                                          \
                                              ma::to_wstring(#in_M_item_type) + L".",                                  \
                                          {}};                                                                         \
                                                                                                                       \
            return info;                                                                                               \
        }                                                                                                              \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_item_type##Var, in_M_NameSpace, ObservableVar)
#else
    #define M_CPP_ITEM_TYPE_VAR_WITH_NAMESPACE(in_M_item_type, in_M_NameSpace)                                         \
        const TypeInfo &in_M_NameSpace::in_M_item_type##Var::Get##in_M_NameSpace####in_M_item_type##VarTypeInfo()      \
        {                                                                                                              \
            static const TypeInfo info = {Get##in_M_NameSpace####in_M_item_type##VarClassName(),                       \
                                          L"Permet de faire référence à un autre item qui ne soit pas enfant de "   \
                                          L"l'item possédant une variable " +                                          \
                                              ma::to_wstring(#in_M_item_type) + L".",                                  \
                                          {}};                                                                         \
                                                                                                                       \
            return info;                                                                                               \
        }                                                                                                              \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_item_type##Var, in_M_NameSpace, ObservableVar)
#endif

    /// Permet de faire référence à un item dans une variable
    M_HEADER_ITEM_TYPE_VAR(Item)

    /// Permet de faire référence à une autre variable pointant sur une variable.
    /// \remarks Le mécanisme des observations a besoin de cet objet pour fonctionner. Les EnumVar utilisent aussi cette
    /// classe.
    class M_DLL_EXPORT VarVar: public ma::ObservableVar
    {
        public:
            typedef ma::Var value_type;
            typedef ma::VarPtr ptr_type;

        protected:
            ma::VarPtr m_Var;

            void SetFromItem(const std::wstring &) override;
            virtual void SetFromItem(const ma::ObservablePtr &observable);
            ma::VarPtr Copy() const override;

        public:
            VarVar(const ma::Var::ConstructorParameters &params, const ma::ObservablePtr &var);
            explicit VarVar(const ma::Var::ConstructorParameters &params);
            VarVar() = delete;
            VarVar(const VarVar &) = delete;
            ~VarVar() override;

            /// \return clef de l'item, suivit de la clef de la variable.
            /// Si la variable est nulle alors une chaîne de caractère vide est renvoyée.
            /// \example "{"00000000-0000-0000-0000-000000000002", "m_VariableName"}"
            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;

            /// À partir de la clef de l'item et de la clef de la variable, elle retrouve la référence à la variable et
            /// l'affecte à m_Var.
            void fromString(const std::wstring &) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement
            /// correct.
            /// \remarks à utiliser avant d'appeler fromString.
            bool IsValidString(const std::wstring &) const override;

            void Reset() override;

            void Set(const ObservablePtr &) override;
            ObservablePtr Get() const override;
            virtual void SetVar(const VarPtr &);
            virtual const ma::VarPtr &GetVar() const;

            virtual VarVar &operator=(const ma::VarPtr &);
            ObservableVar &operator=(const ma::ObservablePtr &) override;

            M_HEADER_CLASSHIERARCHY(VarVar)
    };
    typedef std::shared_ptr<VarVar> VarVarPtr;

    template M_DLL_EXPORT VarVarPtr ma::Item::AddVar<ma::VarVar>(const ma::Var::key_type &key, const ma::VarPtr &value);

    template M_DLL_EXPORT VarVarPtr ma::Item::AddVar<ma::VarVar>(const ma::Var::key_type &key);

    /// Classe abstraite qui permet de référencer une variable de type
    /// ma::ObserverPtr.
    class M_DLL_EXPORT ObserverVar: public Var
    {
        public:
            typedef ma::Observer *value_type;

        protected:
            value_type m_Value;

            /// Pour éviter l'avertissement -Woverloaded-virtual
            /// \see https://stackoverflow.com/questions/9995421/gcc-woverloaded-virtual-warnings
            void SetFromItem(const std::wstring &value) override = 0;
            virtual void SetFromItem(const value_type &value);

        public:
            ObserverVar(const ma::Var::ConstructorParameters &params, const value_type &var);
            explicit ObserverVar(const ma::Var::ConstructorParameters &params);
            ObserverVar() = delete;
            ObserverVar(const ObserverVar &) = delete;
            ~ObserverVar() override;

            /// Permet de savoir si l'observateur est valide ou non.
            /// Si par exemple l'observateur héritait de « wxWindow » et qu'il eu été détruit avant la fin du mécanisme
            /// d'observation, alors on peut grâce à l'id de la fenêtre savoir si celui-ci est valide grâce à sa
            /// fonction wxWindow::FindWindowById. Si par exemple l'observateur héritait de ma::Item, alors la clef de
            /// l'item permettrait grâce à ma::HasItem de savoir si celui-ci est actif.
            virtual bool IsValid() const = 0;

            virtual void Set(const value_type &value);
            const value_type &Get() const;

            virtual ObserverVar &operator=(const value_type &value);

            M_HEADER_CLASSHIERARCHY(ObserverVar)
    };
    typedef std::shared_ptr<ma::ObserverVar> ObserverVarPtr;

#ifdef wxUSE_GUI
    template<typename T>
    class M_DLL_EXPORT wxObserverVar: public ma::ObserverVar
    {
        protected:
            /// Permet de vérifier la validité de la fenêtre et donc de l'observateur.
            wxWindowID m_WindowId;

            void SetFromItem(const std::wstring &str) override;
            ma::VarPtr Copy() const override;

        public:
            wxObserverVar(const ma::Var::ConstructorParameters &params, const value_type &var);
            explicit wxObserverVar(const ma::Var::ConstructorParameters &params);
            wxObserverVar() = delete;
            wxObserverVar(const wxObserverVar &) = delete;
            ~wxObserverVar() override;

            void Set(const value_type &value) override;

            /// Renvoie l'id de type long sous forme de chaîne de caractère.
            std::wstring toString() const override;

            std::wstring GetKeyValueToString() const override;

            /// À partir de l'id la fenêtre de type T sera retrouvée.
            void fromString(const std::wstring &str) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
            /// \remarks à utiliser avant d'appeler fromString.
            bool IsValidString(const std::wstring &str) const override;

            bool IsValid() const override;

            void Reset() override;
    };
#endif // wxUSE_GUI

    /// Permet de référencer dans une variable un item observateur.
    class M_DLL_EXPORT ItemObserverVar: public ma::ObserverVar
    {
        protected:
            /// \exception std::invalid_argument value n'est pas un item.
            /// \exception std::invalid_argument value est un item non actif.
            void SetFromItem(const value_type &value) override;

            /// \exception std::invalid_argument La clef ne représente pas un item convertible en observateur.
            void SetFromItem(const std::wstring &str) override;
            ma::VarPtr Copy() const override;

            ma::ItemPtr m_Item;

        public:
            using ma::ObserverVar::ObserverVar;

            /// Renvoie la clef de l'item
            std::wstring toString() const override;
            std::wstring toStringRepresentation() const override;
            std::wstring GetKeyValueToString() const override;

            /// À partir de la clef de l'item
            void fromString(const std::wstring &str) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
            /// \remarks À utiliser avant d'appeler fromString.
            bool IsValidString(const std::wstring &str) const override;

            bool IsValid() const override;

            void Reset() override;

            M_HEADER_CLASSHIERARCHY(ItemObserverVar)
    };

    /// Permet de référencer dans une variable, une variable observatrice.
    class M_DLL_EXPORT VarObserverVar: public ma::ObserverVar
    {
        protected:
            /// \exception std::invalid_argument value n'est pas une variable.
            /// \exception std::invalid_argument value est une variable d'une
            /// item non actif.
            void SetFromItem(const value_type &value) override;

            /// \exception std::invalid_argument La clef ne représente pas une
            /// variable convertible en observateur.
            void SetFromItem(const std::wstring &str) override;
            ma::VarPtr Copy() const override;

            ma::VarPtr m_Var;

        public:
            using ma::ObserverVar::ObserverVar;

            /// \return clef de l'item, suivit de la clef de la variable.
            /// Si la variable est nulle alors une chaîne de caractère vide est
            /// renvoyée.
            /// \example "{"00000000-0000-0000-0000-000000000002", "m_VariableName"}"
            std::wstring toString() const override;

            std::wstring GetKeyValueToString() const override;

            /// À partir de la clef de l'item et de la variable.
            /// \example "{"00000000-0000-0000-0000-000000000002", "m_VariableName"}"
            void fromString(const std::wstring &str) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
            /// \remarks à utiliser avant d'appeler fromString.
            bool IsValidString(const std::wstring &str) const override;

            bool IsValid() const override;

            void Reset() override;

            M_HEADER_CLASSHIERARCHY(VarObserverVar)
    };
} // namespace ma

#include "ma/Variable.tpp"
