/// \file Serialization.h
/// \author Pontier Pierre
/// \date 2021-16-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <limits>
#include <typeinfo>
#include <regex>
#include <ios> // std::fixed

#include <set>
#include <map>
#include <array>
#include <vector>
#include <deque>
#include <forward_list>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>

#include "ma/Prerequisites.h"
#include "ma/Algorithm.h"
#include "ma/Error.h"
#include "ma/Item.h"
#include "ma/Dll.h"

#include "ma/eigen/Eigen.h"

// Explication sur les std::string_view
// https://dev.to/younup/comprendre-std-stringview-de-c-17-1jep
// https://www.modernescpp.com/index.php/c-17-avoid-copying-with-std-string-view

// S'il y a besoin de faire l'opération pour les tuples:
// https://stackoverflow.com/questions/26902633/how-to-iterate-over-a-stdtuple-in-c-11
// https://coliru.stacked-crooked.com/a/786ec97ec7611629
// https://coliru.stacked-crooked.com/a/ddcae311dc3636cf
// https://coliru.stacked-crooked.com/a/74c3d71816375619
// https://www.developpez.net/forums/d2103527/c-cpp/cpp/langage/to_string-template-conteneur-cpp11-cpp14/
// SFINAE: https://en.cppreference.com/w/cpp/language/sfinae

// https://solarianprogrammer.com/2011/10/12/cpp-11-regex-tutorial/
// https://ideone.com/lyDtfi
// http://cplusplus.com/forum/general/98982/
// https://stackoverflow.com/questions/37989081/how-to-use-unicode-range-in-c-regex

// https://www.regular-expressions.info/floatingpoint.html
// https://regex101.com/
// http://www.suryavarman.fr/string_est_un_reel_ou_pas_avec_regex/
// (groupe1|groupe2) le groupe1 ne peut détecter 1., c'est là qu'intervient le
// groupe2
//
// Créer un compilateur (https://fr.wikipedia.org/wiki/Analyse_syntaxique)
// Un tuto pour écrire un compilateur en Rust.
// https://www.developpez.net/forums/d270226/general-developpement/langages-programmation/creer-langage-programmation/
// Un tuto pour écrire un compilateur de parseurs monadiques ( haskell )
// https://juliendehos.developpez.com/tutoriels/haskell/comment-ecrire-compilateurs-en-haskell/

namespace ma
{
    static const std::wregex g_IntRegType(ma::to_wstring(R"(^[\s]*[+-]?[0-9]+[\s]*$)"));
    static const std::wregex g_UIntRegType(ma::to_wstring(R"(^[\s]*[+]?[0-9]+[\s]*$)"));

    static const std::wregex g_RealRegType(
        ma::to_wstring(R"((^[\s]*[-+]?[0-9]*?\.?[0-9]+([eE][-+]?[0-9]+)?[\s]*$|^[\s]*[-+]?[0-9]+\.?[\s]*$))"));
    static const std::wregex g_URealRegType(
        ma::to_wstring(R"((^[\s]*[+]?[0-9]*?\.?[0-9]+([eE][-+]?[0-9]+)?[\s]*$|^[\s]*[-+]?[0-9]+\.?[\s]*$))"));

    /// \todo Voir si c'est encore utile maintenant qu'il y a l'analyseur
    /// syntaxique.
    static const std::wstring g_BeginKeyWord = L"{";
    static const std::wstring g_EndKeyWord = L"}";
    static const std::wstring g_SeparatorKeyWord = L",";

    inline std::wstring Quoted(const std::wstring& in_str)
    {
        return LR"#(")#" + in_str + LR"#(")#";
    }

    namespace Serialization
    {
        struct Node;
        typedef std::shared_ptr<ma::Serialization::Node> NodePtr;
        typedef std::vector<std::pair<size_t, std::wstring>> Errors;

        /// Les nœuds sont générés lors de l'analyse syntaxique. Ils décrivent la composition d'une chaîne de caractère.
        struct M_DLL_EXPORT Node
        {
                enum Type
                {
                    Struct, ///< Le nœud représente une structure.
                    Value, ///< Le nœud représente une valeur.
                    Invalid ///< Le nœud est incorrect.
                };
                /// Le type du nœud.
                ma::Serialization::Node::Type m_Type;

                ///  L'index de m_String dans la chaîne de caractères d'origine.
                /// \example
                /// - «{}» ─► 0
                /// - « {}» ─► 0
                /// - «{1, {}}» ─► 0 pour la 1ère structure, 3 pour la 2ᵉ
                ///    0123456
                /// - «{""}» ─► 2 pour la valeur entre guillemets. m_String sera
                ///    0123     vide.
                /// - «{,}» ─► 1 pour la première valeur et 2 pour la deuxième,
                ///    012     Les nœuds des deux valeurs auront une chaîne de
                ///            caractère vide (m_String).
                const size_t m_Index;

                /// La chaîne de texte représentant le nœud. La vue pointe sur
                /// m_String du parser associé.
                std::wstring_view m_String;

                typedef std::vector<NodePtr> Nodes;
                /// Les enfants du nœud.
                Nodes m_Nodes;

                Node() = delete;

                /// Constructeur de nœud.
                /// \param index L'index (m_Index) où commence le premier caractère du nœud dans la chaîne de caractère
                /// d'origine, c'est-à-dire celle de l'analyseur syntaxique (Parser).
                /// \see ma::Serialization::Node::m_Index
                explicit Node(size_t index);

                /// Constructeur de nœud.
                /// \param type Le type (m_Type) du nœud valeur, structure ou
                /// invalide.
                /// \param index L'index (m_Index) où commence le premier caractère du nœud dans la chaîne de caractère
                /// d'origine, c'est-à-dire celle de l'analyseur syntaxique (Parser).
                /// \param str La chaîne de caractères (m_String) représentant le nœud.
                /// \see ma::Serialization::Node::m_Type
                /// \see ma::Serialization::Node::m_String
                /// \see ma::Serialization::Node::m_Index
                explicit Node(ma::Serialization::Node::Type type, size_t index, const std::wstring_view &str);

                /// Constructeur de nœud.
                /// \param type Le type (m_Type) du nœud valeur, structure ou invalide.
                /// \param index L'index (m_Index) où commence le premier caractère du nœud dans la chaîne de caractère
                /// d'origine, c'est-à-dire celle de l'analyseur syntaxique (Parser).
                /// \param str La chaîne de caractères (m_String) représentant le nœud.
                /// \param children Les enfants du nœud (m_Nodes).
                /// \see ma::Serialization::Node::m_Type
                /// \see ma::Serialization::Node::m_String
                /// \see ma::Serialization::Node::m_Index
                /// \see ma::Serialization::Node::m_Nodes
                explicit Node(ma::Serialization::Node::Type type,
                              size_t index,
                              const std::wstring_view &str,
                              const std::vector<ma::Serialization::NodePtr> &children);

                /// Permet de savoir si la syntaxe du nœud et si ses caractéristiques sont valides.
                /// \return Vrais si le nœud est valide, sinon faux.
                bool IsValid() const;

                /// Permet de redimensionner facilement m_String lorsque l'index de fin est trouvé.
                /// \example
                /// {1} L'index de fin de « 1 » est 1
                /// 012
                /// { 111 } L'index de fin de « 111 » est 5
                /// 0123456
                void SetEndIndex(size_t index);
        };

        /// C'est une fonction qui retourne la représentation textuelle de type.
        /// \remarks Elle facilite l'écriture du code pour afficher le type d'un
        /// nœud.
        std::wstring GetTypeStr(ma::Serialization::Node::Type type);

        /// Analyseur syntaxique.
        /// \example
        /// \code {.cpp}
        /// auto value_1 = ma::Serialization::Parser{"2"}.GetValue<int>();
        /// \endcode
        /// \code {.cpp}
        /// ma::Serialization::Parser parser{"{2.5, 3.1, 8.9}"};
        /// auto value_1 = parser.GetValue<std::set<float>>();
        /// auto value_2 = parser.GetValue<std::vector<float>>();
        /// \endcode
        class M_DLL_EXPORT Parser
        {
            protected:
                /// Nœud principale.
                /// Racine de l'arbre décrivant l'analyse syntaxique.
                NodePtr m_Root;

                /// La chaîne de caractères qui sera analyser.
                const std::wstring m_String;

                /// Empile et dépile les structures lors de l'analyse syntaxique.
                /// \see BeginStruct
                /// \see EndStruct
                /// \remarks À la fin de l'analyse syntaxique la pile doit être vide.
                std::stack<NodePtr> m_StructStack;

                /// Permet de détecter s'il y a une erreur dans le nombre d'accolades.
                /// À chaque fois que l'on rencontre un début de structure, nous incrémentons m_StructIncrement
                int m_StructIncrement;

                /// Permet de détecter s'il y a une erreur dans le nombre d'accolades.
                /// À chaque fois que l'on rencontre une fin de structure, nous décrémentons m_StructDecrement.
                int m_StructDecrement;

            public:
                /// La chaîne de caractères qui sera analyser.
                /// Pointe sur m_String
                const std::wstring_view m_Str;

                /// À la construction de l'objet la chaîne sera analyser et sa description sera stockée dans m_Root.
                Parser(const std::wstring &str);
                virtual ~Parser();

                /// \return Le nœud racine de l'analyse syntaxique.
                virtual NodePtr GetRoot() const;

                /// \return Vrais si une erreur a été détectée lors de l'analyse
                /// syntaxique.
                bool HasError() const;

                /// Parcourt l'arbre m_Root pour construire une instance de T et renvoyer une copie.
                /// \remarks Équivalent à :
                /// \code {.cpp}
                /// T value = ma::Serialization::FromNode<T>(parser.GetRoot());
                /// \endcode
                template<typename T>
                T GetValue() const;

                /// Parcours l'arbre m_Root pour vérifier la compatibilité des nœuds avec le type demandé.
                template<typename T>
                bool IsValid() const;

                /// Parcours l'arbre m_Root pour récupérer les erreurs de compatibilité des nœuds avec le type demandé.
                template<typename T>
                Errors GetErrors() const;

                /// Utiliser par règles syntaxiques.
                void BeginStruct(const NodePtr &node);

                /// Utiliser par règles syntaxiques.
                /// \param str_end_index Index de l'accolade fermente.
                NodePtr EndStruct(size_t str_end_index);

                /// \return La structure courante. S'il n'y a plus de structure dans la pile, la racine sera renvoyée.
                /// \remarks Permet de retrouver la structure courante pour les éléments de celle-ci et suivant le
                /// premier élément de celle-ci.
                NodePtr GetCurrentStruct();
        };

        class M_DLL_EXPORT Rule
        {
            public:
                Parser *m_Parser;

                Rule(Parser *parser);
                virtual ~Rule();

                /// \param node Nœud représentant la structure ou la valeur qui
                /// sera anayliser.
                virtual void Parse(NodePtr &node) = 0;

                /// Test la validité du nœud.
                /// Si une erreur est trouvée celui-ci sera invalidé.
                /// La valeur « faux » sera donc retournée, sinon la valeur « vrai » est retournée.
                /// \param node Le nœud est à vérifier.
                /// \return Vrais si les paramètres du nœud semble correct, sinon Faux.
                bool CheckNode(NodePtr &node);
        };

        /// Est appelé lorsque la fin de la structure ou de la valeur est détectée.
        /// Autrement dit cela déclenche la fin de l'analyse syntaxique de la chaîne de caractère.
        /// Id : c
        /// Nom : Fin
        ///
        ///     c
        ///   ┌───┐
        ///  ─┤FIN│
        ///   └───┘
        ///
        struct M_DLL_EXPORT End: public Rule
        {
                End(Parser *parser);
                virtual ~End();

                virtual void Parse(NodePtr &node) override;
        };

        /// C'est le point d'entrée pour parser une chaîne de caractères.
        /// Id: a
        /// Nom : Début
        /// Sorties:
        /// '{'  : b (on incrémente le nombre de structures du « parser » S + 1)
        /// value: c (C'est une valeur)
        ///
        ///     a
        ///   ┌───┐S+1
        ///   │ { ├──►b
        ///  ─┤   │Valeur (nombre ou chaîne de caractères)
        ///   │ V ├──►c
        ///   └───┘
        ///
        struct M_DLL_EXPORT Begin: public Rule
        {
                Begin(Parser *parser);
                virtual ~Begin();

                virtual void Parse(NodePtr &root) override;

                /// Pour initialiser le root correctement, il faut pouvoir définir l'index de départ.
                static size_t GetIndex(const std::wstring &str);
        };

        /// Début d'une structure.
        /// Id: b
        ///
        ///    b
        ///  ┌───┐S+1
        ///  │ { ├──►b
        ///  │   │chaîne de caractères
        ///  │ " ├──►d
        /// ─┤   │nombre
        ///  │ N ├──►e
        ///  │   │S-1
        ///  │ } ├──►f
        ///  └───┘
        struct M_DLL_EXPORT StructBegin: public Rule
        {
                StructBegin(Parser *parser);
                virtual ~StructBegin();

                /// \param struct_node Nœud représentant la structure.
                virtual void Parse(NodePtr &struct_node) override;
        };

        /// Gestion de la fin d'une structure.
        /// Id: f
        ///
        ///   f
        ///  ┌───┐
        ///  │ , ├──►k
        ///  │   │
        /// ─┤Fin│
        ///  │   │s-1
        ///  │ } ├──►f
        ///  └───┘
        class M_DLL_EXPORT StructEnd: public Rule
        {
            public:
                StructEnd(Parser *parser);
                virtual ~StructEnd();

                /// \param struct_node Nœud représentant la structure.
                virtual void Parse(NodePtr &struct_node, size_t in_next_index);

            private:
                virtual void Parse(NodePtr &struct_node) override;
        };

        /// Gestion de l'élément suivant à l'intérieur d'une structure.
        /// Id: k
        ///
        ///    k
        ///  ┌───┐S+1
        ///  │ { ├──►b
        ///  │   │
        /// ─┤ " ├──►d
        ///  │   │
        ///  │ N ├──►e
        ///  └───┘
        ///
        class M_DLL_EXPORT StructNext: public Rule
        {
            public:
                StructNext(Parser *parser);
                virtual ~StructNext();

                /// \param struct_node Nœud représentant la structure.
                /// \param in_next_index l'index de début du prochain élément de
                /// la structure.
                /// \example
                virtual void Parse(NodePtr &struct_node, size_t in_next_index);

            private:
                virtual void Parse(NodePtr &struct_node) override;
        };

        /// Gestion de la fin d'un nombre à l'intérieur d'une structure.
        /// Id: e
        ///
        ///    e
        ///  ┌───┐S-1
        ///  │ } ├──►f
        /// ─┤   │
        ///  │ , ├──►k
        ///  └───┘
        ///
        struct M_DLL_EXPORT NumberNext: public Rule
        {
                NumberNext(Parser *parser);
                virtual ~NumberNext();

                /// \param number_node Nœud du nombre.
                virtual void Parse(NodePtr &number_node) override;
        };

        /// Gestion de la fin d'une chaîne de caractère à l'intérieur d'une structure.
        /// Id: d
        ///
        ///     d
        ///  ┌─────┐
        ///  │ " , ├──►k
        /// ─┤     │S-1
        ///  │ " } ├──►b
        ///  └─────┘
        ///
        struct M_DLL_EXPORT StringNext: public Rule
        {
                StringNext(Parser *parser);
                virtual ~StringNext();

                virtual void Parse(NodePtr &string_node) override;
        };
    } // namespace Serialization

    /// Le type de retour, est « std::wstring » mais ça dépend des conditions.
    /// Ici, nous imposons un paramètre qui doit être d'un type existant
    template<typename... Ts>
    struct string_
    {
            using type = std::wstring;
    };

    /// Notre type de retour, c'est bool mais ça dépend des conditions.
    /// Ici, nous imposons un paramètre qui doit être un type existant.
    template<typename... Ts>
    struct bool_
    {
            using type = bool;
    };

    /// Notre type de retour, c'est « ma::Serialization::NodePtr » mais ça dépend des conditions.
    /// Ici, nous imposons un paramètre qui doit être un type existant.
    template<typename... Ts>
    struct nodeptr_
    {
            using type = ma::Serialization::NodePtr;
    };

    /// Permet de savoir si T a besoin d'être entouré par des guillemets lorsque celui-ci est à l'intérieur d'une
    /// structure.
    /// \example
    /// « {"toto"} » toto est dans une structure, alors il est entouré par des guillemets.
    /// « toto » n'est pas dans une structure, alors il n'est pas entouré par des guillemets.
    /// \remarks par défaut renvoie faux.
    template<typename T>
    bool IsRepresentedByString();

// Type simple //———————————————————————————————————————————————————————————————————————————————————————————————————————
/// \remarks L'implémentation par défaut est pour les réels.
/// On doit pouvoir faire ss << T.
/// \todo Optimiser la suppression des zéros et l'ajout de zéros.
/// \see https://www.developpez.net/forums/d2103527/c-cpp/cpp/langage/to_string-template-conteneur-cpp11-cpp14/
/// \see https://en.cppreference.com/w/cpp/types/climits/FLT_EVAL_METHOD
/// \see https://en.cppreference.com/w/cpp/types/climits
/// \see https://stackoverflow.com/questions/55064191/rounding-a-float-number-to-a-certain-precision
#define MA_SFINAE_NUMBER(in_M_type) typename ma::in_M_type<decltype(std::to_wstring(std::declval<T_NUMBER>()))>::type

#define MA_SFINAE_STRING_NUMBER MA_SFINAE_NUMBER(string_)

#define MA_SFINAE_BOOL_NUMBER MA_SFINAE_NUMBER(bool_)

#define MA_SFINAE_NODE_NUMBER MA_SFINAE_NUMBER(nodeptr_)

    template<typename T_NUMBER>
    MA_SFINAE_STRING_NUMBER toString(T_NUMBER value)
    {
        // https://en.cppreference.com/w/cpp/io/manip/fixed
        // https://stackoverflow.com/questions/46089298/c-float-to-string-with-specific-length
        // https://coliru.stacked-crooked.com/a/b9d65beec231fab7
        // http://cpp.sh/2ecki
        // http://cpp.sh/3d7b5

        std::wstringstream ss;
        const auto precision = std::numeric_limits<T_NUMBER>::digits10 + 1;
        ss.precision(precision);
        ss << std::left << value;
        std::wstring value_str = ss.str();

        // Gestion des nombres avec des puissances ou des virgules se terminant par zéro.
        if(value_str.find('.') != std::string::npos)
        {
            const auto e_pos = value_str.find_last_of('e');

            if(e_pos != std::string::npos || value_str.back() == '0')
            {
                std::wstringstream ss2;
                ss2.precision(precision);
                ss2 << std::fixed;
                ss2 << value;

                value_str = ss2.str();

                while(value_str.find('.') != std::string::npos && value_str.back() == '0')
                    value_str.pop_back();

                if(value_str.back() == '.')
                    value_str.pop_back();
            }
        }

        if(value_str.size() > 1u && value_str.front() == '+')
            value_str.erase(0);

        return value_str;
    }

    /// Analyse une chaîne de caractère pour construire une instance de type T.
    /// \remarks Utilise FromNode qui fonctionne pour tout type de T.
    /// Elle est définie pour chaque groupe de type, car les items ont besoin d'une implémentation spécifique.
    /// Clang n'arrive pas à choisir entre définie par défaut et les items.
    template<typename T_NUMBER>
    T_NUMBER FromString(const MA_SFINAE_STRING_NUMBER &str);

    /// Analyse une chaîne de caractère pour connaître si elle permet de créer
    /// une instance de type T.
    /// \remarks Utilise IsValidNode.
    /// Fonctionne pour tout type de T.
    /// Elle est définie pour chaque groupe de type, car les items ont besoin d'une implémentation spécifique.
    /// Clang n'arrive pas à choisir entre définie par défaut et les wstring.
    template<typename T_NUMBER>
    bool IsValidString(const MA_SFINAE_STRING_NUMBER &str);

    /// Analyse une chaîne de caractères pour connaître les erreurs potentielles qui l'empêcherait de représenter une
    /// instance de type T.
    /// \remarks Utilise ma::GetErrors<T>(Node root). Fonctionne pour tout type de T.
    template<typename T_NUMBER>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_NUMBER &str);

    /// Permet de savoir si T a besoin d'être entouré par des guillemets, lorsque celui-ci est à l'intérieur d'une
    /// structure.
    /// \example
    /// « {"toto"} » → toto est dans une structure, alors il est entouré par des guillemets.
    /// « toto » → toto n'est pas dans une structure, alors il n'est pas entouré par des guillemets.
    /// \remarks Par défaut renvoie faux.
    //    template <typename T_NUMBER>
    //    MA_SFINAE_BOOL_NUMBER IsRepresentedByString();

    /// \remarks L'implémentation par défaut est pour les entiers.
    template<typename T_NUMBER>
    T_NUMBER FromNode(const MA_SFINAE_NODE_NUMBER &node);

    /// \remarks L'implémentation par défaut est pour les entiers.
    template<typename T_NUMBER>
    bool IsValidNode(const MA_SFINAE_NODE_NUMBER &node);

    /// \remarks L'implémentation par défaut est pour les entiers.
    template<typename T_NUMBER>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_NUMBER &node);

// Type std::pair //————————————————————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_PAIR(in_M_type)                                                                                      \
    typename ma::in_M_type<typename T_Pair::first_type, typename T_Pair::second_type>::type

#define MA_SFINAE_STRING_PAIR MA_SFINAE_PAIR(string_)

#define MA_SFINAE_BOOL_PAIR MA_SFINAE_PAIR(bool_)

#define MA_SFINAE_NODE_PAIR MA_SFINAE_PAIR(nodeptr_)

#define MA_SFINAE_NODE_PAIR2 typename ma::nodeptr_<typename std::pair<T1, T2>>::type

    template<typename T1, typename T2>
    std::wstring toString(const std::pair<T1, T2> &value);

    template<typename T1, typename T2>
    std::pair<T1, T2> FromString(const std::wstring &str);

    template<typename T_Pair>
    T_Pair FromString(const MA_SFINAE_STRING_PAIR &str);

    template<typename T_Pair>
    bool IsValidString(const MA_SFINAE_STRING_PAIR &str);

    template<typename T_Pair>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_PAIR &str);

    //    template <typename T_Pair>
    //    MA_SFINAE_BOOL_PAIR IsRepresentedByString();

    template<typename T_Pair>
    T_Pair FromNode(const MA_SFINAE_NODE_PAIR &node);

    template<typename T_Pair>
    bool IsValidNode(const MA_SFINAE_NODE_PAIR &node);

    template<typename T1, typename T2>
    bool IsValidNode(const MA_SFINAE_NODE_PAIR2 &node);

    template<typename T_Pair>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_PAIR &node);

// Type insert container //—————————————————————————————————————————————————————————————————————————————————————————————
/// insert pour le différencier des queues et des piles.
/// clear pour le différencier
#define MA_SFINAE_CONTAINER(in_M_type)                                                                                 \
    typename ma::in_M_type<typename T_Container::const_iterator,                                                       \
                           decltype(std::declval<T_Container>().insert(                                                \
                               std::declval<T_Container>().end(), std::declval<typename T_Container::value_type>())),  \
                           decltype(std::declval<T_Container>().clear()),                                              \
                           std::enable_if_t<!std::is_base_of<std::basic_string<typename T_Container::value_type>,      \
                                                             T_Container>::value>>::type

#define MA_SFINAE_STRING_CONTAINER MA_SFINAE_CONTAINER(string_)

#define MA_SFINAE_BOOL_CONTAINER MA_SFINAE_CONTAINER(bool_)

#define MA_SFINAE_NODE_CONTAINER MA_SFINAE_CONTAINER(nodeptr_)

    template<typename T_Container>
    MA_SFINAE_STRING_CONTAINER toString(const T_Container &value);

    template<typename T_Container>
    T_Container FromString(const MA_SFINAE_STRING_CONTAINER &str);

    template<typename T_Container>
    bool IsValidString(const MA_SFINAE_STRING_CONTAINER &str);

    template<typename T_Container>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_CONTAINER &str);

    //    template <typename T_Container>
    //    MA_SFINAE_BOOL_CONTAINER IsRepresentedByString();

    template<typename T_Container>
    T_Container FromNode(const MA_SFINAE_NODE_CONTAINER &node);

    template<class T_Container>
    bool IsValidNode(const MA_SFINAE_NODE_CONTAINER &node);

    template<class T_Container>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_CONTAINER &node);

    // Type push container //———————————————————————————————————————————————————————————————————————————————————————————
    // Conteneurs qui poussent ses éléments pour en ajouter un.
    // std::stack
    // std::queue
    // std::priority_queue <-- attention ce conteneur n'a pas d'opérateur
    //                         permettant de comparer deux priority_queue

    template<class T, class Container = std::vector<T>, class Compare = std::less<typename Container::value_type>>
    class priority_queue: public std::priority_queue<T, Container, Compare>
    {
        public:
            using std::priority_queue<T, Container, Compare>::priority_queue;

            bool operator!=(const priority_queue &value) const
            {
                return this->c != value.c;
            }
    };

#define MA_SFINAE_PUSH_CONTAINER(in_M_type)                                                                            \
    typename ma::in_M_type<decltype(std::declval<T_Container>().push(                                                  \
        std::declval<typename T_Container::value_type>()))>::type

#define MA_SFINAE_STRING_PUSH_CONTAINER MA_SFINAE_PUSH_CONTAINER(string_)

#define MA_SFINAE_BOOL_PUSH_CONTAINER MA_SFINAE_PUSH_CONTAINER(bool_)

#define MA_SFINAE_NODE_PUSH_CONTAINER MA_SFINAE_PUSH_CONTAINER(nodeptr_)

    template<typename T_Container>
    MA_SFINAE_STRING_PUSH_CONTAINER toString(const T_Container &value);

    template<typename T_Container>
    T_Container FromString(const MA_SFINAE_STRING_PUSH_CONTAINER &str);

    template<typename T_Container>
    bool IsValidString(const MA_SFINAE_STRING_PUSH_CONTAINER &str);

    template<typename T_Container>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_PUSH_CONTAINER &str);

    //    template <typename T_Container>
    //    MA_SFINAE_BOOL_PUSH_CONTAINER IsRepresentedByString();

    template<typename T_Container>
    T_Container FromNode(const MA_SFINAE_NODE_PUSH_CONTAINER &node);

    template<class T_Container>
    bool IsValidNode(const MA_SFINAE_NODE_PUSH_CONTAINER &node);

    template<class T_Container>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_PUSH_CONTAINER &node);

    /// N'est utilisé que pour permettre d'itérer sur un conteneur qui pousse ses éléments
    /// (std::queue, std::stack, std::priority_queue).
    /// Est utiliser dans la fonction MA_SFINAE_STRING_PUSH_CONTAINER toString
    /// \see https://stackoverflow.com/questions/1259099/stdqueue-iteration
    template<typename T_Container>
    class iterable_push_container: public T_Container
    {
        public:
            typedef typename T_Container::container_type::iterator iterator;
            typedef typename T_Container::container_type::const_iterator const_iterator;

            iterable_push_container(const T_Container &container): T_Container(container)
            {}

            iterator begin()
            {
                return this->c.begin();
            }
            iterator end()
            {
                return this->c.end();
            }
            const_iterator begin() const
            {
                return this->c.begin();
            }
            const_iterator end() const
            {
                return this->c.end();
            }
    };

// Type std::array //———————————————————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_ARRAY(in_M_type)                                                                                     \
    typename ma::in_M_type<typename T_Array::value_type,                                                               \
                           typename T_Array::size_type,                                                                \
                           typename T_Array::const_iterator,                                                           \
                           decltype(std::declval<T_Array>().fill(std::declval<typename T_Array::value_type>()))>::type

#define MA_SFINAE_STRING_ARRAY MA_SFINAE_ARRAY(string_)

#define MA_SFINAE_BOOL_ARRAY MA_SFINAE_ARRAY(bool_)

#define MA_SFINAE_NODE_ARRAY MA_SFINAE_ARRAY(nodeptr_)

    template<typename T_Array>
    MA_SFINAE_STRING_ARRAY toString(const T_Array &value);

    template<typename T_Array>
    T_Array FromString(const MA_SFINAE_STRING_ARRAY &str);

    template<typename T_Array>
    bool IsValidString(const MA_SFINAE_STRING_ARRAY &str);

    template<typename T_Array>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_ARRAY &str);

    //    template <typename T_Array>
    //    MA_SFINAE_BOOL_ARRAY IsRepresentedByString();

    template<class T_Array>
    T_Array FromNode(const MA_SFINAE_NODE_ARRAY &node);

    template<class T_Array>
    bool IsValidNode(const MA_SFINAE_NODE_ARRAY &node);

    template<class T_Array>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_ARRAY &node);

    // Type Eigen::Matrix //————————————————————————————————————————————————————————————————————————————————————————————

#define MA_SFINAE_EIGEN_MATRIX(in_M_type) typename ma::in_M_type<typename T_EigenMatrix::Base::PlainObject>::type

#define MA_SFINAE_STRING_EIGEN_MATRIX MA_SFINAE_EIGEN_MATRIX(string_)

#define MA_SFINAE_BOOL_EIGEN_MATRIX MA_SFINAE_EIGEN_MATRIX(bool_)

#define MA_SFINAE_NODE_EIGEN_MATRIX MA_SFINAE_EIGEN_MATRIX(nodeptr_)

    template<
        class T_EigenMatrix,
        std::enable_if_t<T_EigenMatrix::RowsAtCompileTime != 1 && T_EigenMatrix::ColsAtCompileTime != 1, bool> = true>
    MA_SFINAE_STRING_EIGEN_MATRIX toString(const T_EigenMatrix &value);

    template<
        typename T_EigenMatrix,
        std::enable_if_t<T_EigenMatrix::RowsAtCompileTime == 1 || T_EigenMatrix::ColsAtCompileTime == 1, bool> = true>
    MA_SFINAE_STRING_EIGEN_MATRIX toString(const T_EigenMatrix &value);

    template<typename T_EigenMatrix>
    T_EigenMatrix FromString(const MA_SFINAE_STRING_EIGEN_MATRIX &str);

    template<typename T_EigenMatrix>
    bool IsValidString(const MA_SFINAE_STRING_EIGEN_MATRIX &str);

    template<typename T_EigenMatrix>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_EIGEN_MATRIX &str);

    //    template <typename T_EigenMatrix>
    //    MA_SFINAE_BOOL_EIGEN_MATRIX IsRepresentedByString();

    template<class T_EigenMatrix>
    T_EigenMatrix FromNode(const MA_SFINAE_NODE_EIGEN_MATRIX &node);

    template<class T_EigenMatrix>
    bool IsValidNode(const MA_SFINAE_NODE_EIGEN_MATRIX &node);

    template<class T_EigenMatrix>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_EIGEN_MATRIX &node);

// Type Eigen::Quaternion //————————————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_EIGEN_QUATERNION(in_M_type)                                                                          \
    typename ma::in_M_type<typename T_EigenQuaternion::RotationMatrixType>::type

#define MA_SFINAE_STRING_EIGEN_QUATERNION MA_SFINAE_EIGEN_QUATERNION(string_)

#define MA_SFINAE_BOOL_EIGEN_QUATERNION MA_SFINAE_EIGEN_QUATERNION(bool_)

#define MA_SFINAE_NODE_EIGEN_QUATERNION MA_SFINAE_EIGEN_QUATERNION(nodeptr_)

    template<typename T_EigenQuaternion>
    MA_SFINAE_STRING_EIGEN_QUATERNION toString(const T_EigenQuaternion &value);

    template<typename T_EigenQuaternion>
    T_EigenQuaternion FromString(const MA_SFINAE_STRING_EIGEN_QUATERNION &str);

    template<typename T_EigenQuaternion>
    bool IsValidString(const MA_SFINAE_STRING_EIGEN_QUATERNION &str);

    template<typename T_EigenQuaternion>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_EIGEN_QUATERNION &str);

    //    template <typename T_EigenQuaternion>
    //    MA_SFINAE_BOOL_EIGEN_QUATERNION IsRepresentedByString();

    template<class T_EigenQuaternion>
    T_EigenQuaternion FromNode(const MA_SFINAE_NODE_EIGEN_QUATERNION &node);

    template<class T_EigenQuaternion>
    bool IsValidNode(const MA_SFINAE_NODE_EIGEN_QUATERNION &node);

    template<class T_EigenQuaternion>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_EIGEN_QUATERNION &node);

// Type ::Eigen::Transform //———————————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_EIGEN_TRANSFORM(in_M_type)                                                                           \
    typename ma::in_M_type<typename T_EigenTransform::TransformTimeDiagonalReturnType>::type

#define MA_SFINAE_STRING_EIGEN_TRANSFORM MA_SFINAE_EIGEN_TRANSFORM(string_)

#define MA_SFINAE_BOOL_EIGEN_TRANSFORM MA_SFINAE_EIGEN_TRANSFORM(bool_)

#define MA_SFINAE_NODE_EIGEN_TRANSFORM MA_SFINAE_EIGEN_TRANSFORM(nodeptr_)

    template<typename T_EigenTransform>
    MA_SFINAE_STRING_EIGEN_TRANSFORM toString(const T_EigenTransform &value);

    template<typename T_EigenTransform>
    T_EigenTransform FromString(const MA_SFINAE_STRING_EIGEN_TRANSFORM &str);

    template<typename T_EigenTransform>
    bool IsValidString(const MA_SFINAE_STRING_EIGEN_TRANSFORM &str);

    template<typename T_EigenTransform>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_EIGEN_TRANSFORM &str);

    //    template <typename T_EigenTransform>
    //    MA_SFINAE_BOOL_EIGEN_TRANSFORM IsRepresentedByString();

    template<class T_EigenTransform>
    T_EigenTransform FromNode(const MA_SFINAE_NODE_EIGEN_TRANSFORM &node);

    template<class T_EigenTransform>
    bool IsValidNode(const MA_SFINAE_NODE_EIGEN_TRANSFORM &node);

    template<class T_EigenTransform>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_EIGEN_TRANSFORM &node);

    // Type std::tuple //———————————————————————————————————————————————————————————————————————————————————————————————
    // https://stackoverflow.com/questions/1198260/how-can-you-iterate-over-the-elements-of-an-stdtuple
    // tuple: https://en.cppreference.com/w/cpp/utility/tuple
    // apply: https://en.cppreference.com/w/cpp/utility/apply
    // expression repliée (fold expression): https://en.cppreference.com/w/cpp/language/fold
    // tuple_size_v (ued at compile time): https://en.cppreference.com/w/cpp/utility/tuple/tuple_size
    // tuple_size_v ne suffit pas à reconnaître un tuple. Cela peut être un array, une paire ou si nous passons à c++20
    // un std::ranges::subrange.
    // std::get permet d'identifier un std::variant, std::array, std::pair, et std::ranges::subrange.
    // std::tuple_element: https://en.cppreference.com/w/cpp/utility/tuple/tuple_element
    // pour changer une valeur d'un tuple :
    // https://stackoverflow.com/questions/7453600/how-to-set-stdtuple-element-by-index
    // Pour aller plus loin:
    // https://stackoverflow.com/questions/41708491/making-stdget-play-nice-with-sfinae
    namespace Tuple
    {
        /// Est utilisé pour définir la SFINAE des tuples.
        template<typename T_Tuple, std::size_t... I>
        constexpr auto t2t_impl(const T_Tuple &in_tuple, std::index_sequence<I...>)
        {
            return std::make_tuple(std::get<I>(in_tuple)...);
        }

        /// Est utilisé pour définir la SFINAE des tuples.
        /// L'objectif est de créer un std::tuple avec les éléments d'un
        /// T_Tuple.
        template<typename T_Tuple>
        constexpr auto t2t(const T_Tuple &in_tuple)
        {
            auto index_sequence = std::make_index_sequence<std::tuple_size_v<T_Tuple>>();
            return ma::Tuple::t2t_impl(in_tuple, index_sequence);
        }

        /// Permet d'utiliser « std::apply » et une expression pliée... ne peut fonctionner que si « ma::toString » est
        /// défini pour les tuples.
        template<typename... Ts>
        std::wstring toString(std::tuple<Ts...> const &value);

        template<typename T_Tuple, std::size_t index>
        void FromNode(T_Tuple &in_tuple,
                      const ma::Serialization::Node::Nodes::const_iterator &begin,
                      const ma::Serialization::Node::Nodes::const_iterator &end);

        template<typename T_Tuple, std::size_t index>
        bool IsValidNode(const ma::Serialization::Node::Nodes::const_iterator &begin,
                         const ma::Serialization::Node::Nodes::const_iterator &end);

        template<typename T_Tuple, std::size_t index>
        ma::Serialization::Errors GetErrors(const ma::Serialization::Node::Nodes::const_iterator &begin,
                                            const ma::Serialization::Node::Nodes::const_iterator &end);
    } // namespace Tuple

/// Le code typename « std::tuple_size<T_Tuple>::value_type » accepte seulement les structures de type tuple compatibles
/// comme les « std::array ».
/// Ce qui permet d'utiliser t2t sans les erreurs de ce genre :
/// erreur: type « std::tuple_size<bool> » incomplet utilisé dans un spécificateur de noms imbriqué.
/// Le code std::enable_if_t<std::is_base_of<decltype(t2t(std::declval<T_Tuple>())), T_Tuple>::value> compose un tuple à
/// partir des types des éléments que compose T_Tuple.
/// Si le tuple composé est du même type ou que T_Tuple en hérite, alors la condition est vrais.
#define MA_SFINAE_TUPLE(in_M_type)                                                                                     \
    typename ma::in_M_type<                                                                                            \
        typename std::tuple_size<T_Tuple>::value_type,                                                                 \
        std::enable_if_t<std::is_base_of<decltype(ma::Tuple::t2t(std::declval<T_Tuple>())), T_Tuple>::value>>::type

#define MA_SFINAE_STRING_TUPLE MA_SFINAE_TUPLE(string_)

#define MA_SFINAE_BOOL_TUPLE MA_SFINAE_TUPLE(bool_)

#define MA_SFINAE_NODE_TUPLE MA_SFINAE_TUPLE(nodeptr_)

    template<class... Ts>
    std::wstring toString(const std::tuple<Ts...> &value);

    template<class T_Tuple>
    T_Tuple FromString(const MA_SFINAE_STRING_TUPLE &str);

    template<typename T_Tuple>
    bool IsValidString(const MA_SFINAE_STRING_TUPLE &str);

    template<typename T_Tuple>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_TUPLE &str);

    // template <typename T_Tuple>
    // MA_SFINAE_BOOL_TUPLE IsRepresentedByString();

    template<class T_Tuple>
    T_Tuple FromNode(const MA_SFINAE_NODE_TUPLE &node);

    template<class T_Tuple>
    bool IsValidNode(const MA_SFINAE_NODE_TUPLE &node);

    template<class T_Tuple>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_TUPLE &node);

// Type std::wstring //—————————————————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_TEMPLATE_STRING(in_M_type)                                                                           \
    typename ma::in_M_type<                                                                                            \
        std::enable_if_t<std::is_base_of<std::basic_string<typename T_String::value_type>, T_String>::value>>::type

    //    #define MA_SFINAE_TEMPLATE_STRING_REF(in_M_type)
    //        typename ma::in_M_type<std::enable_if_t<std::is_base_of<std::basic_string<typename T_String::value_type>,
    //        T_String&>::value>>::type

#define MA_SFINAE_TEMPLATE_STRING_STRING MA_SFINAE_TEMPLATE_STRING(string_)

#define MA_SFINAE_TEMPLATE_BOOL_STRING MA_SFINAE_TEMPLATE_STRING(nodeptr_)

#define MA_SFINAE_TEMPLATE_NODE_STRING MA_SFINAE_TEMPLATE_STRING(nodeptr_)

    // #define MA_SFINAE_STR(in_M_type)
    //     typename ma::in_M_type<std::enable_if_t<std::is_base_of<std::basic_string<typename
    //     std::wstring::value_type>, std::wstring>::value>>::type
    //
    // #define MA_SFINAE_STRING_STRING
    //     MA_SFINAE_STR(string_)
    //
    // #define MA_SFINAE_BOOL_STRING
    //     MA_SFINAE_STR(bool_)
    //
    // #define MA_SFINAE_NODE_STRING
    //     MA_SFINAE_STR(nodeptr_)

    template<typename T_String>
    MA_SFINAE_TEMPLATE_STRING_STRING toString(const T_String &value);

    template<typename T_String>
    T_String FromString(const MA_SFINAE_TEMPLATE_STRING_STRING &str);

    template<typename T_String>
    bool IsValidString(const MA_SFINAE_TEMPLATE_STRING_STRING &str);

    template<typename T_String>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_TEMPLATE_STRING_STRING &str);

    //    template <typename T_String>
    //    MA_SFINAE_TEMPLATE_BOOL_STRING IsRepresentedByString();

    template<typename T_String>
    bool IsValidNode(const MA_SFINAE_TEMPLATE_NODE_STRING &node);

    template<typename T_String>
    T_String FromNode(const MA_SFINAE_TEMPLATE_NODE_STRING &node);

    template<typename T_String>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_TEMPLATE_NODE_STRING &node);

    template<>
    M_DLL_EXPORT bool IsRepresentedByString<std::wstring>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<std::wstring &>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<std::wstring &&>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<const std::wstring>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<const std::wstring &>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<const std::wstring &&>();

// template M_DLL_EXPORT MA_SFINAE_STRING_STRING toString<std::wstring>(const std::wstring& value);
// template M_DLL_EXPORT std::wstring FromString(const MA_SFINAE_STRING_STRING& str);
// template M_DLL_EXPORT bool IsValidString<std::wstring>(const MA_SFINAE_STRING_STRING& str);
//
// template M_DLL_EXPORT std::wstring FromNode<std::wstring>(const MA_SFINAE_NODE_STRING& node);
// template M_DLL_EXPORT bool IsValidNode<std::wstring>(const MA_SFINAE_NODE_STRING& node);
// template M_DLL_EXPORT ma::Serialization::Errors GetErrors<std::wstring>(const MA_SFINAE_NODE_STRING& node);

// Type std::filesystem::path //————————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_TEMPLATE_PATH(in_M_type) typename ma::in_M_type<typename T_Path::format>::type

#define MA_SFINAE_TEMPLATE_STRING_PATH MA_SFINAE_TEMPLATE_PATH(string_)

#define MA_SFINAE_TEMPLATE_BOOL_PATH MA_SFINAE_TEMPLATE_PATH(nodeptr_)

#define MA_SFINAE_TEMPLATE_NODE_PATH MA_SFINAE_TEMPLATE_PATH(nodeptr_)

    template<typename T_Path>
    MA_SFINAE_TEMPLATE_STRING_PATH toString(const T_Path &value);

    template<typename T_Path>
    T_Path FromString(const MA_SFINAE_TEMPLATE_STRING_PATH &str);

    template<typename T_Path>
    bool IsValidString(const MA_SFINAE_TEMPLATE_STRING_PATH &str);

    template<typename T_Path>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_TEMPLATE_STRING_PATH &str);

    template<typename T_Path>
    bool IsValidNode(const MA_SFINAE_TEMPLATE_NODE_PATH &node);

    template<typename T_Path>
    T_Path FromNode(const MA_SFINAE_TEMPLATE_NODE_PATH &node);

    template<typename T_Path>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_TEMPLATE_NODE_PATH &node);

    template<>
    M_DLL_EXPORT bool IsRepresentedByString<std::filesystem::path>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<std::filesystem::path &>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<std::filesystem::path &&>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<const std::filesystem::path &>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<const std::filesystem::path &&>();

// spécialisation des templates //——————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_EXPORT_NUMBER(in_M_sfinae_type, in_M_type)                                                           \
    typename ma::in_M_sfinae_type<decltype(std::to_wstring(std::declval<in_M_type>()))>::type

#define MA_SFINAE_EXPORT_STRING_NUMBER(in_M_type) MA_SFINAE_EXPORT_NUMBER(string_, in_M_type)

#define MA_SFINAE_EXPORT_BOOL_NUMBER(in_M_type) MA_SFINAE_EXPORT_NUMBER(bool_, in_M_type)

#define MA_SFINAE_EXPORT_NODE_NUMBER(in_M_type) MA_SFINAE_EXPORT_NUMBER(nodeptr_, in_M_type)

    template<>
    M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(bool) toString<bool>(bool value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(int) toString<int>(int value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(long) toString(long value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(long long) toString(long long value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(unsigned) toString(unsigned value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(unsigned long) toString(unsigned long value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(unsigned long long) toString(unsigned long long value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(float) toString(float value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(double) toString(double value);
    template M_DLL_EXPORT MA_SFINAE_EXPORT_STRING_NUMBER(long double) toString(long double value);

    /*
    template<> M_DLL_EXPORT int FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(int)& str);
    template<> M_DLL_EXPORT long FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(long)& str);
    template<> M_DLL_EXPORT long long FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(long long)& str);
    template<> M_DLL_EXPORT unsigned FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(unsigned)& str);
    template<> M_DLL_EXPORT unsigned long FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(unsigned long)& str);
    template<> M_DLL_EXPORT unsigned long long FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(unsigned long long)&
    str); template<> M_DLL_EXPORT float FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(float)& str); template<>
    M_DLL_EXPORT double FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(double)& str); template<> M_DLL_EXPORT long
    double FromString(const MA_SFINAE_EXPORT_STRING_NUMBER(long double)& str);

    template<> M_DLL_EXPORT bool IsValidString<int>(const MA_SFINAE_EXPORT_STRING_NUMBER(int)& str);
    template<> M_DLL_EXPORT bool IsValidString<long>(const MA_SFINAE_EXPORT_STRING_NUMBER(long)& str);
    template<> M_DLL_EXPORT bool IsValidString<long long>(const MA_SFINAE_EXPORT_STRING_NUMBER(long long)& str);
    template<> M_DLL_EXPORT bool IsValidString<unsigned>(const MA_SFINAE_EXPORT_STRING_NUMBER(unsigned)& str);
    template<> M_DLL_EXPORT bool IsValidString<unsigned long>(const MA_SFINAE_EXPORT_STRING_NUMBER(unsigned long)& str);
    template<> M_DLL_EXPORT bool IsValidString<unsigned long long>(const MA_SFINAE_EXPORT_STRING_NUMBER(unsigned long
    long)& str); template<> M_DLL_EXPORT bool IsValidString<float>(const MA_SFINAE_EXPORT_STRING_NUMBER(float)& str);
    template<> M_DLL_EXPORT bool IsValidString<double>(const MA_SFINAE_EXPORT_STRING_NUMBER(double)& str);
    template<> M_DLL_EXPORT bool IsValidString<long double>(const MA_SFINAE_EXPORT_STRING_NUMBER(long double)& str);
    //*/

    template<>
    M_DLL_EXPORT bool FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(bool) & node);
    template<>
    M_DLL_EXPORT int FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(int) & node);
    template<>
    M_DLL_EXPORT long FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(long) & node);
    template<>
    M_DLL_EXPORT long long FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(long long) & node);
    template<>
    M_DLL_EXPORT unsigned FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned) & node);
    template<>
    M_DLL_EXPORT unsigned long FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long) & node);
    template<>
    M_DLL_EXPORT unsigned long long FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long long) & node);
    template<>
    M_DLL_EXPORT float FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(float) & node);
    template<>
    M_DLL_EXPORT double FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(double) & node);
    template<>
    M_DLL_EXPORT long double FromNode(const MA_SFINAE_EXPORT_NODE_NUMBER(long double) & node);

    template<>
    M_DLL_EXPORT bool IsValidNode<bool>(const MA_SFINAE_EXPORT_NODE_NUMBER(bool) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<int>(const MA_SFINAE_EXPORT_NODE_NUMBER(int) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long long) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<unsigned>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<unsigned long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<unsigned long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long long) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<float>(const MA_SFINAE_EXPORT_NODE_NUMBER(float) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<double>(const MA_SFINAE_EXPORT_NODE_NUMBER(double) & node);
    template<>
    M_DLL_EXPORT bool IsValidNode<long double>(const MA_SFINAE_EXPORT_NODE_NUMBER(long double) & node);

    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<bool>(const MA_SFINAE_EXPORT_NODE_NUMBER(bool) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<int>(const MA_SFINAE_EXPORT_NODE_NUMBER(int) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long long) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<unsigned>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<unsigned long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long) &
                                                                    node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors
    GetErrors<unsigned long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long long) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<float>(const MA_SFINAE_EXPORT_NODE_NUMBER(float) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<double>(const MA_SFINAE_EXPORT_NODE_NUMBER(double) & node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<long double>(const MA_SFINAE_EXPORT_NODE_NUMBER(long double) &
                                                                  node);

// wxULongLong fonctionne seulement avec std::stringstream{}
// template M_DLL_EXPORT MA_SFINAE_STRING_NUMBER(wxULongLong) toString(wxULongLong value, bool inside_struct);
// template<> M_DLL_EXPORT wxULongLong FromString(const MA_SFINAE_STRING_NUMBER(wxULongLong)& str);
// template<> M_DLL_EXPORT bool IsValidString<wxULongLong>(const MA_SFINAE_STRING_NUMBER(wxULongLong)& str);

// Serialization Item //————————————————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_STRING_ITEM typename ma::string_<decltype(T_Item::m_SavePath)>::type

#define MA_SFINAE_STRING_ITEM_PTR typename ma::string_<decltype(T_ItemPtr::element_type::m_SavePath)>::type

// Bizarre cette SFINAE
#define MA_SFINAE_STRING_ITEM_T typename ma::string_<decltype(ma::Item::m_SavePath)>::type

    template<typename T_Item>
    std::shared_ptr<T_Item> FromString(const MA_SFINAE_STRING_ITEM &str);

    template<typename T_ItemPtr>
    T_ItemPtr FromString(const MA_SFINAE_STRING_ITEM_PTR &str);

    template<typename T_Item>
    bool IsValidString(const MA_SFINAE_STRING_ITEM &str);

    template<typename T_ItemPtr>
    bool IsValidString(const MA_SFINAE_STRING_ITEM_PTR &str);

    template<>
    M_DLL_EXPORT bool IsRepresentedByString<ma::Item>();
    template<>
    M_DLL_EXPORT bool IsRepresentedByString<ma::ItemPtr>();

    template<>
    M_DLL_EXPORT bool IsValidString<ma::Item>(const std::wstring &str);
    template<>
    M_DLL_EXPORT bool IsValidString<ma::ItemPtr>(const std::wstring &str);

    template<typename T_Item>
    MA_SFINAE_STRING_ITEM toString(const std::shared_ptr<T_Item> &value);
    template<typename T_ItemPtr>
    MA_SFINAE_STRING_ITEM_PTR toString(const T_ItemPtr &value);

#define MA_SFINAE_NODE_ITEM typename ma::nodeptr_<decltype(T_Item::m_SavePath)>::type

#define MA_SFINAE_NODE_ITEM_PTR typename ma::nodeptr_<decltype(T_ItemPtr::element_type::m_SavePath)>::type

#define MA_SFINAE_NODE_ITEM_T typename ma::nodeptr_<decltype(ma::Item::m_SavePath)>::type

    /// Renvoie un pointeur partagé d'item.
    /// Le pointeur peut être null si la chaîne de caractère est vide.
    /// Si l'item existe, celui-ci sera renvoyé.
    /// \remarks Si chaîne de caractères n'est pas vide, elle doit faire référence à un item existant.
    /// Car sinon on ne peut pas récupérer cet item lors de l'appel à FromNode ou FromString.
    template<typename T_Item>
    std::shared_ptr<T_Item> FromNode(const MA_SFINAE_NODE_ITEM &node);

    /// Renvoie un pointeur partagé d'item.
    /// Le pointeur peut être nul si la chaîne de caractère est vide.
    /// Si l'item existe, celui-ci sera renvoyé.
    /// \remarks Si la chaîne de caractères n'est pas vide, elle doit faire référence à un item existant.
    /// Car sinon on ne peut pas récupérer cet item lors de l'appel à FromNode ou FromString.
    template<typename T_ItemPtr>
    T_ItemPtr FromNode(const MA_SFINAE_NODE_ITEM_PTR &node);

    /// Un nœud représentant un item est valide si sa chaîne de caractère est vide ou si elle est un GUID, c'est-à-dire
    /// que « Id::IsValid » renvoie vrais et que cet Id correspond à un item existant.
    /// \remarks Si chaîne de caractères n'est pas vide, elle doit faire référence à un item existant.
    /// Car sinon on ne peut pas récupérer cet item lors de l'appel à FromNode ou FromString.
    template<typename T_Item>
    bool IsValidNode(const MA_SFINAE_NODE_ITEM &node);

    /// Un nœud représentant un item est valide si sa chaîne de caractère est vide ou si elle est un GUID, c'est-à-dire
    /// que « Id::IsValid » renvoie vrais et que c'est Id correspond à un item existant.
    /// \remarks Si la chaîne de caractères n'est pas vide, elle doit faire référence à un item existant.
    /// Car sinon on ne peut pas récupérer cet item lors de l'appel à FromNode ou FromString.
    template<typename T_ItemPtr>
    bool IsValidNode(const MA_SFINAE_NODE_ITEM_PTR &node);

    template<typename T_Item>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_ITEM &node);

    template<typename T_ItemPtr>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_ITEM_PTR &node);

    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<ma::Item>(const MA_SFINAE_NODE_ITEM_T &node);
    template<>
    M_DLL_EXPORT ma::Serialization::Errors GetErrors<ma::ItemPtr>(const MA_SFINAE_NODE_ITEM_T &node);

    template<>
    M_DLL_EXPORT bool IsValidNode<ma::Item>(const MA_SFINAE_NODE_ITEM_T &node);
    template<>
    M_DLL_EXPORT bool IsValidNode<ma::ItemPtr>(const MA_SFINAE_NODE_ITEM_T &node);

// Sérialisation des variables //———————————————————————————————————————————————————————————————————————————————————————
#define MA_SFINAE_STRING_VAR typename ma::string_<decltype(T_Variable::m_ReadOnly)>::type

#define MA_SFINAE_STRING_VAR_PTR typename ma::string_<decltype(T_VariablePtr::element_type::m_ReadOnly)>::type

#define MA_SFINAE_STRING_VAR_T typename ma::string_<decltype(ma::Var::m_ReadOnly)>::type

    template<typename T_Variable>
    MA_SFINAE_STRING_VAR toString(const std::shared_ptr<T_Variable> &value);

    template<typename T_VariablePtr>
    MA_SFINAE_STRING_VAR_PTR toString(const T_VariablePtr &value);

    template<typename T_Variable>
    std::shared_ptr<T_Variable> FromString(const MA_SFINAE_STRING_VAR &str);

    template<typename T_VariablePtr>
    T_VariablePtr FromString(const MA_SFINAE_STRING_VAR_PTR &str);

    template<typename T_Variable>
    bool IsValidString(const MA_SFINAE_STRING_VAR &str);

    template<typename T_VariablePtr>
    bool IsValidString(const MA_SFINAE_STRING_VAR_PTR &str);

    template<typename T_Variable>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_VAR &str);

    template<typename T_VariablePtr>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_STRING_VAR_PTR &str);

#define MA_SFINAE_NODE_VAR typename ma::nodeptr_<decltype(T_Variable::m_ReadOnly)>::type

#define MA_SFINAE_NODE_VAR_PTR typename ma::nodeptr_<decltype(T_VariablePtr::element_type::m_ReadOnly)>::type

#define MA_SFINAE_NODE_VAR_T typename ma::nodeptr_<decltype(ma::Var::m_ReadOnly)>::type

    /// Renvoie un pointeur partagé de variable.
    /// Le pointeur peut être nul si la chaîne de caractère est vide.
    /// Si la variable existe, celle-ci sera renvoyée.
    /// \remarks Si la chaîne de caractères n'est pas vide, elle doit faire référence à un item existant et une variable
    /// appartenant à cet item.
    /// Car sinon on ne peut pas retrouver la variable lors de l'appel à FromNode ou FromString.
    template<typename T_Variable>
    std::shared_ptr<T_Variable> FromNode(const MA_SFINAE_NODE_VAR &node);

    /// Renvoie un pointeur partagé de variable.
    /// Le pointeur peut être nul si la chaîne de caractère est vide.
    /// Si la variable existe, celle-ci sera renvoyée.
    /// \remarks Si la chaîne de caractères n'est pas vide, elle doit faire référence à un item existant et une variable
    /// appartenant à cet item.
    /// Car sinon on ne peut pas retrouver la variable lors de l'appel à FromNode ou FromString.
    template<typename T_VariablePtr>
    T_VariablePtr FromNode(const MA_SFINAE_NODE_VAR_PTR &node);

    /// Un nœud représentant une variable est valide si sa chaîne de caractères est vide ou si elle représente une paire
    /// {GUID d'item existant, clef d'une variable existante}
    /// \remarks Si la chaîne de caractères n'est pas vide, elle doit faire référence à une variable existante.
    /// Car sinon on ne peut pas récupérer cette variable lors de l'appel à FromNode ou FromString.
    template<typename T_Variable>
    bool IsValidNode(const MA_SFINAE_NODE_VAR &node);

    /// Un nœud représentant une variable est valide si sa chaîne de caractères est vide ou si elle représente une paire
    /// {GUID d'item existant, clef d'une variable existante}
    /// \remarks Si la chaîne de caractères n'est pas vide, elle doit faire référence à une variable existante.
    /// Car sinon on ne peut pas récupérer cette variable lors de l'appel à FromNode ou FromString.
    template<typename T_VariablePtr>
    bool IsValidNode(const MA_SFINAE_NODE_VAR_PTR &node);

    template<typename T_Variable>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_VAR &node);

    template<typename T_VariablePtr>
    ma::Serialization::Errors GetErrors(const MA_SFINAE_NODE_VAR_PTR &node);
} // namespace ma

#include "ma/Serialization.tpp"
