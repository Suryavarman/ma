/// \file Sort.tpp
/// \author Pontier Pierre
/// \date 2020-03-11
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

template<typename T_Compare>
ma::ItemsOrder
ma::Sort::SortItems(const ma::Item::key_type &key_parent, const T_Compare &compare, ma::Item::SearchMod search_mod)
{
    auto item_parent = ma::Item::GetItem(key_parent);
    return ma::Sort::SortItems(item_parent, compare, search_mod);
}

template<typename T_Compare>
ma::ItemsOrder
ma::Sort::SortItems(const ma::ItemPtr &item_parent, const T_Compare &compare, ma::Item::SearchMod search_mod)
{
    ma::ItemsOrder result = item_parent->GetItemsPostOrder(search_mod);

    auto it_part = std::partition(result.begin(), result.end(), compare);
    std::sort(result.begin(), it_part, compare);

    return result;
}

// CompareItemsLessVar //———————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Var>
ma::CompareItemsLessVar<T_Var>::CompareItemsLessVar(const ma::Var::key_type &key_var):
ma::CompareItems(),
m_KeyVar(key_var)
{}

template<typename T_Var>
ma::CompareItemsLessVar<T_Var>::CompareItemsLessVar(const CompareItemsLessVar &other):
ma::CompareItems(),
m_KeyVar(other.m_KeyVar)
{}

template<typename T_Var>
ma::CompareItemsLessVar<T_Var>::~CompareItemsLessVar() = default;

template<typename T_Var>
bool ma::CompareItemsLessVar<T_Var>::operator()(ma::ItemPtr &a, ma::ItemPtr &b) const
{
    MA_ASSERT(a->HasVar(m_KeyVar),
              L"L'item «" + a->GetKey() + L"» ne possède pas de variable «" + m_KeyVar + L"».",
              std::invalid_argument);

    MA_ASSERT(b->HasVar(m_KeyVar),
              L"L'item «" + b->GetKey() + L"» ne possède pas de variable «" + m_KeyVar + L"».",
              std::invalid_argument);

    auto name_a = a->GetVar<T_Var>(m_KeyVar);
    auto name_b = b->GetVar<T_Var>(m_KeyVar);

    return name_a->Get() < name_b->Get();
}

template<typename T_Var>
bool ma::CompareItemsLessVar<T_Var>::operator()(ma::ItemPtr &item) const
{
    return item->HasVar(m_KeyVar);
}

// SortItemVar //———————————————————————————————————————————————————————————————————————————————————————————————————————
template<class T_Compare>
ma::SortItemVar<T_Compare>::SortItemVar(const ma::Var::ConstructorParameters &params, const T_Compare &compare):
ma::Var(params),
m_Compare(compare)
{}

template<class T_Compare>
ma::SortItemVar<T_Compare>::~SortItemVar()
{}

template<class T_Compare>
std::wstring ma::SortItemVar<T_Compare>::toString() const
{
    return L"";
}

template<class T_Compare>
void ma::SortItemVar<T_Compare>::fromString(const std::wstring &value)
{
    // Ne se passe rien, car la variable ne fait que rendre compte de son observation.
}

template<class T_Compare>
void ma::SortItemVar<T_Compare>::Reset()
{
    // Ne se passe rien car la variable observe
}

template<class T_Compare>
bool ma::SortItemVar<T_Compare>::IsValidString(const std::wstring &value) const
{
    return true;
}

template<class T_Compare>
ma::ItemsOrder ma::SortItemVar<T_Compare>::Get()
{
    return ma::Sort::SortItems(GetItemKey(), m_Compare, ma::Item::SearchMod::LOCAL);
}
