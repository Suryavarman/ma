/// \file ModuleC.h
/// \author Pontier Pierre
/// \date 2023-11-15
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Gestion des module compiler en c/c++
///

#pragma once

#include "ma/Module.h"
#include "ma/Dll.h"

#if MA_PLATFORM == MA_PLATFORM_WIN32
struct HINSTANCE__;
typedef struct HINSTANCE__ *MA_DYNLIB_HANDLE;
#else
typedef void *MA_DYNLIB_HANDLE;
#endif

namespace ma
{
    /// Gestion des modules c/c++.
    /// Le module doit contenir les fonctions suivantes:
    /// \code{.cpp}
    /// #include <ma/Dll.h>
    /// #include <ma/Ma.h>
    /// extern "C" void M_DLL_EXPORT dllStartPlugin(void);
    /// extern "C" void M_DLL_EXPORT dllStopPlugin(void);
    /// \endcode
    class M_DLL_EXPORT ModuleCMaker: public ModuleMaker
    {
        public:
            /// - « .so » sur Linux. \n
            /// - « .dll » sur Windows. \n
            /// - « .dylib, .framework » sur MacOS. \n
            /// \exception std::domain_error La plateforme n'est pas géré, aucune extension n'est retournée.
            static const std::initializer_list<std::wstring> &GetExtensions();

            /// Renvoie les préfixes des librairies « "lib", "" ». \n
            /// Exemple: \n
            /// « libmodule_sample.so »
            /// \remarks Le plus probable est placé en premier.
            /// \todo Gérer selon la plateforme.
            static const std::initializer_list<std::wstring> &GetLibPrefixes();

            explicit ModuleCMaker(const Item::ConstructorParameters &in_params);
            ModuleCMaker() = delete;
            ~ModuleCMaker() override;

            DataMakerPtr Clone(const Item::key_type &key) const;

            DataPtr CreateData(ContextLinkPtr &link, ma::ItemPtr client, const CreateParameters &params) const override;

            const TypeInfo &GetFactoryTypeInfo() const override;
            static std::wstring GetDataClassName();
            static std::wstring GetClientClassName();

            /// \return Si le dossier du module contient une librairie du même nom avec l'extension : \n
            /// - « .so » sur Linux. \n
            /// - « .dll » sur Windows. \n
            /// - « .dylib, .framework » sur MacOS. \n
            /// Alors la fonction renvoie le chemin du fichier d'entré du module.
            std::optional<std::filesystem::path>
            Match(const std::filesystem::directory_entry &dir_entry) const override;

            M_HEADER_CLASSHIERARCHY(ModuleCMaker)
    };
    typedef std::shared_ptr<ModuleCMaker> ModuleCMakerPtr;
    M_HEADER_MAKERDATA_TYPE(ModuleCMaker)

    /// Module pour charger dynamiquement une librairie.
    /// Le code a été compilé et les accès se font via du code écrit en C.
    /// Linux: .so
    /// Windows: .dll
    /// Darwin: .dylib, .dynlib
    /// Documentation:
    /// https://developer.apple.com/library/archive/documentation/DeveloperTools/Conceptual/DynamicLibraries/000-Introduction/Introduction.html
    ///
    /// \remarks Le fichier de configuration sera utilisé pour définir les options de chargement des librairies
    /// dynamiques.
    ///  Le fichier de configuration qui sera lu avant de charger le point d'entré (la librairie dynamique).
    /// Il porte le même nom que le dossier du module et possède l'extension « .cfg ».
    /// Exemples: \n
    /// module_toto —> libmodule_toto.cfg \n
    /// moduuli_toto2 —> moduuli_toto.cfg \n
    /// Les groupes possibles sont: \n
    /// - [linux] \n
    /// - [mac] \n
    /// - [windows] \n
    /// - [dependencies] \n
    /// Les valeurs possibles pour chaque groupe sont:
    /// - [linux] \n
    /// flag={"RTLD_LAZY", "RTLD_NOW", "RTLD_GLOBAL", "RTLD_LOCAL", "RTLD_NODELETE", "RTLD_NOLOAD",
    ///       "RTLD_DEEPBIND"} \n
    /// \see https://linux.die.net/man/3/dlsym \n
    /// Par défaut: {" RTLD_NOW", "RTLD_LOCAL"}
    /// - [mac] \n
    /// - [windows] \n
    /// - [dependencies] \n
    ///     modules={"chemin relatif du dossier du module A", \n
    ///              "chemin relatif du dossier du module B"} \n
    ///     Ex: \n
    ///         modules={"02c3f38c-7477-430d-9e9c-2298b3263768"} \n
    /// \n
    ///    Le chemin relatif permet de retrouver le module. Le « .guid » sera utilisé pour retrouvé rapidement
    /// le dossier si il est déjà surveillé, sinon il sera chargé. \n
    ///
    class M_DLL_EXPORT ModuleC: public Module
    {
        protected:
            /// Raccourcis pour accéder au gestionnaire d'observation.
            ma::ObservationManagerPtr m_ObservationManager;

            /// Raccourcis pour accéder au registre des types.
            ma::ClassInfoManagerPtr m_ClassInfoManager;

            /// Chemin du fichier d'entré.
            /// Cela permet d'établir le chemin une fois.
            std::filesystem::path m_PluginPath;

            /// Chemin du fichier de configuration pour définir les options du chargement du module.
            /// Cela permet d'établir le chemin une fois.
            std::filesystem::path m_ConfigPath;

            /// Le pointeur sur la dll le so ou la dylib.
            MA_DYNLIB_HANDLE m_Handle;

            /// \todo Observer le parent du maker pour que mettre m_MakerC à null quand le maker n'est plus observable.
            ///       Et si m_MakerC pointe sur null et que le parent des fabriques contient le maker ce type mettre à
            ///       jour m_MakerC. Ou trouver une manière pour que les fabriques ne soient pas détruites au
            ///       chargement.
            ModuleCMakerPtr m_MakerC;

#if MA_PLATFORM == MA_PLATFORM_LINUX
            /// \see https://linux.die.net/man/3/dlsym
            int m_LinuxFlag;
#endif

            /// Renvoie la dernière erreur.
            std::wstring DynLibError();

            /// Returns the address of the given symbol from the loaded library.
            /// \param name The name of the symbol to search for
            /// \return If the function succeeds, the returned value is a handle to the symbol.
            /// \par If the function fails, the returned value is <b>nullptr</b>.
            /// \todo remplacer void*.
            void *getSymbol(const std::wstring &name) const noexcept;

#if MA_PLATFORM == MA_PLATFORM_LINUX
            /// \return La valeur associé au drapeaux.
            /// \see https://linux.die.net/man/3/dlsym
            /// Si flags est vide les valeurs par défauts sont :
            /// RTLD_NOW | RTLD_LOCAL
            static int GetLinuxFlag(const std::set<std::wstring> &flags);
#endif

        public:
            /// Représente le fichier qui sera charger. \n
            /// Le fichier d'entré doit être nommé ainsi: \n
            /// {prefix lib}{prexfix module}_{nom du dossier parent sans le préfixe « module_ »} \n
            /// Exemples: \n
            /// module_toto —> libmodule_toto.so \n
            /// moduuli_toto2 —> moduuli_toto.dll
            ma::ResourceVarPtr m_EntryPoint;

            explicit ModuleC(const Item::ConstructorParameters &in_params,
                             const ContextLinkPtr &link,
                             const ItemPtr &client,
                             ModuleCMakerPtr maker);
            ModuleC() = delete;
            ~ModuleC() override;

            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            bool Load() override;
            bool UnLoad() override;

            void MakerReplacement(ma::DataMakerPtr old_maker, ma::DataMakerPtr new_maker) override;

            M_HEADER_CLASSHIERARCHY(ModuleC)
    };
    typedef std::shared_ptr<ModuleC> ModuleCPtr;
    M_HEADER_MAKERDATA_TYPE(ModuleC)

} // namespace ma
