/// \file Noncopyable.h
/// \author Pontier Pierre
/// \date 2019-11-04
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once
#include "ma/Dll.h"

namespace ma
{
    class M_DLL_EXPORT Noncopyable
    {
        public:
            Noncopyable() = default;
            ~Noncopyable() = default;

        private:
            Noncopyable(const Noncopyable &) = delete;
            Noncopyable &operator=(const Noncopyable &) = delete;
    };
} // namespace ma
