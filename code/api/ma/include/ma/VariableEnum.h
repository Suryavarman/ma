/// \file VariableEnum.h
/// \author Pontier Pierre
/// \date 2021-05-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/Variable.h"
#include "ma/ObservationManager.h"

namespace ma
{
    /// Une énumération est définie dans Ma comme étant une série de couples clef, valeur. La clef étant unique.
    /// Sa représentation en chaîne de caractères se fait ainsi:
    /// \code
    /// R"#({{"rouge", 0}, {"bleu", 1}})#"
    /// \endcode
    /// Ce qui en C++ correspondrait à cet énumération:
    /// \code
    /// enum color
    /// {
    ///     rouge = 0,
    ///     bleu = 1
    /// };
    /// \endcode
    /// \remarks Se la clef est unique. On peut donc écrire
    /// R"#({{"rouge", 0}, {"bleu", 0}})#"
    /// \endcode
    /// \todo Voir si la valeur doit être aussi unique.
    template<typename T_Name, typename T_Value, typename T_Compare = std::less<T_Name>>
    class T_EnumerationVar: public T_Var<std::map<T_Name, T_Value, T_Compare>>
    {
        public:
            typedef T_Name enum_key_type;
            typedef T_Value enum_value_type;

#if MA_COMPILER_CLANG == MA_COMPILER
            using T_Var<std::map<T_Name, T_Value, T_Compare>>::T_Var;
#else
            using typename T_Var<std::map<T_Name, T_Value, T_Compare>>::T_Var;
#endif
    };

    /// T_EnumVar défini la classe qui servira à stocker la valeur sélectionnée
    /// d'une énumération
    template<typename T_EnumerationVar>
    class T_EnumVar: public ma::ObservableVar, public ma::Observer
    {
        public:
            typedef T_EnumerationVar enumeration_type;
            typedef std::shared_ptr<T_EnumerationVar> enumeration_ptr_type;

            /// Le type des noms/clefs de l'énumération.
            typedef typename T_EnumerationVar::enum_key_type enum_key_type;

            /// Le type de la valeur associée
            typedef typename T_EnumerationVar::enum_value_type enum_value_type;

            /// Le type qui représente la valeur de cette variable.
            /// La clef sélectionnée de l'énumération et la variable possédant
            /// les valeurs de l'énumération.
            typedef std::pair<enum_key_type, enumeration_ptr_type> value_type;

        protected:
            value_type m_Value,
                m_Previous, ///< L'ancienne valeur
                m_Default; ///< La valeur par défaut.

            virtual void SetFromItem(const std::wstring &) override;
            virtual void SetFromItem(const value_type &value);

            virtual ma::VarPtr Copy() const override;

            /// Permet de vérifier si la clef choisi fait partie des clefs de
            /// l'énumération. Si ce n'est pas le cas la clef sera affectée
            /// d'une chaîne de caractères vide.
            virtual void EnumerationCheck();

        public:
            T_EnumVar(const ma::Var::ConstructorParameters &params, const value_type &value);
            T_EnumVar(const ma::Var::ConstructorParameters &params,
                      const ObservablePtr &enum_var,
                      const enum_key_type &enum_choice_name);
            T_EnumVar(const ma::Var::ConstructorParameters &params, const ObservablePtr &enum_var);
            T_EnumVar(const ma::Var::ConstructorParameters &params);
            T_EnumVar() = delete;
            T_EnumVar(const T_EnumVar &) = delete;
            virtual ~T_EnumVar();

            /// \return clef de l'item, suivit de la clef de la variable.
            /// Si la variable est nulle alors une chaîne de caractère vide est
            /// renvoyée.
            /// \example "{"00000000-0000-0000-0000-000000000002", "m_VariableName"}"
            virtual std::wstring toString() const override;

            virtual std::wstring GetKeyValueToString() const override;

            /// A partir de la clef de l'item et de la clef de la variable, elle
            /// retrouve la référence à la variable et l'affecte à m_Var.
            virtual void fromString(const std::wstring &) override;

            /// Permet de tester si la chaîne de caractère est syntaxiquement
            /// correct.
            /// \remarks à utiliser avant d'appeler fromString.
            virtual bool IsValidString(const std::wstring &) const override;

            virtual void Reset() override;

            virtual void Set(const ObservablePtr &) override;
            virtual ObservablePtr Get() const override;
            virtual ObservableVar &operator=(const ObservablePtr &) override;

            virtual void SetVar(const std::shared_ptr<T_EnumerationVar> &);
            virtual const std::shared_ptr<T_EnumerationVar> &GetVar() const;

            /// L'observation d'un observable débute.
            virtual void BeginObservation(const ObservablePtr &observable) override;

            /// L'observable a été modifié, cette fonction est donc appelée avec
            /// les informations caractérisant la ou les modifications
            /// appliquées à l'observable.
            virtual void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;

            /// l'observable vient de se détruire il faut implémenter la
            /// réaction de l'observateur
            virtual void EndObservation(const ObservablePtr &observable) override;
    };

    /// Énumération classique.
    /// Sa représentation en chaîne de caractères se fait ainsi:
    /// \code
    /// R"#({{"rouge", 0}, {"bleu", 1}})#"
    /// \endcode
    /// Ce qui en C++ correspondrait à cet énumération:
    /// \code
    /// enum color
    /// {
    ///     rouge = 0,
    ///     bleu = 1
    /// };
    /// \endcode
    class M_DLL_EXPORT EnumerationVar: public T_EnumerationVar<std::wstring, unsigned int>
    {
        public:
            using T_EnumerationVar<std::wstring, unsigned int>::T_EnumerationVar;
            M_HEADER_CLASSHIERARCHY(EnumerationVar)
    };
    typedef std::shared_ptr<ma::EnumerationVar> EnumerationVarPtr;
    template M_DLL_EXPORT EnumerationVarPtr
    ma::Item::AddVar<ma::EnumerationVar>(const ma::Var::key_type &key, const ma::EnumerationVar::value_type &value);

    /// Choix d'une clef provenant d'une énumération dynamique composée de
    /// chaînes de caractères associées à un entier non signé.
    /// Sa représentation en chaîne de caractères se fait ainsi:
    /// \code
    /// R"#({"rouge", {"2798396d-d211-40b9-8b8f-55a631d74032", m_EnumerationName}})#"
    ///       choix     clef de l'item                         clef de la variable
    /// \endcode
    class M_DLL_EXPORT EnumerationChoiceVar: public T_EnumVar<ma::EnumerationVar>
    {
        public:
            using T_EnumVar<ma::EnumerationVar>::T_EnumVar;
            M_HEADER_CLASSHIERARCHY(EnumerationChoiceVar)
    };
    typedef std::shared_ptr<ma::EnumerationChoiceVar> EnumerationChoiceVarPtr;
    template M_DLL_EXPORT EnumerationChoiceVarPtr
    ma::Item::AddVar<ma::EnumerationChoiceVar>(const ma::Var::key_type &key,
                                               const ma::EnumerationChoiceVar::value_type &value);

    /// Variable indiquant le choix d'une valeur appartenant à une énumération
    /// constante et statique composée de chaînes de caractères associées à un
    /// entier non signé.
    /// Sa représentation en chaîne de caractères se fait ainsi:
    /// \code
    /// R"#({"rouge"})#"
    /// \endcode
    /// \remarks A l'inverse de l'énumération dynamique, la statique doit
    /// représenter un choix parmi les valeurs de l'énumération. Si la valeur
    /// par défaut n'est pas spécifiée, elle sera représentée par la première
    /// valeur de l'énumération.
    /// \tparam T_Name le type de la représentation de la valeur. Le nom associé
    /// à la valeur.
    /// \tparam T_Value le type de la valeur. La valeur associé au nom.
    /// \tparam T_EnumFoncteur  L'énumération s'obtient ainsi :
    /// T_EnumFoncteur::Get(). Attention Get doit retourner une référence.
    /// \code
    /// struct BitmapTypes
    /// {
    ///     ma::wx::BitmapTypesVar::value_type Get();
    /// };
    ///
    /// #define M_BITMAP_TYPE_DEF(type) {std::wstring(ma::to_wstring(#type)), type}
    //
    /// ma::wx::BitmapTypesVar::value_type ma::wx::BitmapTypes::Get()
    /// {
    ///     ma::wx::BitmapTypesVar::value_type result
    ///     {
    ///         M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_INVALID),
    ///         M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_BMP)
    ///     };
    ///
    ///     return result;
    /// }
    /// \tparam T_Compare Permet d'ordonnée les énumération
    /// Exemple: std::less<T_Name>
    /// \remarks Les valeurs doivent être unique, sinon une exception sera
    /// levée. Cela permet de retrouver à partir de la valeur la chaîne de
    /// caractère associée.
    /// \see https://stackoverflow.com/questions/4104147/why-cant-template-non-type-parameters-be-of-class-type
    template<typename T_Name, typename T_Value, typename T_EnumFoncteur, typename T_Compare = std::less<T_Value>>
    class T_StaticEnumVar: public T_Var<T_Value>
    {
        public:
            typedef T_Name enum_name_type;
            typedef T_Value enum_value_type;

            /// Le but est d'ordonner, de veiller à l'unicité des noms et
            /// d'associer le nom à une valeur.
            typedef std::map<T_Name, T_Value, T_Compare> name_value_type;

            /// Le but est de retrouver au plus vite la représentation de la
            /// la valeur et de veiller à l'unicité des valeurs.
            typedef std::unordered_map<T_Value, T_Name> value_name_type;

        protected:
            virtual void SetFromItem(const T_Value &) override;

            /// La clef est le noms de la valeur.
            static const name_value_type &ms_NameValues;

            /// La clef est la valeur de type T_Value et la valeur est le nom du
            /// type T_Name
            static const value_name_type &ms_ValueNames;

        public:
            /// La clef est le noms de la valeur.
            /// \remarks je n'utilise pas de variable statique mais plutôt une
            /// fonction pour être sur que la valeur existe bien lors de
            /// son utilisation. La variable statique existe bien à l'intérieur
            /// de la fonction.
            static const name_value_type &GetNameValues();

            /// La clef est la valeur de type T_Value et la valeur est le nom du
            /// type T_Name
            /// \remarks je n'utilise pas de variable statique mais plutôt une
            /// fonction pour être sur que la valeur existe bien lors de
            /// son utilisation. La variable statique existe bien à l'intérieur
            /// de la fonction.
            static const value_name_type &GetValueNames();

            T_StaticEnumVar(const ma::Var::ConstructorParameters &params);
            T_StaticEnumVar(const ma::Var::ConstructorParameters &params, const T_Value &value);
            T_StaticEnumVar(const ma::Var::ConstructorParameters &params,
                            const T_Value &value,
                            const T_Value &default_value);
            T_StaticEnumVar() = delete;
            T_StaticEnumVar(const T_StaticEnumVar &) = delete;
            virtual ~T_StaticEnumVar();

            virtual bool IsValidString(const std::wstring &value) const override;
    };
} // namespace ma

#include "ma/VariableEnum.tpp"
