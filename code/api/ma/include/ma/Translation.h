/// \file Translation.h
/// \author Pontier Pierre
/// \date 2020-11-05
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/String.h"

// https://fr.cppreference.com/w/cpp/locale/setlocale
namespace ma
{
    M_DLL_EXPORT std::wstring Tr(const std::wstring &str);
}

#define M_Tr(str) ma::Tr((str))
