/// \file Prerequisites.h
/// \author Pontier Pierre
/// \date 2020-12-16
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

/*
 *-------------------------------------------------------------------------
 *This source file was a part of OGRE
 *(Object-oriented Graphics Rendering Engine)
 *
 *For the latest info, see http://www.ogre3d.org/
 *
 *Original version Copyright (C) 2000-2009 Torus Knot Software Ltd
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *THE SOFTWARE
 *-------------------------------------------------------------------------
 */

#pragma once

#include "ma/Platform.h"
// #include <cstddef>

/** @todo Pierre Pontier : généré une erreur si C++11 n'est pas géré */
// #if GCC_VERSION >= 4.7
//// http://en.wikipedia.org/wiki/C11_%28C_standard_revision%29
//// http://stackoverflow.com/questions/4991707/how-to-find-my-current-compilers-standard-like-if-it-is-c90-etc
// #if(__STDC_VERSION__ >= 201112L)
//     #define M_C11
// #endif

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cstdarg>
#include <cmath>

// STL containers
#include <vector>
#include <map>
#include <string>
#include <set>
#include <list>
#include <deque>
#include <queue>
#include <bitset>
#include <unordered_map>
#include <unordered_set>

// STL algorithms & functions
#include <algorithm>
#include <functional>
#include <limits>

// C++ Stream stuff
#include <fstream>
#include <iosfwd>
#include <sstream>

#if MA_PLATFORM == MA_PLATFORM_WIN32 || MA_PLATFORM == MA_PLATFORM_WINRT
    #undef min
    #undef max
    #if defined(__MINGW32__)
        #include <unistd.h>
    #endif
#endif

// #ifndef nullptr
// #	define nullptr std::nullptr_t()
// #endif

/*
// This header provides a compatibility layer for some MSVC symbols
// for MinGW. They may be needed when using MSVC libraries, most
// notably the DirectX SDK.

#ifdef __MINGW32__
    #include <stdint.h>
        // define a number of symbols MSVC uses for annotation.

    #include <specstrings.h>
    #define __in_z
    #define __in_z_opt

    #ifdef __MINGW64_VERSION_MAJOR
        #define __in
    #endif

    #define UINT8 uint8_t
    #define WINAPI_INLINE inline
    #define __uuidof(Object) IID_##Object

#endif // __MINGW32__
*/
