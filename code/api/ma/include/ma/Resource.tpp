/// \file Resource.tpp
/// \author Pontier Pierre
/// \date 2020-11-15
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

template<typename T_Context, typename T>
std::shared_ptr<T> ma::Resource::Get(ma::ItemPtr client)
{}
