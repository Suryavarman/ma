/// \file TypeInfo.h
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Prerequisites.h"
#include "ma/Dll.h"
#include "ma/String.h"
#include "ma/Error.h"

#include <vector>
#include <optional>

namespace ma
{
    /// Une fonction qui peut être utile pour afficher le nom d'un type quelconque.
    /// \see https://stackoverflow.com/questions/81870/is-it-possible-to-print-a-variables-type-in-standard-c
    /// \example
    /// \code
    /// std::cout << ma::TypeName<decltype(value)>() << std::endl;
    /// \endcode
    template<typename T>
    constexpr auto TypeName() noexcept
    {
        std::string_view name, prefix, suffix;
#ifdef __clang__
        name = __PRETTY_FUNCTION__;
        prefix = "auto type_name() [T = ";
        suffix = "]";
#elif defined(__GNUC__)
        name = __PRETTY_FUNCTION__;
        prefix = "constexpr auto type_name() [with T = ";
        suffix = "]";
#elif defined(_MSC_VER)
        name = __FUNCSIG__;
        prefix = "auto __cdecl type_name<";
        suffix = ">(void) noexcept";
#else
        name = "Error: unsupported compiler";
#endif

        name.remove_prefix(prefix.size());
        name.remove_suffix(suffix.size());

        return std::string(name);
    }

    template<typename T>
    constexpr auto TypeNameW() noexcept
    {
        std::wstring_view name, prefix, suffix;
#ifdef __clang__
        name = ma::to_wstring(__PRETTY_FUNCTION__);
        prefix = L"auto type_name() [T = ";
        suffix = L"]";
#elif defined(__GNUC__)
        name = ma::to_wstring(__PRETTY_FUNCTION__);
        prefix = L"constexpr auto type_name() [with T = ";
        suffix = L"]";
#elif defined(_MSC_VER)
        name = ma::to_wstring(__FUNCSIG__);
        prefix = L"auto __cdecl type_name<";
        suffix = L">(void) noexcept";
#else
        name = L"Error: unsupported compiler";
#endif

        name.remove_prefix(prefix.size());
        name.remove_suffix(suffix.size());

        return std::wstring(name);
    }

    class TypeInfo;
    typedef std::vector<std::vector<ma::TypeInfo>> ClassHierarchy;

    /// Clef : le nom de la classe. \n
    /// Valeur : Liste des classes parentes. \n
    typedef std::unordered_map<std::wstring, std::unordered_set<std::wstring>> QuickAccessHierarchies;

    /// Information sur le type des Item et des variables.
    /// \example
    /// \code{.cpp}
    /// namespace ma
    /// {
    ///     class Toto : public Item
    ///     {
    ///         public:
    ///             using ma::Item::Item;
    ///             M_HEADER_CLASSHIERARCHY(Toto)
    ///     };
    /// }
    ///
    /// namespace ma
    /// {
    ///     static const TypeInfo& GetTotoTypeInfo()
    ///     {
    ///         static const TypeInfo info =
    ///         {
    ///             GetTotoClassName(),
    ///             "Description",
    ///             {}
    ///         };
    ///
    ///         return info;
    ///     }
    ///     M_CPP_CLASSHIERARCHY(Toto, Item)
    /// }
    ///
    /// \endcode
    /// \exemple
    /// Code pour hériter des données du parents.
    /// \code{.cpp}
    /// TypeInfo ModuleManager::GetModuleManagerTypeInfo()
    /// {
    ///     auto context_typeinfo = Context::GetContextTypeInfo();
    ///     // clang-format off
    ///     static const TypeInfo info =
    ///     {
    ///         GetModuleManagerClassName(),
    ///         L"La classe « ModuleManager » permet de définir une base commune pour charger et décharger un module.",
    ///         context_typeinfo.MergeDatas(
    ///         {
    ///             {
    ///                 TypeInfo::Key::GetWhiteListChildrenData(),
    ///                 {
    ///                     Module::GetModuleClassName(), ContextDataMaker::GetContextDataMakerClassName()
    ///                 }
    ///             }
    ///         })
    ///     };
    ///     // clang-format on
    ///
    ///     return info;
    /// }
    /// \endcode
    /// \remarks Dans cet exemple « GetTotoClassName » et « GetTypeInfo » sont déclarés par les deux macros: \n
    ///  - M_HEADER_CLASSHIERARCHY \n
    ///  - M_CPP_CLASSHIERARCHY \n
    ///
    class M_DLL_EXPORT TypeInfo
    {
        public:
            typedef std::unordered_map<std::wstring, std::vector<std::wstring>> Data;

            struct Key
            {
                    /// \brief Identifiant des données pour définir la liste des noms des classes acceptés comme enfant
                    /// par le type représenté par cette instance de TypeInfo. \see ma::Item::AcceptToAddChild \see
                    /// ma::Item::AcceptToRemoveChild
                    static const Data::key_type &GetWhiteListChildrenData();

                    /// \brief Identifiant des données pour définir la liste des noms des classes qui ne sont pas
                    /// acceptés comme enfant par le type représenté par cette instance de TypeInfo. \see
                    /// ma::Item::AcceptToAddChild \see ma::Item::AcceptToRemoveChild
                    static const Data::key_type &GetBlackListChildrenData();

                    /// \brief Identifiant des données pour définir la liste des noms des classes acceptés comme parents
                    /// par le type représenté par cette instance de TypeInfo. \see ma::Item::AcceptParent
                    static const Data::key_type &GetWhiteListParentsData();

                    /// \brief Identifiant des données pour définir la liste des noms
                    /// des classes qui ne sont pas acceptés comme parents par le type
                    /// représenté par cette instance de TypeInfo.
                    /// \see ma::Item::AcceptParent
                    static const Data::key_type &GetBlackListParentsData();

                    /// \brief Permet aux interfaces graphiques de connaître la variable à utiliser pour trier les
                    /// enfants de l'éléments. Si vide par défaut les éléments sont affichés par ordre d'insertion Si
                    /// non vide contient la clef de la variable à utiliser pour trier les éléments. Exemple: \n
                    /// TypeInfo::Key::GetVarOrderChildrenDisplay(),
                    /// {
                    ///     Item::Key::Var::ms_Name
                    /// }
                    static const Data::key_type &GetVarOrderChildrenDisplay();

                    /// \brief Permet de définir les dépendances d'un type héritant d'Item.
                    /// Exemple d'utilité:
                    /// Cela permet à un module qui se ferme, de retrouver les instances qui dépendent de ses
                    /// définitions.
                    /// \remarks Les dépendances sont les clefs des éléments nécessaires à l'instanciation du type
                    /// ainsi défini.
                    /// \code{.cpp}
                    /// TypeInfo Toto::GetTotoTypeInfo()
                    /// {
                    ///     // clang-format off
                    ///     static const TypeInfo info =
                    ///     {
                    ///         GetTotoClassName(),
                    ///         L"La classe « Toto » est une classe du module « ModuleToto ».",
                    ///         {
                    ///             {
                    ///                 TypeInfo::Key::GetDependenciesData(),
                    ///                 {
                    ///                     // La clef unique du module
                    ///                     ma::Module::GetGuid(M_FILE_NAME)
                    ///                 }
                    ///             }
                    ///         })
                    ///     };
                    ///     // clang-format on
                    ///
                    ///     return info;
                    /// }
                    /// \endcode
                    static const TypeInfo::Data::key_type &GetDependenciesData();
            };

            /// \remarks Une précondition le nom de la classe doit être le même que celui généré par la macro
            /// m_ClassName. Le nom de classe doit être unique indépendamment des espaces de nom.
            /// \see M_CPP_MAKER_TYPE(in_M_type)
            /// \example
            /// ma::Item aura pour nom de classe "Item" \n
            /// ma::Engine::Node aura pour nom de classe "Engine::Node" \n
            /// \warning Ne pas changer l'ordre des variables. La déclaration des TypeInfos en est dépendante. \n
            /// 1- m_ClassName \n
            /// 2- m_Description \n
            /// 3- m_Data \n
            std::wstring m_ClassName;

            /// Description du type.
            /// Dans un futur proche la description devra être écrite en Markdown.
            /// https://github.com/Orc/discount/tree/v2.2.6
            /// https://stackoverflow.com/questions/889434/markdown-implementations-for-c-c
            /// \todo m_Description devra être dans m_Data.
            ///       Ainsi il sera facile de récupérer les descriptions au travers de la hiérarchie.
            std::wstring m_Description;

            /// Permet d'ajouter des informations supplémentaires.
            Data m_Data;

            /// \return la hiérarchie de la classe
            const ma::ClassHierarchy &GetHierarchy() const;

            /// Le clef représente le nom de la classe
            /// La valeur est la hiérarchie de la classe.
            typedef std::unordered_map<std::wstring, ClassHierarchy> ClassHierarchies;
            static const ClassHierarchies &AddClassHierarchies(const ClassHierarchy &hierarchy);

            /// Permet d'accéder à l'ensemble des hiérarchies de classe de Ma.
            static const ClassHierarchies &GetClassHierarchies();

            /// Permet d'accéder rapidement à l'ensemble des noms des parents
            /// Pour chaque classe enregistrée.
            static const QuickAccessHierarchies &GetQuickAccessHierarchies();

            /// Permet d'accéder rapidement à l'ensemble des noms des parents de
            /// cette classe.
            const QuickAccessHierarchies::mapped_type &GetQuickAccessHierarchy() const;

            /// Le type de résultat retourné par GetDatas \n
            /// - std::wstring le nom de la classe \n
            /// - Le niveau de hiérarchie de la classe. Permet si de besoin de regrouper les données par niveau. \n
            /// - Les valeurs associées à la clef pour cette classe
            typedef std::vector<std::optional<std::tuple<std::wstring, size_t, Data::mapped_type>>> datas_type;

            /// Permet de récupérer les données associées à une clef à travers
            /// la hiérarchie de la classe.
            /// \param class_name Le nom de la classe dans laquelle nous cherchons un type de données.
            /// \param key La clef des données recherchées.
            /// \code {.cpp}
            /// const auto datas = TypeInfo::GetDatas(ma::Engine::Nodef::GetNodefClassName(),
            ///                                       ma::TypeInfo::ms_KeyBlackListParentsData);
            /// for(const auto& key_value : datas)
            /// {
            ///     if(key_value)
            ///     {
            ///         // Le nom de la classe: std::wstring
            ///         const auto& key = std::get<0>(key_value.value);
            ///
            ///         // La niveau de hiérarchie de la classe
            ///         const size_t level = std::get<1u>(key_value.value);
            ///
            ///         // Data::mapped_type -> std::vector<std::wstring>
            ///         const auto& value = std::get<2u>(key_value.value);
            ///     }
            /// }
            /// \endcode
            static datas_type GetDatas(const std::wstring &class_name, const Data::key_type &key);

            /// Permet de récupérer les données associées à une clef à travers
            /// la hiérarchie de la classe.
            /// \param key La clef des données recherchées.
            /// \code {.cpp}
            /// const auto datas = item->GetDatas(ma::TypeInfo::ms_KeyBlackListParentsData);
            /// for(const auto& key_value : datas)
            /// {
            ///     if(key_value)
            ///     {
            ///         // Le nom de la classe: std::wstring
            ///         const auto& key = std::get<0>(key_value.value);
            ///
            ///         // La niveau de hiérarchie de la classe
            ///         const size_t level = std::get<1u>(key_value.value);
            ///
            ///         // Data::mapped_type -> std::vector<std::wstring>
            ///         const auto& value = std::get<2u>(key_value.value);
            ///     }
            /// }
            /// \endcode
            datas_type GetDatas(const Data::key_type &key) const;

            /// Permet de retrouver les informations d'un type de classe à partir du nom de la classe.
            static const ma::TypeInfo &GetTypeInfo(const std::wstring &class_name);

            /// Permet de savoir si le nom d'une classe est associé à un TypeInfo.
            [[maybe_unused]]
            static bool HasTypeInfo(const std::wstring &class_name);

            /// Permet aux classes enfants de récupérer les données de la classe parente et ainsi d'éviter de les
            /// réécrire manuellement.
            Data MergeDatas(const Data &in_data) const;

            /// \brief Permet de récupérer la liste des dépendances pour le type en question.
            TypeInfo::Data::mapped_type GetDependencies() const;

            /// \brief Permet de récupérer la liste des dépendances pour une classe donnée.
            /// \param class_name Le nom de la classe dont on cherche ses dépendances.
            static TypeInfo::Data::mapped_type GetDependencies(const std::wstring &class_name);
    };

    typedef std::unordered_map<std::wstring, std::wstring> InstanceInfo;

/// Déclare les fonctions et variable nécessaire pour définir une classe une classe qui sera utiliser via la ma::RTTI
/// et la classe ma::ClassInfo.
/// \todo renvoyer les TypeInfo par référence constante. Les TypeInfo sont déclaré statiquement.
#define M_HEADER_CLASSHIERARCHY(in_M_Class)                                                                            \
    static const std::wstring ms_##in_M_Class##ClassName;                                                              \
    static ma::ClassHierarchy Get##in_M_Class##ClassHierarchy();                                                       \
    static std::wstring Get##in_M_Class##ClassName();                                                                  \
    [[maybe_unused]]                                                                                                   \
    static const ma::TypeInfo &Get##in_M_Class##TypeInfo();                                                            \
    const ma::TypeInfo &GetTypeInfo() const override;

/// Déclare les fonctions et variable nécessaire pour définir une classe une classe qui sera utiliser via la ma::RTTI et
/// la classe ma::ClassInfo.
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                         \
        static const std::wstring ms_##in_M_NameSpace##in_M_Class##ClassName;                                          \
        static ma::ClassHierarchy Get##in_M_NameSpace##in_M_Class##ClassHierarchy();                                   \
        static std::wstring Get##in_M_NameSpace##in_M_Class##ClassName();                                              \
        [[maybe_unused]]                                                                                               \
        static const ma::TypeInfo &Get##in_M_NameSpace##in_M_Class##TypeInfo();                                        \
        const ma::TypeInfo &GetTypeInfo() const override;
#else
    #define M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                         \
        static const std::wstring ms_##in_M_NameSpace##in_M_Class##ClassName;                                          \
        static ma::ClassHierarchy Get##in_M_NameSpace##in_M_Class##ClassHierarchy();                                   \
        static std::wstring Get##in_M_NameSpace##in_M_Class##ClassName();                                              \
        static const ma::TypeInfo &Get##in_M_NameSpace##in_M_Class##TypeInfo();                                        \
        const ma::TypeInfo &GetTypeInfo() const override;
#endif // MA_COMPILER_CLANG
/// \see M_HEADER_CLASSHIERARCHY
/// Dans le cas ou GetTypeInfo n'est pas surchargé.
#define M_HEADER_CLASSHIERARCHY_NO_OVERRIDE(in_M_Class)                                                                \
    static const std::wstring ms_##in_M_Class##ClassName;                                                              \
    static ma::ClassHierarchy Get##in_M_Class##ClassHierarchy();                                                       \
    static std::wstring Get##in_M_Class##ClassName();                                                                  \
    [[maybe_unused]]                                                                                                   \
    static const ma::TypeInfo &Get##in_M_Class##TypeInfo();                                                            \
    virtual const ma::TypeInfo &GetTypeInfo() const;

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// Dans le cas ou GetTypeInfo n'est pas surchargé.
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_HEADER_CLASSHIERARCHY_NO_OVERRIDE_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                             \
        static const std::wstring ms_##in_M_NameSpace##in_M_Class##ClassName;                                          \
        static ma::ClassHierarchy Get##in_M_NameSpace##in_M_Class##ClassHierarchy();                                   \
        static std::wstring Get##in_M_NameSpace##in_M_Class##ClassName();                                              \
        [[maybe_unused]]                                                                                               \
        static const ma::TypeInfo &Get##in_M_NameSpace##in_M_Class##TypeInfo();                                        \
        virtual const ma::TypeInfo &GetTypeInfo() const;
#else
    #define M_HEADER_CLASSHIERARCHY_NO_OVERRIDE_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                             \
        static const std::wstring ms_##in_M_NameSpace####in_M_Class##ClassName;                                        \
        static ma::ClassHierarchy Get##in_M_NameSpace####in_M_Class##ClassHierarchy();                                 \
        static std::wstring Get##in_M_NameSpace####in_M_Class##ClassName();                                            \
        [[maybe_unused]]                                                                                               \
        static const ma::TypeInfo &Get##in_M_NameSpace####in_M_Class##TypeInfo();                                      \
        virtual const ma::TypeInfo &GetTypeInfo() const;
#endif // MA_COMPILER_CLANG

/// Permet de définir le nom d'une classe et d'ajouter la hiérarchie des classes de celle-ci à «hierarchies» de la
/// fonction ma::TypeInfo::AddClassHierarchies.
/// «hierarchies» peut être récupérer via:
/// ma::TypeInfo::GetClassHierarchies()
/// \see ma::TypeInfo
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSNAME(in_M_Class)                                                                                    \
    const std::wstring ma::in_M_Class::ms_##in_M_Class##ClassName =                                                    \
        ma::TypeInfo::AddClassHierarchies(in_M_Class::Get##in_M_Class##ClassHierarchy())                               \
            .at(wxString(#in_M_Class).ToStdWstring())[0][0]                                                            \
            .m_ClassName;                                                                                              \
                                                                                                                       \
    std::wstring ma::in_M_Class::Get##in_M_Class##ClassName()                                                          \
    {                                                                                                                  \
        return wxString(#in_M_Class).ToStdWstring();                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_Class::GetTypeInfo() const                                                            \
    {                                                                                                                  \
        return Get##in_M_Class##TypeInfo();                                                                            \
    }

/// Permet de définir le nom d'une classe avec un sous nom de domaine et d'ajouter la hiérarchie des classes de celle-ci
/// à « hierarchies » de la fonction.
/// ma::TypeInfo::AddClassHierarchies.
/// «hierarchies» peut être récupérer via:
/// ma::TypeInfo::GetClassHierarchies()
/// \see ma::TypeInfo
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
/// \example ma::wx::Context le nom de classe sera "wx::Context"
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                 \
        const std::wstring ma::in_M_NameSpace::in_M_Class::ms_##in_M_NameSpace##in_M_Class##ClassName =                \
            ma::TypeInfo::AddClassHierarchies(                                                                         \
                in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy())                         \
                .at(wxString(#in_M_NameSpace "::" #in_M_Class).ToStdWstring())[0][0]                                   \
                .m_ClassName;                                                                                          \
                                                                                                                       \
        std::wstring ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassName()                      \
        {                                                                                                              \
            return wxString(#in_M_NameSpace "::" #in_M_Class).ToStdWstring();                                          \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_Class::GetTypeInfo() const                                        \
        {                                                                                                              \
            return Get##in_M_NameSpace##in_M_Class##TypeInfo();                                                        \
        }
#else
    #define M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                 \
        const std::wstring ma::in_M_NameSpace::in_M_Class::ms_##in_M_NameSpace####in_M_Class##ClassName =              \
            ma::TypeInfo::AddClassHierarchies(                                                                         \
                in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy())                       \
                .at(wxString(#in_M_NameSpace "::" #in_M_Class).ToStdWstring())[0][0]                                   \
                .m_ClassName;                                                                                          \
                                                                                                                       \
        std::wstring ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassName()                    \
        {                                                                                                              \
            return wxString(#in_M_NameSpace "::" #in_M_Class).ToStdWstring();                                          \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_Class::GetTypeInfo() const                                        \
        {                                                                                                              \
            return Get##in_M_NameSpace####in_M_Class##TypeInfo();                                                      \
        }
#endif // MA_COMPILER_CLANG
/// \see M_HEADER_CLASSHIERARCHY
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY_1(in_M_Class)                                                                             \
    M_CPP_CLASSNAME(in_M_Class)                                                                                        \
    ma::ClassHierarchy ma::in_M_Class::Get##in_M_Class##ClassHierarchy()                                               \
    {                                                                                                                  \
        return {{Get##in_M_Class##TypeInfo()}};                                                                        \
    }

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_1(in_M_Class, in_M_NameSpace)                                          \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            return {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}};                                                    \
        }
#else
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_1(in_M_Class, in_M_NameSpace)                                          \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            return {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}};                                                  \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY_2(in_M_Class, in_M_ClassParent_1)                                                         \
    M_CPP_CLASSNAME(in_M_Class)                                                                                        \
    ma::ClassHierarchy ma::in_M_Class::Get##in_M_Class##ClassHierarchy()                                               \
    {                                                                                                                  \
        ma::ClassHierarchy hierarchy = {{Get##in_M_Class##TypeInfo()}},                                                \
                           parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy();     \
                                                                                                                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                       \
        return hierarchy;                                                                                              \
    }

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_2(in_M_Class, in_M_NameSpace, in_M_ClassParent_1)                      \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}},                            \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_2(in_M_Class, in_M_NameSpace, in_M_ClassParent_1)                      \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}},                          \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY2_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_2(in_M_Class, in_M_NameSpace, in_M_ParentHierarchy_1)                 \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}};                            \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_2(in_M_Class, in_M_NameSpace, in_M_ParentHierarchy_1)                 \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}};                          \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY_3(in_M_Class, in_M_ClassParent_1, in_M_ClassParent_2)                                     \
    M_CPP_CLASSNAME(in_M_Class)                                                                                        \
    ma::ClassHierarchy ma::in_M_Class::Get##in_M_Class##ClassHierarchy()                                               \
    {                                                                                                                  \
        ma::ClassHierarchy hierarchy = {{Get##in_M_Class##TypeInfo()}},                                                \
                           parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(),     \
                           parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy();     \
                                                                                                                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                       \
        return hierarchy;                                                                                              \
    }

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_3(in_M_Class, in_M_NameSpace, in_M_ClassParent_1, in_M_ClassParent_2)  \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}},                            \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_3(in_M_Class, in_M_NameSpace, in_M_ClassParent_1, in_M_ClassParent_2)  \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}},                          \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_3(                                                                    \
        in_M_Class, in_M_NameSpace, in_M_ParentHierarchy_1, in_M_ParentHierarchy_2)                                    \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}};                            \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_3(                                                                    \
        in_M_Class, in_M_NameSpace, in_M_ParentHierarchy_1, in_M_ParentHierarchy_2)                                    \
        M_CPP_CLASSNAME_WITH_NAMESPACE(in_M_Class, in_M_NameSpace)                                                     \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}};                          \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY_4(in_M_Class, in_M_ClassParent_1, in_M_ClassParent_2, in_M_ClassParent_3)                 \
    M_CPP_CLASSNAME(in_M_Class)                                                                                        \
    ma::ClassHierarchy ma::in_M_Class::Get##in_M_Class##ClassHierarchy()                                               \
    {                                                                                                                  \
        ma::ClassHierarchy hierarchy = {{Get##in_M_Class##TypeInfo()}},                                                \
                           parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(),     \
                           parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(),     \
                           parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy();     \
                                                                                                                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                       \
        return hierarchy;                                                                                              \
    }

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_4(                                                                     \
        in_M_Class, in_M_NameSpace, in_M_ClassParent_1, in_M_ClassParent_2, in_M_ClassParent_3)                        \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}},                            \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_4(                                                                     \
        in_M_Class, in_M_NameSpace, in_M_ClassParent_1, in_M_ClassParent_2, in_M_ClassParent_3)                        \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}},                          \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_4(                                                                    \
        in_M_Class, in_M_NameSpace, in_M_ParentHierarchy_1, in_M_ParentHierarchy_2, in_M_ParentHierarchy_3)            \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}};                            \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_4(                                                                    \
        in_M_Class, in_M_NameSpace, in_M_ParentHierarchy_1, in_M_ParentHierarchy_2, in_M_ParentHierarchy_3)            \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}};                          \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY_5(                                                                                        \
    in_M_Class, in_M_ClassParent_1, in_M_ClassParent_2, in_M_ClassParent_3, in_M_ClassParent_4)                        \
    M_CPP_CLASSNAME(in_M_Class)                                                                                        \
    ma::ClassHierarchy ma::in_M_Class::Get##in_M_Class##ClassHierarchy()                                               \
    {                                                                                                                  \
        ma::ClassHierarchy hierarchy = {{Get##in_M_Class##TypeInfo()}},                                                \
                           parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(),     \
                           parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(),     \
                           parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(),     \
                           parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy();     \
                                                                                                                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                       \
        return hierarchy;                                                                                              \
    }

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_5(                                                                     \
        in_M_Class, in_M_NameSpace, in_M_ClassParent_1, in_M_ClassParent_2, in_M_ClassParent_3, in_M_ClassParent_4)    \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}},                            \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(), \
                               parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                   \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_5(                                                                     \
        in_M_Class, in_M_NameSpace, in_M_ClassParent_1, in_M_ClassParent_2, in_M_ClassParent_3, in_M_ClassParent_4)    \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}},                          \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(), \
                               parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                   \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_5(in_M_Class,                                                         \
                                                   in_M_NameSpace,                                                     \
                                                   in_M_ParentHierarchy_1,                                             \
                                                   in_M_ParentHierarchy_2,                                             \
                                                   in_M_ParentHierarchy_3,                                             \
                                                   in_M_ParentHierarchy_4)                                             \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}};                            \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
            const ma::ClassHierarchy parent_hierarchy_4 = in_M_ParentHierarchy_4;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.cbegin(), parent_hierarchy_4.cend());                 \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_5(in_M_Class,                                                         \
                                                   in_M_NameSpace,                                                     \
                                                   in_M_ParentHierarchy_1,                                             \
                                                   in_M_ParentHierarchy_2,                                             \
                                                   in_M_ParentHierarchy_3,                                             \
                                                   in_M_ParentHierarchy_4)                                             \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}};                          \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
            const ma::ClassHierarchy parent_hierarchy_4 = in_M_ParentHierarchy_4;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.cbegin(), parent_hierarchy_4.cend());                 \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY_6(                                                                                        \
    in_M_Class, in_M_ClassParent_1, in_M_ClassParent_2, in_M_ClassParent_3, in_M_ClassParent_4, in_M_ClassParent_5)    \
    M_CPP_CLASSNAME(in_M_Class)                                                                                        \
    ma::ClassHierarchy ma::in_M_Class::Get##in_M_Class##ClassHierarchy()                                               \
    {                                                                                                                  \
        ma::ClassHierarchy hierarchy = {{Get##in_M_Class##TypeInfo()}},                                                \
                           parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(),     \
                           parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(),     \
                           parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(),     \
                           parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(),     \
                           parent_hierarchy_5 = ma::in_M_ClassParent_5::Get##in_M_ClassParent_5##ClassHierarchy();     \
                                                                                                                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_5.begin(), parent_hierarchy_5.end());                       \
        return hierarchy;                                                                                              \
    }

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_6(in_M_Class,                                                          \
                                                  in_M_NameSpace,                                                      \
                                                  in_M_ClassParent_1,                                                  \
                                                  in_M_ClassParent_2,                                                  \
                                                  in_M_ClassParent_3,                                                  \
                                                  in_M_ClassParent_4,                                                  \
                                                  in_M_ClassParent_5)                                                  \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}},                            \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(), \
                               parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(), \
                               parent_hierarchy_5 = ma::in_M_ClassParent_5::Get##in_M_ClassParent_5##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.begin(), parent_hierarchy_5.end());                   \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_6(in_M_Class,                                                          \
                                                  in_M_NameSpace,                                                      \
                                                  in_M_ClassParent_1,                                                  \
                                                  in_M_ClassParent_2,                                                  \
                                                  in_M_ClassParent_3,                                                  \
                                                  in_M_ClassParent_4,                                                  \
                                                  in_M_ClassParent_5)                                                  \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}},                          \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(), \
                               parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(), \
                               parent_hierarchy_5 = ma::in_M_ClassParent_5::Get##in_M_ClassParent_5##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.begin(), parent_hierarchy_5.end());                   \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_6(in_M_Class,                                                         \
                                                   in_M_NameSpace,                                                     \
                                                   in_M_ParentHierarchy_1,                                             \
                                                   in_M_ParentHierarchy_2,                                             \
                                                   in_M_ParentHierarchy_3,                                             \
                                                   in_M_ParentHierarchy_4,                                             \
                                                   in_M_ParentHierarchy_5)                                             \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}};                            \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
            const ma::ClassHierarchy parent_hierarchy_4 = in_M_ParentHierarchy_4;                                      \
            const ma::ClassHierarchy parent_hierarchy_5 = in_M_ParentHierarchy_5;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.cbegin(), parent_hierarchy_4.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.cbegin(), parent_hierarchy_5.cend());                 \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_6(in_M_Class,                                                         \
                                                   in_M_NameSpace,                                                     \
                                                   in_M_ParentHierarchy_1,                                             \
                                                   in_M_ParentHierarchy_2,                                             \
                                                   in_M_ParentHierarchy_3,                                             \
                                                   in_M_ParentHierarchy_4,                                             \
                                                   in_M_ParentHierarchy_5)                                             \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}};                          \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
            const ma::ClassHierarchy parent_hierarchy_4 = in_M_ParentHierarchy_4;                                      \
            const ma::ClassHierarchy parent_hierarchy_5 = in_M_ParentHierarchy_5;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.cbegin(), parent_hierarchy_4.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.cbegin(), parent_hierarchy_5.cend());                 \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY
/// \see M_CPP_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY_7(in_M_Class,                                                                             \
                               in_M_ClassParent_1,                                                                     \
                               in_M_ClassParent_2,                                                                     \
                               in_M_ClassParent_3,                                                                     \
                               in_M_ClassParent_4,                                                                     \
                               in_M_ClassParent_5,                                                                     \
                               in_M_ClassParent_6)                                                                     \
    M_CPP_CLASSNAME(in_M_Class)                                                                                        \
    ma::ClassHierarchy ma::in_M_Class::Get##in_M_Class##ClassHierarchy()                                               \
    {                                                                                                                  \
        ma::ClassHierarchy hierarchy = {{Get##in_M_Class##TypeInfo()}},                                                \
                           parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(),     \
                           parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(),     \
                           parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(),     \
                           parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(),     \
                           parent_hierarchy_5 = ma::in_M_ClassParent_5::Get##in_M_ClassParent_5##ClassHierarchy(),     \
                           parent_hierarchy_6 = ma::in_M_ClassParent_6::Get##in_M_ClassParent_6##ClassHierarchy();     \
                                                                                                                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_5.begin(), parent_hierarchy_5.end());                       \
        hierarchy.insert(hierarchy.end(), parent_hierarchy_6.begin(), parent_hierarchy_6.end());                       \
        return hierarchy;                                                                                              \
    }

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_7(in_M_Class,                                                          \
                                                  in_M_NameSpace,                                                      \
                                                  in_M_ClassParent_1,                                                  \
                                                  in_M_ClassParent_2,                                                  \
                                                  in_M_ClassParent_3,                                                  \
                                                  in_M_ClassParent_4,                                                  \
                                                  in_M_ClassParent_5,                                                  \
                                                  in_M_ClassParent_6)                                                  \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}},                            \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(), \
                               parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(), \
                               parent_hierarchy_5 = ma::in_M_ClassParent_5::Get##in_M_ClassParent_5##ClassHierarchy(), \
                               parent_hierarchy_6 = ma::in_M_ClassParent_6::Get##in_M_ClassParent_6##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.begin(), parent_hierarchy_5.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_6.begin(), parent_hierarchy_6.end());                   \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_7(in_M_Class,                                                          \
                                                  in_M_NameSpace,                                                      \
                                                  in_M_ClassParent_1,                                                  \
                                                  in_M_ClassParent_2,                                                  \
                                                  in_M_ClassParent_3,                                                  \
                                                  in_M_ClassParent_4,                                                  \
                                                  in_M_ClassParent_5,                                                  \
                                                  in_M_ClassParent_6)                                                  \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}},                          \
                               parent_hierarchy_1 = ma::in_M_ClassParent_1::Get##in_M_ClassParent_1##ClassHierarchy(), \
                               parent_hierarchy_2 = ma::in_M_ClassParent_2::Get##in_M_ClassParent_2##ClassHierarchy(), \
                               parent_hierarchy_3 = ma::in_M_ClassParent_3::Get##in_M_ClassParent_3##ClassHierarchy(), \
                               parent_hierarchy_4 = ma::in_M_ClassParent_4::Get##in_M_ClassParent_4##ClassHierarchy(), \
                               parent_hierarchy_5 = ma::in_M_ClassParent_5::Get##in_M_ClassParent_5##ClassHierarchy(), \
                               parent_hierarchy_6 = ma::in_M_ClassParent_6::Get##in_M_ClassParent_6##ClassHierarchy(); \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.begin(), parent_hierarchy_1.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.begin(), parent_hierarchy_2.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.begin(), parent_hierarchy_3.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.begin(), parent_hierarchy_4.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.begin(), parent_hierarchy_5.end());                   \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_6.begin(), parent_hierarchy_6.end());                   \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_7(in_M_Class,                                                         \
                                                   in_M_NameSpace,                                                     \
                                                   in_M_ParentHierarchy_1,                                             \
                                                   in_M_ParentHierarchy_2,                                             \
                                                   in_M_ParentHierarchy_3,                                             \
                                                   in_M_ParentHierarchy_4,                                             \
                                                   in_M_ParentHierarchy_5,                                             \
                                                   in_M_ParentHierarchy_6)                                             \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace##in_M_Class##ClassHierarchy()           \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace##in_M_Class##TypeInfo()}};                            \
            const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                                      \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
            const ma::ClassHierarchy parent_hierarchy_4 = in_M_ParentHierarchy_4;                                      \
            const ma::ClassHierarchy parent_hierarchy_5 = in_M_ParentHierarchy_5;                                      \
            const ma::ClassHierarchy parent_hierarchy_6 = in_M_ParentHierarchy_6;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.cbegin(), parent_hierarchy_4.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.cbegin(), parent_hierarchy_5.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_6.cbegin(), parent_hierarchy_6.cend());                 \
            return hierarchy;                                                                                          \
        }
#else
    #define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_7(in_M_Class,                                                         \
                                                   in_M_NameSpace,                                                     \
                                                   in_M_ParentHierarchy_1,                                             \
                                                   in_M_ParentHierarchy_2,                                             \
                                                   in_M_ParentHierarchy_3,                                             \
                                                   in_M_ParentHierarchy_4,                                             \
                                                   in_M_ParentHierarchy_5,                                             \
                                                   in_M_ParentHierarchy_6)                                             \
        M_CPP_CLASSNAME(in_M_Class, in_M_NameSpace)                                                                    \
        ma::ClassHierarchy ma::in_M_NameSpace::in_M_Class::Get##in_M_NameSpace####in_M_Class##ClassHierarchy()         \
        {                                                                                                              \
            ma::ClassHierarchy hierarchy = {{Get##in_M_NameSpace####in_M_Class##TypeInfo()}},                          \
                               const ma::ClassHierarchy parent_hierarchy_1 = in_M_ParentHierarchy_1;                   \
            const ma::ClassHierarchy parent_hierarchy_2 = in_M_ParentHierarchy_2;                                      \
            const ma::ClassHierarchy parent_hierarchy_3 = in_M_ParentHierarchy_3;                                      \
            const ma::ClassHierarchy parent_hierarchy_4 = in_M_ParentHierarchy_4;                                      \
            const ma::ClassHierarchy parent_hierarchy_5 = in_M_ParentHierarchy_5;                                      \
            const ma::ClassHierarchy parent_hierarchy_6 = in_M_ParentHierarchy_6;                                      \
                                                                                                                       \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_1.cbegin(), parent_hierarchy_1.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_2.cbegin(), parent_hierarchy_2.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_3.cbegin(), parent_hierarchy_3.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_4.cbegin(), parent_hierarchy_4.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_5.cbegin(), parent_hierarchy_5.cend());                 \
            hierarchy.insert(hierarchy.end(), parent_hierarchy_6.cbegin(), parent_hierarchy_6.cend());                 \
            return hierarchy;                                                                                          \
        }
#endif // MA_COMPILER_CLANG

/// \remarks Cette macro utilise la technique du "paired, sliding arg list".
/// Cette technique permet d'appeler une macro en fonction du nombre de paramètres.
/// \see M_CPP_CLASSHIERARCHY
#define _GET_CLASSHIERARCHY_OVERRIDE(_1, _2, _3, _4, _5, _6, _7, NAME, ...) NAME

/// \remarks Cette macro utilise la technique du "paired, sliding arg list".
/// Cette technique permet d'appeler une macro en fonction du nombre de paramètres.
/// \see M_CPP_CLASSHIERARCHY_WITH_NAMESPACE
/// \remarks le _0 sert juste au fonctionnement de la macro.
#define _GET_CLASSHIERARCHY_OVERRIDE_WITH_NAMESPACE(_0, _1, _2, _3, _4, _5, _6, _7, NAME, ...) NAME

/// Exemple d'utilisation :
/// \example
/// \code {.cpp}
/// namespace ma{M_CPP_CLASSHIERARCHY(Observable)}
/// namespace ma{M_CPP_CLASSHIERARCHY(Item, Observable)}
/// \endcode
/// \see ma::TypeInfo
/// \see M_HEADER_CLASSHIERARCHY
#define M_CPP_CLASSHIERARCHY(...)                                                                                      \
    _GET_CLASSHIERARCHY_OVERRIDE(__VA_ARGS__,                                                                          \
                                 M_CPP_CLASSHIERARCHY_7,                                                               \
                                 M_CPP_CLASSHIERARCHY_6,                                                               \
                                 M_CPP_CLASSHIERARCHY_5,                                                               \
                                 M_CPP_CLASSHIERARCHY_4,                                                               \
                                 M_CPP_CLASSHIERARCHY_3,                                                               \
                                 M_CPP_CLASSHIERARCHY_2,                                                               \
                                 M_CPP_CLASSHIERARCHY_1, )                                                             \
    (__VA_ARGS__)

/// Si la classe parente est dans l'espace de nom API vous pouvez appeler cette macro.
/// Exemple d'utilisation :
/// \example
/// \code {.cpp}
/// M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Image2, wx, Resource)
/// \endcode
/// \see ma::TypeInfo
/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
#define M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(...)                                                                       \
    _GET_CLASSHIERARCHY_OVERRIDE_WITH_NAMESPACE(__VA_ARGS__,                                                           \
                                                M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_7,                                 \
                                                M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_6,                                 \
                                                M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_5,                                 \
                                                M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_4,                                 \
                                                M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_3,                                 \
                                                M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_2,                                 \
                                                M_CPP_CLASSHIERARCHY_WITH_NAMESPACE_1, )                               \
    (__VA_ARGS__)

/// Si la classe parente n'est pas dans l'espace de nom « ma » vous pouvez appeler cette macro.
/// Exemple d'utilisation :
/// \example
/// \code {.cpp}
/// M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Camerad, Engine, ma::Engine::Noded::GetEngineNodedClassHierarchy())
/// \endcode
/// \see ma::TypeInfo
/// \see M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE
#define M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(...)                                                                      \
    _GET_CLASSHIERARCHY_OVERRIDE_WITH_NAMESPACE(__VA_ARGS__,                                                           \
                                                M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_7,                                \
                                                M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_6,                                \
                                                M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_5,                                \
                                                M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_4,                                \
                                                M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_3,                                \
                                                M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_2,                                \
                                                M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE_1, )                              \
    (__VA_ARGS__)
} // namespace ma
