/// \file ObservationManager.tpp
/// \author Pontier Pierre
/// \date 2020-01-10
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

template<typename T_ObserverVar, typename T_ObservableVar>
ma::ObservationPtr ma::ObservationManager::AddObservation(ma::ObserverPtr observer, ma::ObservablePtr observable)
{
    MA_ASSERT(observable->IsObservable(),
              L"L'observable ne peut être observé car il n'est pas actif ou valide. Clef: « " + observable->GetKey() +
                  L" » Nom de la classe: « " + observable->GetTypeInfo().m_ClassName + L" ».",
              std::invalid_argument);

    ma::ObservationPtr observation;

    if(!HasObservation(observer, observable))
    {
        observation = ma::Item::CreateItem<ma::Observation>({GetKey(), false});

        observation->m_ObservableVar =
            observation->AddReadOnlyMemberVar<T_ObservableVar>(L"m_ObservableVar", observable);
        observation->m_ObserverVar = observation->AddReadOnlyMemberVar<T_ObserverVar>(L"m_ObserverVar", observer);

        MA_ASSERT(observation->m_ObservableVar, L"«observation->m_ObservableVar est nulle.", std::runtime_error);

        MA_ASSERT(observation->m_ObserverVar, L"«observation->m_ObserverVar» est nulle.", std::runtime_error);

        auto key = observation->GetKey();
        m_ObseMap[observer->GetObserverKey()][observable] = key;
        m_ObsaMap[observable][observer->GetObserverKey()] = key;

        observable->SetObserve(true);

        if(IsObservable())
        {
            // On met d'abord à jour le parent
            this->ma::Observable::UpdateObservations({{Key::Obs::ms_AddItemEnd, key}});
        }

        // Puis, on met à jour l'enfant
        observation->EndConstruction();

        observation->m_ObserverVar->Get()->BeginObservation(observation->m_ObservableVar->Get());
    }
    else
    {
        observation = GetObservation(observer, observable);
    }
    return observation;
}
