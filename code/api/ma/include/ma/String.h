/// \file String.h
/// \author Pontier Pierre
/// \date 2020-11-12
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

// https://fr.cppreference.com/w/cpp/string/wide
// https://fr.cppreference.com/w/cpp/locale/setlocale
// https://riptutorial.com/cplusplus/example/4190/conversion-to-std--wstring
// https://stackoverflow.com/questions/58440700/how-to-print-utf-8-symbols-by-entering-decimal-number

#pragma once

#include "ma/Dll.h"
#include <string>
#include <sstream>

using namespace std::literals;

namespace ma
{
    // typedef std::basic_istringstream<std::wstring::value_type> istringstream;
    // typedef std::basic_stringstream<std::wstring::value_type> stringstream;

    /// \brief Permet de définir que std::wcout et que la langue locale globale sera en UTF-8.
    M_DLL_EXPORT void SetGlobalLocal();

    M_DLL_EXPORT std::string to_string(const std::wstring &wstring);
    M_DLL_EXPORT std::wstring to_wstring(const std::string &str);

    // \todo implémenter une fonction Split
    // https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
    // https://en.cppreference.com/w/cpp/ranges/split_view

} // namespace ma
