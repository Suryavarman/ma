/// \file Sort.h
/// \author Pontier Pierre
/// \date 2020-03-11
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/Item.h"
#include "ma/Variable.h"

namespace ma
{
    struct M_DLL_EXPORT CompareItems
    {
            virtual bool operator()(ma::ItemPtr &a, ma::ItemPtr &b) const = 0;

            /// \return Vrais si l'item peut être comparé, sinon Faux. Par défaut la fonction retourne toujours vrais.
            virtual bool operator()(ma::ItemPtr &) const;

            virtual ~CompareItems();
    };

    /// Le type des variables à trier
    template<typename T_Var>
    struct CompareItemsLessVar: public CompareItems
    {
            /// Clef des variables
            /// \example ma::Item::ms_KeyNameVar
            const ma::Var::key_type m_KeyVar;

            /// Constructeur
            /// \param key_var Clef des variables utilisé pour effectuer le tri.
            CompareItemsLessVar(const ma::Var::key_type &key_var);
            CompareItemsLessVar(const CompareItemsLessVar &other);
            CompareItemsLessVar() = delete;
            ~CompareItemsLessVar() override;

            /// \exception std::invalid_parameter l'un des items ou les deux ne
            /// possèdent pas de variable ayant pour clef: «m_KeyVar»
            bool operator()(ma::ItemPtr &a, ma::ItemPtr &b) const override;

            /// \return Vrais si l'item possède une variable ayant pour clef: «m_KeyVar».
            bool operator()(ma::ItemPtr &) const override;
    };

    typedef CompareItemsLessVar<ma::StringVar> CompareItemsString;
    typedef CompareItemsLessVar<ma::BoolVar> CompareItemsBool;
    typedef CompareItemsLessVar<ma::IntVar> CompareItemsInt;
    typedef CompareItemsLessVar<ma::LongVar> CompareItemsLong;
    typedef CompareItemsLessVar<ma::LongLongVar> CompareItemsLongLong;
    typedef CompareItemsLessVar<ma::UVar> CompareItemsU;
    typedef CompareItemsLessVar<ma::ULongVar> CompareItemsULong;
    typedef CompareItemsLessVar<ma::ULongLongVar> CompareItemsULongLong;
    typedef CompareItemsLessVar<ma::FloatVar> CompareItemsFloat;
    typedef CompareItemsLessVar<ma::DoubleVar> CompareItemsDouble;
    typedef CompareItemsLessVar<ma::LongDoubleVar> CompareItemsLongDouble;

    class M_DLL_EXPORT Sort
    {
        public:
            typedef ma::ItemsOrder ItemsOrder;
            typedef ItemsOrder value_type;

            /// \remarks Fonction en template car CompareItems est abstrait et ne peut être utilisé pour construire des
            /// deque.
            template<typename T_Compare>
            static ItemsOrder
            SortItems(const ma::Item::key_type &key_parent, const T_Compare &compare, ma::Item::SearchMod search_mod);

            /// \remarks Fonction en template car CompareItems est abstrait et ne peut être utilisé pour construire des
            /// deque.
            /// Les éléments n'ayant pas la variable utilisée par le tri seront placé à la fin du conteneur.
            template<typename T_Compare>
            static ItemsOrder
            SortItems(const ma::ItemPtr &item_parent, const T_Compare &compare, ma::Item::SearchMod search_mod);

            enum comparisons_type
            {
                less,
                greater
            };

            //            /// En fonction du type des variables liée à la clef de la variable, cette fonction va choisir
            //            le type de Comparaison. static ItemsOrder AutoTypeSortItems(const ma::Item::key_type
            //            &key_parent, comparisons_type type, ma::Var::key_type key_var, ma::Item::SearchMod
            //            search_mod);
            //
            //            /// En fonction du type des variables liée à la clef de la variable, cette fonction va choisir
            //            le type de Comparaison. static ItemsOrder AutoTypeSortItems(const ma::ItemPtr &item_parent,
            //            comparisons_type type, ma::Var::key_type key_var, ma::Item::SearchMod search_mod);
    };

    /// Trie les items enfants d'un item.
    /// https://en.cppreference.com/w/cpp/algorithm/sort
    /// https://en.cppreference.com/w/cpp/utility/functional/greater
    /// https://en.cppreference.com/w/cpp/algorithm/execution_policy_tag_t
    /// \code
    /// struct CustomLess
    /// {
    ///     bool operator()(const ma::ItemPtr& a, const ma::ItemPtr& b) const
    ///     {
    ///         if(a->HasVar(ma::Item::ms_KeyNameVar) && b->HasVar(ma::Item::ms_KeyNameVar))
    ///         {
    ///             auto name_a = a->GetVar(ma::Item::ms_KeyNameVar);
    ///             auto name_b = b->GetVar(ma::Item::ms_KeyNameVar);
    ///             return name_a->Get() < name_b->Get();
    ///         }
    ///         else
    ///            return false;
    ///     }
    ///
    ///     bool IsRecursive()
    ///     {
    ///        return false;
    ///     }
    /// };
    /// \endcode
    template<class T_Compare>
    class SortItemVar: public Var
    {
        public:
            typedef ItemsOrder value_type;

        protected:
            T_Compare m_Compare;
            ItemsOrder m_OrderedItems;

        public:
            SortItemVar(const ma::Var::ConstructorParameters &params, const T_Compare &compare = T_Compare());
            SortItemVar() = delete;
            SortItemVar(const SortItemVar &) = delete;
            virtual ~SortItemVar();

            virtual std::wstring toString() const override;

            /// Ne se passe rien car la variable ne fait que rendre compte de
            /// son observation.
            virtual void fromString(const std::wstring &) override;

            /// Ne se passe rien car la variable ne fait que rendre compte de
            /// son observation.
            virtual void Reset() override;

            /// Renvoie toujours vrais.
            virtual bool IsValidString(const std::wstring &value) const override;

            /// Retourne les items triés.
            virtual ma::ItemsOrder Get();
    };
    // typedef std::shared_ptr<ma::SortItemVar> SortItemVarPtr;

    // template
    // M_DLL_EXPORT SortItemVarPtr ma::Item::AddVar<ma::SortItemVar>(const ma::Var::key_type& key);
} // namespace ma

#include "ma/Sort.tpp"
