/// \file VariableEnum.tpp
/// \author Pontier Pierre
/// \date 2021-05-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

// T_EnumVar ///////////////////////////////////////////////////////////////////
template<typename T_EnumerationVar>
ma::T_EnumVar<T_EnumerationVar>::T_EnumVar(const ma::Var::ConstructorParameters &params,
                                           const ma::T_EnumVar<T_EnumerationVar>::value_type &value):
ma::ObservableVar::ObservableVar(params),
m_Value{value},
m_Previous{},
m_Default{value}
{
    auto enum_var_ptr = std::dynamic_pointer_cast<T_EnumerationVar>(value.second);
    MA_ASSERT(enum_var_ptr,
              L"value.second n'est pas du type " + ma::TypeNameW<decltype(enum_var_ptr)>(),
              std::invalid_argument);

    auto &enums = enum_var_ptr->Get();

    if(enums.size() == 0)
    {
        MA_ASSERT(value.first == enum_key_type{},
                  L"La valeur sélectionnée" + value.first + L" doit tre vide/nulle car l'énumération est vide.",
                  std::invalid_argument);
    }
    else
    {
        MA_ASSERT(enums.find(value.first) != enums.end(),
                  L"La valeur sélectionnée" + value.first + L"n'existe pas dans l'énumération.",
                  std::invalid_argument);
    }
}

template<typename T_EnumerationVar>
ma::T_EnumVar<T_EnumerationVar>::T_EnumVar(
    const ma::Var::ConstructorParameters &params,
    const ma::ObservablePtr &enum_var,
    const typename ma::T_EnumVar<T_EnumerationVar>::enum_key_type &enum_choice_name):
ma::ObservableVar::ObservableVar(params),
m_Value{enum_choice_name, enum_var},
m_Previous{},
m_Default{enum_choice_name, enum_var}
{
    auto enum_var_ptr = std::dynamic_pointer_cast<T_EnumerationVar>(enum_var);
    MA_ASSERT(
        enum_var_ptr, L"enum_var n'est pas du type " + ma::TypeNameW<decltype(enum_var_ptr)>(), std::invalid_argument);

    auto &enums = enum_var_ptr->Get();

    if(enums.size() == 0)
    {
        MA_ASSERT(enum_choice_name = enum_key_type{},
                  L"La valeur sélectionnée" + enum_choice_name + L" doit être vide/nulle car l'énumération est vide.",
                  std::invalid_argument);
    }
    else
    {
        MA_ASSERT(enums.find(enum_choice_name) != enums.end(),
                  L"La valeur sélectionnée" + enum_choice_name + L"n'existe pas dans l'énumération.",
                  std::invalid_argument);
    }
}

template<typename T_EnumerationVar>
ma::T_EnumVar<T_EnumerationVar>::T_EnumVar(const ma::Var::ConstructorParameters &params,
                                           const ma::ObservablePtr &enum_var):
ma::ObservableVar::ObservableVar(params),
m_Value{{}, enum_var},
m_Previous{},
m_Default{{}, enum_var}
{
    auto enum_var_ptr = std::dynamic_pointer_cast<T_EnumerationVar>(enum_var);
    MA_ASSERT(
        enum_var_ptr, L"enum_var n'est pas du type " + ma::TypeNameW<decltype(enum_var_ptr)>(), std::invalid_argument);

    auto &enums = enum_var_ptr->Get();
    m_Value.first = enums.size() > 0 ? enums->begin()->first : enum_key_type{};
}

template<typename T_EnumerationVar>
ma::T_EnumVar<T_EnumerationVar>::T_EnumVar(const ma::Var::ConstructorParameters &params):
ma::ObservableVar::ObservableVar(params),
m_Value{},
m_Previous{},
m_Default{}
{}

template<typename T_EnumerationVar>
ma::T_EnumVar<T_EnumerationVar>::~T_EnumVar() = default;

template<typename T_EnumerationVar>
std::wstring ma::T_EnumVar<T_EnumerationVar>::toString() const
{
    return ma::toString(m_Value);
}

template<typename T_EnumerationVar>
std::wstring ma::T_EnumVar<T_EnumerationVar>::GetKeyValueToString() const
{
    return ma::toString(std::make_pair(m_Key, m_Value));
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::SetFromItem(const std::wstring &str)
{
    SetFromItem(ma::FromString<ma::T_EnumVar<T_EnumerationVar>::value_type>(str));
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::SetFromItem(const ma::T_EnumVar<T_EnumerationVar>::value_type &value)
{
    auto enum_var_ptr = value.second;
    if(enum_var_ptr)
    {
        auto &enums = enum_var_ptr->Get();

        if(enums.size() == 0)
        {
            MA_ASSERT(value.first == enum_key_type{},
                      L"La valeur sélectionnée" + value.first + L" doit être vide/nulle car l'énumération est vide.",
                      std::invalid_argument);
        }
        else
        {
            MA_ASSERT(enums.find(value.first) != enums.end(),
                      L"La valeur sélectionnée" + value.first + L"n'existe pas dans l'énumération.",
                      std::invalid_argument);
        }
    }
    else
    {
        MA_ASSERT(value.first == enum_key_type{},
                  L"La valeur sélectionnée" + value.first +
                      L" doit être vide/nulle car la variable représentant l'énumération est nulle.",
                  std::invalid_argument);
    }

    if(value != m_Value)
    {
        UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

        bool enumeration_change = m_Value.second != value.second;

        ma::ObservationManagerPtr obs_manager =
            ma::Item::HasItem(Item::Key::GetObservationManager()) ?
                ma::Item::GetItem<ma::ObservationManager>(Item::Key::GetObservationManager()) :
                nullptr;

        if(enumeration_change && obs_manager && obs_manager->HasObservation(this, m_Value.second))
            obs_manager->RemoveObservation(this, m_Value.second);

        m_Previous = m_Value;
        m_Value = value;

        if(enumeration_change && obs_manager)
        {
            obs_manager->AddObservation<ma::VarObserverVar, ma::VarVar>(this, m_Value.second);
        }

        UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::fromString(const std::wstring &str)
{
    SetFromItem(str);
}

template<typename T_EnumerationVar>
ma::VarPtr ma::T_EnumVar<T_EnumerationVar>::Copy() const
{
    auto ptr = std::shared_ptr<ma::T_EnumVar<T_EnumerationVar>>(
        new ma::T_EnumVar<T_EnumerationVar>(GetConstructorParameters(), m_Value));

    ptr->m_Previous = m_Previous;
    ptr->m_Default = m_Default;

    return ptr;
}

template<typename T_EnumerationVar>
bool ma::T_EnumVar<T_EnumerationVar>::IsValidString(const std::wstring &str) const
{
    bool validation = ma::IsValidString<ma::T_EnumVar<T_EnumerationVar>::value_type>(str);

    if(validation)
    {
        ma::Serialization::Parser parser{str};
        auto value = parser.GetValue<ma::T_EnumVar<T_EnumerationVar>::value_type>();

        auto enum_var_ptr = value.second;

        if(enum_var_ptr)
        {
            auto &enums = enum_var_ptr->Get();

            if(enums.size() == 0)
                validation = value.first == enum_key_type{};
            else
                validation = enums.find(value.first) != enums.end() || value.first == enum_key_type{};
        }
    }

    return validation;
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::Reset()
{
    MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

    UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
    m_Previous = m_Value;
    m_Value = m_Default;
    UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::Set(const ObservablePtr &observable)
{
    auto enum_var_ptr = std::dynamic_pointer_cast<T_EnumerationVar>(observable);
    MA_ASSERT(
        enum_var_ptr, L"observable n'est pas du type " + ma::TypeNameW<decltype(observable)>(), std::invalid_argument);

    SetFromItem({m_Value.first, enum_var_ptr});
}

template<typename T_EnumerationVar>
ma::ObservablePtr ma::T_EnumVar<T_EnumerationVar>::Get() const
{
    return m_Value.second;
}

template<typename T_EnumerationVar>
ma::ObservableVar &ma::T_EnumVar<T_EnumerationVar>::operator=(const ma::ObservablePtr &observable)
{
    Set(observable);
    return *this;
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::SetVar(const std::shared_ptr<T_EnumerationVar> &var)
{
    SetFromItem({m_Value.first, var});
}

template<typename T_EnumerationVar>
const std::shared_ptr<T_EnumerationVar> &ma::T_EnumVar<T_EnumerationVar>::GetVar() const
{
    return m_Value.second;
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::EnumerationCheck()
{
    auto &enums = m_Value.second->Get();
    const ma::T_EnumVar<T_EnumerationVar>::enum_key_type none_value{};

    if(enums.size() == 0)
    {
        if(m_Value.first != none_value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Value.first = none_value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }
    else
    {
        if(enums.find(m_Value.first) == enums.end())
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Value.first = none_value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::BeginObservation(const ObservablePtr &observable)
{
    EnumerationCheck();
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::UpdateObservation(const ObservablePtr &observable,
                                                        const ma::Observable::Data &data)
{
    //
    //    std::wcout << L"Observable: {" << observable->GetTypeInfo().m_ClassName << L", " << observable->GetKey() <<
    //    L"}" << std::endl; for(auto it : data.GetOrdered())
    //    {
    //        std::wcout << L"{" << it.first << L", " << it.second + L"}" << std::endl;
    //    }
    //
    //    std::wcout << ma::toString(m_Value) << std::endl;

    if(m_Value.second == observable)
    {
        if(data.HasActions(Key::Obs::ms_EndSetValue))
        {
            EnumerationCheck();
        }
    }

    //    std::wcout << ma::toString(m_Value) << std::endl;
}

template<typename T_EnumerationVar>
void ma::T_EnumVar<T_EnumerationVar>::EndObservation(const ObservablePtr &observable)
{}

// T_StaticEnumVar ///////////////////////////////////////////////////////////////////
#define M_TEMPLATE_STATICENUMVAR template<typename T_Name, typename T_Value, typename T_EnumGetter, typename T_Compare>

#define M_T_StaticEnumVar ma::T_StaticEnumVar<T_Name, T_Value, T_EnumGetter, T_Compare>

M_TEMPLATE_STATICENUMVAR
const typename M_T_StaticEnumVar::name_value_type &M_T_StaticEnumVar::ms_NameValues{M_T_StaticEnumVar::GetNameValues()};

M_TEMPLATE_STATICENUMVAR
const typename M_T_StaticEnumVar::value_name_type &M_T_StaticEnumVar::ms_ValueNames{M_T_StaticEnumVar::GetValueNames()};

M_TEMPLATE_STATICENUMVAR
const typename M_T_StaticEnumVar::name_value_type &M_T_StaticEnumVar::GetNameValues()
{
    static const M_T_StaticEnumVar::name_value_type result{T_EnumGetter::Get()};
    return result;
}

M_TEMPLATE_STATICENUMVAR
const typename M_T_StaticEnumVar::value_name_type &M_T_StaticEnumVar::GetValueNames()
{
    static M_T_StaticEnumVar::value_name_type value_names;

    if(value_names.size() == 0)
    {
        const auto &name_values = M_T_StaticEnumVar::GetNameValues();

        for(auto it : name_values)
        {
            MA_ASSERT(value_names.find(it.second) == value_names.end(),
                      L"La valeur " + ma::toString(it.second) + L" n'est pas unique.",
                      std::invalid_argument);

            value_names[it.second] = it.first;
        }
    }

    return value_names;
}

M_TEMPLATE_STATICENUMVAR
M_T_StaticEnumVar::T_StaticEnumVar(const ma::Var::ConstructorParameters &params): ma::T_Var<T_Value>(params, {})
{
    MA_ASSERT(ms_ValueNames.size(), L"ms_ValueNames ne peut être vide.", std::invalid_argument);

    MA_ASSERT(ms_NameValues.size(), L"ms_NameValues ne peut être vide.", std::invalid_argument);

    this->m_Value = *ms_ValueNames.begin()->first;
    this->m_Previous = this->m_Default = this->m_Value;
}

M_TEMPLATE_STATICENUMVAR
M_T_StaticEnumVar::T_StaticEnumVar(const ma::Var::ConstructorParameters &params, const T_Value &value):
ma::T_Var<T_Value>(params, {})
{
    MA_ASSERT(ms_ValueNames.size(), L"ms_ValueNames ne peut être vide.", std::invalid_argument);

    MA_ASSERT(ms_NameValues.size(), L"ms_NameValues ne peut être vide.", std::invalid_argument);

    MA_ASSERT(ms_ValueNames.find(value) != ms_ValueNames.end(),
              L"La valeur de value n'appartient pas à ms_ValueNames.",
              std::invalid_argument);

    this->m_Value = value;
    this->m_Previous = this->m_Default = ms_ValueNames.begin()->first;
}

M_TEMPLATE_STATICENUMVAR
M_T_StaticEnumVar::T_StaticEnumVar(const ma::Var::ConstructorParameters &params,
                                   const T_Value &value,
                                   const T_Value &default_value):
ma::T_Var<T_Value>(params, {})
{
    MA_ASSERT(ms_ValueNames.size(), L"ms_ValueNames ne peut être vide.", std::invalid_argument);

    MA_ASSERT(ms_NameValues.size(), L"ms_NameValues ne peut être vide.", std::invalid_argument);

    MA_ASSERT(ms_ValueNames.find(value) != ms_ValueNames.end(),
              L"La valeur n'appartient pas à ms_ValueNames.",
              std::invalid_argument);

    MA_ASSERT(ms_ValueNames.find(default_value) != ms_ValueNames.end(),
              L"default_value n'appartient pas à ms_Enumeration.",
              std::invalid_argument);

    this->m_Value = value;
    this->m_Previous = this->m_Default = default_value;
}

M_TEMPLATE_STATICENUMVAR
M_T_StaticEnumVar::~T_StaticEnumVar()
{}

M_TEMPLATE_STATICENUMVAR
void M_T_StaticEnumVar::SetFromItem(const T_Value &value)
{
    MA_ASSERT(ms_ValueNames.find(value) != ms_ValueNames.end(),
              L"La valeur n'appartient pas à ms_ValueNames.",
              std::invalid_argument);

    ma::T_Var<T_Value>::SetFromItem(value);
}

M_TEMPLATE_STATICENUMVAR
bool M_T_StaticEnumVar::IsValidString(const std::wstring &str) const
{
    const T_Value value = ma::FromString<T_Value>(str);

    return ms_ValueNames.find(value) != ms_ValueNames.end();
}
