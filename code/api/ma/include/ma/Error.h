/// \file Error.h
/// \author Pontier Pierre
/// \date 2019-11-18
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/String.h"
#include "ma/Macros.h"
#include <stdexcept>

// https://codecraft.co/2014/11/25/variadic-macros-tricks/
#define MA_ASSERT2(in_test, in_msg)                                                                                    \
    MA_STATEMENT_MACRO_BEGIN                                                                                           \
    {                                                                                                                  \
        bool test = true;                                                                                              \
        test = !!(in_test);                                                                                            \
        ma::Assert<std::logic_error>(test, in_msg, M_FUNCTION_NAME, M_FILE_NAME, M_LINE);                              \
    }                                                                                                                  \
    MA_STATEMENT_MACRO_END

#define MA_ASSERT3(in_test, in_msg, in_exception_type)                                                                 \
    MA_STATEMENT_MACRO_BEGIN                                                                                           \
    {                                                                                                                  \
        bool test = true;                                                                                              \
        test = !!(in_test);                                                                                            \
        ma::Assert<in_exception_type>(test, in_msg, M_FUNCTION_NAME, M_FILE_NAME, M_LINE);                             \
    }                                                                                                                  \
    MA_STATEMENT_MACRO_END

#define MA_ASSERT4(in_test, in_msg, in_function_name, in_exception_type)                                               \
    MA_STATEMENT_MACRO_BEGIN                                                                                           \
    {                                                                                                                  \
        bool test = true;                                                                                              \
        test = !!(in_test);                                                                                            \
        ma::Assert<in_exception_type>(test, in_msg, in_function_name, M_FILE_NAME, M_LINE);                            \
    }                                                                                                                  \
    MA_STATEMENT_MACRO_END

// Define a macro that uses the "paired, sliding arg list"
// technique to select the appropriate override.
#define _GET_ASSERT_OVERRIDE(_1, _2, _3, _4, NAME, ...) NAME

#define MA_ASSERT(...) _GET_ASSERT_OVERRIDE(__VA_ARGS__, MA_ASSERT4, MA_ASSERT3, MA_ASSERT2)(__VA_ARGS__)

#define MA_WARNING2(in_test, in_msg)                                                                                   \
    MA_STATEMENT_MACRO_BEGIN                                                                                           \
    {                                                                                                                  \
        bool test = true;                                                                                              \
        test = !!(in_test);                                                                                            \
        ma::Warning(test, in_msg, M_FUNCTION_NAME, M_FILE_NAME, M_LINE);                                               \
    }                                                                                                                  \
    MA_STATEMENT_MACRO_END

#define MA_WARNING3(in_test, in_msg, in_function_name)                                                                 \
    MA_STATEMENT_MACRO_BEGIN                                                                                           \
    {                                                                                                                  \
        bool test = true;                                                                                              \
        test = !!(in_test);                                                                                            \
        ma::Warning(test, in_msg, in_function_name, M_FILE_NAME, M_LINE);                                              \
    }                                                                                                                  \
    MA_STATEMENT_MACRO_END

// Define a macro that uses the "paired, sliding arg list"
// technique to select the appropriate override.
#define _GET_WARNING_OVERRIDE(_1, _2, _3, NAME, ...) NAME

#define MA_WARNING(...) _GET_WARNING_OVERRIDE(__VA_ARGS__, MA_WARNING3, MA_WARNING2)(__VA_ARGS__)

#define MA_HAS_TO_BE_IMPLEMENTED MA_ASSERT(false, L"La fonction doit être implémentée"s, std::logic_error)

namespace ma
{
    /// \brief Permet de définir le niveau de verbosité.
    /// 0 = Ni les messages d'erreurs et d'avertissements ne s'afficheront dans
    /// la console.
    /// 1 = Seules les messages d'erreurs s'afficheront dans la console
    /// 2 = Les messages d'erreurs et d'avertissements s'afficheront dans la
    /// console.
    /// \remarks La valeur par défaut est de 2.
    extern unsigned int g_ConsoleVerbose;

    /// Le type T est une exception comme définie ici :
    /// \see - <a href="https://en.cppreference.com/w/cpp/error/exception" rel="noopener noreferrer"
    /// target="_blank">std::exception</a>
    template<typename T>
#if MA_COMPILER == MA_COMPILER_CLANG
    void Assert(bool test,
                const std::wstring &msg,
                const std::wstring &function_name,
                const std::wstring &filename,
                const unsigned int &line);
#else
    void Assert(bool test,
                const std::wstring &msg = L"",
                const std::wstring &function_name = M_FUNCTION_NAME,
                const std::wstring &filename = M_FILE_NAME,
                const unsigned int &line = M_LINE);
#endif

#if MA_COMPILER == MA_COMPILER_CLANG
    void Warning(bool test,
                 const std::wstring &msg,
                 const std::wstring &function_name,
                 const std::wstring &filename,
                 const unsigned int &line);
#else
    void Warning(bool test,
                 const std::wstring &msg = L"",
                 const std::wstring &function_name = M_FUNCTION_NAME,
                 const std::wstring &filename = M_FILE_NAME,
                 const unsigned int &line = M_LINE);
#endif

    namespace wx
    {
        /// \remarks Pour éviter des problèmes de confusion avec la macro _() de wxWidgets et pybind11 l'appel à
        /// wxWidgets n'est pas dans l'en-tête.
        M_DLL_EXPORT void Assert(const std::wstring &msg,
                                 const std::wstring &function_name,
                                 const std::wstring &filename,
                                 const unsigned int &line);

        M_DLL_EXPORT void AssertTextBegin();
        M_DLL_EXPORT void AssertTextEnd();

        /// \remarks Pour éviter des problèmes de confusion avec la macro _() de wxWidgets et pybind11 l'appel à
        /// wxWidgets n'est pas dans l'en-tête.
        M_DLL_EXPORT void Warning(const std::wstring &msg,
                                  const std::wstring &function_name,
                                  const std::wstring &filename,
                                  const unsigned int &line);
    } // namespace wx
} // namespace ma

#include "ma/Error.tpp"
