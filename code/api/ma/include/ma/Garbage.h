/// \file Garbage.h
/// \author Pontier Pierre
/// \date 2022-02-11
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Fonctionnement du Garbage Collector de Java :
/// https://welovedevs.com/fr/articles/le-fonctionnement-du-garbage-collector/
/// YoungGen et OldGen, nombre de cycles de survie au CG, plus il survit, plus il a des chances partir dans le OldGen.
///

#pragma once

#include "ma/String.h"
#include "ma/TypeInfo.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/Dll.h"

namespace ma
{
    /// \brief Un débris pointe sur un item qui ne fait plus partie de « Item::ms_Map ».
    /// Cela permet de retrouver les items qui sont encore référencés par d'autres pointeurs partagés.
    class M_DLL_EXPORT Trash: public Item
    {
        public:
            /// \brief L'item qui a été retiré de « Item::ms_Map »
            ma::ItemPtr m_Item;

            /// \brief Permet d'identifier m_Item en affichant sa clef.
            ma::StringVarPtr m_ItemKey;

            /// \brief Le nom de la classe de m_Item.
            ma::StringVarPtr m_ItemClassName;

            /// \brief Constructeur de débris.
            /// \param in_params Les paramètres de construction du type « ma::Item ».
            /// \param item L'item qui est à associer à ce débris.
            /// \exception std::invalid_argument L'item est nul.
            explicit Trash(const Item::ConstructorParameters &in_params, ma::ItemPtr item);
            Trash() = delete;
            virtual ~Trash();

            /// \brief Un débris n'est pas observable sinon il va générer de plus en plus d'items.
            /// \return false
            bool IsObservable() const override;

            M_HEADER_CLASSHIERARCHY(Trash)
    };
    typedef std::shared_ptr<Trash> TrashPtr;

    /// \brief Reçois les items qui sont retirés de « Item::ms_Map ».
    /// \remarks C'est à l'application d'appeler Garbage::CleanTrashes
    class M_DLL_EXPORT Garbage: public Item, public Observer
    {
        protected:
            // /// Si vous accédez à m_Thrashes veillez à utiliser ce « mutex »
            // /// pour sécuriser l'accès.
            // mutex m_ItemsMutex;

            // /// Tous les items qui ont été effacés de « Item::ms_Maps » sont placés dans m_Thrashes en attendant leur
            // /// destruction.
            // ItemsMap m_Thrashes;

            /// \brief Permet d'accéder rapidement à un item depuis sa clef.
            /// La clef de l'item permettra de retrouver l'item résidu pointant sur l'instance de l'item.
            typedef std::unordered_map<Item::key_type, Item::key_type> Links;

            /// \brief Permet d'accéder rapidement à un item depuis sa clef.
            /// La clef de l'item permettra de retrouver l'item résidu pointant sur l'instance de l'item.
            Links m_Links;

            /// Raccourcis d'écriture pour accéder à la racine des éléments.
            ItemPtr m_Root;

            void OnEnable() override;
            void OnDisable() override;

        public:
            /// Constructeur de la poubelle à Item.
            explicit Garbage(const Item::ConstructorParameters &in_params);

            /// Suppression du constructeur par défaut.
            Garbage() = delete;

            /// Destructeur de la poubelle à Item.
            ~Garbage() override;

            void EndConstruction() override;

            // Concrétisation de ma::Observer
            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            /// \brief Permet de savoir si un item est associé à un débris.
            /// \param key La clef de l'item à chercher.
            /// \remark L'item est pointé par la variable « m_Item » du débris.
            virtual bool HasTrash(const Item::key_type &key) const;

            /// \brief Permet de savoir si un item est associé à un débris.
            /// \param key La clef de l'item à chercher.
            /// \remark L'item est pointé par la variable « m_Item » du débris.
            virtual std::optional<TrashPtr> HasTrashOpt(const Item::key_type &key) const;

            /// \brief Permet d'ajouter un item inactif. Il sera associé un item
            /// \param item La clef de l'item à chercher. L'item est pointé par la variable « m_Item » du débris.
            /// \exception std::invalid_argument L'item fait déjà partie de cette poubelle.
            virtual TrashPtr AddTrash(const ItemPtr &item);

            /// \brief Permet d'accéder de retrouver le débris possédant l'item identifié par la clef « key ».
            /// \param key Clef de l'item désiré.
            /// \return Le débris associé à l'identifiant de l'item désiré.
            /// \exception std::invalid_argument L'item ne fait pas partie de cette poubelle.
            virtual TrashPtr GetTrash(const Item::key_type &key);

            /// \brief Retire l'item de la poubelle.
            /// \exception std::invalid_argument L'item ne fait pas partie de cette poubelle.
            virtual void RemoveTrash(const Item::key_type &key);

            /// \brief Surcharge de « Item::RemoveChild » pour s'assurer que « Garbage::m_Links » soit à jour.
            virtual void RemoveChild(ma::ItemPtr item) override;

            /// \brief Retire les débris qui n'ont plus qu'eux pour référencer leur item respectif.
            /// \remarks C'est à l'application d'appeler cette fonction.
            virtual void CleanTrashes();

            M_HEADER_CLASSHIERARCHY(Garbage)
    };
    typedef std::shared_ptr<Garbage> GarbagePtr;
} // namespace ma
