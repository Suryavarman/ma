/// \file Macros.h
/// \author Pontier Pierre
/// \date 2020-12-16
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \remarks Pour récupérer le représentation en chaîne de caractères d'un type donné, veuillez vous reporter au fichier
/// « Info.h » et à la fonction « ma::TypeName<T>() » et « ma::TypeNameW<T>() ».
///

#pragma once

#include <iostream>
#include <string>

#if !defined(M_NOGUI)
    #include <wx/log.h>
#endif

#if defined(__cpp_lib_source_location)
    #include <source_location>
#endif

// L'unicode avec les exceptions
// https://stackoverflow.com/questions/3760731/exceptions-with-unicode-what

// Exemple de ligne de conduite avec l'utilisation des exception.
// https://www.boost.org/community/error_handling.html

// Pour tester la présence ou non d'une implémentation.
// https://en.cppreference.com/w/cpp/feature_test

// gcc/clang __PRETTY_FUNCTION__ vc++ __FUNCSIG__ // std::source_location::function_name __cpp_lib_source_location
// https://stackoverflow.com/questions/34405913/how-could-stdexperimentalsource-location-be-implemented
// Pour gcc il y aussi int __builtin_LINE(), __builtin_FUNCTION(), __builtin_FILE()
// https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html

#if defined(__cpp_lib_source_location)
    #define M_FUNCTION_NAME std::source_location::current().function_name()
#elif MA_COMPILER == MA_COMPILER_GNUC
    #define M_FUNCTION_NAME ma::to_wstring(__PRETTY_FUNCTION__)
#elif MA_COMPILER == MA_COMPILER_CLANG
    #define M_FUNCTION_NAME ma::to_wstring(__PRETTY_FUNCTION__)
#elif MA_COMPILER == MA_COMPILER_MSVC
    #define M_FUNCTION_NAME ma::to_wstring(__FUNCSIG__)
#else
    #error("La définition automatique du nom de la fonction courante n'est pas gérée pour le compilateur courant.")
#endif

#if defined(__cpp_lib_source_location)
    #define M_FILE_NAME ma::to_wstring(std::source_location::current().filename())
#else
    #define M_FILE_NAME ma::to_wstring(__FILE__)
#endif

#if defined(__cpp_lib_source_location)
    #define M_LINE std::source_location::current().line()
#else
    #define M_LINE static_cast<unsigned int>(__LINE__)
#endif

// Pour éviter de problème de déclaration vide et des problèmes liés à « clang-format » : Il faut ajouter ce code aux
// macros qui n'ont pas besoin d'être clôturées par un « ; ». Cela les oblige à être clôturé par « ; »
// Solution provenant des macros de wxWidgets (copie de la documentation et traduction automatique depuis Deepl.)
// Des aides pour définir des macros qui se développent en une seule instruction.
// La solution standard consiste à utiliser l'instruction "do { ... } while (0)" mais MSVC
// génère un avertissement C4127 "l'expression de la condition est constante".
// Utilisons donc quelque chose qui est juste assez compliqué pour ne pas être reconnu comme une constante, mais
// suffisamment simple pour être optimisé.
// Une autre solution serait d'utiliser __pragma() pour désactiver temporairement C4127.
// Remarquez que wxASSERT_ARG_TYPE dans « wx/strvargarg.h » s'appuie sur ces macros créant une sorte de boucle, car il
// utilise "break".
//
// Traduit avec www.DeepL.com/Translator (version gratuite)
#define MA_STATEMENT_MACRO_BEGIN                                                                                       \
    do                                                                                                                 \
    {
#define MA_STATEMENT_MACRO_END                                                                                         \
    }                                                                                                                  \
    while((void)0, 0)

// define M_MSG
#if !defined(M_NOGUI)
    #define MA_MSG(inMsg)                                                                                              \
        MA_STATEMENT_MACRO_BEGIN                                                                                       \
        {                                                                                                              \
            std::wstring msg = M_FILE_NAME + L" : "s + ma::toString(M_LINE) + L" : "s + inMsg;                         \
                                                                                                                       \
            const bool wx_widgets_on = !!wxApp::GetInstance();                                                         \
            if(wx_widgets_on)                                                                                          \
            {                                                                                                          \
                /* wxUSE_UNICODE && !wxUSE_UTF8_LOCALE_ONLY    */                                                      \
                wxLogMessage(msg.c_str());                                                                             \
            }                                                                                                          \
            else                                                                                                       \
                std::cout << ma::to_string(msg) << std::endl;                                                          \
        }                                                                                                              \
        MA_STATEMENT_MACRO_END
#else
    #define MA_MSG(inMsg)                                                                                              \
        MA_STATEMENT_MACRO_BEGIN                                                                                       \
        {                                                                                                              \
            std::cout << ma::to_string(inMsg) << std::endl;                                                            \
        }                                                                                                              \
        MA_STATEMENT_MACRO_END
#endif

// define M_MSGLINE
#define MA_MSGLINE std::cout << __LINE__ << std::endl;
