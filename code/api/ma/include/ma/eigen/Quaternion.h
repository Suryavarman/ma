/// \file eigen/Quaternion.h
/// \author Pontier Pierre
/// \date 2022-04-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/RTTI.h" // Pour que la macro M_HEADER_MAKERVAR_TYPE fonctionne.

#include "ma/eigen/Eigen.h"

/// Le setFromItem ne peut être surchargé, gcc et clang ne prennent pas en compte la surcharge de setFromItem…
/// Je n'ai aucune idée de la raison.
/// EDIT : Je pense savoir pourquoi.
/// C'est parce que je n'avais pas inclus l'Eigen provenant des dépendances pour les projets API_Py et GUI.
/// Le compilateur prenait donc les en-têtes d'Eigen présent dans le système.
#define EIGEN_VAR_QUATERNION_HEADER(in_M_quaternion_type)                                                              \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Eigen                                                                                                \
        {                                                                                                              \
            class M_DLL_EXPORT in_M_quaternion_type##Var: public ma::T_Var<ma::Eigen::in_M_quaternion_type>            \
            {                                                                                                          \
                protected:                                                                                             \
                    virtual ma::VarPtr Copy() const override;                                                          \
                    virtual void SetFromItem(const value_type &value) override;                                        \
                                                                                                                       \
                public:                                                                                                \
                    in_M_quaternion_type##Var(const ma::Var::ConstructorParameters &params, const value_type &var);    \
                    in_M_quaternion_type##Var(const ma::Var::ConstructorParameters &params);                           \
                    in_M_quaternion_type##Var() = delete;                                                              \
                    in_M_quaternion_type##Var(const in_M_quaternion_type##Var &) = delete;                             \
                    virtual ~in_M_quaternion_type##Var();                                                              \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_quaternion_type##Var, Eigen)                           \
            };                                                                                                         \
            typedef std::shared_ptr<in_M_quaternion_type##Var> in_M_quaternion_type##VarPtr;                           \
        }                                                                                                              \
        template M_DLL_EXPORT ma::Eigen::in_M_quaternion_type##VarPtr                                                  \
        ma::Item::AddVar<ma::Eigen::in_M_quaternion_type##Var>(const ma::Var::key_type &key,                           \
                                                               const ma::Eigen::in_M_quaternion_type &value);          \
        template M_DLL_EXPORT ma::Eigen::in_M_quaternion_type##VarPtr                                                  \
        ma::Item::AddVar<ma::Eigen::in_M_quaternion_type##Var>(const ma::Var::key_type &key);                          \
        template<>                                                                                                     \
        M_DLL_EXPORT void                                                                                              \
        ma::T_Var<ma::Eigen::in_M_quaternion_type>::SetFromItem(const ma::Eigen::in_M_quaternion_type &value);         \
    }                                                                                                                  \
    M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(in_M_quaternion_type##Var, Eigen)

#define EIGEN_VAR_QUATERNION_CPP(in_M_quaternion_type)                                                                 \
    ma::Eigen::in_M_quaternion_type##Var::in_M_quaternion_type##Var(const ma::Var::ConstructorParameters &params,      \
                                                                    const value_type &var):                            \
    ma::T_Var<ma::Eigen::in_M_quaternion_type>::T_Var(params, var)                                                     \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::in_M_quaternion_type##Var::in_M_quaternion_type##Var(const ma::Var::ConstructorParameters &params):     \
    ma::T_Var<ma::Eigen::in_M_quaternion_type>::T_Var(params, ma::Eigen::in_M_quaternion_type::Identity())             \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::in_M_quaternion_type##Var::~in_M_quaternion_type##Var()                                                 \
    {}                                                                                                                 \
                                                                                                                       \
    ma::VarPtr ma::Eigen::in_M_quaternion_type##Var::Copy() const                                                      \
    {                                                                                                                  \
        auto ptr = std::shared_ptr<in_M_quaternion_type##Var>(                                                         \
            new in_M_quaternion_type##Var(GetConstructorParameters(), m_Value));                                       \
                                                                                                                       \
        ptr->m_Previous = m_Previous;                                                                                  \
        ptr->m_Default = m_Default;                                                                                    \
                                                                                                                       \
        return ptr;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Eigen::in_M_quaternion_type##Var::SetFromItem(const value_type &value)                                    \
    {                                                                                                                  \
        if(value != m_Value)                                                                                           \
        {                                                                                                              \
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});                  \
            m_Previous = m_Value;                                                                                      \
            m_Value = value;                                                                                           \
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});                    \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    template<>                                                                                                         \
    void ma::T_Var<ma::Eigen::in_M_quaternion_type>::SetFromItem(const ma::Eigen::in_M_quaternion_type &value)         \
    {                                                                                                                  \
        if(value != m_Value)                                                                                           \
        {                                                                                                              \
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});                  \
            m_Previous = m_Value;                                                                                      \
            m_Value = value;                                                                                           \
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});                    \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        const TypeInfo &Eigen::in_M_quaternion_type##Var::GetEigen##in_M_quaternion_type##VarTypeInfo()                \
        {                                                                                                              \
            static const TypeInfo info = {GetEigen##in_M_quaternion_type##VarClassName(),                              \
                                          L"Variable d'item de type «"s + ma::to_wstring(#in_M_quaternion_type) +      \
                                              L"».\n"s + L"Le type des scalaires du quaternion est: « "s +             \
                                              ma::TypeNameW<in_M_quaternion_type::Scalar>() + L" » .\n"s,              \
                                          {}};                                                                         \
                                                                                                                       \
            return info;                                                                                               \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_quaternion_type##Var, Eigen, Var)                                         \
    M_CPP_MAKERVAR_TYPE_WITH_MAMESPACE(in_M_quaternion_type##Var, Eigen)

namespace ma::Eigen
{
    // Des quaternions d'entier ça ne peut pas fonctionner… à la rigueur
    // à partir d'angles d'Euler exprimés en entier.
    // typedef ::Eigen::Quaternion<int> Quaternioni;
    // typedef ::Eigen::Quaternion<unsigned> Quaternionu;
    // typedef ::Eigen::Quaternion<long> Quaternionl;
    // typedef ::Eigen::Quaternion<long long> Quaternionll;

    typedef ::Eigen::Quaternion<float> Quaternionf;
    typedef ::Eigen::Quaternion<double> Quaterniond;
    typedef ::Eigen::Quaternion<long double> Quaternionld;
} // namespace ma::Eigen

// EIGEN_VAR_QUATERNION_HEADER(Quaternioni)
// EIGEN_VAR_QUATERNION_HEADER(Quaternionu)
// EIGEN_VAR_QUATERNION_HEADER(Quaternionl)
// EIGEN_VAR_QUATERNION_HEADER(Quaternionll)

EIGEN_VAR_QUATERNION_HEADER(Quaternionf)
EIGEN_VAR_QUATERNION_HEADER(Quaterniond)
EIGEN_VAR_QUATERNION_HEADER(Quaternionld)
