/// \file eigen/Transform.h
/// \author Pontier Pierre
/// \date 2022-04-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/RTTI.h" // Pour que la macro M_HEADER_MAKERVAR_TYPE fonctionne.
#include "ma/eigen/Matrix.h"
#include "ma/eigen/Quaternion.h"
#include "ma/eigen/Vector.h"

#include "ma/eigen/Eigen.h"

// https://eigen.tuxfamily.org/dox-devel/group__TutorialGeometry.html

// m_Value.translation() = (translation_type(position) - m_Value.translation());
//      const long double to_rad = M_PI / 180.0;
//     const auto rot_mat = ::Eigen::AngleAxis<Vector3##in_M_Scalar_type::value_type>(euler[0] * to_rad,
//     Vector3##in_M_Scalar_type::UnitZ())
//                        * ::Eigen::AngleAxis<Vector3##in_M_Scalar_type::value_type>(euler[1] * to_rad,
//                        Vector3##in_M_Scalar_type::UnitX())
//                        * ::Eigen::AngleAxis<Vector3##in_M_Scalar_type::value_type>(euler[2] * to_rad,
//                        Vector3##in_M_Scalar_type::UnitZ());
// m_Value = (m_Value.rotation().inverse() * rot_mat) * m_Value;
// m_Value = (m_Value.scale().inverse() * scale) * m_Value;
// m_Value = (m_Value.rotation().inverse() * quaternion) * m_Value;

// const ::Eigen::Translation translation{position};
// t *= Eigen::Scaling(scale);// https://docs.ros.org/en/groovy/api/win_eigen/html/classEigen_1_1Scaling.html

/// Le setFromItem ne peut être surchargé, gcc et clang ne prennent pas en compte la surcharge de setFromItem…
/// Je n'ai aucune idée de la raison.
/// EDIT: Je pense savoir pourquoi c'est parce que je n'avais pas inclus l'Eigen provenant des dépendances pour les
/// projets API_Py et GUI.
/// Le compilateur prenait donc les en-têtes d'Eigen présent dans le système.
/// \todo SetPosition, SetQuaternion, SetEuler et SetScale devraient être protégé et accessible pour les nœuds 3D.
///       SetPosition, SetQuaternion, SetEuler et SetScale ne testeront pas si la valeur est différentes de l'actuelle.
///       Cela doit être effectué en amont.
#define EIGEN_VAR_TRANSFORM_HEADER(in_M_Scalar_type)                                                                   \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Eigen                                                                                                \
        {                                                                                                              \
            class M_DLL_EXPORT Transform##in_M_Scalar_type##Var:                                                       \
            public ma::T_Var<ma::Eigen::Transform##in_M_Scalar_type>                                                   \
            {                                                                                                          \
                public:                                                                                                \
                    typedef ma::Eigen::Transform##in_M_Scalar_type::Scalar scalar_type;                                \
                    typedef ma::Eigen::Vector3##in_M_Scalar_type position_type;                                        \
                    typedef ma::Eigen::Quaternion##in_M_Scalar_type quaternion_type;                                   \
                    typedef ma::Eigen::Vector3##in_M_Scalar_type euler_type;                                           \
                    typedef ma::Eigen::Vector3##in_M_Scalar_type scale_type;                                           \
                    typedef ma::Eigen::Transform##in_M_Scalar_type translation_type;                                   \
                                                                                                                       \
                protected:                                                                                             \
                    virtual ma::VarPtr Copy() const override;                                                          \
                    virtual void SetFromItem(const value_type &value) override;                                        \
                                                                                                                       \
                public:                                                                                                \
                    Transform##in_M_Scalar_type##Var(const ma::Var::ConstructorParameters &params,                     \
                                                     const value_type &var);                                           \
                    Transform##in_M_Scalar_type##Var(const ma::Var::ConstructorParameters &params);                    \
                    Transform##in_M_Scalar_type##Var() = delete;                                                       \
                    Transform##in_M_Scalar_type##Var(const Transform##in_M_Scalar_type##Var &) = delete;               \
                    virtual ~Transform##in_M_Scalar_type##Var();                                                       \
                                                                                                                       \
                    virtual void                                                                                       \
                    Set(const position_type &position, const quaternion_type &quaternion, const scale_type &scale);    \
                    virtual void Set(const translation_type &transform) override;                                      \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Transform##in_M_Scalar_type##Var, Eigen)                    \
            };                                                                                                         \
            typedef std::shared_ptr<Transform##in_M_Scalar_type##Var> Transform##in_M_Scalar_type##VarPtr;             \
        }                                                                                                              \
        template M_DLL_EXPORT ma::Eigen::Transform##in_M_Scalar_type##VarPtr                                           \
        ma::Item::AddVar<ma::Eigen::Transform##in_M_Scalar_type##Var>(                                                 \
            const ma::Var::key_type &key,                                                                              \
            const ma::Eigen::Transform##in_M_Scalar_type &value);                                                      \
        template M_DLL_EXPORT ma::Eigen::Transform##in_M_Scalar_type##VarPtr                                           \
        ma::Item::AddVar<ma::Eigen::Transform##in_M_Scalar_type##Var>(const ma::Var::key_type &key);                   \
        template<>                                                                                                     \
        M_DLL_EXPORT void ma::T_Var<ma::Eigen::Transform##in_M_Scalar_type>::SetFromItem(                              \
            const ma::Eigen::Transform##in_M_Scalar_type &value);                                                      \
    }                                                                                                                  \
    M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(Transform##in_M_Scalar_type##Var, Eigen)

#define EIGEN_VAR_TRANSFORM_CPP(in_M_Scalar_type)                                                                      \
    ma::Eigen::Transform##in_M_Scalar_type##Var::Transform##in_M_Scalar_type##Var(                                     \
        const ma::Var::ConstructorParameters &params, const value_type &var):                                          \
    ma::T_Var<ma::Eigen::Transform##in_M_Scalar_type>::T_Var(params, var)                                              \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::Transform##in_M_Scalar_type##Var::Transform##in_M_Scalar_type##Var(                                     \
        const ma::Var::ConstructorParameters &params):                                                                 \
    ma::T_Var<ma::Eigen::Transform##in_M_Scalar_type>::T_Var(params,                                                   \
                                                             ma::Eigen::Transform##in_M_Scalar_type::Identity())       \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::Transform##in_M_Scalar_type##Var::~Transform##in_M_Scalar_type##Var()                                   \
    {}                                                                                                                 \
                                                                                                                       \
    ma::VarPtr ma::Eigen::Transform##in_M_Scalar_type##Var::Copy() const                                               \
    {                                                                                                                  \
        auto ptr = std::shared_ptr<Transform##in_M_Scalar_type##Var>(                                                  \
            new Transform##in_M_Scalar_type##Var(GetConstructorParameters(), m_Value));                                \
                                                                                                                       \
        ptr->m_Previous = m_Previous;                                                                                  \
        ptr->m_Default = m_Default;                                                                                    \
                                                                                                                       \
        return ptr;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Eigen::Transform##in_M_Scalar_type##Var::Set(                                                             \
        const position_type &position, const quaternion_type &quaternion, const scale_type &scale)                     \
    {                                                                                                                  \
        UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});                      \
        m_Previous = m_Value;                                                                                          \
        m_Value.fromPositionOrientationScale(position, quaternion, scale);                                             \
        UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});                        \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Eigen::Transform##in_M_Scalar_type##Var::Set(const translation_type &transform)                           \
    {                                                                                                                  \
        SetFromItem(transform);                                                                                        \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Eigen::Transform##in_M_Scalar_type##Var::SetFromItem(const value_type &value)                             \
    {                                                                                                                  \
        if(!value.isApprox(m_Value))                                                                                   \
        {                                                                                                              \
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});                  \
            m_Previous = m_Value;                                                                                      \
            m_Value = value;                                                                                           \
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});                    \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    template<>                                                                                                         \
    void ma::T_Var<ma::Eigen::Transform##in_M_Scalar_type>::SetFromItem(                                               \
        const ma::Eigen::Transform##in_M_Scalar_type &value)                                                           \
    {                                                                                                                  \
        if(!value.isApprox(m_Value))                                                                                   \
        {                                                                                                              \
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});                  \
            m_Previous = m_Value;                                                                                      \
            m_Value = value;                                                                                           \
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});                    \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        const TypeInfo &Eigen::Transform##in_M_Scalar_type##Var::GetEigen##Transform##in_M_Scalar_type##VarTypeInfo()  \
        {                                                                                                              \
            static const TypeInfo info = {GetEigen##Transform##in_M_Scalar_type##VarClassName(),                       \
                                          L"Variable d'item de type «"s +                                              \
                                              ma::to_wstring("Transform" #in_M_Scalar_type) + L"».\n"s +               \
                                              L"Le type des scalaires de la transformation est: « "s +                 \
                                              ma::TypeNameW<Transform##in_M_Scalar_type::Scalar>() + L" » .\n"s,       \
                                          {}};                                                                         \
                                                                                                                       \
            return info;                                                                                               \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Transform##in_M_Scalar_type##Var, Eigen, Var)                                  \
    M_CPP_MAKERVAR_TYPE_WITH_MAMESPACE(Transform##in_M_Scalar_type##Var, Eigen)

namespace ma::Eigen
{
    // Il faudrait définir des angles d'Euler entiers à la place des quaternions
    // pour que des transformes entiers existent.
    // typedef ::Eigen::Transform<int,         3u, ::Eigen::TransformTraits::Affine> Transformi;
    // typedef ::Eigen::Transform<unsigned,    3u, ::Eigen::TransformTraits::Affine> Transformu;
    // typedef ::Eigen::Transform<long,        3u, ::Eigen::TransformTraits::Affine> Transforml;
    // typedef ::Eigen::Transform<long long,   3u, ::Eigen::TransformTraits::Affine> Transformll;

    typedef ::Eigen::Transform<float, 3u, ::Eigen::TransformTraits::Affine> Transformf;
    typedef ::Eigen::Transform<double, 3u, ::Eigen::TransformTraits::Affine> Transformd;
    typedef ::Eigen::Transform<long double, 3u, ::Eigen::TransformTraits::Affine> Transformld;
} // namespace ma::Eigen

// EIGEN_VAR_TRANSFORM_HEADER(i)
// EIGEN_VAR_TRANSFORM_HEADER(u)
// EIGEN_VAR_TRANSFORM_HEADER(l)
// EIGEN_VAR_TRANSFORM_HEADER(ll)

EIGEN_VAR_TRANSFORM_HEADER(f)
EIGEN_VAR_TRANSFORM_HEADER(d)
EIGEN_VAR_TRANSFORM_HEADER(ld)
