/// \file eigen/Eigen.h
/// \author Pontier Pierre
/// \date 2023-12-01
/// \brief Inclure ce fichier pour inclure Eigen. L'inclure en dernier. C'est à cause de X11.
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

// https://gitlab.com/libeigen/eigen/-/issues/253
// X.h est constitué de plein de define qui perturbe le fonctionnement des autres librairies.
#ifdef Success
    #undef Success
#endif

#include <Eigen/Dense>
#include <Eigen/Geometry>
