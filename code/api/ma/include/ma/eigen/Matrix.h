/// \file eigen/Matrix.h
/// \author Pontier Pierre
/// \date 2022-04-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// https://eigen.tuxfamily.org/dox/group__QuickRefPage.html
/// https://www.methodemaths.fr/cours_matrices/
///

#pragma once

#include "ma/Dll.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/RTTI.h" // Pour que la macro M_HEADER_MAKERVAR_TYPE fonctionne.

#include "ma/eigen/Eigen.h"

#define EIGEN_VAR_MATRIX_HEADER(in_M_matrix_type)                                                                      \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Eigen                                                                                                \
        {                                                                                                              \
            class M_DLL_EXPORT in_M_matrix_type##Var: public ma::T_Var<ma::Eigen::in_M_matrix_type>                    \
            {                                                                                                          \
                protected:                                                                                             \
                    virtual ma::VarPtr Copy() const override;                                                          \
                                                                                                                       \
                public:                                                                                                \
                    in_M_matrix_type##Var(const ma::Var::ConstructorParameters &params, const value_type &var);        \
                    in_M_matrix_type##Var(const ma::Var::ConstructorParameters &params);                               \
                    in_M_matrix_type##Var() = delete;                                                                  \
                    in_M_matrix_type##Var(const in_M_matrix_type##Var &) = delete;                                     \
                    virtual ~in_M_matrix_type##Var();                                                                  \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_matrix_type##Var, Eigen)                               \
            };                                                                                                         \
            typedef std::shared_ptr<in_M_matrix_type##Var> in_M_matrix_type##VarPtr;                                   \
        }                                                                                                              \
        template M_DLL_EXPORT ma::Eigen::in_M_matrix_type##VarPtr                                                      \
        ma::Item::AddVar<ma::Eigen::in_M_matrix_type##Var>(const ma::Var::key_type &key,                               \
                                                           const ma::Eigen::in_M_matrix_type &value);                  \
        template M_DLL_EXPORT ma::Eigen::in_M_matrix_type##VarPtr                                                      \
        ma::Item::AddVar<ma::Eigen::in_M_matrix_type##Var>(const ma::Var::key_type &key);                              \
    }                                                                                                                  \
    M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(in_M_matrix_type##Var, Eigen)

#define EIGEN_VAR_MATRIX_CPP(in_M_matrix_type)                                                                         \
    ma::Eigen::in_M_matrix_type##Var::in_M_matrix_type##Var(const ma::Var::ConstructorParameters &params,              \
                                                            const value_type &var):                                    \
    ma::T_Var<ma::Eigen::in_M_matrix_type>::T_Var(params, var)                                                         \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::in_M_matrix_type##Var::in_M_matrix_type##Var(const ma::Var::ConstructorParameters &params):             \
    ma::T_Var<ma::Eigen::in_M_matrix_type>::T_Var(params, ma::Eigen::in_M_matrix_type::Identity())                     \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::in_M_matrix_type##Var::~in_M_matrix_type##Var()                                                         \
    {}                                                                                                                 \
                                                                                                                       \
    ma::VarPtr ma::Eigen::in_M_matrix_type##Var::Copy() const                                                          \
    {                                                                                                                  \
        auto ptr =                                                                                                     \
            std::shared_ptr<in_M_matrix_type##Var>(new in_M_matrix_type##Var(GetConstructorParameters(), m_Value));    \
                                                                                                                       \
        ptr->m_Previous = m_Previous;                                                                                  \
        ptr->m_Default = m_Default;                                                                                    \
                                                                                                                       \
        return ptr;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        const TypeInfo &Eigen::in_M_matrix_type##Var::GetEigen##in_M_matrix_type##VarTypeInfo()                        \
        {                                                                                                              \
            static const TypeInfo info = {GetEigen##in_M_matrix_type##VarClassName(),                                  \
                                          L"Variable d'item de type «"s + ma::to_wstring(#in_M_matrix_type) +          \
                                              L"».\n"s + L"Le type des scalaires du quaternion est: « "s +             \
                                              ma::TypeNameW<in_M_matrix_type::Scalar>() + L" » .\n"s,                  \
                                          {}};                                                                         \
                                                                                                                       \
            return info;                                                                                               \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_matrix_type##Var, Eigen, Var)                                             \
    M_CPP_MAKERVAR_TYPE_WITH_MAMESPACE(in_M_matrix_type##Var, Eigen)

namespace ma::Eigen
{
    typedef ::Eigen::Matrix<int, 2, 2> Matrix22i;
    typedef ::Eigen::Matrix<unsigned, 2, 2> Matrix22u;
    typedef ::Eigen::Matrix<long, 2, 2> Matrix22l;
    typedef ::Eigen::Matrix<long long, 2, 2> Matrix22ll;
    typedef ::Eigen::Matrix<float, 2, 2> Matrix22f;
    typedef ::Eigen::Matrix<double, 2, 2> Matrix22d;
    typedef ::Eigen::Matrix<long double, 2, 2> Matrix22ld;

    typedef ::Eigen::Matrix<int, 3, 3> Matrix33i;
    typedef ::Eigen::Matrix<unsigned, 3, 3> Matrix33u;
    typedef ::Eigen::Matrix<long, 3, 3> Matrix33l;
    typedef ::Eigen::Matrix<long long, 3, 3> Matrix33ll;
    typedef ::Eigen::Matrix<float, 3, 3> Matrix33f;
    typedef ::Eigen::Matrix<double, 3, 3> Matrix33d;
    typedef ::Eigen::Matrix<long double, 3, 3> Matrix33ld;

    typedef ::Eigen::Matrix<int, 4, 4> Matrix44i;
    typedef ::Eigen::Matrix<unsigned, 4, 4> Matrix44u;
    typedef ::Eigen::Matrix<long, 4, 4> Matrix44l;
    typedef ::Eigen::Matrix<long long, 4, 4> Matrix44ll;
    typedef ::Eigen::Matrix<float, 4, 4> Matrix44f;
    typedef ::Eigen::Matrix<double, 4, 4> Matrix44d;
    typedef ::Eigen::Matrix<long double, 4, 4> Matrix44ld;
} // namespace ma::Eigen

EIGEN_VAR_MATRIX_HEADER(Matrix22i)
EIGEN_VAR_MATRIX_HEADER(Matrix22u)
EIGEN_VAR_MATRIX_HEADER(Matrix22l)
EIGEN_VAR_MATRIX_HEADER(Matrix22ll)
EIGEN_VAR_MATRIX_HEADER(Matrix22f)
EIGEN_VAR_MATRIX_HEADER(Matrix22d)
EIGEN_VAR_MATRIX_HEADER(Matrix22ld)

EIGEN_VAR_MATRIX_HEADER(Matrix33i)
EIGEN_VAR_MATRIX_HEADER(Matrix33u)
EIGEN_VAR_MATRIX_HEADER(Matrix33l)
EIGEN_VAR_MATRIX_HEADER(Matrix33ll)
EIGEN_VAR_MATRIX_HEADER(Matrix33f)
EIGEN_VAR_MATRIX_HEADER(Matrix33d)
EIGEN_VAR_MATRIX_HEADER(Matrix33ld)

EIGEN_VAR_MATRIX_HEADER(Matrix44i)
EIGEN_VAR_MATRIX_HEADER(Matrix44u)
EIGEN_VAR_MATRIX_HEADER(Matrix44l)
EIGEN_VAR_MATRIX_HEADER(Matrix44ll)
EIGEN_VAR_MATRIX_HEADER(Matrix44f)
EIGEN_VAR_MATRIX_HEADER(Matrix44d)
EIGEN_VAR_MATRIX_HEADER(Matrix44ld)
