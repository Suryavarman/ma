/// \file eigen/Vector.h
/// \author Pontier Pierre
/// \date 2022-04-01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// https://eigen.tuxfamily.org/dox/group__QuickRefPage.html

#pragma once

#include "ma/Dll.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/RTTI.h" // Pour que la macro M_HEADER_MAKERVAR_TYPE fonctionne.

#include "ma/eigen/Eigen.h"

#define EIGEN_VAR_VECTOR_HEADER(in_M_vector_type)                                                                      \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Eigen                                                                                                \
        {                                                                                                              \
            class M_DLL_EXPORT in_M_vector_type##Var: public ma::T_Var<ma::Eigen::in_M_vector_type>                    \
            {                                                                                                          \
                protected:                                                                                             \
                    virtual ma::VarPtr Copy() const override;                                                          \
                                                                                                                       \
                public:                                                                                                \
                    in_M_vector_type##Var(const ma::Var::ConstructorParameters &params, const value_type &var);        \
                    in_M_vector_type##Var(const ma::Var::ConstructorParameters &params);                               \
                    in_M_vector_type##Var() = delete;                                                                  \
                    in_M_vector_type##Var(const in_M_vector_type##Var &) = delete;                                     \
                    virtual ~in_M_vector_type##Var();                                                                  \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_vector_type##Var, Eigen)                               \
            };                                                                                                         \
            typedef std::shared_ptr<in_M_vector_type##Var> in_M_vector_type##VarPtr;                                   \
        }                                                                                                              \
        template M_DLL_EXPORT ma::Eigen::in_M_vector_type##VarPtr                                                      \
        ma::Item::AddVar<ma::Eigen::in_M_vector_type##Var>(const ma::Var::key_type &key,                               \
                                                           const ma::Eigen::in_M_vector_type &value);                  \
        template M_DLL_EXPORT ma::Eigen::in_M_vector_type##VarPtr                                                      \
        ma::Item::AddVar<ma::Eigen::in_M_vector_type##Var>(const ma::Var::key_type &key);                              \
    }                                                                                                                  \
    M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(in_M_vector_type##Var, Eigen)

#define EIGEN_VAR_VECTOR_CPP(in_M_vector_type)                                                                         \
    ma::Eigen::in_M_vector_type##Var::in_M_vector_type##Var(const ma::Var::ConstructorParameters &params,              \
                                                            const value_type &var):                                    \
    ma::T_Var<ma::Eigen::in_M_vector_type>::T_Var(params, var)                                                         \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::in_M_vector_type##Var::in_M_vector_type##Var(const ma::Var::ConstructorParameters &params):             \
    ma::T_Var<ma::Eigen::in_M_vector_type>::T_Var(params, ma::Eigen::in_M_vector_type::Zero())                         \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Eigen::in_M_vector_type##Var::~in_M_vector_type##Var()                                                         \
    {}                                                                                                                 \
                                                                                                                       \
    ma::VarPtr ma::Eigen::in_M_vector_type##Var::Copy() const                                                          \
    {                                                                                                                  \
        auto ptr =                                                                                                     \
            std::shared_ptr<in_M_vector_type##Var>(new in_M_vector_type##Var(GetConstructorParameters(), m_Value));    \
                                                                                                                       \
        ptr->m_Previous = m_Previous;                                                                                  \
        ptr->m_Default = m_Default;                                                                                    \
                                                                                                                       \
        return ptr;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        const TypeInfo &Eigen::in_M_vector_type##Var::GetEigen##in_M_vector_type##VarTypeInfo()                        \
        {                                                                                                              \
            static const TypeInfo info = {GetEigen##in_M_vector_type##VarClassName(),                                  \
                                          L"Variable d'item de type «"s + ma::to_wstring(#in_M_vector_type) +          \
                                              L"».\n"s + L"Le type des scalaires du vecteur est: « "s +                \
                                              ma::TypeNameW<in_M_vector_type::Scalar>() + L" » .\n"s +                 \
                                              L"Le nombre d'éléments du vecteur est de « "s +                          \
                                              ma::toString(in_M_vector_type::SizeAtCompileTime) + L" » .\n"s,          \
                                          {}};                                                                         \
                                                                                                                       \
            return info;                                                                                               \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_vector_type##Var, Eigen, Var)                                             \
    M_CPP_MAKERVAR_TYPE_WITH_MAMESPACE(in_M_vector_type##Var, Eigen)

namespace ma::Eigen
{
    typedef ::Eigen::Matrix<int, 1, 2> Vector2i;
    typedef ::Eigen::Matrix<unsigned, 1, 2> Vector2u;
    typedef ::Eigen::Matrix<long, 1, 2> Vector2l;
    typedef ::Eigen::Matrix<long long, 1, 2> Vector2ll;
    typedef ::Eigen::Matrix<float, 1, 2> Vector2f;
    typedef ::Eigen::Matrix<double, 1, 2> Vector2d;
    typedef ::Eigen::Matrix<long double, 1, 2> Vector2ld;

    typedef ::Eigen::Matrix<int, 1, 3> Vector3i;
    typedef ::Eigen::Matrix<unsigned, 1, 3> Vector3u;
    typedef ::Eigen::Matrix<long, 1, 3> Vector3l;
    typedef ::Eigen::Matrix<long long, 1, 3> Vector3ll;
    typedef ::Eigen::Matrix<float, 1, 3> Vector3f;
    typedef ::Eigen::Matrix<double, 1, 3> Vector3d;
    typedef ::Eigen::Matrix<long double, 1, 3> Vector3ld;

    typedef ::Eigen::Matrix<int, 1, 4> Vector4i;
    typedef ::Eigen::Matrix<unsigned, 1, 4> Vector4u;
    typedef ::Eigen::Matrix<long, 1, 4> Vector4l;
    typedef ::Eigen::Matrix<long long, 1, 4> Vector4ll;
    typedef ::Eigen::Matrix<float, 1, 4> Vector4f;
    typedef ::Eigen::Matrix<double, 1, 4> Vector4d;
    typedef ::Eigen::Matrix<long double, 1, 4> Vector4ld;
} // namespace ma::Eigen

EIGEN_VAR_VECTOR_HEADER(Vector2i)
EIGEN_VAR_VECTOR_HEADER(Vector2u)
EIGEN_VAR_VECTOR_HEADER(Vector2l)
EIGEN_VAR_VECTOR_HEADER(Vector2ll)
EIGEN_VAR_VECTOR_HEADER(Vector2f)
EIGEN_VAR_VECTOR_HEADER(Vector2d)
EIGEN_VAR_VECTOR_HEADER(Vector2ld)

EIGEN_VAR_VECTOR_HEADER(Vector3i)
EIGEN_VAR_VECTOR_HEADER(Vector3u)
EIGEN_VAR_VECTOR_HEADER(Vector3l)
EIGEN_VAR_VECTOR_HEADER(Vector3ll)
EIGEN_VAR_VECTOR_HEADER(Vector3f)
EIGEN_VAR_VECTOR_HEADER(Vector3d)
EIGEN_VAR_VECTOR_HEADER(Vector3ld)

EIGEN_VAR_VECTOR_HEADER(Vector4i)
EIGEN_VAR_VECTOR_HEADER(Vector4u)
EIGEN_VAR_VECTOR_HEADER(Vector4l)
EIGEN_VAR_VECTOR_HEADER(Vector4ll)
EIGEN_VAR_VECTOR_HEADER(Vector4f)
EIGEN_VAR_VECTOR_HEADER(Vector4d)
EIGEN_VAR_VECTOR_HEADER(Vector4ld)
