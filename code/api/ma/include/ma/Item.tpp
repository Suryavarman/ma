/// \file Item.tpp
/// \author Pontier Pierre
/// \date 2019-12-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Error.h"

namespace ma
{
    template<typename T, typename... Args>
    std::shared_ptr<T> Item::CreateRoot(const Args &...args)
    {
        // Il faut faire attention à garder le lien avec les pointeurs
        // intelligent de ma::Item::ms_Map
        T *item = new T({Key::GetNoParent(), Key::GetRoot()}, args...);
        auto key = item->GetKey();
        std::shared_ptr<T> itemPtr = ma::Item::GetItem<T>(key);

        MA_ASSERT(itemPtr,
                  L"Le pointeur partagé possède une référence null. Cela ne devrait pas être le cas. Il devrait "
                  L"posséder item. C'est une erreur interne à l'API.",
                  std::runtime_error);

        MA_ASSERT(itemPtr.get() == item,
                  L"Le pointeur partagé possède une référence différente de item. Cela ne devrait pas être le cas.",
                  std::runtime_error);

        itemPtr->EndConstruction();
        return itemPtr;
    }

    template<typename T, typename... Args>
    std::shared_ptr<T> Item::CreateItem(const ma::Item::CreateParameters &params, const Args &...args)
    {
        // Il faut faire attention à garder le lien avec les pointeurs
        // intelligent de ma::Item::ms_Map
        const ma::Item::ConstructorParameters constructor_parameters{
            params.m_ParentKey, params.m_Key, true, params.m_CanBeDeleteOnLoad};

        T *item = new T(constructor_parameters, args...);

        //        Oui mais non. Ex: les gestionnaires ont leur clef de défini dans leur constructeur.
        //        MA_ASSERT(key == item->GetKey(),
        //                  L"La clef de l'item « " + item->GetKey() +
        //                      L" n'est pas la même que celle passé en paramètre au constructeur.",
        //                  std::logic_error);

        const auto key = item->GetKey();

        ma::ItemPtr itemPtr;

        if(params.m_ParentKey != Key::GetNoParent())
            itemPtr = ma::Item::GetItem(key);
        else
            itemPtr = ma::ItemPtr(item);

        MA_ASSERT(itemPtr,
                  L"Le pointeur partagé possède une référence nulle. Cela ne devrait pas être le cas. "
                  L"Il devrait posséder item. C'est une erreur interne à l'api « ma ».",
                  std::runtime_error);

        MA_ASSERT(itemPtr.get() == item,
                  L"Le pointeur partagé possède une référence différente de item. Cela ne devrait pas être le cas. ",
                  std::runtime_error);

        std::shared_ptr<T> typePtr = std::dynamic_pointer_cast<T>(itemPtr);
        MA_ASSERT(typePtr, L"Impossible de convertir dynamiquement l'item.", std::runtime_error);

        // L'item doit être construit pour que AcceptChild fonctionne.
        if(params.m_ParentKey != Key::GetNoParent())
        {
            auto item_parent = GetItem(params.m_ParentKey);

            MA_ASSERT(item_parent,
                      L"La clef, pour désigner l'item parent, n'est associée à aucun item.",
                      std::invalid_argument);

            item_parent->AddChild(key);
        }

        if(params.m_CallEndConstruction)
        {
            auto parent_key = typePtr->GetParentKey();

            if(parent_key != Key::GetNoParent())
            {
                auto parent = ma::Item::GetItem(typePtr->GetParentKey());
                // On met d'abord à jour le parent.
                parent->UpdateObservations({{Key::Obs::ms_AddItemEnd, typePtr->GetKey()}});
            }

            // Puis, on met à jour l'enfant.
            typePtr->EndConstruction();
            MA_ASSERT(
                typePtr->GetEndConstruction(),
                L"L'élément de type « ma::" + typePtr->GetTypeInfo().m_ClassName +
                    L" » n'a pas finalisé sa construction.\n"
                    L"Vous avez sûrement défini « ma::" +
                    typePtr->GetTypeInfo().m_ClassName +
                    L"::EndConstruction » "
                    L" sans appeler la fonction membre « EndConstruction » de la classe parente.\n"
                    L"La fonction « ma::Item::EndConstruction » doit être appelé pour finaliser la construction de "
                    L"cet élément.",
                std::logic_error);
        }

        return typePtr;
    }

#if MA_COMPILER_CLANG == MA_COMPILER
    template<typename T>
    std::shared_ptr<T> Item::CreateItem(const Item::CreateParameters &params)
    {
        // Il faut faire attention à garder le lien avec les pointeurs intelligent de « ma::Item::ms_Map ».
        T *item = new T({params.m_ParentKey, params.m_Key, true, params.m_CanBeDeleteOnLoad});

        auto key = item->GetKey();

        ItemPtr itemPtr;

        if(params.m_ParentKey != Key::GetNoParent())
            itemPtr = Item::GetItem(key);
        else
            itemPtr = ItemPtr(item);

        MA_ASSERT(itemPtr,
                  L"Le pointeur partagé possède une référence nulle. Cela ne devrait pas être le cas. Il devrait "
                  L"posséder item. C'est une erreur interne à l'API.",
                  std::runtime_error);

        MA_ASSERT(itemPtr.get() == item,
                  L"Le pointeur partagé possède une référence différente de item. Cela ne devrait pas être le cas. ",
                  std::runtime_error);

        std::shared_ptr<T> typePtr = std::dynamic_pointer_cast<T>(itemPtr);
        MA_ASSERT(typePtr, L"Impossible de convertir dynamiquement l'item.", std::runtime_error);

        // L'item doit être construit pour que AcceptChild fonctionne.
        if(params.m_ParentKey != Key::GetNoParent())
        {
            auto item_parent = GetItem(params.m_ParentKey);

            MA_ASSERT(item_parent,
                      L"La clef, pour désigner l'item parent, n'est associée à aucun item.",
                      std::invalid_argument);

            item_parent->AddChild(key);
        }

        if(params.m_CallEndConstruction)
        {
            auto parent_key = typePtr->GetParentKey();

            if(parent_key != Key::GetNoParent())
            {
                auto parent = Item::GetItem(typePtr->GetParentKey());
                // On met d'abord à jour le parent.
                parent->UpdateObservations({{Key::Obs::ms_AddItemEnd, typePtr->GetKey()}});
            }

            // Puis, nous mettons à jour l'enfant.
            typePtr->EndConstruction();
        }

        return typePtr;
    }
#endif // MA_COMPILER_CLANG

    template<typename T, typename... Args>
    std::shared_ptr<T> Item::GetOrCreateItem(const Item::CreateParameters &params, const Args &...args)
    {
        MA_ASSERT(!params.m_Key.empty(),
                  L"« params.m_Key » est vide, la clef de l'item doit être définie, sinon veuillez utiliser "
                  L"« Item::CreateItem ».",
                  std::invalid_argument);

        MA_ASSERT(Id::IsValid(params.m_Key), L"params.m_Key ne représente pas une clef valide.", std::invalid_argument);

        std::shared_ptr<T> item;
        if(auto opt = Item::HasItemOpt(params.m_Key))
        {
            item = std::dynamic_pointer_cast<T>(opt.value());

            MA_ASSERT(item,
                      L"La clef pointe sur un élément qui n'est pas du type « " + ma::TypeNameW<T>() +
                          L" », "
                          L"l'élément existant est du type « " +
                          opt.value()->GetTypeInfo().m_ClassName + L" ».",
                      std::invalid_argument);
        }
        else
            item = Item::CreateItem<T>({params.m_ParentKey, false, true, params.m_Key}, args...);

        return item;
    }

    template<typename T>
    std::shared_ptr<T> Item::GetRoot()
    {
        std::shared_ptr<T> typePtr = std::dynamic_pointer_cast<T>(Item::GetRoot());
        MA_ASSERT(typePtr, L"Impossible de convertir dynamiquement l'item racine.", std::runtime_error);

        return typePtr;
    }

    template<typename T>
    std::shared_ptr<T> Item::GetItem(const key_type &key)
    {
        auto item = Item::GetItem(key);

        std::shared_ptr<T> typePtr = std::dynamic_pointer_cast<T>(item);

        MA_ASSERT(typePtr, L"Impossible de convertir dynamiquement l'item.", std::runtime_error);

        return typePtr;
    }

    template<typename T>
    std::optional<std::shared_ptr<T>> Item::HasItemOpt(const key_type &key)
    {
        lock_guard mutex(ms_MapMutex);
        auto it_find = ms_Map.find(key);
        if(it_find != ms_Map.end())
            return std::dynamic_pointer_cast<T>(it_find->second);
        else
            return {};
    }

    template<typename T>
    std::optional<std::shared_ptr<T>> Item::HasItemOpt(const key_type &key, SearchMod mod) const
    {
        std::optional<std::shared_ptr<T>> result;

        if(mod == Item::SearchMod::GLOBAL)
            result = HasItemOpt<T>(key);
        else
        {
            auto it_find = m_Children.find(key);
            if(it_find != m_Children.end())
            {
                result = std::dynamic_pointer_cast<T>(it_find->second);
            }
            else if(mod == Item::SearchMod::RECURSIVE)
            {
                auto children = GetItems(Item::SearchMod::RECURSIVE);

                auto it_find2 = children.find(key);
                if(it_find2 != children.end())
                    result = std::dynamic_pointer_cast<T>(it_find2->second);
            }
        }

        return result;
    }

    template<typename T>
    std::optional<std::shared_ptr<T>> ma::Item::GetParentOpt() const
    {
        if(m_ParentKey != Key::GetNoParent() && HasItem(m_ParentKey))
        {
            auto item_parent = GetItem(m_ParentKey);

            MA_ASSERT(item_parent->HasItem(GetKey(), SearchMod::LOCAL),
                      L"La clef du parent pointe sur un item ne possédant pas cet enfant.",
                      std::logic_error);

            auto parent = std::dynamic_pointer_cast<T>(item_parent);
            if(parent)
                return parent;
        }

        return {};
    }

    template<typename T>
    std::shared_ptr<T> ma::Item::GetParent() const
    {
        auto item = Item::GetItem(m_ParentKey);

        std::shared_ptr<T> typePtr = std::dynamic_pointer_cast<T>(item);

        MA_ASSERT(typePtr, L"Impossible de convertir dynamiquement l'item.", std::runtime_error);

        return typePtr;
    }

    template<typename T, typename T_ConstructorParameters, typename... Args>
    std::shared_ptr<T> ma::Item::_AddVar(const T_ConstructorParameters &params, const Args &...args)
    {
        // _AddVar est une factorisation, le nom des fonctions appelantes est plus claire.
        // static const std::wstring name1 = L"std::shared_ptr<T> ma::Item::AddVar(const ma::Var::key_type& key, const
        // Args &...args)"; static const std::wstring name2 = L"std::shared_ptr<T> ma::Item::AddMemberVar(const
        // ma::Var::key_type& key, const Args &...args)"; const std::wstring function_name = dynamic ? name1 : name2;

        T *var = new T(params, args...);

        MA_ASSERT(var->m_KeyItem == Key::GetNoParent(),
                  L"La variable est déjà associée à l'item «" + var->m_KeyItem + L"»",
                  std::invalid_argument);

        MA_ASSERT(m_Vars.find(var->m_Key) == m_Vars.end(),
                  L"La clef de variable «" + var->m_Key + L"» est déjà associée à l'item.",
                  std::invalid_argument);

        std::shared_ptr<T> result(var);
        m_Vars[result->m_Key] = result;

        result->m_KeyItem = GetKey();

        if(GetBatchState())
            result->BeginBatch();

        result->EndConstruction();

        // UpdateObservations({std::pair{ms_KeyObsAddVarBegin, GetKey()}});
        UpdateObservations({{L"add_var", params.m_Key}});

        return result;
    }

    template<typename T, typename... Args>
    std::shared_ptr<T> ma::Item::AddVar(const ma::Var::key_type &key, const Args &...args)
    {
        return _AddVar<T>(ma::Var::ConstructorParameters{key, true, false}, args...);
    }

    template<typename T, typename... Args>
    std::shared_ptr<T> ma::Item::AddMemberVar(const ma::Var::key_type &key, const Args &...args)
    {
        return _AddVar<T>(ma::Var::ConstructorParameters{key, false, false}, args...);
    }

    template<typename T, typename... Args>
    std::shared_ptr<T> ma::Item::AddReadOnlyVar(const ma::Var::key_type &key, const Args &...args)
    {
        return _AddVar<T>(ma::Var::ConstructorParameters{key, true, true}, args...);
    }

    template<typename T, typename... Args>
    std::shared_ptr<T> ma::Item::AddReadOnlyMemberVar(const ma::Var::key_type &key, const Args &...args)
    {
        return _AddVar<T>(ma::Var::ConstructorParameters{key, false, true}, args...);
    }

    template<typename T>
    std::shared_ptr<T> ma::Item::GetVar(const ma::Var::key_type &key) const
    {
        auto it_find = m_Vars.find(key);
        MA_ASSERT(it_find != m_Vars.end(), L"La variable n'est pas associée à l'item.", std::invalid_argument);

        std::shared_ptr<T> typePtr = std::dynamic_pointer_cast<T>(it_find->second);

        MA_ASSERT(typePtr, L"Impossible de convertir dynamiquement la variable.", std::invalid_argument);

        return typePtr;
    }

    template<typename T>
    std::optional<std::shared_ptr<T>> ma::Item::HasVarOpt(const ma::Var::key_type &key) const
    {
        auto it_find = m_Vars.find(key);
        if(it_find != m_Vars.end())
        {
            auto typePtr = std::dynamic_pointer_cast<T>(it_find->second);
            MA_ASSERT(typePtr, L"Impossible de convertir dynamiquement la variable.", std::invalid_argument);
            return typePtr;
        }
        else
            return {};
    }

    template<typename T_Var, typename... Args>
    void ma::Item::SetVarFromValue(const VarsMap::key_type &key, const Args &...args)
    {
        auto var = GetVar<T_Var>(key);
        var->SetFromItem(args...);
    }

    template<typename T>
    std::shared_ptr<T> ma::Item::CopyVar(const std::shared_ptr<T> &var)
    {
        // La fonction Copy est généralement protégée.
        // Pour éviter que les variables implémentant la fonction Copy ont à définir « ma::Item » comme ami, nous
        // convertissons la variable en « ma::VarPtr » car elle a défini « ma::Item » comme amie.
        ma::VarPtr new_var = std::static_pointer_cast<ma::Var>(var)->Copy();
        auto result = std::dynamic_pointer_cast<T>(new_var);

        MA_ASSERT(m_Vars.find(result->m_Key) == m_Vars.end(),
                  L"La variable est déjà associée à l'item.",
                  std::invalid_argument);

        m_Vars[result->m_Key] = result;
        result->m_KeyItem = GetKey();

        if(GetBatchState())
            result->BeginBatch();

        result->EndConstruction();

        return result;
    }

    template<typename T>
    std::shared_ptr<T> ma::Item::GetPtr() const
    {
        auto result = std::dynamic_pointer_cast<T>(GetPtr());
        MA_ASSERT(result,
                  L"Cet élément est du type « " + this->GetTypeInfo().m_ClassName +
                      L" » et ne peut être convertit en « " + ma::TypeNameW<T>() + L" ».");
        return result;
    }
} // namespace ma
