/// \file Colour.h
/// \author Pontier Pierre
/// \date 2020-10-25
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/wx/wx.h"
#include "ma/Dll.h"
#include "ma/String.h"
#include "ma/Item.h" // La déclaration de la classe Variable est dans Item.h
#include "ma/Id.h"
#include "ma/Variable.h"
#include "ma/RTTI.h" // Pour que la macro M_HEADER_MAKERVAR_TYPE fonctionne.
#include <regex>

namespace ma
{
    template<typename T, size_t T_ComponentCount, typename T_ComponentType>
    class VarColour: public Var
    {
        protected:
            T m_Value,
                m_Previous, ///< L'ancienne valeur.
                m_Default; ///< La valeur par défaut.

            virtual void SetFromItem(const std::wstring &value) override;
            virtual void SetFromItem(const T &value);
            virtual ma::VarPtr Copy() const override;

        public:
            typedef T value_type;
            typedef T_ComponentType component_type;
            static const size_t m_ComponentCount;

            template<typename... Args>
            VarColour(const ma::Var::ConstructorParameters &params, const Args &...args);
            VarColour(const ma::Var::ConstructorParameters &params);
            VarColour() = delete;
            VarColour(const VarColour &) = delete;
            virtual ~VarColour();

            virtual std::wstring toString() const override;
            virtual std::wstring GetKeyValueToString() const override;

            virtual void fromString(const std::wstring &) override;
            virtual bool IsValidString(const std::wstring &value) const override;

            virtual void Reset() override;

            virtual void Set(const T &value);
            virtual T Get() const;
            virtual T GetPrevious() const;

            virtual VarColour<T, T_ComponentCount, T_ComponentType> &operator=(const value_type &value);
    };

    class M_DLL_EXPORT wxColourVar: public VarColour<wxColour, 4u, unsigned char>
    {
        protected:
            virtual void SetFromItem(const std::wstring &value) override;
            virtual ma::VarPtr Copy() const override;

        public:
            typedef VarColour<wxColour, 4u, unsigned char> VarColourBase;
            explicit wxColourVar(const ma::Var::ConstructorParameters &params);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params, const wxColour &color);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params,
                                 unsigned char red,
                                 unsigned char green,
                                 unsigned char blue,
                                 unsigned char alpha = wxALPHA_OPAQUE);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params,
                                 unsigned int red,
                                 unsigned int green,
                                 unsigned int blue,
                                 unsigned int alpha = wxALPHA_OPAQUE);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params,
                                 int red,
                                 int green,
                                 int blue,
                                 int alpha = wxALPHA_OPAQUE);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params,
                                 unsigned int red,
                                 unsigned int green,
                                 unsigned int blue,
                                 float alpha);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params,
                                 int red,
                                 int green,
                                 int blue,
                                 float alpha);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params, unsigned long colRGB);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params, const wxString &color_name);
            explicit wxColourVar(const ma::Var::ConstructorParameters &params, const std::wstring &color_name);

            virtual ~wxColourVar();

            /// Converti la couleur au format suivant :
            /// «rgb(r,g,b)» ou «rgba(r,g,b,a)»
            virtual std::wstring toString() const override;

            virtual std::wstring GetKeyValueToString() const override;

            /// Accepte les noms de couleur cités ici :
            /// https://docs.wxwidgets.org/3.1/classwx_colour_database.html
            /// Accepte les format suivants:
            /// \li rgb(0, 0, 0)
            /// \li rgba(0, 0, 0, 0)
            /// \li #000000 seulement trois composantes
            virtual void fromString(const std::wstring &) override;

            /// Accepte les noms de couleur cités ici :
            /// https://docs.wxwidgets.org/3.1/classwx_colour_database.html
            /// Accepte les format suivants:
            /// \li rgb(0, 0, 0)
            /// \li rgba(0, 0, 0, 0)
            /// \li #000000 seulement trois composantes
            virtual bool IsValidString(const std::wstring &value) const override;

            virtual void
            Set(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = wxALPHA_OPAQUE);
            virtual void
            Set(unsigned int red, unsigned int green, unsigned int blue, unsigned int alpha = wxALPHA_OPAQUE);
            virtual void Set(int red, int green, int blue, int alpha = wxALPHA_OPAQUE);
            virtual void Set(unsigned int red, unsigned int green, unsigned int blue, float alpha);
            virtual void Set(int red, int green, int blue, float alpha);
            virtual void Set(unsigned long colRGB);
            virtual void Set(const wxString &color_name);
            virtual void Set(const std::wstring &color_name);

            /// Pour éviter que le compilateur veuille utiliser les autres fonctions Set…
            /// Je ne comprend pas pourquoi g++ ne prend pas la fonction Set de la classe parente. Peut être à cause de
            /// l'implémentation de wxColour…
            virtual void Set(const wxColour &color) override;

            M_HEADER_CLASSHIERARCHY(wxColourVar)
    };

    typedef std::shared_ptr<ma::wxColourVar> wxColourVarPtr;

    template M_DLL_EXPORT wxColourVarPtr ma::Item::AddVar<ma::wxColourVar>(const ma::Var::key_type &key);
    template M_DLL_EXPORT wxColourVarPtr ma::Item::AddVar<ma::wxColourVar>(const ma::Var::key_type &key,
                                                                           const unsigned char &red,
                                                                           const unsigned char &green,
                                                                           const unsigned char &blue,
                                                                           const unsigned char &alpha = wxALPHA_OPAQUE);
    template M_DLL_EXPORT wxColourVarPtr ma::Item::AddVar<ma::wxColourVar>(const ma::Var::key_type &key,
                                                                           const wxColour &value);
    template M_DLL_EXPORT wxColourVarPtr ma::Item::AddVar<ma::wxColourVar>(const ma::Var::key_type &key,
                                                                           const unsigned long &colRGB);
    template M_DLL_EXPORT wxColourVarPtr ma::Item::AddVar<ma::wxColourVar>(const ma::Var::key_type &key,
                                                                           const wxString &color_name);
    template M_DLL_EXPORT wxColourVarPtr ma::Item::AddVar<ma::wxColourVar>(const ma::Var::key_type &key,
                                                                           const std::wstring &color_name);
} // namespace ma

#include "ma/Colour.tpp"

M_HEADER_MAKERVAR_TYPE(wxColourVar)
