/// \file RTTI.h
/// \author Pontier Pierre
/// \date 2019-12-10
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Prerequisites.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/VariableEnum.h"
#include "ma/Resource.h"
#include "ma/wx/PanelItem.h"
#include "ma/Dll.h"
#include <memory>

#define M_HEADER_MAKERITEM_TYPE(in_M_type)                                                                             \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        class M_DLL_EXPORT in_M_type##Maker: public MakerItem                                                          \
        {                                                                                                              \
            private:                                                                                                   \
                static unsigned int &GetCounterRef();                                                                  \
                                                                                                                       \
            public:                                                                                                    \
                typedef in_M_type value_type;                                                                          \
                explicit in_M_type##Maker(const ConstructorParameters &in_params);                                     \
                in_M_type##Maker() = delete;                                                                           \
                ~in_M_type##Maker() override;                                                                          \
                ItemPtr CreateItem(const CreateParameters &params) const final;                                        \
                const TypeInfo &GetFactoryTypeInfo() const override;                                                   \
                static std::wstring GetClassName();                                                                    \
                                                                                                                       \
                M_HEADER_CLASSHIERARCHY(in_M_type##Maker)                                                              \
        };                                                                                                             \
        typedef std::shared_ptr<in_M_type##Maker> in_M_type##MakerPtr;                                                 \
    }

#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(in_M_type, in_M_NameSpace)                                          \
        namespace ma                                                                                                   \
        {                                                                                                              \
            namespace in_M_NameSpace                                                                                   \
            {                                                                                                          \
                class M_DLL_EXPORT in_M_type##Maker: public ma::MakerItem                                              \
                {                                                                                                      \
                    private:                                                                                           \
                        static unsigned int &GetCounterRef();                                                          \
                                                                                                                       \
                    public:                                                                                            \
                        typedef ma::in_M_NameSpace::in_M_type value_type;                                              \
                        explicit in_M_type##Maker(const ConstructorParameters &in_params);                             \
                        in_M_type##Maker() = delete;                                                                   \
                        ~in_M_type##Maker() override;                                                                  \
                        ma::ItemPtr CreateItem(const CreateParameters &params) const final;                            \
                        const ma::TypeInfo &GetFactoryTypeInfo() const override;                                       \
                        static std::wstring GetClassName();                                                            \
                                                                                                                       \
                        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace)                       \
                };                                                                                                     \
                typedef std::shared_ptr<ma::in_M_NameSpace::in_M_type##Maker> in_M_type##MakerPtr;                     \
            }                                                                                                          \
        }
#else
    #define M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(in_M_type, in_M_NameSpace)                                          \
        namespace ma                                                                                                   \
        {                                                                                                              \
            namespace in_M_NameSpace                                                                                   \
            {                                                                                                          \
                class M_DLL_EXPORT in_M_type##Maker: public ma::MakerItem                                              \
                {                                                                                                      \
                    private:                                                                                           \
                        static unsigned int &GetCounterRef();                                                          \
                                                                                                                       \
                    public:                                                                                            \
                        typedef ma::in_M_NameSpace::in_M_type value_type;                                              \
                        explicit in_M_type##Maker(const ConstructorParameters &in_params);                             \
                        in_M_type##Maker() = delete;                                                                   \
                        ~in_M_type##Maker() override;                                                                  \
                        ma::ItemPtr CreateItem(const CreateParameters &params = CreateParameters()) const final;       \
                        const ma::TypeInfo &GetFactoryTypeInfo() const override;                                       \
                        static std::wstring GetClassName();                                                            \
                                                                                                                       \
                        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace)                       \
                };                                                                                                     \
                typedef std::shared_ptr<ma::in_M_NameSpace::in_M_type##Maker> in_M_type##MakerPtr;                     \
            }                                                                                                          \
        }
#endif

#define M_CPP_MAKERITEM_TYPE(in_M_type)                                                                                \
    unsigned int &ma::in_M_type##Maker::GetCounterRef()                                                                \
    {                                                                                                                  \
        static unsigned int ref = 0;                                                                                   \
        return ref;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    ma::in_M_type##Maker::in_M_type##Maker(const ConstructorParameters &in_params):                                    \
    ma::MakerItem(in_params, in_M_type##Maker::GetClassName())                                                         \
    {                                                                                                                  \
        static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                                   \
        static const std::wstring function_name =                                                                      \
            type_name + L"::" + type_name + L"(const key_type &in_key, const key_type &parent_key)";                   \
                                                                                                                       \
        auto &ref = GetCounterRef();                                                                                   \
        ref++;                                                                                                         \
        MA_ASSERT(ref == 1u,                                                                                           \
                  L"Plus d'une instance de " + in_M_type##Maker::GetClassName() + L" ont été instanciée.",             \
                  function_name,                                                                                       \
                  std::logic_error);                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    ma::in_M_type##Maker::~in_M_type##Maker()                                                                          \
    {                                                                                                                  \
        std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                                                \
        std::wstring function_name = type_name + L"::~" + type_name + L"()";                                           \
                                                                                                                       \
        auto &ref = GetCounterRef();                                                                                   \
        ref--;                                                                                                         \
        MA_ASSERT(ref == 0,                                                                                            \
                  L"Plus d'une instance de " + in_M_type##Maker::GetClassName() + L" ont été instanciée.",             \
                  function_name,                                                                                       \
                  std::logic_error);                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    ma::ItemPtr ma::in_M_type##Maker::CreateItem(const CreateParameters &params) const                                 \
    {                                                                                                                  \
        return ma::Item::CreateItem<in_M_type>(params);                                                                \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_type##Maker::GetFactoryTypeInfo() const                                               \
    {                                                                                                                  \
        return ma::in_M_type::Get##in_M_type##TypeInfo();                                                              \
    }                                                                                                                  \
                                                                                                                       \
    std::wstring ma::in_M_type##Maker::GetClassName()                                                                  \
    {                                                                                                                  \
        return ma::to_wstring(#in_M_type);                                                                             \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_type##Maker::Get##in_M_type##MakerTypeInfo()                                          \
    {                                                                                                                  \
        static const ma::TypeInfo type_info = {ma::to_wstring(#in_M_type) + L"Maker",                                  \
                                               L"A partir de la chaîne de caractère «" + ma::to_wstring(#in_M_type) +  \
                                                   L"» cette fabrique conçoit des items de type " +                    \
                                                   ma::to_wstring(#in_M_type),                                         \
                                               {}};                                                                    \
        return type_info;                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY(in_M_type##Maker, MakerItem)

#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(in_M_type, in_M_NameSpace)                                             \
        unsigned int &ma::in_M_NameSpace::in_M_type##Maker::GetCounterRef()                                            \
        {                                                                                                              \
            static unsigned int ref = 0;                                                                               \
            return ref;                                                                                                \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::in_M_type##Maker(const ConstructorParameters &in_params):                \
        ma::MakerItem(in_params, ma::in_M_NameSpace::in_M_type##Maker::GetClassName())                                 \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                               \
            static const std::wstring function_name = ma::to_wstring("ma::" #in_M_NameSpace "::") + type_name +        \
                                                      L"::"s + type_name +                                             \
                                                      L"(const key_type &in_key, const key_type &parent_key)"s;        \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref++;                                                                                                     \
            MA_ASSERT(ref == 1u,                                                                                       \
                      L"Plus d'une instance de "s + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +             \
                          L" ont été instanciée."s,                                                                    \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::~in_M_type##Maker()                                                      \
        {                                                                                                              \
            std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker"s;                                           \
            std::wstring function_name =                                                                               \
                ma::to_wstring("ma::" #in_M_NameSpace "::") + type_name + L"::~"s + type_name + L"()"s;                \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref--;                                                                                                     \
            MA_ASSERT(ref == 0,                                                                                        \
                      L"Plus d'une instance de "s + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +             \
                          L" ont été instanciée."s,                                                                    \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::ItemPtr ma::in_M_NameSpace::in_M_type##Maker::CreateItem(const ma::Item::CreateParameters &params) const   \
        {                                                                                                              \
            return ma::Item::CreateItem<ma::in_M_NameSpace::in_M_type>(params);                                        \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::GetFactoryTypeInfo() const                           \
        {                                                                                                              \
            return ma::in_M_NameSpace::in_M_type::Get##in_M_NameSpace##in_M_type##TypeInfo();                          \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_NameSpace::in_M_type##Maker::GetClassName()                                              \
        {                                                                                                              \
            return ma::to_wstring(#in_M_NameSpace "::" #in_M_type);                                                    \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::Get##in_M_NameSpace##in_M_type##MakerTypeInfo()      \
        {                                                                                                              \
            static const ma::TypeInfo type_info = {                                                                    \
                ma::to_wstring(#in_M_NameSpace "::" #in_M_type) + L"Maker"s,                                           \
                L"A partir de la chaîne de caractère «"s + ma::to_wstring(#in_M_NameSpace "::" #in_M_type) +           \
                    L"» cette fabrique conçoit des items de type "s + ma::to_wstring(#in_M_NameSpace "::" #in_M_type), \
                {}};                                                                                                   \
            return type_info;                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace, MakerItem)
#else
    #define M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(in_M_type, in_M_NameSpace)                                             \
        unsigned int &ma::in_M_NameSpace::in_M_type##Maker::GetCounterRef()                                            \
        {                                                                                                              \
            static unsigned int ref = 0;                                                                               \
            return ref;                                                                                                \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::in_M_type##Maker(const ConstructorParameters &in_params):                \
        ma::MakerItem(in_params, ma::in_M_NameSpace::in_M_type##Maker::GetClassName())                                 \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                               \
            static const std::wstring function_name = ma::to_wstring("API" #in_M_NameSpace "::") + type_name + L"::" + \
                                                      type_name +                                                      \
                                                      L"(const key_type &in_key, const key_type &parent_key)";         \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref++;                                                                                                     \
            MA_ASSERT(ref == 1u,                                                                                       \
                      L"Plus d'une instance de " + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +              \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::~in_M_type##Maker()                                                      \
        {                                                                                                              \
            std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                                            \
            std::wstring function_name =                                                                               \
                ma::to_wstring("API" #in_M_NameSpace "::") + type_name + L"::~" + type_name + L"()";                   \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref--;                                                                                                     \
            MA_ASSERT(ref == 0,                                                                                        \
                      L"Plus d'une instance de " + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +              \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::ItemPtr ma::in_M_NameSpace::in_M_type##Maker::CreateItem(const ma::Item::CreateParameters &params) const   \
        {                                                                                                              \
            return ma::Item::CreateItem<ma::in_M_NameSpace::in_M_type>(params);                                        \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::GetFactoryTypeInfo() const                           \
        {                                                                                                              \
            return ma::in_M_NameSpace::in_M_type::Get##in_M_NameSpace####in_M_type##TypeInfo();                        \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_NameSpace::in_M_type##Maker::GetClassName()                                              \
        {                                                                                                              \
            return ma::to_wstring(#in_M_NameSpace "::" #in_M_type);                                                    \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::Get##in_M_NameSpace####in_M_type##MakerTypeInfo()    \
        {                                                                                                              \
            static const ma::TypeInfo type_info = {                                                                    \
                ma::to_wstring(#in_M_NameSpace "::" #in_M_type) + L"Maker"s,                                           \
                L"A partir de la chaîne de caractère «"s + ma::to_wstring(#in_M_NameSpace "::" #in_M_type) +           \
                    L"» cette fabrique conçoit des items de type "s + ma::to_wstring(#in_M_NameSpace "::" #in_M_type), \
                {}};                                                                                                   \
            return type_info;                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace, MakerItem)
#endif // MA_COMPILER_CLANG

#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(in_M_type, in_M_NameSpace)                                           \
        namespace ma::in_M_NameSpace                                                                                   \
        {                                                                                                              \
            class M_DLL_EXPORT in_M_type##Maker: public ma::MakerVar                                                   \
            {                                                                                                          \
                private:                                                                                               \
                    static unsigned int &GetCounterRef();                                                              \
                                                                                                                       \
                public:                                                                                                \
                    typedef ma::in_M_NameSpace::in_M_type value_type;                                                  \
                                                                                                                       \
                    explicit in_M_type##Maker(const ConstructorParameters &in_params);                                 \
                    in_M_type##Maker() = delete;                                                                       \
                    virtual ~in_M_type##Maker();                                                                       \
                    virtual ma::VarPtr CreateVar(const ma::Var::key_type &var_key,                                     \
                                                 const key_type &item_key) const override;                             \
                                                                                                                       \
                    const ma::TypeInfo &GetFactoryTypeInfo() const override;                                           \
                    static std::wstring GetClassName();                                                                \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace)                           \
            };                                                                                                         \
            typedef std::shared_ptr<ma::in_M_NameSpace::in_M_type##Maker> in_M_type##MakerVarPtr;                      \
        }
#else
    #define M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(in_M_type, in_M_NameSpace)                                           \
        namespace ma::in_M_NameSpace                                                                                   \
        {                                                                                                              \
            class M_DLL_EXPORT in_M_type##Maker: public ma::MakerVar                                                   \
            {                                                                                                          \
                private:                                                                                               \
                    static unsigned int &GetCounterRef();                                                              \
                                                                                                                       \
                public:                                                                                                \
                    typedef ma::in_M_NameSpace::in_M_type value_type;                                                  \
                    explicit in_M_type##Maker(const ConstructorParameters &in_params);                                 \
                    in_M_type##Maker() = delete;                                                                       \
                    virtual ~in_M_type##Maker();                                                                       \
                    virtual ma::VarPtr CreateVar(const ma::Var::key_type &var_key,                                     \
                                                 const key_type &item_key) const override;                             \
                                                                                                                       \
                    const ma::TypeInfo &GetFactoryTypeInfo() const override;                                           \
                    static std::wstring GetClassName();                                                                \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace)                           \
            };                                                                                                         \
            typedef std::shared_ptr<ma::in_M_NameSpace::in_M_type##Maker> in_M_type##MakerVarPtr;                      \
        }
#endif // MA_COMPILER_CLANG

#define M_HEADER_MAKERVAR_TYPE(in_M_type)                                                                              \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        class M_DLL_EXPORT in_M_type##Maker: public MakerVar                                                           \
        {                                                                                                              \
            private:                                                                                                   \
                static unsigned int &GetCounterRef();                                                                  \
                                                                                                                       \
            public:                                                                                                    \
                typedef in_M_type value_type;                                                                          \
                explicit in_M_type##Maker(const ConstructorParameters &in_params);                                     \
                in_M_type##Maker() = delete;                                                                           \
                virtual ~in_M_type##Maker();                                                                           \
                virtual VarPtr CreateVar(const Var::key_type &var_key, const key_type &item_key) const override;       \
                                                                                                                       \
                const TypeInfo &GetFactoryTypeInfo() const override;                                                   \
                static std::wstring GetClassName();                                                                    \
                                                                                                                       \
                M_HEADER_CLASSHIERARCHY(in_M_type##Maker)                                                              \
        };                                                                                                             \
        typedef std::shared_ptr<in_M_type##Maker> in_M_type##MakerVarPtr;                                              \
    }

#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_CPP_MAKERVAR_TYPE_WITH_MAMESPACE(in_M_type, in_M_NameSpace)                                              \
        unsigned int &ma::in_M_NameSpace::in_M_type##Maker::GetCounterRef()                                            \
        {                                                                                                              \
            static unsigned int ref = 0;                                                                               \
            return ref;                                                                                                \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::in_M_type##Maker(const ConstructorParameters &in_params):                \
        ma::MakerVar(in_params, ma::in_M_NameSpace::in_M_type##Maker::GetClassName())                                  \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                               \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace) + type_name + L"::~" + type_name + L"()";                       \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref++;                                                                                                     \
            MA_ASSERT(ref == 1u,                                                                                       \
                      L"Plus d'une instance de " + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +              \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::~in_M_type##Maker()                                                      \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                               \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace) + type_name + L"::~" + type_name + L"()";                       \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref--;                                                                                                     \
            MA_ASSERT(ref == 0,                                                                                        \
                      L"Plus d'une instance de " + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +              \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::VarPtr ma::in_M_NameSpace::in_M_type##Maker::CreateVar(const ma::Var::key_type &var_key,                   \
                                                                   const key_type &item_key) const                     \
        {                                                                                                              \
            auto item = ma::Item::GetItem(item_key);                                                                   \
            return item->AddVar<ma::in_M_NameSpace::in_M_type>(var_key);                                               \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::GetFactoryTypeInfo() const                           \
        {                                                                                                              \
            return ma::in_M_NameSpace::in_M_type::Get##in_M_NameSpace##in_M_type##TypeInfo();                          \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_NameSpace::in_M_type##Maker::GetClassName()                                              \
        {                                                                                                              \
            return ma::to_wstring(#in_M_NameSpace "::" #in_M_type);                                                    \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::Get##in_M_NameSpace##in_M_type##MakerTypeInfo()      \
        {                                                                                                              \
            static const ma::TypeInfo type_info = {ma::to_wstring(#in_M_NameSpace "::" #in_M_type) + L"Maker",         \
                                                   L"A partir de la chaîne de caractère «" +                           \
                                                       ma::to_wstring(#in_M_NameSpace "::" #in_M_type) +               \
                                                       L"» cette fabrique conçoit des variables de type " +            \
                                                       ma::to_wstring(#in_M_NameSpace "::" #in_M_type),                \
                                                   {}};                                                                \
            return type_info;                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace, MakerVar)
#else
    #define M_CPP_MAKERVAR_TYPE_WITH_MAMESPACE(in_M_type, in_M_NameSpace)                                              \
        unsigned int &ma::in_M_NameSpace::in_M_type##Maker::GetCounterRef()                                            \
        {                                                                                                              \
            static unsigned int ref = 0;                                                                               \
            return ref;                                                                                                \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::in_M_type##Maker(const ConstructorParameters &in_params):                \
        ma::MakerVar(in_params, ma::in_M_NameSpace::in_M_type##Maker::GetClassName())                                  \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                               \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace) + type_name + L"::~" + type_name + L"()";                       \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref++;                                                                                                     \
            MA_ASSERT(ref == 1u,                                                                                       \
                      L"Plus d'une instance de " + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +              \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::in_M_NameSpace::in_M_type##Maker::~in_M_type##Maker()                                                      \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                               \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace) + type_name + L"::~" + type_name + L"()";                       \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref--;                                                                                                     \
            MA_ASSERT(ref == 0,                                                                                        \
                      L"Plus d'une instance de " + ma::in_M_NameSpace::in_M_type##Maker::GetClassName() +              \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        ma::VarPtr ma::in_M_NameSpace::in_M_type##Maker::CreateVar(const ma::Var::key_type &var_key,                   \
                                                                   const key_type &item_key) const                     \
        {                                                                                                              \
            auto item = ma::Item::GetItem(item_key);                                                                   \
            return item->AddVar<ma::in_M_NameSpace::in_M_type>(var_key);                                               \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::GetFactoryTypeInfo() const                           \
        {                                                                                                              \
            return ma::in_M_NameSpace::in_M_type::Get##in_M_NameSpace####in_M_type##TypeInfo();                        \
        }                                                                                                              \
                                                                                                                       \
        std::wstring ma::in_M_NameSpace::in_M_type##Maker::GetClassName()                                              \
        {                                                                                                              \
            return ma::to_wstring(#in_M_NameSpace "::" #in_M_type);                                                    \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &ma::in_M_NameSpace::in_M_type##Maker::Get##in_M_NameSpace####in_M_type##MakerTypeInfo()    \
        {                                                                                                              \
            static const ma::TypeInfo type_info = {ma::to_wstring(#in_M_NameSpace "::" #in_M_type) + L"Maker",         \
                                                   L"A partir de la chaîne de caractère «" +                           \
                                                       ma::to_wstring(#in_M_NameSpace "::" #in_M_type) +               \
                                                       L"» cette fabrique conçoit des variables de type " +            \
                                                       ma::to_wstring(#in_M_NameSpace "::" #in_M_type),                \
                                                   {}};                                                                \
            return type_info;                                                                                          \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_type##Maker, in_M_NameSpace, MakerVar)
#endif

#define M_CPP_MAKERVAR_TYPE(in_M_type)                                                                                 \
    unsigned int &ma::in_M_type##Maker::GetCounterRef()                                                                \
    {                                                                                                                  \
        static unsigned int ref = 0;                                                                                   \
        return ref;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    ma::in_M_type##Maker::in_M_type##Maker(const ConstructorParameters &in_params):                                    \
    ma::MakerVar(in_params, in_M_type##Maker::GetClassName())                                                          \
    {                                                                                                                  \
        static const std::wstring type_name = ma::in_M_type::Get##in_M_type##ClassName();                              \
        static const std::wstring function_name =                                                                      \
            type_name + L"::" + type_name + L"(const key_type &in_key, const key_type &parent_key)";                   \
                                                                                                                       \
        auto &ref = GetCounterRef();                                                                                   \
        ref++;                                                                                                         \
        MA_ASSERT(ref == 1u,                                                                                           \
                  L"Plus d'une instance de " + in_M_type##Maker::GetClassName() + L" ont été instanciée.",             \
                  function_name,                                                                                       \
                  std::logic_error);                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    ma::in_M_type##Maker::~in_M_type##Maker()                                                                          \
    {                                                                                                                  \
        static const std::wstring type_name = ma::to_wstring(#in_M_type) + L"Maker";                                   \
        static const std::wstring function_name = type_name + L"::~" + type_name + L"()";                              \
                                                                                                                       \
        auto &ref = GetCounterRef();                                                                                   \
        ref--;                                                                                                         \
        MA_ASSERT(ref == 0,                                                                                            \
                  L"Plus d'une instance de " + ma::in_M_type##Maker::GetClassName() + L" ont été instanciée.",         \
                  function_name,                                                                                       \
                  std::logic_error);                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    ma::VarPtr ma::in_M_type##Maker::CreateVar(const ma::Var::key_type &var_key, const key_type &item_key) const       \
    {                                                                                                                  \
        auto item = ma::Item::GetItem(item_key);                                                                       \
        return item->AddVar<in_M_type>(var_key);                                                                       \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_type##Maker::GetFactoryTypeInfo() const                                               \
    {                                                                                                                  \
        return ma::in_M_type::Get##in_M_type##TypeInfo();                                                              \
    }                                                                                                                  \
                                                                                                                       \
    std::wstring ma::in_M_type##Maker::GetClassName()                                                                  \
    {                                                                                                                  \
        return ma::to_wstring(#in_M_type);                                                                             \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_type##Maker::Get##in_M_type##MakerTypeInfo()                                          \
    {                                                                                                                  \
        static const ma::TypeInfo type_info = {ma::to_wstring(#in_M_type) + L"Maker",                                  \
                                               L"A partir de la chaîne de caractère «" + ma::to_wstring(#in_M_type) +  \
                                                   L"» cette fabrique conçoit des variables de type " +                \
                                                   ma::to_wstring(#in_M_type),                                         \
                                               {}};                                                                    \
        return type_info;                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY(in_M_type##Maker, MakerVar)

// todo déplacer les macros dédié à wxwWidgets dans le sous-dossier wx
#if(MA_COMPILER == MA_COMPILER_CLANG)
    /// À déclarer dans la portée de l'espace de nom « ma::in_M_NameSpace ».
    #define M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(in_M_wx_var_type, in_M_NameSpace)                                  \
        class M_DLL_EXPORT in_M_wx_var_type##Maker: public ma::MakerwxVar                                              \
        {                                                                                                              \
            private:                                                                                                   \
                static unsigned int &GetCounterRef();                                                                  \
                                                                                                                       \
            public:                                                                                                    \
                typedef ma::in_M_NameSpace::in_M_wx_var_type value_type;                                               \
                explicit in_M_wx_var_type##Maker(const ConstructorParameters &in_params);                              \
                in_M_wx_var_type##Maker() = delete;                                                                    \
                ~in_M_wx_var_type##Maker() override;                                                                   \
                wxWindow *CreatewxVar(const ma::Var::key_type &var_key,                                                \
                                      const key_type &item_key,                                                        \
                                      wxWindow *parent) const override;                                                \
                                                                                                                       \
                const ma::TypeInfo &GetFactoryTypeInfo() const override;                                               \
                                                                                                                       \
                std::wstring GetFabricVarClassName() const override;                                                   \
                static std::wstring Get##in_M_NameSpace##in_M_wx_var_type##FabricVarClassName();                       \
                                                                                                                       \
                M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_wx_var_type##Maker, in_M_NameSpace)                        \
        };                                                                                                             \
        typedef std::shared_ptr<ma::in_M_NameSpace::in_M_wx_var_type##Maker> in_M_wx_var_type##MakerVarPtr;
#else
    #define M_HEADER_MAKERWXVAR_TYPE_WITH_NAMESPACE(in_M_wx_var_type, in_M_NameSpace)                                  \
        class M_DLL_EXPORT in_M_wx_var_type##Maker: public ma::MakerwxVar                                              \
        {                                                                                                              \
            private:                                                                                                   \
                static unsigned int &GetCounterRef();                                                                  \
                                                                                                                       \
            public:                                                                                                    \
                typedef in_M_wx_var_type value_type;                                                                   \
                explicit in_M_wx_var_type##Maker(const ConstructorParameters &in_params);                              \
                in_M_wx_var_type##Maker() = delete;                                                                    \
                ~in_M_wx_var_type##Maker() override;                                                                   \
                wxWindow *CreatewxVar(const ma::Var::key_type &var_key,                                                \
                                      const key_type &item_key,                                                        \
                                      wxWindow *parent) const override;                                                \
                                                                                                                       \
                const ma::TypeInfo &GetFactoryTypeInfo() const override;                                               \
                                                                                                                       \
                virtual std::wstring GetFabricVarClassName() const override;                                           \
                static std::wstring Get##in_M_NameSpace####in_M_wx_var_type##FabricVarClassName();                     \
                                                                                                                       \
                M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(in_M_wx_var_type##Maker, in_M_NameSpace)                        \
        };                                                                                                             \
        typedef std::shared_ptr<ma::in_M_NameSpace::in_M_wx_var_type##Maker> in_M_wx_var_type##MakerVarPtr;
#endif // MA_COMPILER_CLANG

#define M_HEADER_MAKERWXVAR_TYPE(in_M_wx_var_type)                                                                     \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        class M_DLL_EXPORT in_M_wx_var_type##Maker: public ma::MakerwxVar                                              \
        {                                                                                                              \
            private:                                                                                                   \
                static unsigned int &GetCounterRef();                                                                  \
                                                                                                                       \
            public:                                                                                                    \
                typedef in_M_wx_var_type value_type;                                                                   \
                explicit in_M_wx_var_type##Maker(const ConstructorParameters &in_params);                              \
                in_M_wx_var_type##Maker() = delete;                                                                    \
                virtual ~in_M_wx_var_type##Maker();                                                                    \
                virtual wxWindow *CreatewxVar(const ma::Var::key_type &var_key,                                        \
                                              const key_type &item_key,                                                \
                                              wxWindow *parent) const override;                                        \
                                                                                                                       \
                const ma::TypeInfo &GetFactoryTypeInfo() const override;                                               \
                                                                                                                       \
                virtual std::wstring GetFabricVarClassName() const override;                                           \
                static std::wstring Get##in_M_wx_var_type##FabricVarClassName();                                       \
                                                                                                                       \
                M_HEADER_CLASSHIERARCHY(in_M_wx_var_type##Maker)                                                       \
        };                                                                                                             \
        typedef std::shared_ptr<ma::in_M_wx_var_type##Maker> in_M_wx_var_type##MakerVarPtr;                            \
    }

#if(MA_COMPILER == MA_COMPILER_CLANG)
    /// À utiliser dans la portée de l'espace de nom « ma::in_M_NameSpace ».
    #define M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(in_M_wx_var_type, in_M_var_type, in_M_NameSpace)                      \
        unsigned int &in_M_wx_var_type##Maker::GetCounterRef()                                                         \
        {                                                                                                              \
            static unsigned int ref = 0;                                                                               \
            return ref;                                                                                                \
        }                                                                                                              \
                                                                                                                       \
        in_M_wx_var_type##Maker::in_M_wx_var_type##Maker(const ConstructorParameters &in_params):                      \
        MakerwxVar(in_params, ma::to_wstring(#in_M_var_type))                                                          \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_wx_var_type) + L"Maker";                        \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace "::") + type_name + L"::~" + type_name + L"()";                  \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref++;                                                                                                     \
            MA_ASSERT(ref == 1u,                                                                                       \
                      L"Plus d'une instance de " + ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) +            \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        in_M_wx_var_type##Maker::~in_M_wx_var_type##Maker()                                                            \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_wx_var_type) + L"Maker";                        \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace "::") + type_name + L"::~" + type_name + L"()";                  \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref--;                                                                                                     \
            MA_ASSERT(ref == 0,                                                                                        \
                      L"Plus d'une instance de " + ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) +            \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        wxWindow *in_M_wx_var_type##Maker::CreatewxVar(                                                                \
            const ma::Var::key_type &var_key, const key_type &item_key, wxWindow *parent) const                        \
        {                                                                                                              \
            auto item = ma::Item::GetItem(item_key);                                                                   \
            in_M_wx_var_type::ptr_type var =                                                                           \
                std::dynamic_pointer_cast<ma::in_M_NameSpace::in_M_wx_var_type::value_type>(item->GetVar(var_key));    \
            return new in_M_wx_var_type(var, parent);                                                                  \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &in_M_wx_var_type##Maker::GetFactoryTypeInfo() const                                        \
        {                                                                                                              \
            return ma::in_M_NameSpace::in_M_wx_var_type::Get##in_M_NameSpace##in_M_wx_var_type##TypeInfo();            \
        }                                                                                                              \
                                                                                                                       \
        std::wstring in_M_wx_var_type##Maker::GetFabricVarClassName() const                                            \
        {                                                                                                              \
            return ma::to_wstring(#in_M_var_type);                                                                     \
        }                                                                                                              \
                                                                                                                       \
        std::wstring in_M_wx_var_type##Maker::Get##in_M_NameSpace##in_M_wx_var_type##FabricVarClassName()              \
        {                                                                                                              \
            return ma::to_wstring(#in_M_NameSpace #in_M_var_type);                                                     \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &in_M_wx_var_type##Maker::Get##in_M_NameSpace##in_M_wx_var_type##MakerTypeInfo()            \
        {                                                                                                              \
            static const ma::TypeInfo result = {ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) + L"Maker",     \
                                                L"A partir de la chaîne de caractères «" +                             \
                                                    ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) +           \
                                                    L"» cette fabrique conçoit des wxVar de type " +                   \
                                                    ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type),            \
                                                {}};                                                                   \
            return result;                                                                                             \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_wx_var_type##Maker, in_M_NameSpace, MakerwxVar)
#else
    #define M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(in_M_wx_var_type, in_M_var_type, in_M_NameSpace)                      \
        unsigned int &in_M_wx_var_type##Maker::GetCounterRef()                                                         \
        {                                                                                                              \
            static unsigned int ref = 0;                                                                               \
            return ref;                                                                                                \
        }                                                                                                              \
                                                                                                                       \
        in_M_wx_var_type##Maker::in_M_wx_var_type##Maker(const ConstructorParameters &in_params):                      \
        MakerwxVar(in_params, ma::to_wstring(#in_M_var_type))                                                          \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_wx_var_type) + L"Maker";                        \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace "::") + type_name + L"::~" + type_name + L"()";                  \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref++;                                                                                                     \
            MA_ASSERT(ref == 1u,                                                                                       \
                      L"Plus d'une instance de " + ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) +            \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        in_M_wx_var_type##Maker::~in_M_wx_var_type##Maker()                                                            \
        {                                                                                                              \
            static const std::wstring type_name = ma::to_wstring(#in_M_wx_var_type) + L"Maker";                        \
            static const std::wstring function_name =                                                                  \
                ma::to_wstring("ma::" #in_M_NameSpace "::") + type_name + L"::~" + type_name + L"()";                  \
                                                                                                                       \
            auto &ref = GetCounterRef();                                                                               \
            ref--;                                                                                                     \
            MA_ASSERT(ref == 0,                                                                                        \
                      L"Plus d'une instance de " + ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) +            \
                          L" ont été instanciée.",                                                                     \
                      function_name,                                                                                   \
                      std::logic_error);                                                                               \
        }                                                                                                              \
                                                                                                                       \
        wxWindow *in_M_wx_var_type##Maker::CreatewxVar(                                                                \
            const ma::Var::key_type &var_key, const key_type &item_key, wxWindow *parent) const                        \
        {                                                                                                              \
            auto item = ma::Item::GetItem(item_key);                                                                   \
            in_M_wx_var_type::ptr_type var =                                                                           \
                std::dynamic_pointer_cast<ma::in_M_NameSpace::in_M_wx_var_type::value_type>(item->GetVar(var_key));    \
            return new in_M_wx_var_type(var, parent);                                                                  \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &in_M_wx_var_type##Maker::GetFactoryTypeInfo() const                                        \
        {                                                                                                              \
            return ma::in_M_NameSpace::in_M_wx_var_type::Get##in_M_NameSpace####in_M_wx_var_type##TypeInfo();          \
        }                                                                                                              \
                                                                                                                       \
        std::wstring in_M_wx_var_type##Maker::GetFabricVarClassName() const                                            \
        {                                                                                                              \
            return ma::to_wstring(#in_M_var_type);                                                                     \
        }                                                                                                              \
                                                                                                                       \
        std::wstring in_M_wx_var_type##Maker::Get##in_M_NameSpace####in_M_wx_var_type##FabricVarClassName()            \
        {                                                                                                              \
            return ma::to_wstring(#in_M_NameSpace #in_M_var_type);                                                     \
        }                                                                                                              \
                                                                                                                       \
        const ma::TypeInfo &in_M_wx_var_type##Maker::Get##in_M_NameSpace####in_M_wx_var_type##MakerTypeInfo()          \
        {                                                                                                              \
            static const ma::TypeInfo result = {ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) + L"Maker",     \
                                                L"A partir de la chaîne de caractères «" +                             \
                                                    ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type) +           \
                                                    L"» cette fabrique conçoit des wxVar de type " +                   \
                                                    ma::to_wstring(#in_M_NameSpace "::" #in_M_wx_var_type),            \
                                                {}};                                                                   \
            return result;                                                                                             \
        }                                                                                                              \
                                                                                                                       \
        M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(in_M_wx_var_type##Maker, in_M_NameSpace, MakerwxVar)
#endif // MA_COMPILER_CLANG

#define M_CPP_MAKERWXVAR_TYPE(in_M_wx_var_type, in_M_var_type)                                                         \
    unsigned int &ma::in_M_wx_var_type##Maker::GetCounterRef()                                                         \
    {                                                                                                                  \
        static unsigned int ref = 0;                                                                                   \
        return ref;                                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    ma::in_M_wx_var_type##Maker::in_M_wx_var_type##Maker(const ConstructorParameters &in_params):                      \
    ma::MakerwxVar(in_params, ma::to_wstring(#in_M_var_type))                                                          \
    {                                                                                                                  \
        static const std::wstring type_name = in_M_wx_var_type##Maker::ms_##in_M_wx_var_type##Info.m_ClassName;        \
        static const std::wstring function_name =                                                                      \
            type_name + L"::" + type_name + L"(const key_type &in_key, const key_type &parent_key)";                   \
                                                                                                                       \
        auto &ref = GetCounterRef();                                                                                   \
        ref++;                                                                                                         \
        MA_ASSERT(ref == 1u,                                                                                           \
                  L"Plus d'une instance de " + ma::to_wstring(#in_M_wx_var_type) + L" ont été instanciée.",            \
                  function_name,                                                                                       \
                  std::logic_error);                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    ma::in_M_wx_var_type##Maker::~in_M_wx_var_type##Maker()                                                            \
    {                                                                                                                  \
        static const std::wstring type_name = ma::to_wstring(#in_M_wx_var_type) + L"Maker";                            \
        static const std::wstring function_name = type_name + L"::~" + type_name + L"()";                              \
                                                                                                                       \
        auto &ref = GetCounterRef();                                                                                   \
        ref--;                                                                                                         \
        MA_ASSERT(ref == 0,                                                                                            \
                  L"Plus d'une instance de " + ma::to_wstring(#in_M_wx_var_type) + L" ont été instanciée.",            \
                  function_name,                                                                                       \
                  std::logic_error);                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    wxWindow *ma::in_M_wx_var_type##Maker::CreatewxVar(                                                                \
        const ma::Var::key_type &var_key, const key_type &item_key, wxWindow *parent) const                            \
    {                                                                                                                  \
        auto item = ma::Item::GetItem(item_key);                                                                       \
        in_M_wx_var_type::ptr_type var =                                                                               \
            std::dynamic_pointer_cast<in_M_wx_var_type::value_type>(item->GetVar(var_key));                            \
        return new in_M_wx_var_type(var, parent);                                                                      \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_wx_var_type##Maker::GetFactoryTypeInfo() const                                        \
    {                                                                                                                  \
        return in_M_wx_var_type::Get##in_M_wx_var_type##TypeInfo();                                                    \
    }                                                                                                                  \
                                                                                                                       \
    std::wstring ma::in_M_wx_var_type##Maker::GetFabricVarClassName() const                                            \
    {                                                                                                                  \
        return ma::to_wstring(#in_M_var_type);                                                                         \
    }                                                                                                                  \
                                                                                                                       \
    std::wstring ma::in_M_wx_var_type##Maker::Get##in_M_wx_var_type##FabricVarClassName()                              \
    {                                                                                                                  \
        return ma::to_wstring(#in_M_var_type);                                                                         \
    }                                                                                                                  \
                                                                                                                       \
    const ma::TypeInfo &ma::in_M_wx_var_type##Maker::Get##in_M_wx_var_type##MakerTypeInfo()                            \
    {                                                                                                                  \
        static const ma::TypeInfo result = {                                                                           \
            ma::to_wstring(#in_M_wx_var_type) + L"Maker",                                                              \
            L"A partir de la chaîne de caractère «" + ma::to_wstring(#in_M_wx_var_type) +                              \
                L"» cette fabrique conçoit des wxVar de type " + ma::to_wstring(#in_M_wx_var_type),                    \
            {}};                                                                                                       \
        return result;                                                                                                 \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY(in_M_wx_var_type##Maker, MakerwxVar)

namespace ma
{
    class MakerItem;
    class MakerVar;
    class MakerwxVar;
    typedef std::shared_ptr<MakerItem> MakerItemPtr;
    typedef std::shared_ptr<MakerVar> MakerVarPtr;
    typedef std::shared_ptr<MakerwxVar> MakerwxVarPtr;
    typedef std::unordered_map<std::wstring, MakerItemPtr> MakerItemsMap;
    typedef std::unordered_map<std::wstring, MakerVarPtr> MakerVarsMap;
    typedef std::unordered_map<std::wstring, MakerwxVarPtr> MakerwxVarsMap;

    class M_DLL_EXPORT RTTI: public ma::Item
    {
        public:
            // friend std::shared_ptr<ma::RTTI> ma::Item::CreateItem(const key_type &parent_key);
            // https://en.cppreference.com/w/cpp/language/using_declaration
            // https://stackoverflow.com/questions/8093882/using-c-base-class-constructors
            /// En public afin que « ma::Item::CreateItem » puisse créer la RTTI.
            using ma::Item::Item;

            explicit RTTI(const Item::ConstructorParameters &in_params);

            ~RTTI() override;
            ItemPtr CreateItem(const std::wstring &class_name,
                               const CreateParameters &params = CreateParameters()) const;

            VarPtr
            CreateVar(const std::wstring &class_name, const ma::Var::key_type &var_key, const key_type &item_key) const;

            /// « class_name » est le type de la variable à afficher.
            /// \return La fenêtre qui observera la variable de type class_name
            wxWindow *CreatewxVar(const ma::Var::key_type &var_key, const key_type &item_key, wxWindow *parent) const;

            /// \return Renvoie un dictionnaire où :
            /// \li la clef représente le nom de la classe héritant d'Item.
            /// \li la valeur la fabrique qui génère des « Items » du type « nom de la classe ».
            ma::MakerItemsMap GetRegisteredMakerItems() const;

            /// \return Renvoie un dictionnaire où :
            /// \li la clef représente le nom de la classe héritant de Var.
            /// \li la valeur la fabrique qui génère des « Var » du type « nom de la classe ».
            ma::MakerVarsMap GetRegisteredMakerVars() const;

            /// \return Renvoie un dictionnaire où :
            /// \li la clef représente le nom de la classe héritant de Var.
            /// \li la valeur la fabrique qui génère des « wxVar » du type « nom de la classe ».
            ma::MakerwxVarsMap GetRegisteredMakerwxVars() const;

            /// \return Tous les fabriques produisant des éléments héritant du type « class_name ».
            [[maybe_unused]]
            ma::MakerItemsMap GetRegisteredMakerItems(const std::wstring &class_name) const;

            /// \return Tous les fabriques produisant des variables héritant du type « class_name ».
            [[maybe_unused]]
            ma::MakerVarsMap GetRegisteredMakerVars(const std::wstring &class_name) const;

            /// \return Tous les fabriques produisant des variables du type wxVar héritant du type « class_name ».
            [[maybe_unused]]
            ma::MakerwxVarsMap GetRegisteredMakerwxVars(const std::wstring &class_name) const;

            M_HEADER_CLASSHIERARCHY(RTTI)
    };
    typedef std::shared_ptr<RTTI> RTTIPtr;

    class M_DLL_EXPORT MakerItem: public ma::Item
    {
        protected:
            using ma::Item::Item;
            explicit MakerItem(const Item::ConstructorParameters &in_params, const std::wstring &class_name);

        public:
            struct Key
            {
                    /// Clef pour accéder au nom de la classe
                    static const ma::Var::key_type &GetClassNameItemProperty();
            };

            /// Variable contenant le nom de la classe. La variable est
            /// identifiée par ms_KeyClassNameProperty.
            const ma::StringVarPtr m_ClassName;

            virtual ma::ItemPtr CreateItem(const CreateParameters &params) const = 0;

            ~MakerItem() override;

            virtual const ma::TypeInfo &GetFactoryTypeInfo() const = 0;

            M_HEADER_CLASSHIERARCHY(MakerItem)
    };

    class M_DLL_EXPORT MakerVar: public ma::Item
    {
        protected:
            using ma::Item::Item;
            explicit MakerVar(const Item::ConstructorParameters &in_params, const std::wstring &class_name);

        public:
            struct Key
            {
                    /// Clef pour accéder au nom de la classe
                    static const ma::Var::key_type &GetClassNameVarProperty();
            };

            /// Variable contenant le nom de la classe. La variable est
            /// identifiée par ms_ClassNameVarProperty.
            const ma::StringVarPtr m_ClassName;

            virtual ma::VarPtr CreateVar(const ma::Var::key_type &var_key, const key_type &item_key) const = 0;

            ~MakerVar() override;

            virtual const ma::TypeInfo &GetFactoryTypeInfo() const = 0;

            M_HEADER_CLASSHIERARCHY(MakerVar)
    };

    /// \remarks La classe ma::wx::Var doit définir:
    /// \li typedef T value_type;
    /// \li typedef std::shared_ptr<T> ptr_type;
    /// « T » est un type héritant de « ma::Var ».
    /// Le constructeur doit avoir pour premier paramètre : « ptr_type var » et pour
    /// second: « wxWindow* parent », les suivants doivent avoir des valeurs par défaut.
    class M_DLL_EXPORT MakerwxVar: public ma::Item
    {
        protected:
            using ma::Item::Item;
            explicit MakerwxVar(const Item::ConstructorParameters &in_params, const std::wstring &class_name);

        public:
            struct Key
            {
                    /// Clef pour accéder au nom de la classe
                    static const ma::Var::key_type &GetClassNamewxVarProperty();
            };

            /// Variable contenant le nom de la classe.
            /// La variable est identifiée par « ms_ClassNamewxVarProperty ».
            const ma::StringVarPtr m_ClassName;

            virtual wxWindow *
            CreatewxVar(const ma::Var::key_type &var_key, const key_type &item_key, wxWindow *parent) const = 0;

            ~MakerwxVar() override;

            virtual const ma::TypeInfo &GetFactoryTypeInfo() const = 0;
            virtual std::wstring GetFabricVarClassName() const = 0;

            M_HEADER_CLASSHIERARCHY(MakerwxVar)
    };
} // namespace ma

M_HEADER_MAKERITEM_TYPE(Item)
M_HEADER_MAKERITEM_TYPE(Folder)

M_HEADER_MAKERVAR_TYPE(DefineVar)
M_HEADER_MAKERVAR_TYPE(StringVar)
M_HEADER_MAKERVAR_TYPE(PathVar)
M_HEADER_MAKERVAR_TYPE(DirectoryVar)
M_HEADER_MAKERVAR_TYPE(FilePathVar)
M_HEADER_MAKERVAR_TYPE(BoolVar)
M_HEADER_MAKERVAR_TYPE(StringHashSetVar)
M_HEADER_MAKERVAR_TYPE(StringSetVar)
M_HEADER_MAKERVAR_TYPE(ItemHashSetVar)
M_HEADER_MAKERVAR_TYPE(ItemVectorVar)
M_HEADER_MAKERVAR_TYPE(ItemVar)
M_HEADER_MAKERVAR_TYPE(IntVar)
M_HEADER_MAKERVAR_TYPE(LongVar)
M_HEADER_MAKERVAR_TYPE(LongLongVar)
M_HEADER_MAKERVAR_TYPE(UVar)
M_HEADER_MAKERVAR_TYPE(ULongVar)
M_HEADER_MAKERVAR_TYPE(ULongLongVar)
M_HEADER_MAKERVAR_TYPE(FloatVar)
M_HEADER_MAKERVAR_TYPE(DoubleVar)
M_HEADER_MAKERVAR_TYPE(LongDoubleVar)
M_HEADER_MAKERVAR_TYPE(EnumerationVar)
M_HEADER_MAKERVAR_TYPE(EnumerationChoiceVar)
