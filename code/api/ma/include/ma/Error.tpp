/// \file Error.tpp
/// \author Pontier Pierre
/// \date 2019-12-06
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \see https://www.tutorialspoint.com/what-s-the-difference-between-pretty-function-function-func-in-c-cplusplus

#include <iostream>

namespace ma
{
    template<typename T>
    void Assert(bool test,
                const std::wstring &msg,
                const std::wstring &function_name,
                const std::wstring &filename,
                const unsigned int &line)
    {
        if(!test)
        {
#if !defined(M_NOGUI)

            ma::wx::Assert(msg, function_name, filename, line);
            ma::wx::AssertTextBegin();
#endif

            std::wstringstream ss;
            ss << L"Erreur levée: " << '\n';
            ss << L"Message: " << msg << '\n';
            ss << L"Fonction: " << function_name << '\n';
            ss << L"Fichier: " << filename << '\n';
            ss << L"Ligne: " << line << std::endl;

            if(ma::g_ConsoleVerbose > 0)
            {
                // Pour être sur d'avoir un message affiché.
                // \todo A voir si il y a mieux que cette possible redondance.
                // ou moyen de faire une asse
                // J'utilise cout et non wcout pour avoir les messages qui
                // s'affichent correctement dans la console, mais logiquement
                // cette solution est hasardeuse.
                std::cout << ma::to_string(ss.str()) << std::endl;
            }

#if !defined(M_NOGUI)
            ma::wx::AssertTextEnd();
#endif

            throw T(to_string(ss.str()));
        }
    }
} // namespace ma
