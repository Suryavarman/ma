/// \file Enable.h
/// \author Pontier Pierre
/// \date 2023-05-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"

namespace ma
{
    /// Classe de base qui définie le comportement d'un objet qui s'active et se désactive.
    /// \remarks Par défaut l'instance n'est pas active.
    class M_DLL_EXPORT Enable
    {
        private:
            /// Par défaut est à faux.
            bool m_Enable;

        public:
            /// Constructeur par défaut.
            Enable();

            /// Destructeur par défaut.
            virtual ~Enable();

            bool IsEnable() const;

        protected:
            virtual void OnEnable();
            virtual void OnDisable();
    };
} // namespace ma
