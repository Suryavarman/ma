/// \file Observer.h
/// \author Pontier Pierre
/// \date 2020-01-05
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Prerequisites.h"
#include "ma/Enable.h"
#include "ma/TypeInfo.h"
#include "ma/Id.h"
#include "ma/Dll.h"

namespace ma
{
    class Observable;

    typedef std::shared_ptr<Observable> ObservablePtr;

    class M_DLL_EXPORT Observable: virtual public Enable, public std::enable_shared_from_this<ma::Observable>
    {
        public:
            typedef std::wstring key_type;

            /// {identifiant de l'action, identifiant de l'observable}
            /// \example L'identifiant de l'action, la clef de la variable dont la valeur associée a changé.
            ///          {ma::Var::ms_KeyObsBeginSetValue, var->GetKey() + ma::Var::ms_Sep + var->toString() }
            ///          Ce qui donne :
            ///          {"var_set_value", "m_Name,0"}
            /// \example L'identifiant de l'action, la clef de l'item qui a été ajouté à l'item parent.
            ///          {ma::Item::ms_KeyObsAddItemEnd, item->GetKey()}
            ///          Ce qui donne
            ///          {"add_item_end", "7bcd757f-5b10-4f9b-af69-1a1f226f3b3e"}
            typedef std::pair<std::wstring, std::wstring> Action;

            /// \example {{"var_set_value", "m_Name,0"}, {"var_set_value", "m_Name,1"}, {"add_item_end",
            /// "00000000-0000-0000-0000-000000000004"}, {"add_item_end", "7bcd757f-5b10-4f9b-af69-1a1f226f3b3e"}}
            typedef std::vector<Action> DataOrdered;

            /// \example {{"add_item_end", {"00000000-0000-0000-0000-000000000004",
            /// "7bcd757f-5b10-4f9b-af69-1a1f226f3b3e"}}, {"var_set_value", "m_Name,0"}}
            typedef std::unordered_map<std::wstring, std::vector<std::wstring>> DataUnordered;

            /// Le problème d'utiliser un « std::unordered_multimap » c'est que dans le cas d'actions groupées, nous ne
            /// connaîtrons pas l'ordre des actions.
            /// https://en.cppreference.com/w/cpp/container/unordered_multimap/deduction_guides
            /// http://www.cplusplus.com/reference/unordered_map/unordered_multimap/equal_range/
            /// Accéder via une clef pour questionner les actions à entreprendre sans pour autant boucler sur toutes les
            /// données du Data est pratique.
            /// Mais pour chaque clef beaucoup d'observateurs n'auront besoin que de la dernière clef pour une action
            /// spécifiée.
            class Data
            {
                public:
                    typedef Action::first_type key_type;

                private:
                    DataOrdered m_Ordered;
                    DataUnordered m_Unordered;

                public:
                    Data(std::initializer_list<DataOrdered::value_type> init);
                    explicit Data(const DataOrdered &);
                    Data();
                    ~Data();

                    const DataOrdered &GetOrdered() const;
                    const DataUnordered &GetUnordered() const;

                    bool HasActions(const key_type &) const;
                    Action::second_type GetLastAction(const key_type &) const;
                    Action::second_type GetFirstAction(const key_type &) const;
                    DataUnordered::mapped_type GetActions(const key_type &) const;
                    DataUnordered::mapped_type::size_type GetCount(const key_type &) const;

                    void SetAction(const Action &);
                    void Clear();
                    DataUnordered::size_type GetSize() const;
                    void Merge(const Data &);
                    std::wstring toString() const;
            };

            // Chaque observable qui a fini de se construire notifie
            // l'évènement via cette clef.
            // static const Data::key_type ms_KeyObsConstructionEnd;

            /// Lorsqu'un observable se construit, il faut attendre la fin de sa construction.
            /// Pour éviter les problèmes d'observation avant la fin de la construction, par défaut l'observable
            /// accumule les mises à jour. \see EndConstruction()
            Observable();
            ~Observable() override;

            /// Un observable doit être associé à une clef.
            /// \todo Voir une optimisation avec une string_view et/ou une référence ?
            virtual key_type GetKey() const = 0;

            /// Permet de savoir si un Observable est observable.
            /// Comme les observables sont gérés par des « std::shared_ptr », il est nécessaire de savoir si
            /// l'observable est actif ou pas. Il y a aussi le cas des objets qui ne peuvent être observé.
            /// Par exemple une « ma::Observation » n'est pas observable sinon le nombre d'observations serait infini.
            /// \remarks Attention dans votre surcharge de IsObservable n'oubliez pas de prendre en compte le résultat
            /// de la fonction « GetEndConstruction() ».
            virtual bool IsObservable() const;

            /// Cette fonction permet de notifier ses observateurs d'un changement.
            /// \param data Permet de donner des informations sur le changement.
            /// \example
            /// \code {.cpp}
            /// this->UpdateObservations({"ADD_ITEM", new_item1->GetKey()}, {"ADD_ITEM", new_item2->GetKey()} );
            /// \endcode
            /// Cet exemple montre un cas où il y a eu plusieurs ajouts d'item.
            /// Cet exemple ne pourrait arriver avec le fonctionnement de base des Items où seul un élément peut être
            /// ajouté à la fois.
            /// \remarks Si ma::Item::ms_KeyObservationManager n'est pas présent dans ma::Item::ms_Map alors
            /// UpdateObservations ne fera rien. Si l'observable n'est pas observable alors la fonction ne fera rien.
            virtual void UpdateObservations(const Data &data);

            /// Une fois BeginBatch() appelé la mise à jour sera bloquée jusqu'à l'appel de EndBatch()
            virtual void BeginBatch();

            /// Peu importe le nombre de BeginBatch au premier EndBatch ça met à jour.
            /// \remarks Sauf si EndConstruction() a été appelé.
            /// Du coup, ça sera l'appel de « EndConstruction() » qui enclenchera les mises à jour.
            virtual void EndBatch();

            virtual bool GetBatchState();

            /// Permet au Manager d'observation d'indiquer à l'observable qu'il est observé.
            /// Fonctionnement interne à la mécanique d'observation.
            virtual void SetObserve(bool);

            /// \return Vrais si l'observable est observé.
            bool IsObserve() const;

            /// Permet de savoir si l'item a fini sa construction.
            bool GetEndConstruction() const;

        protected:
            /// \brief Fonction interne qui permet à l'observable d'avoir un pointeur partagé de son instance.
            virtual ObservablePtr GetPtr() const = 0;

            virtual void SetNeedUpdate(bool);

            /// \brief Une fois l'observable construit, cette fonction doit être appelée pour débloquer l'observation de
            /// cette instance.
            virtual void EndConstruction();

        private:
            /// Par défaut, elle est vrais pour permettre à l'observable de finir de se construire.
            bool m_EndConstruction;

            /// Tant que m_BeginBatch est vrais la mise à jour ne se fera pas.
            bool m_BeginBatch;
            bool m_NeedUpdate;
            bool m_IsObserve;

            /// Dans le cas d'une action groupée m_CurrentData permet de stocker les donner de chaque observation.
            Data m_CurrentData;

        public:
            M_HEADER_CLASSHIERARCHY_NO_OVERRIDE(Observable)
    };

    /// \brief Fait le lien entre une Frame, un Panel, un Item, une variable…
    /// tout objet souhaitant observer un observable.
    /// L'observateur surcharger
    /// \remarks ...
    class M_DLL_EXPORT Observer: virtual public Enable
    {
        public:
            typedef Id::key_type key_type;

        private:
            Id m_ObserverKey;

            /// \brief Permet de savoir si l'objet est en destruction.
            /// Cela permet d'éviter les appels à UpdateObservation.
            bool m_DestroyCall;

        public:
            Observer();
            ~Observer() override;

            /// \brief L'observation d'un observable débute.
            virtual void BeginObservation(const ObservablePtr &observable) = 0;

            /// \brief L'observable a été modifié, cette fonction est donc appelée avec les informations caractérisant
            /// la ou les modifications appliquées à l'observable.
            virtual void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) = 0;

            /// \brief L'observable vient de se détruire, il faut implémenter la réaction de l'observateur.
            virtual void EndObservation(const ObservablePtr &) = 0;

            virtual ma::InstanceInfo GetInstanceInfo() const;

            /// \brief Un observateur ne garantit pas que son pointeur reste valide après la fin d'une observation.
            /// Le gestionnaire d'observations peut avoirs besoin d'identifier les observations pour les nettoyer.
            key_type GetObserverKey() const;

            bool GetDestroyCall() const;

            void OnEnable() override;
            void OnDisable() override;

            M_HEADER_CLASSHIERARCHY_NO_OVERRIDE(Observer)
    };
    typedef Observer *ObserverPtr;
} // namespace ma
