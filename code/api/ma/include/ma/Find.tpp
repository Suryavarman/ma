/// \file Find.tpp
/// \author Pontier Pierre
/// \date 2020-03-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <utility>

template<typename T, typename T_Item>
ma::Match<T, T_Item>::~Match()
{}

template<typename T_Item>
ma::MatchType<T_Item>::MatchType(): ma::Match<ma::Item::key_type, T_Item>()
{}

template<typename T_Item>
bool ma::MatchType<T_Item>::operator()(std::shared_ptr<T_Item> &item) const
{
    return true;
}

template<typename T_Item>
ma::Item::key_type ma::MatchType<T_Item>::GetValue(std::shared_ptr<T_Item> &item) const
{
    return item->GetKey();
}

template<typename T_Item>
ma::MatchType<T_Item>::~MatchType()
{}

template<typename T_Var, typename T_Item>
ma::MatchVar<T_Var, T_Item>::MatchVar(ma::Var::key_type key):
ma::Match<typename T_Var::value_type, T_Item>(),
m_Key(std::move(key))
{}

template<typename T_Var, typename T_Item>
bool ma::MatchVar<T_Var, T_Item>::operator()(std::shared_ptr<T_Item> &item) const
{
    return item->HasVar(m_Key) && std::dynamic_pointer_cast<T_Var>(item->GetVar(m_Key)) != nullptr;
}

template<typename T_Var, typename T_Item>
typename T_Var::value_type ma::MatchVar<T_Var, T_Item>::GetValue(std::shared_ptr<T_Item> &item) const
{
    auto var = item->ma::Item::template GetVar<T_Var>(m_Key);
    return var->Get();
}

template<typename T_Var, typename T_Item>
ma::MatchVar<T_Var, T_Item>::~MatchVar()
{}

template<typename T_Match>
typename ma::Find<T_Match>::deque
ma::Find<T_Match>::GetDeque(const ma::Item::key_type &key_parent, const T_Match &match, ma::Item::SearchMod search_mod)
{
    auto item_parent = ma::Item::GetItem(key_parent);
    auto items = item_parent->GetItems(search_mod);

    typename ma::Find<T_Match>::deque result;
    for(auto it : items)
    {
        auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);
        if(item && match(item))
            result.push_back(item);
    }

    return result;
}

template<typename T_Match>
typename ma::Find<T_Match>::deque
ma::Find<T_Match>::GetDeque(ma::Item *parent, const T_Match &match, ma::Item::SearchMod search_mod)
{
    MA_ASSERT(parent, L"L'item « parent » pointe sur nul.", std::invalid_argument);

    auto items = parent->GetItems(search_mod);

    typename ma::Find<T_Match>::deque result;
    for(auto it : items)
    {
        auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);
        if(item && match(item))
            result.push_back(item);
    }

    return result;
}

template<typename T_Match>
typename ma::Find<T_Match>::set
ma::Find<T_Match>::GetSet(const ma::Item::key_type &key_parent, const T_Match &match, ma::Item::SearchMod search_mod)
{
    auto item_parent = ma::Item::GetItem(key_parent);
    auto items = item_parent->GetItems(search_mod);

    typename ma::Find<T_Match>::set result;
    for(auto it : items)
    {
        auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);
        if(item && match(item))
            result.insert(item);
    }

    return result;
}

template<typename T_Match>
typename ma::Find<T_Match>::set
ma::Find<T_Match>::GetSet(ma::Item *parent, const T_Match &match, ma::Item::SearchMod search_mod)
{
    MA_ASSERT(parent, L"L'item « parent » pointe sur nul.", std::invalid_argument);

    auto items = parent->GetItems(search_mod);

    typename ma::Find<T_Match>::set result;
    for(auto it : items)
    {
        auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);
        if(item && match(item))
            result.insert(item);
    }

    return result;
}

template<typename T_Match>
typename ma::Find<T_Match>::unordered_set ma::Find<T_Match>::GetHashSet(const ma::Item::key_type &key_parent,
                                                                  const T_Match &match,
                                                                  ma::Item::SearchMod search_mod)
{
    auto item_parent = ma::Item::GetItem(key_parent);
    auto items = item_parent->GetItems(search_mod);

    typename ma::Find<T_Match>::unordered_set result;
    for(auto it : items)
    {
        auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);
        if(item && match(item))
            result.insert(item);
    }

    return result;
}

template<typename T_Match>
typename ma::Find<T_Match>::unordered_set
ma::Find<T_Match>::GetHashSet(ma::Item *parent, const T_Match &match, ma::Item::SearchMod search_mod)
{
    MA_ASSERT(parent, L"L'item « parent » pointe sur nul.", std::invalid_argument);

    auto items = parent->GetItems(search_mod);

    typename ma::Find<T_Match>::unordered_set result;
    for(auto it : items)
    {
        auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);
        if(item && match(item))
            result.insert(item);
    }

    return result;
}

#ifndef M_DEFINE_GET_CONTAINER
    #define M_DEFINE_GET_CONTAINER(in_m_container_type, in_m_function_name)                                            \
        template<typename T_Match>                                                                                     \
        typename ma::Find<T_Match>::in_m_container_type ma::Find<T_Match>::in_m_function_name(                         \
            const ma::Item::key_type &key_parent, const T_Match &match, ma::Item::SearchMod search_mod)                \
        {                                                                                                              \
            auto item_parent = ma::Item::GetItem(key_parent);                                                          \
            auto items = item_parent->GetItems(search_mod);                                                            \
                                                                                                                       \
            typename ma::Find<T_Match>::in_m_container_type result;                                                    \
            for(auto &it : items)                                                                                      \
            {                                                                                                          \
                auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);                         \
                if(item && match(item))                                                                                \
                    result.insert({match.GetValue(item), item});                                                       \
            }                                                                                                          \
                                                                                                                       \
            return result;                                                                                             \
        }                                                                                                              \
                                                                                                                       \
        template<typename T_Match>                                                                                     \
        typename ma::Find<T_Match>::in_m_container_type ma::Find<T_Match>::in_m_function_name(                         \
            ma::Item *parent, const T_Match &match, ma::Item::SearchMod search_mod)                                    \
        {                                                                                                              \
            MA_ASSERT(parent, L"L'item « parent » pointe sur nul.", std::invalid_argument);                            \
            auto items = parent->GetItems(search_mod);                                                                 \
                                                                                                                       \
            typename ma::Find<T_Match>::in_m_container_type result;                                                    \
            for(auto &it : items)                                                                                      \
            {                                                                                                          \
                auto item = std::dynamic_pointer_cast<typename T_Match::item_type>(it.second);                         \
                if(item && match(item))                                                                                \
                    result.insert({match.GetValue(item), item});                                                       \
            }                                                                                                          \
                                                                                                                       \
            return result;                                                                                             \
        }
#endif // M_DEFINE_GET_CONTAINER

M_DEFINE_GET_CONTAINER(map, GetMap)
M_DEFINE_GET_CONTAINER(multimap, GetMultiMap)
M_DEFINE_GET_CONTAINER(unordered_map, GetHashMap)
M_DEFINE_GET_CONTAINER(unordered_multimap, GetHashMultiMap)
