/// \file Version.h
/// \author Pontier Pierre
/// \date 2020-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// https://blog.conan.io/2019/09/02/Deterministic-builds-with-C-C++.html
/// https://stackoverflow.com/questions/11697820/how-to-use-date-and-time-predefined-macros-in-as-two-integers-then-stri
/// https://askcodez.com/comment-gerer-le-numero-de-version-de-git.html
/// https://en.wikipedia.org/wiki/Software_versioning
///

#pragma once

#include "ma/String.h"
#include "ma/Dll.h"

namespace ma
{
/// \brief La date de compilation du code. ex: Jul 27 2022
/// \hideinitializer
#define MA_DATE __DATE__

#define COMPUTE_BUILD_DAY (((__DATE__[4] >= '0') ? (__DATE__[4] - '0') * 10 : 0) + (__DATE__[5] - '0'))

#define BUILD_MONTH_IS_JAN (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_FEB (__DATE__[0] == 'F')
#define BUILD_MONTH_IS_MAR (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
#define BUILD_MONTH_IS_APR (__DATE__[0] == 'A' && __DATE__[1] == 'p')
#define BUILD_MONTH_IS_MAY (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
#define BUILD_MONTH_IS_JUN (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_JUL (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
#define BUILD_MONTH_IS_AUG (__DATE__[0] == 'A' && __DATE__[1] == 'u')
#define BUILD_MONTH_IS_SEP (__DATE__[0] == 'S')
#define BUILD_MONTH_IS_OCT (__DATE__[0] == 'O')
#define BUILD_MONTH_IS_NOV (__DATE__[0] == 'N')
#define BUILD_MONTH_IS_DEC (__DATE__[0] == 'D')

#define COMPUTE_BUILD_MONTH                                                                                            \
    ((BUILD_MONTH_IS_JAN) ? 1 :                                                                                        \
     (BUILD_MONTH_IS_FEB) ? 2 :                                                                                        \
     (BUILD_MONTH_IS_MAR) ? 3 :                                                                                        \
     (BUILD_MONTH_IS_APR) ? 4 :                                                                                        \
     (BUILD_MONTH_IS_MAY) ? 5 :                                                                                        \
     (BUILD_MONTH_IS_JUN) ? 6 :                                                                                        \
     (BUILD_MONTH_IS_JUL) ? 7 :                                                                                        \
     (BUILD_MONTH_IS_AUG) ? 8 :                                                                                        \
     (BUILD_MONTH_IS_SEP) ? 9 :                                                                                        \
     (BUILD_MONTH_IS_OCT) ? 10 :                                                                                       \
     (BUILD_MONTH_IS_NOV) ? 11 :                                                                                       \
     (BUILD_MONTH_IS_DEC) ? 12 :                                                                                       \
                            /* error default */ 99)

#define COMPUTE_BUILD_YEAR                                                                                             \
    ((__DATE__[7] - '0') * 1000 + (__DATE__[8] - '0') * 100 + (__DATE__[9] - '0') * 10 + (__DATE__[10] - '0'))

#define COMPUTE_BUILD_HOUR ((__TIME__[0] - '0') * 10 + __TIME__[1] - '0')
#define COMPUTE_BUILD_MIN ((__TIME__[3] - '0') * 10 + __TIME__[4] - '0')
#define COMPUTE_BUILD_SEC ((__TIME__[6] - '0') * 10 + __TIME__[7] - '0')

#define BUILD_DATE_IS_BAD (__DATE__[0] == '?')

/// \brief L'année de la compilation. Ex : 2022
/// \hideinitializer
#define MA_BUILD_YEAR ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_YEAR)

/// \brief Le mois de la compilation. Ex : 11 pour novembre.
/// \hideinitializer
#define MA_BUILD_MONTH ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_MONTH)

/// \brief Le jour de la compilation. Ex : 23
/// \hideinitializer
#define MA_BUILD_DAY ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_DAY)

#define BUILD_TIME_IS_BAD (__TIME__[0] == '?')

/// \brief L'heure de la compilation.
/// \hideinitializer
#define MA_BUILD_HOUR ((BUILD_TIME_IS_BAD) ? 99 : COMPUTE_BUILD_HOUR)

/// \brief Les minutes de l'heure de la compilation.
/// \hideinitializer
#define MA_BUILD_MINUTES ((BUILD_TIME_IS_BAD) ? 99 : COMPUTE_BUILD_MIN)

/// \brief Les secondes de l'heure de la compilation.
/// \hideinitializer
#define MA_BUILD_SEC ((BUILD_TIME_IS_BAD) ? 99 : COMPUTE_BUILD_SEC)

/// \brief Le numéro de version majeure correspond au cycle courant de la feuille de route.
/// Ex : 0 pour 1er cycle, 1 pour le 2ᵉ cycle.
/// \remarks La feuille de route est décrite dans le « lisez-moi » à la racine
/// du projet.
/// \hideinitializer
#define MA_MAJOR 0

/// \brief La version mineure correspond aux nombres d'étapes achevées qui sont définies dans le cycle courant.
/// \remarks La feuille de route est décrite dans le « lisez-moi » à la racine du projet.
/// \hideinitializer
#define MA_MINOR 9

/// \brief Le statut de cette version « a », « rc » ou «» (vide).
/// Le statut est défini ainsi :
/// - « a » Pour « alpha », les étapes du cycle en cours ne sont pas finies.
/// Sa présence indique aussi qu'un cycle est en cours.
/// - « rc » Pour « Release Candidate » (version candidate à la diffusion), les étapes du cycle en cours sont terminées.
/// La version en cours est alors peaufinée, corrigée et est préparée pour être livrée et étiquetée comme stable.
/// - «» Vide indique que c'est une version publiée en cours d'amélioration.
/// Le numéro de révision et de compilation servent ainsi à identifier la version diffusée.
/// \hideinitializer
#define MA_STATUS "a"

/// \brief Le statut cette version « Alpha », « Candidate à la diffusion » ou « Diffusée ».
/// Le statut est défini ainsi :
/// - « Alpha », les étapes du cycle en cours ne sont pas finies.
/// Sa présence indique aussi qu'un cycle est en cours.
/// - « Candidate à la diffusion » (version candidate à la diffusion), les étapes du cycle en cours sont terminées.
/// La version en cours est alors peaufinée, corrigé et est préparé pour être livrée et étiquetée comme stable.
/// - « Publiée » indique que c'est une version publiée en cours d'amélioration.
/// Le numéro de révision et de compilation servent alors à identifier la version diffusée.

/// \hideinitializer
#define MA_STATUS_VERBOSE "Alpha"

    /// \brief  L'identifiant de compilation correspond à la date et à l'heure de la compilation.
    M_DLL_EXPORT std::wstring GetBuildId();

/// \copydoc ma::GetBuildId
/// \hideinitializer
#define MA_BUILD GetBuildId()

    /// \brief  Le numéro de révision correspond au nom de la branche et à l'identifiant raccourci du commit.
    /// L'identifiant de l'enregistrement des modifications dans le dépôt.
    M_DLL_EXPORT std::wstring GetRevisiondId();

/// \copydoc ma::GetRevisiondId
/// \hideinitializer
#define MA_REVISION ma::GetRevisiondId()

    /// \brief Le numéro de version reprend les numéros de version majeure, mineur, le statut, la compilation et la
    /// révision.
    M_DLL_EXPORT std::wstring GetVersion();

/// \copydoc ma::GetVersion
/// \hideinitializer
#define MA_VERSION_STRING ma::GetVersion()
} // namespace ma
