/// \file Config.h
/// \author Pontier Pierre
/// \date 2023-03-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <cstdlib>
#include <regex>
#include <map>
#include <filesystem>
#include "ma/Dll.h"
#include "ma/String.h"
#include "ma/Item.h"
#include "ma/RTTI.h"

namespace ma
{
    /// \return La valeur de la variable d'environnement, faux si la clef ne correspond à aucune variable.
    /// \see https://stackoverflow.com/questions/631664/accessing-environment-variables-in-c
    inline std::optional<std::wstring> GetEnv(std::wstring const &key)
    {
        auto const key_w = ma::to_string(key);
        char const *val = std::getenv(key_w.c_str());
        return val ? std::optional<std::wstring>{ma::to_wstring(std::string(val))} : std::nullopt;
    }

    using Envs = std::unordered_map<std::wstring, std::wstring>;

    /// \return La liste des variables d'environnements.
    /// \see https://stackoverflow.com/questions/2085302/printing-all-environment-variables-in-c-c
    inline Envs GetEnvs()
    {
        char **env;
#if defined(WIN) && (_MSC_VER >= 1900)
        env = *__p__environ();
#else
        extern char **environ;
        env = environ;
#endif
        Envs result;
        for(; *env; ++env)
        {
            const std::wstring key{ma::to_wstring(std::string{*env})};
            if(auto opt = GetEnv(key))
                result[key] = opt.value();
        }

        return result;
    }

    /// \return Les chemins contenu dans la variable d'environnement "PATH"
    inline std::vector<std::filesystem::path> GetEnvPaths()
    {
        std::vector<std::filesystem::path> result;

        if(auto const opt = GetEnv(L"PATH"))
        {
            auto const paths = ma::Split(opt.value(), L':');
            for(auto const &path : paths)
                result.emplace_back(path);
        }

        return result;
    }

    // https://codereview.stackexchange.com/questions/172644/c-environment-variable-expansion
    inline std::wstring ExpandEnv(std::wstring text)
    {
        static const std::wregex env_re{LR"--(\$\{([^}]+)\})--"};
        std::wsmatch match;
        while (std::regex_search(text, match, env_re)) {
            auto const from = match[0u];
            auto const var_name = match[1u].str();

            if(auto opt = GetEnv(var_name))
                text.replace(from.first, from.second, opt.value());
        }

        return text;
    }

    class Config
    {
        public:
            /// Un conteneur de type « map » permet d'ordonner les données automatiquement.
            /// Le désavantage, c'est que cela ne conserve pas l'ordre d'insertion ou celui du fichier de configuration.
            typedef std::map<std::wstring, std::map<std::wstring, std::wstring>> datas_type;

            std::filesystem::path m_FilePath;
            datas_type m_Datas;

            explicit Config(std::filesystem::path path);
            virtual ~Config();

            virtual void Load();
            virtual void Save();
    };

    // Quelques idées pour implémenter un Item lié à un fichier de configuration.
    // typedef std::tuple<std::wstring, std::wstring, T> group_key_var;
    //
    // ou
    //
    // typedef std::pair<std::wstring, std::map<std::wstring, T>> group_keys_var;
    //
    // Celle retenue:

    class ConfigGroup: public Item
    {
        public:
            /// Nous sauvegardons les clefs des variables étant présentent dans les classes parentes.
            /// Seulement celles non présentent dans « m_VarFromSuperClass » seront enregistrées dans le fichier de
            /// configuration.
            std::set<Var::key_type> m_VarFromSuperClass;

            StringVarPtr m_Name;

            /// Constructeur pour la RTTI.
            /// \remarks à cause de Clang il nous faut deux constructeurs.
            /// Le paramètre name avec une valeur par défaut ne passe pas avec Clang.
            explicit ConfigGroup(const Item::ConstructorParameters &params, const std::wstring &name = L"");

            /// Constructeur pour la construction d'un ConfigGroup depuis le code.
            // ConfigGroup(const Item::ConstructorParameters& params, const std::wstring& name);
            ConfigGroup() = delete;

            /// Destructeur de la classe ConfigGroup.
            ~ConfigGroup() override;

            void LoadVars() override;

            /// Charge juste les données du fichier de configuration dans les variables associées.
            virtual void LoadConfig();

            // Config::datas_type::value_type GetNameKeyValues() const;
            M_HEADER_CLASSHIERARCHY(ConfigGroup)
    };
    typedef std::shared_ptr<ConfigGroup> ConfigGroupPtr;

    /// Permet de faire référence exclusivement à un ConfigGroup dans une variable.
    M_HEADER_ITEM_TYPE_VAR(ConfigGroup)

    /// \brief Lors de sa sauvegarde un « ma::Config » est créé.
    /// Tous les ConfiGroup enfant sont parcourus pour récupérer leurs « pair<nom, map<clefs, valeurs>> » via
    /// GetNameKeyValues.
    /// Le résultat est stocké dans le conteneur « ma::Config::m_Datas » et ensuite « ma::Config::Save() » est appelé.
    /// Lors du chargement le fichier est chargé dans un « ma::Config ».
    /// Si un ConfigGroup n'est pas présent, alors il sera créé.
    class ConfigItem: public Item
    {
        protected:
            friend ConfigGroup;

            /// \brief Le fichier de config est accessible seulement aux enfants de type ConfigGroup.
            Config m_Config;

        public:
            /// \brief Chemin du fichier de configuration.
            FilePathVarPtr m_ConfigFilePath;

            /// \brief Constructeur de ConfigItem.
            /// \param params Les paramètres de construction d'un item.
            /// \param path Le chemin du fichier de configuration.
            explicit ConfigItem(const Item::ConstructorParameters &params, const std::filesystem::path &path = {});
            ConfigItem() = delete;

            /// \brief Destructeur de ConfigItem
            ~ConfigItem() override;

            void Save() override;
            Item::key_errors LoadItems() override;

            /// \brief Charge les données du fichier de configuration dans m_ConfigGroup
            virtual void LoadConfig();

            const Config &GetConfig();

            /// \brief Permet d'accéder à un groupe appartenant à cette configuration.
            /// \return Le groupe correspondant au nom group_name.
            std::optional<ma::ConfigGroupPtr> GetGroup(const std::wstring &group_name);

            M_HEADER_CLASSHIERARCHY(ConfigItem)
    };
    typedef std::shared_ptr<ConfigItem> ConfigItemPtr;

    /// Permet de faire référence exclusivement à un ConfigItem dans une variable.
    M_HEADER_ITEM_TYPE_VAR(ConfigItem)

} // namespace ma

M_HEADER_MAKERITEM_TYPE(ConfigGroup)
M_HEADER_MAKERITEM_TYPE(ConfigItem)
