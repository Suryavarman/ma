/// \file Id.h
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Noncopyable.h"
#include "ma/String.h"
#include <memory>
#include "ma/Dll.h"
#include "ma/Prerequisites.h"

namespace ma
{
    class IdSmile;

    /// http://www.ossp.org/pkg/lib/uuid/
    /// #include <Rpc.h> //visual
    /// https://github.com/graeme-hill/crossguid
    /// https://forums.wxwidgets.org/viewtopic.php?t=5650&p=26072
    /// https://docs.microsoft.com/en-us/cpp/windows/attributes/uuid-cpp-attributes?view=vs-2019
    /// Pour générer un GUID manuellement: https://guidgenerator.com/online-guid-generator.aspx
    class M_DLL_EXPORT Id: public Noncopyable
    {
        public:
            typedef std::wstring key_type;
            /// \return La taille d'une clef.
            static std::wstring::size_type GetStrLength();

            static key_type GenerateKey();

            Id();
            Id(const Id &) = delete;
            explicit Id(const key_type &);
            virtual ~Id();

            [[nodiscard]]
            virtual key_type GetKey() const;

            /// \see IsValid(const key_type&)
            [[nodiscard]]
            virtual bool IsValid() const;

            bool operator==(const Id &other) const;
            bool operator!=(const Id &other) const;
            bool operator==(const key_type &) const;
            bool operator!=(const key_type &) const;

            /// Vérifie si la clef est déjà utilisée.
            /// Si oui, elle n'est pas alors considérée comme une clef unique, sinon elle est considérée comme unique.
            /// \return Vrai si la clef n'est pas déjà utilisé. Sinon la
            /// la fonction renvoie faux.
            static bool IsUnique(const key_type &key);

            /// Permet d'effacer toutes les clefs qui furent enregistrées.
            /// Exemple:
            /// Edition d'un projet puis rechargement du même projet.
            static void ClearKeys();

            /// Renvoie true si la clef est un GUID valide.
            static bool IsValid(const key_type &);

            /// Renvoie true si la clef est un GUID valide.
            static bool IsValid(const std::wstring_view &);

        protected:
            typedef std::unordered_set<key_type> Keys;
            /// Contient toutes les clefs de l'application, ce qui permet de vérifier si une clef existe déjà.
            static Keys ms_Keys;

        private:
            /// Pointeur opaque.
            std::unique_ptr<IdSmile> m_IdSmile;

            /// La clef générée lors de la construction.
            const key_type m_Key;
    };
} // namespace ma
