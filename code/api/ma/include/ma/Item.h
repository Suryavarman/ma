/// \file Item.h
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#define M_HEADER_DEFINE_PARENTS_TYPE(class_name)                                                                           \
    static const ClassNames ms_##class_name##AuthorisedClassNames;                                                         \
    static const ClassNames ms_##class_name##ProhibitedClassNames;                                                         \
                                                                                                                           \
    /** À titre indicatif, le comportement de AcceptParent peut être modifié et ainsi ne plus dépendre seulement du */ \
    /** type du parent à tester. */                                                                                       \
    virtual ClassNames GetAuthorisedParentsType() const override;                                                          \
                                                                                                                           \
    /** À titre indicatif, le comportement de AcceptParent peut être modifié et ainsi ne plus dépendre seulement du */ \
    /** type du parent à tester. */                                                                                       \
    virtual ClassNames GetProhibitedParentsType() const override;

/// \example
/// \code{.cpp}
/// M_CPP_DEFINE_PARENTS_TYPE(L"Toto", {L"Maman", L"Papa"}, {L"Tata", L"Tonton"})
/// M_CPP_DEFINE_PARENTS_TYPE(L"Tout", {}, {})
/// M_CPP_DEFINE_PARENTS_TYPE(L"SeulementOncle", {}, {L"Tata", L"Tonton"})
/// \endcode
#define M_CPP_DEFINE_PARENTS_TYPE(class_name, authorised_class_names, prohibited_class_names)                          \
    const ClassNames ma::class_name::ms_##class_name##AuthorisedClassNames = authorised_class_names;                   \
    const ClassNames ma::class_name::ms_##class_name##ProhibitedClassNames = prohibited_class_names;                   \
                                                                                                                       \
    ClassNames ma::class_name::GetAuthorisedParentsType() const                                                        \
    {                                                                                                                  \
        return ma::class_name::ms_##class_name##AuthorisedClassNames;                                                  \
    }                                                                                                                  \
                                                                                                                       \
    ClassNames ma::class_name::GetProhibitedParentsType() const                                                        \
    {                                                                                                                  \
        return ma::class_name::ms_##class_name##ProhibitedClassNames;                                                  \
    }

#if(MA_COMPILER == MA_COMPILER_CLANG)
    #define M_HEADER_DEFINE_PARENTS_TYPE_WITH_NAMESPACE(class_name, name_space)                                        \
        static const ClassNames ms_##name_space##class_name##AuthorisedClassNames;                                     \
        static const ClassNames ms_##name_space##class_name##ProhibitedClassNames;                                     \
                                                                                                                       \
        /** À titre indicatif, le comportement de AcceptParent peut être */                                          \
        /** modifié et ainsi ne plus dépendre seulement du type du parent à */                                      \
        /** tester. */                                                                                                 \
        virtual ClassNames GetAuthorisedParentsType() const override;                                                  \
                                                                                                                       \
        /** À titre indicatif, le comportement de AcceptParent peut être */                                          \
        /** modifié et ainsi ne plus dépendre seulement du type du parent à */                                      \
        /** tester. */                                                                                                 \
        virtual ClassNames GetProhibitedParentsType() const override;
#else
    #define M_HEADER_DEFINE_PARENTS_TYPE_WITH_NAMESPACE(class_name, name_space)                                        \
        static const ClassNames ms_##name_space####class_name##AuthorisedClassNames;                                   \
        static const ClassNames ms_##name_space####class_name##ProhibitedClassNames;                                   \
                                                                                                                       \
        /** À titre indicatif, le comportement de AcceptParent peut être modifié et ainsi ne plus dépendre */      \
        /** modifié et ainsi ne plus dépendre seulement du type du parent à tester.*/                               \
        virtual ClassNames GetAuthorisedParentsType() const override;                                                  \
                                                                                                                       \
        /** À titre indicatif, le comportement de AcceptParent peut être modifié et ainsi ne plus dépendre  */     \
        /** seulement du type du parent à tester. */                                                                  \
        virtual ClassNames GetProhibitedParentsType() const override;
#endif

#if(MA_COMPILER == MA_COMPILER_CLANG)
    /// \example
    /// \code {.cpp}
    /// M_CPP_DEFINE_PARENTS_TYPE(L"Toto", L"EspaceDeNom", {L"Maman", L"Papa"}, {L"Tata", L"Tonton"})
    /// \endcode
    #define M_CPP_DEFINE_PARENTS_TYPE_WITH_NAMESPACE(                                                                  \
        class_name, name_space, authorised_class_names, prohibited_class_names)                                        \
        const ClassNames ma::class_name::ms_##name_space##class_name##AuthorisedClassNames = authorised_class_names;   \
        const ClassNames ma::class_name::ms_##name_space##class_name##ProhibitedClassNames = prohibited_class_names;   \
                                                                                                                       \
        ClassNames ma::class_name::GetAuthorisedParentsType() const                                                    \
        {                                                                                                              \
            return ma::class_name::ms_##name_space##class_name##AuthorisedClassNames;                                  \
        }                                                                                                              \
                                                                                                                       \
        ClassNames ma::class_name::GetProhibitedParentsType() const                                                    \
        {                                                                                                              \
            return ma::class_name::ms_##name_space##class_name##ProhibitedClassNames;                                  \
        }
#else
    /// \example
    /// \code{.cpp}
    /// M_CPP_DEFINE_PARENTS_TYPE(L"Toto",
    ///                           L"EspaceDeNom",
    ///                           {L"Maman", L"Papa"},
    ///                           {L"Tata", L"Tonton"})
    /// \endcode
    #define M_CPP_DEFINE_PARENTS_TYPE_WITH_NAMESPACE(                                                                  \
        class_name, name_space, authorised_class_names, prohibited_class_names)                                        \
        const ClassNames ma::class_name::ms_##name_space####class_name##AuthorisedClassNames = authorised_class_names; \
        const ClassNames ma::class_name::ms_##name_space####class_name##ProhibitedClassNames = prohibited_class_names; \
                                                                                                                       \
        ClassNames ma::class_name::GetAuthorisedParentsType() const                                                    \
        {                                                                                                              \
            return ma::class_name::ms_##name_space####class_name##AuthorisedClassNames;                                \
        }                                                                                                              \
                                                                                                                       \
        ClassNames ma::class_name::GetProhibitedParentsType() const                                                    \
        {                                                                                                              \
            return ma::class_name::ms_##name_space####class_name##ProhibitedClassNames;                                \
        }
#endif

#include <memory> // shared_ptr
#include <mutex>
#include <queue>
#include <filesystem> // sauvegarde des items
#include "ma/String.h"
#include "ma/Id.h"
#include "ma/TypeInfo.h"
#include "ma/Observer.h"
#include "ma/Dll.h"

namespace ma
{
    class Item;
    class Var;

    // https://gcc.gnu.org/onlinedocs/gcc-4.6.3/libstdc++/api/a01103_source.html
    // http://www.enseignement.polytechnique.fr/informatique/INF478/docs/Cpp/en/cpp/header/unordered_map.html
    // https://stackoverflow.com/questions/16923748/c-inserting-unique-ptr-in-map
    // https://stackoverflow.com/questions/3906295/c-unique-ptr-and-map
    // https://stackoverflow.com/questions/20965200/stdhash-for-unique-ptr-in-unordered-map
    // https://pybind11.readthedocs.io/en/stable/advanced/smart_ptrs.html
    typedef std::shared_ptr<Item> ItemPtr;
    typedef std::shared_ptr<Var> VarPtr;
    typedef std::unordered_map<Id::key_type, ItemPtr> ItemsMap;
    typedef std::deque<ItemPtr> ItemsOrder;
    typedef std::unordered_map<Id::key_type, std::wstring> Dictionary;
    typedef std::unordered_set<Id::key_type> Keys;
    typedef std::unordered_map<std::wstring, VarPtr> VarsMap;

    /// Pour définir une liste de noms de classe uniques.
    typedef std::set<std::wstring> ClassNames;

    /// \brief Élément de base de la structure de données de Ma.
    /// Un item contient une liste d'enfants, la clef de son parent et la liste de ses variables.
    /// \todo gérer l'ordre des enfants.
    /// \remarks À partir d'un Item parent pour surveiller l'ajout ou la suppression d'un enfant, vous pouvez soit
    /// observer l'item parent, soit surcharger les fonctions :
    /// \li virtual void AddChild(ma::ItemPtr item)
    /// \li virtual void RemoveChild(ma::ItemPtr item)
    /// \note Comment choisir entre une clef statique et une clef dynamique ?
    /// Certaines parties du code ne connaissent pas la classe « Root » et certaines ne le doivent pas pour permettre de
    /// générer d'autre type de racines.
    /// Pour accéder à un gestionnaire sans connaitre la racine, il faut connaître la clef qui lui est associée. \n
    /// Exemple:
    /// Une clef statique comme « ma::Item::ms_KeyGarbage » permet de retrouver la corbeille des items.
    /// Le chargement et la sauvegarde rentrent en ligne de compte aussi.
    /// La RTTI est utilisée lors du chargement pour recréer les items définis dans la sauvegarde.
    /// Les items qui ne peuvent pas être créés par la RTTI ou difficilement doivent être avec une clef statique pour
    /// être retrouvé et recevoir leurs nouvelles valeurs. \n
    /// Exemple:
    /// « wxWindowParameter » nécessite à sa création d'un pointeur sur une fenêtre transmis paramètre via le
    /// constructeur.
    /// Il faut alors voir s'il vaut le coup d'implémenter la capacité de retrouver ces fenêtres ou de garder en ces
    /// items car ils représenteront la même donnée avec des valeurs différentes.
    class M_DLL_EXPORT Item: public ma::Observable, public std::enable_shared_from_this<ma::Item>
    {
        public:
            typedef Id::key_type key_type;
            typedef std::mutex mutex;
            typedef std::lock_guard<std::mutex> lock_guard;

            /// Raccourcis d'écriture
            typedef std::pair<std::wstring, std::wstring> pair_str;

            /// \brief Le type des données qui sont ou seront sauvegardés dans l'en-tête de la sauvegarde.
            ///
            /// \example
            /// \code{.cpp}
            /// {
            ///     {"key", "7a3ec54e-e7cd-4c18-87d6-36bfc1c7bb72"},
            ///     {"type", "Resource"}
            /// }
            /// \endcode
            /// \remarks Le fichier de sauvegarde d'un item est composé d'un numéro de version, d'un en-tête et d'un
            /// corps.
            typedef std::tuple<std::pair<std::wstring, Item::key_type>, pair_str> header_save_type;

            /// \brief Le type des données qui sont ou seront sauvegardées pour représenter une variable.
            /// \example
            /// \code{.cpp}
            /// {
            ///     {"key", "m_Name"},
            ///     {"type", "StringVar"},
            ///     {"value", "print_HelloWorld.py"},
            ///     {"info", {{"__doc__", "Exemple 'information sur cette instance."}}}
            /// }
            /// \endcode
            /// \todo Ajouter les IInfos
            typedef std::tuple<std::pair<std::wstring, VarsMap::key_type>,
                               pair_str,
                               pair_str,
                               std::pair<std::wstring, ma::InstanceInfo>>
                var_save_type;

            /// \brief Le type des données qui sont ou seront sauvegardées dans le corps de la sauvegarde.
            /// \example
            /// \code{.cpp}
            /// {
            ///     {
            ///         {"key", "m_Name"},
            ///         {"type", "StringVar"},
            ///         {"value", "print_HelloWorld.py"}
            ///     },
            ///     {
            ///         {"key", "m_CustomSavePath"},
            ///         {"type", "BoolVar"},
            ///         {"value", "false"}
            ///     }
            /// }
            /// \endcode
            typedef std::vector<var_save_type> body_save_type;

            /// \brief Une représentation du corps de la sauvegarde plus facile à manipuler.
            /// \example
            /// \code{.cpp}
            /// {
            ///    {
            ///         {"m_Name"},
            ///         {"StringVar", "print_HelloWorld.py",
            ///             {
            ///                 {"__doc__", "Exemple d'information d'instance."
            ///             }
            ///          }
            ///     },
            ///    {
            ///         {"m_CustomSavePath"}, {"BoolVar", "false", {}}
            ///     }
            /// }
            /// \endcode
            typedef std::unordered_map<VarsMap::key_type, std::tuple<std::wstring, std::wstring, ma::InstanceInfo>>
                vars_save_type;

            /// type des dépendances retourné par GetDependenciesData()
            typedef std::vector<Item::key_type> dependencies_type;

            struct Key
            {
                    /// \brief Si un item se retrouve sans parents, l'id de son parent
                    /// sera celui-ci.
                    /// Hormis l'item racine, un item sans parents est un item qui n'est pas enregistré dans ms_Map.
                    /// \remarks Un item sans parents doit être considéré comme geler.
                    /// Les opérations telles que GetItem, utilisent ms_Map pour retrouver les items.
                    static const key_type &GetNoParent();

                    /// \brief Tout item actif hérite de root. « Root » est item possédant l'id « ms_KeyRoot ».
                    /// Tout item qui n'est pas enfant de root, n'est pas présent dans « ms_Map ».
                    static const key_type &GetRoot();

                    /// \brief Clef du gestionnaire des observations
                    /// \see ma::ObservationManager
                    static const key_type &GetObservationManager();

                    /// \brief Clef du gestionnaire des informations sur la hiérarchie des classes.
                    /// \see ma::ClassInfo
                    static const key_type &GetClassInfoManager();

                    /// \brief Clef du gestionnaire de fabriques d'items, de variables…
                    /// \see ma::ClassInfo
                    static const key_type &GetRTTI();

                    /// \brief Clef du gestionnaire de ressources.
                    /// \see ma::ResourceManager
                    static const key_type &GetResourceManager();

                    /// \brief Clef du gestionnaire de contextes.
                    /// \see ma::ContextManager
                    static const key_type &GetContextManager();

                    /// \brief Clef pour accéder aux éléments standard de l'interface graphique.
                    /// \see ma::GuiAccess
                    /// \todo Devrait être remplacer par le système de vues « .
                    static const key_type &GetGuiAccess();

                    /// \brief Clef du gestionnaire des paramètres d'affichage de l'interface wxWidgets.
                    static const key_type &GetwxParameterManager();

                    /// \brief Clef de la poubelle à items.
                    /// \see ma::Garbage
                    static const key_type &GetGarbage();

                    /// \brief La clef du fichier de configuration de l'application.
                    /// \see ma::ConfigItem
                    static const key_type &GetConfig();

                    /// \brief Clef du gestionnaire de modules.
                    /// \see ma::ModulesManager
                    static const key_type &GetModuleManager();

                    struct Var
                    {
                            /// \brief Clef utilisée par défaut pour nommer l'instance d'un item.
                            /// La convention est que la variable est du type « ma::StringVar ».
                            /// Par défaut cette variable n'est pas présente.
                            static const ma::VarsMap::key_type &GetName();

                            /// \brief Clef utilisée par défaut pour décrire l'instance d'un item.
                            /// La convention est que la variable est du type « ma::StringVar ».
                            /// Par défaut cette variable n'est pas présente.
                            static const ma::VarsMap::key_type &GetDoc();

                            /// \brief Clef utiliser pour nommer la variable m_SavePath.
                            /// \see m_SavePath
                            static const ma::VarsMap::key_type &GetSavePath();

                            /// \brief Clef utiliser pour nommer la variable m_LastSavePath.
                            /// \see m_LastSavePath
                            static const ma::VarsMap::key_type &GetLastSavePath();

                            /// \brief Clef utiliser pour nommer la variable m_CustomSavePath.
                            /// \see m_CustomSavePath
                            static const ma::VarsMap::key_type &GetCustomSavePath();

                            /// \brief Clef de la variable contenant la liste des dépendances de le l'élément.
                            /// La variable associée est du type: ma::ItemVectorVar. Une dépendence peut être un dossier
                            /// une ressource, un module, etc.
                            static const ma::VarsMap::key_type &GetDependencies();
                    };

                    struct Obs
                    {
                            /// \brief Clef qui permettra à un observateur d'items de reconnaître un évènement d'ajout
                            /// d'item. La valeur associée devra être la clef de l'item ajouté.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsAddItemBegin, item->GetKey()}});
                            /// \endcode
                            /// \remarks Les items n'ont pas encore été ajouté à ms_Map et à la liste des enfants de
                            /// l'item.
                            static const ma::Observable::Data::key_type ms_AddItemBegin;

                            /// \brief Clef qui permettra à un observateur d'items de reconnaître un évènement d'ajout
                            /// d'item. La valeur associée devra être la clef de l'item ajouté.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsAddItemEnd, item->GetKey()}});
                            /// \endcode
                            /// \remarks Les items ont été ajoutés à ms_Map et à la liste des enfants de l'item.
                            static const ma::Observable::Data::key_type ms_AddItemEnd;

                            /// \brief Clef qui permettra à un observateur d'items de reconnaître un évènement de
                            /// suppression d'item.
                            /// À noter que ce n'est pas la destruction de l'item.
                            /// La valeur associée devra être la clef de l'item supprimé.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsRemoveItemBegin, item->GetKey()}});
                            /// \endcode
                            /// \remarks Les items n'ont pas été effacé de ms_Map et de la liste des enfants de l'item.
                            static const ma::Observable::Data::key_type ms_RemoveItemBegin;

                            /// \brief Clef qui permettra à un observateur d'items de reconnaître un évènement de
                            /// suppression d'item.
                            /// À noter que ce n'est pas la destruction de l'item.
                            /// La valeur associée devra être la clef de l'item supprimé.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsRemoveItemEnd, item->GetKey()}});
                            /// \endcode
                            /// \remarks Les items ont été effacés de ms_Map et de la liste des enfants de l'item.
                            static const ma::Observable::Data::key_type ms_RemoveItemEnd;

                            /// \brief Clef qui permettra à un observateur d'items de reconnaître un évènement d'ajout
                            /// d'une variable.
                            /// À noter que ce n'est pas la destruction de la variable.
                            /// La valeur associée devra être la clef de la variable ajoutée.
                            /// \remarks Attention dans « Item.tpp », la variable « ms_KeyObsAddVar » ne peut être
                            /// utilisée, car la référence à « ms_KeyObsAddVar » n'existe as encore.
                            /// \todo Utiliser une fonction pour retourner ms_KeyObsAddVar
                            ///       \code{.cpp}
                            ///       this->UpdateObservations({{ms_KeyObsAddVar, var->GetKey()}});
                            ///       \endcode
                            /// \remarks Lorsque l'évènement est déclenché la variable a déjà été ajoutée à l'item.
                            static const ma::Observable::Data::key_type ms_AddVar;

                            /// \brief Clef qui permettra à un observateur d'items de reconnaître un évènement de
                            /// suppression d'une variable.
                            /// À noter que ce n'est pas la destruction de la variable.
                            /// La valeur associée devra être la clef de la variable supprimée.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsRemoveVarBegin, var->GetKey()}});
                            /// \endcode
                            /// \remarks La variable n'a pas encore été effacé de l'item.
                            static const ma::Observable::Data::key_type ms_RemoveVarBegin;

                            /// \brief Clef qui permettra à un observateur d'items de reconnaître un évènement de
                            /// suppression d'une variable.
                            /// À noter que ce n'est pas la destruction de la variable.
                            /// La valeur associée devra être la clef de la variable supprimée.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsRemoveVarBegin, var->GetKey()}});
                            /// \endcode
                            /// \remarks La variable a été effacé de l'item.
                            static const ma::Observable::Data::key_type ms_RemoveVarEnd;

                            /// \brief Clef qui sert à identifier l'évènement de fin de construction d'un item.
                            /// L'item s'il a un parent, utilisera le parent pour notifier l'évènement.
                            /// Ce qui permettra de commencer son observation.
                            static const ma::Observable::Data::key_type ms_ConstructionEnd;

                            static const ma::Observable::Data::key_type ms_GlobalAddItemBegin;
                            static const ma::Observable::Data::key_type ms_GlobalAddItemEnd;
                            static const ma::Observable::Data::key_type ms_GlobalRemoveItemBegin;
                            static const ma::Observable::Data::key_type ms_GlobalRemoveItemEnd;
                    };
            };

            /// \brief Le type du paramètre du constructeur des items.
            /// \todo Lors du passage au C++20 il faudra voir pour gérer une initialisation agrégée.
            ///       Pour ce faire il faudra peut être créer une structure supplémentaire sans constructeur et avec des
            ///       paramètres plus court.
            ///       https://en.cppreference.com/w/cpp/language/aggregate_initialization
            ///       https://en.cppreference.com/w/cpp/language/list_initialization
            ///       Ce qui permettra de faire ainsi {.parent_key=key, .savable=false}.
            struct ConstructorParameters
            {
                    /// \brief Le parent dans lequel sera ajouter le nouvel item.
                    /// \remarks Par défaut l'item sera ajouté à la racine des
                    /// items.
                    key_type m_ParentKey;

                    /// \brief La clef de l'item à créer. Par défaut la clef est vide.
                    /// \remarks « CreateItem » s'occupe de générer la clef si celle-ci n'est pas spécifiée.
                    key_type m_Key;

                    /// \copydoc ma::Item::m_Savable
                    /// \see ma::Item::m_Savable
                    bool m_Savable;

                    /// \copydoc ma::Item::m_CanBeDeleteOnLoad
                    /// \see ma::Item::m_CanBeDeleteOnLoad
                    bool m_CanBeDeleteOnLoad;

                    /// \brief Constructeur de « ConstructorParameters »
                    /// \param parent_key @copybrief ma::Item::ConstructorParameters::m_ParentKey
                    /// \param key @copybrief ma::Item::ConstructorParameters::m_Key
                    /// \param savable @copybrief ma::Item::ConstructorParameters::m_Savable
                    /// \param can_be_delete_on_load @copybrief ma::Item::ConstructorParameters::m_CanBeDeleteOnLoad
                    /// \remarks Si vous avez ce genre d'erreurs avec le module python :
                    ///  « ma.cpython-311-x86_64-linux-gnu.so: undefined symbol:
                    ///    _ZN2ma4Item21ConstructorParametersC1ERKNSt7__cxx1112basic_stringIwSt11char_traits... ».
                    /// Il vous faudra supprimer les sous-dossiers « ma » et « modules/module_python » de la cible
                    /// courante de cmake.
                    ConstructorParameters(Item::key_type parent_key = Key::GetRoot(),
                                          Item::key_type key = {},
                                          bool savable = true,
                                          bool can_be_delete_on_load = true);

                    /// \brief Constructeur de « ConstructorParameters ».
                    /// Permet de modifier un paramètre constant en le modifiant.
                    /// \example
                    /// \code{.cpp}
                    ///     Item({in_params, ma::Item::ms_KeyGarbage, false})
                    /// \endcode
                    ConstructorParameters(const ConstructorParameters &params,
                                          Item::key_type key,
                                          bool savable = true,
                                          bool can_be_delete_on_load = true);

                    /// \brief Constructeur de « ConstructorParameters ».
                    /// Permet de modifier un paramètre constant en le modifiant.
                    /// \example
                    /// \code{.cpp}
                    ///     Item({params, false, true})
                    /// \endcode
                    ConstructorParameters(const ConstructorParameters &params,
                                          bool savable,
                                          bool can_be_delete_on_load = true);
            };

            /// \brief Le type du paramètre des CreateItem.
            /// Cela permet d'éviter de multiplier les retouches du code des constructeurs et de faciliter l'appel à
            /// « Item::CreateItem ».
            struct CreateParameters
            {
                    /// \brief Le parent dans lequel sera ajouter le nouvel item. Par défaut l'item sera ajouté à la
                    /// racine des items.
                    key_type m_ParentKey;

                    /// \brief Permet de dire à « CreateItem » d'appeler ou non « item->EndConstruction » une fois la
                    /// construction de l'item terminée.
                    /// Dans certains cas comme par exemples les observations, « CreateItem » ne pourra pas finaliser la
                    /// construction de l'item.
                    /// C'est ainsi qu'en mettant faux à « call_end_construction », la fonction appelante pourra
                    /// finaliser la construction de l'item et appeler « item->EndConstruction » au moment opportun.
                    bool m_CallEndConstruction;

                    /// \copydoc ma::Item::m_CanBeDeleteOnLoad
                    /// \see ma::Item::m_CanBeDeleteOnLoad
                    bool m_CanBeDeleteOnLoad;

                    /// \brief La clef de l'item à créer. Par défaut la clef est vide.
                    /// « CreateItem » s'occupe de générer la clef si celle-ci n'est pas spécifiée.
                    key_type m_Key;

                    CreateParameters(key_type parent_key = Key::GetRoot(),
                                     bool call_end_construction = true,
                                     bool can_be_delete_on_load = true,
                                     key_type key = {});
            };

            /// \brief Seulement pour pouvoir rendre amis des fonctions qui permettent aux items propriétaires de
            /// variables de les éditer.
            friend Var;

#if MA_COMPILER_CLANG == MA_COMPILER
            /// \brief Permet de construire un item.
            /// Pour créer/initialiser la racine, utilisez ja fonction « CreateRoot() ».
            /// \param params Les paramètres d'initialisation de base des items.
            /// \param args Les arguments supplémentaires pour créer l'item.
            /// \remarks La clef du parent doit être défini dans l'appel du CreateItem et non pas dans le constructeur
            /// de l'item. La clef du parent défini dans le CreateParameters prévaux.
            /// \remarks Pourquoi créer un item sans parents ?
            /// \example Certains parents n'accepteront pas l'item.
            /// Parfois, il est nécessaire de tester si le parent va accepter l'item.
            /// Pour cela, il faut pouvoir lui donner l'item à tester et donc instancier cet item.
            /// \example
            /// \code{.cpp}
            /// auto child = ma::Item::CreateItem({ma::Item::ms_KeyNoParent});
            /// if(item_parent->AcceptToAddChild(child))
            ///     item_parent->AddChild(child);
            /// \endcode
            /// \remarks
            /// Clang ne sais pas gérer les paramètres par défauts avec un nombre de paramètres variable.
            /// La règle en c++ est pour le cas spécifique des paramètres variables et les paramètres par défaut.
            /// Pour que le compilateur puisse déduire les paramètres, nous plaçons en premier les paramètres par défaut
            /// et en dernier les paramètres variables «...».
            /// \see https://en.cppreference.com/w/cpp/language/parameter_pack
            template<typename T, typename... Args>
            static std::shared_ptr<T> CreateItem(const CreateParameters &params, const Args &...args);

            template<typename T>
            static std::shared_ptr<T> CreateItem(const CreateParameters &params = CreateParameters());
#else
            /// \brief Permet de construire un item.
            /// Pour créer/initialiser la racine, utilisez CreateRoot().
            /// \param params Les paramètres d'initialisation de tous les items.
            /// \param args Les arguments supplémentaires pour créer l'item
            /// \remarks Pourquoi créer un item sans parents ?
            /// \example
            /// Certains parents n'accepteront pas l'item.
            /// Parfois il est nécessaire de tester si le parent va accepter l'item.
            ///  Pour cela il faut pouvoir lui donner l'item à tester et donc instancier cet item.
            /// \example
            /// \code{.cpp}
            /// auto child = ma::Item::CreateItem({ma::Item::ms_KeyNoParent});
            /// if(item_parent->AcceptToAddChild(child))
            ///     item_parent->AddChild(child);
            /// \endcode
            template<typename T, typename... Args>
            static std::shared_ptr<T> CreateItem(const CreateParameters &params = CreateParameters(),
                                                 const Args &...args);
#endif // MA_COMPILER_CLANG

            /// Raccourcis pour récupérer un item si il existe ou sinon le créer.
            /// \remarks Est souvent utilisé pour le code qui s'exécute après le chargement d'un projet.
            /// \exception std::invalid_argument params.m_Key doit être défini et valide.
            template<typename T, typename... Args>
            static std::shared_ptr<T> GetOrCreateItem(const CreateParameters &params, const Args &...args);

            /// \brief La fonction permettant d'initialiser le root.
            /// \remarks avant toute création d'item il vous faut créer la
            /// racine.
            template<typename T, typename... Args>
            static std::shared_ptr<T> CreateRoot(const Args &...args);

            /// \brief C'est la fonction qui permet d'effacer de ms_Map la racine.
            /// Les enfants de la racine seront, eux aussi, effacés.
            static void EraseRoot();

            /// \brief Renvoie l'item racine dont tous les items enregistrés héritent.
            /// \exception std::runtime_error  L'item racine n'a pas encore été créé.
            template<typename T>
            static std::shared_ptr<T> GetRoot();

            /// \brief Renvoie l'item racine dont tous les items enregistrés héritent.
            /// \exception std::runtime_error  L'item racine n'a pas encore été créé.
            static ItemPtr GetRoot();

            /// \brief  Permet d'effacer de ms_Map un item.
            /// L'item sera effacer de son parent et ses enfants seront effacés à leur tour.
            /// \param key La clef de l'item à effacer.
            /// \exception std::runtime_error key est égale à ms_KeyRoot ou à aucun item de ms_Map.
            static void EraseItem(const key_type &key);

            /// \brief  Permet d'effacer de ms_Map un item.
            /// L'item sera effacer de son parent et ses enfants seront effacés à leur tour.
            /// \param item L'item à effacer.
            /// \exception std::runtime_error key est égale à ms_KeyRoot ou à aucun item de ms_Map.
            static void EraseItem(const std::shared_ptr<Item> item);

            /// \return Renvoie une copie du conteneur globale d'items
            static ItemsMap GetItems();

            /// \brief C'est la même fonction que GetItems, excepté que l'ordre des items est important.
            /// Il est retourné de manière, à ce que les enfants de chaque branche soient placés avant les parents.
            /// L'algorithme de parcours est d'un ordre post-fixé appelé aussi post-ordre.
            /// \return Renvoie une copie du conteneur  globale d'items
            /// \see ma::Item::GetItems
            /// \example <a
            /// https://rmdiscala.developpez.com/cours/LesChapitres.html/Cours4/Chap4.8.htm" target="_blank">Parcours
            /// d'arbres</a>
            /// \code
            ///     . a .
            ///    .     .
            ///   b       c
            ///  . .     .
            /// d   e   f
            /// \endcode
            /// La liste d'items retournée sera dans l'un des ordres suivants :
            /// \li debfca
            /// \li edbfca
            /// \li fcdeba
            /// \li fcedba
            /// \todo Il faut ordonner les enfants pour avoir un contrôle sur
            ///       l'ordre des items.
            static ItemsOrder GetItemsPostOrder();

            /// \brief Recherche de manière globale un item.
            /// \param key La clef de l'identifiant de l'item recherché.
            /// \return Renvoie l'item trouvé. Si l'item n'existe pas une exception sera levée et nullptr sera retourné.
            /// \exception std::invalid_argument Si la clef ne fait référence à aucun item.
            /// \exception std::runtime_error Il est impossible de convertir l'item dans le type souhaité.
            template<typename T>
            static std::shared_ptr<T> GetItem(const key_type &key);

            /// \brief Recherche de manière globale un item.
            /// \param key La clef de l'identifiant de l'item recherché.
            /// \return Renvoie l'item trouvé. Si l'item n'existe pas une exception sera levée et nullptr sera retourné.
            /// \exception std::invalid_argument Si la clef ne fait référence à aucun item.
            static ma::ItemPtr GetItem(const key_type &key);

            /// \brief Recherche de manière globale l'item associé à cette clef.
            /// \return Renvoie vrais si la clef est associé un item de ms_Map.
            /// \remarks Par défaut le comportement de HasItem est de vérifier
            static bool HasItem(const key_type &key);

            /// \brief Recherche de manière globale l'item associé à cette clef.
            /// \return Renvoie vrais si la clef est associé à un item de ms_Map.
            /// Si vrais, il renvoie l'item.
            /// \example
            /// \code{cpp}
            /// if(auto opt = HasItemOpt<ConfigItem>())
            ///     std::wcout << opt.value()->m_ConfigFilePath->toString() << std::endl;
            /// \endcode
            template<typename T>
            static std::optional<std::shared_ptr<T>> HasItemOpt(const key_type &key);

            /// \brief Recherche de manière globale l'item associé à cette clef.
            /// \return Renvoie vrais si la clef est associé à un item de ms_Map.
            /// Si vrais, il renvoie l'item.
            static std::optional<ma::ItemPtr> HasItemOpt(const key_type &key);

            /// \return renvoie le nombre d'items contenus dans ms_Map
            static ItemsMap::size_type GetCount();

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Récupération des types autorisés ou non pour être enfant ou parent d'un type d'item.

            /// Permet de connaître la liste des classes d'item qu'un type d'item peut avoir en tant qu'enfant.
            static ClassNames GetWhiteListChildren(const std::wstring &class_name);

            /// Permet de connaître la liste des classes d'item qu'un type d'item ne peut pas avoir en tant qu'enfant.
            static ClassNames GetBlackListChildren(const std::wstring &class_name);

            /// Permet de connaître la liste des classes d'item qu'un type d'item peut avoir en tant que parent.
            static ClassNames GetWhiteListParents(const std::wstring &class_name);

            /// Permet de connaître la liste des classes d'item qu'un type d'item ne peut pas avoir en tant que parent.
            static ClassNames GetBlackListParents(const std::wstring &class_name);

            /// Permet de récupérer la liste des classes autorisées comme
            /// parents.
            ///
            /// \remarks Le comportement par défaut est le suivant :
            /// Si aucune classe n'est listée dans la liste blanche :
            /// « GetWhiteListParents » ayant et si aucune classe n'est listée dans la liste noire GetBlackListParents
            /// alors tous les types dérivant d'Item (Item inclus) seront acceptés.
            /// Si « GetWhiteListParents » et/ou GetBlackListParents renvoie une liste non vide de noms de classe voici
            /// le comportement :
            /// \li Si la liste blanche est vide alors tous les types d'items sont acceptés sauf ceux ayant leur nom de
            /// classe ou héritant d'un des noms de classe référencés dans la liste noire.
            /// \li Si la liste blanche n'est pas vide alors seulement les noms de classe qui y sont référencés seront
            /// acceptés sauf si l'un d'eux fait partie ou hérite de la liste noire. Cette dernière étant prioritaire
            static ClassNames GetAuthorisedParentsType(const std::wstring &class_name);
            static ClassNames GetProhibitedParentsType(const std::wstring &class_name);

            static ClassNames GetAuthorisedChildrenType(const std::wstring &class_name);
            static ClassNames GetProhibitedChildrenType(const std::wstring &class_name);

        private:
            /// \brief Il est mis à vrais lors du premier appel du destructeur.
            /// Au deuxième une exception est levée. Cela permet de faciliter le débogage, car l'utilisation de
            /// « shared_ptr » peut vite mener à de mauvaises utilisations.
            bool m_DeleteProcessing;

            /// \brief Factorisation de AddVar et AddMemberVar
            template<typename T, typename T_ConstructorParameters, typename... Args>
            std::shared_ptr<T> _AddVar(const T_ConstructorParameters &params, const Args &...args);

            /// \brief Factorisation pour initialiser les variables « m_SavePath » et « m_LastSavePath »
            /// \see m_SavePath
            /// \see m_LastSavePath
            void InitPathVars();

            /// \brief Si GetModuleDependency() renvoie un module, alors cette fonction va créer une variable nommée par
            /// la fonction « ma::Item::Key::Var::GetDependenciesData() » et y stocker cette dépendance.
            void InitDependencyVar();

            /// {clef de l'item, {clefs root, ..., clef parent + 1, clef parent}}
            typedef std::unordered_map<ma::Item::key_type, std::deque<ma::Item::key_type>> parenting_map_type;

            /// Utilisé pour le chargement des éléments.
            void FillParentingMap(parenting_map_type &dependencies_map, const ItemPtr &item, const Item::key_type &key);

            /// Utilisé pour le chargement des éléments.
            /// C'est une fonction membre pour avoir accès à la lecture des dossiers et des fichiers.
            void FillParentingMap(parenting_map_type &dependencies_map,
                                  const std::filesystem::directory_entry &directory_path,
                                  const Item::key_type &parent_key);

        protected:
            /// \brief Si vous accédez à ms_Map veillez à utiliser ce mutex pour sécuriser l'accès.
            static mutex ms_MapMutex;

            /// \brief Tous les items actifs sont stockés ici.
            /// \remarks Les items gèrent eux même leur insertion et suppression dans ms_Map.
            static ItemsMap ms_Map;

            /// \brief Les enfants de l'item.
            /// \code{.cpp}
            /// {Key, ItemPtr}.
            /// \endcode
            ItemsMap m_Children;

            /// \brief L'identifiant de l'item
            const Id m_Id;

            /// \brief La clef de l'item parent.
            /// \remarks Si l'item n'a pas de parent « m_ParentKey » sera affecté de la valeur de ms_KeyNoParent.
            key_type m_ParentKey;

            ///\brief Les variables de l'item
            ///\remarks La clef doit être unique dans le contexte de l'item.
            VarsMap m_Vars;

            /// \brief Permet de créer un item avec une clef spécifique, mais ne peut pas l'assigner à son parent.
            /// \param params Les paramètres de création de l'Item.
            /// \todo Faire les tests unitaires pour tester ms_KeyNoParent.
            /// \remarks AddChild ne peut pas fonctionner correctement si l'item n'est pas fini d'être construit.
            /// L'appelant doit finir le travail en ajoutant l'item à son parent.
            /// Notez bien que tant que la fonction « AddChild » n'est pas appelée la clef du parent n'est pas assignée
            /// à la variable « m_ParentKey ».
            /// \todo Cette gestion d'AddChild est un problème pour les items dérivant d'Item qui ont besoin de la clef
            ///       du parent.
            ///       Ils doivent pour le moment utiliser params.m_ParentKey. Ce qui n'est pas bien fait et peut amener
            ///       à des erreurs.
            explicit Item(const ConstructorParameters &params);

        public:
            /// Suppression du constructeur par défaut.
            Item() = delete;

            /// Suppression du constructeur par copie.
            Item(const Item &) = delete;

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Gestion protégée des variables

        protected:
            /// \brief Permet d'initialiser une variable de l'item.
            /// \return renvoie le pointeur intelligent qui s'occupe de variable ajoutée.
            /// \exception std::invalid_parameter la variable est déjà associée à un item ou à ce même item.
            /// \remarks La « propriété » de la variable est délégué aux « VariablePtr » qui la géreront et la
            /// libéreront le moment venu.
            template<typename T, typename... Args>
            std::shared_ptr<T> AddMemberVar(const VarsMap::key_type &key, const Args &...args);

            /// Tout comme AddMemberVar mais la variable n'est accessible qu'en lecture.
            template<typename T, typename... Args>
            std::shared_ptr<T> AddReadOnlyMemberVar(const VarsMap::key_type &key, const Args &...args);

            /// \brief Permet d'ajouter une variable à l'item.
            /// \exception std::invalid_parameter la variable est déjà associée à un item ou à ce même item.
            /// \remarks Cette fonction AddVar permet d'ajouter une variable dont le constructeur ne peut être défini
            /// par le patron du « template » des autres fonctions AddVar ou AddMemberVar. Mais cette fonction est
            /// protégée afin d'assurer que la propriété « ma::Var::m_Dynamique » soit définie exclusivement par les
            /// items.
            void AddVar(const ma::VarPtr &);

            /// Permet à un item possédant une variable de la modifier même si celle-ci est en lecture seul.
            void SetVarFromString(const VarsMap::key_type &key, const std::wstring &value);

            /// Permet à un item possédant une variable de la modifier même si celle-ci est en lecture seul.
            template<typename T_Var, typename... Args>
            void SetVarFromValue(const VarsMap::key_type &key, const Args &...args);

            /// Concrétisation de la classe Observable.
            /// \see ma::Observable::GetPtr()
            ObservablePtr GetPtr() const override;

            /// \exception std::invalide_argument L'élément n'est pas du type « T ».
            template<typename T>
            std::shared_ptr<T> GetPtr() const;

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Les fonctions protégées d'aide au chargement et à la sauvegarde
            //  des items.

            /// \brief Retourne le nom du dossier de sauvegarde qui représente cet item.
            /// \param item L'item duquel nous souhaitons connaître le nom de son dossier.
            /// \see
            /// https://stackoverflow.com/questions/1976007/what-characters-are-forbidden-in-windows-and-linux-directory-names
            /// \todo gérer les caractères interdits.
            virtual std::wstring GetFolderSaveName(const Item &item) const;

            /// \brief En fonction d'un dossier renvoie la clef de l'item contenu dans le nom du dossier.
            /// \param dir_entry Le dossier correspondant à l'item.
            virtual key_type GetKeyFromFolderSavePath(const std::filesystem::directory_entry &dir_entry) const;

            /// \brief En fonction d'un dossier renvoie le nom de classe de l'item contenu dans le nom du dossier.
            /// \param dir_entry Le dossier correspondant à l'item.
            /// \remarks Ne fonctionne pas avec les espaces de noms.
            /// Cette information est à titre indicatif pour faciliter la navigation dans les dossiers.
            /// Pour être sûr du résultat privilégié la fonction GetTypeFromSave.
            /// \see GetTypeFromSave
            // virtual std::wstring GetClassNameFromFolderSavePath(const std::filesystem::directory_entry& dir_entry)
            // const;

            /// \brief En fonction d'un dossier renvoie le nom de l'item contenu dans le nom du dossier.
            /// \param dir_entry Le dossier correspondant à l'item.
            /// \remarks Un item peut ne pas avoir de nom. Si l'item à un nom, le nom du dossier en sera composé à titre
            /// indicatif pour faciliter la lecture des dossiers.
            /// \see GetBodyFromSave
            // virtual std::wstring GetNameFromFolderSavePath(const std::filesystem::directory_entry& dir_entry) const;

            /// \brief En fonction d'un dossier renvoie l'item enfant
            /// correspondant.
            /// \param dir_entry Le dossier correspondant à l'item recherché.
            /// \return L'item correspondant ou un pointeur pointant sur
            /// nullptr.
            /// \remarks Certains items ne peuvent être détruits pour être recréés.
            /// Pour faire le lien entre le dossier de sauvegarde et l'item, l'item parent devra implémenter cette
            /// fonction.
            virtual ma::ItemPtr GetItemFromFolderPath(const std::filesystem::directory_entry &dir_entry) const;

            /// \brief Permet de récupérer le texte de la sauvegarde, représentant l'instance de l'item enfant de cet
            /// item parent. \param dir_entry Le chemin du dossier de l'enfant de cet item.
            virtual std::wstring GetTextFromSave(const std::filesystem::directory_entry &dir_entry) const;

            /// Permet de récupérer l'en-tête du fichier de sauvegarde.
            /// \param dir_entry Le chemin du dossier de l'enfant de cet item.
            /// \return Un tuple contenant l'en-tête / l'identification de l'item, c'est-à-dire la clef et le type.
            /// \example
            /// \code{.cpp}
            /// {{"key", "f5ce55cf-04b2-45fd-b2ab-abd7a5a60edc"}, {"type", "Folder"}}
            /// \endcode
            virtual header_save_type GetHeaderFromSave(const std::filesystem::directory_entry &dir_entry) const;

            /// \brief Permet de récupérer la clef inscrite dans le fichier de
            /// la sauvegarde de l'item.
            /// \param dir_entry Le chemin du dossier de l'enfant de cet item.
            virtual key_type GetKeyFromSave(const std::filesystem::directory_entry &dir_entry) const;

            /// \brief Permet de récupérer le type de l'item inscrit dans le
            /// fichier de la sauvegarde de l'item.
            /// \param dir_entry Le chemin du dossier de l'enfant de cet item.
            virtual std::wstring GetTypeFromSave(const std::filesystem::directory_entry &dir_entry) const;

            /// \brief Permet de récupérer le corps du fichier de sauvegarde.
            /// \param dir_entry Le chemin du dossier de l'enfant de cet item.
            /// \return Une liste de tuples décrivant les variables de l'item.
            /// \example
            /// \code{.cpp}
            /// {
            ///     {
            ///         {"key", "m_Name"},
            ///         {"type", "StringVar"},
            ///         {"value", "print_HelloWorld.py"},
            ///         {{"__doc__", "documentation A"}, {"toto", "tata"}}
            ///     },
            ///     {
            ///         {"key", "m_CustomSavePath"},
            ///         {"type", "BoolVar"},
            ///         {"value", "false"},
            ///         {{"__doc__", "documentation B"}}
            ///     }
            /// }
            /// \endcode
            virtual body_save_type GetBodyFromSave(const std::filesystem::directory_entry &dir_entry) const;

            /// \brief Permet de récupérer le corps du fichier de sauvegarde, sous une forme plus simple à manipuler.
            /// \param dir_entry Le chemin du dossier de l'enfant de cet item.
            /// \return Un tableau associatif de clef/valeur décrivant les variables de l'item décrite dans le fichier
            /// de sauvegarde de l'item.
            /// \example
            /// \code{.cpp}
            /// {
            ///     {
            ///         {"m_Name"},
            ///         {"StringVar", "print_HelloWorld.py", {{"__doc__", "documentation A"}, {"toto", "tata"}}}
            ///     },
            ///     {
            ///         {"m_CustomSavePath"},
            ///         {"BoolVar", "false", {{"__doc__", "documentation B"}}}
            ///     }
            /// }
            /// \endcode
            virtual vars_save_type GetVarsFromSave(const std::filesystem::directory_entry &dir_entry) const;

            /// \return La liste des clefs des éléments dont dépend l'élément représenté par le dossier « dir_entry ».
            virtual dependencies_type GetDependenciesFromSave(const std::filesystem::directory_entry &dir_entry) const;

            /// \brief Lors du chargement de la sauvegarde, si l'item enfant est d'un type que la RTTI ne gère pas cette
            /// fonction sera appelée pour permettre au parent de créé l'enfant.
            /// \remarks Par défaut la fonction renvoie un pointeur sur nul.
            /// \exception std::invalid_argument L'enfant est déjà existant.
            virtual ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry);

        public:
            /// \brief Le chemin du dossier où sera sauvegarder l'item.
            /// \remarks Par défaut, il sera défini en fonction de l'item parent.
            /// Sa modification ne supprimera pas la sauvegarde actuelle.
            /// Il faudra lancer l'action de sauvegarde pour supprimer le dossier actuel de sauvegarde et créer le
            /// nouveau.
            /// Cette variable est du type « ma::DirectoryVarPtr », mais comme elle n'est pas encore déclarée au niveau
            /// d'Item.h, nous nous limitons au type « ma::VarPtr ».
            /// \warning Attention est nulle si « m_Savable » est faux.
            ma::VarPtr m_SavePath;

            /// \brief Le chemin du dossier où est sauvegardé l'item. Permet de comparer avec « m_SavePath » pour savoir
            /// s'il faut supprimer ou non le dossier actuel de la sauvegarde
            /// \remarks Cette variable est du type « ma::DirectoryVarPtr » mais comme elle n'est pas encore déclarée au
            /// niveau d'Item.h, nous nous limitons au type « ma::VarPtr ».
            /// \warning Attention est nulle si « m_Savable » est faux.
            ma::VarPtr m_LastSavePath;

            /// \brief Si vrais alors « m_SavePath » sera utilisé sinon le chemin sera déduit de son parent.
            /// \warning Attention est nulle si « m_Savable » est faux.
            ma::VarPtr m_CustomSavePath;

            /// \brief Permet de savoir si un item peut être chargé et sauvegardé.
            /// \remarks Cette variable n'est pas une « ma::Var » car son existence est justement pour alléger
            /// l'interface graphique des variables inutilisées.
            /// \warning Si l'item ne peut être sauvegardé alors les variables « m_SavePath », m_LastSavePath et
            /// « m_CustomSavePath » ne seront pas créées et pointeront sur nullptr.
            const bool m_Savable;

            /// \brief Permet de garder un item même si il n'existe pas dans la sauvegarde.
            /// \remarks Cela est utile pour recréer des nœuds dans leur état d'origine. Cela permet d'ajouter des nœùds
            /// avant le chargement des nœuds et ainsi de les conserver en vie si cela n'existe pas dans la sauvegarde.
            const bool m_CanBeDeleteOnLoad;

            /// \brief La destruction de l'item suppose que l'item ne soit possédé par aucun ItemPtr. Basiquement l'item
            /// ne fait plus partie de ms_Map de la liste des enfants de son parent et n'est plus référencé par aucun
            /// pointeur partagé.
            /// \todo Essayer de gérer les la destruction d'un élément sans générer d'exceptions.
            ///       Cela peut provoqué des comportement indéfini.
            ~Item() override;

            /// \brief Le mode de recherche d'un item.
            /// À partir d'un item, nous pouvons effectuer des recherches d'items.
            /// Elles peuvent s'effectuer localement, récursivement ou globalement.
            enum SearchMod
            {
                LOCAL, ///< La recherche directe de l'enfant
                RECURSIVE, ///< Recherche récursive de l'enfant
                GLOBAL, ///< Recherche globale parmi tous les items existants.
            };

            /// Retourne la clef de l'identifiant de l'item.
            key_type GetKey() const final;

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Gestion publique de la parenté.

            /// Retourne la clef de l'identifiant de l'item parent.
            key_type GetParentKey() const;

            /// \brief Raccourcis d'écriture pour accéder au parent d'un item.
            /// \example
            /// \code{cpp}
            /// if(auto opt = GetParentOpt<ConfigItem>())
            ///     std::wcout << opt.value()->m_ConfigFilePath->toString() << std::endl;
            /// \endcode
            template<typename T>
            std::optional<std::shared_ptr<T>> GetParentOpt() const;

            std::optional<ItemPtr> GetParentOpt() const;

            /// \brief Raccourcis pour accéder au parent.
            /// \remarks Un parent n'est accessible que si celui-ci est actif, c'est-à-dire observable, c'est-à-dire
            /// présent dans Item::ms_Map.
            /// \exception std::logic_error La clef du parent ne correspond à aucun item de ms_Map.
            template<typename T>
            std::shared_ptr<T> GetParent() const;

            std::shared_ptr<Item> GetParent() const;

            /// Renvoie au choix :
            /// \li soit un item contenu dans m_Children (local).
            /// \li soit un item contenu dans m_Children ou dans les sous-enfants
            ///   (récursif).
            /// \li soit dans ms_Map (globale).
            /// \param key La clef de l'identifiant de l'item recherché.
            /// \param mod défini le mode de recherche de l'item.
            /// \return Renvoie l'item trouvé. Si l'item n'existe pas une exception sera levée et nullptr sera retourné.
            /// \exception std::invalid_argument Si la clef ne fait référence à aucun item.
            /// \attention Si le paramètre mod n'est pas spécifié, c'est la fonction GetItem statique
            /// « ma::Item::GetItem(const key_type& key) » qui est appelée.
            ma::ItemPtr GetItem(const key_type &key, SearchMod mod) const;

            /// \return Renvoie le conteneur des items au choix :
            /// \li les enfants directs (local)
            /// \li les enfants directs et tous les sous-enfants. (récursif)
            /// \li tout les items existants. (global)
            /// \attention Si vous appelé « ma::Item::GetItems » sans paramètre, c'est la fonction
            /// « ma::Item::GetItems » statique qui est appelée.
            ItemsMap GetItems(SearchMod mod) const;

            /// \return Renvoie le conteneur des items au choix :
            /// \li les enfants directs (local)
            /// \li les enfants directs et tous les sous-enfants. (récursif)
            /// \li tout les items existants. (global)
            /// C'est la même fonction que GetItems, excepté que l'ordre des items est important.
            /// Il est retourné de telle manière que les enfants de chaque branche soient placés avant les parents.
            /// L'algorithme de parcours est dans un ordre post-fixé appelé aussi post-ordre.
            /// \see GetItems
            /// <a https://rmdiscala.developpez.com/cours/LesChapitres.html/Cours4/Chap4.8.htm" target="_blank">Parcours
            /// d'arbres</a>
            /// \example
            /// \code
            ///     . a .
            ///    .     .
            ///   b       c
            ///  . .     .
            /// d   e   f
            /// \endcode
            /// La liste d'items retournée sera dans l'un des ordres suivants :
            /// \li debfca
            /// \li edbfca
            /// \li fcdeba
            /// \li fcedba
            /// \todo il faut ordonner les enfants pour avoir un contrôle sur
            ///       l'ordre des items.
            ItemsOrder GetItemsPostOrder(SearchMod mod) const;

            /// \return Renvoie vrais si la clef est associée à un item défini dans la portée de mod.
            /// \attention Attention si vous appelez HasItem sans paramètre, c'est la fonction HasItem statique qui sera
            /// appelée.
            bool HasItem(const key_type &key, SearchMod mod) const;

            /// \brief Recherche de manière globale l'item associé à cette clef.
            /// \return Renvoie vrais si la clef est associée à item, défini par la portée de mod. Si juste renvoi
            /// l'item.
            /// \attention Attention si vous appelez HasItemOpt sans paramètre, c'est la fonction HasItemOpt statique
            /// qui est appelée.
            template<typename T>
            std::optional<std::shared_ptr<T>> HasItemOpt(const key_type &key, SearchMod mod) const;

            /// \return Renvoie vrais si la clef est associée à item, défini par la portée de mod.
            /// Si juste renvoi l'item.
            /// \attention Attention si vous appelez HasItemOpt sans paramètre, c'est la fonction HasItemOpt statique
            /// qui est appelée.
            std::optional<ma::ItemPtr> HasItemOpt(const key_type &key, SearchMod mod) const;

            /// \return
            /// \li Le nombre d'enfants direct si « mod » est égale à LOCAL
            /// \li Le nombre d'enfants et de sous enfants si « mod » est égale à RECURSIVE
            /// \li Le nombre d'items actifs globaux si « mod » est égale à GLOBAL
            /// \attention Si vous appelez GetCount sans paramètre, c'est la fonction GetCount statique qui est appelée.
            ItemsMap::size_type GetCount(SearchMod mod) const;

            /// \retrun Vrais si l'item n'a plus d'enfant.
            bool Empty() const;

            /// \brief Permet de connaître la profondeur de l'item.
            /// Dit autrement cette fonction compte le nombre de parents de l'item.
            /// \exemple
            /// - root —> 0u
            /// - root > item —> 1u
            /// - root > parent_0 > item —> 2u
            ItemsMap::size_type GetParentingDepth() const;

            /// \brief Détache l'item enfant de son précédent parent pour l'ajouter à « this ».
            /// Assigne la clef de son identifiant à la variable m_ParentKey
            /// de l'enfant:
            /// \code{.cpp}
            /// m_ParentKey = parent.GetIdKey()
            /// \endcode
            /// \remarks La clef doit faire référence à un item de ms_Map. Et ne peut être égale à ms_KeyRoot.
            /// AddChild(const key_type&) appel void AddChild(ma::ItemPtr item) c'est pour cela que seul
            /// « void AddChild(ma::ItemPtr item) » est virtuelle.
            /// Si l'item enfant n'est pas fini d'être construit (voir : GetEndConstruction()), la fonction appelante
            /// doit se charger d'appeler:
            /// \example
            /// \code{.cpp} parent->UpdateObservations({{ms_KeyObsRemoveItemEnd,
            /// child_key}});
            /// \endcode
            /// \remarks Une fois la construction de l'enfant terminée.
            /// À noter que EndConstruction() doit être appelé après l'appel à UpdateObservations().
            /// EndConstruction() met à jour les observations sur l'enfant.
            /// On met à jour d'abord le parent et ensuite l'enfant.
            /// Ce qui permet de construire ou mettre à jour les structures d'observations du parent avant celles de
            /// l'enfant. Ce qui permet par exemple de construire la vue de l'arbre des items.
            /// \exception std::runtime_error la clef fait référence à la racine de tous les items ou à ms_KeyNoParent
            /// ou la clef n'est pas valide.
            /// \see ma::Id::IsValid
            void AddChild(const key_type &);

            /// \brief Ajoute l'item enfant à sa liste d'items.
            /// Les enfants sont donc actifs et ils sont donc ajoutés à ms_Map.
            /// Assigne la clef de son identifiant à la variable m_ParentKey de l'enfant (m_ParentKey =
            /// parent.GetIdKey()). Si l'item est attaché à un autre parent, il en sera détaché.
            /// \remarks Lorsque l'on a désactivé un item, c'est-à-dire effacer sa clef et celles de ses enfants de
            /// ms_Map, pour le réactiver, il faut le placer dans un parent avec cette fonction.
            /// Pour surcharger la méthode d'ajout d'un item, surcharger celle-ci.
            /// \exception std::runtime_error l'item que vous tentez d'ajouter est la racine de
            /// tous les items.
            /// \exception std::invalid_argument Vous tentez d'ajouter l'item à lui-même.
            /// \exception std::invalid_argument l'item que vous tentez d'ajouter, a été refusé par ce parent.
            /// Veuillez utiliser la fonction membre « AcceptChild » pour tester si le parent désiré acceptera l'item.
            /// \todo créer un OnAddChild en virtuel pour éviter que le comportement soit modifié.
            virtual void AddChild(ma::ItemPtr item);

            /// Efface l'item de m_Children et de ms_Map. Tous les enfants et sous-enfants de l'item sont détachés de
            /// ms_Map. La variable m_ParentKey, de l'item enfant, se voit assigner l'identifiant ms_KeyNoParent.
            /// \exception std::runtime_error Si la clef n'est pas présente dans m_Children ou la clef n'est pas valide.
            /// \see ma::Id::IsValid.
            /// \remarks Fait appel à RemoveChild(ma::ItemPtr item).
            void RemoveChild(const key_type &);

            /// \brief Efface l'item de m_Children et de ms_Map.
            /// Tous les enfants et sous-enfants de l'item sont détachés de ms_Map.
            /// La variable m_ParentKey, de l'item enfant, se voit assigner
            /// l'identifiant ms_KeyNoParent.
            /// \exception std::runtime_error Si la clef n'est pas présente dans m_Children ou la clef n'est pas valide.
            /// \see ma::Id::IsValid.
            /// \remarks Surcharger cette fonction pour
            /// \todo créer un OnRemoveChild en virtuel pour éviter que le comportement soit modifié.
            virtual void RemoveChild(ma::ItemPtr item);

            /// \brief Efface les items enfants de m_Children et de ms_map.
            /// Pour chaque item détaché ses enfants et sous-enfants sont détachés de ms_Map.
            /// La variable m_ParentKey, de chaque item détaché, se voit
            /// assigner l'identifiant ms_KeyNoParent.
            /// \todo Implémenter une méthode plus rapide que d'appeler pour chaque enfant RemoveChild.
            virtual void RemoveChildren();

            /// Raccourcis d'écriture qui permet de retirer l'item de son parent.
            /// \exception std::invalid_argument Si le parent n'est pas dans ms_Map ou si l'item n'a pas de parent.
            virtual void Remove();

            /// \return Vrais si le parent accepte d'avoir pour enfant cet item.
            /// \remarks
            /// Le comportement par défaut est le suivant :
            ///  1. Si aucune classe n'est listée dans la liste blanche « GetWhiteListParents » et si aucune classe
            /// n'est listée dans la liste noire « GetBlackListParents » alors tous les types dérivants d'Item (Item
            /// inclus) seront acceptés.
            ///  2. Si GetWhiteListParents et/ou GetBlackListParents renvoie une liste non vide de noms de classe voici
            ///  le comportement :
            /// \li Si la liste blanche est vide alors tous les types d'items sont acceptés sauf ceux ayant leur nom de
            /// classe ou héritant d'un des noms de classe référencés dans la liste noire.
            /// \li Si la liste blanche n'est pas vide alors seulement les noms de classe qui y sont référencés seront
            /// acceptés sauf si l'un d'eux fait partie ou hérite de la liste noire. Cette dernière étant prioritaire.
            /// Attention l'héritage ne sera vérifier que si :
            /// GetItem<ma::ClassInfoManager>(ms_KeyClassInfoManager) existe.
            /// \example
            /// \code{.cpp}
            /// auto child = ma::Item::CreateItem({ma::Item::ms_KeyNoParent});
            /// if(item_parent->AcceptToAddChild(child))
            ///     item_parent->AddChild(child);
            /// \endcode
            virtual bool AcceptToAddChild(const ma::ItemPtr &) const;

            /// \return vrais si l'item parent accepte de supprimer cet enfant.
            /// \see AcceptToAddChild
            /// \remarks Par défaut renvoie vrai si la clef représente un enfant direct de l'item sinon faux.
            virtual bool AcceptToRemoveChild(const key_type &) const;

            /// \brief Défini si l'enfant accepte d'avoir pour parent l'item passé en paramètre.
            /// \param item Représente le futur parent à accepter ou non.
            /// \return Vrais si cette instance accepte d'avoir pour parent cet item.
            /// \remarks Par défaut renvoie vrai sauf si le pointeur sur l'item est vide.
            virtual bool AcceptParent(const ma::ItemPtr &item) const;

            /// À titre indicatif, le comportement de AcceptToAddChild peut être modifié et ainsi ne plus dépendre
            /// seulement du type de l'item à tester.
            virtual ClassNames GetAuthorisedChildrenType() const;

            /// À titre indicatif, le comportement de AcceptToAddChild peut être modifié et ainsi ne plus dépendre
            /// seulement du type de l'item à tester.
            virtual ClassNames GetProhibitedChildrenType() const;

            /// À titre indicatif, le comportement de AcceptParent peut être modifié et ainsi ne plus dépendre seulement
            /// du type du parent à tester.
            virtual ClassNames GetAuthorisedParentsType() const;

            /// À titre indicatif, le comportement de AcceptParent peut être modifié et ainsi ne plus dépendre seulement
            /// du type du parent à tester.
            virtual ClassNames GetProhibitedParentsType() const;

            /// Permet de récupérer les dépendances statiques et dynamiques de l'élément.
            /// Si l'élément possède des dépendances elles seront définies dans une variable nommé par la fonction
            /// « ma::Item::Key::GetDependenciesData() ».
            /// \see ma::Item::Key::Var::GetDependenciesData
            /// \see ma::TypeInfo::GetDependenciesData()
            /// \see ma::TypeInfo::GetDependenciesData(const std::wstring& class_name)
            /// \see ma::Item::dependencies_type
            virtual dependencies_type GetDependencies() const;

            /// Ajoute en contrôle l'unicité des dépendances.
            /// \remarks Ajoute à la fin les nouvelles dépendances.
            void AddDependencies(const std::vector<ma::Item::key_type> &item_keys);

            /// Conserve l'ordre et contrôle l'unicité.
            /// Supprime ceux n'étant pas dans item_keys.
            /// Aucun item ne sera supprimé puis ajouté. Ce qui évitera des opérations d'observation en trops.
            void SetDependencies(const std::vector<ma::Item::key_type> &item_keys);

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Gestion publique des variables

            /// Permet d'ajouter une variable à l'item.
            /// \return renvoie le pointeur intelligent qui s'occupe de variable ajoutée.
            /// \exception std::invalid_parameter la variable est déjà associée à un item ou à ce même item.
            /// \remarks La propriété de la variable est déléguée aux VarPtr qui la gèrera et la libérera le moment
            /// venu.
            template<typename T, typename... Args>
            std::shared_ptr<T> AddVar(const VarsMap::key_type &key, const Args &...args);

            /// Tout comme AddVar mais la variable n'est pas accessible qu'en lecture. Cela ne veut pas dire que la
            /// valeur ne peut pas être modifiée. L'item possédant la variable peut la modifier.
            template<typename T, typename... Args>
            std::shared_ptr<T> AddReadOnlyVar(const VarsMap::key_type &key, const Args &...args);

            /// Créé une copie de la variable. La variable doit provenir d'un autre item.
            /// \param var La variable à copier.
            /// \exception std::invalid_argument une variable ayant la même clef existe déjà.
            /// \exception std::invalid_argument L'item contenant cette variable est celui-là même.
            template<typename T>
            std::shared_ptr<T> CopyVar(const std::shared_ptr<T> &var);

            /// Créé une copie de la variable. La variable doit provenir d'un autre item.
            /// \param var La variable à copier.
            /// \exception std::invalid_argument une variable ayant la même clef existe déjà.
            /// \exception std::invalid_argument L'item contenant cette variable est celui-là même.
            virtual ma::VarPtr CopyVar(const ma::VarPtr &var);

            /// Efface du registre de l'item la variable associée à la clef.
            /// \exception std::invalid_parameter la variable n'est pas associée à cet item.
            /// \exception std::invalid_parameter la variable est membre de l'item. Pour vérifier si la variable est
            /// membre ou non d'un item, il faut utiliser « Var::m_Dynamic ». \see ma::Var::m_Dynamic
            void EraseVar(const VarsMap::key_type &);

            /// \return la variable associée à la clef.
            /// \exception std::invalid_parameter la variable n'est pas associée à cet item.
            /// \exception std::invalid_parameter la variable n'est pas du type T.
            template<typename T>
            std::shared_ptr<T> GetVar(const VarsMap::key_type &key) const;

            /// \return la variable associée à la clef.
            /// \exception std::invalid_parameter la variable n'est pas associée à cet item.
            ma::VarPtr GetVar(const VarsMap::key_type &) const;

            /// Permet de savoir si l'item est associé à cette variable.
            /// \return Renvoie vrais si l'item est associé à cette variable.
            bool HasVar(const VarsMap::key_type &) const;

            /// \return Renvoie vrais si la clef est associée à une variable de l'item.
            /// Si juste renvoi la variable.
            /// \exception std::invalid_parameter la variable n'est pas du type T.
            template<typename T>
            std::optional<std::shared_ptr<T>> HasVarOpt(const VarsMap::key_type &key) const;

            /// \return Renvoie vrais si la clef est associée à une variable de l'item.
            /// Si juste renvoi la variable.
            std::optional<ma::VarPtr> HasVarOpt(const VarsMap::key_type &key) const;

            /// \return Renvoie une copie de la liste des variables.
            VarsMap GetVars() const;

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Gestion publique des observations

            /// \see ma::Observable::IsObservable()
            bool IsObservable() const override;

            /// \remarks en public pour que les éléments utilisant la « RTTI » puissent appeler « EndConstruction » si
            /// nécessaire.
            /// Si vous surchargez cette fonction n'oubliez pas d'appeler « ma::Item::EndConstruction() ».
            void EndConstruction() override;

            /// Une fois « BeginBatch() » appelé la mise à jour sera bloquée jusqu'à l'appel de « EndBatch() »
            /// \remarks La fonction « BeginBatch() » sera appelée de manière récursive.
            /// Toutes les variables, tous les enfants verront leur fonction « BeginBatch() » appelée.
            /// \see ma::Observable::BeginBatch()
            void BeginBatch() override;

            /// Une fois « BeginBatch() » appelé la mise à jour sera bloquée jusqu'à l'appel de « EndBatch() »
            /// \remarks La fonction « BeginBatch() » sera appelée de manière récursive.
            /// Toutes les variables, tous les enfants verront leur fonction « BeginBatch() » appelée.
            /// \see ma::Observable::BeginBatch()
            void EndBatch() override;

            // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Gestion publique du chargement et de la sauvegarde.

            /// \brief Sauvegarde les attributs de l'instance dans un fichier « vars.json ».
            /// Le fichier est placé dans le dossier renvoyé par la fonction GetFolderPath() du parent de l'instance.
            /// Si l'item ne possède pas de parent, il utilisera le chemin par défaut renvoyé par
            /// « ma::Savable::GetFolderPath() ». Un item est sauvegardé de cette manière.
            /// Un dossier avec son nom et son identifiant sera créé par son parent :
            /// \example
            /// « 00000000-0000-0000-0000-000000000002_root »
            ///
            /// Ce dossier contiendra:
            /// \li Un fichier « vars.cpp » contenant la sérialisation des variables de l'item.
            /// \li La sauvegarde de chaque enfant.
            /// \remarks Seul l'id contenu dans le nom du dossier sera utilisé.
            /// Le nom est à titre indicatif.
            /// Les dossiers enfants qui ne représentent plus les enfants de l'item seront supprimés.
            virtual void Save();

            typedef std::unordered_map<key_type, std::wstring> key_errors;
            /// Charge les enfants de l'instance depuis la liste des dossiers contenus dans celui représentant cet item.
            /// \see ma::Savable::Save()
            /// \return La liste éléments n'ayant pas été chargé, avec l'erreur associée.
            /// \remarks Doit être appelé avant LoadVars.
            virtual key_errors LoadItems();

            /// Charge les variables de l'item puis ceux des enfants.
            /// \remarks Doit être appelé une fois la structure des items chargée, sinon des variables pointant sur des
            /// items pourraient ne pas trouver leur item.
            virtual void LoadVars();

            /// Supprime les fichiers et les dossiers de la sauvegarde de l'item.
            virtual void DeleteSaveFolder();

            /// \brief Retourne le chemin du dossier contenant le fichier à sauvegarder.
            /// \param vars_loading Si vrais alors l'appel à GetFolderPath est effectué lors du chargement des
            /// variables. Cela permet d'utiliser m_LastFolderPath qui ne sera pas perturbé par les éventuelles mises à
            /// jour de m_SavePath et feront pointer les enfants sur des faux chemins.
            /// \todo chemins relatifs excepté pour la racine ou celui ayant un chemin ne dépendant pas de son parent.
            virtual std::filesystem::path GetFolderPath(bool vars_loading = false) const;

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Récupération des types autorisés ou non pour être enfant ou parent de cet item.

            /// Permet de connaître la liste des classes d'item qu'un type d'item peut avoir en tant qu'enfant.
            ClassNames GetWhiteListChildren() const;

            /// Permet de connaître la liste des classes d'item qu'un type d'item ne peut pas avoir en tant qu'enfant.
            ClassNames GetBlackListChildren() const;

            /// Permet de connaître la liste des classes d'item qu'un type d'item peut avoir en tant que parent.
            ClassNames GetWhiteListParents() const;

            /// Permet de connaître la liste des classes d'item qu'un type d'item ne peut pas avoir en tant que parent.
            ClassNames GetBlackListParents() const;

            // —————————————————————————————————————————————————————————————————————————————————————————————————————————
            //  Évènements d'un item

        protected:
            /// L'item est actif si « ma::Enable::m_Enable » est à vrais.
            void OnEnable() override;

            /// L'item est actif si « ma::Enable::m_Enable » est à faux.
            void OnDisable() override;

        public:
            M_HEADER_CLASSHIERARCHY(Item)
    };

    /// \class Var
    /// \brief Une variable permet de stocker une donnée d'un type donnée.
    /// Elle permet de récupérer la donnée sous la forme d'une chaîne de caractère et la modifier sous la forme d'une
    /// chaîne de caractères.
    /// Une variable appartient à un item et elle n'est pas ???? et ni ne représente un « ma::Item ».
    class M_DLL_EXPORT Var: public std::enable_shared_from_this<ma::Var>, public ma::Observable
    {
        public:
            /// \brief Le type de la clef de la variable.
            /// \see ma::Var::m_Key
            typedef VarsMap::key_type key_type;

            /// \brief Le type du paramètre du constructeur des variables.
            /// \see ma::Var::ConstructorParameters
            /// \remarks Est déclaré avant _AddVar car
            /// ma::Var::ConstructorParameters est déclaré après « ma::Item »
            struct ConstructorParameters
            {
                    /// \copydoc ma::Var::m_Key
                    ma::VarsMap::key_type m_Key;

                    /// \copydoc ma::Var::m_Dynamic
                    bool m_Dynamic;

                    /// \copydoc ma::Var::m_ReadOnly
                    bool m_ReadOnly;

                    /// \copydoc ma::Var::m_Savable
                    bool m_Savable;

                    /// \brief Constructeur de « ConstructorParameters »
                    /// \param key @copybrief ma::Var::ConstructorParameters::m_Key
                    /// \param dynamic @copybrief ma::Var::m_Dynamic
                    /// \param read_only @copybrief ma::Var::m_ReadOnly
                    /// \param savable @copybrief ma::Var::m_Savable
                    ConstructorParameters(const ma::VarsMap::key_type &key,
                                          bool dynamic = true,
                                          bool read_only = false,
                                          bool savable = true);
                    virtual ~ConstructorParameters();
            };

            friend void Item::SetVarFromString(const VarsMap::key_type &key, const std::wstring &value);

            /// Pour surcharger SetFromItem reporter ces deux lignes dans la nouvelle définition de classe.
            template<typename T_Var, typename... Args>
            friend void Item::SetVarFromValue(const VarsMap::key_type &key, const Args &...args);

        private:
            /// C'est à l'item de modifier la clef de la variable qui l'associe.
            /// Pour se faire les fonctions membres de Item doivent être amies.
            friend class ma::Item;

        protected:
            /// Permet de stocker diverses informations sur l'instance de la variable.
            /// \example
            /// \code{.cpp}
            /// {
            ///     {"doc", "C'est une variable qui sert à faire des choses"},
            ///     {"author", "Toto Toto"},
            ///     {"script", "add_variable_exemple.py"
            /// }
            /// \endcode
            /// \remarks Pour raccourcir le nom « Instance Info », « IInfo » sera souvent employé.
            /// \see ms_KeyObsBeginSetIInfo
            /// \see ms_KeyObsEndSetIInfo
            /// \see ms_KeyObsBeginAddIInfo
            /// \see ms_KeyObsEndAddIInfo
            /// \see ms_KeyObsBeginRemoveIInfo
            /// \see ms_KeyObsEndRemoveIInfo
            /// \see GetIInfo
            /// \see InsertIInfoKeyValue
            /// \see InsertIInfo
            /// \see GetIInfoValue
            /// \see HasIInfoKey
            /// \see RemoveIIKeyValue
            ma::InstanceInfo m_InstanceInfo;

            /// La clef de l'item associé.
            ma::Item::key_type m_KeyItem;

            /// Concrétisation de la classe Observable.
            /// \see ma::Observable::GetPtr()
            ObservablePtr GetPtr() const override;

            /// Raccourcis d'écriture.
            template<typename T>
            std::shared_ptr<T> GetPtr() const;

            /// Destiné à l'item possédant la variable afin qu'il puisse la modifier même si celle-ci est en lecture
            /// seul.
            virtual void SetFromItem(const std::wstring &value) = 0;

            /// Permet de copier une variable via la fonction
            /// ma::Item::CopyVar(const ma::VarPtr& var)
            /// \return La copie de la variable,
            /// \remarks Elle n'est liée à aucun item. La variable doit être ajoutée à un item.
            /// \see ma::Item::CopyVar(const ma::VarPtr& var)
            virtual ma::VarPtr Copy() const = 0;

        public:
            struct Key
            {
                    struct Obs
                    {
                            /// Clef qui permettra à un observateur de variable de reconnaître un évènement qui se
                            /// déclenche juste avant la modification de la valeur de la variable.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsBeginSetValue, this->GetKey()+ ms_Sep +
                            /// this->toString()}});
                            /// \endcode
                            static const ma::Observable::Data::key_type ms_BeginSetValue;

                            /// Clef qui permettra à un observateur de variable de reconnaître un évènement qui se
                            /// déclenche juste après la modification de la valeur de la variable.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_KeyObsEndSetValue, this->GetKey() + ms_Sep +
                            /// this->toString()}});
                            /// \endcode
                            static const ma::Observable::Data::key_type ms_EndSetValue;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_BeginSetIInfo;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_EndSetIInfo;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_BeginSetIInfoValue;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_EndSetIInfoValue;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_BeginAddIInfo;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_EndAddIInfo;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_BeginRemoveIInfo;

                            /// \see m_InstanceInfo
                            static const ma::Observable::Data::key_type ms_EndRemoveIInfo;
                    };

                    struct IInfo
                    {
                            /// Clef qui sera utilisée pour documenter une variable.
                            /// \example
                            /// \code{.cpp}
                            ///     InsertIInfoKeyValue(ma::Var::ms_KeyDocIInfo, "Cette variable sert à faire...")
                            /// \endcode
                            static const ma::InstanceInfo::key_type ms_Doc;
                    };
            };

            /// Clef qui permet d'indiquer qu'elle est l'interface graphique souhaitée pour afficher la variable.
            /// La valeur représente le nom de classe héritant de « ma::Observer ».
            /// \see wxVar
            /// \see wxVariable.h
            static const ma::Observable::Data::key_type ms_KeyGUI;

            /// Séparateur pour les données des évènements dans le cas où il est nécessaire de lier la clef de la
            /// variable et sa valeur.
            /// \example
            /// \code{.cpp} this->UpdateObservations({{ms_KeyObsEndSetValue,
            /// this->GetKey() + ms_Sep + this->toString()}});
            /// \endcode
            static const ma::Observable::Data::key_type ms_Sep;

            /// \brief Permet de
            /// \param str "clef de la variable, valeur sous la forme d'une chaîne de caractères" soit "clef,
            /// valeur_str".
            /// \return une paire {clef, valeur_str}.
            /// \example
            /// \code{.cpp}
            /// if(data.HasActions(ma::Var::ms_KeyObsBeginSetValue))
            /// {
            ///     auto previous_key_item =
            ///     ma::Var::GetKeyValue(data.GetFirstAction(ma::Var::ms_KeyObsBeginSetValue)).second;
            /// \endcode
            static std::pair<std::wstring, std::wstring> GetKeyValue(const ma::Var::Action::second_type &str);

            /// \brief La clef de la variable.
            /// \example
            /// \code{.cpp}
            /// L"m_Name"s
            /// \endcode
            const key_type m_Key;

            /// \brief Permet de savoir si la variable a été créée
            /// dynamiquement.
            ///
            /// C'est-à-dire que la variable n'est pas une donnée membre par défaut de la classe.
            /// Si une variable n'est pas dynamique et que vous tentez de la supprimer de son item
            /// « ma::Item::EraseVar » déclenchera une exception.
            /// \example Variable dynamique :
            /// \code{.cpp}
            ///     m_Item->AddVar(new ma::DefineVar(L"m_DefineVar"s));
            /// \endcode
            /// Exemple Variable membre (non dynamique):
            /// \code{.cpp}
            ///     void ExempleItem::InitVars()
            ///     {
            ///         m_DefineVar = AddMemberVar(new ma::DefineVar("m_DefineVar"));
            ///     }
            /// \endcode
            const bool m_Dynamic;

            /// \brief Permet de savoir si la variable est en lecture seul.
            const bool m_ReadOnly;

            /// \brief Permet de savoir si la variable doit être sauvegardée.
            const bool m_Savable;

            /// Constructeur par défaut
            /// \param params \copydoc ma::Var::ConstructorParameters
            /// \remarks m_KeyItems est initialisé avec la valeur de « ma::Item::ms_KeyNoParent ».
            /// Il vous faudra utiliser « ma::Item::AddVar » après avoir instancié la variable.
            /// \example
            /// \code{.cpp}
            ///     m_DefineVar = AddVar<ma::DefineVar>("m_DefineVar");
            /// \endcode
            explicit Var(const ma::Var::ConstructorParameters &params);
            Var() = delete;
            Var(const Var &) = delete;
            ~Var() override;

            /// Permet de générer les paramètres de construction de la variable.
            virtual ma::Var::ConstructorParameters GetConstructorParameters() const;

            /// \return Une chaîne de caractère représentant la valeur de la variable.
            virtual std::wstring toString() const = 0;

            /// Si nous souhaitons sous forme de chaîne de caractères représenter la valeur dans une structure, il faut
            /// pouvoir demander cette représentation à la variable.
            /// \example
            /// valeur : L"toto"s
            /// \code{.cpp}
            /// std::wcout << var->toString() : toto
            /// std::wcout << var->toStringRepresentation() : "toto"
            /// \endcode
            /// valeur : 1u
            /// \code{.cpp}
            /// std::wcout << var->toString() : 1
            /// std::wcout << var->toStringRepresentation() : 1
            /// \endcode
            virtual std::wstring toStringRepresentation() const;

            /// \return Une chaîne de caractère représentant une paire contenant
            /// la clef et la valeur de la variable.
            /// S'il n'y a pas de valeur comme un « define » la valeur renvoyée sera vide.
            /// \example
            /// \code
            /// {"m_Name", ""}
            /// \endcode
            /// \remarks « toString » ne permet pas de savoir si la chaîne de caractères renvoyée doit être encadrée par
            /// des guillemets ou non. Grâce à cette fonction le problème est réglé.
            virtual std::wstring GetKeyValueToString() const = 0;

            /// \exception std::logic_error La variable est en lecture seul.
            virtual void fromString(const std::wstring &) = 0;

            /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
            /// \remarks À utiliser avant d'appeler fromString.
            virtual bool IsValidString(const std::wstring &) const = 0;

            /// Remet la valeur par défaut.
            /// \remarks Si la variable est en lecture seule une exception sera tout de même déclenchée.
            /// \exception std::logic_error La variable est en lecture seul.
            virtual void Reset() = 0;

            /// Retourne la clef de l'identifiant de la variable.
            /// \remarks Concrétisation de classe « ma::Observable ».
            key_type GetKey() const final;

            /// Renvoie la clef de l'item associé.
            ma::Item::key_type GetItemKey() const;

            /// \return les informations sur l'instance de cette variable.
            virtual ma::InstanceInfo GetIInfo() const;

            /// Insertion d'un couple
            /// \code{.cpp}
            /// InsertIInfoKeyValue("clef0", "valeur0")
            /// \endcode
            virtual void InsertIInfoKeyValue(const ma::InstanceInfo::key_type &key,
                                             const ma::InstanceInfo::mapped_type &value);

            /// Insertion de couples
            /// \code{.cpp}
            /// InsertIInfo({{"clef0", "valeur0"}, {"clef1", "valeur1"}})
            /// \endcode
            virtual void InsertIInfo(const ma::InstanceInfo &);

            /// \return La valeur associée à la clef.
            /// \exception std::invalid_argument la clef n'existe pas
            virtual ma::InstanceInfo::mapped_type GetIInfoValue(const ma::InstanceInfo::key_type &key);

            /// \return Vrais si la clef existe sinon faux.
            virtual bool HasIInfoKey(const ma::InstanceInfo::key_type &key);

            /// Supprime la paire clef, valeur.
            /// \exception std::invalid_argument la clef n'existe pas
            virtual void RemoveIInfoKeyValue(const ma::InstanceInfo::key_type &key);

            /// \return Le nombre de couples clef, valeurs d'information d'instance.
            virtual size_t GetIInfoCount();

            /// \see ma::Observable::IsObservable()
            bool IsObservable() const override;

        public:
            M_HEADER_CLASSHIERARCHY(Var)
    };
} // namespace ma

#include "ma/Item.tpp"
