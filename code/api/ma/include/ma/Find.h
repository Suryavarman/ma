/// \file Find.h
/// \author Pontier Pierre
/// \date 2020-03-11
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Dll.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include <map>

namespace ma
{
    template<typename T, typename T_Item>
    struct Match
    {
            typedef T value_type;
            typedef T_Item item_type;
            typedef std::shared_ptr<T_Item> item_ptr;

            /// \return Vrais si l'item correspond aux critères de recherche, sinon
            /// Faux.
            virtual bool operator()(item_ptr &) const = 0;

            /// \return la valeur qui sera utilisée comme identifiant.
            /// Cela permet de stocker les items
            virtual T GetValue(item_ptr &) const = 0;

            virtual ~Match();
    };

    /// Permet de retrouver les items héritant de T_Item. Ils seront triés par la clef de l'item.
    /// \code{.cpp}
    /// typedef ma::MatchType<ma::Savable> MatchSavable;
    ///
    /// auto savable = ma::Find<MatchSavable>::GetDeque(item->GetKey(), MatchSavable(), ma::Item::SearchMod::LOCAL);
    /// \endcode
    template<typename T_Item>
    struct MatchType: public Match<ma::Item::key_type, T_Item>
    {
            MatchType();
            virtual ~MatchType();

            bool operator()(std::shared_ptr<T_Item> &) const override;

            ma::Item::key_type GetValue(std::shared_ptr<T_Item> &) const override;
    };

    /// Permet de retrouver un item héritant d'un type d'item défini. Les items seront triés selon la valeur de la
    /// variable de type T et ayant pour identifiant m_Key.
    /// \remarks Ne fonctionne pas pour le type « ma::DefineVar ».
    /// \code{.cpp}
    /// typedef ma::MatchVar<ma::DirectoryVar, ma::Folder> MatchFolder;
    ///
    /// ma::Folder::MatchFolder matcher({ma::Folder::ms_KeyPath});
    ///
    /// auto folders = ma::Find<MatchFolder>::GetHashMap(item->GetKey(), matcher, ma::Item::SearchMod::LOCAL);
    /// \endcode
    template<typename T_Var, typename T_Item>
    struct MatchVar: public Match<typename T_Var::value_type, T_Item>
    {
            typedef T_Var var_type;

            const ma::Var::key_type m_Key;

            explicit MatchVar(ma::Var::key_type key);
            virtual ~MatchVar();

            bool operator()(std::shared_ptr<T_Item> &) const override;

            typename T_Var::value_type GetValue(std::shared_ptr<T_Item> &) const override;
    };

    /// Permet d'utiliser les foncteurs de type Match pour récupérer un conteneur d'items sélectionné par le foncteur de
    /// type Match.
    ///
    /// Exemples:
    /// 1 - On cherche les items du type « Savable » enfant d'item.
    ///
    /// \code{.cpp}
    /// typedef ma::MatchType<ma::Savable> MatchSavable;
    /// auto savable = ma::Find<MatchSavable>::GetDeque(item->GetKey(), MatchSavable(), ma::Item::SearchMod::LOCAL);
    /// \endcode
    ///
    /// 2 - Permet de retrouver les items :
    ///         - héritant de « ma::Folder »
    ///         - possédant une variable de type Directory et ayant une clef définie lors de la création du « matcher ».
    /// Les items seront triés selon la valeur de la variable.
    ///
    /// \code{.cpp}
    /// typedef ma::MatchVar<ma::DirectoryVar, ma::Folder> MatchFolder;
    ///
    /// ma::Folder::MatchFolder matcher({"m_Path"});
    /// auto folders = ma::Find<MatchFolder>::GetHashMap(item->GetKey(), matcher, ma::Item::SearchMod::LOCAL);
    /// \endcode
    template<typename T_Match>
    struct Find
    {
            typedef typename T_Match::value_type value_type;
            typedef typename T_Match::item_ptr item_ptr;

            typedef std::deque<item_ptr> deque;
            typedef std::set<item_ptr> set;
            typedef std::map<value_type, item_ptr> map;
            typedef std::multimap<value_type, item_ptr> multimap;
            typedef std::unordered_set<item_ptr> unordered_set;
            typedef std::unordered_map<value_type, item_ptr> unordered_map;
            typedef std::unordered_multimap<value_type, item_ptr> unordered_multimap;

            static deque GetDeque(const ma::Item::key_type &, const T_Match &match, ma::Item::SearchMod search_mod);
            static set GetSet(const ma::Item::key_type &, const T_Match &match, ma::Item::SearchMod search_mod);
            static map GetMap(const ma::Item::key_type &, const T_Match &match, ma::Item::SearchMod search_mod);
            static multimap
            GetMultiMap(const ma::Item::key_type &, const T_Match &match, ma::Item::SearchMod search_mod);
            static unordered_set
            GetHashSet(const ma::Item::key_type &, const T_Match &match, ma::Item::SearchMod search_mod);
            static unordered_map
            GetHashMap(const ma::Item::key_type &, const T_Match &match, ma::Item::SearchMod search_mod);
            static unordered_multimap
            GetHashMultiMap(const ma::Item::key_type &, const T_Match &match, ma::Item::SearchMod search_mod);

            /// Permet de rechercher des items dans un parent non actif / non observable / en-dehors de ms_Map.
            static deque GetDeque(Item *parent, const T_Match &match, ma::Item::SearchMod search_mod);
            static set GetSet(Item *parent, const T_Match &match, ma::Item::SearchMod search_mod);
            static map GetMap(Item *parent, const T_Match &match, ma::Item::SearchMod search_mod);
            static multimap GetMultiMap(Item *parent, const T_Match &match, ma::Item::SearchMod search_mod);
            static unordered_set GetHashSet(Item *parent, const T_Match &match, ma::Item::SearchMod search_mod);
            static unordered_map GetHashMap(Item *parent, const T_Match &match, ma::Item::SearchMod search_mod);
            static unordered_multimap
            GetHashMultiMap(Item *parent, const T_Match &match, ma::Item::SearchMod search_mod);
    };

} // namespace ma

#include "ma/Find.tpp"
