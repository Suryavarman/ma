/// \file engine/Node.h
/// \author Pontier Pierre
/// \date 2022-03-26
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

/// m_Transform:
/// La transformation du nœud en lecture seul.
/// Le nœud observe m_Position, m_Quaternion et m_Scalar.
/// Il reporte les modifications dans m_Transform.
///
/// m_Position:
/// La position du nœud.
/// La modification de celui-ci sera reportée dans m_Transform.
///
/// m_Quaternion et m_Euler:
/// L'orientation du nœud.
/// La modification de celui-ci sera reportée dans m_Transform.
///
/// m_Scale:
/// L'échelle du nœud.
/// La modification de celui-ci sera reportée dans m_Transform.
///
/// m_WhiteListParents:
/// Seul un parent de type Node peut être accepté
///
/// EndConstruction():
/// Permet d'initier les observations des variables.
/// \todo faire des accès par référence aux valeurs des variables position, quaternion, euler et scale.
#define ENGINE_NODE_HEADER(in_M_scalar_type)                                                                           \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Engine                                                                                               \
        {                                                                                                              \
            class M_DLL_EXPORT Node##in_M_scalar_type: public ma::Item, public ma::Observer                            \
            {                                                                                                          \
                protected:                                                                                             \
                    virtual void SetObservation(const ma::ObservablePtr &observable);                                  \
                    void OnEnable() override;                                                                          \
                    void OnDisable() override;                                                                         \
                                                                                                                       \
                public:                                                                                                \
                    typedef ma::Eigen::Transform##in_M_scalar_type::Scalar scalar_type;                                \
                    typedef ma::Eigen::Vector3##in_M_scalar_type position_type;                                        \
                    typedef ma::Eigen::Quaternion##in_M_scalar_type quaternion_type;                                   \
                    typedef ma::Eigen::Vector3##in_M_scalar_type euler_type;                                           \
                    typedef ma::Eigen::Vector3##in_M_scalar_type scale_type;                                           \
                    typedef ::Eigen::Translation<scalar_type, 3> translation_type;                                     \
                                                                                                                       \
                    ma::StringVarPtr m_Name;                                                                           \
                                                                                                                       \
                    ma::Eigen::Transform##in_M_scalar_type##VarPtr m_Transform;                                        \
                                                                                                                       \
                    ma::Eigen::Vector3##in_M_scalar_type##VarPtr m_Position;                                           \
                    ma::Eigen::Quaternion##in_M_scalar_type##VarPtr m_Quaternion;                                      \
                    ma::Eigen::Vector3##in_M_scalar_type##VarPtr m_Euler;                                              \
                    ma::Eigen::Vector3##in_M_scalar_type##VarPtr m_Scale;                                              \
                                                                                                                       \
                    const ma::StringHashSetVarPtr m_WhiteListParents;                                                  \
                                                                                                                       \
                    explicit Node##in_M_scalar_type(const ma::Item::ConstructorParameters &in_params,                  \
                                                    const std::wstring &in_name = {});                                 \
                    Node##in_M_scalar_type() = delete;                                                                 \
                    ~Node##in_M_scalar_type() override;                                                                \
                                                                                                                       \
                    void EndConstruction() override;                                                                   \
                                                                                                                       \
                    void BeginObservation(const ObservablePtr &observable) override;                                   \
                    void UpdateObservation(const ObservablePtr &observable,                                            \
                                           const ma::Observable::Data &data) override;                                 \
                    void EndObservation(const ObservablePtr &) override;                                               \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Node##in_M_scalar_type, Engine)                             \
            };                                                                                                         \
            typedef std::shared_ptr<ma::Engine::Node##in_M_scalar_type> Node##in_M_scalar_type##Ptr;                   \
        }                                                                                                              \
    }                                                                                                                  \
    M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Node##in_M_scalar_type, Engine)

#define ENGINE_NODE_CPP(in_M_scalar_type)                                                                              \
    ma::Engine::Node##in_M_scalar_type::Node##in_M_scalar_type(const ma::Item::ConstructorParameters &in_params,       \
                                                               const std::wstring &in_name):                           \
    ma::Item(in_params),                                                                                               \
    m_Name(AddMemberVar<ma::StringVar>(L"m_Name", in_name)),                                                           \
    m_Transform(AddReadOnlyMemberVar<ma::Eigen::Transform##in_M_scalar_type##Var>(L"m_Transform")),                    \
    m_Position(AddMemberVar<ma::Eigen::Vector3##in_M_scalar_type##Var>(L"m_Position", position_type::Zero())),         \
    m_Quaternion(                                                                                                      \
        AddMemberVar<ma::Eigen::Quaternion##in_M_scalar_type##Var>(L"m_Quaternion", quaternion_type::Identity())),     \
    m_Euler(AddMemberVar<ma::Eigen::Vector3##in_M_scalar_type##Var>(L"m_Euler", euler_type::Zero())),                  \
    m_Scale(AddMemberVar<ma::Eigen::Vector3##in_M_scalar_type##Var>(L"m_Scale", scale_type::Ones()))                   \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Engine::Node##in_M_scalar_type::~Node##in_M_scalar_type()                                                      \
    {}                                                                                                                 \
                                                                                                                       \
    void ma::Engine::Node##in_M_scalar_type::EndConstruction()                                                         \
    {                                                                                                                  \
        ma::Item::EndConstruction();                                                                                   \
                                                                                                                       \
        if(auto opt = HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))                                \
        {                                                                                                              \
            auto observer_manager = opt.value();                                                                       \
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_Position);                       \
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_Quaternion);                     \
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_Euler);                          \
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_Scale);                          \
        }                                                                                                              \
        else                                                                                                           \
        {                                                                                                              \
            MA_WARNING(false,                                                                                          \
                       L"Le gestionnaire d'observations n'ai pas "s +                                                  \
                           L"disponible. Aucun item n'est lié à la clef qui lui est attitrée: "s +                     \
                           Key::GetObservationManager() + L" Ce nœud ne pourra pas observer les variables\n"s +        \
                           L" - m_Position \n - m_Quaternion \n - m_Euler \n - m_Scale\n"s);                           \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Engine::Node##in_M_scalar_type::SetObservation(const ObservablePtr &observable)                           \
    {                                                                                                                  \
        if(m_Position == observable || m_Quaternion == observable || m_Scale == observable)                            \
        {                                                                                                              \
            m_Transform->Set(m_Position->Get(), m_Quaternion->Get(), m_Scale->Get());                                  \
        }                                                                                                              \
        else if(m_Euler == observable)                                                                                 \
        {                                                                                                              \
            const long double to_rad = M_PI / 180.0;                                                                   \
            const auto &euler = m_Euler->Get();                                                                        \
            const auto rot_mat =                                                                                       \
                quaternion_type(::Eigen::AngleAxis<scalar_type>(euler[0] * to_rad, euler_type::UnitX()) *              \
                                ::Eigen::AngleAxis<scalar_type>(euler[1] * to_rad, euler_type::UnitY()) *              \
                                ::Eigen::AngleAxis<scalar_type>(euler[2] * to_rad, euler_type::UnitZ()));              \
                                                                                                                       \
            m_Quaternion->Set(rot_mat);                                                                                \
            m_Transform->Set(m_Position->Get(), m_Quaternion->Get(), m_Scale->Get());                                  \
        }                                                                                                              \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Engine::Node##in_M_scalar_type::BeginObservation(const ObservablePtr &observable)                         \
    {                                                                                                                  \
        SetObservation(observable);                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Engine::Node##in_M_scalar_type::UpdateObservation(const ObservablePtr &observable,                        \
                                                               const ma::Observable::Data &data)                       \
    {                                                                                                                  \
        SetObservation(observable);                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Engine::Node##in_M_scalar_type::EndObservation(const ObservablePtr &observable)                           \
    {}                                                                                                                 \
                                                                                                                       \
    const ma::TypeInfo &ma::Engine::Node##in_M_scalar_type::GetEngineNode##in_M_scalar_type##TypeInfo()                \
    {                                                                                                                  \
        static const TypeInfo info = {                                                                                 \
            GetEngineNode##in_M_scalar_type##ClassName(),                                                              \
            L"Item de type «"s + ma::to_wstring("Node" #in_M_scalar_type) + L"».\n"s,                                  \
            {{TypeInfo::Key::GetWhiteListChildrenData(), {GetEngineNode##in_M_scalar_type##ClassName()}},              \
             {TypeInfo::Key::GetWhiteListParentsData(), {GetEngineNode##in_M_scalar_type##ClassName()}}}};             \
                                                                                                                       \
        return info;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Engine::Node##in_M_scalar_type::OnEnable()                                                                \
    {                                                                                                                  \
        ma::Observer::OnEnable();                                                                                      \
        ma::Item::OnEnable();                                                                                          \
    }                                                                                                                  \
                                                                                                                       \
    void ma::Engine::Node##in_M_scalar_type::OnDisable()                                                               \
    {                                                                                                                  \
        ma::Observer::OnDisable();                                                                                     \
        ma::Item::OnDisable();                                                                                         \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Node##in_M_scalar_type, Engine, Item, Observer)                                \
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Node##in_M_scalar_type, Engine)

#include "ma/Item.h"
#include "ma/eigen/Vector.h"
#include "ma/eigen/Quaternion.h"
#include "ma/eigen/Matrix.h"
#include "ma/eigen/Transform.h"
#include "ma/Dll.h"

// ma::Engine::Nodei
// ma::Engine::Nodeu
// ma::Engine::Nodel
// ma::Engine::Nodell
// ma::Engine::Nodef
// ma::Engine::Noded
// ma::Engine::Nodeld

// ENGINE_NODE_HEADER(i)
// ENGINE_NODE_HEADER(u)
// ENGINE_NODE_HEADER(l)
// ENGINE_NODE_HEADER(ll)

ENGINE_NODE_HEADER(f)
ENGINE_NODE_HEADER(d)
ENGINE_NODE_HEADER(ld)
