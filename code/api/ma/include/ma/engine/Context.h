/// \file engine/Context.h
/// \author Pontier Pierre
/// \date 2023-11-23
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#pragma once

#include "ma/Context.h"

namespace ma::Engine
{
    class M_DLL_EXPORT Context: public ma::Context
    {
        public:
            explicit Context(const Item::ConstructorParameters &in_params, const std::wstring &name);
            explicit Context() = delete;
            ~Context() override;

            /// Renvoie la fenêtre de rendu.
            virtual wxWindow *GetView() = 0;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Context, Engine)
    };
    typedef std::shared_ptr<Context> ContextPtr;
} // namespace ma::Engine
