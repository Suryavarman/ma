/// \file engine/Light.h
/// \author Pontier Pierre
/// \date 2022-03-26
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/engine/Node.h"
#include "ma/Colour.h"

#include "ma/Dll.h"
#define ENGINE_LIGHT_HEADER(in_M_scalar_type, in_M_scalar_var_type)                                                    \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Engine                                                                                               \
        {                                                                                                              \
            class M_DLL_EXPORT Light##in_M_scalar_type: public ma::Engine::Node##in_M_scalar_type                      \
            {                                                                                                          \
                public:                                                                                                \
                    explicit Light##in_M_scalar_type(const Item::ConstructorParameters &in_params,                     \
                                                     scalar_type in_intensity = 1.0,                                   \
                                                     const std::wstring &in_name = {});                                \
                    Light##in_M_scalar_type() = delete;                                                                \
                    virtual ~Light##in_M_scalar_type();                                                                \
                                                                                                                       \
                    /** L'intensité de la lumière */                                                                 \
                    const std::shared_ptr<in_M_scalar_var_type> m_Intensity;                                           \
                                                                                                                       \
                    /* Couleur de la lumière. */                                                                      \
                    ma::wxColourVarPtr m_Colour;                                                                       \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Light##in_M_scalar_type, Engine)                            \
            };                                                                                                         \
            typedef std::shared_ptr<ma::Engine::Light##in_M_scalar_type> Light##in_M_scalar_type##Ptr;                 \
        }                                                                                                              \
    }                                                                                                                  \
    M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Light##in_M_scalar_type, Engine)

#define ENGINE_LIGHT_CPP(in_M_scalar_type, in_M_scalar_var_type)                                                       \
    ma::Engine::Light##in_M_scalar_type::Light##in_M_scalar_type(                                                      \
        const ma::Item::ConstructorParameters &in_params, scalar_type in_intensity, const std::wstring &in_name):      \
    ma::Engine::Node##in_M_scalar_type{in_params, in_name},                                                            \
    m_Intensity(AddMemberVar<in_M_scalar_var_type>(L"m_Intensity", in_intensity)),                                     \
    m_Colour(AddMemberVar<wxColourVar>(L"m_Colour"))                                                                   \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Engine::Light##in_M_scalar_type::~Light##in_M_scalar_type()                                                    \
    {}                                                                                                                 \
                                                                                                                       \
    const ma::TypeInfo &ma::Engine::Light##in_M_scalar_type::GetEngineLight##in_M_scalar_type##TypeInfo()              \
    {                                                                                                                  \
        static const TypeInfo info = {GetEngineLight##in_M_scalar_type##ClassName(),                                   \
                                      L"Définition d'une caméra 3D ayant pour scalaire le type: «" +                   \
                                          TypeNameW<scalar_type>() + L"».\n",                                          \
                                      {}};                                                                             \
                                                                                                                       \
        return info;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(                                                                              \
        Light##in_M_scalar_type,                                                                                       \
        Engine,                                                                                                        \
        ma::Engine::Node##in_M_scalar_type::GetEngineNode##in_M_scalar_type##ClassHierarchy())                         \
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Light##in_M_scalar_type, Engine)

ENGINE_LIGHT_HEADER(f, ma::FloatVar)
ENGINE_LIGHT_HEADER(d, ma::DoubleVar)
ENGINE_LIGHT_HEADER(ld, ma::LongDoubleVar)
