/// \file engine/Scene.h
/// \author Pontier Pierre
/// \date 2022-03-26
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/engine/Node.h"

#include "ma/Dll.h"

/// m_Name:
/// Variable contenant le nom de la scène.
///
/// m_WhiteListChildren:
/// Seuls des nœuds 3D du même type scalaire peuvent être ajouté à cette instance.
///
/// m_WhiteListParents:
/// Seule un Item ou un nœud 3D du même type scalaire peuvent être parent de cette instance.
#define ENGINE_SCENE_HEADER(in_M_scalar_type)                                                                          \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Engine                                                                                               \
        {                                                                                                              \
            class M_DLL_EXPORT Scene##in_M_scalar_type: public ma::Engine::Node##in_M_scalar_type                      \
            {                                                                                                          \
                public:                                                                                                \
                    explicit Scene##in_M_scalar_type(const Item::ConstructorParameters &in_params,                     \
                                                     const std::wstring &in_name = {});                                \
                    Scene##in_M_scalar_type() = delete;                                                                \
                    ~Scene##in_M_scalar_type() override;                                                               \
                                                                                                                       \
                    const ma::StringHashSetVarPtr m_WhiteListChildren;                                                 \
                                                                                                                       \
                    const ma::StringHashSetVarPtr m_WhiteListParents;                                                  \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Scene##in_M_scalar_type, Engine)                            \
            };                                                                                                         \
            typedef std::shared_ptr<ma::Engine::Scene##in_M_scalar_type> Scene##in_M_scalar_type##Ptr;                 \
        }                                                                                                              \
    }                                                                                                                  \
    M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Scene##in_M_scalar_type, Engine)

#define ENGINE_SCENE_CPP(in_M_scalar_type)                                                                             \
    ma::Engine::Scene##in_M_scalar_type::Scene##in_M_scalar_type(const ma::Item::ConstructorParameters &in_params,     \
                                                                 const std::wstring &in_name):                         \
    ma::Engine::Node##in_M_scalar_type{in_params, in_name}                                                             \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Engine::Scene##in_M_scalar_type::~Scene##in_M_scalar_type()                                                    \
    {}                                                                                                                 \
                                                                                                                       \
    const ma::TypeInfo &ma::Engine::Scene##in_M_scalar_type::GetEngineScene##in_M_scalar_type##TypeInfo()              \
    {                                                                                                                  \
        static const TypeInfo info = {                                                                                 \
            GetEngineScene##in_M_scalar_type##ClassName(),                                                             \
            L"Définition d'une scène 3D ayant pour scalaire le type: «"s + TypeNameW<scalar_type>() + L"».\n"s,        \
            {{TypeInfo::Key::GetWhiteListChildrenData(), {GetEngineNode##in_M_scalar_type##ClassName()}},              \
             {TypeInfo::Key::GetWhiteListParentsData(), {ma::Item::GetItemClassName()}}}};                             \
                                                                                                                       \
        return info;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(                                                                              \
        Scene##in_M_scalar_type,                                                                                       \
        Engine,                                                                                                        \
        ma::Engine::Node##in_M_scalar_type::GetEngineNode##in_M_scalar_type##ClassHierarchy())                         \
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Scene##in_M_scalar_type, Engine)

// ENGINE_SCENE_HEADER(f)
ENGINE_SCENE_HEADER(d)
ENGINE_SCENE_HEADER(ld)

namespace ma::Engine
{
    class M_DLL_EXPORT Scenef: public ma::Engine::Nodef
    {
        public:
            explicit Scenef(const Item::ConstructorParameters &in_params, const std::wstring &in_name = {});
            Scenef() = delete;
            ~Scenef() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Scenef, Engine)
    };
    typedef std::shared_ptr<ma::Engine::Scenef> ScenefPtr;

    /// Permet de créer un contexte en lian par défaut la scène 3D la plus proche de la racine.
    template<typename T_Context>
    std::shared_ptr<T_Context> CreateContext();

    /// Permet de créer un lien par défaut avec la scène 3D la plus proche de la racine.
    template<typename T_Context>
    std::shared_ptr<T_Context> BuildContextLink(std::shared_ptr<T_Context> context);

} // namespace ma::Engine
M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Scenef, Engine)

#include "ma/engine/Scene.tpp"
