/// \file engine/Light.tpp
/// \author Pontier Pierre
/// \date 2022-05-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

namespace ma::Engine
{
    template<typename T_ScalarVar, typename T_Node>
    T_Light<T_ScalarVar, T_Node>::T_Light(const Item::ConstructorParameters &in_params,
                                          typename T_Light<T_ScalarVar, T_Node>::value_type in_intensity):
    T_Node{in_params},
    m_Intensity(this->API::Item::template AddMemberVar<T_ScalarVar>(L"m_Intensity", in_intensity))
    {}

    template<typename T_ScalarVar, typename T_Node>
    T_Light<T_ScalarVar, T_Node>::~T_Light()
    {}

    template<typename T_ScalarVar, typename T_Node>
    T_Omni<T_ScalarVar, T_Node>::T_Omni(const Item::ConstructorParameters &in_params,
                                        typename T_Light<T_ScalarVar, T_Node>::value_type in_intensity,
                                        typename T_Light<T_ScalarVar, T_Node>::value_type radius):
    T_Light<T_ScalarVar, T_Node>{in_params, in_intensity},
    m_Radius(this->API::Item::template AddMemberVar<T_ScalarVar>(L"m_Radius", radius))
    {}

    template<typename T_ScalarVar, typename T_Node>
    T_Omni<T_ScalarVar, T_Node>::~T_Omni()
    {}
} // namespace ma::Engine
