/// \file engine/Camera
/// \author Pontier Pierre
/// \date 2022-04-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

namespace ma::Engine
{
    template<typename T_ScalarVar, typename T_Node>
    T_Cube<T_ScalarVar, T_Node>::T_Cube(const Item::ConstructorParameters &in_params, const value_type &in_size):
    T_Node{in_params}
    {}

    template<typename T_ScalarVar, typename T_Node>
    T_Cube<T_ScalarVar, T_Node>::~T_Cube()
    {}
} // namespace ma::Engine
