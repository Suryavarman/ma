/// \file engine/Camera
///  \author Pontier Pierre
/// \date 2022-04-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/engine/Node.h"
#include "ma/Find.h"
#include "ma/Context.h"

namespace ma::Engine
{
    //    template<typename T_ScalarVar, typename T_Node>
    //    T_Scene<T_ScalarVar, T_Node>::T_Scene(const Item::ConstructorParameters &in_params, const std::wstring
    //    &in_name): T_Node{in_params}, m_Name(this->ma::Item::template AddMemberVar<ma::StringVar>(L"m_Name",
    //    in_name)), m_WhiteList(AddReadOnlyMemberVar<ma::StringHashSetVar>(
    //        ma::Item::ms_KeyWhiteListChildrenVar,
    //        ma::StringHashSetVar::value_type({// le nom de la classe de T_Node. Tout ce qui est de type Node peut
    //        être
    //                                           // ajouter à une scène
    //                                           GetTypeInfo().GetHierarchy()[0].GetClassName()})))
    //    {}
    //
    //    template<typename T_ScalarVar, typename T_Node>
    //    T_Scene<T_ScalarVar, T_Node>::~T_Scene()
    //    {}

    template<typename T_Context>
    std::shared_ptr<T_Context> CreateContext()
    {
        const auto context_manager_key = ma::Item::Key::GetContextManager();
        auto context = ma::Item::CreateItem<T_Context>(context_manager_key);

        return BuildContextLink(context);
    }

    template<typename T_Context>
    std::shared_ptr<T_Context> BuildContextLink(std::shared_ptr<T_Context> context)
    {
        auto context_link = ma::Item::CreateItem<ma::ContextLink>(context->GetKey());

        typedef ma::MatchType<ma::Engine::Scenef> MatchScenes_f;
        typedef ma::MatchType<ma::Engine::Scened> MatchScenes_d;
        typedef ma::MatchType<ma::Engine::Sceneld> MatchScenes_ld;

        const auto &item_key = ma::Item::Key::GetRoot();
        auto scenes_f = ma::Find<MatchScenes_f>::GetDeque(item_key, MatchScenes_f(), ma::Item::SearchMod::RECURSIVE);
        auto scenes_d = ma::Find<MatchScenes_d>::GetDeque(item_key, MatchScenes_d(), ma::Item::SearchMod::RECURSIVE);
        auto scenes_ld = ma::Find<MatchScenes_ld>::GetDeque(item_key, MatchScenes_ld(), ma::Item::SearchMod::RECURSIVE);

        ma::ItemPtr closest_scene;
        auto closest_scene_parenting_depth = std::numeric_limits<unsigned int>::max();

        for(auto &&scene : scenes_f)
        {
            const auto parenting_depth = scene->GetParentingDepth();
            if(closest_scene_parenting_depth > parenting_depth)
            {
                closest_scene_parenting_depth = parenting_depth;
                closest_scene = scene;
            }
        }

        for(auto &&scene : scenes_d)
        {
            const auto parenting_depth = scene->GetParentingDepth();
            if(closest_scene_parenting_depth > parenting_depth)
            {
                closest_scene_parenting_depth = parenting_depth;
                closest_scene = scene;
            }
        }

        for(auto &&scene : scenes_ld)
        {
            const auto parenting_depth = scene->GetParentingDepth();
            if(closest_scene_parenting_depth > parenting_depth)
            {
                closest_scene_parenting_depth = parenting_depth;
                closest_scene = scene;
            }
        }

        if(closest_scene)
            context_link->m_Client->SetItem(closest_scene);

        return context;
    }

} // namespace ma::Engine
