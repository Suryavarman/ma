/// \file engine/Camera.h
/// \author Pontier Pierre
/// \date 2022-03-26
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/engine/Node.h"
#include "ma/Colour.h"

#include "ma/Dll.h"

#define ENGINE_CAMERA_HEADER(in_M_scalar_type, in_M_scalar_var_type)                                                   \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Engine                                                                                               \
        {                                                                                                              \
            class M_DLL_EXPORT Camera##in_M_scalar_type: public ma::Engine::Node##in_M_scalar_type                     \
            {                                                                                                          \
                public:                                                                                                \
                    explicit Camera##in_M_scalar_type(                                                                 \
                        const Item::ConstructorParameters &in_params,                                                  \
                        scalar_type in_near = 0.1,                                                                     \
                        scalar_type in_far = 1000.0, /*scalar_type in_ratio = 16. / 9.,*/                              \
                        const std::wstring &in_name = {});                                                             \
                    Camera##in_M_scalar_type() = delete;                                                               \
                    virtual ~Camera##in_M_scalar_type();                                                               \
                                                                                                                       \
                    /** Plus proche que ce plan la 3D n'est pas affichée. */                                          \
                    std::shared_ptr<in_M_scalar_var_type> m_Near;                                                      \
                                                                                                                       \
                    /**  Au-delà de ce plan la 3D n'est plus affichée. */                                            \
                    std::shared_ptr<in_M_scalar_var_type> m_Far;                                                       \
                                                                                                                       \
                    /**  Le ratio largeur/hauteur : */                                                                 \
                    /**  HD	        1280x720 */                                                                        \
                    /**  Full-HD	1920x1080 */                                                                          \
                    /**  UHD	    3840x2160 */                                                                          \
                    /**  4K	        4096x2160 */                                                                       \
                    /**  */                                                                                            \
                    /**  16:9    3840x2160	2560×1440	1920x1080	920x540 */                                             \
                    /**  21:9	(ultra-larges)  3440×1440	2560×1080 */                                                 \
                    /**  32:9	(super-ultra-larges)    5120×1440	3840×1200	3840×1080 */                              \
                    /* std::shared_ptr<in_M_scalar_var_type> m_Ratio;*/                                                \
                                                                                                                       \
                    /* Couleur du fond. */                                                                             \
                    ma::wxColourVarPtr m_BackgroundColour;                                                             \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Camera##in_M_scalar_type, Engine)                           \
            };                                                                                                         \
            typedef std::shared_ptr<ma::Engine::Camera##in_M_scalar_type> Camera##in_M_scalar_type##Ptr;               \
        }                                                                                                              \
    }                                                                                                                  \
    M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Camera##in_M_scalar_type, Engine)

#define ENGINE_CAMERA_CPP(in_M_scalar_type, in_M_scalar_var_type)                                                      \
    ma::Engine::Camera##in_M_scalar_type::Camera##in_M_scalar_type(const ma::Item::ConstructorParameters &in_params,   \
                                                                   scalar_type in_near,                                \
                                                                   scalar_type in_far, /*scalar_type in_ratio,*/       \
                                                                   const std::wstring &in_name):                       \
    ma::Engine::Node##in_M_scalar_type{in_params, in_name},                                                            \
    m_Near(AddMemberVar<in_M_scalar_var_type>(L"m_Near", in_near)),                                                    \
    m_Far(AddMemberVar<in_M_scalar_var_type>(                                                                          \
        L"m_Far", in_far)), /*m_Ratio(AddMemberVar<in_M_scalar_var_type>(L"m_Ratio", in_ratio)),  */                   \
    m_BackgroundColour(AddMemberVar<wxColourVar>(L"m_BackgroundColor", 128, 128, 128))                                 \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Engine::Camera##in_M_scalar_type::~Camera##in_M_scalar_type()                                                  \
    {}                                                                                                                 \
                                                                                                                       \
    const ma::TypeInfo &ma::Engine::Camera##in_M_scalar_type::GetEngineCamera##in_M_scalar_type##TypeInfo()            \
    {                                                                                                                  \
        static const TypeInfo info = {GetEngineCamera##in_M_scalar_type##ClassName(),                                  \
                                      L"Définition d'une caméra 3D ayant pour scalaire le type: «" +                   \
                                          TypeNameW<scalar_type>() + L"».",                                            \
                                      {}};                                                                             \
                                                                                                                       \
        return info;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(                                                                              \
        Camera##in_M_scalar_type,                                                                                      \
        Engine,                                                                                                        \
        ma::Engine::Node##in_M_scalar_type::GetEngineNode##in_M_scalar_type##ClassHierarchy())                         \
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Camera##in_M_scalar_type, Engine)

ENGINE_CAMERA_HEADER(f, ma::FloatVar)
ENGINE_CAMERA_HEADER(d, ma::DoubleVar)
ENGINE_CAMERA_HEADER(ld, ma::LongDoubleVar)
