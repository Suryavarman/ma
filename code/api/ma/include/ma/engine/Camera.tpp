/// \file engine/Camera
/// \author Pontier Pierre
/// \date 2022-04-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

namespace ma::Engine
{
    template<typename T_ScalarVar, typename T_Node>
    T_Camera<T_ScalarVar, T_Node>::T_Camera(const Item::ConstructorParameters &in_params,
                                            value_type in_near,
                                            value_type in_far):
    // value_type in_ratio):
    T_Node{in_params},
    m_Near(this->API::Item::template AddMemberVar<T_ScalarVar>(L"m_Near", in_near)),
    m_Far(this->API::Item::template AddMemberVar<T_ScalarVar>(L"m_Far", in_far))
    {}

    template<typename T_ScalarVar, typename T_Node>
    T_Camera<T_ScalarVar, T_Node>::~T_Camera()
    {}
} // namespace ma::Engine
