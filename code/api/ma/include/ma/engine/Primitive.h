/// \file engine/Primitive.h
/// \author Pontier Pierre
/// \date 2022-03-26
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/engine/Node.h"

#include "ma/Dll.h"

#define ENGINE_CUBE_HEADER(in_M_scalar_type, in_M_scalar_var_type)                                                     \
    namespace ma                                                                                                       \
    {                                                                                                                  \
        namespace Engine                                                                                               \
        {                                                                                                              \
            class M_DLL_EXPORT Cube##in_M_scalar_type: public ma::Engine::Node##in_M_scalar_type                       \
            {                                                                                                          \
                public:                                                                                                \
                    explicit Cube##in_M_scalar_type(const Item::ConstructorParameters &in_params,                      \
                                                    const scalar_type &in_size = 1.0,                                  \
                                                    const std::wstring &in_name = {});                                 \
                    Cube##in_M_scalar_type() = delete;                                                                 \
                    virtual ~Cube##in_M_scalar_type();                                                                 \
                                                                                                                       \
                    M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cube##in_M_scalar_type, Engine)                             \
            };                                                                                                         \
            typedef std::shared_ptr<ma::Engine::Cube##in_M_scalar_type> Cube##in_M_scalar_type##Ptr;                   \
        }                                                                                                              \
    }                                                                                                                  \
    M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Cube##in_M_scalar_type, Engine)

#define ENGINE_CUBE_CPP(in_M_scalar_type, in_M_scalar_var_type)                                                        \
    ma::Engine::Cube##in_M_scalar_type::Cube##in_M_scalar_type(                                                        \
        const ma::Item::ConstructorParameters &in_params, const scalar_type &in_size, const std::wstring &in_name):    \
    ma::Engine::Node##in_M_scalar_type{in_params, in_name}                                                             \
    {}                                                                                                                 \
                                                                                                                       \
    ma::Engine::Cube##in_M_scalar_type::~Cube##in_M_scalar_type()                                                      \
    {}                                                                                                                 \
                                                                                                                       \
    const ma::TypeInfo &ma::Engine::Cube##in_M_scalar_type::GetEngineCube##in_M_scalar_type##TypeInfo()                \
    {                                                                                                                  \
        static const TypeInfo info = {GetEngineCube##in_M_scalar_type##ClassName(),                                    \
                                      L"Définition d'un cube 3D ayant pour scalaire le type: «" +                      \
                                          TypeNameW<scalar_type>() + L"».\n",                                          \
                                      {}};                                                                             \
                                                                                                                       \
        return info;                                                                                                   \
    }                                                                                                                  \
                                                                                                                       \
    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(                                                                              \
        Cube##in_M_scalar_type,                                                                                        \
        Engine,                                                                                                        \
        ma::Engine::Node##in_M_scalar_type::GetEngineNode##in_M_scalar_type##ClassHierarchy())                         \
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Cube##in_M_scalar_type, Engine)

ENGINE_CUBE_HEADER(f, ma::FloatVar)
ENGINE_CUBE_HEADER(d, ma::DoubleVar)
ENGINE_CUBE_HEADER(ld, ma::LongDoubleVar)
