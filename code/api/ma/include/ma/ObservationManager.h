/// \file ObservationManager.h
/// \author Pontier Pierre
/// \date 2020-01-05
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Prerequisites.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/Dll.h"

namespace ma
{
    /// Effectue le lien entre l'observable et l'observateur.
    class M_DLL_EXPORT Observation: public ma::Item
    {
            friend class ObservationManager;

        protected:
            using ma::Item::Item;

            /// Permet au gestionnaire d'observation de ne pas entamer plus
            /// d'une fois la destruction de l'observation.
            bool m_DestructionBegin;

            /// A titre informatif permet de connaitre l'avancement de la
            /// destruction de l'observation.
            bool m_DestructionEnd;

        public:
            ma::ObserverVarPtr m_ObserverVar;
            ma::ObservableVarPtr m_ObservableVar;

            explicit Observation(const Item::ConstructorParameters &in_params);
            Observation() = delete;
            virtual ~Observation();
            virtual bool IsObservable() const override;

            M_HEADER_CLASSHIERARCHY(Observation)
    };
    typedef std::shared_ptr<Observation> ObservationPtr;

    typedef std::unordered_map<ma::Item::key_type, ObservationPtr> Observations;

    /// C'est le gestionnaire d'observations qui crée, met à jour et détruit les
    /// liens d'observation, c'est à dire les instances de ma::Observation.
    /// \remarks Attention aux appels en boucle. Le code suivant montre que sans
    /// le booléen m_EndObservationBegin l'appel à EndObservation appellera sans
    /// fin UpdateObservation puis EndObservation…
    /// \code {.cpp}
    /// TOTO::UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data)
    /// {
    ///     if(observable == m_CurrentItemVar &&
    ///       data.HasActions(ma::Var::ms_KeyObsBeginSetValue) &&
    ///       !m_EndObservationBegin)
    ///     {
    ///         if(item && root->obs->HasObservation(this, item))
    ///             root->obs->RemoveObservation(this, item);
    ///     }
    /// }
    ///
    /// TOTO::EndObservation(const ma::ObservablePtr& observable)
    /// {
    ///     static const std::string function_name = "void TOTO::EndObservation(const ma::ObservablePtr& observable)";
    ///
    ///    if(ma::ClassInfoManager::IsInherit(observable, ma::Item::GetItemClassName()))
    ///    {
    ///        auto item = std::dynamic_pointer_cast<ma::Item>(observable); // l'objet est en cours de destruction. La
    ///        classe Item est déjà détruite.
    ///
    ///        MA_ASSERT(item,
    ///                  "item est nulle.",
    ///                  function_name,
    ///                  std::runtime_error);
    ///
    ///        if(m_CurrentItemVar->GetItem() == item)
    ///        {
    ///            auto parent_key = item->GetParentKey();
    ///
    ///            if(ma::Item::HasItem(parent_key))
    ///            {
    ///                m_EndObservationBegin = true;
    ///                m_CurrentItemVar->SetItem(ma::Item::GetItem(parent_key));
    ///                m_EndObservationBegin = false;
    ///            }
    ///        }
    ///    }
    ///}
    /// \endcode

    class M_DLL_EXPORT ObservationManager: public ma::Item
    {
        public:
            typedef ma::Item::key_type key_type;

            /// Pour permettre au destructeur des observateurs d'avoir accès à la fonction membre :
            /// virtual void RemoveObserverFromObserverDestroy(const ma::ObserverPtr& observer)
            friend ma::Observer::~Observer();

        private:
            /// \see <a href="https://en.cppreference.com/w/cpp/memory/shared_ptr/hash" target="_blank">«shared_ptr
            /// hash»</a>
            typedef std::unordered_map<ObservablePtr, key_type> ObsaObservations;
            typedef std::unordered_map<ma::Observer::key_type, key_type> ObseObservations;
            typedef std::unordered_map<key_type, ma::ObservationPtr> Observations;

            /// {observable, {observateur, clef observation}}
            typedef std::unordered_map<ObservablePtr, ObseObservations> MapObsa;

            /// {observateur, {observable, clef observation}}
            typedef std::unordered_map<ma::Observer::key_type, ObsaObservations> MapObse;

            /// À partir de la clef d'un observable, on peut retrouver toutes les observations qui lui sont associées.
            MapObsa m_ObsaMap;

            /// À partir d'un observer, on peut retrouver toutes les observations qui lui sont associées.
            MapObse m_ObseMap;

            /// Permet de sauvegarder l'observation durant sa destruction.
            /// Ce qui permet d'accéder à l'observation même si elle n'est plus active.
            /// Ce qui permet ainsi de différencier la non-présence de l'observation liée à une erreur et d'une
            /// observation inactive à cause d'une destruction en cours qui exécute des appels en boucle.
            Observations m_ObservationsDestruction;

            /// Permet de factoriser l'appel à EndObservation
            void EndObservationBlock(ma::ObservationPtr observation);

            /// Fonction qui sera appelé via ma::Observer::~Observer()
            /// Ce qui oblige à ne pas utiliser les fonctions virtuelles et
            /// virtuelles pures de la classe ma::Observer::~Observer().
            /// \remarks C'est pour cela que le destructeur des observateurs est ami de cette classe.
            void RemoveObserverFromObserverDestroy(ma::ObserverPtr observer);

        protected:
            /// <summary>
            /// Pour éviter l'erreur E1790 sous visual
            /// </summary>
            ObservationManager() = delete;
            using ma::Item::Item;

            /// Permet de retrouver une observation à partir de l'observateur et
            /// de l'observable.
            /// \param observer L'observateur
            /// \param observable L'observé
            /// \param with_destruction_progress si vrais alors les observations en cours de destruction seront prises
            /// en compte.
            virtual bool HasObservation(ma::ObserverPtr observer,
                                        ma::ObservablePtr observable,
                                        bool with_destruction_progress) const;

            virtual key_type GetObservationKey(ma::ObserverPtr observer,
                                               ma::ObservablePtr observable,
                                               bool with_destruction_progress) const;
            virtual ObservationPtr GetObservation(ma::ObserverPtr observer,
                                                  ma::ObservablePtr observable,
                                                  bool with_destruction_progress) const;
            virtual Observations GetObservations(ma::ObserverPtr observer, bool with_destruction_progress) const;
            virtual Observations GetObservations(ma::ObservablePtr observable, bool with_destruction_progress) const;

        public:
            explicit ObservationManager(const Item::ConstructorParameters &in_params);
            virtual ~ObservationManager();

            /// Permet de retrouver une observation à partir de l'observateur et de l'observable.
            /// \param observer L'observateur
            /// \param observable L'observé
            /// \remarks les observations en cours de destruction ne seront pas prises en compte.
            virtual bool HasObservation(ma::ObserverPtr observer, ma::ObservablePtr observable) const;

            /// Permet de savoir si un observable est observé.
            /// \param observable L'observé
            /// \remarks Pour des raisons d'optimisation HasObservable ne test pas si ses observations sont en cours de
            /// destruction.
            virtual bool HasObservable(ma::ObservablePtr observable) const;

            /// Permet de savoir si un observateur observe.
            /// \param observer L'observateur
            /// \remarks Pour des raisons d'optimisation HasObserver ne test pas si ses observations sont en cours de
            /// destruction.
            virtual bool HasObserver(ma::ObserverPtr observer) const;

            /// Pour initialiser les variables m_Observer et m_Observable d'une
            /// Observation. Il faut implémenter le comportement de:
            /// \li ma::ObserverVar qui est une classe abstraite.
            /// \li ma::ObservableVar qui est une classe abstraite.
            /// \tparam T_ObserverVar hérite du type ma::ObserverVar qui est une classe
            /// abstraite.
            /// \tparam T_ObservableVar hérite du type ma::ObservableVar qui est une classe
            /// abstraite.
            /// \param observer L'observateur qui observe l'observable.
            /// \param observable_key La clef de l'observable.
            /// \exception std::invalid_argument Si l'observable ne peut être
            /// observé car il est non actif.
            /// \li Un item est considéré non actif si il n'est plus dans
            /// «ma::Item::ms_Map»
            /// \li Une variable est considérée non active si son item n'est pas
            /// observable et/ou si la variable n'est plus associée à un item.
            /// \return L'observation ainsi crée ou celle déjà existante.
            /// \example
            /// \code {.cpp}
            /// obs_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(item.get(), var.second);
            /// \endcode
            template<typename T_ObserverVar, typename T_ObservableVar>
            ObservationPtr AddObservation(ObserverPtr observer, ma::ObservablePtr observable);

            virtual key_type GetObservationKey(ma::ObserverPtr observer, ma::ObservablePtr observable) const;
            virtual ObservationPtr GetObservation(ma::ObserverPtr observer, ma::ObservablePtr observable) const;
            virtual Observations GetObservations(ma::ObserverPtr observer) const;
            virtual Observations GetObservations(ma::ObservablePtr observable) const;

            /// \exception std::runtime_error l'obrservable n'est plus observable.
            virtual void UpdateObservations(ma::ObservablePtr, const ma::Observable::Data &data);

            /// Redifinition pour éviter une sarcharge du mauvais UpdateObservations
            /// \see -Woverloaded-virtual
            /// \see https://stackoverflow.com/questions/9995421/gcc-woverloaded-virtual-warnings
            virtual void UpdateObservations(const Data &data) override;

            /// Pour couper le lien d'observation.
            virtual void RemoveObservation(ma::ObserverPtr, ma::ObservablePtr);

            /// Toutes les observations liées à cet observable seront effacées.
            virtual void RemoveObservable(ma::ObservablePtr);

            /// Toutes les observations liées à cet observateur seront effacées.
            /// \param observer L'observateur qui souhaite ne plus observer.
            /// \remarks Attention l'observateur appellera sa fonction « EndObservation » pour chaque observable qu'il
            /// observe.
            /// \exception std::logic_error L'observateur est cours de destruction.
            /// Peut-être avez-vous appelé RemoveObserver.
            /// Laissez plus tôt le destructeur de l'observateur informer le gestionnaire d'observations.
            virtual void RemoveObserver(ma::ObserverPtr observer);

            virtual void RemoveChild(ma::ItemPtr item) override;

            M_HEADER_CLASSHIERARCHY(ObservationManager)
    };
    typedef std::shared_ptr<ObservationManager> ObservationManagerPtr;
} // namespace ma

#include "ma/ObservationManager.tpp"
