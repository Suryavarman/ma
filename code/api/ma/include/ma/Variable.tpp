/// \file Variable.tpp
/// \author Pontier Pierre
/// \date 2019-12-17
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Algorithm.h"

#include <utility>
#include <limits>

namespace ma
{
    // Type Var /////////////////////////////////////////////////////////////
    template<typename T>
    std::shared_ptr<T> Var::GetPtr() const
    {
        return std::static_pointer_cast<T>(GetPtr());
    }

    // Type T_Var /////////////////////////////////////////////////////////////
    template<typename T>
    T_Var<T>::T_Var(const Var::ConstructorParameters &params, const value_type &value):
    Var(params),
    m_Value{value},
    m_Previous{},
    m_Default{value}
    {}

    template<typename T>
    T_Var<T>::T_Var(const Var::ConstructorParameters &params): Var(params), m_Value{}, m_Previous{}, m_Default{}
    {}

    template<typename T>
    T_Var<T>::~T_Var()
    {}

    template<typename T>
    std::wstring T_Var<T>::toString() const
    {
        return ma::toString(m_Value);
    }

    template<typename T>
    std::wstring T_Var<T>::toStringRepresentation() const
    {
        if(IsRepresentedByString<T>())
            return L'"' + ma::toString<T>(m_Value) + L'"';
        else
            return ma::toString<T>(m_Value);
    }

    template<typename T>
    std::wstring T_Var<T>::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    template<typename T>
    void T_Var<T>::SetFromItem(const std::wstring &str)
    {
        const T value = FromString<T>(str);
        SetFromItem(value);
    }

    template<typename T>
    void T_Var<T>::SetFromItem(const T &value)
    {
        // Attention aux conteneurs, comme std::priority_queue, qui n'ont pas d'opérateurs.
        // Ils ne pourront pas être comparés à un autre conteneur du même type.
        // Pour contourner cela, vous pouvez créer une classe qui hérite du conteneur et implémenter l'opérateur
        // manquant.
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    template<typename T>
    void T_Var<T>::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    template<typename T>
    VarPtr T_Var<T>::Copy() const
    {
        auto ptr = std::shared_ptr<T_Var<T>>(new T_Var<T>(GetConstructorParameters()));

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    template<typename T>
    bool T_Var<T>::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<T>(value);
    }

    template<typename T>
    void T_Var<T>::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Previous = m_Value;
        m_Value = m_Default;
        UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    template<typename T>
    void T_Var<T>::Set(const value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    template<typename T>
    const typename T_Var<T>::value_type &T_Var<T>::Get() const
    {
        return m_Value;
    }

    template<typename T>
    const typename T_Var<T>::value_type &T_Var<T>::GetPrevious() const
    {
        return m_Previous;
    }

    template<typename T>
    T_Var<T> &T_Var<T>::operator=(const typename T_Var<T>::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    // Type EnumVar////////////////////////////////////////////////////////////
    /*
    template<typename T_Value, T_Name>
    EnumVar<T_Value, T_Name>::EnumVar(const Var::key_type& key, bool dynamic, bool read_only, const
    value_names& enumeration): Var(key, dynamic, read_only), m_Enumeration(enumeration), m_Previous({}),
    m_Default(enumeration)
    {}

    template<typename T_Value, T_Name>
    EnumVar<T_Value, T_Name>::EnumVar(const Var::key_type& key, bool dynamic, bool read_only):
    Var(key, dynamic, read_only),
    m_Enumeration({}),
    m_Previous({}),
    m_Default({})
    {}

    template<typename T_Value, T_Name>
    EnumVar<T_Value, T_Name>::~EnumVar()
    {}

    template<typename T_Value, T_Name>
    std::wstring EnumVar<T_Value, T_Name>::toString() const
    {
        // R"#({value_1: "name_1", value_2: "name_2"})#"

        MA_ASSERT(false,
                  "La fonction doit être implémentée pour les types demandé.",
                  std::logic_error);
    }

    template<typename T_Value, T_Name>
    std::wstring EnumVar<T_Value, T_Name>::GetKeyValueToString() const
    {
        // R"#({value_1: "name_1", value_2: "name_2"})#"

        MA_ASSERT(false,
                  "La fonction doit être implémentée pour les types demandé.",
                  std::logic_error);
    }

    template<typename T_Value, T_Name>
    void EnumVar<T_Value, T_Name>::SetFromItem(const std::wstring& enumeration_str)
    {
        // R"#({value_1: "name_1", value_2: "name_2"})#"
        MA_ASSERT(false,
                  "La fonction doit être implémentée pour les types demandé.",
                  std::logic_error);
    }

    template<typename T_Value, T_Name>
    VarPtr EnumVar<T_Value, T_Name>::Copy() const
    {
         auto ptr = std::shared_ptr<VarNumber<T>>(new VarNumber<T>(m_Key, m_Dynamic, m_ReadOnly,
    m_Enumeration));

         ptr->m_Previous = m_Previous;
         ptr->m_Default = m_Default;

         return ptr;
    }

    template<typename T_Value, T_Name>
    void VarPtr EnumVar<T_Value, T_Name>::fromString(const std::wstring& enumeration_str)
    {

    }

    template<typename T_Value, T_Name>
    bool VarPtr EnumVar<T_Value, T_Name>::IsValidString(const std::wstring& value) const
    {

    }

    template<typename T_Value, T_Name>
    void EnumVar<T_Value, T_Name>::Reset()
    {

    }

    template<typename T_Value, T_Name>
    void EnumVar<T_Value, T_Name>::Set(const value_names& value)
    {

    }

    template<typename T_Value, T_Name>
    const value_names& EnumVar<T_Value, T_Name>::Get() const
    {

    }

    template<typename T_Value, T_Name>
    const value_names& EnumVar<T_Value, T_Name>::GetPrevious() const
    {

    }

    template<typename T_Value, T_Name>
    void EnumVar<T_Value, T_Name>::Erase()
    {

    }
    */

    // Type BaseItemVar//———————————————————————————————————————————————————————————————————————————————————————————————
    template<typename T>
    BaseItemVar<T>::BaseItemVar(const Var::ConstructorParameters &params, const ObservablePtr &item):
    ObservableVar(params),
    m_Item(item ? std::static_pointer_cast<T>(item) : nullptr),
    m_PreviousKey{},
    m_Default(m_Item ? m_Item->GetKey() : L"")
    {
        if(item)
        {
            MA_ASSERT(
                m_Item,
                L"L'item passé en paramètre n'est pas ou n'hérite pas du type souhaité par cette variable. Veuillez "
                L"vous reporter à la documentation du type: " +
                    GetTypeInfo().m_ClassName,
                std::invalid_argument);
        }
    }

    template<typename T>
    BaseItemVar<T>::BaseItemVar(const Var::ConstructorParameters &params):
    ObservableVar(params),
    m_Item(),
    m_PreviousKey{},
    m_Default{}
    {}

    template<typename T>
    BaseItemVar<T>::~BaseItemVar()
    {}

    template<typename T>
    void BaseItemVar<T>::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        if(Item::HasItem(m_Default))
        {
            SetItemFromItem(Item::GetItem<T>(m_Default));
        }
    }

    template<typename T>
    std::wstring BaseItemVar<T>::toString() const
    {
        return ma::toString<T>(m_Item);
    }

    template<typename T>
    std::wstring BaseItemVar<T>::toStringRepresentation() const
    {
        if(IsRepresentedByString<T>())
            return L'"' + ma::toString<T>(m_Item) + L'"';
        else
            return ma::toString<T>(m_Item);
    }

    template<typename T>
    std::wstring BaseItemVar<T>::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Item));
    }

    template<typename T>
    void BaseItemVar<T>::fromString(const std::wstring &str)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(str);
    }

    template<typename T>
    bool BaseItemVar<T>::IsValidString(const std::wstring &str) const
    {
        return ma::IsValidString<T>(str);
    }

    template<typename T>
    void BaseItemVar<T>::SetItem(const BaseItemVar<T>::ptr_type &item)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetItemFromItem(item);
    }

    template<typename T>
    void BaseItemVar<T>::Set(const ObservablePtr &observable)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        if(m_Item != observable)
        {
            if(observable)
            {
                BaseItemVar<T>::ptr_type item = std::dynamic_pointer_cast<T>(observable);
                MA_ASSERT(
                    item,
                    L"L'observable passé en paramètre n'est pas ou n'hérite pas du type souhaité par cette variable. "
                    L"Veuillez vous reporter à la documentation du type: " +
                        GetTypeInfo().m_ClassName,
                    std::runtime_error);

                SetItemFromItem(item);
            }
            else
            {
                SetItemFromItem(nullptr);
            }
        }
    }

    template<typename T>
    ObservablePtr BaseItemVar<T>::Get() const
    {
        return m_Item;
    }

    template<typename T>
    const typename BaseItemVar<T>::ptr_type &BaseItemVar<T>::GetItem() const
    {
        return m_Item;
    }

    template<typename T>
    typename BaseItemVar<T>::ptr_type BaseItemVar<T>::GetPrevious() const
    {
        return Item::HasItem(m_PreviousKey) ? Item::GetItem<T>(m_PreviousKey) : nullptr;
    }

    template<typename T>
    typename Item::key_type BaseItemVar<T>::GetPreviousKey() const
    {
        return m_PreviousKey;
    }

    template<typename T>
    BaseItemVar<T> &BaseItemVar<T>::operator=(const BaseItemVar<T>::ptr_type &item)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetItemFromItem(item);
        return *this;
    }

    template<typename T>
    BaseItemVar<T> &BaseItemVar<T>::operator=(const ObservablePtr &observable)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        if(m_Item != observable)
        {
            BaseItemVar<T>::ptr_type item = std::dynamic_pointer_cast<T>(observable);
            MA_ASSERT(item,
                      L"L'observable passé en paramètre n'est pas ou n'hérite pas du type souhaité par cette variable. "
                      L"Veuillez vous reporter à la documentation du type: " +
                          GetTypeInfo().m_ClassName,
                      std::runtime_error);

            SetItem(item);
        }
        return *this;
    }

    template<typename T>
    void BaseItemVar<T>::SetFromItem(const std::wstring &str)
    {
        // \todo je ne comprend pas la raisons de toutes ces conditions…
        if((m_Item && str.empty()) || // On s'assure que cela ne sera pas appelé
                                      // plusieurs fois de suite.
           (!m_Item && Item::HasItem(str)) || (m_Item && m_Item->GetKey() != str))
        // (m_Item && m_Item->GetKey() == str)) // c'était comme ça avant… ça ressemble à une erreur.
        {
            SetItemFromItem(FromString<T>(str));
        }
    }

    template<typename T>
    void BaseItemVar<T>::SetFromItem(const ObservablePtr &observable)
    {
        if(m_Item != observable)
        {
            if(observable)
            {
                BaseItemVar<T>::ptr_type item = std::dynamic_pointer_cast<T>(observable);
                MA_ASSERT(
                    item,
                    L"L'observable passé en paramètre n'est pas ou n'hérite pas du type souhaité par cette variable. "
                    L"Veuillez vous reporter à la documentation du type: " +
                        GetTypeInfo().m_ClassName,
                    std::runtime_error);

                SetItemFromItem(item);
            }
            else
            {
                SetItemFromItem(nullptr);
            }
        }
    }

    template<typename T>
    void BaseItemVar<T>::SetItemFromItem(const BaseItemVar<T>::ptr_type &item)
    {
        if(m_Item != item)
        {
            bool active = Item::HasItem(GetItemKey());

            const auto var_key = GetKey();

            if(item)
            {
                const auto item_key = item->GetKey();

                MA_ASSERT(
                    active == Item::HasItem(item_key),
                    L"Si l'item de la variable est actif alors l'item passé en paramètre le doit être aussi. Pour "
                    L"rappel actif signifie que Item::HasItem renvoie vrais.",
                    std::invalid_argument);

                UpdateObservations({{Key::Obs::ms_BeginSetValue, var_key + ms_Sep + this->toString()}});
                m_PreviousKey = m_Item ? m_Item->GetKey() : L"";

                /*
                MA_ASSERT(active == Item::HasItem(item_key),
                          "L'item passé en paramètre a changé d'état après la " \
                          "mise à jour de l'observation. Si l'item de la variable" \
                          " est actif alors l'item passé en paramètre le doit " \
                          "être aussi. Pour rappel actif signifie que " \
                          "Item::HasItem renvoie vrais.",
                          function_name,
                          std::invalid_argument);
                //*/

                m_Item = active == Item::HasItem(item_key) ? item : nullptr;

                UpdateObservations({{Key::Obs::ms_EndSetValue, var_key + ms_Sep + this->toString()}});

                MA_ASSERT(
                    !m_Item || active == Item::HasItem(item_key),
                    L"L'item passé en paramètre a changé d'état après la mise à jour de l'observation. Si l'item de "
                    L"la variable est actif alors l'item passé en paramètre le doit être aussi. Pour rappel actif "
                    L"signifie que Item::HasItem renvoie vrais.",
                    std::invalid_argument);
            }
            else
            {
                UpdateObservations({{Key::Obs::ms_BeginSetValue, var_key + ms_Sep + this->toString()}});

                m_PreviousKey = m_Item ? m_Item->GetKey() : L"";
                m_Item = item;

                UpdateObservations({{Key::Obs::ms_EndSetValue, var_key + ms_Sep + this->toString()}});
            }
        }
    }

    template<typename T>
    VarPtr BaseItemVar<T>::Copy() const
    {
        auto ptr = std::shared_ptr<BaseItemVar<T>>(new BaseItemVar<T>(GetConstructorParameters()));

        ptr->m_PreviousKey = m_PreviousKey;
        ptr->m_Default = m_Default;

        return ptr;
    }

    // Type T_ItemHashSetVar<T> //——————————————————————————————————————————————————————————————————————————————————————
    template<typename T>
    T_ItemHashSetVar<T>::T_ItemHashSetVar(const Var::ConstructorParameters &params,
                                          T_ItemHashSetVar<T>::value_type value):
    HashSetVar(params),
    m_Value(std::move(value))
    {}

    template<typename T>
    T_ItemHashSetVar<T>::T_ItemHashSetVar(const Var::ConstructorParameters &params): HashSetVar(params), m_Value()
    {}

    template<typename T>
    T_ItemHashSetVar<T>::~T_ItemHashSetVar()
    {}

    template<typename T>
    std::wstring T_ItemHashSetVar<T>::toString() const
    {
        return ma::toString(m_Value);
    }

    template<typename T>
    std::wstring T_ItemHashSetVar<T>::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    template<typename T>
    void T_ItemHashSetVar<T>::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    template<typename T>
    void T_ItemHashSetVar<T>::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }

    template<typename T>
    bool T_ItemHashSetVar<T>::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<T_ItemHashSetVar<T>::value_type>(value);
    }

    template<typename T>
    void T_ItemHashSetVar<T>::Set(const T_ItemHashSetVar<T>::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    template<typename T>
    const typename T_ItemHashSetVar<T>::value_type &T_ItemHashSetVar<T>::Get() const
    {
        return m_Value;
    }

    template<typename T>
    T_ItemHashSetVar<T> &T_ItemHashSetVar<T>::operator=(const T_ItemHashSetVar<T>::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    template<typename T>
    void T_ItemHashSetVar<T>::SetFromItem(const std::wstring &value)
    {
        MA_ASSERT(!value.empty(), L"La chaîne de caractères est vide.", std::invalid_argument);

        MA_ASSERT(value.size() > 1u, L"La chaîne de caractères est invalide.", std::invalid_argument);

        MA_ASSERT(IsValidString(value), L"La chaîne de caractères est invalide.", std::invalid_argument);

        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

        m_Value = FromString<T_ItemHashSetVar<T>::value_type>(value);

        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    template<typename T>
    void T_ItemHashSetVar<T>::SetFromItem(const T_ItemHashSetVar<T>::value_type &value)
    {
        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Value = value;
        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    template<typename T>
    VarPtr T_ItemHashSetVar<T>::Copy() const
    {
        return std::shared_ptr<T_ItemHashSetVar<T>>(new T_ItemHashSetVar<T>(GetConstructorParameters(), m_Value));
    }

    template<typename T>
    void T_ItemHashSetVar<T>::Insert(T_ItemHashSetVar<T>::item_ptr item)
    {
        if(m_Value.find(item) == m_Value.end())
        {
            UpdateObservations({{Key::Obs::ms_BeginInsertValue, GetKey() + ms_Sep + item->GetKey()}});
            m_Value.insert(item);
            UpdateObservations({{Key::Obs::ms_EndInsertValue, GetKey() + ms_Sep + item->GetKey()}});
        }
    }

    template<typename T>
    void T_ItemHashSetVar<T>::Insert(const Item::key_type &key)
    {
        Insert(Item::GetItem(key));
    }

    template<typename T>
    void T_ItemHashSetVar<T>::Erase(item_ptr item)
    {
        MA_ASSERT(m_Value.find(item) != m_Value.end(),
                  L"L'item n'est pas présent dans le set. Vérifier sa présence avant d'appeler Erase",
                  std::invalid_argument);

        UpdateObservations({{Key::Obs::ms_BeginEraseValue, GetKey() + ms_Sep + item->GetKey()}});
        m_Value.erase(item);
        UpdateObservations({{Key::Obs::ms_EndEraseValue, GetKey() + ms_Sep + item->GetKey()}});
    }

    template<typename T>
    void T_ItemHashSetVar<T>::Erase(const Item::key_type &key)
    {
        Erase(Item::GetItem(key));
    }

    template<typename T>
    bool T_ItemHashSetVar<T>::HasItem(item_ptr item)
    {
        return m_Value.count(item);
    }

    template<typename T>
    bool T_ItemHashSetVar<T>::HasItem(const Item::key_type &key)
    {
        return m_Value.count(Item::GetItem(key));
    }

    // T_ItemVectorVar //——————————————————————————————————————————————————————————————————————————————————————
    template<typename T>
    T_ItemVectorVar<T>::T_ItemVectorVar(const Var::ConstructorParameters &params, T_ItemVectorVar<T>::value_type value):
    VectorVar(params),
    m_Value(std::move(value))
    {}

    template<typename T>
    T_ItemVectorVar<T>::T_ItemVectorVar(const Var::ConstructorParameters &params): VectorVar(params), m_Value()
    {}

    template<typename T>
    T_ItemVectorVar<T>::~T_ItemVectorVar()
    {}

    template<typename T>
    std::wstring T_ItemVectorVar<T>::toString() const
    {
        return ma::toString(m_Value);
    }

    template<typename T>
    std::wstring T_ItemVectorVar<T>::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    template<typename T>
    void T_ItemVectorVar<T>::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    template<typename T>
    void T_ItemVectorVar<T>::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }

    template<typename T>
    bool T_ItemVectorVar<T>::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<T_ItemVectorVar<T>::value_type>(value);
    }

    template<typename T>
    void T_ItemVectorVar<T>::Set(const T_ItemVectorVar<T>::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    template<typename T>
    const typename T_ItemVectorVar<T>::value_type &T_ItemVectorVar<T>::Get() const
    {
        return m_Value;
    }

    template<typename T>
    typename T_ItemVectorVar<T>::keys_type T_ItemVectorVar<T>::GetKeys() const
    {
        typename T_ItemVectorVar<T>::keys_type keys;

        for(const auto &item : m_Value)
            keys.push_back(item->GetKey());

        return keys;
    }

    template<typename T>
    T_ItemVectorVar<T> &T_ItemVectorVar<T>::operator=(const T_ItemVectorVar<T>::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    template<typename T>
    void T_ItemVectorVar<T>::SetFromItem(const std::wstring &value)
    {
        MA_ASSERT(!value.empty(), L"La chaîne de caractères est vide.", std::invalid_argument);

        MA_ASSERT(value.size() > 1u, L"La chaîne de caractères est invalide.", std::invalid_argument);

        MA_ASSERT(IsValidString(value), L"La chaîne de caractères est invalide.", std::invalid_argument);

        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

        m_Value = FromString<T_ItemVectorVar<T>::value_type>(value);

        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    template<typename T>
    void T_ItemVectorVar<T>::SetFromItem(const T_ItemVectorVar<T>::value_type &value)
    {
        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Value = value;
        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    template<typename T>
    VarPtr T_ItemVectorVar<T>::Copy() const
    {
        return std::shared_ptr<T_ItemVectorVar<T>>(new T_ItemVectorVar<T>(GetConstructorParameters(), m_Value));
    }

    template<typename T_Item>
    void T_ItemVectorVar<T_Item>::Insert(typename value_type::size_type index, T_ItemVectorVar<T_Item>::item_ptr item)
    {
        if(index >= 0 && index < m_Value.size())
        {
            UpdateObservations({{Key::Obs::ms_BeginInsertValue, GetKey() + ms_Sep + item->GetKey()}});
            m_Value.insert(std::next(m_Value.begin() + index), item);
            UpdateObservations({{Key::Obs::ms_EndInsertValue, GetKey() + ms_Sep + item->GetKey()}});
        }
    }

    template<typename T_Item>
    void T_ItemVectorVar<T_Item>::Insert(typename value_type::size_type index, const Item::key_type &key)
    {
        Insert(Item::GetItem(key));
    }

    template<typename T_Item>
    typename T_ItemVectorVar<T_Item>::value_type::size_type T_ItemVectorVar<T_Item>::Size() const
    {
        return m_Value.size();
    }

    template<typename T_Item>
    bool T_ItemVectorVar<T_Item>::Empty() const
    {
        return m_Value.empty();
    }

    template<typename T_Item>
    void T_ItemVectorVar<T_Item>::PushBack(item_ptr item)
    {
        UpdateObservations({{Key::Obs::ms_BeginInsertValue, GetKey() + ms_Sep + item->GetKey()}});
        m_Value.push_back(item);
        UpdateObservations({{Key::Obs::ms_EndInsertValue, GetKey() + ms_Sep + item->GetKey()}});
    }

    template<typename T_Item>
    void T_ItemVectorVar<T_Item>::PushBack(const ma::Item::key_type &key)
    {
        PushBack(Item::GetItem(key));
    }

    template<typename T>
    void T_ItemVectorVar<T>::Erase(item_ptr item)
    {
        auto it_find = std::find(m_Value.begin(), m_Value.end(), item);
        MA_ASSERT(it_find != m_Value.end(),
                  L"L'item n'est pas présent dans le set. Vérifier sa présence avant d'appeler Erase",
                  std::invalid_argument);

        UpdateObservations({{Key::Obs::ms_BeginEraseValue, GetKey() + ms_Sep + item->GetKey()}});
        m_Value.erase(it_find);
        UpdateObservations({{Key::Obs::ms_EndEraseValue, GetKey() + ms_Sep + item->GetKey()}});
    }

    template<typename T>
    void T_ItemVectorVar<T>::Erase(const Item::key_type &key)
    {
        Erase(Item::GetItem(key));
    }

    template<typename T>
    bool T_ItemVectorVar<T>::HasItem(item_ptr item)
    {
        return std::find(m_Value.begin(), m_Value.end(), item) != m_Value.end();
    }

    template<typename T>
    bool T_ItemVectorVar<T>::HasItem(const Item::key_type &key)
    {
        return HasItem(Item::GetItem(key));
    }

#ifdef wxUSE_GUI

    // wxObserverVar //—————————————————————————————————————————————————————————————————————————————————————————————————
    template<typename T>
    wxObserverVar<T>::wxObserverVar(const Var::ConstructorParameters &params, const value_type &var):
    ObserverVar(params, var),
    m_WindowId(var && (dynamic_cast<wxWindow *>(var) != nullptr) ? dynamic_cast<wxWindow *>(var)->GetId() : 0)
    {
        auto *window = dynamic_cast<wxWindow *>(m_Value);

        MA_ASSERT(window, L"Impossible de convertir «m_Value» en «wxWindow».", std::runtime_error);
    }

    template<typename T>
    wxObserverVar<T>::wxObserverVar(const Var::ConstructorParameters &params): ObserverVar(params), m_WindowId(0)
    {}

    template<typename T>
    wxObserverVar<T>::~wxObserverVar()
    {}

    template<typename T>
    std::wstring wxObserverVar<T>::toString() const
    {
        std::wstringstream ss;

        if(m_Value)
        {
            ss.precision(std::numeric_limits<int>::digits10);

            auto *window = dynamic_cast<wxWindow *>(m_Value);

            MA_ASSERT(window, L"Impossible de convertir «m_Value» en «wxWindow».", std::runtime_error);

            ss << window->GetId();
        }
        return ss.str();
    }

    template<typename T>
    std::wstring wxObserverVar<T>::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, toString()));
    }

    template<typename T>
    void wxObserverVar<T>::fromString(const std::wstring &str)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(str);
    }

    template<typename T>
    void wxObserverVar<T>::Set(const ObserverVar::value_type &value)
    {
        ObserverVar::Set(value);

        if(m_Value)
        {
            auto *window = dynamic_cast<wxWindow *>(m_Value);

            MA_ASSERT(window, L"Impossible de convertir «m_Value» en «wxWindow».", std::runtime_error);
        }
    }

    template<typename T>
    void wxObserverVar<T>::SetFromItem(const std::wstring &str)
    {
        MA_ASSERT(std::regex_match(str, g_IntRegType),
                  L"L'item ne prend en paramètre qu'une chaîne de caractère, représentent un chiffre de type «long».",
                  std::invalid_argument);

        long id = std::stoi(str);

        MA_ASSERT(id != 0,
                  L"L'id d'une fenêtre ne doit pas égale à 0. Cela pour éviter des comportements bizarres.",
                  std::invalid_argument);

        MA_ASSERT(id != 1,
                  L"L'id d'une fenêtre ne doit pas égale à 1. Cela pour éviter des comportements bizarres.",
                  std::invalid_argument);

        wxWindow *window = wxWindow::FindWindowById(id);

        MA_ASSERT(window, L"Aucune fenêtre n'est associée à l'id: «" + str + L"».", std::invalid_argument);

        auto value = dynamic_cast<T *>(window);

        MA_ASSERT(value, L"Impossible de convertir la fenêtre en «T».", std::invalid_argument);

        UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Value = value;
        m_WindowId = static_cast<wxWindowID>(id);
        UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    template<typename T>
    VarPtr wxObserverVar<T>::Copy() const
    {
        return std::shared_ptr<wxObserverVar<T>>(new wxObserverVar<T>(GetConstructorParameters(), m_Value));
    }

    template<typename T>
    bool wxObserverVar<T>::IsValidString(const std::wstring &str) const
    {
        if(std::regex_match(str, g_IntRegType))
        {
            try
            {
                long id = std::stoi(str);
                return id != 0 && id != 1 && wxWindow::FindWindowById(id);
            }
            catch(const std::exception &e)
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    template<typename T>
    bool wxObserverVar<T>::IsValid() const
    {
        return m_Value && m_WindowId != 0 && m_WindowId != 1 && wxWindow::FindWindowById(m_WindowId) != nullptr;
    }

    template<typename T>
    void wxObserverVar<T>::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }
#endif // wxUSE_GUI
} // namespace ma