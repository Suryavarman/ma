/// \file Resource.h
/// \author Pontier Pierre
/// \date 2020-03-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/Sort.h"
#include "ma/Find.h"
#include "ma/Dll.h"

// #include "wx/fswatcher.h"

/// Spécialisation de « std::hash » pouvant injecter dans l'espace de nom « std ».
// namespace std
//{
//     template<>
//     struct hash<wxFileName>
//     {
//             std::size_t operator()(wxFileName const &filename) const noexcept
//             {
//                 return std::hash<std::wstring>{}(filename.GetFullPath().ToStdWstring());
//             }
//     };
// } // namespace std

namespace ma
{

    class M_DLL_EXPORT Resource: public ma::Item
    {
        public:
            typedef ma::MatchVar<ma::FilePathVar, ma::Resource> MatchResource;

            static const ma::Var::key_type ms_KeyPath;
            static const ma::Var::key_type ms_KeySize;

            ma::FilePathVarPtr m_Path;

            /// La taille du fichier lisible par un humain.
            ma::StringVarPtr m_Size;

            /// Le nom du dossier.
            ma::StringVarPtr m_Name;

            /// \tparam T_Context Le contexte qui sera utilisé pour renvoyer la ressource au format T.
            /// \tparam T Le type de la donnée qui sera renvoyée. Ça sera une variable d'item qui contiendra le type
            /// recherché.
            /// \param client L'item qui a besoin de cette ressource.
            /// \example wxImageVarPtr image = resource->Get<wxContext, wxImageVar>(this->GetPtr());
            template<typename T_Context, typename T>
            std::shared_ptr<T> Get(ma::ItemPtr client);

            Resource(const Item::ConstructorParameters &in_params, const std::filesystem::path &path);
            Resource() = delete;
            ~Resource() override;

            /// Pour récupérer d'un item parent les items de type ressource.
            /// \code {.cpp}
            /// auto folders1 = ma::Find<ma::Resource::MatchResource>::GetHashMap(ma::Item::ms_KeyResourceManager,
            /// ma::Resource::GetMatchResource(), ma::Item::SearchMod::RECURSIVE);
            /// auto folders2 = ma::Find<ma::Resource::MatchResource>::GetDeque(item->GetKey(),
            /// ma::Resource::GetMatchResource(), ma::Item::SearchMod::LOCAL); \endcode
            static const ma::Resource::MatchResource &GetMatchResource();

            M_HEADER_CLASSHIERARCHY(Resource)
    };
    typedef std::shared_ptr<Resource> ResourcePtr;
    typedef std::unordered_map<Resource::MatchResource::value_type, ma::ResourcePtr> Resources;

    class M_DLL_EXPORT ResourceVar: public ma::BaseItemVar<Resource>
    {
        protected:
            using BaseItemVar<Resource>::Set;

        public:
            using BaseItemVar<Resource>::BaseItemVar;

            M_HEADER_CLASSHIERARCHY(ResourceVar)
    };
    typedef std::shared_ptr<ResourceVar> ResourceVarPtr;

    template M_DLL_EXPORT ResourceVarPtr ma::Item::AddVar<ma::ResourceVar>(const ma::Var::key_type &key,
                                                                           const ma::ResourceVar::ptr_type &value);

    template M_DLL_EXPORT ResourceVarPtr ma::Item::AddVar<ma::ResourceVar>(const ma::Var::key_type &key);

    class Folder;
    typedef std::shared_ptr<Folder> FolderPtr;

    class M_DLL_EXPORT Folder: public ma::Item, public ma::Observer
    {
        private:
            friend ma::Item;

        protected:
            virtual void Build();

            /// \todo Bizarre voir pour quoi cette fonction existe encore ? Le système de sauvegarde ne devrait pas en
            ///       avoir besoin.
            /// \throw std::invalid_argument si l'élément existe déjà dans l'instance courante
            ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry) override;

            void OnEnable() override;
            void OnDisable() override;

        public:
            typedef ma::MatchVar<ma::DirectoryVar, ma::Folder> MatchFolder;

            struct Key
            {
                    struct Var
                    {
                            static const ma::Var::key_type &GetPath();
                            static const ma::Var::key_type &GetRecursive();
                            static const ma::Var::key_type &GetSize();
                            static const ma::Var::key_type &GetHasToExist();
                    };

                    struct Obs
                    {
                            /// \brief Clef qui permettra à un observateur de ce dossier, d'être notifié lorsque le
                            /// dossier est fini d'être construit. C'est à dire lorsque tout ses enfants ont été ajouté.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{Key::Obs::ms_FolderConstructionEnd, folder->GetKey()}});
                            /// \endcode
                            static const ma::Observable::Data::key_type ms_ConstructionEnd;
                    };
            };

            /// Si vrais le chemin du dossier doit exister.
            ma::BoolVarPtr m_HasToExist;

            /// Chemin du dossier
            ma::DirectoryVarPtr m_Path;

            /// Si vrais alors les dossiers qu'il contient seront ajouté à
            /// l'arborescence.
            ma::BoolVarPtr m_Recursive;

            /// La taille du dossier lisible par un humain.
            ma::StringVarPtr m_Size;

            /// Le nom du dossier.
            ma::StringVarPtr m_Name;

            /// \return Vrais:
            /// - si le chemin du dossier existe
            /// - si c'est un dossier
            /// - si les droits en lecture et en écriture sont disponibles (les tests ne fonctionnent pas).
            /// \remarks Si vous souhaitez lever une exception dans le cas où le chemin n'est pas valide, il serait
            /// peut-être meilleur d'utiliser la fonction « AssertValid ».
            /// \see AssertValid
            static bool IsValid(const std::wstring &path);

            /// Lance une exception si le chemin n'est pas valide. Cela permet de connaître l'erreur qui a été levée.
            /// \see IsValid
            static void AssertValid(const std::filesystem::path &value);

            /// \return Le chemin normalisé. Le chemin normalisé est absolue.
            /// Il est sans ".." et sans ".", toutes les variables d'environnement sont remplacées par leur valeur
            /// associée.
            /// \remarks Dans de très rares cas normaliser un chemin peut transformer un chemin valide en
            /// chemin invalide. (Les quels?).
            /// Si vous utilisez « std::filesystem::path » il vous faudra le créer ainsi :
            /// \code
            /// {.cpp} std::filesystem::path path = ma::Path(value); \endcode \see
            /// https://docs.wxwidgets.org/trunk/classwx_file_name.html#a2db3fcc42863daa265619de0d5f390df
            static void Normalize(std::filesystem::path& path);

            /// \return La liste des ressources incluses dans ce dossier. C'est à dire pas celles dans les
            /// sous-dossiers.
            virtual ma::Resources GetResources(ma::Item::SearchMod search_mod = ma::Item::SearchMod::LOCAL) const;

            virtual std::optional<ma::ResourcePtr>
            HasResource(const std::filesystem::path &path,
                        ma::Item::SearchMod search_mod = ma::Item::SearchMod::LOCAL) const;

            /// C'est ce constructeur qui sera utilisé par la RTTI
            explicit Folder(const Item::ConstructorParameters &in_params,
                            const std::wstring &path = {},
                            bool recursive = true,
                            bool has_to_exist = false);

            /// \remarks Si path est créé à partir d'une chaîne de caractères.
            /// Il doit être créé ainsi :
            /// \code {.cpp}
            /// ma::Path(str);
            /// \endcode
            explicit Folder(const Item::ConstructorParameters &in_params,
                            const std::filesystem::path &path,
                            bool recursive = true,
                            bool has_to_exist = false);

            /// Constructeur avec le chemin du répertoire représenté par la std.
            explicit Folder(const Item::ConstructorParameters &in_params,
                            const std::filesystem::directory_entry &dir_entry,
                            bool recursive = true,
                            bool has_to_exist = false);

            /// \remarks Pour éviter l'erreur E1790 sous visual
            Folder() = delete;

            /// Destructeur de la classe Folder
            ~Folder() override;
            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            /// \param folder_name Nom du sous-dossier à tester.
            /// \return vrais si le nom du dossier correspond à un sous-dossier
            /// direct de cette instance.
            virtual bool HasSubFolder(const std::wstring &folder_name);

            /// \param path Chemin du sous-dossier à tester.
            /// \return vrais si le nom du dossier correspond à un sous-dossier
            /// direct de cette instance.
            virtual bool HasSubFolder(const std::filesystem::path &path);

            /// \exception std::invalid_argument folder_name correspond à un
            /// sous-dossier déjà présent.
            virtual ma::FolderPtr AddSubFolder(const std::wstring &folder_name);

            /// \exception std::invalid_argument path correspond à un
            /// sous-dossier déjà présent.
            virtual ma::FolderPtr AddSubFolder(const std::filesystem::path &path);

            /// Renvoie faux si la ressource ou le dossier est déjà enfant.
            bool AcceptToAddChild(const ma::ItemPtr &item) const override;

            /// Pour récupérer d'un item parent les items de type ressource.
            /// \code {.cpp}
            /// auto folders1 = ma::Find<ma::Folder::MatchFolder>::GetHashMap(ma::Item::ms_KeyResourceManager,
            /// ma::Folder::GetMatchFolder(), ma::Item::SearchMod::RECURSIVE);
            /// auto folders2 = ma::Find<ma::Folder::MatchFolder>::GetDeque(item->GetKey(),
            /// ma::Folder::GetMatchFolder(), ma::Item::SearchMod::LOCAL); \endcode
            static const MatchFolder &GetMatchFolder();

            M_HEADER_CLASSHIERARCHY(Folder)
    };
    M_HEADER_ITEM_TYPE_VAR(Folder)

    /// \todo Faire un module qui concrétise cette classe.
    ///       https://github.com/ThomasMonkman/filewatch
    ///       https://solarianprogrammer.com/2019/01/13/cpp-17-filesystem-write-file-watcher-monitor/
    ///       https://github.com/SpartanJ/efsw
    class FileWatcher : public Item, public Observer
    {
        public:
            struct Key
            {
                    struct Obs
                    {
                            /// \brief Clef qui permettra à un observateur de dossier de reconnaître un évènement
                            /// d'ajout de dossier ou de ressource.
                            /// La valeur associée devra être la clef de l'item ajouté.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_Add, item->GetKey()}});
                            /// \endcode
                            static const ma::Observable::Data::key_type ms_Add;

                            /// \brief Clef qui permettra à un observateur de dossier ou de ressource de reconnaître
                            /// un évènement de suppression d'un dossier ou d'une ressource.
                            /// La valeur associée devra être la clef de l'item ajouté.
                            /// \example
                            /// \code{.cpp}
                            /// this->UpdateObservations({{ms_Delete, item->GetKey()}});
                            /// \endcode
                            static const ma::Observable::Data::key_type ms_Delete;

                            static const ma::Observable::Data::key_type ms_Modified;
                            static const ma::Observable::Data::key_type ms_Created;
                            static const ma::Observable::Data::key_type ms_Moved;
                            static const ma::Observable::Data::key_type ms_Rename;
                    };
            };

            explicit FileWatcher(const Item::ConstructorParameters &in_params);
            FileWatcher() = delete;
            ~FileWatcher() override;

            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            virtual void AddWatch(ma::FolderPtr folder) = 0;
            virtual void AddWatch(ma::ResourcePtr resource) = 0;
            virtual void RemoveWatch(ma::FolderPtr folder) = 0;
            virtual void RemoveWatch(ma::ResourcePtr resource) = 0;

        protected:
            void OnEnable() override;
            void OnDisable() override;

            void OnFileSystemEvent(const ma::Observable::Data::key_type &event, const std::filesystem::path& path);
    };

    /// \see https://docs.wxwidgets.org/trunk/classwx_file_system_watcher.html
    /// \see https://github.com/wxWidgets/wxWidgets/blob/master/samples/fswatcher/fswatcher.cpp
    class M_DLL_EXPORT ResourceManager: public Item, public Observer
    {
        protected:
            void OnEnable() override;
            void OnDisable() override;

        public:
            static const ma::Var::key_type ms_KeyResources;

            /// \todo le refaire sans wxWidgets:
            ///       https://github.com/ThomasMonkman/filewatch
            ///       https://solarianprogrammer.com/2019/01/13/cpp-17-filesystem-write-file-watcher-monitor/
            ///       https://github.com/SpartanJ/efsw
            /// C'est à l'application wxWidgets d'instancier m_FileSystemWatcher
            /// \remarks Implementation limitations: this class is currently implemented for MSW, macOS and GTK ports
            /// but doesn't detect all changes correctly everywhere: under MSW accessing the file is not detected (only
            /// modifying it is) and under macOS neither accessing nor modifying is detected (only creating and deleting
            /// files is). Moreover, macOS version doesn't currently collapse pairs of create/delete events in a rename
            /// event, unlike the other ones.
            ///     The application's event loop needs to be running before a wxFileSystemWatcher can be properly
            ///     created,
            /// and that is why one should not be created too early during application startup. If you intend to create
            /// a wxFileSystemWatcher at startup, you can override wxAppConsole::OnEventLoopEnter() to ensure it is not
            /// done too early.
            ///
            /// \see https://docs.wxwidgets.org/stable/classwx_file_system_watcher.html
            // std::unique_ptr<wxFileSystemWatcher> m_FileSystemWatcher;

            /// La liste des clefs des ressources.
            const ma::StringHashSetVarPtr m_Resources;

            /// Relie la hiérarchie d'un dossier à un Item de type Folder.
            /// \exception std::invalid_argument Le dossier ou si « recursive » est vrais et que l'un des sous-dossiers
            /// est déjà observé.
            ma::FolderPtr AddFolder(const std::wstring &path, bool recursive);

            /// Relie la hiérarchie d'un dossier à un Item de type Folder.
            /// \exception std::invalid_argument Le dossier ou si « recursive » est vrais et que l'un des sous-dossiers
            /// est déjà observé.
            ma::FolderPtr AddFolder(const std::filesystem::directory_entry &dir_entry, bool recursive);

            /// Supprime la gestion des ressources et l'observation d'un dossier et de ses sous-dossiers.
            /// \exception std::invalid_argument
            void RemoveFolder(const std::wstring &path);

            /// \return Vrais si le dossier est observé.
            /// \exception std::invalid_argument Le chemin du dossier est invalide.
            ///                         Normaliser le, avant de l'utiliser.
            /// \see ma::Folder::Normalize
            bool HasFolder(const std::wstring &path);

            /// \exception std::invalid_argument Le chemin du dossier est invalide.
            /// Normaliser le, avant de l'utiliser.
            /// \see ma::Folder::Normalize
            std::optional<ma::FolderPtr> HasFolderOpt(const std::wstring &path);

            /// \return Le dossier associé au chemin.
            /// \exception std::invalid_argument Le chemin du dossier est invalide.
            /// Normaliser le, avant de l'utiliser.
            /// \exception std::invalid_argument Le dossier pointer par le chemin n'est pas surveillé.
            /// \see ma::Folder::Normalize
            ma::FolderPtr GetFolder(const std::wstring &path);

            /// \return Toutes les ressources observées.
            ma::Resources GetResources();

            /// \todo le refaire sans wxWidgets:
            ///       https://github.com/ThomasMonkman/filewatch
            ///       https://solarianprogrammer.com/2019/01/13/cpp-17-filesystem-write-file-watcher-monitor/
            ///       https://github.com/SpartanJ/efsw
            /// ResourceManager ne possède le mécanisme d'évènements de
            /// wxWidgets qui permet d'appeler OnFileSystemEvent.
            /// C'est à l'application graphique de faire le lien.
            /// \see <a
            /// https://github.com/wxWidgets/wxWidgets/blob/master/samples/fswatcher/fswatcher.cpp>fswatcher.cpp</a>
            /// \example
            /// \code (.cpp)
            /// MyFrame::MyFrame(…):
            /// m_FileSystemWatcher()
            /// {
            ///     ma::root()->resources->m_FileSystemWatcher = &m_FileSystemWatcher;
            ///     m_FileSystemWatcher.SetOwner(this);
            ///     …
            ///     Bind(wxEVT_FSWATCHER, &MyFrame::OnFileSystemEvent, this);
            /// }
            ///
            /// MyFrame::OnFileSystemEvent(wxFileSystemWatcherEvent& event)
            /// {
            ///     ma::root()->resources->OnFileSystemEvent(event);
            /// }
            /// \endcode
            // void OnFileSystemEvent(wxFileSystemWatcherEvent &event);

            explicit ResourceManager(const Item::ConstructorParameters &in_params);
            ResourceManager() = delete;
            ~ResourceManager() override;

            /// Permet au gestionnaire de ressources de s'observer.
            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            /// C'est ici que commence l'observation des nouveaux dossiers à observer.
            /// Par contre l'observation des sous dossiers ne débute pas ici.
            void AddChild(ma::ItemPtr item) override;

            /// Si le dossier est déjà surveillé alors sera refusé.
            bool AcceptToAddChild(const ma::ItemPtr &item) const override;

            M_HEADER_CLASSHIERARCHY(ResourceManager)
    };
    typedef std::shared_ptr<ResourceManager> ResourceManagerPtr;

    class M_DLL_EXPORT ResourceVectorVar: public ma::T_ItemVectorVar<ma::Resource>
    {
        public:
            using ma::T_ItemVectorVar<ma::Resource>::T_ItemVectorVar;
            M_HEADER_CLASSHIERARCHY(ResourceVectorVar)
    };
    typedef std::shared_ptr<ResourceVectorVar> ResourceVectorVarPtr;

    template M_DLL_EXPORT ResourceVectorVarPtr
    ma::Item::AddVar<ResourceVectorVar>(const ma::Var::key_type &key, const ResourceVectorVar::value_type &value);
    template M_DLL_EXPORT ResourceVectorVarPtr ma::Item::AddVar<ResourceVectorVar>(const ma::Var::key_type &key);

    class M_DLL_EXPORT FolderVectorVar: public ma::T_ItemVectorVar<ma::Folder>
    {
        public:
            using ma::T_ItemVectorVar<ma::Folder>::T_ItemVectorVar;
            M_HEADER_CLASSHIERARCHY(FolderVectorVar)
    };
    typedef std::shared_ptr<FolderVectorVar> FolderVectorVarPtr;

    template M_DLL_EXPORT FolderVectorVarPtr
    ma::Item::AddVar<FolderVectorVar>(const ma::Var::key_type &key, const FolderVectorVar::value_type &value);
    template M_DLL_EXPORT FolderVectorVarPtr ma::Item::AddVar<FolderVectorVar>(const ma::Var::key_type &key);
} // namespace ma

#include "ma/Resource.tpp"
