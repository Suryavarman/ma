/// \file Context.tpp
/// \author Pontier Pierre
/// \date 2020-11-15
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

// ContextManager // ———————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Context>
std::shared_ptr<T_Context> ma::ContextManager::Get(const ma::Var::key_type &name)
{
    MatchVar<ma::StringVar, T_Context> match(Key::Var::GetName());
    auto contexts = ma::Find<MatchVar<ma::StringVar, T_Context>>::GetDeque(GetKey(), match, ma::Item::SearchMod::LOCAL);

    auto it_find = contexts.find(name);
    MA_ASSERT(it_find != contexts.end(), L"Le contexte «" + name + " n'existe pas.", std::invalid_argument);

    return it_find.second;
}

template<typename T_Context>
bool ma::ContextManager::Has(const ma::Var::key_type &name)
{
    MatchVar<ma::StringVar, T_Context> match(Key::Var::GetName());
    auto contexts = ma::Find<MatchVar<ma::StringVar, T_Context>>::GetDeque(GetKey(), match, ma::Item::SearchMod::LOCAL);

    return contexts.find(name) != contexts.end();
}

// Context // ——————————————————————————————————————————————————————————————————————————————————————————————————————————
template<typename T_Data>
std::shared_ptr<T_Data> ma::Context::GetData(ma::ItemPtr client) const
{
    MA_HAS_TO_BE_IMPLEMENTED;

    return {};
}

template<typename T_DataMaker>
std::deque<std::shared_ptr<T_DataMaker>> ma::Context::GetDataMakers() const
{
    MA_ASSERT(m_DataMakers, L"m_DataMakers est nul.", std::logic_error);
    typedef MatchVar<StringVar, T_DataMaker> match_data_maker;

    match_data_maker matcher{DataMaker::ms_Key_ClientClassName};

    return Find<match_data_maker>::GetDeque(m_DataMakers->GetKey(), matcher, ma::Item::SearchMod::LOCAL);
}

template<typename T_DataMaker, typename... Args>
std::shared_ptr<T_DataMaker> ma::Context::AddDataMaker(const Args &...args)
{
    auto makers = GetContextDataMaker();

    typedef ma::MatchType<T_DataMaker> match_data_maker_type;
    match_data_maker_type match_item;
    const auto makers_key = makers->GetKey();
    auto data_makers = ma::Find<match_data_maker_type>::GetDeque(makers_key, match_item, ma::Item::SearchMod::LOCAL);

    MA_ASSERT(data_makers.size() < 2u,
              L"Un type de donnée ne peut avoir qu'un seul type de fabricateur associé.",
              std::logic_error);

    std::shared_ptr<T_DataMaker> result;

    if(data_makers.empty())
        result = ma::Item::CreateItem<T_DataMaker>(ma::Item::CreateParameters(makers_key, true, false), args...);
    else
        result = data_makers[0];

    return result;
}

template<typename T_DataMaker>
void ma::Context::RemoveDataMaker()
{
    typedef ma::MatchType<ContextDataMaker> match_context_data_type;
    auto context_data_makers =
        ma::Find<match_context_data_type>::GetDeque(GetKey(), match_context_data_type(), ma::Item::SearchMod::LOCAL);

    const auto context_class_name = GetTypeInfo().m_ClassName;

    MA_ASSERT(context_data_makers.size() < 2u,
              L"Il y a plus d'un « ContextDataMaker » dans ce context: " + context_class_name,
              std::logic_error);

    ma::ContextDataMakerPtr context_data_maker;

    MA_ASSERT(!context_data_makers.empty(), L"Aucun context n'est associé à cette fabrique.", std::invalid_argument);
    context_data_maker = context_data_makers[0];

    typedef ma::MatchType<T_DataMaker> match_data_maker_type;
    match_data_maker_type match_item;
    const auto data_maker_key = context_data_maker->GetKey();
    auto data_makers =
        ma::Find<match_data_maker_type>::GetDeque(data_maker_key, match_item, ma::Item::SearchMod::LOCAL);

    MA_ASSERT(data_makers.size() < 2u,
              L"Un type de donnée ne peut avoir qu'un seul type de fabricateur associé.",
              std::logic_error);

    std::shared_ptr<T_DataMaker> result;

    MA_ASSERT(!data_makers.empty(), L"Aucune fabrique n'a été trouvée.", std::logic_error);

    context_data_maker->RemoveChild(data_makers[0]);
    if(!context_data_maker->Empty())
        RemoveChild(context_data_maker);
}