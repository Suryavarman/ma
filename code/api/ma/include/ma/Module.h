/// \file Module.h
/// \author Pontier Pierre
/// \date 2023-07-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/Resource.h"
#include "ma/Context.h"
#include "ma/ClassInfo.h"
#include "ma/Dll.h"

namespace ma
{
    // namespace module // ... module est un mot clef depuis c++20 https://en.cppreference.com/w/cpp/language/modules

    class ModuleManager;

    /// Permet selon le chemin du dossier du module de savoir si cet Item peut s'occuper de la génération du module.
    class M_DLL_EXPORT ModuleMaker: public DataMaker
    {
        public:
            std::shared_ptr<ModuleManager> m_ModuleManager;

            explicit ModuleMaker(const Item::ConstructorParameters &in_params,
                                 const std::wstring &data_class_name,
                                 const std::wstring &client_class_name);
            ModuleMaker() = delete;
            ~ModuleMaker() override;

            /// \return Si le dossier du module est géré par ce type, la fonction renvoie le chemin du fichier d'entré
            /// du module.
            /// Exemple:
            /// module_toto
            /// |
            /// *—— module_toto.dll ou libmodule_toto.dll (voir ModuleCMaker)
            /// \remarks Pour certains type de module, le faite de renvoyé le chemin du fichier d'entré, ne sera pas
            /// forcément utile ou ayant un sens mais pour les types de modules pour lesquels cela fait sens, cette
            /// valeur de retour sera très utile.
            virtual std::optional<std::filesystem::path>
            Match(const std::filesystem::directory_entry &dir_entry) const = 0;

            /// \return Si le dossier du module contient un fichier de configuration du même nom avec l'extension : \n
            /// - « .cfg »
            /// alors la fonction renverra le chemin du fichier de configuration du module.
            static std::optional<std::filesystem::path> HasConfig(const std::filesystem::directory_entry &dir_entry);

            /// \return Renvoie la liste module dont décrite dans le fichier de configuration.
            /// {{clef du module A, chemin du répertoire du module A}, {clef du module B, chemin du répertoire du module
            /// B}}
            /// \remarks Nous utilisons un vector pour conserver l'ordre d'insertion.
            /// \exception std::invalid_argument Le chemin du fichier de configuration est un répertoire.
            /// \exception std::invalid_argument Le chemin du fichier de configuration n'existe pas.
            using ModuleDependencies = std::vector<std::pair<Item::key_type, std::filesystem::directory_entry>>;
            static ModuleDependencies GetModuleDependencies(const std::filesystem::path &config_path);

            M_HEADER_CLASSHIERARCHY(ModuleMaker)
    };
    typedef std::shared_ptr<ModuleMaker> ModuleMakerPtr;

    /// Un module est défini par un dossier contenant une librairie dynamique ou un script python.
    /// Ce dossier porte le nom du module.
    /// ex: %user%/.ma/gui/apps/3a4f8ad3-f856-1bba-x7c2-a351bfa5ffa2/modules/ogre
    /// À l'intérieur de ce dossier :
    /// Si le module est une librairie dynamique, elle portera alors le nom ogre.so, ogre.dll ou ogre.dynlib.
    /// Si le module est du code python le script qui sera recherché sera ogre.py.
    /// \remarks Les « ma::TypeInfo » définis dans les modules, doivent être lié à leur module. Cela permettra que
    /// lorsque le module se décharge, de libérer la définition des types qui ont été ajoutés par le module.
    /// \code{.cpp}
    ///
    /// \endcode
    class M_DLL_EXPORT Module: public ma::Data, public ma::Observer
    {
        protected:
            virtual void SetObservation(const ma::ObservablePtr &observable);

            /// Appel Data::OnEnable et Observer::OnEnable
            void OnEnable() override;

            /// Appel Data::OnDisable et Observer::OnDisable
            void OnDisable() override;

        public:
            /// Le context s'occupant de charger les modules.
            std::shared_ptr<ModuleManager> m_Context;

            /// Permet d'avoir accès à l'élément qui a été capable de le générer.
            ModuleMakerPtr m_Maker;

            /// Le dossier contenant le module.
            FolderVarPtr m_Folder;

            /// Le nom du module.
            /// \remarks Il portera le nom du dossier.
            StringVarPtr m_Name;

            /// Le fichier de configuration qui sera lu avant de charger le module.
            /// Il porte le même nom que le dossier du module et possède l'extension « .cfg ».
            /// Exemples: \n
            /// module_toto —> libmodule_toto.cfg \n
            /// moduuli_toto2 —> moduuli_toto.cfg \n
            /// Les groupes possibles sont: \n
            /// - [dependencies] \n
            /// Les valeurs possibles pour chaque groupe sont:
            /// - [dependencies] \n
            ///     modules={"chemin relatif du dossier du module A", \n
            ///              "chemin relatif du dossier du module B"} \n
            ///     Ex: \n
            ///         modules={"02c3f38c-7477-430d-9e9c-2298b3263768"} \n
            /// \n
            ///    Le chemin relatif permet de retrouver le module. Le « .guid » sera utilisé pour retrouvé rapidement
            /// le dossier si il est déjà surveillé, sinon il sera chargé. \n
            ma::ResourceVarPtr m_Config;

            explicit Module(const Item::ConstructorParameters &params,
                            const ContextLinkPtr &link,
                            const ItemPtr &client,
                            ModuleMakerPtr maker);
            Module() = delete;
            ~Module() override;

            /// Pour finaliser la construction.
            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            /// Charge le module.
            /// \exception std::invalid_argument Le module ne peut être chargé par ce contexte. Match renvoie faux.
            bool Load() override;

            /// Décharge le module
            bool UnLoad() override;

            /// Par défaut le comportement est d'abord d'appeler Unload et si celui-ci renvoie vrai.
            bool Reload() override;

            /// \param module_directory Le chemin du dossier du module.
            /// \return Le GUID du module défini dans le fichier module_mom_du_module.guid contenu dans module_directory
            /// Exemple de fichier « .guid »
            /// \code
            /// 8b12dd47-51c5-4336-9b24-3748163dc1df
            /// \endcode
            static Item::key_type GetGuid(const std::filesystem::directory_entry &module_directory);

            /// \param plugin_file_name Le chemin d'un fichier contenu directement dans le dossier du module.
            /// \return Le GUID du module défini dans le fichier module_mom_du_module.guid contenu dans le dossier
            /// parent du fichier.
            /// Exemple de fichier « .guid »
            /// \code
            /// 8b12dd47-51c5-4336-9b24-3748163dc1df
            /// \endcode
            static Item::key_type GetGuid(const std::filesystem::path &plugin_file_name);

            /// \copydoc ma::Data::MakerReplacement
            void MakerReplacement(ma::DataMakerPtr old_maker, ma::DataMakerPtr new_maker) override;

            M_HEADER_CLASSHIERARCHY(Module)
    };
    typedef std::shared_ptr<Module> ModulePtr;

    /// La ressource (.dll, .so, .dylib, .py, etc) sera chargé dans le context qui lui est dédié.
    /// Si c'est une .dll ça sera le contexte ModuleC qui saura comment la charger.
    /// \remarks Convention le nom d'un dossier préfixé « module_ » sera considéré comme un module. ModuleManager
    /// s'occupera alors de trouver la surcharge de « Module » qui lui correspond. Ce qui implique que « ModuleManager »
    /// observe le gestionnaire de ressources. Dès qu'il perçoit l'ajout ou la suppression d'un dossier, il va tester le
    /// nom du dossier et gérer en conséquence les modules.
    class M_DLL_EXPORT ModuleManager: public Context, public ma::Observer
    {
        private:
            /// Raccourcis pour accéder au gestionnaire de ressources.
            ma::ResourceManagerPtr m_ResourceManager;

            /// Raccourcis pour accéder au gestionnaire d'observation.
            ma::ObservationManagerPtr m_ObservationManager;

            /// Raccourcis pour accéder au registre des types.
            ma::ClassInfoManagerPtr m_ClassInfoManager;

            /// Test si une fabrique de module peut gérer ce dossier, si oui celle-ci le créera.
            std::optional<ma::ContextLinkPtr> MakersCheckFolder(const ma::FolderPtr &folder);

        protected:
            void OnEnable() override;
            void OnDisable() override;

            /// Au chargement des éléments, il faut pouvoir charger un module sans le que système d'observation soit
            /// enclanché. Certains type sont définis dans des modules et doivent être charger pour les instancier.
            ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry) override;

        public:
            /// Liste les différents prefixes qui seront utilisé pour définir un dossier comme étant un module.
            /// \remarks Cela est surtout utilisé pour définir le mot « module » dans différentes langues.
            ma::StringSetVarPtr m_PrefixesFolder;

            explicit ModuleManager(const Item::ConstructorParameters &in_params);
            ModuleManager() = delete;
            ~ModuleManager() override;
            void EndConstruction() override;

            // Concrétisation de ma::Observer
            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            ma::DataPtr BuildData(ma::ContextLinkPtr link, ma::DataPtr parent, ma::ItemPtr client) override;

            /// \return Les modules trié selon leur nom.
            std::unordered_multimap<std::wstring, ma::ModulePtr> GetModules();

            /// \return Vrais si le dossier contient est composé de l'un des prefix de m_PrefixesFolder puis du
            /// caractère underscore "_" puis du nom du module.
            /// \example « โมดูล_toto » ou « module_tata »
            std::optional<std::wstring> IsValidModuleFolderName(const std::filesystem::directory_entry &entry);

            M_HEADER_CLASSHIERARCHY(ModuleManager)
    };
    typedef std::shared_ptr<ModuleManager> ModuleManagerPtr;

} // namespace ma
