/// \file Ma.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

/// \mainpage Maçon de l'espace - Documentation de l'api.
/// Maçon de l'espace alias [Ma](https://framagit.org/Suryavarman/ma) est un projet écrit en c++17 et python3.
///
/// L'objectif est de créer un éditeur d'applications 3D simple qui permet d'interfacer différents moteurs 3D tel que
/// Ogre3D et Pand3D.
///
/// La version du projet en est encore au stade d'alpha.
///
/// ## Liens
///
/// Documentation de l'api de Ma : https://suryavarman.fr/stockage/ma/doc/
///
/// Git : [https://framagit.org/Suryavarman/ma](https://framagit.org/Suryavarman/ma)
///
/// Extraits de code : https://framagit.org/Suryavarman/ma/-/snippets
/// \htmlonly <div><center><a href="https://youtu.be/Bezg7UYlQkA?list=PLwg0gkr8hTmXBe0G7GkteWBA462lpuQYG"
/// target="_blank"><img src="http://img.youtube.com/vi/qBFl-uu-lHs/0.jpg" alt="wxCppPy démonstration" width="480"
/// height="360" border="10" /></a></center></div> \endhtmlonly
/// \section intro_sec L'api de Ma définie la structure sa donnée de Ma.
/// todo écrire le contenu de cette section.
///
/// \section ma L'api de Maçon de l'espace est la base de la solution.
/// todo écrire le contenu de cette section.
///
/// <a href="https://framagit.org/Suryavarman/ma" target="_blank">Maçon de l'espace</a>.
///
/// L'aide pour installer ce projet :
/// - <a href="https://framagit.org/Suryavarman/ma/-/blob/master/API/README.md" target="_blank">README.md</a>
///
/// Information sur la license MIT.
/// - <a href="https://framagit.org/Suryavarman/ma/-/blob/master/LICENSE" target="_blank">LICENSE</a>
///
/// Thème utilisé pour « Doxygène »: [doxygen-awesome-css](https://jothepro.github.io/doxygen-awesome-css/index.html)
///

#pragma once

/// L'espace de nom de l'api de Ma. L'API définie la gestion de la structure de données de Ma.
/// L'API est destinée à être utilisé pour des éditeurs de données.
/// \brief L'API de la structure de données du projet Ma.
namespace ma
{}

// Cœur
#include "ma/Platform.h"
#include "ma/Prerequisites.h"
#include "ma/Macros.h"
#include "ma/String.h"
#include "ma/Serialization.h"
#include "ma/wx/wx.h"
#include "ma/Algorithm.h"
#include "ma/TypeInfo.h"
#include "ma/Error.h"
#include "ma/Noncopyable.h"
#include "ma/crossguid/guid.h"
#include "ma/Id.h"
#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/RTTI.h"
#include "ma/Garbage.h"
#include "ma/Observer.h"
#include "ma/ObservationManager.h"
#include "ma/Resource.h"
#include "ma/Root.h"
#include "ma/ClassInfo.h"
#include "ma/Sort.h"
#include "ma/Find.h"
#include "ma/Translation.h"
// Pour éviter de longues compilations répétitives.
// Devra être inclus par les projets qui l'utilisent.
// #include "ma/Version.h"
#include "ma/VariableEnum.h"
#include "ma/Config.h"
#include "ma/Module.h"

// Engine
#include "ma/engine/Node.h"
#include "ma/engine/Scene.h"
#include "ma/engine/Camera.h"
#include "ma/engine/Light.h"
#include "ma/engine/Primitive.h"
#include "ma/engine/Context.h"

// wxWidgets
#ifndef M_NOGUI
    #include "ma/wx/Variable.h"
    #include "ma/wx/GuiAccess.h"
    #include "ma/wx/Parameter.h"
    #include "ma/wx/TreeItems.h"
    #include "ma/wx/PanelItem.h"
    #include "ma/wx/Colour.h"
    #include "ma/wx/AddItemDialog.h"
    #include "ma/wx/AddVarDialog.h"
    #include "ma/wx/PanelResourcesExplorer.h"
    #include "ma/wx/ResourcePreview.h"
    #include "ma/wx/Context.h"
    #include "ma/wx/View.h"
#endif // M_NOGUI

// À cause de X.h eigen doit être inclus en dernier.
// Maths
#include "ma/Colour.h"
#include "ma/eigen/Matrix.h"
#include "ma/eigen/Quaternion.h"
#include "ma/eigen/Transform.h"
#include "ma/eigen/Vector.h"
