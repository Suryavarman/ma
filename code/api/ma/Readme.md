[![License](https://img.shields.io/badge/license-MIT-green)](https://framagit.org/Suryavarman/ma/-/blob/master/LICENSE)
[![pipeline status](https://framagit.org/Suryavarman/ma/badges/master/pipeline.svg)](https://framagit.org/Suryavarman/ma/-/commits/master)
<div style="text-align: center;">
<h1>L'api C++ du projet :<br>Maçon de l'espace</h1>
</div>

<div style="text-align: center;"><a href="https://youtu.be/Bezg7UYlQkA?list=PLwg0gkr8hTmXBe0G7GkteWBA462lpuQYG
" target="_blank"><img src="https://img.youtube.com/vi/qBFl-uu-lHs/0.jpg"
 alt="Maçon de l'espace" width="480" height="360" border="10" /></a></div>


## Présentation

Maçon de l'espace alias [Ma](https://framagit.org/Suryavarman/ma) est un projet écrit en c++17 et python3.

L'objectif est de créer un éditeur d'applications 3D simple qui permet d'interfacer différents moteurs 3D tel que Ogre3D
et Pand3D.

La version du projet en est encore au stade d'alpha.

## Liens

Documentation de l'API : https://suryavarman.fr/stockage/ma/doc/

Git :  [https://framagit.org/Suryavarman/ma](https://framagit.org/Suryavarman/ma)

Extraits de code : https://framagit.org/Suryavarman/ma/-/snippets

## Feuille de route

### Premier cycle:

- [x] ~~Via une interface C++, créer une communication entre wxWidgets C++ et wxPython.~~ [pybind11](https://github.com/pybind/pybind11)
- [x] ~~Créer les scriptes nécessaires pour installer correctement les dépendances~~ ( [pybind11](https://github.com/pybind/pybind11), [wxWidgets3](http://wxwidgets.org/),  [wxPython4](https://github.com/wxWidgets/Phoenix), [Criterion](https://github.com/Snaipe/Criterion), ... ) ~~et l'environnement virtuel python.~~
- [x] ~~Créer une structure de données C++/Python.~~ [ma::Item](./include/ma/Item.h) / [ma::py::PyItem](./../../modules/ma/module_python/src/bind/Item.py.cpp)
- [ ] Mettre en place le gestionnaire de ressources.
- [ ] Mettre en place le système d'annuler et refaire.
- [ ] Mettre en place le système d'interface unique de rendu 3D avec [Ogre](https://www.ogre3d.org/), [Panda](https://www.panda3d.org/) et [Cycle](https://www.cycles-renderer.org/).
- [x] ~~Mettre en place un système de modules. Séparer du cœur~~ [module_ogre](https://www.ogre3d.org/), [module_panda](https://www.panda3d.org/), [module_cycles](https://www.cycles-renderer.org/), [module_python](/../../modules/ma/module_python/) ~~et peut-être~~ [wxWidgets3](http://wxwidgets.org/).
- [ ] Intégrer la démo [Sponza](https://www.intel.com/content/www/us/en/developer/topic-technology/graphics-research/samples.html) 
- [x] ~~Mettre en place la sauvegarde d'un projet Ma.~~
- [ ] Générer une application à partir d'un projet Ma.
- [ ] Mettre en place le Multi-langues [Application non anglaise](https://docs.wxwidgets.org/3.0/overview_nonenglish.html) / [wxConvAUto](https://docs.wxwidgets.org/3.0/classwx_conv_auto.html) [wxMBConv](https://docs.wxwidgets.org/3.0/classwx_m_b_conv.html) / [ISO/IEC 8859-15:1999](https://en.wikipedia.org/wiki/ISO/IEC_8859-15)
- [x] ~~Ne plus utiliser CppClay~~
- [x] ~~Compiler la solution avec Clang~~
- [ ] Installation pour Windows
- [ ] Installation pour MacOS
- [ ] Mettre en place l'écriture de scripts en C++. (avec [Cling](https://github.com/root-project/cling/) ? )
- [x] ~~Créer un CMakeLists.txt~~
- [x] ~~Implémenter la CI avec Jenkins et Gitlab~~

### Second cycle:

- [ ] Intégrer le rendu et la création d'interfaces graphique dans le rendu 3D : [imgui](https://github.com/ocornut/imgui), 
  [cegui](https://github.com/cegui/cegui), [SFGUI](https://github.com/TankOs/SFGUI)
- [ ] Séparer du noyau wxWidget
- [ ] Implémenter une interface graphique avec [GTK](https://www.gtk.org/docs/language-bindings/cpp/)
- [ ] Implémenter une interface graphique avec [QT5](https://www.qt.io/qt-5-12)
- [ ] Faire une belle interface avec l'éditeur basé sur wxWidgets.
- [ ] Compiler la solution avec [Intel](https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html#dpcpp-cpp) [CMake + Intel](https://www.intel.com/content/www/us/en/develop/documentation/oneapi-dpcpp-cpp-compiler-dev-guide-and-reference/top/compiler-setup/using-the-command-line/using-cmake-with-data-parallel-c-dpc.html)
- [ ] Ajouter une deuxième gestion des évènements « bind(item, fonction) », elle complètera et simplifiera celle de base.
- [ ] Multi-thread
- [ ] Intégration de l'édition des scripts avec PyCharm voir plus.
- [ ] Créer une version à installer et son installateur
- [ ] Créer une solution pour mettre à jour la version installée
- [ ] Gestion des performances
- [ ] Améliorer et tester les performances [AwesomePerfCpp](https://fenbf.github.io/AwesomePerfCpp/#libraries) [Celero](https://github.com/DigitalInBlue/Celero)
- [ ] Simplifier l'api python ex: root.m_Current.SetItem(item) deviendrait root.m_Current = item
- [ ] Faciliter la découverte, l'accès, la lisibilité, la structure de l'API.
- [ ] Ajouter la gestion des commentaires à la sérialisation.
- [ ] Obtenir le Badge de la [OpenSSF](https://bestpractices.coreinfrastructure.org/fr)
- [ ] Installation sur une OS BSD avec [Ghost BSD](https://www.ghostbsd.org/) ou [Nomad BSD](https://www.nomadbsd.org).
- [ ] Installation sur une OS [Gnu / Hurd](https://www.gnu.org/software/hurd/) avec [Debian Hurd](https://cdimage.debian.org/cdimage/ports/stable/hurd-i386/) ou [ArchHurd](https://archhurd.org/).
- [ ] Installation sur une OS [Haiku](https://www.haiku-os.org/).
- [ ] Générer un « core-dump » en cas de plantage de l'éditeur, des applications générées et générer des « .gdbinit ».
- [ ] Générer des logs avec un format et utiliser un videngeur de log. Ex: [g3log sinker](https://github.com/KjellKod/g3log) 

## Installation

Voir le fichier [Jenkinsfile](Jenkinsfile). 
Les dépendances nécessaires et le procéssus d'installation y sont décrits. 

## Quelques liens

- [c++11/14/17/20](https://fr.cppreference.com/w/cpp)

- [pybind11](https://github.com/pybind/pybind11)

- [wxWidgets 3](http://wxwidgets.org/)

- [python 3](https://www.python.org/)

- [wxPython 4](https://github.com/wxWidgets/Phoenix)

- [CodeBlock](http://www.codeblocks.org/)

- [Cling](https://rawcdn.githack.com/root-project/cling/d59d27ad61f2f3a78cd46e652cd9fb8adb893565/www/build.html)

## Les erreurs à l'installation des dépendances

### pip is configured with locations that require TLS/SSL, however the ssl module in Python is not available.

Essayer d'installer libssl ou openssll, sinon installer un environnement virtuel au lieu de recompiler python.

### Retrying (Retry(total=4, connect=None, read=None, redirect=None, status=None)) after connection broken by 'SSLError(SSLError(161, '[SSL: LIBRARY_HAS_NO_CIPHERS] library has no ciphers (_ssl.c:3014)'))': /simple/numpy/

Le système ne possède peut-être pas le service ssl d'activé. Vous pouvez essayer d'installer la librairie manquante et
le recompiler python ou de réienstaller l'environnement python mais cette fois ci au lieu de compiler python générer
un environnement virtuel de python.

### [Errno 2] No such file or directory: 'sip/cpp/_core.sbf'

Lors de la compilation le processus doit placer des fichiers dans le dossier sip. Si les sous dossiers de sip ne
contiennent que des fichiers «lisez-moi» alors sip n'a pas été généré. Peut être cela vient de la compilation de python,
où il aurait fallu le compiler avec l'option --enable-shared, si vous avez compiler python à l'aide du scipt
install_py_env.py l'option est activée. Le problème n'est donc pas lié à cette option de compliation de python.

### xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

    Install Xcode
    Run sudo xcode-select -s /Applications/Xcode.app/Contents/Developer

### error encoding endline :

Essayez d'ouvrir les documents en UTF-8.
CodeBolcks > Settings > Editor… > General Settings > Encoding settings:
Use encoding when opening files: choisir UTF-8, faite OK puis relancé CodeBlocks.

### MacOSX zsh: no such file or directory

http://forums.codeblocks.org/index.php/topic,24301.msg165774.html#msg165774

https://sourabhbajaj.com/mac-setup/iTerm/zsh.html
ou

```
sudo sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
chsh -s /bin/zsh
source ~/.zhrc
```

ou plus avancé
https://www.freecodecamp.org/news/how-to-configure-your-macos-terminal-with-zsh-like-a-pro-c0ab3f3c1156/

### MacOSX zsh: permission denied

Ne faite pas ça: https://support.apple.com/en-in/HT208050
(réinstaller zsh https://sourabhbajaj.com/mac-setup/iTerm/zsh.html)

### Mageia8 lib64harfbuzz-devel

```
/usr/include/pango-1.0/pango/pango-coverage.h:28:10: erreur fatale: hb.h : Aucun fichier ou dossier de ce type
   28 | #include <hb.h>
      |          ^~~~~~
```

https://www.mageialinux-online.org/forum/topic-28579+mageia8pango-includehb-h-not-found.php
Le chemin a inclure est sûrement:
`/usr/include/harfbuzz/`

### Plus d'informations:

[Mastodon](https://mamot.fr/@Suryavarman)

### Quelques liens sur les moteurs 3D :

[Ogre3D](https://www.ogre3d.org) (Un moteur très permissif graphiquement)

[Panda3D](https://www.panda3d.org/) (Moteur de Disney la VR y est bien implémentée (à vérifier))

[Cycle](https://projects.blender.org/blender/cycles) (Le moteur de rendu de Blender)

[Eevee](https://developer.blender.org/project/view/96/) (Le moteur 3D temps réel de Blender)

[raylib](https://www.raylib.com/) (API simple, épurée et concise)

[toy engine](https://github.com/hugoam/toy)(Un moteur minimaliste.)

[Openscenegraph](http://www.openscenegraph.org/) (Moteur qu'utilise la dernière version de [OpenMW](https://openmw.org/) )

[Wiked Engine Net](https://wickedengine.net/)

[Irrlicht](https://irrlicht.sourceforge.io/)

[Space Engine](https://spaceengine.org/) (pas open source [ou presque](https://sourceforge.net/projects/spengine/))

[VTK](https://gitlab.kitware.com/vtk/vtk) (Utilisé pour les images 3d médicales)

[G3DInnovation](https://casual-effects.com/g3d/www/index.html)

[Proland](https://proland.inrialpes.fr/) (visualisateur de planète de l'inria)

[Doom3](https://github.com/id-Software/DOOM-3), [Doom](https://github.com/id-Software/DOOM), [Quake III Arena](https://github.com/id-Software/Quake-III-Arena)

[OpenMW](https://openmw.org/) (Morrowind)

[Evergine](https://evergine.com/) ( anciennement WaveEngine )

[VRage2](https://github.com/KeenSoftwareHouse/SpaceEngineers) ( SpaceEngineers ) [VRage1](https://github.com/KeenSoftwareHouse/Miner-Wars-2081) (Miner Wars 2081)

[O3DE](https://www.o3de.org/)

[senthel](https://www.esenthel.com/)

[C4 Engine](http://c4engine.com/)

[Urho3D](https://github.com/urho3d/Urho3D)

[eadwerks](https://www.leadwerks.com/engine)

[Torque3D](https://github.com/GarageGames/Torque3D)

[Source Engine](https://github.com/ValveSoftware/source-sdk-2013) (2013)

[Lumix Engine](https://github.com/nem0/LumixEngine)

[PyVista](https://www.pyvista.org/)

[Yafaray](https://github.com/YafaRay/libYafaRay) (lancé de rayon [monté carlo](https://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M2PROIMA/2021/tp3.html))

[LuxCoreRender](https://github.com/LuxCoreRender/LuxCore) (Moteur de rendu physiquement correct)

[PovRay](http://www.povray.org/) (C'est vieux) ([Github](https://github.com/POV-Ray/povray/tree/3.7-stable))

[RenderMan](https://renderman.pixar.com/) (Pixar)

[MoonRay](https://github.com/dreamworksanimation/openmoonray) (DreamWorks)

[AppleSeed HQ](https://appleseedhq.net/)

[Torque 6](https://github.com/andr3wmac/Torque6)

## Quelques liens d'interface graphique pour la 3D
[imgui](https://github.com/ocornut/imgui)
[cegui](https://github.com/cegui/cegui)
[SFGUI](https://github.com/TankOs/SFGUI)
[TGUI](https://github.com/texus/TGUI)
[MyGUI](https://github.com/MyGUI/mygui)
[NanoGUI](https://github.com/mitsuba-renderer/nanogui)
[raygui](https://github.com/raysan5/raygui)

[Pour rechercher des GUI](https://www.libhunt.com/r/mygui)

## GUI
[wxWidgets](https://github.com/wxWidgets/wxWidgets)
[Qt](https://www.qt.io/qt-5-12)
[Nana](https://github.com/cnjinhao/nana)
[Nukelear](https://github.com/Immediate-Mode-UI/Nuklear)
[FLTK](https://github.com/fltk/fltk)
[Gnome Gtk](https://github.com/GNOME/gtk)
[KDE Baloo](https://github.com/KDE/baloo-widgets)
[lvgl](https://github.com/lvgl/lvgl)
[GUIlite.h](https://github.com/idea4good/GuiLite)



## Quelques liens utiles:
[godbolt: Tester sur plusieurs compilateurs](https://godbolt.org/)

[quick-bench: Tests de performances C++](https://quick-bench.com/q/Yr3oqgxa5l-ifcQ5pirymQcniKE)

### Tests de performances graphique:
[RenderDoc: UI + API: Python/C](https://github.com/baldurk/renderdoc)

[Intel: UI + API: Python/C++](https://www.intel.com/content/www/us/en/developer/tools/graphics-performance-analyzers/overview.html)






