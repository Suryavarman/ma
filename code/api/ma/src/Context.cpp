/// \file Context.cpp
/// \author Pontier Pierre
/// \date 2020-03-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Context.h"
#include "ma/ClassInfo.h"
#include "ma/ObservationManager.h"

const ma::Var::key_type ma::DataMaker::ms_Key_DataClassName{L"m_DataClassName"};
const ma::Var::key_type ma::DataMaker::ms_Key_ClientClassName{L"m_ClientClassName"};

namespace ma
{
    // ContextManager //————————————————————————————————————————————————————————————————————————————————————————————————
    ContextManager::ContextManager(const Item::ConstructorParameters &in_params):
    Item({in_params, Key::GetContextManager(), true, false})
    {}

    ContextManager::~ContextManager() = default;

    const TypeInfo &ContextManager::GetContextManagerTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetContextManagerClassName(),
            L"La classe «ContextManager» permet de gérer les différents contexte.",
            {
                {TypeInfo::Key::GetWhiteListChildrenData(),{ma::Context::GetContextClassName()}}
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ContextManager, Item)

    // ContextDataMaker //——————————————————————————————————————————————————————————————————————————————————————————————
    ContextDataMaker::ContextDataMaker(const Item::ConstructorParameters &in_params, const std::wstring &context_name):
    ma::Item(in_params),
    m_ContextName(AddReadOnlyMemberVar<StringVar>(L"m_ContextName", context_name))
    {}

    ContextDataMaker::~ContextDataMaker() = default;

    ma::ItemPtr ContextDataMaker::CreateChildFromSave(const std::filesystem::directory_entry &dir_entry)
    {
        const auto class_name = GetTypeFromSave(dir_entry);
        const auto key = GetKeyFromSave(dir_entry);

        typedef ma::MatchType<DataMaker> match_data_type;
        auto makers = ma::Find<match_data_type>::GetDeque(GetKey(), match_data_type(), ma::Item::SearchMod::LOCAL);

        typedef ma::MatchType<ContextLink> match_type;
        const auto links = ma::Find<match_type>::GetHashMap(GetParentKey(), {}, ma::Item::SearchMod::LOCAL);

        for(auto &maker : makers)
        {
            if(maker->GetTypeInfo().m_ClassName == class_name)
            {
                auto clone = maker->Clone(key);
                RemoveChild(maker);

                for(auto &&[link_key, link] : links)
                {
                    for(auto &&[data_key, data] : link->m_ClientDatas)
                    {
                        if(data)
                            data->MakerReplacement(maker, clone);
                    }
                }

                return clone;
            }
        }

        return Item::CreateChildFromSave(dir_entry);
    }

    const TypeInfo &ContextDataMaker::GetContextDataMakerTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetContextDataMakerClassName(),
            L"Les enfants de ContextDataMaker sont des DataMaker associé au type de contexte  représenté par la "
            L"variable « m_ContextName »."
            L"Le gestionnaire de contextes créé ses ContextDataMaker pour permettre aux contextes de savoir comment "
            L"générer un type de donnée.",
            {
                {TypeInfo::Key::GetWhiteListChildrenData(), {ma::DataMaker::GetDataMakerClassName()}},
                {TypeInfo::Key::GetWhiteListParentsData(), {ma::Context::GetContextClassName()}}
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ContextDataMaker, Item)

    // Context //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Context::Context(const Item::ConstructorParameters &in_params,
                     const std::wstring &name,
                     ma::Item::key_type data_maker_key):
    Item(in_params),
    m_DataMakersKey{std::move(data_maker_key)},
    m_DataMakers{},
    m_Name(AddReadOnlyMemberVar<ma::StringVar>(L"m_Name", name))
    {}

    Context::~Context() = default;

    void Context::EndConstruction()
    {
        Item::EndConstruction();
        const auto context_class_name = GetTypeInfo().m_ClassName;
        m_DataMakers = ma::Item::CreateItem<ContextDataMaker>(
            ma::Item::CreateParameters(GetKey(), true, false, m_DataMakersKey), context_class_name);
    }

    void Context::OnEnable()
    {
        Item::OnEnable();
    }

    void Context::OnDisable()
    {
        Item::OnDisable();

        //        typedef MatchType<ContextLink> match_context_link;
        //
        //        auto context_links = Find<match_context_link>::GetDeque(this,
        //                                                                match_context_link{},
        //                                                                ma::Item::SearchMod::LOCAL);
        //
        //        for(auto& context_link : context_links)
        //            RemoveChild(context_link);
    }

    ContextDataMakerPtr Context::GetContextDataMaker()
    {
        typedef ma::MatchType<ContextDataMaker> match_context_data_type;
        auto context_data_makers = ma::Find<match_context_data_type>::GetDeque(
            GetKey(), match_context_data_type(), ma::Item::SearchMod::LOCAL);

        const auto context_class_name = GetTypeInfo().m_ClassName;

        MA_ASSERT(context_data_makers.size() < 2u,
                  L"Il y plus d'un « ContextDataMaker » dans ce context: " + context_class_name,
                  std::logic_error);

        ma::ContextDataMakerPtr context_data_maker;

        if(context_data_makers.empty())
        {
            m_DataMakers = ma::Item::CreateItem<ContextDataMaker>(
                ma::Item::CreateParameters(GetKey(), true, false, m_DataMakersKey), context_class_name);
            context_data_maker = m_DataMakers;
        }
        else
            context_data_maker = context_data_makers[0];

        return context_data_maker;
    }

    ma::ItemPtr Context::CreateChildFromSave(const std::filesystem::directory_entry &dir_entry)
    {
        const auto class_name = GetTypeFromSave(dir_entry);

        if(class_name == ContextDataMaker::GetContextDataMakerClassName())
            return GetContextDataMaker();
        else
            return {};
    }

    std::deque<DataMakerPtr> Context::GetDataMakers() const
    {
        MA_ASSERT(m_DataMakers, L"m_DataMakers est nul.", std::logic_error);
        typedef MatchVar<StringVar, DataMaker> match_data_maker;

        match_data_maker matcher({DataMaker::ms_Key_ClientClassName});

        return Find<match_data_maker>::GetDeque(m_DataMakers->GetKey(), matcher, ma::Item::SearchMod::LOCAL);
    }

    std::optional<ma::ContextLinkPtr> Context::HasLink(ma::ItemPtr client) const
    {
        if(client)
        {
            typedef ma::MatchType<ContextLink> match_type;

            const auto links = ma::Find<match_type>::GetHashMap(GetKey(), {}, ma::Item::SearchMod::LOCAL);

            for(const auto &key_link : links)
            {
                const auto link = key_link.second;
                auto it_find = link->m_ClientDatas.find(client->GetKey());
                if(it_find != link->m_ClientDatas.end())
                    return link;
            }
        }

        return {};
    }

    std::optional<ma::DataPtr> Context::HasData(ma::ItemPtr client) const
    {
        if(client)
        {
            typedef ma::MatchType<ContextLink> match_type;

            const auto links = ma::Find<match_type>::GetHashMap(GetKey(), {}, ma::Item::SearchMod::LOCAL);

            for(const auto &key_link : links)
            {
                const auto link = key_link.second;
                auto it_find = link->m_ClientDatas.find(client->GetKey());
                if(it_find != link->m_ClientDatas.end())
                    return it_find->second;
            }
        }

        return {};
    }

    ma::DataPtr Context::BuildData(ma::ContextLinkPtr link, ma::DataPtr parent, ma::ItemPtr client)
    {
        ma::DataPtr result;

        if(link)
        {
            if(client)
            {
                try
                {
                    const auto key_parent = parent ? parent->GetKey() : link->GetKey();

                    auto data_makers = GetDataMakers();

                    for(auto &data_maker : data_makers)
                    {
                        if(ma::ClassInfoManager::IsInherit(client, data_maker->m_ClientClassName->Get()))
                        {
                            result = data_maker->CreateData(link, client, {key_parent});
                        }
                    }
                }
                catch(const std::exception &e)
                {
                    MA_ASSERT(false,
                              L"La création de l'item correspondant au client : \n"s + L"Clef: " + client->GetKey() +
                                  L" classe: " + client->GetItemTypeInfo().m_ClassName +
                                  L"\nn'a peu se faire à cause de l'erreur suivante:" + ma::to_wstring(e.what()),
                              std::invalid_argument);
                }
            }
        }

        return result;
    }

    const TypeInfo &Context::GetContextTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetContextClassName(),
            L"La classe «Context» permet de définir le milieu dans lequel une ressource sera utilisée.",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        ContextLink::GetContextLinkClassName(),
                        ContextDataMaker::GetContextDataMakerClassName()
                    }
                },
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        ma::ContextManager::GetContextManagerClassName()
                    }
                }
            }
        };
        // clang-format on
        return info;
    }
    M_CPP_CLASSHIERARCHY(Context, Item)
    M_CPP_ITEM_TYPE_VAR(Context)

    // ContextLink //———————————————————————————————————————————————————————————————————————————————————————————————————
    ContextLink::ContextLink(const Item::ConstructorParameters &in_params, ma::ItemPtr client):
    Item(in_params),
    Observer(),
    m_ObserverManager{},
    m_ClientDatas{},
    m_Client{AddMemberVar<ma::ItemVar>(GetClientVarKey(), client)},
    m_Name{AddReadOnlyMemberVar<ma::StringVar>(Item::Key::Var::GetName(), L"")}
    {}

    void ContextLink::EndConstruction()
    {
        ma::Item::EndConstruction();

        m_ObserverManager = ma::Item::GetItem<ma::ObservationManager>(Key::GetObservationManager());
        m_ObserverManager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_Client);
    }

    ContextLink::~ContextLink() = default;

    ItemPtr ContextLink::CreateChildFromSave(const std::filesystem::directory_entry &dir_entry)
    {
        ItemPtr result;
        auto context = GetContext();
        // std::filesystem::directory_entry context_link_dir{dir_entry.path().parent_path()};
        std::filesystem::directory_entry context_link_dir{m_SavePath->toString()};

        const auto vars = GetVarsFromSave(context_link_dir);
        if(auto it_find = vars.find(GetClientVarKey()); it_find != vars.end())
        {
            const Item::key_type client_key = std::get<1u>(it_find->second);
            if(auto opt = Item::HasItemOpt(client_key))
            {
                m_Client->SetItem(opt.value());
            }
            else
            {
                const auto dependencies_from_item = GetDependencies();
                const auto dependencies_from_save = GetDependenciesFromSave(context_link_dir);

                if(std::count(dependencies_from_item.begin(), dependencies_from_item.end(), client_key) > 0)
                {
                    MA_WARNING(
                        false,
                        L"L'item « " + client_key +
                            L" » n'appartient pas à la hiérarchie des éléments. Celui-ci est pourtant défini dans la "
                            L"liste des dépendances de ce lien. Le chargement des éléments aurait dû chargé les "
                            L"dépendances en-premier lieu. Peut être que cet élément n'a peut être chargé.");
                }
                else
                {
                    if(std::count(dependencies_from_save.begin(), dependencies_from_save.end(), client_key) > 0)
                    {
                        MA_WARNING(false,
                                   L"L'item « " + client_key +
                                       L" » n'appartient pas à la hiérarchie des éléments. Pourtant la sauvegarde du "
                                       L"lien défini bien cet item comme étant une dépendance.");
                    }
                    else
                    {
                        MA_WARNING(false,
                                   L"L'item « " + client_key +
                                       L" » n'appartient pas à la hiérarchie des éléments. Le fichier de sauvegarde "
                                       L"n'est peut être pas bien formé, car cet élément client aurait dû être défini "
                                       L"comme étant une dépendance de ce lien.");
                    }
                }
            }
        }
        else
        {
            MA_WARNING(false,
                       L"Il est impossible de trouver la variable « " + GetClientVarKey() + L" » dans le dossier « " +
                           context_link_dir.path().wstring() + L" ».");
        }

        if(auto client = m_Client->GetItem(); client)
        {
            // Permettra de d'observer le client.
            // Il faut d'abord s'assurer que le client soit chargé.
            // Exemple un dossier devra avoir son m_Path d'initialisé, et cela, malgré que les variables n'aient pas
            // encore été chargées.
            client->LoadVars();

            BeginObservation(m_Client);

            const auto client_key = client->GetKey();
            if(auto it_find = m_ClientDatas.find(client_key); it_find != m_ClientDatas.end())
                result = it_find->second;
            else
            {
                MA_WARNING(false,
                           L"L'observation du client « " + client_key + L" » de type « " +
                               client->GetTypeInfo().m_ClassName +
                               L" » n'a pas permis de créer la donnée qui le représente dans ce contexte « " +
                               GetContext()->GetKey() + L" »");
            }
        }

        return result;
    }

    void ContextLink::UpdateName()
    {
        std::wstring name{};

        if(auto client = m_Client->GetItem(); client)
        {
            name = client->GetTypeInfo().m_ClassName;

            if(auto opt = client->HasVarOpt(Item::Key::Var::GetName()))
                name = opt.value()->toString();
        }

        if(m_Name->Get() != name)
            SetVarFromValue<ma::StringVar>(Item::Key::Var::GetName(), name);
    }

    void ContextLink::UpdateDependencies()
    {
        const auto previous_key = m_Client->GetPreviousKey();
        const auto current_key = m_Client->toString();

        if(previous_key != current_key)
        {
            auto dependencies = GetDependencies();

            std::replace(dependencies.begin(), dependencies.end(), previous_key, current_key);

            if(std::find(dependencies.begin(), dependencies.end(), current_key) == dependencies.end())
                dependencies.push_back(current_key);

            SetDependencies(dependencies);
        }
    }

    const ma::Item::key_type &ContextLink::GetClientVarKey()
    {
        static const auto var_id{L"m_Client"s};
        return var_id;
    }

    ma::ContextPtr ContextLink::GetContext() const
    {
        return ma::Item::GetItem<ma::Context>(m_ParentKey);
    }

    void ContextLink::BeginObservation(const ObservablePtr &observable)
    {
        // Nous observons la variable m_Client.
        // Si celle-ci possède un item, nous commençons son observation.
        if(m_Client == observable)
        {
            auto &client = m_Client->GetItem();
            if(client)
            {
                m_ObserverManager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, client);
            }
        }
        else
        {
            const auto key = observable->GetKey();
            // Si l'observable n'est pas client
            if(m_ClientDatas.find(key) == m_ClientDatas.end())
            {
                auto client = ma::Item::GetItem(key);
                UpdateName();
                UpdateDependencies();

                auto context = GetContext();
                auto link = GetPtr<ContextLink>();

                auto it_find = m_ClientDatas.find(client->GetParentKey());

                ma::DataPtr parent_data = it_find != m_ClientDatas.end() ? it_find->second : nullptr;

                auto data = context->BuildData(link, parent_data, client);
                m_ClientDatas[key] = data;

                // Nous parcourons les enfants du client pour commencer leur observation.
                if(!client->Empty())
                {
                    for(const auto &child_client : client->GetItemsPostOrder(ma::Item::SearchMod::LOCAL))
                    {
                        m_ObserverManager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, child_client);
                    }
                }
            }
        }
    }

    void ContextLink::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        // On regarde si l'item pointé par la variable m_Client est le même.
        if(m_Client == observable)
        {
            ma::ItemPtr client = m_Client->GetItem();

            if(client)
            {
                auto it_find = m_ClientDatas.find(client->GetParentKey());

                // Il faut supprimer toutes les données et commencer l'observation de ce nouveau client.
                if(it_find == m_ClientDatas.end())
                {
                    if(!m_Client->GetPreviousKey().empty() && m_Client->GetPrevious() != client)
                        RemoveDatas();

                    m_ObserverManager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, client);
                }
            }
            else // Il faut supprimer toutes les données
            {
                RemoveDatas();
            }

            UpdateName();
            UpdateDependencies();
        }
        else
        {
            for(const Action &action : data.GetOrdered())
            {
                if(action.first != ma::Item::Key::Obs::ms_AddItemBegin &&
                   action.first != ma::Item::Key::Obs::ms_AddItemEnd)
                {
                    // regarder si c'est un ajout d'enfant, dont la construction est terminée.
                    if(action.first == Key::Obs::ms_ConstructionEnd)
                    {
                        MA_ASSERT(m_ClientDatas.count(action.second) == 0,
                                  L"m_ClientDatas possède déjà le client: " + action.second,
                                  std::logic_error);

                        m_ObserverManager->AddObservation<ma::ItemObserverVar, ma::VarVar>(
                            this, ma::Item::GetItem(action.second));
                    }
                    // regarder si c'est une suppression d'enfant
                    else if(action.first == Key::Obs::ms_RemoveItemEnd)
                    {
                        MA_ASSERT(m_ClientDatas.count(action.second) > 0,
                                  L"m_ClientDatas ne possède pas le client: " + action.second,
                                  std::logic_error);

                        m_ObserverManager->RemoveObservation(this, ma::Item::GetItem(action.second));
                    }
                }
            }
        }
    }

    void ContextLink::EndObservation(const ObservablePtr &observable)
    {
        typedef MatchType<ma::Data> match_data;

        // Quand le contexte est sorti de ms_Map cette fonction peut être appelée. Dans ce cas, il faut appeler GetDeque
        // avec « this » plutôt que « GetKey() ».
        auto datas = Find<match_data>::GetDeque(this, match_data(), ma::Item::SearchMod::LOCAL);

        for(auto &data : datas)
        {
            if(data->IsEnable())
            {
                data->OnDisable();
            }
        }
    }

    void ContextLink::RemoveDatas()
    {
        for(auto &&[key, child] : ma::Item::GetItems(ma::Item::SearchMod::LOCAL))
        {
            if(ma::ClassInfoManager::IsInherit(child, ma::Data::GetDataTypeInfo().m_ClassName))
            {
                auto data = std::dynamic_pointer_cast<ma::Data>(child);
                m_ObserverManager->RemoveObservation(this, data->m_Client->GetItem());
            }
        }

        MA_ASSERT(m_ClientDatas.empty(),
                  L"Après la suppression de toutes les données m_ClientDatas n'est pas vide.",
                  std::logic_error);
    }

    void ContextLink::OnEnable()
    {
        Observer::OnEnable();
        Item::OnEnable();
    }

    void ContextLink::OnDisable()
    {
        Observer::OnDisable();
        Item::OnDisable();
    }

    const TypeInfo &ContextLink::GetContextLinkTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetContextLinkClassName(),
            L"La classe «ContextLink» est le lien entre un item à rendre et un contexte.",
            {
                {TypeInfo::Key::GetWhiteListChildrenData(), {ma::Data::GetDataClassName()}},
                {TypeInfo::Key::GetWhiteListParentsData(), {ma::Context::GetContextClassName()}}
            }
        };
        // clang-format on
        return info;
    }
    M_CPP_CLASSHIERARCHY(ContextLink, Item, Observer)
    M_CPP_ITEM_TYPE_VAR(ContextLink)
    M_CPP_MAKERITEM_TYPE(ContextLink)

    // Data //——————————————————————————————————————————————————————————————————————————————————————————————————————————
    Data::Data(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, const ma::ItemPtr &client):
    Item(in_params),
    m_KeyLink(link->GetKey()),
    m_IsLoad(AddReadOnlyMemberVar<ma::BoolVar>(GetIsLoadVarId(), false)),
    m_State(AddReadOnlyMemberVar<ma::StringVar>(GetSateVarId(), L"Création de la classe Data"s)),
    m_Link(AddReadOnlyMemberVar<ma::ContextLinkVar>(GetLinkVarId(), link)),
    m_Client(AddReadOnlyMemberVar<ma::ItemVar>(GetClientVarId(), client))
    {}

    Data::~Data() = default;

    const ma::Item::key_type &Data::GetIsLoadVarId()
    {
        static const auto var_id{L"m_IsLoad"s};
        return var_id;
    }

    const ma::Item::key_type &Data::GetSateVarId()
    {
        static const auto var_id{L"m_State"s};
        return var_id;
    }

    const ma::Item::key_type &Data::GetLinkVarId()
    {
        static const auto var_id{L"m_Link"s};
        return var_id;
    }

    const ma::Item::key_type &Data::GetClientVarId()
    {
        static const auto var_id{L"m_Client"s};
        return var_id;
    }

    bool Data::Load()
    {
        auto context_link = GetItem<ma::ContextLink>(m_KeyLink);
        SetVarFromValue<ma::ContextLinkVar>(GetLinkVarId(), context_link);
        return true;
    }

    bool Data::UnLoad()
    {
        SetVarFromValue<ma::ContextLinkVar>(GetLinkVarId(), ma::ContextLinkPtr{});
        return true;
    }

    bool Data::Reload()
    {
        UnLoad();
        Load();
        return true;
    }

    void Data::OnEnable()
    {
        Item::OnEnable();
        SetVarFromValue<ma::BoolVar>(m_IsLoad->GetKey(), Load());
    }

    void Data::OnDisable()
    {
        SetVarFromValue<ma::BoolVar>(m_IsLoad->GetKey(), !UnLoad());
        Item::OnDisable();
    }

    void Data::MakerReplacement(std::shared_ptr<DataMaker>, std::shared_ptr<DataMaker>)
    {}

    const TypeInfo &Data::GetDataTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetDataClassName(),
            L"La classe «Data» est la représentation d'un item dans un contexte donné.",
            {}
        };
        // clang-format on
        return info;
    }
    M_CPP_CLASSHIERARCHY(Data, Item)
    M_CPP_ITEM_TYPE_VAR(Data)

    // DataMaker //—————————————————————————————————————————————————————————————————————————————————————————————————————
    DataMaker::DataMaker(const Item::ConstructorParameters &in_params,
                         const std::wstring &data_class_name,
                         const std::wstring &client_class_name):
    Item(in_params),
    m_DataClassName(AddReadOnlyMemberVar<ma::StringVar>(ms_Key_DataClassName, data_class_name)),
    m_ClientClassName(AddReadOnlyMemberVar<ma::StringVar>(ms_Key_ClientClassName, client_class_name))
    {}

    DataMaker::~DataMaker() = default;

    const TypeInfo &DataMaker::GetDataMakerTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetDataMakerClassName(),
            L"La classe «DataMaker» est la classe de abstraite permettant de générer une classe Data à partir du non de"
            L" celle-ci.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(DataMaker, Item)

} // namespace ma
