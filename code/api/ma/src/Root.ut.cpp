/// \file Root.ut.cpp
/// \author Pontier Pierre
/// \date 2019-12-06
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires de la classe Root.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

// M_NOGUI est défini donc la racine n'a pas de définie l'item wx.
static const size_t count_root_children = 7u;
// static const size_t count_root_member_vars = 3u;

Test(Root, root_refcounting)
{
    ma::g_ConsoleVerbose = 1u;

    ma::Id::ClearKeys();
    cr_assert_eq(ma::Item::GetCount(), 0u);

    cr_assert_none_throw(ma::Item::CreateRoot<ma::Root>(L""));

//    for(auto it : ma::root()->GetItems(ma::Item::SearchMod::LOCAL))
//    {
//        M_MSG(it.second->GetItemClassName())
//    }

    // Les nœuds root et rtti.
    cr_assert_eq(ma::root()->GetCount(ma::Item::SearchMod::LOCAL), count_root_children);

    cr_assert_none_throw(ma::Item::EraseRoot());

    cr_assert_eq(ma::Item::GetCount(), 0u);

    ma::Id::ClearKeys();
}

Test(Root, root)
{
    ma::g_ConsoleVerbose = 1u;

    ma::Id::ClearKeys();
    cr_assert_eq(ma::Item::GetCount(), 0u);

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(ma::Item::GetRoot<ma::Item>(), std::logic_error);
    cr_assert_throw(ma::Item::GetRoot<ma::Root>(), std::logic_error);
    ma::g_ConsoleVerbose = 1u;
    {
        ma::RootPtr root;
        cr_assert_none_throw(root = ma::Item::CreateRoot<ma::Root>(L""));
        ma::g_ConsoleVerbose = 0;
        cr_assert_any_throw(ma::Item::CreateRoot<ma::Item>());
        ma::g_ConsoleVerbose = 1u;
        cr_assert_eq(root->GetKey(), ma::Item::Key::GetRoot());
        cr_assert_eq(ma::root()->GetCount(ma::Item::SearchMod::LOCAL), count_root_children);
        cr_assert_eq(ma::Item::GetItems()[ma::Item::Key::GetRoot()], root);
    }
    cr_assert_eq(ma::root()->GetCount(ma::Item::SearchMod::LOCAL), count_root_children);
    cr_assert_neq(ma::Item::GetItems()[ma::Item::Key::GetRoot()], nullptr);

    ma::g_ConsoleVerbose = 0;
    cr_assert_any_throw(ma::Item::EraseItem(ma::Item::Key::GetRoot()));
    ma::g_ConsoleVerbose = 1u;

    cr_assert_none_throw(ma::Item::EraseRoot());

    cr_assert_eq(ma::Item::GetCount(), 0u);

    ma::Id::ClearKeys();
}
