/// \file wx/PanelResourcesExplorer.cpp
/// \author Pontier Pierre
/// \date 2020-04-04
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/Error.h"
#include "ma/Item.h"
#include "ma/Root.h"
#include "ma/wx/PanelResourcesExplorer.h"
#include "ma/wx/Variable.h"
#include "ma/ObservationManager.h"
#include "ma/wx/Utility.h"
#include <wx/mimetype.h>
#include <wx/filename.h>
#include <wx/dnd.h>
#include <cmath>
#include <sstream>
// #include <filesystem> // c++17
//  namespace fs = std::filesystem; // c++17

// #include <experimental/filesystem>
//  namespace fs = std::experimental::filesystem;

// https://docs.wxwidgets.org/trunk/classwx_file_type_info.html
// https://docs.wxwidgets.org/trunk/classwx_mime_types_manager.html

namespace ma::wx
{
    const int PanelResourceItem::ms_ID_CopyKey = wxNewId();
    const int PanelResourceItem::ms_ID_CopyPath = wxNewId();
    const int PanelResourceItem::ms_ID_OpenFileExplorer = wxNewId();
    const int PanelResourceItem::ms_ID_OpenFileEditor = wxNewId();

    PanelResourceItem::PanelResourceItem(const ma::ResourcePtr &resource,
                                         PanelResourcesExplorerParameterPtr parameter,
                                         wxWindow *parent,
                                         wxWindowID id,
                                         const wxPoint &pos,
                                         const wxSize &size,
                                         long style,
                                         const wxString &name):
    wxPanel(parent, id, pos, size, style, name),
    ma::Observer(),
    m_FileBitmap(nullptr),
    m_Resource(resource),
    m_ObservedItem(parameter ? parameter->m_ObservedItem : nullptr),
    m_Parameter(parameter),
    m_Hover(false),
    m_DragBegin(false)
    {
        MA_ASSERT(m_Resource, L"m_Resource est nulle.", std::invalid_argument);

        MA_ASSERT(m_Parameter, L"m_Parameter est nulle.", std::invalid_argument);

        MA_ASSERT(m_ObservedItem, L"m_Parameter est nulle.", std::invalid_argument);

        wxBoxSizer *item_sizer;
        item_sizer = new wxBoxSizer(wxVERTICAL);

        m_FileBitmap = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0);

        wxString extension = wxT(".") + m_Resource->m_Path->GetExt();
        wxIconLocation *icon_location = nullptr;
        auto file_type = wxTheMimeTypesManager->GetFileTypeFromExtension(extension);

        if(icon_location && file_type && file_type->GetIcon(icon_location))
        {
            wxBitmap bitmap;
            bitmap.CopyFromIcon(wxIcon(*icon_location));
            m_FileBitmap->SetBitmap(bitmap);
        }
        else
        {
            m_FileBitmap->SetBitmap(wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_MESSAGE_BOX));
        }

        std::wstringstream tool_tip;

        tool_tip << L"Nom: " << m_Resource->m_Name->Get() << std::endl;
        tool_tip << L"Clef: " << m_Resource->GetKey() << std::endl;
        tool_tip << L"wxWindow ID: " << this->GetId() << std::endl;

        m_FileBitmap->SetToolTip(tool_tip.str());

        // item_sizer->Add(m_FileBitmap, 1, wxALL | wxEXPAND | wxALIGN_CENTER_HORIZONTAL, 5);
        item_sizer->Add(m_FileBitmap, 1, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

        m_FileNameStaticText =
            new wxStaticText(this, wxID_ANY, m_Resource->m_Name->Get(), wxDefaultPosition, wxDefaultSize, 0);
        m_FileNameStaticText->Wrap(-1);
        m_FileNameStaticText->SetToolTip(tool_tip.str());
        item_sizer->Add(m_FileNameStaticText, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

        this->SetSizer(item_sizer);
        this->Layout();

        m_PopupMenu.Append(ms_ID_CopyKey,
                           M_Tr(L"Copier la clef"),
                           M_Tr(L"Copie dans le presse papier la clef de l'item de type ressource."));

        m_PopupMenu.Append(ms_ID_CopyPath,
                           M_Tr(L"Copier le chemin"),
                           M_Tr(L"Copie dans le presse papier l'adresse du chemin de la ressource."));

        m_PopupMenu.Append(
            ms_ID_OpenFileExplorer,
            M_Tr(L"Ouvrir dans l'explorateur"),
            M_Tr(L"Ouvre l'explorateur de fichier par défaut du système d'exploitation à l'adresse de la "
                 L"ressource."));

        m_PopupMenu.Append(ms_ID_OpenFileEditor,
                           M_Tr(L"Ouvrir avec un éditeur externe"),
                           M_Tr(L"Ouvre la ressource dans l'éditeur externe qui lui est associé."));

        m_FileBitmap->Bind(wxEVT_LEFT_DOWN, &PanelResourceItem::OnLeftDown, this);
        m_FileBitmap->Bind(wxEVT_LEFT_UP, &PanelResourceItem::OnLeftUp, this);
        m_FileBitmap->Bind(wxEVT_LEFT_DCLICK, &PanelResourceItem::OnLeftDClick, this);
        m_FileBitmap->Bind(wxEVT_RIGHT_UP, &PanelResourceItem::OnRightClick, this);
        m_FileBitmap->Bind(wxEVT_MOTION, &PanelResourceItem::OnMouseMove, this);

        Bind(wxEVT_ENTER_WINDOW, &PanelResourceItem::OnEnterWindow, this);
        Bind(wxEVT_LEAVE_WINDOW, &PanelResourceItem::OnLeaveWindow, this);

        Bind(wxEVT_MENU, &PanelResourceItem::OnCopyKey, this, ms_ID_CopyKey);
        Bind(wxEVT_MENU, &PanelResourceItem::OnCopyPath, this, ms_ID_CopyPath);
        Bind(wxEVT_MENU, &PanelResourceItem::OnOpenFileExplorer, this, ms_ID_OpenFileExplorer);
        Bind(wxEVT_MENU, &PanelResourceItem::OnOpenFileEditor, this, ms_ID_OpenFileEditor);

        auto obs = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());

        obs->AddObservation<PanelResourceItemVar, ma::ItemVar>(this, m_Resource);
        obs->AddObservation<PanelResourceItemVar, ma::VarVar>(this, ma::root()->m_Current);

        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsBackgroundColour);
        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsForegroundColour);
        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsSelectedBackgroundColour);
        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsSelectedForegroundColour);
    }

    PanelResourceItem::~PanelResourceItem() = default;

    void PanelResourceItem::OnCopyKey(wxCommandEvent &event)
    {
        ma::root()->m_wxGuiAccess->SendTextToClipboard(m_Resource->GetKey());
    }

    void PanelResourceItem::OnCopyPath(wxCommandEvent &event)
    {
        ma::root()->m_wxGuiAccess->SendTextToClipboard(m_Resource->m_Path->toString());
    }

    void PanelResourceItem::OnOpenFileExplorer(wxCommandEvent &event)
    {
        auto folder = ma::Item::GetItem<ma::Folder>(m_Resource->GetParentKey());

        MA_ASSERT(folder, L"La ressource a pour parent un item qui n'est pas du type «ma::Folder».", std::logic_error);

        wxLaunchDefaultApplication(folder->m_Path->toString());
    }

    void PanelResourceItem::OnOpenFileEditor(wxCommandEvent &event)
    {
        wxLaunchDefaultApplication(m_Resource->m_Path->toString());
    }

    void PanelResourceItem::OnLeftDown(wxMouseEvent &event)
    {
        m_DragBegin = true;
    }

    void PanelResourceItem::OnMouseMove(wxMouseEvent &event)
    {
        if(m_DragBegin)
        {
            // http://www.wxdev.fr/snipp30-D&D_Drag_and_Drop_Text.html
            wxTextDataObject data_object(m_Resource->GetKey());

            wxDropSource drag_source(m_FileBitmap);
            drag_source.SetData(data_object);

            drag_source.DoDragDrop(true);
            m_DragBegin = false;
        }
    }

    void PanelResourceItem::OnLeftUp(wxMouseEvent &event)
    {
        if(wxGetKeyState(WXK_CONTROL))
            ma::root()->m_Current->SetItem(m_Resource);

        m_DragBegin = false;
        event.Skip();
    }

    void PanelResourceItem::OnLeftDClick(wxMouseEvent &event)
    {
        ma::root()->m_Current->SetItem(m_Resource);
        event.Skip();
    }

    void PanelResourceItem::OnRightClick(wxMouseEvent &event)
    {
        PopupMenu(&m_PopupMenu, event.GetPosition());
        event.Skip();
    }

    void PanelResourceItem::OnEnterWindow(wxMouseEvent &event)
    {
        m_Hover = true;
        UpdateSelectionRendering();

        event.Skip();
    }

    void PanelResourceItem::OnLeaveWindow(wxMouseEvent &event)
    {
        m_Hover = false;
        UpdateSelectionRendering();

        event.Skip();
    }

    void PanelResourceItem::BeginObservation(const ma::ObservablePtr &observable)
    {
        auto root = ma::root();
        if(observable == root->m_Current || observable == m_Parameter->m_ItemsBackgroundColour ||
           observable == m_Parameter->m_ItemsForegroundColour ||
           observable == m_Parameter->m_ItemsSelectedBackgroundColour ||
           observable == m_Parameter->m_ItemsSelectedForegroundColour)
        {
            UpdateSelectionRendering();
        }
    }

    void PanelResourceItem::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(data.HasActions(ma::Var::Key::Obs::ms_EndSetValue))
        {
            auto root = ma::root();
            if(observable == root->m_Current || observable == m_Parameter->m_ItemsBackgroundColour ||
               observable == m_Parameter->m_ItemsForegroundColour ||
               observable == m_Parameter->m_ItemsSelectedBackgroundColour ||
               observable == m_Parameter->m_ItemsSelectedForegroundColour)
            {
                UpdateSelectionRendering();
            }
        }
    }

    void PanelResourceItem::EndObservation(const ma::ObservablePtr &observable)
    {}

    void PanelResourceItem::UpdateSelectionRendering()
    {
        auto root = ma::root();
        if(root->m_Current->GetItem() == m_Resource)
        {
            SetBackgroundColour(m_Parameter->m_ItemsSelectedBackgroundColour->Get());
            SetForegroundColour(m_Parameter->m_ItemsSelectedForegroundColour->Get());
        }
        else
        {
            if(m_Hover)
            {
                SetBackgroundColour(m_Parameter->m_ItemsHoverBackgroundColour->Get());
                SetForegroundColour(m_Parameter->m_ItemsHoverForegroundColour->Get());
            }
            else
            {
                SetBackgroundColour(m_Parameter->m_ItemsBackgroundColour->Get());
                SetForegroundColour(m_Parameter->m_ItemsForegroundColour->Get());
            }
        }
    }

    const ma::TypeInfo &PanelResourceItem::GetwxPanelResourceItemTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxPanelResourceItemClassName(),
            L"Représentation d'un fichier pour l'explorateur.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(PanelResourceItem, wx, Observer)

    // PanelFolderItem //———————————————————————————————————————————————————————————————————————————————————————————————
    const int PanelFolderItem::ms_ID_CopyKey = wxNewId();
    const int PanelFolderItem::ms_ID_CopyPath = wxNewId();
    const int PanelFolderItem::ms_ID_OpenFileExplorer = wxNewId();

    PanelFolderItem::PanelFolderItem(const ma::FolderPtr &folder,
                                     PanelResourcesExplorerParameterPtr parameter,
                                     wxWindow *parent,
                                     wxWindowID id,
                                     const wxPoint &pos,
                                     const wxSize &size,
                                     long style,
                                     const wxString &name):
    wxPanel(parent, id, pos, size, style, name),
    ma::Observer(),
    m_FileBitmap(nullptr),
    m_Folder(folder),
    m_PopupMenu(),
    m_ObservedItem(parameter ? parameter->m_ObservedItem : nullptr),
    m_Parameter(parameter),
    m_Hover(false)
    {
        MA_ASSERT(m_Folder, L"« m_Folder » est nul.", std::invalid_argument);

        MA_ASSERT(m_Parameter, L"« m_Parameter » est nul.", std::invalid_argument);

        MA_ASSERT(m_ObservedItem, L"« m_Parameter » est nul.", std::invalid_argument);

        wxBoxSizer *item_sizer;
        item_sizer = new wxBoxSizer(wxVERTICAL);

        m_FileBitmap = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0);

        m_FileBitmap->SetBitmap(wxArtProvider::GetBitmap(wxART_FOLDER, wxART_MESSAGE_BOX));
        std::wstring tool_tip = m_Folder->m_Name->Get() + L"\n" + m_Folder->GetKey();
        m_FileBitmap->SetToolTip(tool_tip);

        // item_sizer->Add(m_FileBitmap, 1, wxALL | wxEXPAND | wxALIGN_CENTER_HORIZONTAL, 5);
        item_sizer->Add(m_FileBitmap, 1, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

        m_FileNameStaticText =
            new wxStaticText(this, wxID_ANY, m_Folder->m_Name->Get(), wxDefaultPosition, wxDefaultSize, 0);
        m_FileNameStaticText->Wrap(-1);
        m_FileNameStaticText->SetToolTip(tool_tip);
        item_sizer->Add(m_FileNameStaticText, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

        this->SetSizer(item_sizer);
        this->Layout();

        m_PopupMenu.Append(ms_ID_CopyKey,
                           M_Tr(L"Copier la clef"),
                           M_Tr(L"Copie dans le presse papier la clef de l'item de type"
                                L" dossier"));

        m_PopupMenu.Append(ms_ID_CopyPath,
                           M_Tr(L"Copier le chemin"),
                           M_Tr(L"Copie dans le presse papier l'adresse du chemin du dossier."));

        m_PopupMenu.Append(
            ms_ID_OpenFileExplorer,
            M_Tr(L"Ouvrir dans l'explorateur"),
            M_Tr(L"Ouvre l'explorateur de fichier par défaut du système d'exploitation à l'adresse du dossier."));

        m_FileBitmap->Bind(wxEVT_LEFT_UP, &PanelFolderItem::OnLeftClick, this);
        m_FileBitmap->Bind(wxEVT_RIGHT_UP, &PanelFolderItem::OnRightClick, this);

        Bind(wxEVT_ENTER_WINDOW, &PanelFolderItem::OnEnterWindow, this);
        Bind(wxEVT_LEAVE_WINDOW, &PanelFolderItem::OnLeaveWindow, this);

        Bind(wxEVT_MENU, &PanelFolderItem::OnCopyKey, this, ms_ID_CopyKey);
        Bind(wxEVT_MENU, &PanelFolderItem::OnCopyPath, this, ms_ID_CopyPath);
        Bind(wxEVT_MENU, &PanelFolderItem::OnOpenFileExplorer, this, ms_ID_OpenFileExplorer);

        auto obs = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());

        obs->AddObservation<PanelFolderItemVar, ma::ItemVar>(this, m_Folder);
        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsBackgroundColour);
        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsForegroundColour);
        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsSelectedBackgroundColour);
        obs->AddObservation<PanelFolderItemVar, ma::VarVar>(this, m_Parameter->m_ItemsSelectedForegroundColour);
    }

    PanelFolderItem::~PanelFolderItem() = default;

    void PanelFolderItem::BeginObservation(const ma::ObservablePtr &observable)
    {
        if(observable == m_Parameter->m_ItemsBackgroundColour || observable == m_Parameter->m_ItemsForegroundColour ||
           observable == m_Parameter->m_ItemsSelectedBackgroundColour ||
           observable == m_Parameter->m_ItemsSelectedForegroundColour)
        {
            UpdateSelectionRendering();
        }
    }

    void PanelFolderItem::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(data.HasActions(ma::Var::Key::Obs::ms_EndSetValue))
        {
            if(observable == m_Parameter->m_ItemsBackgroundColour ||
               observable == m_Parameter->m_ItemsForegroundColour ||
               observable == m_Parameter->m_ItemsSelectedBackgroundColour ||
               observable == m_Parameter->m_ItemsSelectedForegroundColour)
            {
                UpdateSelectionRendering();
            }
        }
    }

    void PanelFolderItem::EndObservation(const ma::ObservablePtr &observable)
    {}

    void PanelFolderItem::OnCopyKey(wxCommandEvent &event)
    {
        ma::root()->m_wxGuiAccess->SendTextToClipboard(m_Folder->GetKey());
        event.Skip();
    }

    void PanelFolderItem::OnCopyPath(wxCommandEvent &event)
    {
        ma::root()->m_wxGuiAccess->SendTextToClipboard(m_Folder->m_Path->toString());
        event.Skip();
    }

    void PanelFolderItem::OnOpenFileExplorer(wxCommandEvent &event)
    {
        wxLaunchDefaultApplication(m_Folder->m_Path->toString());
        event.Skip();
    }

    void PanelFolderItem::OnLeftClick(wxMouseEvent &event)
    {
        if(wxGetKeyState(WXK_CONTROL))
            ma::root()->m_Current->SetItem(m_Folder);
        else
            m_ObservedItem->SetItem(m_Folder);
    }

    void PanelFolderItem::OnRightClick(wxMouseEvent &event)
    {
        PopupMenu(&m_PopupMenu, event.GetPosition());
    }

    void PanelFolderItem::OnEnterWindow(wxMouseEvent &event)
    {
        m_Hover = true;
        UpdateSelectionRendering();

        event.Skip();
    }

    void PanelFolderItem::OnLeaveWindow(wxMouseEvent &event)
    {
        m_Hover = false;
        UpdateSelectionRendering();

        event.Skip();
    }

    void PanelFolderItem::OnBeginDrag(wxMouseEvent &event)
    {}

    void PanelFolderItem::UpdateSelectionRendering()
    {
        auto root = ma::root();
        if(root->m_Current->GetItem() == m_Folder)
        {
            SetBackgroundColour(m_Parameter->m_ItemsSelectedBackgroundColour->Get());
            SetForegroundColour(m_Parameter->m_ItemsSelectedForegroundColour->Get());
        }
        else
        {
            if(m_Hover)
            {
                SetBackgroundColour(m_Parameter->m_ItemsHoverBackgroundColour->Get());
                SetForegroundColour(m_Parameter->m_ItemsHoverForegroundColour->Get());
            }
            else
            {
                SetBackgroundColour(m_Parameter->m_ItemsBackgroundColour->Get());
                SetForegroundColour(m_Parameter->m_ItemsForegroundColour->Get());
            }
        }
    }

    const ma::TypeInfo &PanelFolderItem::GetwxPanelFolderItemTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxPanelFolderItemClassName(),
            L"Représentation d'un dossier pour l'explorateur.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(PanelFolderItem, wx, Observer)

    // PanelResourcesExplorer //————————————————————————————————————————————————————————————————————————————————————————
    wxIMPLEMENT_DYNAMIC_CLASS(PanelResourcesExplorer, wxPanel);

    PanelResourcesExplorer::PanelResourcesExplorer():
    wxPanel(),
    m_EndObservationBegin(false),
    m_SetCurrentItem(false),
    m_NoEndObservation(false),
    m_DirButtons(),
    m_DirSeparators(),
    m_ScrolledWindow(nullptr),
    m_GridSizer(nullptr),
    m_HeaderHorizontalSizer(nullptr),
    m_PanelsItems(),
    m_CurrentItemVar(ma::root()->m_Current),
    m_Parameters(nullptr)
    {}

    PanelResourcesExplorer::PanelResourcesExplorer(wxWindow *parent,
                                                   const wxString &name,
                                                   wxWindowID id,
                                                   const wxPoint &pos,
                                                   const wxSize &size,
                                                   long style):
    wxPanel(parent, id, pos, size, style, name),
    m_EndObservationBegin(false),
    m_SetCurrentItem(false),
    m_NoEndObservation(false),
    m_DirButtons(),
    m_DirSeparators(),
    m_ScrolledWindow(nullptr),
    m_GridSizer(nullptr),
    m_HeaderHorizontalSizer(nullptr),
    m_PanelsItems(),
    m_CurrentItemVar(ma::root()->m_Current),
    m_Parameters(nullptr)
    {
        wxBoxSizer *vertical_sizer;
        vertical_sizer = new wxBoxSizer(wxVERTICAL);

        m_HeaderHorizontalSizer = new wxBoxSizer(wxHORIZONTAL);
        vertical_sizer->Add(m_HeaderHorizontalSizer, 0, wxEXPAND, 5);

        m_ScrolledWindow =
            new wxScrolledWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL | wxVSCROLL);
        m_ScrolledWindow->SetScrollRate(5, 5);

        m_GridSizer = new wxGridSizer(6);
        // m_GridSizer = new wxFlexGridSizer(6);
        m_ScrolledWindow->SetSizer(m_GridSizer);
        m_ScrolledWindow->Layout();
        ResizeGrid();

        vertical_sizer->Add(m_ScrolledWindow, 1, wxEXPAND | wxALL, 5);

        this->SetSizer(vertical_sizer);
        this->Layout();

        this->Connect(wxEVT_SIZE, wxSizeEventHandler(PanelResourcesExplorer::OnSize));
        auto root = ma::root();

        if(auto opt = ma::Item::HasItemOpt<ma::wx::PanelResourcesExplorerParameter>(
               ma::wx::PanelResourcesExplorerParameter::Key::Get()))
            m_Parameters = opt.value();
        else
            m_Parameters =
                ma::Item::CreateItem<ma::wx::PanelResourcesExplorerParameter>({ma::Item::Key::GetwxParameterManager()});

        MA_ASSERT(ma::Item::HasItem(m_Parameters->GetKey()),
                  L"TEST: Erreur l'item n'est pas dans la hiérarchie des items.",
                  std::logic_error);

        auto obs_manager = root->m_ObservationManager;
        obs_manager->AddObservation<PanelResourcesExplorerVar, ma::VarVar>(this, m_CurrentItemVar);
        obs_manager->AddObservation<PanelResourcesExplorerVar, ma::VarVar>(this, m_Parameters->m_ObservedItem);
    }

    PanelResourcesExplorer::~PanelResourcesExplorer()
    {
        this->Disconnect(wxEVT_SIZE, wxSizeEventHandler(PanelResourcesExplorer::OnSize));
    }

    void PanelResourcesExplorer::OnSize(wxSizeEvent &event)
    {
        ResizeGrid();
        event.Skip();
    }

    void PanelResourcesExplorer::ResizeGrid()
    {
        MA_ASSERT(m_GridSizer, L"m_GridSizer est nulle.", std::logic_error);

        const float window_size = static_cast<float>(GetSize().GetWidth());

        // Largeur minimale.
        const float panel_width = m_Parameters ? static_cast<float>(m_Parameters->m_PanelItemWidth->Get()) : 160.0f;

        // Hauteur minimale.
        const float panel_height = m_Parameters ? static_cast<float>(m_Parameters->m_PanelItemHeight->Get()) : 160.0f;

        const int count_width_panel = std::ceil(window_size / panel_width);

        if(m_GridSizer->GetCols() != count_width_panel)
        {
            m_GridSizer->SetCols(count_width_panel);
            m_ScrolledWindow->Layout();

            for(auto it : m_PanelsItems)
            {
                it.second->SetMinSize(wxSize(static_cast<int>(panel_width), static_cast<int>(panel_height)));
            }

            // It is recommended to override RepositionChildren() in the derived
            // classes, but if they don't do it, this method must be overridden.
            // m_GridSizer->RecalcSizes();
            m_GridSizer->RepositionChildren(m_GridSizer->CalcMin());
        }
    }

    void PanelResourcesExplorer::BeginObservation(const ma::ObservablePtr &observable)
    {
        if(observable == m_CurrentItemVar)
        {
            auto item = m_CurrentItemVar->GetItem();

            if(item && item->IsObservable() && ma::Item::HasItem(item->GetKey()))
                UpdateObservedItem(m_CurrentItemVar);

            BuildGrid();
        }
    }

    void PanelResourcesExplorer::UpdateObservation(const ma::ObservablePtr &observable,
                                                   const ma::Observable::Data &data)
    {
        auto root = ma::root();

        for(const auto &action : data.GetOrdered())
        {
            if(observable == m_CurrentItemVar)
            {
                if(action.first == ma::Var::Key::Obs::ms_EndSetValue)
                {
                    auto item = m_CurrentItemVar->GetItem();
                    if(item)
                    {
                        auto item_key = item->GetKey();

                        if(!root->m_ObservationManager->HasObservation(this, item) && item->IsObservable() &&
                           ma::Item::HasItem(item_key))
                            UpdateObservedItem(m_CurrentItemVar);
                    }
                }
            }
            else if(m_Parameters->m_ObservedItem == observable)
            {
                m_NoEndObservation = true;

                if(action.first == ma::Var::Key::Obs::ms_BeginSetValue && !m_EndObservationBegin)
                {
                    auto previous_key_item = ma::Var::GetKeyValue(action.second).second;

                    if(ma::Item::HasItem(previous_key_item))
                    {
                        auto previous_item = ma::Item::GetItem(previous_key_item);
                        bool has_observation = root->m_ObservationManager->HasObservation(this, previous_item);

                        if(has_observation)
                            root->m_ObservationManager->RemoveObservation(this, previous_item);
                    }
                }
                else if(action.first == ma::Var::Key::Obs::ms_EndSetValue)
                {
                    UpdateObservedItem(m_Parameters->m_ObservedItem);

                    auto item = m_Parameters->m_ObservedItem->GetItem();

                    if(item)
                    {
                        // Si l'item est observé alors, on peut commencer la construction de la grille.
                        bool has_observation = root->m_ObservationManager->HasObservation(this, item);
                        if(has_observation)
                            BuildGrid();
                    }
                }

                m_NoEndObservation = false;
            }
            else if(m_Parameters->m_ObservedItem->GetItem() == observable &&
                    (action.first == ma::Item::Key::Obs::ms_AddItemEnd ||
                     action.first == ma::Item::Key::Obs::ms_RemoveItemEnd))
            {
                BuildGrid();
            }
        }
    }

    void PanelResourcesExplorer::EndObservation(const ma::ObservablePtr &observable)
    {
        if(ma::ClassInfoManager::IsInherit(observable, ma::Item::GetItemClassName()))
        {
            auto item = m_Parameters->m_ObservedItem->GetItem();

            if(item == observable && !m_NoEndObservation && !m_SetCurrentItem)
            {
                Clear();

                auto parent_key = item->GetParentKey();

                if(ma::Item::HasItem(parent_key))
                {
                    auto parent_item = ma::Item::GetItem(parent_key);
                    auto folders = ma::Find<ma::Folder::MatchFolder>::GetDeque(
                        parent_key, ma::Folder::GetMatchFolder(), ma::Item::SearchMod::LOCAL);
                    auto resources = ma::Find<ma::Resource::MatchResource>::GetDeque(
                        parent_key, ma::Resource::GetMatchResource(), ma::Item::SearchMod::LOCAL);

                    if(!folders.empty() || !resources.empty() ||
                       ma::ClassInfoManager::IsInherit(parent_item, ma::Folder::GetFolderClassName()))
                    {
                        m_EndObservationBegin = true;
                        m_Parameters->m_ObservedItem->SetItem(parent_item);
                        m_EndObservationBegin = false;
                    }
                }
            }
        }
        else if(observable == m_CurrentItemVar || observable == m_Parameters->m_ObservedItem)
        {
            Clear();
        }
    }

    bool PanelResourcesExplorer::SetNewItem(ma::ItemPtr old_item, ma::ItemPtr new_item)
    {
        auto root = ma::root();

        bool result = false;

        if(old_item != new_item && old_item)
        {
            bool has_observation = root->m_ObservationManager->HasObservation(this, old_item);
            if(has_observation)
                root->m_ObservationManager->RemoveObservation(this, old_item);
        }

        if(new_item)
        {
            bool has_observation = root->m_ObservationManager->HasObservation(this, new_item);

            result = has_observation;
            if(!has_observation && new_item->IsObservable())
            {
                root->m_ObservationManager->AddObservation<PanelResourcesExplorerVar, ma::ItemVar>(this, new_item);
                result = true;
            }
        }

        return result;
    }

    void PanelResourcesExplorer::UpdateObservedItem(ma::ItemVarPtr var)
    {
        if(!m_SetCurrentItem)
        {
            m_SetCurrentItem = true;

            const auto item = var->GetItem();
            const auto current_observed_item = m_Parameters->m_ObservedItem->GetItem();
            const auto current_observed_key =
                current_observed_item ? current_observed_item->GetKey() : ma::Item::Key::GetNoParent();

            if(ma::ClassInfoManager::IsInherit(item, ma::Resource::GetResourceClassName()))
            {
                const auto resource = std::dynamic_pointer_cast<ma::Resource>(item);

                MA_ASSERT(resource, L"resource est nulle.", std::runtime_error);

                const auto parent_key = resource->GetParentKey();

                if(current_observed_key != parent_key && ma::Item::HasItem(parent_key))
                {
                    auto new_item = ma::Item::GetItem(parent_key);
                    SetNewItem(current_observed_item, new_item);
                    m_Parameters->m_ObservedItem->SetItem(new_item);
                }
            }
            else if(ma::ClassInfoManager::IsInherit(item, ma::Folder::GetFolderClassName()))
            {
                auto folder = std::dynamic_pointer_cast<ma::Folder>(item);

                MA_ASSERT(folder, L"folder est nulle.", std::runtime_error);

                const auto folder_key = folder->GetKey();

                if(SetNewItem(current_observed_item, folder) && current_observed_key != folder_key)
                    m_Parameters->m_ObservedItem->SetItem(folder);
            }
            else
            {
                const auto item_key = item->GetKey();
                const auto folders = ma::Find<ma::Folder::MatchFolder>::GetDeque(
                    item_key, ma::Folder::GetMatchFolder(), ma::Item::SearchMod::LOCAL);
                const auto resources = ma::Find<ma::Resource::MatchResource>::GetDeque(
                    item_key, ma::Resource::GetMatchResource(), ma::Item::SearchMod::LOCAL);

                // On ne met que les item qui sont observés et qui contiennent au
                // moins un dossier et/ou un fichier.
                if((!folders.empty() || !resources.empty()) && SetNewItem(current_observed_item, item) &&
                   current_observed_key != item_key)
                    m_Parameters->m_ObservedItem->SetItem(item);
            }

            m_SetCurrentItem = false;
        }
    }

    void PanelResourcesExplorer::BuildGrid()
    {
        static const std::wstring function_name = L"void ma::wxPanelResourcesExplorer::BuildGrid()";

        Clear();

        auto item = m_Parameters->m_ObservedItem->GetItem();

        if(item && item->IsObservable())
        {
            auto root = ma::root();

            if(ma::ClassInfoManager::IsInherit(item, ma::Resource::GetResourceClassName()))
            {
                const auto resource = std::dynamic_pointer_cast<ma::Resource>(item);

                MA_ASSERT(resource, L"resource est nulle.", std::runtime_error);

                const auto parent_key = resource->GetParentKey();

                if(ma::Item::HasItem(parent_key))
                {
                    item = ma::Item::GetItem(parent_key);
                }
            }

            // Si l'item courant est modifié, il faut vérifier s'il contient des
            // dossiers ou des ressources si oui, on reconstruit la vue, sinon on ne fait rien.
            const auto item_key = item->GetKey();
            const auto folders = ma::Find<ma::Folder::MatchFolder>::GetDeque(
                item_key, ma::Folder::GetMatchFolder(), ma::Item::SearchMod::LOCAL);
            const auto resources = ma::Find<ma::Resource::MatchResource>::GetDeque(
                item_key, ma::Resource::GetMatchResource(), ma::Item::SearchMod::LOCAL);

            if(!folders.empty() || !resources.empty())
            {
                for(const auto &folder : folders)
                {
                    if(folder->IsObservable())
                    {
                        auto panel = new PanelFolderItem(folder, m_Parameters, m_ScrolledWindow);
                        m_GridSizer->Add(panel, 1, wxEXPAND, 5);
                        m_PanelsItems[folder->GetKey()] = panel;
                    }
                }

                for(const auto &resource : resources)
                {
                    if(resource->IsObservable())
                    {
                        auto panel = new PanelResourceItem(resource, m_Parameters, m_ScrolledWindow);
                        m_GridSizer->Add(panel, 1, wxEXPAND, 5);
                        m_PanelsItems[resource->GetKey()] = panel;
                    }
                }

                m_ScrolledWindow->Layout();
                // m_GridSizer->Fit(m_ScrolledWindow);
                this->Layout();
            }

            if(ma::ClassInfoManager::IsInherit(item, ma::Folder::GetFolderClassName()))
                SetupDir();
        }
    }

    class M_DLL_EXPORT ObjectItemParameter: public ObjectKey<ma::Item::key_type>
    {
        public:
            PanelResourcesExplorerParameterPtr m_Parameters;

            ObjectItemParameter(): ObjectKey(), m_Parameters()
            {}

            ObjectItemParameter(const ma::Item::key_type &key, PanelResourcesExplorerParameterPtr parameters):
            ObjectKey(key),
            m_Parameters(parameters)
            {}

        protected:
            wxDECLARE_DYNAMIC_CLASS(wxObjectItemParameter);
    };

    wxIMPLEMENT_DYNAMIC_CLASS(ObjectItemParameter, wxObject)

        void PanelResourcesExplorer::SetupDir()
    {
        for(auto it : m_DirButtons)
            it->Destroy();
        m_DirButtons.clear();

        for(auto it : m_DirSeparators)
            it->Destroy();
        m_DirSeparators.clear();

        // Pour nettoyer les espaces
        m_HeaderHorizontalSizer->Clear(true);

        auto item = m_Parameters->m_ObservedItem->GetItem();
        if(ma::ClassInfoManager::IsInherit(item, ma::Resource::GetResourceClassName()))
        {
            auto resource = std::dynamic_pointer_cast<ma::Resource>(item);

            MA_ASSERT(resource, L"resource est nulle.", std::runtime_error);

            auto parent_key = resource->GetParentKey();

            if(ma::Item::HasItem(parent_key))
            {
                item = ma::Item::GetItem(parent_key);
            }
        }
        auto root = MA_ROOT;

        if(item && root->m_ClassInfoManager->IsInherit(item, ma::Folder::GetFolderClassName()))
        {
            ma::FolderPtr folder = std::dynamic_pointer_cast<ma::Folder>(item);
            const auto path = folder->m_Name->toString();

            std::vector<std::pair<std::wstring, ma::Item::key_type>> folder_names = {};

            while(folder && folder_names.size() <= (m_Parameters ? m_Parameters->m_CountDisplayResPath->Get() : 4u))
            {
                auto parent_key = folder->GetParentKey();
                folder = nullptr;

                if(ma::Item::HasItem(parent_key))
                {
                    auto item2 = ma::Item::GetItem(parent_key);
                    if(root->m_ClassInfoManager->IsInherit(item2, ma::Folder::GetFolderClassName()))
                    {
                        folder = std::dynamic_pointer_cast<ma::Folder>(item2);
                        folder_names.emplace_back(folder->m_Name->Get(), item->GetKey());
                    }
                }
            }

            folder_names.emplace_back(L"./", ma::Item::Key::GetResourceManager());

            for(auto it = folder_names.rbegin(); it != folder_names.rend(); ++it)
            {
                auto dir_button = new wxButton(
                    this, wxID_ANY, it->first, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE | wxBU_EXACTFIT);

                m_HeaderHorizontalSizer->Add(5, dir_button->GetSize().y);
                m_HeaderHorizontalSizer->Add(dir_button, 0, wxTOP | wxBOTTOM, 5);
                m_HeaderHorizontalSizer->Add(5, dir_button->GetSize().y);
                m_DirButtons.push_back(dir_button);

                dir_button->Bind(
                    wxEVT_COMMAND_BUTTON_CLICKED,
                    [](wxCommandEvent &event)
                    {
                        auto obj = event.GetEventUserData();

                        auto objkey = dynamic_cast<ObjectItemParameter *>(obj);

                        MA_ASSERT(objkey,
                                  L"objkey est nulle",
                                  L"ma::wxPanelResourcesExplorer::SetupDir() : lambda",
                                  std::runtime_error);

                        objkey->m_Parameters->m_ObservedItem->SetItem(ma::Item::GetItem(objkey->GetKey()));
                    },
                    wxID_ANY,
                    wxID_ANY,
                    new ObjectItemParameter(it->second, m_Parameters));

                auto dir_separator = new wxStaticText(this, wxID_ANY, _(">"), wxDefaultPosition, wxDefaultSize, 0);
                dir_separator->Wrap(-1);
                m_HeaderHorizontalSizer->Add(dir_separator, 0, wxALIGN_CENTER_VERTICAL | wxTOP | wxBOTTOM, 5);
                m_DirSeparators.push_back(dir_separator);
            }

            auto dir_button =
                new wxButton(this, wxID_ANY, path, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE | wxBU_EXACTFIT);
            m_HeaderHorizontalSizer->Add(5, dir_button->GetSize().y);
            m_HeaderHorizontalSizer->Add(dir_button, 0, wxTOP | wxBOTTOM, 5);
            m_HeaderHorizontalSizer->Add(5, dir_button->GetSize().y);
            m_DirButtons.push_back(dir_button);

            m_HeaderHorizontalSizer->Layout();
            this->Layout();
        }
    }

    void PanelResourcesExplorer::Clear()
    {
        for(auto it : m_PanelsItems)
            it.second->Destroy();
        m_PanelsItems.clear();

        for(auto it : m_DirButtons)
            it->Destroy();
        m_DirButtons.clear();

        for(auto it : m_DirSeparators)
            it->Destroy();
        m_DirSeparators.clear();

        m_ScrolledWindow->Layout();
        this->Layout();
    }

    const ma::TypeInfo &PanelResourcesExplorer::GetwxPanelResourcesExplorerTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxPanelResourcesExplorerClassName(),
            L"Explorateur des fichiers ressources.", {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(PanelResourcesExplorer, wx, Observer)

    // PanelResourcesExplorerParameter //———————————————————————————————————————————————————————————————————————————————
    const ma::Item::key_type &PanelResourcesExplorerParameter::Key::Get()
    {
        static const ma::Item::key_type key{L"e79dc01a-1776-4425-b8af-ed2b5d2907c9"};
        return key;
    }

    PanelResourcesExplorerParameter::PanelResourcesExplorerParameter(const ConstructorParameters &params):
    Parameter::Parameter({params, Key::Get(), true, false}, PanelResourcesExplorer::ms_classInfo.GetClassName()),
    // clang-format off
    m_CountDisplayResPath          (AddMemberVar<ma::UVar>       (L"m_CountDisplayResPath"          , 4u)),
    m_PanelItemWidth               (AddMemberVar<ma::UVar>       (L"m_PanelItemWidth"               , 160u)),
    m_PanelItemHeight              (AddMemberVar<ma::UVar>       (L"m_PanelItemHeight"              , 160u)),
    m_ItemsBackgroundColour        (AddMemberVar<ma::wxColourVar>(L"m_ItemsBackgroundColour"        , wxColour(255, 255, 255, 0))),
    m_ItemsForegroundColour        (AddMemberVar<ma::wxColourVar>(L"m_ItemsForegroundColour"        , wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT))),
    m_ItemsHoverBackgroundColour   (AddMemberVar<ma::wxColourVar>(L"m_ItemsHoverBackgroundColour"   , wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHT))),
    m_ItemsHoverForegroundColour   (AddMemberVar<ma::wxColourVar>(L"m_ItemsHoverForegroundColour"   , wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHTTEXT))),
    m_ItemsSelectedBackgroundColour(AddMemberVar<ma::wxColourVar>(L"m_ItemsSelectedBackgroundColour", wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHT))),
    m_ItemsSelectedForegroundColour(AddMemberVar<ma::wxColourVar>(L"m_ItemsSelectedForegroundColour", wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHTTEXT))),
    // clang-format on
    m_ObservedItem(AddMemberVar<ma::ItemVar>(L"m_ObservedItem"))
    {}

    void PanelResourcesExplorerParameter::EndConstruction()
    {
        Parameter::EndConstruction();

        // clang-format off
        const auto& doc = ma::Var::Key::IInfo::ms_Doc;

        std::wstring message;

        message = L"Le nombre de sous-dossiers afficher dans l'en-tête de l'explorateur de ressources.";
        m_CountDisplayResPath->InsertIInfoKeyValue(doc, message);

        message = L"Définition en pixel de la largeur d'un item de l'explorateur de ressources.";
        m_PanelItemWidth->InsertIInfoKeyValue(doc, message);

        message = L"Définition en pixel de la hauteur d'un item de l'explorateur de ressources.";
        m_PanelItemHeight->InsertIInfoKeyValue(doc, message);

        message = L"Couleur de fond des éléments représentant les fichiers et les dossiers.";
        m_ItemsBackgroundColour->InsertIInfoKeyValue(doc, message);

        message = L"Couleur de devant des éléments représentant les fichiers et les dossiers.";
        m_ItemsForegroundColour->InsertIInfoKeyValue(doc, message);

        message = L"Couleur d'arrière plan pour signifier le passage de la sourie sur un des éléments représentant les "
                  L"fichiers et les dossiers.";
        m_ItemsHoverBackgroundColour->InsertIInfoKeyValue(doc, message);

        message = L"Couleur de premier plan pour signifier le passage de la sourie sur un des éléments représentant les"
                  L"fichiers et les dossiers.";
        m_ItemsHoverForegroundColour->InsertIInfoKeyValue(doc, message);

        message = L"Couleur d'arrière plan pour signifier la sélection des éléments représentant les fichiers et les "
                  L"dossiers.";
        m_ItemsSelectedBackgroundColour->InsertIInfoKeyValue(doc, message);

        message = L"Couleur de premier plan pour signifier la sélection des éléments représentant les fichiers et les "
                  L"dossiers.";
        m_ItemsSelectedForegroundColour->InsertIInfoKeyValue(doc, message);

        message = L"L'item qui a été choisi pour construire la vue.\n"
                  L"Remarques:\n"
                  L"Si « root->current » est un fichier, alors « m_ObserverdItem » sera le parent de cette ressource.\n"
                  L"Si « root->current » ne contient ni dossier et ni ressource, alors « m_ObserverdItem » ne sera pas"
                  L"modifier.\n"
                  L"Si root->current contient un ou plusieurs dossier et ou ressources alors « m_ObserverdItem » sera "
                  L"« root->current ».";
        m_ObservedItem->InsertIInfoKeyValue(doc, message);
        // clang-format on

        auto obs = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
        // Pour obtenir le pointeur partagé de l'item, il faut attendre l'item soit construit.
        // Sans ça une erreur sera levée lors de la création de l'observation.
        obs->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_CountDisplayResPath);
        obs->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_PanelItemWidth);
        obs->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_PanelItemHeight);
    }

    PanelResourcesExplorerParameter::~PanelResourcesExplorerParameter() = default;

    void PanelResourcesExplorerParameter::UpdateVar(ma::VarPtr var, const ma::Observable::Data &data, wxWindow *window)
    {
        Parameter::UpdateVar(var, data, window);

        auto *explorer = wxDynamicCast(window, ma::wx::PanelResourcesExplorer);

        if(m_CountDisplayResPath->GetKey() == var->GetKey())
        {
            explorer->SetupDir();
        }

        if(m_PanelItemWidth->GetKey() == var->GetKey())
        {
            explorer->ResizeGrid();
        }

        if(m_PanelItemHeight->GetKey() == var->GetKey())
        {
            explorer->ResizeGrid();
        }
    }

    const ma::TypeInfo &PanelResourcesExplorerParameter::GetwxPanelResourcesExplorerParameterTypeInfo()
    {
        const auto parameter_typeinfo = Parameter::GetwxParameterTypeInfo();

        // clang-format off
        static const ma::TypeInfo result =
        {
            GetwxPanelResourcesExplorerParameterClassName(),
            L"Classe qui fait l'interface entre l'explorateur de ressources et l'interface graphique afin d'afficher et"
            L" de modifier les paramètres d'affichage de l'explorateur de ressources.",
            parameter_typeinfo.MergeDatas({})
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(PanelResourcesExplorerParameter, wx, Parameter::GetwxParameterClassHierarchy())
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(PanelResourcesExplorerParameter, wx)

    // PanelResourcesExplorerView //————————————————————————————————————————————————————————————————————————————————————
    const ma::Item::key_type &PanelResourcesExplorerView::Key::Get()
    {
        static const ma::Item::key_type key{L"335ddc51-943c-454f-a898-6406595f63c8"};
        return key;
    }

    PanelResourcesExplorerView::PanelResourcesExplorerView(const ma::Item::ConstructorParameters &params):
    View({params, Key::Get(), true, false}, L"Explorateur des ressources")
    {}

    PanelResourcesExplorerView::~PanelResourcesExplorerView() = default;

    void PanelResourcesExplorerView::EndConstruction()
    {
        View::EndConstruction();

        AddVar<StringVar>(ma::Item::Key::Var::GetDoc(),
                          L"L'explorateur de ressources permet de parcourir la hiérarchies de dossiers et de fichiers "
                          L"enfant du gestionnaire de ressources.");

        ma::Item::CreateItem<Bottom>({GetKey()});
        ma::Item::CreateItem<Y>({GetKey()});
        ma::Item::CreateItem<Widgets>({GetKey()});
    }

    wxWindow *PanelResourcesExplorerView::BuildWindow(wxWindow *parent)
    {
        static const wxWindowID id{14272};

        return new ma::wx::PanelResourcesExplorer(parent, _(L"Explorateur de ressources"), id);
    }

    const ma::TypeInfo &PanelResourcesExplorerView::GetwxPanelResourcesExplorerViewTypeInfo()
    {
        const auto view_typeinfo = View::GetwxViewTypeInfo();

        // clang-format off
        static const ma::TypeInfo result =
        {
            GetwxPanelResourcesExplorerViewClassName(),
            L"L'explorateur de ressources permet de parcourir la hiérarchies de dossiers et de fichiers enfant du "
            L"gestionnaire de ressources.",
            view_typeinfo.MergeDatas({})
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(PanelResourcesExplorerView, wx, View::GetwxViewClassHierarchy())

} // namespace ma::wx
M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(PanelResourcesExplorerView, wx)
