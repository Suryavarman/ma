/// \file wx/View.cpp
/// \author Pontier Pierre
/// \date 2024-01-01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/wx/View.h"
#include "ma/Find.h"

namespace ma::wx
{
    // Tag // ——————————————————————————————————————————————————————————————————————————————————————————————————————————
    Tag::Tag(const Item::ConstructorParameters &in_params): Item(in_params)
    {}

    Tag::~Tag() = default;

    const TypeInfo &Tag::GetwxTagTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxTagClassName(),
            L"La classe « Tag » permet de créer en héritant d'elle des étiquettes pour définir l'usage des vues.",
            {
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        View::GetwxViewClassName()
                    }
                },
                {
                    TypeInfo::Key::GetBlackListParentsData(),
                    {
                        Views::GetwxViewsClassName()
                    }
                },
                {
                    TypeInfo::Key::GetBlackListChildrenData(),
                    {
                        View::GetwxViewClassName(),
                        Views::GetwxViewsClassName()
                    }
                }
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Tag, wx, Item)

} // namespace ma::wx

namespace ma
{
    M_CPP_ITEM_TYPE_VAR_WITH_NAMESPACE(Tag, wx)
}

MA_CPP_DEF_TAG(Position, Tag, L"Classe parente regroupent les étiquette indiquent le positionnement privilégié.")
MA_CPP_DEF_TAG(Bottom, Position, L"La vue sera placé de préférence en bas.")
MA_CPP_DEF_TAG(Top, Position, L"La vue sera placé de préférence en haut.")
MA_CPP_DEF_TAG(Right, Position, L"La vue sera placé de préférence à droite")
MA_CPP_DEF_TAG(Left, Position, L"La vue sera placé de préférence à gauche.")
MA_CPP_DEF_TAG(Center, Position, L"La vue sera placé de préférence au centre.")

MA_CPP_DEF_TAG(Axis, Tag, L"L'Axe sur lequel la vue s'étend de préférence.")
MA_CPP_DEF_TAG(X, Axis, L"La vue s'étend dans le sense de la largeur.")
MA_CPP_DEF_TAG(Y, Axis, L"La vue s'étend dans le sense de la hauteur.")
MA_CPP_DEF_TAG(XY, Axis, L"La vue s'étend dans le sense de la hauteur et de la largeur.")

MA_CPP_DEF_TAG(Type, Tag, L"Type de la vue.")
MA_CPP_DEF_TAG(Text, Type, L"La vue est une zone d'édition de texte.")
MA_CPP_DEF_TAG(Widgets, Type, L"La vue est un ensemble d'éléments d'interface.")
MA_CPP_DEF_TAG(Rendering, Type, L"La vue est une zone de rendu.")
MA_CPP_DEF_TAG(RealTime, Rendering, L"La vue est une zone de rendu en temps réel.")
MA_CPP_DEF_TAG(Precompute, Rendering, L"La vue est une zone de rendu pré-calculé..")

namespace ma::wx
{
    // View // —————————————————————————————————————————————————————————————————————————————————————————————————————————
    constexpr auto var_key_display = L"m_Display";

    View::View(const Item::ConstructorParameters &params, const ma::Var::key_type &name):
    Item(params),
    Observer(),
    m_WindowID{0},
    m_Display{AddMemberVar<ma::BoolVar>(var_key_display, false)},
    m_Name{AddReadOnlyMemberVar<ma::StringVar>(Key::Var::GetName(), name)}
    {}

    View::~View() = default;

    void View::EndConstruction()
    {
        Item::EndConstruction();
    }

    wxWindow *View::CreateView(wxWindow *parent)
    {
        auto *window = BuildWindow(parent);

        m_WindowID = window->GetId();

        m_Display->Set(true);
        return window;
    }

    void View::ViewDestroyed()
    {
        m_Display->Set(false);
        m_WindowID = 0;
    }

    wxWindow *View::GetView() const
    {
        if(m_WindowID != 0 && m_WindowID != 1)
        {
            wxWindow *window = wxWindow::FindWindowById(m_WindowID);

            // Test
            MA_WARNING(window, L"Aucune fenêtre n'est associée à l'id: «" + std::to_wstring(m_WindowID) + L"».");

            return window;
        }
        else
            return nullptr;
    }

    View::tags_type View::GetTags() const
    {
        typedef ma::MatchType<Tag> MatchTags;
        auto tags = ma::Find<MatchTags>::GetSet(GetKey(), MatchTags(), ma::Item::SearchMod::LOCAL);

        View::tags_type result;

        for(const auto &tag : tags)
            result.insert(tag->GetTypeInfo().m_ClassName);

        return result;
    }

    void View::BeginObservation(const ObservablePtr &observable)
    {}

    void View::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {}

    void View::EndObservation(const ObservablePtr &observable)
    {}

    void View::OnEnable()
    {
        Item::OnEnable();
        Observer::OnEnable();
    }

    void View::OnDisable()
    {
        Item::OnDisable();
        Observer::OnDisable();
    }

    const TypeInfo &View::GetwxViewTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxViewClassName(),
            L"La classe « View » permet de créer une vue sans connaitre le gestionnaire qui l'affichera."
            L"Par exemple un module peut ajouter une console python sans pour autant connaître la structure de "
            L"l'interface graphique.",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        Tag::GetwxTagClassName()
                    }
                },
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        Views::GetwxViewsClassName()
                    }
                }
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(View, wx, Item)

    // Views // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Item::key_type &Views::Key::Get()
    {
        static const ma::Item::key_type key{L"67b3b21e-2b86-426a-88ef-94046eb6be26"};
        return key;
    }

    Views::Views(const Item::ConstructorParameters &in_params): Item({in_params, Key::Get(), true, false})
    {}

    Views::~Views() = default;

    void Views::EndConstruction()
    {
        Item::EndConstruction();
    }

    Views::views_type Views::GetViews() const
    {
        typedef ma::MatchType<ma::wx::View> MatchViews;
        auto views = ma::Find<MatchViews>::GetDeque(GetKey(), MatchViews(), ma::Item::SearchMod::LOCAL);
        return views;
    }

    Views::tags_type Views::GetTags() const
    {
        tags_type tags;

        auto class_parents = TypeInfo::GetQuickAccessHierarchies();

        for(auto &&[class_name, parents] : class_parents)
        {
            if(parents.count(Tag::GetwxTagClassName()) > 0)
                tags.insert(class_name);
        }

        return tags;
    }

    const TypeInfo &Views::GetwxViewsTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxViewsClassName(),
            L"La classe « Views » sert de conteneur unique aux vue de type « View ».",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        View::GetwxViewClassName(),
                    }
                },
                {
                    TypeInfo::Key::GetBlackListChildrenData(),
                    {
                        Tag::GetwxTagClassName()
                    }
                },
                {
                    TypeInfo::Key::GetBlackListParentsData(),
                    {
                        Tag::GetwxTagClassName(),
                        View::GetwxViewClassName()
                    }
                }
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Views, wx, Item)

} // namespace ma::wx
