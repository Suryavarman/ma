/// \file wx/AddVarDialog.cpp
/// \author Pontier Pierre
/// \date 2020-03-16
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/Error.h"
#include "ma/Algorithm.h"
#include "ma/Translation.h"
#include "ma/Item.h"
#include "ma/Root.h"
#include "ma/RTTI.h"
#include "ma/wx/AddVarDialog.h"

namespace ma::wx
{
    AddVarDialog::AddVarDialog(const ma::Item::key_type &key,
                               wxWindow *parent,
                               wxWindowID id,
                               const wxString &title,
                               const wxPoint &pos,
                               const wxSize &size,
                               long style):
    wxDialog(parent, id, title, pos, size, style),
    m_Item(ma::Item::GetItem(key))
    {
        wxArrayString class_check_list_choices = GetList();

        this->SetSizeHints(wxDefaultSize, wxDefaultSize);

        wxBoxSizer *vertical_sizer;
        vertical_sizer = new wxBoxSizer(wxVERTICAL);

        wxBoxSizer *horizontal_sizer_top;
        horizontal_sizer_top = new wxBoxSizer(wxHORIZONTAL);

        m_KeyStaticText =
            new wxStaticText(this, wxID_ANY, M_Tr(L"Clef de la variable :"), wxDefaultPosition, wxDefaultSize, 0);
        m_KeyStaticText->Wrap(-1);
        horizontal_sizer_top->Add(m_KeyStaticText, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_KeyTextCtrl =
            new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
        m_KeyTextCtrl->SetToolTip(
            M_Tr(L"Clef de la variable. \nVoici un exemple de clef: \n«m_Var»\n\nLa variable doit "
                 L"être unique. Le texte est rouge si la clef n'est pas unique."));

        horizontal_sizer_top->Add(m_KeyTextCtrl, 1, wxALL, 5);

        vertical_sizer->Add(horizontal_sizer_top, 0, wxEXPAND, 5);

        wxBoxSizer *horizontal_sizer_middle;
        horizontal_sizer_middle = new wxBoxSizer(wxHORIZONTAL);

        m_TypesListBox = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxSize(200, -1), 0, nullptr, wxLB_SINGLE);
        if(!class_check_list_choices.empty())
        {
            m_TypesListBox->InsertItems(class_check_list_choices, 0);
            horizontal_sizer_middle->Add(m_TypesListBox, 0, wxEXPAND | wxTOP | wxBOTTOM | wxLEFT, 5);
            if(!class_check_list_choices.empty())
            {
                m_TypesListBox->SetSelection(0);
            }
        }
        m_HTMLWin = new wxHtmlWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO);
        horizontal_sizer_middle->Add(m_HTMLWin, 1, wxEXPAND | wxALL, 5);
        SetHtml();

        vertical_sizer->Add(horizontal_sizer_middle, 1, wxEXPAND, 5);

        wxBoxSizer *horizontal_sizer_bottom;
        horizontal_sizer_bottom = new wxBoxSizer(wxHORIZONTAL);

        m_ValidateButton = new wxButton(this, wxID_ANY, M_Tr(L"Ajouter"), wxDefaultPosition, wxDefaultSize, 0);
        m_ValidateButton->SetToolTip(
            M_Tr(L"Valide le choix, créé une instance du type sélectionné et l'ajoute à l'item courant."));

        horizontal_sizer_bottom->Add(m_ValidateButton, 0, wxALL, 5);

        m_CancelButton = new wxButton(this, wxID_ANY, M_Tr(L"Annuler"), wxDefaultPosition, wxDefaultSize, 0);
        m_CancelButton->SetToolTip(M_Tr(L"Ferme la fenêtre sans ajouter de nouvel item."));

        horizontal_sizer_bottom->Add(m_CancelButton, 0, wxALL, 5);

        vertical_sizer->Add(horizontal_sizer_bottom, 0, wxEXPAND, 5);

        this->SetSizer(vertical_sizer);
        this->Layout();

        this->Centre(wxBOTH);

        if(!class_check_list_choices.empty())
        {
            m_KeyTextCtrl->Bind(wxEVT_COMMAND_TEXT_UPDATED, &AddVarDialog::OnText, this);
            m_KeyTextCtrl->Bind(wxEVT_COMMAND_TEXT_ENTER, &AddVarDialog::OnTextEnter, this);
            m_ValidateButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddVarDialog::OnAdd, this);
            m_TypesListBox->Bind(wxEVT_COMMAND_LISTBOX_SELECTED, &AddVarDialog::OnSelected, this);
        }

        m_CancelButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddVarDialog::OnCancel, this);

        m_ValidateButton->Enable(false);
    }

    AddVarDialog::~AddVarDialog() = default;

    void AddVarDialog::Add()
    {
        const std::wstring text = m_KeyTextCtrl->GetValue().ToStdWstring();

        if(!text.empty() && !m_Item->HasVar(text))
        {
            int selection = m_TypesListBox->GetSelection();

            MA_ASSERT(
                selection != wxNOT_FOUND, L"Il devrait toujours avoir un type de sélectionné.", std::runtime_error);

            const std::wstring type = m_TypesListBox->GetString(selection).ToStdWstring();

            ma::root()->m_RttiManager->CreateVar(type, text, m_Item->GetKey());

            EndModal(0);
        }
    }

    void AddVarDialog::OnTextEnter(wxCommandEvent &event)
    {
        Add();
        event.Skip();
    }

    void AddVarDialog::OnText(wxCommandEvent &event)
    {
        const std::wstring text = m_KeyTextCtrl->GetValue().ToStdWstring();

        if(!text.empty() && !m_Item->HasVar(text))
        {
            m_KeyTextCtrl->SetForegroundColour(*wxBLUE);
            m_ValidateButton->Enable(true);
        }
        else
        {
            m_KeyTextCtrl->SetForegroundColour(*wxRED);
            m_ValidateButton->Enable(false);
        }

        event.Skip();
    }

    void AddVarDialog::OnAdd(wxCommandEvent &event)
    {
        Add();
        event.Skip();
    }

    void AddVarDialog::OnCancel(wxCommandEvent &event)
    {
        EndModal(0);
        event.Skip();
    }

    void AddVarDialog::OnSelected(wxCommandEvent &event)
    {
        SetHtml();
        event.Skip();
    }

    wxArrayString AddVarDialog::GetList()
    {
        wxArrayString result;

        const auto &key = ma::MakerVar::Key::GetClassNameVarProperty();
        const auto compare = ma::CompareItemsString(key);
        ma::ItemsOrder children = ma::Sort::SortItems(ma::Item::Key::GetRTTI(), compare, ma::Item::SearchMod::LOCAL);

        for(const auto &item : children)
        {
            if(auto opt = item->HasVarOpt(key))
                result.Add(opt.value()->toString());
        }

        return result;
    }

    void AddVarDialog::SetHtml()
    {
        const int selection = m_TypesListBox->GetSelection();

        if(selection != wxNOT_FOUND)
        {
            const std::wstring type = m_TypesListBox->GetString(selection).ToStdWstring();

            const auto makers = ma::root()->m_RttiManager->GetRegisteredMakerVars();
            const auto it_find = makers.find(type);

            MA_ASSERT(it_find != makers.end(),
                      L"Le nom de la classe «" + type + L"» ne correspond à aucune fabrique de variables enregistrée.",
                      std::runtime_error);

            ma::TypeInfo info = it_find->second->GetFactoryTypeInfo();

            std::wstringstream ss;

            ss << L"<h1>" << type << L"</h1>" << std::endl;

            std::vector<std::wstring> description = ma::Split(info.m_Description, '\n');
            ss << L"<p>" << std::endl;
            for(const auto &text : description)
            {
                ss << text << L"<br>" << std::endl;
            }
            ss << L"</p>" << std::endl;

            ss << "<ul>" << std::endl;
            for(const auto &key_value : info.m_Data)
            {
                ss << L"<li>" << ma::toString(key_value) << L"</li>" << std::endl;
            }
            ss << L"</ul>" << std::endl;

            m_HTMLWin->SetPage(ss.str());
        }
    }
} // namespace ma::wx