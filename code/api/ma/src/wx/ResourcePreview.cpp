/// \file wx/ResourcePreview.cpp
/// \author Pontier Pierre
/// \date 2021-06-22
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/wx/ResourcePreview.h"
#include "ma/Macros.h"

// wx::ResourcePreviewer//——————————————————————————————————————————————————————————————————————————————————————————————
bool ma::wx::ResourcePreviewer::Get(ma::ResourcePtr res, wxImage &image)
{
    MA_ASSERT(
        !ma::wx::ResourcePreviewManager::IsPreviewResource(res->GetParentKey()),
        L"Tentative de créer un aperçu d'un aperçu. La ressource «" + res->GetKey() +
            L" est un aperçu. Pour éviter des problèmes  cycliques, il a été interdit de faire un aperçu d'un aperçu.",
        std::invalid_argument);

    MA_ASSERT(image.GetSize().GetWidth() != 0, L"La largeur de l'image est nulle.", std::invalid_argument);

    MA_ASSERT(image.GetSize().GetHeight() != 0, L"La hauteur de l'image est nulle.", std::invalid_argument);

    return FillImage(res, image);
}

// wx::ArtProviderPR //—————————————————————————————————————————————————————————————————————————————————————————————————
bool ma::wx::ArtProviderPR::FillImage(ma::ResourcePtr res, wxImage &image)
{
    return false;
}

bool ma::wx::ArtProviderPR::Usable() const
{
    return false;
}

// wx::ResourcePreviewMap //————————————————————————————————————————————————————————————————————————————————————————————
void ma::wx::ResourcePreviewMap::SetFromItem(const std::wstring &value)
{
    MA_ASSERT(false, L"cette fonction n'est pas implémentée");
}

ma::VarPtr ma::wx::ResourcePreviewMap::Copy() const
{
    MA_ASSERT(false, L"cette fonction n'est pas implémentée");
    return nullptr;
}

bool ma::wx::ResourcePreviewMap::HasPreviewer(const ma::ResourcePtr &res) const
{
    MA_ASSERT(false, L"cette fonction n'est pas implémentée");
    return false;
}

ma::wx::ResourcePreviewertPtr ma::wx::ResourcePreviewMap::GetPreviewer(ma::ResourcePtr res)
{
    MA_ASSERT(false, L"cette fonction n'est pas implémentée");
    return nullptr;
}

void ma::wx::ResourcePreviewMap::BeginObservation(const ObservablePtr &)
{}

void ma::wx::ResourcePreviewMap::UpdateObservation(const ObservablePtr &, const ma::Observable::Data &)
{}

void ma::wx::ResourcePreviewMap::EndObservation(const ObservablePtr &)
{}

// wx::ResourcePreviewManager
// //————————————————————————————————————————————————————————————————————————————————————————u
const ma::Var::key_type &ma::wx::ResourcePreviewManager::GetRPsKey()
{
    static const ma::Var::key_type &key{L"m_RPs"};
    return key;
}

ma::wx::ResourcePreviewManager::ResourcePreviewManager(const ma::Item::ConstructorParameters &in_params):
ma::Item(in_params),
m_RPs{}
{
    Init();
}

ma::wx::ResourcePreviewManager::~ResourcePreviewManager()
{}

void ma::wx::ResourcePreviewManager::Init()
{
    // clang-format off
    ma::wx::ResourcePreviewPriorityCmp::value_type art_provider_tuple =
    {
        0, {}, ma::Item::CreateItem<ma::wx::ArtProviderPR>({GetKey()})
    };
    // clang-format on

    ma::wx::ResourcePreviewMap::value_type::container_type container = {art_provider_tuple};
    ma::wx::ResourcePreviewMap::value_type rps{container.begin(), container.end()};

    m_RPs = AddMemberVar<ma::wx::ResourcePreviewMap>(GetRPsKey(), rps);
}

ma::wx::ImagePtr ma::wx::ResourcePreviewManager::Get(ma::ResourcePtr resource, unsigned int width, unsigned int height)
{
    // Trouver le RP

    // auto pr = m_RPs->GetPreviewer(resource);

    return ma::wx::ImagePtr();
}

bool ma::wx::ResourcePreviewManager::HasPreview(ma::ResourcePtr resource, unsigned int width, unsigned int height) const
{
    return false;
}

bool ma::wx::ResourcePreviewManager::IsPreviewResource(ma::ResourcePtr res)
{
    return false;
}

bool ma::wx::ResourcePreviewManager::IsPreviewResource(const ma::Item::key_type &key)
{
    return false;
}

ma::FolderPtr ma::wx::ResourcePreviewManager::GetPreviewFolder(ma::ResourcePtr resource)
{
    MA_HAS_TO_BE_IMPLEMENTED;
    return {};
}

namespace ma
{
    const TypeInfo &wx::ResourcePreviewManager::GetwxResourcePreviewManagerTypeInfo()
    {
        static const TypeInfo info = {GetwxResourcePreviewManagerClassName(),
                                      L"La classe «wx::ResourcePreviewManager» fait le lien avec les wxImageHandler.",
                                      {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ResourcePreviewManager, wx, Item)
} // namespace ma
