/// \file wx/TreeItems.cpp
/// \author Pontier Pierre
/// \date 2019-12-08
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/Error.h"
#include "ma/Item.h"
#include "ma/Root.h"
#include "ma/wx/TreeItems.h"
#include "ma/wx/AddItemDialog.h"

#include <regex>
#include <sstream>

#include <wx/dnd.h>

namespace ma::wx
{
    TreeItemData::TreeItemData(ma::Item::key_type key): ::wxTreeItemData(), m_Key(std::move(key))
    {
        MA_ASSERT(ma::Item::HasItem(m_Key),
                  L"La clef associée à cet item de wxTreeCtrl n'est associé à aucun item de ma::Item::ms_Map",
                  std::invalid_argument);
    }

    TreeItemData::~TreeItemData() = default;

    TreeItems::TreeItems(wxWindow *in_parent,
                         wxWindowID in_id,
                         const wxPoint &in_pos,
                         const wxSize &in_size,
                         long in_style,
                         const wxString &in_name):
    wxPanel(in_parent, in_id, in_pos, in_size, in_style, in_name),
    m_Add(nullptr),
    m_Remove(nullptr),
    m_Search(nullptr),
    m_ExecuteSearch(nullptr),
    m_TreeCtrl(nullptr),
    m_ContextMenu{},
    m_ItemsNotRemovable(),
    m_InitializationState(true)
    {
        // Initialisation du menu contextuel :
        m_ContextMenu.Append(
            wxID_ADD,
            _("Ajouter"),
            _("Affiche une fenêtre de dialogue qui permettra de sélectionner de type de l'item à ajouter "
              "en temps qu'enfant de l'item sélectionné."));

        m_ContextMenu.Append(
            wxID_REMOVE,
            _("Supprimer"),
            _("L'item sélectionné sera retiré de la liste des enfants de son parent. L'item sera alors "
              "placé dans la corbeille et si plus personne ne le référence il sera supprimé."));

        wxBoxSizer *vertical_sizer;
        vertical_sizer = new wxBoxSizer(wxVERTICAL);

        wxBoxSizer *horizontal_sizer;
        horizontal_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_Add = new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_Add->SetBitmap(wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON));
        m_Add->SetMinSize(wxSize(30, 30));

        horizontal_sizer->Add(m_Add, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_Remove =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_Remove->SetBitmap(wxArtProvider::GetBitmap(wxART_MINUS, wxART_BUTTON));
        m_Remove->SetMinSize(wxSize(30, 30));

        horizontal_sizer->Add(m_Remove, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_Search = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
        horizontal_sizer->Add(m_Search, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_ExecuteSearch =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_ExecuteSearch->SetBitmap(wxArtProvider::GetBitmap(wxART_FIND, wxART_BUTTON));
        m_ExecuteSearch->SetBitmapPressed(wxArtProvider::GetBitmap(wxART_FIND, wxART_BUTTON));
        m_ExecuteSearch->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));
        m_ExecuteSearch->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));
        m_ExecuteSearch->SetMinSize(wxSize(30, 30));

        horizontal_sizer->Add(m_ExecuteSearch, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        vertical_sizer->Add(horizontal_sizer, 0, wxEXPAND, 5);

        m_TreeCtrl =
            new wxTreeCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE); //|wxTR_EDIT_LABELS);
        vertical_sizer->Add(m_TreeCtrl, 1, wxEXPAND, 5);

        auto root = MA_ROOT;

        if(auto opt = ma::Item::HasItemOpt<ObservationManager>(ma::Item::Key::GetObservationManager()))
        {
            auto obs_manager = opt.value();

            m_ItemsNotRemovable.insert(root->GetKey());

            auto item = ma::Item::GetItem(root->GetKey());
            obs_manager->AddObservation<ma::wx::TreeItemsVar, ma::ItemVar>(this, item);

            auto items = root->GetItemsPostOrder(ma::Item::SearchMod::RECURSIVE);

            for(auto it = items.rbegin(); it != items.rend(); ++it)
            {
                auto sub_item = *it;
                if(sub_item->IsObservable())
                {
                    m_ItemsNotRemovable.insert(sub_item->GetKey());
                    obs_manager->AddObservation<ma::wx::TreeItemsVar, ma::ItemVar>(this, sub_item);
                }
            }
            m_InitializationState = false;

            items = root->GetItemsPostOrder(ma::Item::SearchMod::RECURSIVE);

            // au lieu d'utiliser « rbegin » et « rend » qui peuvent dans le cas peut-être pourraient poser des soucis
            // de mémoire.
            std::reverse(items.begin(), items.end());

            // On fait une deuxième passe pour afficher les objets non observable
            // qui se sont créés pour observer les précédents.
            for(auto &it : items)
            {
                if(!it->IsObservable())
                {
                    m_ItemsNotRemovable.insert(it->GetKey());
                    BeginObservation(it);
                }
            }
        }

        this->m_TreeCtrl->CollapseAll();

        //    auto item = root->current->Get();
        //
        //    if(item)
        //    {
        //        auto key = item->GetKey();
        //
        //        if(item && HasId(item->GetKey()))
        //            m_TreeCtrl->EnsureVisible(GetId(key));
        //    }

        root->m_ObservationManager->AddObservation<ma::wx::TreeItemsVar, ma::VarVar>(this, root->m_Current);

        this->SetSizer(vertical_sizer);
        this->Layout();

        m_Add->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &TreeItems::OnAdd, this);
        m_Remove->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &TreeItems::OnRemove, this);

        Bind(wxEVT_MENU, &TreeItems::OnAdd, this, wxID_ADD);
        Bind(wxEVT_MENU, &TreeItems::OnRemove, this, wxID_REMOVE);

        m_Search->Bind(wxEVT_COMMAND_TEXT_ENTER, &TreeItems::OnSearch, this);
        m_ExecuteSearch->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &TreeItems::OnSearch, this);
        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_ITEM_ACTIVATED, &TreeItems::OnActivated, this);
        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_SEL_CHANGED, &TreeItems::OnSelect, this);
        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_ITEM_MENU, &TreeItems::OnTreeItemMenu, this);
        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_ITEM_GETTOOLTIP, &TreeItems::ShowTreeTooltip, this);

        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_BEGIN_DRAG, &TreeItems::OnBeginDrag, this);
        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_END_DRAG, &TreeItems::OnEndDrag, this);
        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_ITEM_GETTOOLTIP, &TreeItems::ShowTreeTooltip, this);
        m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_DELETE_ITEM, &TreeItems::OnTreeRemove, this);

        // m_TreeCtrl->Bind(wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK, wxTreeEventHandler( wxTreeItemsPanel::OnMenu ), NULL,
        // this
        // );
    }

    TreeItems::~TreeItems()
    {
        // Pour éviter les plantages lorsque l'on ferme l'application.
        m_TreeCtrl->DeleteAllItems();
        // m_TreeCtrl->ClearEventHashTable();
    }

    void TreeItems::OnMenu(wxTreeEvent &event)
    {}

    void TreeItems::ShowTreeTooltip(wxTreeEvent &event)
    {
        auto item_tree = event.GetItem();

        std::stringstream ss;

        if(item_tree.IsOk())
        {
            auto key = GetKey(item_tree);
            auto item = ma::Item::GetItem(key);

            //        if(ma::Item::HasItem(ma::Item::ms_KeyClassInfoManager))
            //        {
            //            auto cli_manager = ma::Item::GetItem(ma::Item::ms_KeyClassInfoManager);
            //            cli_manager
            //
            //        }

            ss << "ID: " << key << "\n";
            ss << "Nombre d'enfants: " << item->GetCount(ma::Item::SearchMod::LOCAL) << "\n";

            auto vars = item->GetVars();
            ss << "Nombre de variables: " << vars.size() << "\n";

            for(auto &&[key_var, var] : vars)
            {
                std::string member = var->m_Dynamic ? "D" : "M";
                std::string read_only = var->m_ReadOnly ? "R" : "RW";

                ss << "[" << ma::to_string(var->GetTypeInfo().m_ClassName) << "][" << member << "][" << read_only
                   << "] " << ma::to_string(key_var) << " = " << ma::to_string(var->toString()) << "\n";
            }

            // Lister les observations effectuées par l'item.
            if(ma::ClassInfoManager::IsInherit(item, ma::Observer::GetObserverClassName()))
            {
                auto observer = dynamic_cast<ma::Observer *>(item.get());

                MA_ASSERT(observer, L"«observer» ne peux être converti en ma::Observer*.", std::logic_error);

                auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
                if(obs_manager->HasObserver(observer))
                {
                    auto observations = obs_manager->GetObservations(observer);

                    if(!observations.empty())
                    {
                        ss << "\n";
                        ss << "Observations:\n";
                        for(auto &&[key_obs, observation] : observations)
                        {
                            const auto observable = observation->m_ObservableVar->Get();
                            ss << "[" << ma::to_string(observation->m_ObservableVar->GetTypeInfo().m_ClassName)
                               << "] => ";
                            ss << "[" << ma::to_string(observable->GetTypeInfo().m_ClassName)
                               << "] = " << observable->GetKey() << "\n";
                        }
                    }
                }
            }

            ss << std::endl;
        }

        if(!ss.str().empty())
            event.SetToolTip(ss.str());
    }

    ma::Item::key_type TreeItems::GetKey(const wxTreeItemId &item_id) const
    {
        MA_ASSERT(item_id.IsOk(), L"item_id n'est pas valide.", std::invalid_argument);

        auto *item_data = dynamic_cast<ma::wx::TreeItemData *>(m_TreeCtrl->GetItemData(item_id));
        MA_ASSERT(
            item_data, L"Impossible de récupérer les données de l'item sélectionné dans l'arbre.", std::runtime_error);

        auto key = item_data->m_Key;
        MA_ASSERT(ma::Item::HasItem(key),
                  L"La clef associé à l'item de l'arbre, n'est associée à aucun item de ma::Item::ms_Map.",
                  std::runtime_error);

        return key;
    }

    bool TreeItems::HasId(const ma::Item::key_type &key) const
    {
        return m_KeyIdMap.find(key) != m_KeyIdMap.end();
    }

    wxTreeItemId TreeItems::GetId(const ma::Item::key_type &key) const
    {
        auto it_find = m_KeyIdMap.find(key);
        MA_ASSERT(it_find != m_KeyIdMap.end(),
                  L"Aucun identifiant «wxTreeItemId» n'est associé à la clef: " + key,
                  std::invalid_argument);

        return it_find->second;
    }

    void TreeItems::Remove()
    {
        auto item_tree = m_TreeCtrl->GetFocusedItem();
        if(item_tree.IsOk())
        {
            auto key = GetKey(item_tree);
            auto root = MA_ROOT;

            // On ne supprime pas le root de l'application.
            if(key != root->GetKey() && m_ItemsNotRemovable.count(key) == 0)
            {
                auto item = ma::Item::GetItem(key);

                // On empêche de supprimer manuellement les items qui ne sont pas
                // observables
                if(item->IsObservable())
                {
                    auto parent = ma::Item::GetItem(item->GetParentKey());
                    if(parent->AcceptToRemoveChild(key))
                    {
                        // Pas besoin de mettre le current à nulle, car un ItemVar observe l'item qu'il pointe.
                        // Et si celui-ci est désactivé, ItemVar passera sa référence à nulle.
                        ma::Item::EraseItem(key);
                    }
                }
            }
        }
    }

    void TreeItems::OnBeginDrag(wxTreeEvent &event)
    {
        auto item_tree = event.GetItem();

        if(item_tree.IsOk())
        {
            const auto key = GetKey(item_tree);
            wxTextDataObject data_object(key);

            wxDropSource drag_source(data_object, this);
            drag_source.DoDragDrop(true);
        }
    }

    void TreeItems::OnEndDrag(wxTreeEvent &event)
    {}

    void TreeItems::OnAdd(wxCommandEvent &event)
    {
        auto parent_item_tree = m_TreeCtrl->GetFocusedItem();

        if(parent_item_tree.IsOk())
        {
            auto parent_key = GetKey(parent_item_tree);

            // Juste pour des tests
            MA_ASSERT(m_KeyIdMap.count(parent_key),
                      L"La clef du nœud sélectionnée n'est associée à aucun «ma::wxTreeItemData».",
                      std::runtime_error);

            // Faire une RTTI et lister les type possible selon l'item

            auto parent = ma::Item::GetItem(parent_key);

            // On empêche d'ajouter manuellement des items dans un item qui ne soit pas observable.
            if(parent->IsObservable())
            {
                ma::wx::AddItemDialog dialog(parent_key, ma::root()->m_wxGuiAccess->GetMotherFrame());

                dialog.ShowModal();
            }
        }
        event.Skip();
    }

    void TreeItems::OnRemove(wxCommandEvent &event)
    {
        Remove();
        event.Skip();
    }

    void TreeItems::OnSearch(wxCommandEvent &event)
    {
        event.Skip();
    }

    void TreeItems::OnTreeRemove(wxTreeEvent &event)
    {
        // Remove(); // Il ne faut pas appeler Remove() sinon les appels à Remove
        // vont boucler.
        event.Skip();
    }

    void TreeItems::OnActivated(wxTreeEvent &event)
    {
        auto item_tree = event.GetItem();

        if(item_tree.IsOk())
        {
            auto key = GetKey(item_tree);
            auto root = MA_ROOT;

            if(key == root->GetKey() && m_ItemsNotRemovable.count(key) == 1)
                m_Remove->Disable();
            else
                m_Remove->Enable();

            root->m_Current->SetItem(ma::Item::GetItem(key));
        }

        event.Skip();
    }

    void TreeItems::OnSelect(wxTreeEvent &event)
    {
        auto item_tree = event.GetItem();

        if(item_tree.IsOk() && wxGetKeyState(WXK_CONTROL))
        {
            auto key = GetKey(item_tree);
            auto root = MA_ROOT;

            if(key == root->GetKey() && m_ItemsNotRemovable.count(key) == 1)
                m_Remove->Disable();
            else
                m_Remove->Enable();

            root->m_Current->SetItem(ma::Item::GetItem(key));
        }

        event.Skip();
    }

    void TreeItems::OnTreeItemMenu(wxTreeEvent &event)
    {
        PopupMenu(&m_ContextMenu, event.GetPoint());
        // event.Skip();
    }

    void TreeItems::SetItemName(const ma::VarPtr &var, bool end_observation)
    {
        MA_ASSERT(var, L"La variable «var» est nulle.", std::invalid_argument);

        const auto key_item = var->GetItemKey();

        wxTreeItemId id = GetId(key_item);

        MA_ASSERT(id.IsOk(), L"L'id n'est pas valide.", std::invalid_argument);

        auto item = ma::Item::GetItem(key_item);
        if(end_observation)
            m_TreeCtrl->SetItemText(id, item->GetTypeInfo().m_ClassName);
        else
            m_TreeCtrl->SetItemText(id, item->GetTypeInfo().m_ClassName + " : " + var->toString());
    }

    void TreeItems::AddVarObservations(const ma::ItemPtr &item)
    {
        MA_ASSERT(item, L"L'item est nulle.", std::invalid_argument);

        // On observe les variables de l'item que s'il est observable et fini d'être construit.
        if(item->IsObservable() && m_KeyIdMap.find(item->GetKey()) != m_KeyIdMap.end())
        {
            // L'item doit initialiser avant d'ajouter l'observation de la
            // variable.
            if(item->HasVar(ma::Item::Key::Var::GetName()))
            {
                auto obs_manager = MA_ROOT->m_ObservationManager;
                obs_manager->AddObservation<ma::wx::TreeItemsVar, ma::VarVar>(
                    this, item->GetVar(ma::Item::Key::Var::GetName()));
            }
        }
    }

    void TreeItems::BeginObservation(const ma::ObservablePtr &observable)
    {
        auto root = MA_ROOT;

        if(root->m_Current == observable && root->m_Current->GetItem() != nullptr)
        {
            ma::ItemPtr item = root->m_Current->GetItem();
            auto item_key = item->GetKey();

            if(item->IsObservable())
            {
                auto item_id = GetId(item_key);
                m_TreeCtrl->EnsureVisible(item_id);
                m_TreeCtrl->SelectItem(item_id);
            }
        }
        else if(root->m_ClassInfoManager->IsInherit(observable, ma::Var::GetVarClassName()))
        {
            if(observable->GetKey() == ma::Item::Key::Var::GetName())
            {
                SetItemName(std::dynamic_pointer_cast<ma::Var>(observable));
            }
        }
        else if(root->m_ClassInfoManager->IsInherit(observable, ma::Item::GetItemClassName()))
        {
            ma::ItemPtr item = std::dynamic_pointer_cast<ma::Item>(observable);

            MA_ASSERT(item, L"Item est nulle.", std::runtime_error);

            if((m_InitializationState && item->IsObservable()) || !m_InitializationState)
            {
                const auto item_key = item->GetKey();
                wxTreeItemId item_id;
                TreeItemData *item_data = nullptr;

                if(item_key == ma::Item::Key::GetRoot())
                {
                    item_data = new TreeItemData(root->GetKey());
                    item_id = m_TreeCtrl->AddRoot(L"root", -1, -1, item_data);
                }
                else
                {
                    item_data = new TreeItemData(item->GetKey());
                    std::wstring name = item->GetTypeInfo().m_ClassName;

                    const auto parent_item_key{item->GetParentKey()};
                    if(!HasId(parent_item_key))
                    {
                        if(auto opt = ma::Item::HasItemOpt<ObservationManager>(ma::Item::Key::GetObservationManager()))
                        {
                            auto obs_manager{opt.value()};
                            auto parent_item{Item::GetItem(parent_item_key)};

                            MA_ASSERT(
                                parent_item->IsObservable(), L"L'item parent n'est pas observable", std::logic_error);

                            obs_manager->AddObservation<ma::wx::TreeItemsVar, ma::ItemVar>(this, parent_item);
                        }
                    }

                    const auto parent_id = GetId(parent_item_key);
                    item_id = m_TreeCtrl->AppendItem(parent_id, name, -1, -1, item_data);

                    if(!item->IsObservable())
                    {
                        m_TreeCtrl->SetItemTextColour(item_id, *wxLIGHT_GREY);
                        m_ItemsNotRemovable.insert(item->GetKey());
                    }
                }

                MA_ASSERT(item_id.IsOk(), L"L'item créé dans cet arbre n'est pas valide.", std::runtime_error);

                //            // Pour déboguer, c'est temporaire
                //            if(m_KeyIdMap.count(item_key) != 0)
                //            {
                //                std::wcout << L"Le wxTreeItemId existe déjà"
                //                 << L"Clef de l'item: "s << item_key
                //                 << L"Classe de l'item: "s << item->GetTypeInfo().m_ClassName
                //                 << std::endl;
                //            }

                MA_ASSERT(m_KeyIdMap.count(item_key) == 0,
                          L"Le wxTreeItemId existe déjà. Clef de l'item: " + item_key + L"Classe de l'item: " +
                              item->GetTypeInfo().m_ClassName,
                          std::invalid_argument);

                m_KeyIdMap[item_key] = item_id;

                AddVarObservations(item);

                // Si beaucoup d'item sont ajoutés en une fois, il ne faut pas à chaque item
                // s'assurer de sa visibilité. Cela peu ralentir grandement l'application
                // et n'a pas trop de sens.
                // Note: Après réflexion « GetBatchSatate » sera toujours à faux, car le mécanisme d'observation ne
                // s'active que après un EndBatch().
                // if(!observable->GetBatchState() && item->IsObservable())
                //    m_TreeCtrl->EnsureVisible(item_id);

                // Cherchons, si il y a un ordre d'affichage préférentiel des enfants pour cet item.
                // L'ordre sera déterminé selon une variable.
                auto &data = item->GetTypeInfo().m_Data;
                auto it_find = data.find(TypeInfo::Key::GetVarOrderChildrenDisplay());

                // Si l'item ajouté possède déjà des enfants, il faut les observer.
                ma::ItemsOrder children;
                if(it_find != data.end() && !it_find->second.empty())
                {
                    auto compare = ma::CompareItemsString(*it_find->second.begin());
                    children = ma::Sort::SortItems(item, compare, ma::Item::SearchMod::LOCAL);
                }
                else
                    children = item->GetItemsPostOrder(ma::Item::SearchMod::LOCAL);

                for(auto &it : children)
                {
                    if(it->GetEndConstruction())
                    {
                        if(it->IsObservable())
                            root->m_ObservationManager->AddObservation<ma::wx::TreeItemsVar, ma::ItemVar>(this, it);
                        else
                            BeginObservation(it);
                    }
                }
            }
        }
        else
        {
            MA_ASSERT(
                false,
                L"L'observable ne correspond à aucun des éléments observé par cette classe. Peut être que vous avez "
                L"oublié d'indiquer que " +
                    observable->GetTypeInfo().m_ClassName +
                    L" dérive de ma::Item ou de ma::Var via la macro M_CPP_CLASSHIERARCHY. Exemple: "
                    L"M_CPP_CLASSHIERARCHY(wxGuiAccess, Item)",
                std::runtime_error);
        }
    }

    void TreeItems::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(ma::Item::HasItem(ma::Item::Key::GetObservationManager()))
        {
            auto root = MA_ROOT;

            // « data.HasActions(ma::Var::ms_KeyObsEndSetValue) » permet de ne pas afficher à nouveau l'item
            // précédemment sélectionné puis l'item nouvellement sélectionné.
            // Évite aussi des erreurs si l'item n'est plus actif.
            if(data.HasActions(ma::Var::Key::Obs::ms_EndSetValue))
            {
                if(root->m_Current == observable && root->m_Current->Get() != nullptr)
                {
                    ma::ItemPtr item = root->m_Current->GetItem();
                    auto item_key = item->GetKey();
                    // Dans le cas d'un lot d'observation d'item peut ne pas encore
                    // être ajouté à l'arbre. Il faut donc tester si l'identifiant
                    // de la feuille existe.
                    if(item->IsObservable() && HasId(item_key))
                    {
                        auto item_id = GetId(item_key);
                        m_TreeCtrl->EnsureVisible(item_id);
                        m_TreeCtrl->SelectItem(item_id);
                    }
                }
                else if(root->m_ClassInfoManager->IsInherit(observable, ma::StringVar::GetStringVarClassName()))
                    SetItemName(std::dynamic_pointer_cast<ma::StringVar>(observable));
            }

            if(root->m_ClassInfoManager->IsInherit(observable, ma::Item::GetItemClassName()))
            {
                const auto &actions = data.GetOrdered();
                for(const auto &action : actions)
                {
                    if(ma::Item::HasItem(action.second))
                    {
                        auto item = ma::Item::GetItem(action.second);
                        bool has_id = m_KeyIdMap.find(action.second) != m_KeyIdMap.end();

                        if(action.first == ma::Item::Key::Obs::ms_ConstructionEnd && !has_id)
                        {
                            AddVarObservations(item);
                            if(item->IsObservable())
                                root->m_ObservationManager->AddObservation<ma::wx::TreeItemsVar, ma::ItemVar>(this,
                                                                                                              item);
                            else
                                BeginObservation(item);
                        }
                        else if(action.first == ma::Item::Key::Obs::ms_AddItemEnd && !has_id)
                        {
                            // Un item qui n'est pas fini d'être construit ne peut être
                            // observé avant la fin de sa construction. Le parent de
                            // celui-ci déclenchera un évènement identifié par la clef
                            // ms_KeyObsConstructionEnd
                            if(item->GetEndConstruction())
                            {
                                if(item->IsObservable())
                                    root->m_ObservationManager->AddObservation<ma::wx::TreeItemsVar, ma::ItemVar>(this,
                                                                                                                  item);
                                else
                                    BeginObservation(item);
                            }
                        }
                        // On utilise ms_KeyObsRemoveItemBegin car EndObservation a besoin de retrouver l'item à partir
                        // de la clef.
                        else if(action.first ==
                                ma::Item::Key::Obs::ms_RemoveItemBegin) // && m_KeyIdMap.find(action.second) !=
                                                                        // m_KeyIdMap.end())
                        {
                            if(!item->IsObservable() && !m_InitializationState)
                                EndObservation(item);
                        }
                    }
                }
            }
        }
    }

    void TreeItems::EndObservation(const ma::ObservablePtr &observable)
    {
        auto root = MA_ROOT;

        if(root->m_ClassInfoManager->IsInherit(observable, ma::StringVar::GetStringVarClassName()))
        {
            SetItemName(std::dynamic_pointer_cast<ma::StringVar>(observable), true);
        }
        else if(root->m_ClassInfoManager->IsInherit(observable, ma::Item::GetItemClassName()))
        {
            auto item_key = observable->GetKey();
            if(item_key == ma::Item::Key::GetRoot())
            {
                m_TreeCtrl->DeleteAllItems();
                m_KeyIdMap.clear();

                if(auto opt = ma::Item::HasItemOpt<ObservationManager>(ma::Item::Key::GetObservationManager()))
                    opt.value()->RemoveObserver(this);
            }
            else
            {
                // Pour éviter d'essayer de supprimer des TreeItem qui n'existe pas.
                if((m_InitializationState && observable->IsObservable()) || !m_InitializationState)
                {
                    auto item_id = GetId(item_key);
                    // Si un item parent a été supprimé alors tout les enfant de l'arbre
                    // l'ont été aussi.
                    if(item_id.IsOk())
                    {
                        m_TreeCtrl->Delete(item_id);
                    }

                    MA_ASSERT(m_KeyIdMap.find(item_key) != m_KeyIdMap.end(),
                              L"La clef: " + item_key + L" n'est pas référencée dans m_KeyDataMap.",
                              std::invalid_argument);

                    m_KeyIdMap.erase(item_key);
                }
            }
        }
    }

    const ma::TypeInfo &TreeItems::GetwxTreeItemsTypeInfo()
    {
        static const ma::TypeInfo info = {GetwxTreeItemsClassName(), L"Affiche un arbre d'items.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(TreeItems, wx, Observer)

} // namespace ma::wx