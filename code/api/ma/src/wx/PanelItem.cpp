/// \file wx/PanelItem.cpp
/// \author Pontier Pierre
/// \date 2019-12-08
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/Error.h"
#include "ma/Item.h"
#include "ma/Root.h"
#include "ma/wx/PanelItem.h"
#include "ma/wx/Variable.h"
#include "ma/ObservationManager.h"
#include "ma/wx/AddVarDialog.h"

namespace ma::wx
{
    PanelItem::PanelItem(wxWindow *parent,
                         wxWindowID id,
                         const wxPoint &pos,
                         const wxSize &size,
                         long style,
                         const wxString &name):
    wxPanel(parent, id, pos, size, style, name),
    m_ItemVar(ma::root()->m_Current)
    {
        wxBoxSizer *vertical_sizer;
        vertical_sizer = new wxBoxSizer(wxVERTICAL);

        m_ScrolledWindow = new wxScrolledWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL);
        m_ScrolledWindow->SetScrollRate(5, 5);

        m_VerticalSizer = new wxBoxSizer(wxVERTICAL);

        wxBoxSizer *title_sizer;
        title_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_Logo = new wxStaticBitmap(m_ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize(30, 30), 0);
        m_Logo->SetToolTip(M_Tr(L"Logo de l'item"));

        title_sizer->Add(m_Logo, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_ClassName = new wxStaticText(
            m_ScrolledWindow, wxID_ANY, M_Tr(L"Nom de la classe"), wxDefaultPosition, wxSize(-1, -1), 0);
        m_ClassName->Wrap(-1);
        m_ClassName->SetFont(wxFont(wxNORMAL_FONT->GetPointSize(),
                                    wxFONTFAMILY_DEFAULT,
                                    wxFONTSTYLE_NORMAL,
                                    wxFONTWEIGHT_NORMAL,
                                    false,
                                    wxEmptyString));
        m_ClassName->SetToolTip(M_Tr(L"Description de la classe\n"));

        title_sizer->Add(m_ClassName, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_CountChild = new wxStaticText(m_ScrolledWindow, wxID_ANY, M_Tr(L"0"), wxDefaultPosition, wxDefaultSize, 0);
        m_CountChild->Wrap(-1);
        m_CountChild->SetToolTip(M_Tr(L"Nom de l'item."));

        title_sizer->Add(m_CountChild, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_Add = new wxBitmapButton(
            m_ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_Add->SetBitmap(wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON));
        m_Add->SetMinSize(wxSize(30, 30));

        title_sizer->Add(m_Add, 0, wxALL, 5);

        m_VerticalSizer->Add(title_sizer, 0, wxEXPAND, 5);

        wxBoxSizer *key_sizer;
        key_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_TextKey = new wxStaticText(m_ScrolledWindow, wxID_ANY, M_Tr(L"ID"), wxDefaultPosition, wxDefaultSize, 0);
        m_TextKey->Wrap(-1);
        key_sizer->Add(m_TextKey, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_Key = new wxStaticText(m_ScrolledWindow,
                                 wxID_ANY,
                                 M_Tr(L"{00000000-0000-0000-0000-000000000001}"),
                                 wxDefaultPosition,
                                 wxSize(292, -1),
                                 0);
        m_Key->Wrap(-1);
        m_Key->SetToolTip(M_Tr(L"Clef de l'item.\nClic droit pour copier l'identifiant."));

        // key_sizer->Add(m_Key, 1, wxALIGN_CENTER_VERTICAL | wxALL | wxALIGN_RIGHT, 5);
        key_sizer->Add(m_Key, 1, wxALL, 5);

        m_VerticalSizer->Add(key_sizer, 0, wxEXPAND, 5);

        m_ScrolledWindow->SetSizer(m_VerticalSizer);
        m_ScrolledWindow->Layout();
        m_VerticalSizer->Fit(m_ScrolledWindow);
        vertical_sizer->Add(m_ScrolledWindow, 1, wxEXPAND | wxALL, 0);

        this->SetSizer(vertical_sizer);
        this->Layout();

        ma::root()->m_ObservationManager->AddObservation<PanelItemVar, ma::VarVar>(this, m_ItemVar);

        // Connect Events
        m_ClassName->Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(PanelItem::OnCopyClassName), nullptr, this);
        m_Add->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(PanelItem::OnAddVar), nullptr, this);
        m_Key->Connect(wxEVT_RIGHT_UP, wxMouseEventHandler(PanelItem::OnCopyId), nullptr, this);
    }

    PanelItem::~PanelItem()
    {
        // Disconnect Events
        m_ClassName->Disconnect(wxEVT_RIGHT_UP, wxMouseEventHandler(PanelItem::OnCopyClassName), nullptr, this);
        m_Add->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(PanelItem::OnAddVar), nullptr, this);
        m_Key->Disconnect(wxEVT_RIGHT_UP, wxMouseEventHandler(PanelItem::OnCopyId), nullptr, this);
    }

    void PanelItem::OnCopyClassName(wxMouseEvent &event)
    {
        if(m_ItemVar && m_ItemVar->Get())
            ma::root()->m_wxGuiAccess->SendTextToClipboard(m_ItemVar->GetItem()->GetTypeInfo().m_ClassName);

        event.Skip();
    }

    void PanelItem::OnCopyId(wxMouseEvent &event)
    {
        if(m_ItemVar && m_ItemVar->Get())
            ma::root()->m_wxGuiAccess->SendTextToClipboard(m_ItemVar->GetItem()->GetKey());

        event.Skip();
    }

    void PanelItem::OnAddVar(wxCommandEvent &event)
    {
        if(m_ItemVar)
        {
            auto item = m_ItemVar->Get();
            if(item && item->IsObservable())
            {
                ma::wx::AddVarDialog dialog(item->GetKey(), ma::root()->m_wxGuiAccess->GetMotherFrame());

                dialog.ShowModal();
            }
        }
        event.Skip();
    }

    void PanelItem::SetLabels()
    {
        if(m_ItemVar && m_ItemVar->Get())
        {
            auto item = m_ItemVar->GetItem();
            auto type_info = item->GetTypeInfo();
            m_ClassName->SetLabel(type_info.m_ClassName);

            auto tool_tip = type_info.m_Description;
            for(auto &&[key, value] : type_info.m_Data)
                tool_tip += L"\n" + key + L" : " + ma::toString(value);

            m_ClassName->SetToolTip(tool_tip);
            m_Key->SetLabel(item->GetKey());
            m_CountChild->SetLabel(std::to_string(item->GetCount(ma::Item::SearchMod::LOCAL)));
        }
        else
        {
            m_ClassName->SetLabel("");
            m_ClassName->SetToolTip("");
            m_Key->SetLabel("");
            m_CountChild->SetLabel("0");
        }
    }

    void PanelItem::ClearPanels()
    {
        for(auto panel : m_Panels)
            panel.second->Destroy();

        m_Panels.clear();

        m_ScrolledWindow->Layout();
        this->Layout();
    }

    void PanelItem::SetPanels()
    {
        if(m_ItemVar && m_ItemVar->Get())
        {
            auto item = m_ItemVar->GetItem();

            for(const auto &it : item->GetVars())
            {
                if(m_Panels.find(it.first) == m_Panels.end())
                {
                    auto rtti = ma::root()->m_RttiManager;
                    MA_ASSERT(rtti, L"La rtti est nulle.", std::runtime_error);

                    wxWindow *window = rtti->CreatewxVar(it.first, item->GetKey(), m_ScrolledWindow);
                    m_VerticalSizer->Add(window, 0, wxEXPAND, 5);
                    m_Panels[it.first] = window;
                }
            }
            m_ScrolledWindow->Layout();
            this->Layout();
        }
    }

    void PanelItem::AddPanel(const ma::Var::key_type &key)
    {
        auto it_find = m_Panels.find(key);

        if(it_find == m_Panels.end())
        {
            if(m_ItemVar && m_ItemVar->Get())
            {
                auto item = m_ItemVar->GetItem();

                MA_ASSERT(item, L"AddPanel est appelé alors que item est nulle.", std::runtime_error);

                auto var = item->GetVar(key);

                auto rtti = ma::root()->m_RttiManager;
                MA_ASSERT(rtti, L"La rtti est nulle.", std::runtime_error);

                wxWindow *window = rtti->CreatewxVar(key, item->GetKey(), m_ScrolledWindow);
                m_VerticalSizer->Add(window, 0, wxEXPAND, 5);
                m_Panels[key] = window;
            }
        }
    }

    void PanelItem::RemovePanel(const ma::Var::key_type &key)
    {
        auto it_find = m_Panels.find(key);

        if(it_find != m_Panels.end())
        {
            it_find->second->Destroy();
            m_Panels.erase(it_find);
        }
    }

    void PanelItem::BeginObservation(const ma::ObservablePtr &observable)
    {
        auto root = MA_ROOT;

        if(m_ItemVar == observable)
        {
            SetLabels();
            ClearPanels();
            SetPanels();
        }
        else
        {
            //            if(root->classinfo->IsInherit(observable, ma::Var::GetVarClassName()))
            //            {}
            //            else if(root->classinfo->IsInherit(observable, ma::Item::GetItemClassName()))
            //            {}
        }
    }

    void PanelItem::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        auto root = MA_ROOT;

        //    M_MSG(function_name)
        //    M_MSG("Observable: {" + observable->GetTypeInfo().m_ClassName + ", " + observable->GetKey() + "}")
        //    for(auto it : data.GetOrdered())
        //    {
        //        M_MSG("{" + it.first + ", " + it.second + "}")
        //    }
        for(const auto &action : data.GetOrdered())
        {
            if(m_ItemVar == observable)
            {
                if(action.first == ma::Var::Key::Obs::ms_BeginSetValue)
                {
                    // On prend la première action, car c'est elle qui possède la clef du précédent item.
                    // Peut-être que ça valait mieux d'utiliser une variable de classe dans wxPaneItem pour stocker la
                    // précédente clef de l'item.
                    auto previous_key_item = ma::Var::GetKeyValue(action.second).second;

                    if(ma::Item::HasItem(previous_key_item))
                    {
                        auto item = ma::Item::GetItem(previous_key_item);
                        if(item && root->m_ObservationManager->HasObservation(this, item))
                            root->m_ObservationManager->RemoveObservation(this, item);
                    }

                    ClearPanels();
                }
                else if(action.first == ma::Var::Key::Obs::ms_EndSetValue)
                {
                    if(!root->m_ObservationManager->HasObservation(this, m_ItemVar))
                        root->m_ObservationManager->AddObservation<PanelItemVar, ma::VarVar>(this, m_ItemVar);

                    auto item = m_ItemVar->Get();

                    if(item && !root->m_ObservationManager->HasObservation(this, item))
                    {
                        if(item->IsObservable())
                            root->m_ObservationManager->AddObservation<PanelItemVar, ma::ItemVar>(this, item);
                    }

                    SetLabels();
                    SetPanels();
                }
            }
            else
            {
                if(root->m_ClassInfoManager->IsInherit(observable, ma::Var::GetVarClassName()))
                {}
                else if(root->m_ClassInfoManager->IsInherit(observable, ma::Item::GetItemClassName()))
                {
                    if(action.first == ma::Item::Key::Obs::ms_AddItemEnd ||
                       action.first == ma::Item::Key::Obs::ms_RemoveItemEnd)
                    {
                        SetLabels();
                    }
                    else if(action.first == ma::Item::Key::Obs::ms_AddVar)
                    {
                        AddPanel(action.second);

                        m_ScrolledWindow->Layout();
                        this->Layout();
                    }
                    else if(action.first == ma::Item::Key::Obs::ms_RemoveVarEnd)
                    {
                        RemovePanel(action.second);

                        m_ScrolledWindow->Layout();
                        this->Layout();
                    }
                }
            }
        }
    }

    void PanelItem::EndObservation(const ma::ObservablePtr &observable)
    {
        if(m_ItemVar == observable)
        {
            m_ItemVar = nullptr;
            SetLabels();
            ClearPanels();
        }
        else
        {
            auto root = MA_ROOT;

            if(root->m_ClassInfoManager->IsInherit(observable, ma::Var::GetVarClassName()))
            {}
            // wxPanelItem n'observe qu'un seul item à la fois.
            // S'il reçoit une fin d'observation provenant d'un item, c'est forcément celui qu'il observe.
            else if(root->m_ClassInfoManager->IsInherit(observable, ma::Item::GetItemClassName()))
            {
                ClearPanels();
            }
        }
    }

    const ma::TypeInfo &PanelItem::GetwxPanelItemTypeInfo()
    {
        static const ma::TypeInfo info = {GetwxPanelItemClassName(), L"Affiche les propriétés d'un item", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(PanelItem, wx, Observer)
} // namespace ma::wx