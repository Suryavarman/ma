///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Dec  8 2019)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/bmpbuttn.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/scrolwin.h>
#include <wx/panel.h>
#include <wx/statline.h>
#include <wx/textctrl.h>
#include <wx/valgen.h>
#include <wx/filepicker.h>
#include <wx/spinctrl.h>
#include <wx/listbox.h>
#include <wx/html/htmlwin.h>
#include <wx/dialog.h>
#include <wx/grid.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class wxPanelItem
///////////////////////////////////////////////////////////////////////////////
class wxPanelItem : public wxPanel
{
	private:

	protected:
		wxScrolledWindow* m_ScrolledWindow;
		wxStaticBitmap* m_Logo;
		wxStaticText* m_ClassName;
		wxStaticText* m_CountChild;
		wxBitmapButton* m_Add;
		wxStaticText* m_TextKey;
		wxStaticText* m_Key;

		// Virtual event handlers, overide them in your derived class
		virtual void OnCopyClassName( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnAddVar( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCopyId( wxMouseEvent& event ) { event.Skip(); }


	public:

		wxPanelItem( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxPanelItem();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVarBaseItem
///////////////////////////////////////////////////////////////////////////////
class wxVarBaseItem : public wxPanel
{
	private:

	protected:
		wxStaticText* m_Name;
		wxStaticLine* m_staticline;
		wxBitmapButton* m_bpButton1;
		wxTextCtrl* m_TextCtrlItem;
		wxBitmapButton* m_ButtonSetItemCourant;
		wxBitmapButton* m_SearchItem;
		wxBitmapButton* m_ResetButton;

	public:

		wxVarBaseItem( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,80 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVarBaseItem();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVarDefine
///////////////////////////////////////////////////////////////////////////////
class wxVarDefine : public wxPanel
{
	private:

	protected:
		wxStaticText* m_Name;
		wxStaticLine* m_staticline;

	public:

		wxVarDefine( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,40 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVarDefine();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVar
///////////////////////////////////////////////////////////////////////////////
class wxVar : public wxPanel
{
	private:

	protected:
		wxBoxSizer* m_VerticalSizer;
		wxBoxSizer* m_TopHorizontalSizer;
		wxStaticText* m_Name;
		wxStaticBitmap* m_Warning;
		wxBitmapButton* m_ResetButton;
		wxBitmapButton* m_DeleteButton;
		wxBitmapButton* m_DisplayInstanceInfosButton;
		wxBoxSizer* m_BottomHorizontalSizer;
		wxTextCtrl* m_TextCtrlVar;
		wxBoxSizer* m_InstanceInfosVerticalSizer;

		// Virtual event handlers, overide them in your derived class
		virtual void OnCopyVarKey( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnReset( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDelete( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDisplayInstanceInfos( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextEnter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextMaxLen( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextURL( wxTextUrlEvent& event ) { event.Skip(); }


	public:

		wxVar( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,-1 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVar();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVarDirectory
///////////////////////////////////////////////////////////////////////////////
class wxVarDirectory : public wxPanel
{
	private:

	protected:
		wxBoxSizer* m_VerticalSizer;
		wxBoxSizer* m_TopHorizontalSizer;
		wxStaticText* m_Name;
		wxStaticBitmap* m_Warning;
		wxBitmapButton* m_ResetButton;
		wxBitmapButton* m_DeleteButton;
		wxBoxSizer* m_BottomHorizontalSizer;
		wxTextCtrl* m_TextCtrlVar;
		wxButton* m_BrowserButton;

		// Virtual event handlers, overide them in your derived class
		virtual void OnCopyVarKey( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnReset( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDelete( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextEnter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextMaxLen( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextURL( wxTextUrlEvent& event ) { event.Skip(); }
		virtual void OnOpenExplorer( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxVarDirectory( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,-1 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVarDirectory();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVarDirectory_A
///////////////////////////////////////////////////////////////////////////////
class wxVarDirectory_A : public wxPanel
{
	private:

	protected:
		wxStaticText* m_Name;
		wxStaticBitmap* m_Warning;
		wxBitmapButton* m_ResetButton;
		wxBitmapButton* m_DeleteButton;
		wxDirPickerCtrl* m_DirPicker;

		// Virtual event handlers, overide them in your derived class
		virtual void OnCopyVarKey( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnReset( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDelete( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxVarDirectory_A( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,80 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVarDirectory_A();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVarDouble
///////////////////////////////////////////////////////////////////////////////
class wxVarDouble : public wxPanel
{
	private:

	protected:
		wxStaticText* m_Name;
		wxStaticLine* m_staticline;
		wxSpinCtrlDouble* m_spinCtrlDouble;
		wxBitmapButton* m_ResetButton;

		// Virtual event handlers, overide them in your derived class
		virtual void OnSpinCtrlDouble( wxSpinDoubleEvent& event ) { event.Skip(); }
		virtual void OnReset( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxVarDouble( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,80 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVarDouble();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVarInt
///////////////////////////////////////////////////////////////////////////////
class wxVarInt : public wxPanel
{
	private:

	protected:
		wxStaticText* m_Name;
		wxStaticLine* m_staticline;
		wxSpinCtrl* m_spinCtrl;
		wxBitmapButton* m_ResetButton;

		// Virtual event handlers, overide them in your derived class
		virtual void OnSpinCtrl( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnReset( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxVarInt( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 332,80 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVarInt();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxAddItemDialog
///////////////////////////////////////////////////////////////////////////////
class wxAddItemDialog : public wxDialog
{
	private:

	protected:
		wxListBox* m_TypesListBox;
		wxHtmlWindow* m_HTMLWin;
		wxButton* m_ValidateButton;
		wxButton* m_CancelButton;

		// Virtual event handlers, overide them in your derived class
		virtual void OnSelected( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAdd( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCancel( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxAddItemDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 758,523 ), long style = wxDEFAULT_DIALOG_STYLE );
		~wxAddItemDialog();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxAddVarDialog
///////////////////////////////////////////////////////////////////////////////
class wxAddVarDialog : public wxDialog
{
	private:

	protected:
		wxStaticText* m_KeyStaticText;
		wxTextCtrl* m_KeyTextCtrl;
		wxListBox* m_TypesListBox;
		wxHtmlWindow* m_HTMLWin;
		wxButton* m_ValidateButton;
		wxButton* m_CancelButton;

		// Virtual event handlers, overide them in your derived class
		virtual void OnText( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextEnter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSelected( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAdd( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCancel( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxAddVarDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 758,523 ), long style = wxDEFAULT_DIALOG_STYLE );
		~wxAddVarDialog();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxPanelResourcesExplorer
///////////////////////////////////////////////////////////////////////////////
class wxPanelResourcesExplorer : public wxPanel
{
	private:

	protected:
		wxButton* m_Dir1Button;
		wxStaticText* m_Dir1Separator;
		wxButton* m_Dir2Button;
		wxStaticText* m_Dir2Separator;
		wxStaticText* m_Dir3;
		wxScrolledWindow* m_ScrolledWindow;
		wxGridSizer* m_GridSizer;
		wxBitmapButton* m_FileImageButton;
		wxStaticText* m_FileNameStaticText;
		wxBitmapButton* m_FileImageButton1;
		wxStaticText* m_FileNameStaticText1;

	public:

		wxPanelResourcesExplorer( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxPanelResourcesExplorer();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxPanelResourceItem
///////////////////////////////////////////////////////////////////////////////
class wxPanelResourceItem : public wxPanel
{
	private:

	protected:
		wxBitmapButton* m_FileImageButton;
		wxStaticText* m_FileNameStaticText;

	public:

		wxPanelResourceItem( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 100,100 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxPanelResourceItem();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxVarIInfo
///////////////////////////////////////////////////////////////////////////////
class wxVarIInfo : public wxPanel
{
	private:

	protected:
		wxBoxSizer* m_InstanceInfosVerticalSizer;
		wxGrid* m_InstanceInfos;
		wxBoxSizer* m_InstanceInfoHorizontalSizer;
		wxBitmapButton* m_InstanceInfoAdd;
		wxBitmapButton* m_InstanceInfoRemove;

		// Virtual event handlers, overide them in your derived class
		virtual void OnSetKeyValue( wxGridEvent& event ) { event.Skip(); }
		virtual void OnAddRow( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRemoveSelected( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxVarIInfo( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~wxVarIInfo();

};

