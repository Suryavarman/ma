# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.9.0 Dec  8 2019)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.html
import wx.grid

import gettext
_ = gettext.gettext

###########################################################################
## Class wxPanelItem
###########################################################################

class wxPanelItem ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,300 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		self.m_ScrolledWindow = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.VSCROLL )
		self.m_ScrolledWindow.SetScrollRate( 5, 5 )
		m_VerticalSizer = wx.BoxSizer( wx.VERTICAL )

		title_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Logo = wx.StaticBitmap( self.m_ScrolledWindow, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 30,30 ), 0 )
		self.m_Logo.SetToolTip( _(u"Logo de l'item") )

		title_sizer.Add( self.m_Logo, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_ClassName = wx.StaticText( self.m_ScrolledWindow, wx.ID_ANY, _(u"Nom de la classe"), wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		self.m_ClassName.Wrap( -1 )

		self.m_ClassName.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_ClassName.SetToolTip( _(u"Description de la classe\nTOTO") )

		title_sizer.Add( self.m_ClassName, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_CountChild = wx.StaticText( self.m_ScrolledWindow, wx.ID_ANY, _(u"0"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_CountChild.Wrap( -1 )

		self.m_CountChild.SetToolTip( _(u"Nom de l'item.") )

		title_sizer.Add( self.m_CountChild, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_Add = wx.BitmapButton( self.m_ScrolledWindow, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_Add.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_PLUS, wx.ART_BUTTON ) )
		self.m_Add.SetMinSize( wx.Size( 30,30 ) )

		title_sizer.Add( self.m_Add, 0, wx.ALL, 5 )


		m_VerticalSizer.Add( title_sizer, 0, wx.EXPAND, 5 )

		key_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_TextKey = wx.StaticText( self.m_ScrolledWindow, wx.ID_ANY, _(u"ID"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_TextKey.Wrap( -1 )

		key_sizer.Add( self.m_TextKey, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_Key = wx.StaticText( self.m_ScrolledWindow, wx.ID_ANY, _(u"{00000000-0000-0000-0000-000000000001}"), wx.DefaultPosition, wx.Size( 292,-1 ), 0 )
		self.m_Key.Wrap( -1 )

		self.m_Key.SetToolTip( _(u"Clef de l'item.") )

		key_sizer.Add( self.m_Key, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.ALIGN_RIGHT, 5 )


		m_VerticalSizer.Add( key_sizer, 0, wx.EXPAND, 5 )


		self.m_ScrolledWindow.SetSizer( m_VerticalSizer )
		self.m_ScrolledWindow.Layout()
		m_VerticalSizer.Fit( self.m_ScrolledWindow )
		vertical_sizer.Add( self.m_ScrolledWindow, 1, wx.EXPAND |wx.ALL, 0 )


		self.SetSizer( vertical_sizer )
		self.Layout()

		# Connect Events
		self.m_ClassName.Bind( wx.EVT_RIGHT_UP, self.OnCopyClassName )
		self.m_Add.Bind( wx.EVT_BUTTON, self.OnAddVar )
		self.m_Key.Bind( wx.EVT_RIGHT_UP, self.OnCopyId )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnCopyClassName( self, event ):
		event.Skip()

	def OnAddVar( self, event ):
		event.Skip()

	def OnCopyId( self, event ):
		event.Skip()


###########################################################################
## Class wxVarBaseItem
###########################################################################

class wxVarBaseItem ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,80 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		horizontal_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Name = wx.StaticText( self, wx.ID_ANY, _(u"Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Name.Wrap( -1 )

		self.m_Name.SetToolTip( _(u"Infomation sur la variable") )

		horizontal_sizer.Add( self.m_Name, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_staticline = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		horizontal_sizer.Add( self.m_staticline, 0, wx.EXPAND |wx.ALL, 5 )

		self.m_bpButton1 = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_bpButton1.SetBitmap( wx.NullBitmap )
		self.m_bpButton1.SetToolTip( _(u"Image, logo, aperçu, ... de l'item sélectionné.") )

		horizontal_sizer.Add( self.m_bpButton1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		self.m_TextCtrlItem = wx.TextCtrl( self, wx.ID_ANY, _(u"None"), wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.m_TextCtrlItem.Enable( False )
		self.m_TextCtrlItem.SetToolTip( _(u"L'item sélectionné") )

		vertical_sizer.Add( self.m_TextCtrlItem, 0, wx.ALL|wx.EXPAND, 5 )

		horinzontal2_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_ButtonSetItemCourant = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_ButtonSetItemCourant.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_GO_BACK, wx.ART_MENU ) )
		self.m_ButtonSetItemCourant.SetToolTip( _(u"Assigner l'item courant.") )

		horinzontal2_sizer.Add( self.m_ButtonSetItemCourant, 0, wx.ALL, 5 )

		self.m_SearchItem = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_SearchItem.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_FIND, wx.ART_BUTTON ) )
		self.m_SearchItem.SetToolTip( _(u"Rechercher un item à affecter.") )

		horinzontal2_sizer.Add( self.m_SearchItem, 0, wx.ALL, 5 )

		self.m_ResetButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_ResetButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_UNDO, wx.ART_BUTTON ) )
		self.m_ResetButton.SetToolTip( _(u"Remet la valeur par défaut.") )

		horinzontal2_sizer.Add( self.m_ResetButton, 0, wx.ALL, 5 )


		vertical_sizer.Add( horinzontal2_sizer, 1, wx.EXPAND, 5 )


		horizontal_sizer.Add( vertical_sizer, 1, wx.EXPAND, 5 )


		self.SetSizer( horizontal_sizer )
		self.Layout()

	def __del__( self ):
		pass


###########################################################################
## Class wxVarDefine
###########################################################################

class wxVarDefine ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,40 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		horizontal_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Name = wx.StaticText( self, wx.ID_ANY, _(u"Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Name.Wrap( -1 )

		self.m_Name.SetToolTip( _(u"Infomation sur la variable") )

		horizontal_sizer.Add( self.m_Name, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_staticline = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		horizontal_sizer.Add( self.m_staticline, 0, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( horizontal_sizer )
		self.Layout()

	def __del__( self ):
		pass


###########################################################################
## Class wxVar
###########################################################################

class wxVar ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,-1 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		m_VerticalSizer = wx.BoxSizer( wx.VERTICAL )

		m_TopHorizontalSizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Name = wx.StaticText( self, wx.ID_ANY, _(u"Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Name.Wrap( -1 )

		self.m_Name.SetToolTip( _(u"Infomation sur la variable") )

		m_TopHorizontalSizer.Add( self.m_Name, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_Warning = wx.StaticBitmap( self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ERROR, wx.ART_TOOLBAR ), wx.DefaultPosition, wx.DefaultSize, 0 )
		m_TopHorizontalSizer.Add( self.m_Warning, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_ResetButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_ResetButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_UNDO, wx.ART_BUTTON ) )
		self.m_ResetButton.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_ResetButton.SetToolTip( _(u"Remet la valeur par défaut.") )

		m_TopHorizontalSizer.Add( self.m_ResetButton, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_DeleteButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_DeleteButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_BUTTON ) )
		self.m_DeleteButton.SetBitmapPosition( wx.BOTTOM )
		self.m_DeleteButton.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_DeleteButton.SetToolTip( _(u"Supprime la variable de l'item.") )

		m_TopHorizontalSizer.Add( self.m_DeleteButton, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_DisplayInstanceInfosButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_DisplayInstanceInfosButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_LIST_VIEW, wx.ART_BUTTON ) )
		self.m_DisplayInstanceInfosButton.SetToolTip( _(u"Affiche ou cache la grille d'édition des informations de la varible.") )

		m_TopHorizontalSizer.Add( self.m_DisplayInstanceInfosButton, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		m_VerticalSizer.Add( m_TopHorizontalSizer, 1, wx.EXPAND, 5 )

		m_BottomHorizontalSizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_TextCtrlVar = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_AUTO_URL|wx.TE_PROCESS_ENTER )
		self.m_TextCtrlVar.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		self.m_TextCtrlVar.SetToolTip( _(u"Valeur de la chaîne de caractères.") )

		m_BottomHorizontalSizer.Add( self.m_TextCtrlVar, 1, wx.ALL|wx.EXPAND, 5 )


		m_VerticalSizer.Add( m_BottomHorizontalSizer, 0, wx.EXPAND, 5 )

		m_InstanceInfosVerticalSizer = wx.BoxSizer( wx.VERTICAL )


		m_VerticalSizer.Add( m_InstanceInfosVerticalSizer, 0, wx.EXPAND, 5 )


		self.SetSizer( m_VerticalSizer )
		self.Layout()

		# Connect Events
		self.m_Name.Bind( wx.EVT_RIGHT_UP, self.OnCopyVarKey )
		self.m_ResetButton.Bind( wx.EVT_BUTTON, self.OnReset )
		self.m_DeleteButton.Bind( wx.EVT_BUTTON, self.OnDelete )
		self.m_DisplayInstanceInfosButton.Bind( wx.EVT_BUTTON, self.OnDisplayInstanceInfos )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT, self.OnTextChange )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT_ENTER, self.OnTextEnter )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT_MAXLEN, self.OnTextMaxLen )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT_URL, self.OnTextURL )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnCopyVarKey( self, event ):
		event.Skip()

	def OnReset( self, event ):
		event.Skip()

	def OnDelete( self, event ):
		event.Skip()

	def OnDisplayInstanceInfos( self, event ):
		event.Skip()

	def OnTextChange( self, event ):
		event.Skip()

	def OnTextEnter( self, event ):
		event.Skip()

	def OnTextMaxLen( self, event ):
		event.Skip()

	def OnTextURL( self, event ):
		event.Skip()


###########################################################################
## Class wxVarDirectory
###########################################################################

class wxVarDirectory ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,-1 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		m_VerticalSizer = wx.BoxSizer( wx.VERTICAL )

		m_TopHorizontalSizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Name = wx.StaticText( self, wx.ID_ANY, _(u"Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Name.Wrap( -1 )

		self.m_Name.SetToolTip( _(u"Infomation sur la variable") )

		m_TopHorizontalSizer.Add( self.m_Name, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_Warning = wx.StaticBitmap( self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ERROR, wx.ART_TOOLBAR ), wx.DefaultPosition, wx.DefaultSize, 0 )
		m_TopHorizontalSizer.Add( self.m_Warning, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_ResetButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_ResetButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_UNDO, wx.ART_BUTTON ) )
		self.m_ResetButton.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_ResetButton.SetToolTip( _(u"Remet la valeur par défaut.") )

		m_TopHorizontalSizer.Add( self.m_ResetButton, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_DeleteButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_DeleteButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_BUTTON ) )
		self.m_DeleteButton.SetBitmapPosition( wx.BOTTOM )
		self.m_DeleteButton.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_DeleteButton.SetToolTip( _(u"Supprime la variable de l'item.") )

		m_TopHorizontalSizer.Add( self.m_DeleteButton, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		m_VerticalSizer.Add( m_TopHorizontalSizer, 1, wx.EXPAND, 5 )

		m_BottomHorizontalSizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_TextCtrlVar = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_AUTO_URL|wx.TE_PROCESS_ENTER )
		self.m_TextCtrlVar.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		self.m_TextCtrlVar.SetToolTip( _(u"Valeur de la chaîne de caractères.") )

		m_BottomHorizontalSizer.Add( self.m_TextCtrlVar, 1, wx.EXPAND|wx.ALL, 5 )

		self.m_BrowserButton = wx.Button( self, wx.ID_ANY, _(u"..."), wx.DefaultPosition, wx.Size( 30,30 ), wx.BU_EXACTFIT )
		m_BottomHorizontalSizer.Add( self.m_BrowserButton, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 3 )


		m_VerticalSizer.Add( m_BottomHorizontalSizer, 0, wx.EXPAND, 5 )


		self.SetSizer( m_VerticalSizer )
		self.Layout()

		# Connect Events
		self.m_Name.Bind( wx.EVT_RIGHT_UP, self.OnCopyVarKey )
		self.m_ResetButton.Bind( wx.EVT_BUTTON, self.OnReset )
		self.m_DeleteButton.Bind( wx.EVT_BUTTON, self.OnDelete )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT, self.OnTextChange )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT_ENTER, self.OnTextEnter )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT_MAXLEN, self.OnTextMaxLen )
		self.m_TextCtrlVar.Bind( wx.EVT_TEXT_URL, self.OnTextURL )
		self.m_BrowserButton.Bind( wx.EVT_BUTTON, self.OnOpenExplorer )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnCopyVarKey( self, event ):
		event.Skip()

	def OnReset( self, event ):
		event.Skip()

	def OnDelete( self, event ):
		event.Skip()

	def OnTextChange( self, event ):
		event.Skip()

	def OnTextEnter( self, event ):
		event.Skip()

	def OnTextMaxLen( self, event ):
		event.Skip()

	def OnTextURL( self, event ):
		event.Skip()

	def OnOpenExplorer( self, event ):
		event.Skip()


###########################################################################
## Class wxVarDirectory_A
###########################################################################

class wxVarDirectory_A ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,80 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		horinzontal_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Name = wx.StaticText( self, wx.ID_ANY, _(u"Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Name.Wrap( -1 )

		self.m_Name.SetToolTip( _(u"Infomation sur la variable") )

		horinzontal_sizer.Add( self.m_Name, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_Warning = wx.StaticBitmap( self, wx.ID_ANY, wx.ArtProvider.GetBitmap( wx.ART_ERROR, wx.ART_TOOLBAR ), wx.DefaultPosition, wx.DefaultSize, 0 )
		horinzontal_sizer.Add( self.m_Warning, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_ResetButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_ResetButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_UNDO, wx.ART_BUTTON ) )
		self.m_ResetButton.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_ResetButton.SetToolTip( _(u"Remet la valeur par défaut.") )

		horinzontal_sizer.Add( self.m_ResetButton, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_DeleteButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_DeleteButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_BUTTON ) )
		self.m_DeleteButton.SetBitmapPosition( wx.BOTTOM )
		self.m_DeleteButton.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_DeleteButton.SetToolTip( _(u"Supprime la variable de l'item.") )

		horinzontal_sizer.Add( self.m_DeleteButton, 0, wx.ALL, 5 )


		vertical_sizer.Add( horinzontal_sizer, 1, wx.EXPAND, 5 )

		self.m_DirPicker = wx.DirPickerCtrl( self, wx.ID_ANY, u"/home/gandi", _(u"Sélectionner un dossier de ressoruces"), wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE|wx.DIRP_DIR_MUST_EXIST|wx.DIRP_SMALL|wx.DIRP_USE_TEXTCTRL )
		vertical_sizer.Add( self.m_DirPicker, 0, wx.ALL|wx.EXPAND, 4 )


		self.SetSizer( vertical_sizer )
		self.Layout()

		# Connect Events
		self.m_Name.Bind( wx.EVT_RIGHT_UP, self.OnCopyVarKey )
		self.m_ResetButton.Bind( wx.EVT_BUTTON, self.OnReset )
		self.m_DeleteButton.Bind( wx.EVT_BUTTON, self.OnDelete )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnCopyVarKey( self, event ):
		event.Skip()

	def OnReset( self, event ):
		event.Skip()

	def OnDelete( self, event ):
		event.Skip()


###########################################################################
## Class wxVarDouble
###########################################################################

class wxVarDouble ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,80 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		horizontal_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Name = wx.StaticText( self, wx.ID_ANY, _(u"Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Name.Wrap( -1 )

		self.m_Name.SetToolTip( _(u"Infomation sur la variable") )

		horizontal_sizer.Add( self.m_Name, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_staticline = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		horizontal_sizer.Add( self.m_staticline, 0, wx.EXPAND |wx.ALL, 5 )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		self.m_spinCtrlDouble = wx.SpinCtrlDouble( self, wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 100, 0, 0.01 )
		self.m_spinCtrlDouble.SetDigits( 2 )
		vertical_sizer.Add( self.m_spinCtrlDouble, 0, wx.ALL|wx.EXPAND, 5 )

		horinzontal2_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_ResetButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_ResetButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_UNDO, wx.ART_BUTTON ) )
		self.m_ResetButton.SetToolTip( _(u"Remet la valeur par défaut.") )

		horinzontal2_sizer.Add( self.m_ResetButton, 0, wx.ALL, 5 )


		vertical_sizer.Add( horinzontal2_sizer, 1, wx.EXPAND, 5 )


		horizontal_sizer.Add( vertical_sizer, 1, wx.EXPAND, 5 )


		self.SetSizer( horizontal_sizer )
		self.Layout()

		# Connect Events
		self.m_spinCtrlDouble.Bind( wx.EVT_SPINCTRLDOUBLE, self.OnSpinCtrlDouble )
		self.m_ResetButton.Bind( wx.EVT_BUTTON, self.OnReset )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnSpinCtrlDouble( self, event ):
		event.Skip()

	def OnReset( self, event ):
		event.Skip()


###########################################################################
## Class wxVarInt
###########################################################################

class wxVarInt ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 332,80 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		horizontal_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Name = wx.StaticText( self, wx.ID_ANY, _(u"Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Name.Wrap( -1 )

		self.m_Name.SetToolTip( _(u"Infomation sur la variable") )

		horizontal_sizer.Add( self.m_Name, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_staticline = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		horizontal_sizer.Add( self.m_staticline, 0, wx.EXPAND |wx.ALL, 5 )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		self.m_spinCtrl = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 0 )
		vertical_sizer.Add( self.m_spinCtrl, 0, wx.ALL|wx.EXPAND, 5 )

		horinzontal2_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_ResetButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_ResetButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_UNDO, wx.ART_BUTTON ) )
		self.m_ResetButton.SetToolTip( _(u"Remet la valeur par défaut.") )

		horinzontal2_sizer.Add( self.m_ResetButton, 0, wx.ALL, 5 )


		vertical_sizer.Add( horinzontal2_sizer, 1, wx.EXPAND, 5 )


		horizontal_sizer.Add( vertical_sizer, 1, wx.EXPAND, 5 )


		self.SetSizer( horizontal_sizer )
		self.Layout()

		# Connect Events
		self.m_spinCtrl.Bind( wx.EVT_SPINCTRL, self.OnSpinCtrl )
		self.m_ResetButton.Bind( wx.EVT_BUTTON, self.OnReset )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnSpinCtrl( self, event ):
		event.Skip()

	def OnReset( self, event ):
		event.Skip()


###########################################################################
## Class wxAddItemDialog
###########################################################################

class wxAddItemDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 758,523 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		horizontal_sizer_top = wx.BoxSizer( wx.HORIZONTAL )

		m_TypesListBoxChoices = [ _(u"Item"), _(u"Resource"), _(u"Mesh"), _(u"Image") ]
		self.m_TypesListBox = wx.ListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 200,-1 ), m_TypesListBoxChoices, wx.LB_SINGLE )
		horizontal_sizer_top.Add( self.m_TypesListBox, 0, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.LEFT, 5 )

		self.m_HTMLWin = wx.html.HtmlWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.html.HW_SCROLLBAR_AUTO )
		horizontal_sizer_top.Add( self.m_HTMLWin, 1, wx.EXPAND|wx.ALL, 5 )


		vertical_sizer.Add( horizontal_sizer_top, 1, wx.EXPAND, 5 )

		horizontal_sizer_bottom = wx.BoxSizer( wx.HORIZONTAL )

		self.m_ValidateButton = wx.Button( self, wx.ID_ANY, _(u"Ajouter"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_ValidateButton.SetToolTip( _(u"Valide le choix, créé une instance du type sélectionné et l'ajoute à l'item courant.") )

		horizontal_sizer_bottom.Add( self.m_ValidateButton, 0, wx.ALL, 5 )

		self.m_CancelButton = wx.Button( self, wx.ID_ANY, _(u"Annuler"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_CancelButton.SetToolTip( _(u"Ferme la fenêtre sans ajouter de nouvel item.") )

		horizontal_sizer_bottom.Add( self.m_CancelButton, 0, wx.ALL, 5 )


		vertical_sizer.Add( horizontal_sizer_bottom, 0, wx.EXPAND, 5 )


		self.SetSizer( vertical_sizer )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_TypesListBox.Bind( wx.EVT_LISTBOX, self.OnSelected )
		self.m_ValidateButton.Bind( wx.EVT_BUTTON, self.OnAdd )
		self.m_CancelButton.Bind( wx.EVT_BUTTON, self.OnCancel )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnSelected( self, event ):
		event.Skip()

	def OnAdd( self, event ):
		event.Skip()

	def OnCancel( self, event ):
		event.Skip()


###########################################################################
## Class wxAddVarDialog
###########################################################################

class wxAddVarDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 758,523 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		horizontal_sizer_top = wx.BoxSizer( wx.HORIZONTAL )

		self.m_KeyStaticText = wx.StaticText( self, wx.ID_ANY, _(u"Clef de la variable :"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_KeyStaticText.Wrap( -1 )

		horizontal_sizer_top.Add( self.m_KeyStaticText, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_KeyTextCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_KeyTextCtrl.SetToolTip( _(u"Clef de la variable. \nVoici un exemple de clef: \n«m_Var»\n\nLa variable doit être unique. Le texte est rouge si la clef n'est pas unique.") )

		horizontal_sizer_top.Add( self.m_KeyTextCtrl, 1, wx.ALL, 5 )


		vertical_sizer.Add( horizontal_sizer_top, 0, wx.EXPAND, 5 )

		horizontal_sizer_middle = wx.BoxSizer( wx.HORIZONTAL )

		m_TypesListBoxChoices = [ _(u"FloatVar"), _(u"DoubleVar"), _(u"LongDoubleVar"), _(u"UVar"), _(u"StringVar") ]
		self.m_TypesListBox = wx.ListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 200,-1 ), m_TypesListBoxChoices, wx.LB_SINGLE )
		horizontal_sizer_middle.Add( self.m_TypesListBox, 0, wx.EXPAND|wx.TOP|wx.BOTTOM|wx.LEFT, 5 )

		self.m_HTMLWin = wx.html.HtmlWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.html.HW_SCROLLBAR_AUTO )
		horizontal_sizer_middle.Add( self.m_HTMLWin, 1, wx.EXPAND|wx.ALL, 5 )


		vertical_sizer.Add( horizontal_sizer_middle, 1, wx.EXPAND, 5 )

		horizontal_sizer_bottom = wx.BoxSizer( wx.HORIZONTAL )

		self.m_ValidateButton = wx.Button( self, wx.ID_ANY, _(u"Ajouter"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_ValidateButton.SetToolTip( _(u"Valide le choix, créé une instance du type sélectionné et l'ajoute à l'item courant.") )

		horizontal_sizer_bottom.Add( self.m_ValidateButton, 0, wx.ALL, 5 )

		self.m_CancelButton = wx.Button( self, wx.ID_ANY, _(u"Annuler"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_CancelButton.SetToolTip( _(u"Ferme la fenêtre sans ajouter de nouvel item.") )

		horizontal_sizer_bottom.Add( self.m_CancelButton, 0, wx.ALL, 5 )


		vertical_sizer.Add( horizontal_sizer_bottom, 0, wx.EXPAND, 5 )


		self.SetSizer( vertical_sizer )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_KeyTextCtrl.Bind( wx.EVT_TEXT, self.OnText )
		self.m_KeyTextCtrl.Bind( wx.EVT_TEXT_ENTER, self.OnTextEnter )
		self.m_TypesListBox.Bind( wx.EVT_LISTBOX, self.OnSelected )
		self.m_ValidateButton.Bind( wx.EVT_BUTTON, self.OnAdd )
		self.m_CancelButton.Bind( wx.EVT_BUTTON, self.OnCancel )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnText( self, event ):
		event.Skip()

	def OnTextEnter( self, event ):
		event.Skip()

	def OnSelected( self, event ):
		event.Skip()

	def OnAdd( self, event ):
		event.Skip()

	def OnCancel( self, event ):
		event.Skip()


###########################################################################
## Class wxPanelResourcesExplorer
###########################################################################

class wxPanelResourcesExplorer ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		vertical_sizer = wx.BoxSizer( wx.VERTICAL )

		horizontal_sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_Dir1Button = wx.Button( self, wx.ID_ANY, _(u"Toto1"), wx.DefaultPosition, wx.DefaultSize, wx.BORDER_NONE|wx.BU_EXACTFIT )
		horizontal_sizer.Add( self.m_Dir1Button, 0, wx.TOP|wx.BOTTOM, 5 )

		self.m_Dir1Separator = wx.StaticText( self, wx.ID_ANY, _(u">"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Dir1Separator.Wrap( -1 )

		horizontal_sizer.Add( self.m_Dir1Separator, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM, 5 )

		self.m_Dir2Button = wx.Button( self, wx.ID_ANY, _(u"Meshs"), wx.DefaultPosition, wx.DefaultSize, wx.BORDER_NONE|wx.BU_EXACTFIT )
		horizontal_sizer.Add( self.m_Dir2Button, 0, wx.TOP|wx.BOTTOM, 5 )

		self.m_Dir2Separator = wx.StaticText( self, wx.ID_ANY, _(u">"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Dir2Separator.Wrap( -1 )

		horizontal_sizer.Add( self.m_Dir2Separator, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM, 5 )

		self.m_Dir3 = wx.StaticText( self, wx.ID_ANY, _(u"Table"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_Dir3.Wrap( -1 )

		self.m_Dir3.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )

		horizontal_sizer.Add( self.m_Dir3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		vertical_sizer.Add( horizontal_sizer, 0, wx.EXPAND, 5 )

		self.m_ScrolledWindow = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.m_ScrolledWindow.SetScrollRate( 5, 5 )
		m_GridSizer = wx.GridSizer( 3, 6, 0, 0 )

		item_sizer = wx.BoxSizer( wx.VERTICAL )

		self.m_FileImageButton = wx.BitmapButton( self.m_ScrolledWindow, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.BORDER_NONE )

		self.m_FileImageButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_INFORMATION, wx.ART_MESSAGE_BOX ) )
		item_sizer.Add( self.m_FileImageButton, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.m_FileNameStaticText = wx.StaticText( self.m_ScrolledWindow, wx.ID_ANY, _(u"table.obj"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_FileNameStaticText.Wrap( -1 )

		item_sizer.Add( self.m_FileNameStaticText, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		m_GridSizer.Add( item_sizer, 1, wx.EXPAND, 5 )

		item_sizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_FileImageButton1 = wx.BitmapButton( self.m_ScrolledWindow, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.BORDER_NONE )

		self.m_FileImageButton1.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_INFORMATION, wx.ART_MESSAGE_BOX ) )
		item_sizer1.Add( self.m_FileImageButton1, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.m_FileNameStaticText1 = wx.StaticText( self.m_ScrolledWindow, wx.ID_ANY, _(u"table.mtl"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_FileNameStaticText1.Wrap( -1 )

		item_sizer1.Add( self.m_FileNameStaticText1, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		m_GridSizer.Add( item_sizer1, 1, wx.EXPAND, 5 )


		self.m_ScrolledWindow.SetSizer( m_GridSizer )
		self.m_ScrolledWindow.Layout()
		m_GridSizer.Fit( self.m_ScrolledWindow )
		vertical_sizer.Add( self.m_ScrolledWindow, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( vertical_sizer )
		self.Layout()

	def __del__( self ):
		pass


###########################################################################
## Class wxPanelResourceItem
###########################################################################

class wxPanelResourceItem ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 100,100 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		item_sizer = wx.BoxSizer( wx.VERTICAL )

		self.m_FileImageButton = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|wx.BORDER_NONE )

		self.m_FileImageButton.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_NORMAL_FILE, wx.ART_MESSAGE_BOX ) )
		item_sizer.Add( self.m_FileImageButton, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.m_FileNameStaticText = wx.StaticText( self, wx.ID_ANY, _(u"table.obj"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_FileNameStaticText.Wrap( -1 )

		item_sizer.Add( self.m_FileNameStaticText, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		self.SetSizer( item_sizer )
		self.Layout()

	def __del__( self ):
		pass


###########################################################################
## Class wxVarIInfo
###########################################################################

class wxVarIInfo ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		m_InstanceInfosVerticalSizer = wx.BoxSizer( wx.VERTICAL )

		self.m_InstanceInfos = wx.grid.Grid( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )

		# Grid
		self.m_InstanceInfos.CreateGrid( 10, 2 )
		self.m_InstanceInfos.EnableEditing( True )
		self.m_InstanceInfos.EnableGridLines( True )
		self.m_InstanceInfos.EnableDragGridSize( False )
		self.m_InstanceInfos.SetMargins( 0, 0 )

		# Columns
		self.m_InstanceInfos.SetColSize( 0, 254 )
		self.m_InstanceInfos.SetColSize( 1, 79 )
		self.m_InstanceInfos.AutoSizeColumns()
		self.m_InstanceInfos.EnableDragColMove( False )
		self.m_InstanceInfos.EnableDragColSize( True )
		self.m_InstanceInfos.SetColLabelSize( 30 )
		self.m_InstanceInfos.SetColLabelValue( 0, _(u"Clefs") )
		self.m_InstanceInfos.SetColLabelValue( 1, _(u"Valeurs") )
		self.m_InstanceInfos.SetColLabelAlignment( wx.ALIGN_CENTER, wx.ALIGN_CENTER )

		# Rows
		self.m_InstanceInfos.AutoSizeRows()
		self.m_InstanceInfos.EnableDragRowSize( True )
		self.m_InstanceInfos.SetRowLabelSize( 1 )
		self.m_InstanceInfos.SetRowLabelAlignment( wx.ALIGN_CENTER, wx.ALIGN_CENTER )

		# Label Appearance

		# Cell Defaults
		self.m_InstanceInfos.SetDefaultCellAlignment( wx.ALIGN_CENTER, wx.ALIGN_CENTER )
		self.m_InstanceInfos.SetMaxSize( wx.Size( -1,200 ) )

		m_InstanceInfosVerticalSizer.Add( self.m_InstanceInfos, 0, wx.EXPAND|wx.ALL, 5 )

		m_InstanceInfoHorizontalSizer = wx.BoxSizer( wx.HORIZONTAL )


		m_InstanceInfoHorizontalSizer.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.m_InstanceInfoAdd = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_InstanceInfoAdd.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_PLUS, wx.ART_BUTTON ) )
		self.m_InstanceInfoAdd.SetToolTip( _(u"Ajouter une nouvelle ligne d'information. Si elle reste vide, aucune informations ne sera ajoutée à la variable.") )

		m_InstanceInfoHorizontalSizer.Add( self.m_InstanceInfoAdd, 0, wx.ALL, 5 )

		self.m_InstanceInfoRemove = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_InstanceInfoRemove.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_MINUS, wx.ART_BUTTON ) )
		self.m_InstanceInfoRemove.SetToolTip( _(u"Supprimer les informations sélectionnées") )

		m_InstanceInfoHorizontalSizer.Add( self.m_InstanceInfoRemove, 0, wx.ALL, 5 )


		m_InstanceInfosVerticalSizer.Add( m_InstanceInfoHorizontalSizer, 0, wx.EXPAND, 5 )


		self.SetSizer( m_InstanceInfosVerticalSizer )
		self.Layout()
		m_InstanceInfosVerticalSizer.Fit( self )

		# Connect Events
		self.m_InstanceInfos.Bind( wx.grid.EVT_GRID_CELL_CHANGED, self.OnSetKeyValue )
		self.m_InstanceInfoAdd.Bind( wx.EVT_BUTTON, self.OnAddRow )
		self.m_InstanceInfoRemove.Bind( wx.EVT_BUTTON, self.OnRemoveSelected )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnSetKeyValue( self, event ):
		event.Skip()

	def OnAddRow( self, event ):
		event.Skip()

	def OnRemoveSelected( self, event ):
		event.Skip()


