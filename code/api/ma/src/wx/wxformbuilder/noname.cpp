///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Dec  8 2019)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "noname.h"

///////////////////////////////////////////////////////////////////////////

wxPanelItem::wxPanelItem( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	m_ScrolledWindow = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL );
	m_ScrolledWindow->SetScrollRate( 5, 5 );
	wxBoxSizer* m_VerticalSizer;
	m_VerticalSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* title_sizer;
	title_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_Logo = new wxStaticBitmap( m_ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 30,30 ), 0 );
	m_Logo->SetToolTip( _("Logo de l'item") );

	title_sizer->Add( m_Logo, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_ClassName = new wxStaticText( m_ScrolledWindow, wxID_ANY, _("Nom de la classe"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_ClassName->Wrap( -1 );
	m_ClassName->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	m_ClassName->SetToolTip( _("Description de la classe\nTOTO") );

	title_sizer->Add( m_ClassName, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_CountChild = new wxStaticText( m_ScrolledWindow, wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_CountChild->Wrap( -1 );
	m_CountChild->SetToolTip( _("Nom de l'item.") );

	title_sizer->Add( m_CountChild, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_Add = new wxBitmapButton( m_ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_Add->SetBitmap( wxArtProvider::GetBitmap( wxART_PLUS, wxART_BUTTON ) );
	m_Add->SetMinSize( wxSize( 30,30 ) );

	title_sizer->Add( m_Add, 0, wxALL, 5 );


	m_VerticalSizer->Add( title_sizer, 0, wxEXPAND, 5 );

	wxBoxSizer* key_sizer;
	key_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_TextKey = new wxStaticText( m_ScrolledWindow, wxID_ANY, _("ID"), wxDefaultPosition, wxDefaultSize, 0 );
	m_TextKey->Wrap( -1 );
	key_sizer->Add( m_TextKey, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_Key = new wxStaticText( m_ScrolledWindow, wxID_ANY, _("{00000000-0000-0000-0000-000000000001}"), wxDefaultPosition, wxSize( 292,-1 ), 0 );
	m_Key->Wrap( -1 );
	m_Key->SetToolTip( _("Clef de l'item.") );

	key_sizer->Add( m_Key, 1, wxALIGN_CENTER_VERTICAL|wxALL|wxALIGN_RIGHT, 5 );


	m_VerticalSizer->Add( key_sizer, 0, wxEXPAND, 5 );


	m_ScrolledWindow->SetSizer( m_VerticalSizer );
	m_ScrolledWindow->Layout();
	m_VerticalSizer->Fit( m_ScrolledWindow );
	vertical_sizer->Add( m_ScrolledWindow, 1, wxEXPAND | wxALL, 0 );


	this->SetSizer( vertical_sizer );
	this->Layout();

	// Connect Events
	m_ClassName->Connect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxPanelItem::OnCopyClassName ), NULL, this );
	m_Add->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanelItem::OnAddVar ), NULL, this );
	m_Key->Connect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxPanelItem::OnCopyId ), NULL, this );
}

wxPanelItem::~wxPanelItem()
{
	// Disconnect Events
	m_ClassName->Disconnect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxPanelItem::OnCopyClassName ), NULL, this );
	m_Add->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanelItem::OnAddVar ), NULL, this );
	m_Key->Disconnect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxPanelItem::OnCopyId ), NULL, this );

}

wxVarBaseItem::wxVarBaseItem( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* horizontal_sizer;
	horizontal_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_Name = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Name->Wrap( -1 );
	m_Name->SetToolTip( _("Infomation sur la variable") );

	horizontal_sizer->Add( m_Name, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticline = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	horizontal_sizer->Add( m_staticline, 0, wxEXPAND | wxALL, 5 );

	m_bpButton1 = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_bpButton1->SetBitmap( wxNullBitmap );
	m_bpButton1->SetToolTip( _("Image, logo, aperçu, ... de l'item sélectionné.") );

	horizontal_sizer->Add( m_bpButton1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	m_TextCtrlItem = new wxTextCtrl( this, wxID_ANY, _("None"), wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	m_TextCtrlItem->Enable( false );
	m_TextCtrlItem->SetToolTip( _("L'item sélectionné") );

	vertical_sizer->Add( m_TextCtrlItem, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* horinzontal2_sizer;
	horinzontal2_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_ButtonSetItemCourant = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_ButtonSetItemCourant->SetBitmap( wxArtProvider::GetBitmap( wxART_GO_BACK, wxART_MENU ) );
	m_ButtonSetItemCourant->SetToolTip( _("Assigner l'item courant.") );

	horinzontal2_sizer->Add( m_ButtonSetItemCourant, 0, wxALL, 5 );

	m_SearchItem = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_SearchItem->SetBitmap( wxArtProvider::GetBitmap( wxART_FIND, wxART_BUTTON ) );
	m_SearchItem->SetToolTip( _("Rechercher un item à affecter.") );

	horinzontal2_sizer->Add( m_SearchItem, 0, wxALL, 5 );

	m_ResetButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_ResetButton->SetBitmap( wxArtProvider::GetBitmap( wxART_UNDO, wxART_BUTTON ) );
	m_ResetButton->SetToolTip( _("Remet la valeur par défaut.") );

	horinzontal2_sizer->Add( m_ResetButton, 0, wxALL, 5 );


	vertical_sizer->Add( horinzontal2_sizer, 1, wxEXPAND, 5 );


	horizontal_sizer->Add( vertical_sizer, 1, wxEXPAND, 5 );


	this->SetSizer( horizontal_sizer );
	this->Layout();
}

wxVarBaseItem::~wxVarBaseItem()
{
}

wxVarDefine::wxVarDefine( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* horizontal_sizer;
	horizontal_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_Name = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Name->Wrap( -1 );
	m_Name->SetToolTip( _("Infomation sur la variable") );

	horizontal_sizer->Add( m_Name, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticline = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	horizontal_sizer->Add( m_staticline, 0, wxEXPAND | wxALL, 5 );


	this->SetSizer( horizontal_sizer );
	this->Layout();
}

wxVarDefine::~wxVarDefine()
{
}

wxVar::wxVar( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	m_VerticalSizer = new wxBoxSizer( wxVERTICAL );

	m_TopHorizontalSizer = new wxBoxSizer( wxHORIZONTAL );

	m_Name = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Name->Wrap( -1 );
	m_Name->SetToolTip( _("Infomation sur la variable") );

	m_TopHorizontalSizer->Add( m_Name, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_Warning = new wxStaticBitmap( this, wxID_ANY, wxArtProvider::GetBitmap( wxART_ERROR, wxART_TOOLBAR ), wxDefaultPosition, wxDefaultSize, 0 );
	m_TopHorizontalSizer->Add( m_Warning, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_ResetButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_ResetButton->SetBitmap( wxArtProvider::GetBitmap( wxART_UNDO, wxART_BUTTON ) );
	m_ResetButton->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	m_ResetButton->SetToolTip( _("Remet la valeur par défaut.") );

	m_TopHorizontalSizer->Add( m_ResetButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_DeleteButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_DeleteButton->SetBitmap( wxArtProvider::GetBitmap( wxART_DELETE, wxART_BUTTON ) );
	m_DeleteButton->SetBitmapPosition( wxBOTTOM );
	m_DeleteButton->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	m_DeleteButton->SetToolTip( _("Supprime la variable de l'item.") );

	m_TopHorizontalSizer->Add( m_DeleteButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_DisplayInstanceInfosButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_DisplayInstanceInfosButton->SetBitmap( wxArtProvider::GetBitmap( wxART_LIST_VIEW, wxART_BUTTON ) );
	m_DisplayInstanceInfosButton->SetToolTip( _("Affiche ou cache la grille d'édition des informations de la varible.") );

	m_TopHorizontalSizer->Add( m_DisplayInstanceInfosButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	m_VerticalSizer->Add( m_TopHorizontalSizer, 1, wxEXPAND, 5 );

	m_BottomHorizontalSizer = new wxBoxSizer( wxHORIZONTAL );

	m_TextCtrlVar = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_AUTO_URL|wxTE_PROCESS_ENTER );
	m_TextCtrlVar->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );
	m_TextCtrlVar->SetToolTip( _("Valeur de la chaîne de caractères.") );

	m_BottomHorizontalSizer->Add( m_TextCtrlVar, 1, wxALL|wxEXPAND, 5 );


	m_VerticalSizer->Add( m_BottomHorizontalSizer, 0, wxEXPAND, 5 );

	m_InstanceInfosVerticalSizer = new wxBoxSizer( wxVERTICAL );


	m_VerticalSizer->Add( m_InstanceInfosVerticalSizer, 0, wxEXPAND, 5 );


	this->SetSizer( m_VerticalSizer );
	this->Layout();

	// Connect Events
	m_Name->Connect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxVar::OnCopyVarKey ), NULL, this );
	m_ResetButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVar::OnReset ), NULL, this );
	m_DeleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVar::OnDelete ), NULL, this );
	m_DisplayInstanceInfosButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVar::OnDisplayInstanceInfos ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( wxVar::OnTextChange ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( wxVar::OnTextEnter ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_MAXLEN, wxCommandEventHandler( wxVar::OnTextMaxLen ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( wxVar::OnTextURL ), NULL, this );
}

wxVar::~wxVar()
{
	// Disconnect Events
	m_Name->Disconnect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxVar::OnCopyVarKey ), NULL, this );
	m_ResetButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVar::OnReset ), NULL, this );
	m_DeleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVar::OnDelete ), NULL, this );
	m_DisplayInstanceInfosButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVar::OnDisplayInstanceInfos ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( wxVar::OnTextChange ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( wxVar::OnTextEnter ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_MAXLEN, wxCommandEventHandler( wxVar::OnTextMaxLen ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( wxVar::OnTextURL ), NULL, this );

}

wxVarDirectory::wxVarDirectory( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	m_VerticalSizer = new wxBoxSizer( wxVERTICAL );

	m_TopHorizontalSizer = new wxBoxSizer( wxHORIZONTAL );

	m_Name = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Name->Wrap( -1 );
	m_Name->SetToolTip( _("Infomation sur la variable") );

	m_TopHorizontalSizer->Add( m_Name, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_Warning = new wxStaticBitmap( this, wxID_ANY, wxArtProvider::GetBitmap( wxART_ERROR, wxART_TOOLBAR ), wxDefaultPosition, wxDefaultSize, 0 );
	m_TopHorizontalSizer->Add( m_Warning, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_ResetButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_ResetButton->SetBitmap( wxArtProvider::GetBitmap( wxART_UNDO, wxART_BUTTON ) );
	m_ResetButton->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	m_ResetButton->SetToolTip( _("Remet la valeur par défaut.") );

	m_TopHorizontalSizer->Add( m_ResetButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_DeleteButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_DeleteButton->SetBitmap( wxArtProvider::GetBitmap( wxART_DELETE, wxART_BUTTON ) );
	m_DeleteButton->SetBitmapPosition( wxBOTTOM );
	m_DeleteButton->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	m_DeleteButton->SetToolTip( _("Supprime la variable de l'item.") );

	m_TopHorizontalSizer->Add( m_DeleteButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	m_VerticalSizer->Add( m_TopHorizontalSizer, 1, wxEXPAND, 5 );

	m_BottomHorizontalSizer = new wxBoxSizer( wxHORIZONTAL );

	m_TextCtrlVar = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_AUTO_URL|wxTE_PROCESS_ENTER );
	m_TextCtrlVar->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );
	m_TextCtrlVar->SetToolTip( _("Valeur de la chaîne de caractères.") );

	m_BottomHorizontalSizer->Add( m_TextCtrlVar, 1, wxEXPAND|wxALL, 5 );

	m_BrowserButton = new wxButton( this, wxID_ANY, _("..."), wxDefaultPosition, wxSize( 30,30 ), wxBU_EXACTFIT );
	m_BottomHorizontalSizer->Add( m_BrowserButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 3 );


	m_VerticalSizer->Add( m_BottomHorizontalSizer, 0, wxEXPAND, 5 );


	this->SetSizer( m_VerticalSizer );
	this->Layout();

	// Connect Events
	m_Name->Connect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxVarDirectory::OnCopyVarKey ), NULL, this );
	m_ResetButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory::OnReset ), NULL, this );
	m_DeleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory::OnDelete ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( wxVarDirectory::OnTextChange ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( wxVarDirectory::OnTextEnter ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_MAXLEN, wxCommandEventHandler( wxVarDirectory::OnTextMaxLen ), NULL, this );
	m_TextCtrlVar->Connect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( wxVarDirectory::OnTextURL ), NULL, this );
	m_BrowserButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory::OnOpenExplorer ), NULL, this );
}

wxVarDirectory::~wxVarDirectory()
{
	// Disconnect Events
	m_Name->Disconnect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxVarDirectory::OnCopyVarKey ), NULL, this );
	m_ResetButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory::OnReset ), NULL, this );
	m_DeleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory::OnDelete ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( wxVarDirectory::OnTextChange ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( wxVarDirectory::OnTextEnter ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_MAXLEN, wxCommandEventHandler( wxVarDirectory::OnTextMaxLen ), NULL, this );
	m_TextCtrlVar->Disconnect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( wxVarDirectory::OnTextURL ), NULL, this );
	m_BrowserButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory::OnOpenExplorer ), NULL, this );

}

wxVarDirectory_A::wxVarDirectory_A( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* horinzontal_sizer;
	horinzontal_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_Name = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Name->Wrap( -1 );
	m_Name->SetToolTip( _("Infomation sur la variable") );

	horinzontal_sizer->Add( m_Name, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_Warning = new wxStaticBitmap( this, wxID_ANY, wxArtProvider::GetBitmap( wxART_ERROR, wxART_TOOLBAR ), wxDefaultPosition, wxDefaultSize, 0 );
	horinzontal_sizer->Add( m_Warning, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_ResetButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_ResetButton->SetBitmap( wxArtProvider::GetBitmap( wxART_UNDO, wxART_BUTTON ) );
	m_ResetButton->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	m_ResetButton->SetToolTip( _("Remet la valeur par défaut.") );

	horinzontal_sizer->Add( m_ResetButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_DeleteButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_DeleteButton->SetBitmap( wxArtProvider::GetBitmap( wxART_DELETE, wxART_BUTTON ) );
	m_DeleteButton->SetBitmapPosition( wxBOTTOM );
	m_DeleteButton->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	m_DeleteButton->SetToolTip( _("Supprime la variable de l'item.") );

	horinzontal_sizer->Add( m_DeleteButton, 0, wxALL, 5 );


	vertical_sizer->Add( horinzontal_sizer, 1, wxEXPAND, 5 );

	m_DirPicker = new wxDirPickerCtrl( this, wxID_ANY, wxT("/home/gandi"), _("Sélectionner un dossier de ressoruces"), wxDefaultPosition, wxDefaultSize, wxDIRP_DEFAULT_STYLE|wxDIRP_DIR_MUST_EXIST|wxDIRP_SMALL|wxDIRP_USE_TEXTCTRL );
	vertical_sizer->Add( m_DirPicker, 0, wxALL|wxEXPAND, 4 );


	this->SetSizer( vertical_sizer );
	this->Layout();

	// Connect Events
	m_Name->Connect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxVarDirectory_A::OnCopyVarKey ), NULL, this );
	m_ResetButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory_A::OnReset ), NULL, this );
	m_DeleteButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory_A::OnDelete ), NULL, this );
}

wxVarDirectory_A::~wxVarDirectory_A()
{
	// Disconnect Events
	m_Name->Disconnect( wxEVT_RIGHT_UP, wxMouseEventHandler( wxVarDirectory_A::OnCopyVarKey ), NULL, this );
	m_ResetButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory_A::OnReset ), NULL, this );
	m_DeleteButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDirectory_A::OnDelete ), NULL, this );

}

wxVarDouble::wxVarDouble( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* horizontal_sizer;
	horizontal_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_Name = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Name->Wrap( -1 );
	m_Name->SetToolTip( _("Infomation sur la variable") );

	horizontal_sizer->Add( m_Name, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticline = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	horizontal_sizer->Add( m_staticline, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	m_spinCtrlDouble = new wxSpinCtrlDouble( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 100, 0, 0.01 );
	m_spinCtrlDouble->SetDigits( 2 );
	vertical_sizer->Add( m_spinCtrlDouble, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* horinzontal2_sizer;
	horinzontal2_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_ResetButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_ResetButton->SetBitmap( wxArtProvider::GetBitmap( wxART_UNDO, wxART_BUTTON ) );
	m_ResetButton->SetToolTip( _("Remet la valeur par défaut.") );

	horinzontal2_sizer->Add( m_ResetButton, 0, wxALL, 5 );


	vertical_sizer->Add( horinzontal2_sizer, 1, wxEXPAND, 5 );


	horizontal_sizer->Add( vertical_sizer, 1, wxEXPAND, 5 );


	this->SetSizer( horizontal_sizer );
	this->Layout();

	// Connect Events
	m_spinCtrlDouble->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( wxVarDouble::OnSpinCtrlDouble ), NULL, this );
	m_ResetButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDouble::OnReset ), NULL, this );
}

wxVarDouble::~wxVarDouble()
{
	// Disconnect Events
	m_spinCtrlDouble->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( wxVarDouble::OnSpinCtrlDouble ), NULL, this );
	m_ResetButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarDouble::OnReset ), NULL, this );

}

wxVarInt::wxVarInt( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* horizontal_sizer;
	horizontal_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_Name = new wxStaticText( this, wxID_ANY, _("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Name->Wrap( -1 );
	m_Name->SetToolTip( _("Infomation sur la variable") );

	horizontal_sizer->Add( m_Name, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticline = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	horizontal_sizer->Add( m_staticline, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	m_spinCtrl = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	vertical_sizer->Add( m_spinCtrl, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* horinzontal2_sizer;
	horinzontal2_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_ResetButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_ResetButton->SetBitmap( wxArtProvider::GetBitmap( wxART_UNDO, wxART_BUTTON ) );
	m_ResetButton->SetToolTip( _("Remet la valeur par défaut.") );

	horinzontal2_sizer->Add( m_ResetButton, 0, wxALL, 5 );


	vertical_sizer->Add( horinzontal2_sizer, 1, wxEXPAND, 5 );


	horizontal_sizer->Add( vertical_sizer, 1, wxEXPAND, 5 );


	this->SetSizer( horizontal_sizer );
	this->Layout();

	// Connect Events
	m_spinCtrl->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( wxVarInt::OnSpinCtrl ), NULL, this );
	m_ResetButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarInt::OnReset ), NULL, this );
}

wxVarInt::~wxVarInt()
{
	// Disconnect Events
	m_spinCtrl->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( wxVarInt::OnSpinCtrl ), NULL, this );
	m_ResetButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarInt::OnReset ), NULL, this );

}

wxAddItemDialog::wxAddItemDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* horizontal_sizer_top;
	horizontal_sizer_top = new wxBoxSizer( wxHORIZONTAL );

	m_TypesListBox = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxSize( 200,-1 ), 0, NULL, wxLB_SINGLE );
	m_TypesListBox->Append( _("Item") );
	m_TypesListBox->Append( _("Resource") );
	m_TypesListBox->Append( _("Mesh") );
	m_TypesListBox->Append( _("Image") );
	horizontal_sizer_top->Add( m_TypesListBox, 0, wxEXPAND|wxTOP|wxBOTTOM|wxLEFT, 5 );

	m_HTMLWin = new wxHtmlWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	horizontal_sizer_top->Add( m_HTMLWin, 1, wxEXPAND|wxALL, 5 );


	vertical_sizer->Add( horizontal_sizer_top, 1, wxEXPAND, 5 );

	wxBoxSizer* horizontal_sizer_bottom;
	horizontal_sizer_bottom = new wxBoxSizer( wxHORIZONTAL );

	m_ValidateButton = new wxButton( this, wxID_ANY, _("Ajouter"), wxDefaultPosition, wxDefaultSize, 0 );
	m_ValidateButton->SetToolTip( _("Valide le choix, créé une instance du type sélectionné et l'ajoute à l'item courant.") );

	horizontal_sizer_bottom->Add( m_ValidateButton, 0, wxALL, 5 );

	m_CancelButton = new wxButton( this, wxID_ANY, _("Annuler"), wxDefaultPosition, wxDefaultSize, 0 );
	m_CancelButton->SetToolTip( _("Ferme la fenêtre sans ajouter de nouvel item.") );

	horizontal_sizer_bottom->Add( m_CancelButton, 0, wxALL, 5 );


	vertical_sizer->Add( horizontal_sizer_bottom, 0, wxEXPAND, 5 );


	this->SetSizer( vertical_sizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_TypesListBox->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( wxAddItemDialog::OnSelected ), NULL, this );
	m_ValidateButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddItemDialog::OnAdd ), NULL, this );
	m_CancelButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddItemDialog::OnCancel ), NULL, this );
}

wxAddItemDialog::~wxAddItemDialog()
{
	// Disconnect Events
	m_TypesListBox->Disconnect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( wxAddItemDialog::OnSelected ), NULL, this );
	m_ValidateButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddItemDialog::OnAdd ), NULL, this );
	m_CancelButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddItemDialog::OnCancel ), NULL, this );

}

wxAddVarDialog::wxAddVarDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* horizontal_sizer_top;
	horizontal_sizer_top = new wxBoxSizer( wxHORIZONTAL );

	m_KeyStaticText = new wxStaticText( this, wxID_ANY, _("Clef de la variable :"), wxDefaultPosition, wxDefaultSize, 0 );
	m_KeyStaticText->Wrap( -1 );
	horizontal_sizer_top->Add( m_KeyStaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_KeyTextCtrl = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_KeyTextCtrl->SetToolTip( _("Clef de la variable. \nVoici un exemple de clef: \n«m_Var»\n\nLa variable doit être unique. Le texte est rouge si la clef n'est pas unique.") );

	horizontal_sizer_top->Add( m_KeyTextCtrl, 1, wxALL, 5 );


	vertical_sizer->Add( horizontal_sizer_top, 0, wxEXPAND, 5 );

	wxBoxSizer* horizontal_sizer_middle;
	horizontal_sizer_middle = new wxBoxSizer( wxHORIZONTAL );

	m_TypesListBox = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxSize( 200,-1 ), 0, NULL, wxLB_SINGLE );
	m_TypesListBox->Append( _("FloatVar") );
	m_TypesListBox->Append( _("DoubleVar") );
	m_TypesListBox->Append( _("LongDoubleVar") );
	m_TypesListBox->Append( _("UVar") );
	m_TypesListBox->Append( _("StringVar") );
	horizontal_sizer_middle->Add( m_TypesListBox, 0, wxEXPAND|wxTOP|wxBOTTOM|wxLEFT, 5 );

	m_HTMLWin = new wxHtmlWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO );
	horizontal_sizer_middle->Add( m_HTMLWin, 1, wxEXPAND|wxALL, 5 );


	vertical_sizer->Add( horizontal_sizer_middle, 1, wxEXPAND, 5 );

	wxBoxSizer* horizontal_sizer_bottom;
	horizontal_sizer_bottom = new wxBoxSizer( wxHORIZONTAL );

	m_ValidateButton = new wxButton( this, wxID_ANY, _("Ajouter"), wxDefaultPosition, wxDefaultSize, 0 );
	m_ValidateButton->SetToolTip( _("Valide le choix, créé une instance du type sélectionné et l'ajoute à l'item courant.") );

	horizontal_sizer_bottom->Add( m_ValidateButton, 0, wxALL, 5 );

	m_CancelButton = new wxButton( this, wxID_ANY, _("Annuler"), wxDefaultPosition, wxDefaultSize, 0 );
	m_CancelButton->SetToolTip( _("Ferme la fenêtre sans ajouter de nouvel item.") );

	horizontal_sizer_bottom->Add( m_CancelButton, 0, wxALL, 5 );


	vertical_sizer->Add( horizontal_sizer_bottom, 0, wxEXPAND, 5 );


	this->SetSizer( vertical_sizer );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_KeyTextCtrl->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( wxAddVarDialog::OnText ), NULL, this );
	m_KeyTextCtrl->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( wxAddVarDialog::OnTextEnter ), NULL, this );
	m_TypesListBox->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( wxAddVarDialog::OnSelected ), NULL, this );
	m_ValidateButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddVarDialog::OnAdd ), NULL, this );
	m_CancelButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddVarDialog::OnCancel ), NULL, this );
}

wxAddVarDialog::~wxAddVarDialog()
{
	// Disconnect Events
	m_KeyTextCtrl->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( wxAddVarDialog::OnText ), NULL, this );
	m_KeyTextCtrl->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( wxAddVarDialog::OnTextEnter ), NULL, this );
	m_TypesListBox->Disconnect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( wxAddVarDialog::OnSelected ), NULL, this );
	m_ValidateButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddVarDialog::OnAdd ), NULL, this );
	m_CancelButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxAddVarDialog::OnCancel ), NULL, this );

}

wxPanelResourcesExplorer::wxPanelResourcesExplorer( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* vertical_sizer;
	vertical_sizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* horizontal_sizer;
	horizontal_sizer = new wxBoxSizer( wxHORIZONTAL );

	m_Dir1Button = new wxButton( this, wxID_ANY, _("Toto1"), wxDefaultPosition, wxDefaultSize, wxBORDER_NONE|wxBU_EXACTFIT );
	horizontal_sizer->Add( m_Dir1Button, 0, wxTOP|wxBOTTOM, 5 );

	m_Dir1Separator = new wxStaticText( this, wxID_ANY, _(">"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Dir1Separator->Wrap( -1 );
	horizontal_sizer->Add( m_Dir1Separator, 0, wxALIGN_CENTER_VERTICAL|wxTOP|wxBOTTOM, 5 );

	m_Dir2Button = new wxButton( this, wxID_ANY, _("Meshs"), wxDefaultPosition, wxDefaultSize, wxBORDER_NONE|wxBU_EXACTFIT );
	horizontal_sizer->Add( m_Dir2Button, 0, wxTOP|wxBOTTOM, 5 );

	m_Dir2Separator = new wxStaticText( this, wxID_ANY, _(">"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Dir2Separator->Wrap( -1 );
	horizontal_sizer->Add( m_Dir2Separator, 0, wxALIGN_CENTER_VERTICAL|wxTOP|wxBOTTOM, 5 );

	m_Dir3 = new wxStaticText( this, wxID_ANY, _("Table"), wxDefaultPosition, wxDefaultSize, 0 );
	m_Dir3->Wrap( -1 );
	m_Dir3->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );

	horizontal_sizer->Add( m_Dir3, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	vertical_sizer->Add( horizontal_sizer, 0, wxEXPAND, 5 );

	m_ScrolledWindow = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	m_ScrolledWindow->SetScrollRate( 5, 5 );
	m_GridSizer = new wxGridSizer( 3, 6, 0, 0 );

	wxBoxSizer* item_sizer;
	item_sizer = new wxBoxSizer( wxVERTICAL );

	m_FileImageButton = new wxBitmapButton( m_ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_FileImageButton->SetBitmap( wxArtProvider::GetBitmap( wxART_INFORMATION, wxART_MESSAGE_BOX ) );
	item_sizer->Add( m_FileImageButton, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_FileNameStaticText = new wxStaticText( m_ScrolledWindow, wxID_ANY, _("table.obj"), wxDefaultPosition, wxDefaultSize, 0 );
	m_FileNameStaticText->Wrap( -1 );
	item_sizer->Add( m_FileNameStaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	m_GridSizer->Add( item_sizer, 1, wxEXPAND, 5 );

	wxBoxSizer* item_sizer1;
	item_sizer1 = new wxBoxSizer( wxVERTICAL );

	m_FileImageButton1 = new wxBitmapButton( m_ScrolledWindow, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_FileImageButton1->SetBitmap( wxArtProvider::GetBitmap( wxART_INFORMATION, wxART_MESSAGE_BOX ) );
	item_sizer1->Add( m_FileImageButton1, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_FileNameStaticText1 = new wxStaticText( m_ScrolledWindow, wxID_ANY, _("table.mtl"), wxDefaultPosition, wxDefaultSize, 0 );
	m_FileNameStaticText1->Wrap( -1 );
	item_sizer1->Add( m_FileNameStaticText1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	m_GridSizer->Add( item_sizer1, 1, wxEXPAND, 5 );


	m_ScrolledWindow->SetSizer( m_GridSizer );
	m_ScrolledWindow->Layout();
	m_GridSizer->Fit( m_ScrolledWindow );
	vertical_sizer->Add( m_ScrolledWindow, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( vertical_sizer );
	this->Layout();
}

wxPanelResourcesExplorer::~wxPanelResourcesExplorer()
{
}

wxPanelResourceItem::wxPanelResourceItem( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* item_sizer;
	item_sizer = new wxBoxSizer( wxVERTICAL );

	m_FileImageButton = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|wxBORDER_NONE );

	m_FileImageButton->SetBitmap( wxArtProvider::GetBitmap( wxART_NORMAL_FILE, wxART_MESSAGE_BOX ) );
	item_sizer->Add( m_FileImageButton, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_FileNameStaticText = new wxStaticText( this, wxID_ANY, _("table.obj"), wxDefaultPosition, wxDefaultSize, 0 );
	m_FileNameStaticText->Wrap( -1 );
	item_sizer->Add( m_FileNameStaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	this->SetSizer( item_sizer );
	this->Layout();
}

wxPanelResourceItem::~wxPanelResourceItem()
{
}

wxVarIInfo::wxVarIInfo( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	m_InstanceInfosVerticalSizer = new wxBoxSizer( wxVERTICAL );

	m_InstanceInfos = new wxGrid( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	// Grid
	m_InstanceInfos->CreateGrid( 10, 2 );
	m_InstanceInfos->EnableEditing( true );
	m_InstanceInfos->EnableGridLines( true );
	m_InstanceInfos->EnableDragGridSize( false );
	m_InstanceInfos->SetMargins( 0, 0 );

	// Columns
	m_InstanceInfos->SetColSize( 0, 254 );
	m_InstanceInfos->SetColSize( 1, 79 );
	m_InstanceInfos->AutoSizeColumns();
	m_InstanceInfos->EnableDragColMove( false );
	m_InstanceInfos->EnableDragColSize( true );
	m_InstanceInfos->SetColLabelSize( 30 );
	m_InstanceInfos->SetColLabelValue( 0, _("Clefs") );
	m_InstanceInfos->SetColLabelValue( 1, _("Valeurs") );
	m_InstanceInfos->SetColLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Rows
	m_InstanceInfos->AutoSizeRows();
	m_InstanceInfos->EnableDragRowSize( true );
	m_InstanceInfos->SetRowLabelSize( 1 );
	m_InstanceInfos->SetRowLabelAlignment( wxALIGN_CENTER, wxALIGN_CENTER );

	// Label Appearance

	// Cell Defaults
	m_InstanceInfos->SetDefaultCellAlignment( wxALIGN_CENTER, wxALIGN_CENTER );
	m_InstanceInfos->SetMaxSize( wxSize( -1,200 ) );

	m_InstanceInfosVerticalSizer->Add( m_InstanceInfos, 0, wxEXPAND|wxALL, 5 );

	m_InstanceInfoHorizontalSizer = new wxBoxSizer( wxHORIZONTAL );


	m_InstanceInfoHorizontalSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_InstanceInfoAdd = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_InstanceInfoAdd->SetBitmap( wxArtProvider::GetBitmap( wxART_PLUS, wxART_BUTTON ) );
	m_InstanceInfoAdd->SetToolTip( _("Ajouter une nouvelle ligne d'information. Si elle reste vide, aucune informations ne sera ajoutée à la variable.") );

	m_InstanceInfoHorizontalSizer->Add( m_InstanceInfoAdd, 0, wxALL, 5 );

	m_InstanceInfoRemove = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_InstanceInfoRemove->SetBitmap( wxArtProvider::GetBitmap( wxART_MINUS, wxART_BUTTON ) );
	m_InstanceInfoRemove->SetToolTip( _("Supprimer les informations sélectionnées") );

	m_InstanceInfoHorizontalSizer->Add( m_InstanceInfoRemove, 0, wxALL, 5 );


	m_InstanceInfosVerticalSizer->Add( m_InstanceInfoHorizontalSizer, 0, wxEXPAND, 5 );


	this->SetSizer( m_InstanceInfosVerticalSizer );
	this->Layout();
	m_InstanceInfosVerticalSizer->Fit( this );

	// Connect Events
	m_InstanceInfos->Connect( wxEVT_GRID_CELL_CHANGED, wxGridEventHandler( wxVarIInfo::OnSetKeyValue ), NULL, this );
	m_InstanceInfoAdd->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarIInfo::OnAddRow ), NULL, this );
	m_InstanceInfoRemove->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarIInfo::OnRemoveSelected ), NULL, this );
}

wxVarIInfo::~wxVarIInfo()
{
	// Disconnect Events
	m_InstanceInfos->Disconnect( wxEVT_GRID_CELL_CHANGED, wxGridEventHandler( wxVarIInfo::OnSetKeyValue ), NULL, this );
	m_InstanceInfoAdd->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarIInfo::OnAddRow ), NULL, this );
	m_InstanceInfoRemove->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxVarIInfo::OnRemoveSelected ), NULL, this );

}
