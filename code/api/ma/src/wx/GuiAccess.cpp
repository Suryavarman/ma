/// \file wx/GuiAccess.cpp
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/wx/GuiAccess.h"
#include "ma/Translation.h"
#include "ma/Error.h"
#include <wx/clipbrd.h>

namespace ma::wx
{

    GuiAccess::GuiAccess(const ma::Item::ConstructorParameters &in_params):
    Item({in_params, Key::GetGuiAccess(), true, false}),
    m_MenuBar(nullptr),
    m_ToolBar(nullptr),
    m_BottomNotebook(nullptr),
    m_LeftNotebook(nullptr),
    m_RightNotebook(nullptr),
    m_CenterNotebook(nullptr),
    m_MotherFrame(nullptr),
    m_ParameterManager(ma::Item::CreateItem<ParameterManager>({GetKey(), true})),
    m_wxContext(
        ma::Item::CreateItem<ma::wx::Context>({Key::GetContextManager(), true, false}, std::wstring(L"wxWidgets")))
    {}

    GuiAccess::~GuiAccess() = default;

    wxMenuBar *GuiAccess::GetMenuBar() const
    {
        MA_ASSERT(m_MenuBar, L"m_MenuBar est nulle.");

        return m_MenuBar;
    }

    wxAuiToolBar *GuiAccess::GetToolBar() const
    {
        MA_ASSERT(m_ToolBar, L"m_ToolBar est nulle.");

        return m_ToolBar;
    }

    wxAuiNotebook *GuiAccess::GetBottomNotebook() const
    {
        MA_ASSERT(m_BottomNotebook, L"m_BottomNotebook est nul.");

        return m_BottomNotebook;
    }

    wxAuiNotebook *GuiAccess::GetLeftNoteBook() const
    {
        MA_ASSERT(m_LeftNotebook, L"m_LeftNotebook est nul.");

        return m_LeftNotebook;
    }

    wxAuiNotebook *GuiAccess::GetRightNoteBook() const
    {
        MA_ASSERT(m_RightNotebook, L"m_RightNotebook est nul.");

        return m_RightNotebook;
    }

    wxAuiNotebook *GuiAccess::GetCenterNoteBook() const
    {
        MA_ASSERT(m_CenterNotebook, L"m_CenterNotebook est nul.");

        return m_CenterNotebook;
    }

    wxFrame *GuiAccess::GetMotherFrame() const
    {
        MA_ASSERT(m_MotherFrame, L"m_MotherFrame est nulle.");

        return m_MotherFrame;
    }

    void GuiAccess::SetMenuBar(wxMenuBar *inMenuBar)
    {
        MA_ASSERT(!m_MenuBar, L"m_MenuBar pointe déjà sur une instance.");

        MA_ASSERT(inMenuBar, L"inMenuBar est nulle.", std::invalid_argument);

        m_MenuBar = inMenuBar;
    }

    void GuiAccess::SetToolBar(wxAuiToolBar *inAuiToolBar)
    {
        MA_ASSERT(!m_ToolBar, L"m_ToolBar pointe déjà sur une instance.");

        MA_ASSERT(inAuiToolBar, L"inAuiToolBar est nulle.", std::invalid_argument);

        m_ToolBar = inAuiToolBar;
    }

    void GuiAccess::SetBottomNotebook(wxAuiNotebook *inNoteBook)
    {
        MA_ASSERT(!m_BottomNotebook, L"m_BottomNotebook pointe déjà sur une instance.");

        MA_ASSERT(inNoteBook, L"inNoteBook est nulle.", std::invalid_argument);

        m_BottomNotebook = inNoteBook;
    }

    void GuiAccess::SetLeftNoteBook(wxAuiNotebook *inNoteBook)
    {
        MA_ASSERT(!m_LeftNotebook, L"m_LeftNotebook pointe déjà sur une instance.");

        MA_ASSERT(inNoteBook, L"inNoteBook est nulle.", std::invalid_argument);

        m_LeftNotebook = inNoteBook;
    }

    void GuiAccess::SetRightNoteBook(wxAuiNotebook *inNoteBook)
    {
        MA_ASSERT(!m_RightNotebook, L"m_RightNotebook pointe déjà sur une instance.");

        MA_ASSERT(inNoteBook, L"inNoteBook est nulle.", std::invalid_argument);

        m_RightNotebook = inNoteBook;
    }

    void GuiAccess::SetCenterNoteBook(wxAuiNotebook *inNoteBook)
    {
        MA_ASSERT(!m_CenterNotebook, L"m_CenterNotebook pointe déjà sur une instance.");

        MA_ASSERT(inNoteBook, L"inNoteBook est nulle.", std::invalid_argument);

        m_CenterNotebook = inNoteBook;
    }

    void GuiAccess::SetMotherFrame(wxFrame *inFrame)
    {
        MA_ASSERT(!m_MotherFrame, L"m_MotherFrame pointe déjà sur une instance.");

        MA_ASSERT(inFrame, L"inFrame est nulle.", std::invalid_argument);

        m_MotherFrame = inFrame;
    }

    bool GuiAccess::SendTextToClipboard(const std::wstring &str)
    {
        if(wxTheClipboard->Open())
        {
            wxTheClipboard->SetData(new wxTextDataObject(str));
            wxTheClipboard->Close();

            auto frame = GetMotherFrame();

            frame->SetStatusText(M_Tr(L"Copie dans le presse papier du texte «" + str + L"»."));

            return true;
        }
        else
            return false;
    }

    const TypeInfo &GuiAccess::GetwxGuiAccessTypeInfo()
    {
        static const TypeInfo info = {GetwxGuiAccessClassName(),
                                      L"wxGuiAccess défini une interface unique qui permet d'attribuer et d'accéder "
                                      L"aux instances de certains éléments de l'application wxWidgets. Pour y accéder "
                                      L"il vous faudra utiliser ma::Root. ex: root.wx.GetMenuBar()",
                                      {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(GuiAccess, wx, Item)
} // namespace ma::wx
