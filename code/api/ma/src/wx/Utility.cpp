/// \file wx/Utility.cpp
/// \author Pontier Pierre
/// \date 2020-05-24
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/wx/Utility.h"

namespace ma::wx
{
    wxIMPLEMENT_DYNAMIC_CLASS(ObjectItemKey, wxObject);
    wxIMPLEMENT_DYNAMIC_CLASS(ObjectVarKey, wxObject);
} // namespace ma::wx
