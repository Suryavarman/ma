/// \file wx/Variable.cpp
/// \author Pontier Pierre
/// \date 2020-02-05
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/wx/Variable.h"
#include "ma/Macros.h"
#include <wx/stdpaths.h>

namespace ma::wx
{
    VarIInfo::VarIInfo(ma::VarPtr var,
                       wxWindow *parent,
                       wxWindowID id,
                       const wxPoint &pos,
                       const wxSize &size,
                       long style,
                       const wxString &name):
    wxPanel(parent, id, pos, size, style, var ? wxString(var->toString()) : wxPanelNameStr),
    ma::Observer(),
    m_Var(var)
    {
        MA_ASSERT(m_Var, L"var est nulle.", std::invalid_argument);

        m_VerticalSizer = new wxBoxSizer(wxVERTICAL);

        m_Grid = new wxGrid(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);

        // Grid
        m_Grid->CreateGrid(10, 2);
        m_Grid->EnableEditing(true);
        m_Grid->EnableGridLines(true);
        m_Grid->EnableDragGridSize(false);
        m_Grid->SetMargins(0, 0);

        // Columns
        m_Grid->SetColSize(0, 152);
        m_Grid->SetColSize(1, 152);
        m_Grid->SetColMinimalWidth(0, 68);
        m_Grid->SetColMinimalWidth(1, 68);
        // m_Grid->AutoSizeColumns(false);
        m_Grid->EnableDragColMove(false);
        m_Grid->EnableDragColSize(true);
        m_Grid->SetColLabelSize(30);
        m_Grid->SetColLabelValue(0, M_Tr(L"Clefs"));
        m_Grid->SetColLabelValue(1, M_Tr(L"Valeurs"));
        m_Grid->SetColLabelAlignment(wxALIGN_CENTER, wxALIGN_CENTER);

        // Rows
        // m_Grid->AutoSizeRows();
        m_Grid->EnableDragRowSize(false);
        m_Grid->SetRowLabelSize(1);
        m_Grid->SetRowLabelAlignment(wxALIGN_CENTER, wxALIGN_CENTER);

        // Label Appearance

        // Cell Defaults
        m_Grid->SetDefaultCellAlignment(wxALIGN_CENTER, wxALIGN_CENTER);
        m_Grid->SetMaxSize(wxSize(-1, 200));

        m_Grid->AppendRows();
        // m_Grid->AutoSize();
        m_Grid->GoToCell(0, 0);

        m_VerticalSizer->Add(m_Grid, 0, wxEXPAND | wxTOP | wxRIGHT | wxLEFT, 5);

        m_HorizontalSizer = new wxBoxSizer(wxHORIZONTAL);

        m_HorizontalSizer->Add(0, 0, 1, wxEXPAND, 5);

        m_Add = new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_Add->SetBitmap(wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON));
        m_Add->SetToolTip(
            M_Tr(L"Ajouter une nouvelle ligne d'information. Si elle reste vide, aucune informations ne sera "
                 L"ajoutée à la variable."));

        m_HorizontalSizer->Add(m_Add, 0, wxALL, 5);

        m_Remove =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_Remove->SetBitmap(wxArtProvider::GetBitmap(wxART_MINUS, wxART_BUTTON));
        m_Remove->SetToolTip(M_Tr(L"Supprimer les informations sélectionnées"));

        m_HorizontalSizer->Add(m_Remove, 0, wxALL, 5);

        m_VerticalSizer->Add(m_HorizontalSizer, 1, wxEXPAND, 5);

        this->SetSizer(m_VerticalSizer);
        this->Layout();

        if(m_Var->IsObservable())
        {
            if(ma::Item::HasItem(ma::Item::Key::GetObservationManager()))
            {
                auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
                obs_manager->AddObservation<ma::wxObserverVar<VarString>, ma::VarVar>(this, m_Var);
            }
        }
        else
        {
            SetVar(m_Var);
        }

        m_Grid->Bind(wxEVT_GRID_CELL_CHANGED, wxGridEventHandler(VarIInfo::OnSetKeyValue), this);
        m_Add->Bind(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(VarIInfo::OnAddRow), this);
        m_Remove->Bind(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(VarIInfo::OnRemoveSelected), this);
    }

    VarIInfo::~VarIInfo()
    {}

    void VarIInfo::OnSetKeyValue(wxGridEvent &event)
    {
        int row = event.GetRow();
        if(-1 < row && row < m_Grid->GetNumberRows())
        {
            auto key = m_Grid->GetCellValue(row, 0).ToStdWstring();
            auto value = m_Grid->GetCellValue(row, 1).ToStdWstring();
            m_Var->InsertIInfoKeyValue(key, value);
        }
    }

    void VarIInfo::OnAddRow(wxCommandEvent &event)
    {
        if(m_Grid->GetNumberRows() == static_cast<int>(m_Var->GetIInfoCount()))
        {
            int row = m_Grid->GetNumberRows() - 1;
            m_Grid->AppendRows();
            m_Grid->AutoSize();
            m_Grid->GoToCell(row, 0);
            m_Grid->SelectRow(row);
        }
    }

    void VarIInfo::OnRemoveSelected(wxCommandEvent &event)
    {
        wxArrayInt rows = m_Grid->GetSelectedRows();
        for(auto it = rows.begin(); it != rows.end(); ++it)
        {
            auto key = m_Grid->GetCellValue(*it, 0).ToStdWstring();
            m_Var->RemoveIInfoKeyValue(key);
        }
    }

    void VarIInfo::SetVar(const ma::ObservablePtr &observable, bool end_observation)
    {
        ma::VarPtr var = std::dynamic_pointer_cast<ma::Var>(observable);

        MA_ASSERT(var, L"Impossible de convertir l'observable en ptr_type, car var est nulle", std::invalid_argument);

        MA_ASSERT(
            m_Var == var,
            L"l'observable est différent de l'instance m_Var qui est supposée être observée. wxVarString observe une "
            L"instance à la fois.",
            std::invalid_argument);

        if(end_observation)
        {
            m_Grid->ClearSelection();
            m_Grid->DeleteRows(0, m_Grid->GetNumberRows());
            m_Grid->ClearGrid();
        }
        else
        {
            ma::InstanceInfo infos = m_Var->GetIInfo();

            // Associe une clef à l'index d'une ligne
            std::unordered_map<ma::InstanceInfo::key_type, int> rows;

            // On nettoie la grille des éléments qui ne sont pas ou plus dans les infos.
            int i = 0;
            while(i < m_Grid->GetNumberRows())
            {
                auto key = m_Grid->GetCellValue(i, 0).ToStdWstring();
                if(infos.find(key) == infos.end())
                    m_Grid->DeleteRows(i);
                else
                {
                    rows[key] = i;
                    i++;
                }
            }

            // Nous parcourons les informations et nous modifions celles qui existent déjà et nous ajoutons les
            // nouvelles.
            for(auto it : infos)
            {
                auto key = it.first;
                auto value = it.second;
                auto it_find = rows.find(key);

                int row_index;

                if(it_find == rows.end())
                {
                    m_Grid->AppendRows();
                    row_index = m_Grid->GetNumberRows() - 1;
                }
                else
                {
                    row_index = it_find->second;
                }

                if(m_Grid->GetCellValue(row_index, 0) != key || m_Grid->GetCellValue(row_index, 1) != value)
                {
                    m_Grid->SetCellValue(row_index, 0, key);
                    m_Grid->SetCellValue(row_index, 1, value);
                }
            }
        }
        this->Layout();
    }

    void VarIInfo::BeginObservation(const ma::ObservablePtr &observable)
    {
        SetVar(observable);
    }

    void VarIInfo::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(data.HasActions(ma::Var::Key::Obs::ms_EndSetIInfo) ||
           data.HasActions(ma::Var::Key::Obs::ms_EndSetIInfoValue) ||
           data.HasActions(ma::Var::Key::Obs::ms_EndAddIInfo) || data.HasActions(ma::Var::Key::Obs::ms_EndRemoveIInfo))
            SetVar(observable);
    }

    void VarIInfo::EndObservation(const ma::ObservablePtr &observable)
    {
        SetVar(observable, true);
    }

    VarIInfo2::VarIInfo2(ma::VarPtr var,
                         wxWindow *parent,
                         wxWindowID id,
                         const wxPoint &pos,
                         const wxSize &size,
                         long style,
                         const wxString &name):
    wxPanel(parent, id, pos, size, style, var ? wxString(var->toString()) : wxPanelNameStr),
    ma::Observer(),
    m_Var(var),
    m_PropertyGrid(nullptr)
    {
        MA_ASSERT(m_Var, L"var est nulle.", std::invalid_argument);

        wxBoxSizer *vertical_sizer;
        vertical_sizer = new wxBoxSizer(wxVERTICAL);

        m_PropertyGrid = new wxPropertyGrid(
            this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxPG_BOLD_MODIFIED | wxPG_DEFAULT_STYLE | wxPG_TOOLTIPS);
        vertical_sizer->Add(m_PropertyGrid, 1, wxALL | wxEXPAND, 5);

        wxBoxSizer *bottom_sizer;
        bottom_sizer = new wxBoxSizer(wxHORIZONTAL);

        bottom_sizer->Add(0, 0, 1, wxEXPAND, 5);

        m_Append =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_Append->SetBitmap(wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON));
        m_Append->SetToolTip(M_Tr(L"Ajouter une nouvelle propriété à cette instance de variable."));

        bottom_sizer->Add(m_Append, 0, wxALL, 5);

        m_Remove =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_Remove->SetBitmap(wxArtProvider::GetBitmap(wxART_DELETE, wxART_BUTTON));
        m_Remove->SetToolTip(M_Tr(L"Supprimer les propriétés sélectionnées."));

        bottom_sizer->Add(m_Remove, 0, wxALL, 5);

        vertical_sizer->Add(bottom_sizer, 0, wxEXPAND, 5);

        this->SetSizer(vertical_sizer);
        this->Layout();

        if(m_Var->IsObservable())
        {
            if(ma::Item::HasItem(ma::Item::Key::GetObservationManager()))
            {
                auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
                obs_manager->AddObservation<ma::wxObserverVar<VarString>, ma::VarVar>(this, m_Var);
            }
        }
        else
        {
            SetVar(m_Var);
        }

        m_PropertyGrid->Bind(wxEVT_PG_CHANGED, &VarIInfo2::OnSetKeyValue, this);
        m_Append->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &VarIInfo2::OnAppendProperty, this);
        m_Remove->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &VarIInfo2::OnRemoveProperty, this);
    }

    VarIInfo2::~VarIInfo2()
    {}

    void VarIInfo2::OnSetKeyValue(wxPropertyGridEvent &event)
    {
        auto key = event.GetPropertyName().ToStdWstring();
        auto value = event.GetPropertyValue().GetString().ToStdWstring();
        m_Var->InsertIInfoKeyValue(key, value);
    }

    void VarIInfo2::OnAppendProperty(wxCommandEvent &event)
    {
        AddIInfo dialog(m_Var, ma::root()->m_wxGuiAccess->GetMotherFrame());
        dialog.ShowModal();
    }

    void VarIInfo2::OnRemoveProperty(wxCommandEvent &event)
    {
        wxPGProperty *property = m_PropertyGrid->GetSelection();

        if(property)
        {
            auto key = property->GetName().ToStdWstring();
            m_Var->RemoveIInfoKeyValue(key);
            m_PropertyGrid->RefreshEditor();
        }
    }

    void VarIInfo2::SetVar(const ma::ObservablePtr &observable, bool end_observation)
    {
        ma::VarPtr var = std::dynamic_pointer_cast<ma::Var>(observable);

        MA_ASSERT(var, L"Impossible de convertir l'observable en ptr_type, car var est nulle", std::invalid_argument);

        MA_ASSERT(
            m_Var == var,
            L"l'observable est différent de l'instance m_Var qui est supposée être observée. wxVarString observe une "
            L"instance à la fois.",
            std::invalid_argument);

        if(end_observation)
        {
            m_PropertyGrid->Clear();
        }
        else
        {
            ma::InstanceInfo infos = m_Var->GetIInfo();

            // On associe une clef à une propriétée
            std::unordered_map<ma::InstanceInfo::key_type, wxPGProperty *> properties;

            wxPGProperty *root = m_PropertyGrid->GetRoot();

            if(root)
            {
                unsigned int i = 0;
                while(i < root->GetChildCount())
                {
                    wxPGProperty *property = root->Item(i);

                    if(property)
                    {
                        auto key = property->GetName().ToStdWstring();
                        if(infos.find(key) == infos.end())
                            m_PropertyGrid->DeleteProperty(wxString(key));
                        else
                        {
                            properties[key] = property;
                            i++;
                        }
                    }
                }
            }

            for(auto it : infos)
            {
                auto key = it.first;
                auto value = it.second;
                auto it_find = properties.find(key);

                wxPGProperty *property;

                if(it_find == properties.end())
                {
                    property = new wxStringProperty();
                    m_PropertyGrid->Append(property);
                }
                else
                {
                    property = it_find->second;
                }

                MA_ASSERT(property, L"La propriété est nulle. Cela ne devrait pas arriver.", std::runtime_error);

                auto property_name = property->GetName().ToStdWstring();
                auto property_value = property->GetValueAsString().ToStdWstring();

                if(property_name != key || property_value != value)
                {
                    property->SetName(key);
                    property->SetLabel(key);

                    wxVariant variant(value);
                    property->SetValue(variant);
                }
            }
        }
        this->Layout();
    }

    void VarIInfo2::BeginObservation(const ma::ObservablePtr &observable)
    {
        SetVar(observable);
    }

    void VarIInfo2::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(data.HasActions(ma::Var::Key::Obs::ms_EndSetIInfo) ||
           data.HasActions(ma::Var::Key::Obs::ms_EndSetIInfoValue) ||
           data.HasActions(ma::Var::Key::Obs::ms_EndAddIInfo) || data.HasActions(ma::Var::Key::Obs::ms_EndRemoveIInfo))
            SetVar(observable);
    }

    void VarIInfo2::EndObservation(const ma::ObservablePtr &observable)
    {
        SetVar(observable, true);
    }

    AddIInfo::AddIInfo(ma::VarPtr var,
                       wxWindow *parent,
                       wxWindowID id,
                       const wxString &title,
                       const wxPoint &pos,
                       const wxSize &size,
                       long style):
    wxDialog(parent, id, title, pos, size, style),
    m_Var(var)
    {
        this->SetSizeHints(wxDefaultSize, wxDefaultSize);

        wxBoxSizer *vertical_sizer;
        vertical_sizer = new wxBoxSizer(wxVERTICAL);

        wxBoxSizer *horizontal_top_sizer;
        horizontal_top_sizer = new wxBoxSizer(wxHORIZONTAL);

        wxBoxSizer *key_sizer;
        key_sizer = new wxBoxSizer(wxVERTICAL);

        m_KeyStaticText = new wxStaticText(this, wxID_ANY, _("Nom de la clef"), wxDefaultPosition, wxDefaultSize, 0);
        m_KeyStaticText->Wrap(-1);
        m_KeyStaticText->SetToolTip(_("Remarque elle doit être non-vide et unique pour l'instance ciblée."));

        key_sizer->Add(m_KeyStaticText, 0, wxALL | wxEXPAND, 5);

        m_KeyTxtCtrl =
            new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
        m_KeyTxtCtrl->SetToolTip(_("Remarque elle doit être non-vide et unique pour l'instance ciblée."));

        key_sizer->Add(m_KeyTxtCtrl, 0, wxALL | wxEXPAND, 5);

        horizontal_top_sizer->Add(key_sizer, 1, wxEXPAND, 5);

        vertical_sizer->Add(horizontal_top_sizer, 1, wxEXPAND, 5);

        wxBoxSizer *horizontal_bottom_sizer;
        horizontal_bottom_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_Append = new wxButton(this, wxID_ANY, _("Ajouter"), wxDefaultPosition, wxDefaultSize, 0);
        horizontal_bottom_sizer->Add(m_Append, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_Cancel = new wxButton(this, wxID_ANY, _("Annuler"), wxDefaultPosition, wxDefaultSize, 0);
        horizontal_bottom_sizer->Add(m_Cancel, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        vertical_sizer->Add(horizontal_bottom_sizer, 0, wxEXPAND, 5);

        this->SetSizer(vertical_sizer);
        this->Layout();

        this->Centre(wxBOTH);

        m_KeyTxtCtrl->Bind(wxEVT_COMMAND_TEXT_UPDATED, &AddIInfo::OnText, this);
        m_KeyTxtCtrl->Bind(wxEVT_COMMAND_TEXT_ENTER, &AddIInfo::OnTextEnter, this);
        m_Append->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddIInfo::OnAppend, this);
        m_Cancel->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AddIInfo::OnCancel, this);
    }

    AddIInfo::~AddIInfo()
    {}

    void AddIInfo::OnText(wxCommandEvent &event)
    {
        const std::wstring text = m_KeyTxtCtrl->GetValue().ToStdWstring();

        if(!m_Var->HasIInfoKey(text))
            m_KeyTxtCtrl->SetForegroundColour(*wxBLUE);
        else
            m_KeyTxtCtrl->SetForegroundColour(*wxRED);

        event.Skip();
    }

    void AddIInfo::OnTextEnter(wxCommandEvent &event)
    {
        const std::wstring key = m_KeyTxtCtrl->GetValue().ToStdWstring();

        if(!m_Var->HasIInfoKey(key))
            m_KeyTxtCtrl->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));
        else
            m_KeyTxtCtrl->SetForegroundColour(*wxRED);

        event.Skip();
    }

    void AddIInfo::OnAppend(wxCommandEvent &event)
    {
        const std::wstring key = m_KeyTxtCtrl->GetValue().ToStdWstring();

        if(!m_Var->HasIInfoKey(key))
            m_Var->InsertIInfoKeyValue(key, L"");

        EndModal(0);
        event.Skip();
    }

    void AddIInfo::OnCancel(wxCommandEvent &event)
    {
        EndModal(0);
        event.Skip();
    }

    VarDirectory::VarDirectory(ma::DirectoryVarPtr var,
                               wxWindow *parent,
                               wxWindowID win_id,
                               const wxPoint &pos,
                               const wxSize &in_size,
                               long style):
    Var<DirectoryVar>(var, parent, true, win_id, pos, in_size, style),
    m_BrowserButton(nullptr)
    {
        m_BrowserButton = new wxButton(this, wxID_ANY, M_Tr(L"..."), wxDefaultPosition, wxSize(30, 30), wxBU_EXACTFIT);
        // m_BottomHorizontalSizer->Add(m_BrowserButton, 0, wxALL | wxALIGN_CENTER_VERTICAL, 3);
        m_BottomHorizontalSizer->Add(m_BrowserButton, 0, wxALL, 3);

        this->Layout();

        m_BrowserButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &VarDirectory::OnOpenExplorer, this);
    }

    VarDirectory::~VarDirectory()
    {}

    void VarDirectory::OnOpenExplorer(wxCommandEvent &event)
    {
        wxDirDialog dlg(this,
                        M_Tr(L"Choisissez un dossier"),
                        wxStandardPaths::Get().GetExecutablePath(),
                        wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST);

        if(dlg.ShowModal() == wxID_OK)
        {
            std::wstring path = dlg.GetPath().ToStdWstring();

            // Si m_Var est vide alors le répertoire qu'il pointe est le courant.
            // Si path est le courant alors on entre pas dans la boucle et cela est
            // problème. Du coup on le test pas.
            // if(m_Var->Get() != path)
            {
                if(m_Var->IsValidString(path))
                    m_Var->fromString(path);
            }
        }
    }

    VarFileName::VarFileName(ma::FilePathVarPtr var,
                             wxWindow *parent,
                             wxWindowID win_id,
                             const wxPoint &pos,
                             const wxSize &in_size,
                             long style):
    Var<FilePathVar>(var, parent, true, win_id, pos, in_size, style),
    m_BrowserButton(nullptr)
    {
        m_BrowserButton = new wxButton(this, wxID_ANY, M_Tr(L"..."), wxDefaultPosition, wxSize(30, 30), wxBU_EXACTFIT);
        m_BottomHorizontalSizer->Add(m_BrowserButton, 0, wxALL | wxALIGN_CENTER_VERTICAL, 3);

        this->Layout();

        m_BrowserButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &VarFileName::OnOpenExplorer, this);
    }

    VarFileName::~VarFileName()
    {}

    void VarFileName::OnOpenExplorer(wxCommandEvent &event)
    {
        auto extensions = m_Var->GetExtensions();

        std::wstring wildcards = L"";
        for(auto extension : extensions)
            if(wildcards.empty())
                wildcards = L"*." + extension;
            else
                wildcards = L"|*." + extension;

        wxString message = wildcards.empty() ? M_Tr(L"Choisissez un fichier.") :
                                               M_Tr(L"Choisissez un fichier parmi les types suivant: ") + wildcards;

        // Si il n'y a pas de types définis, tout les types de fichiers seront pris
        // en compte.
        if(wildcards.empty())
            wildcards = L"*.*";

        // https://docs.wxwidgets.org/trunk/classwx_file_dialog.html#af3ff2981229bd2f892df0fa96fb9265d
        wxFileDialog dlg(this,
                         message,
                         wxStandardPaths::Get().GetExecutablePath(),
                         wxEmptyString,
                         wildcards,
                         wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST);

        if(dlg.ShowModal() == wxID_OK)
        {
            std::wstring path = dlg.GetPath().ToStdWstring();

            if(*m_Var != path)
            {
                if(m_Var->IsValidString(path))
                    m_Var->fromString(path);
            }
        }
    }

    ItemListDropTarget::ItemListDropTarget(ma::ItemHashSetVarPtr var): wxTextDropTarget(), m_Var(var)
    {}

    ItemListDropTarget::~ItemListDropTarget()
    {}

    bool ItemListDropTarget::OnDropText(wxCoord x, wxCoord y, const wxString &text)
    {
        // https://github.com/wxWidgets/wxWidgets/tree/master/samples/listctrl
        // MA_MSG(text)
        ma::Item::key_type key = text.ToStdWstring();
        if(ma::Item::HasItem(key))
            m_Var->Insert(key);

        return true;
    }

    ItemList::ItemList(ma::ItemHashSetVarPtr var,
                       wxWindow *parent,
                       wxWindowID win_id,
                       const wxPoint &pos,
                       const wxSize &in_size,
                       long style,
                       const wxString &name):
    Var<ma::ItemHashSetVar>(var, parent, false, win_id, pos, in_size, style),
    m_KeyStrMap(),
    m_StrKeyMap(),
    m_CurrentOrder(Insertion_Ascending)
    {
        m_OptionsHorizontalSizer = new wxBoxSizer(wxHORIZONTAL);

        m_RemoveButton =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_RemoveButton->SetBitmap(wxArtProvider::GetBitmap(wxART_MINUS, wxART_BUTTON));
        m_RemoveButton->SetToolTip(_("Supprime l'emplacement sélectionné ou par défaut le dernier."));

        m_OptionsHorizontalSizer->Add(m_RemoveButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_OptionsHorizontalSizer->Add(0, 0, 1, wxEXPAND, 5);

        m_InsertionOrder = new wxButton(this, wxID_ANY, _(" 1er "), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
        m_InsertionOrder->SetToolTip(_("Tri les items par ordre d'insertion"));

        m_OptionsHorizontalSizer->Add(m_InsertionOrder, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_AlphabeticalOrder =
            new wxButton(this, wxID_ANY, _(" a.b.c "), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
        m_AlphabeticalOrder->SetToolTip(_("Tri les items par ordre alphabétique."));

        m_OptionsHorizontalSizer->Add(m_AlphabeticalOrder, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);

        m_OptionsHorizontalSizer->Add(0, 0, 1, wxEXPAND, 5);

        m_AscendingOrder =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_AscendingOrder->SetBitmap(wxArtProvider::GetBitmap(wxART_GO_UP, wxART_BUTTON));
        m_AscendingOrder->SetToolTip(_("Tri les items du"));

        m_OptionsHorizontalSizer->Add(m_AscendingOrder, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_DescendingOrder =
            new wxBitmapButton(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW | 0);

        m_DescendingOrder->SetBitmap(wxArtProvider::GetBitmap(wxART_GO_DOWN, wxART_BUTTON));
        m_OptionsHorizontalSizer->Add(m_DescendingOrder, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_VerticalSizer->Add(m_OptionsHorizontalSizer, 0, wxEXPAND, 5);

        wxBoxSizer *m_KeyOrder;
        m_KeyOrder = new wxBoxSizer(wxHORIZONTAL);

        m_KeyOrderTextControl =
            new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
        m_KeyOrderTextControl->SetToolTip(_("Nommer la clef qui servira à trier les items alphabétiquement."));

        m_KeyOrder->Add(m_KeyOrderTextControl, 1, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_VerticalSizer->Add(m_KeyOrder, 1, wxEXPAND, 5);

        wxBoxSizer *m_ListSizer;
        m_ListSizer = new wxBoxSizer(wxHORIZONTAL);

        m_ItemsList =
            new wxListBox(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, wxLB_EXTENDED | wxLB_NEEDED_SB);

        m_ListSizer->Add(m_ItemsList, 1, wxALIGN_CENTER_VERTICAL | wxALL, 5);

        m_VerticalSizer->Add(m_ListSizer, 0, wxEXPAND, 5);

        this->SetSizer(m_VerticalSizer);
        this->Layout();

        // Connect Events
        m_InsertionOrder->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &ma::wx::ItemList::OnInsertOrder, this);
        m_AlphabeticalOrder->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &ma::wx::ItemList::OnAlphabeticalOrder, this);
        m_AscendingOrder->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &ma::wx::ItemList::OnAscendingOrder, this);
        m_DescendingOrder->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &ma::wx::ItemList::OnDescendingOrder, this);

        m_KeyOrderTextControl->Bind(wxEVT_COMMAND_TEXT_UPDATED, &ma::wx::ItemList::OnTextSearch, this);
        m_KeyOrderTextControl->Bind(wxEVT_COMMAND_TEXT_ENTER, &ma::wx::ItemList::OnTextEnterSearch, this);

        m_ItemsList->Bind(wxEVT_COMMAND_LISTBOX_SELECTED, &ma::wx::ItemList::OnItemsSelect, this);
        m_ItemsList->Bind(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED, &ma::wx::ItemList::OnItemsDClick, this);

        m_RemoveButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &ItemList::OnDelete, this);

        if(m_Var->IsObservable())
        {
            if(ma::Item::HasItem(ma::Item::Key::GetObservationManager()))
            {
                auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
                obs_manager->AddObservation<ma::wxObserverVar<ma::wx::ItemList>, ma::VarVar>(this, m_Var);
            }
        }

        SetDropTarget(new ItemListDropTarget(m_Var));
    }

    ItemList::~ItemList()
    {}

    void ItemList::RebuildIndex()
    {
        MA_ASSERT(m_KeyStrMap.size() == m_StrKeyMap.size(),
                  L"m_KeyStrMap et m_StrKeyMap devraient être de la même taille.",
                  std::runtime_error);

        size_t index = 0;
        auto it2 = m_StrKeyMap.begin();
        // std::pair<std::wstring, size_t>
        // std::pair<ma::Item::key_type, size_t>
        for(auto it : m_KeyStrMap)
        {
            std::pair<std::wstring, size_t> &str_index = it.second;
            std::pair<ma::Item::key_type, size_t> &key_index = it2->second;

            key_index.second = str_index.second = index;

            ++it2;
            index++;
        }
    }

    std::wstring ItemList::GetDisplayName(const ma::ItemPtr &item)
    {
        std::wstring name, separator = L" ";
        if(item->HasVar(ma::Item::Key::Var::GetName()))
            name += item->GetVar(ma::Item::Key::Var::GetName())->toString() + separator;

        name += item->GetTypeInfo().m_ClassName + separator + item->GetKey();

        return name;
    }

    void ItemList::RebuildList()
    {
        m_KeyStrMap.clear();
        m_StrKeyMap.clear();
        m_ItemsList->Clear();

        auto items = m_Var->Get();

        for(auto item : items)
        {
            std::wstring name(GetDisplayName(item));

            m_KeyStrMap[item->GetKey()] = {name, 0};
            m_StrKeyMap[name] = {item->GetKey(), 0};
        }

        RebuildIndex();
        SetupOrder(m_CurrentOrder, true);

        this->Layout();
    }

    void ItemList::Append(const ma::ItemPtr &item)
    {
        const std::wstring name(GetDisplayName(item));

        m_KeyStrMap[item->GetKey()] = {name, 0};
        m_StrKeyMap[name] = {item->GetKey(), 0};

        RebuildIndex();

        const size_t index_insertion = m_KeyStrMap[item->GetKey()].second;
        const size_t index_alphabetical = m_StrKeyMap[name].second;

        wxArrayString labels;
        labels.Add(wxString(name));

        switch(m_CurrentOrder)
        {
            case Insertion_Ascending:
                m_ItemsList->InsertItems(labels, index_insertion);
                break;

            case Alphabetical_Ascending:
                m_ItemsList->InsertItems(labels, index_alphabetical);
                break;

            case Insertion_Descending:
                m_ItemsList->InsertItems(labels, m_KeyStrMap.size() - index_insertion);
                break;

            case Alphabetical_Descending:
                m_ItemsList->InsertItems(labels, m_StrKeyMap.size() - index_alphabetical);
                break;

            default:
                MA_ASSERT(false,
                          L"m_CurrentOrder: " + std::to_wstring(m_CurrentOrder) + L" est invalide.",
                          std::runtime_error);
                break;
        }

        this->Layout();
    }

    void ItemList::Remove(const ma::ItemPtr &item)
    {
        auto key = item->GetKey();
        auto it_find_keystr = m_KeyStrMap.find(key);
        MA_ASSERT(it_find_keystr != m_KeyStrMap.end(),
                  L"Aucune clef dans m_KeyStrMap ne correspond à l'item: " + key,
                  std::invalid_argument);

        auto it_find_strkey = m_StrKeyMap.find(it_find_keystr->second.first);
        MA_ASSERT(it_find_strkey != m_StrKeyMap.end(),
                  L"Aucune clef dans m_StrKeyMap ne correspond à l'item: " + it_find_keystr->second.first,
                  std::invalid_argument);

        auto name = it_find_keystr->second.first;
        auto index = m_ItemsList->FindString(name, true);
        MA_ASSERT(
            wxNOT_FOUND != index, L"Aucune chaîne de caractère ne correspond au nom: " + name, std::invalid_argument);

        m_ItemsList->Delete(index);
        m_KeyStrMap.erase(it_find_keystr);
        m_StrKeyMap.erase(it_find_strkey);

        RebuildIndex();

        this->Layout();
    }

    void ItemList::BeginObservation(const ma::ObservablePtr &observable)
    {
        Var<ma::ItemHashSetVar>::BeginObservation(observable);

        // Nous commençons l'observation de la liste d'items.
        if(observable == m_Var)
            RebuildList();
    }

    void ItemList::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        Var<ma::ItemHashSetVar>::UpdateObservation(observable, data);

        if(observable == m_Var)
        {
            const auto actions = data.GetOrdered();
            for(const auto &action : actions)
            {
                if(action.first == ma::HashSetVar::Key::Obs::ms_EndInsertValue)
                {
                    ma::Item::key_type key = ma::Var::GetKeyValue(action.second).second;
                    Append(ma::Item::GetItem(key));

                    SetVar(m_Var);
                }

                if(action.first == ma::HashSetVar::Key::Obs::ms_EndEraseValue)
                {
                    ma::Item::key_type key = ma::Var::GetKeyValue(action.second).second;
                    Remove(ma::Item::GetItem(key));

                    SetVar(m_Var);
                }

                if(action.first == ma::Var::Key::Obs::ms_EndSetValue)
                    RebuildList();
            }
        }
    }

    void ItemList::EndObservation(const ma::ObservablePtr &observable)
    {
        Var<ma::ItemHashSetVar>::EndObservation(observable);

        m_ItemsList->Clear();
        m_KeyStrMap.clear();
        m_StrKeyMap.clear();
    }

    void ItemList::SetupOrder(ItemList::Sort sort_type, bool force_update)
    {
        if(sort_type != m_CurrentOrder || force_update)
        {
            m_ItemsList->Clear();

            switch(sort_type)
            {
                case Insertion_Ascending:
                    for(auto it : m_KeyStrMap)
                        m_ItemsList->Append(it.second.first);
                    break;

                case Alphabetical_Ascending:
                    for(auto it : m_StrKeyMap)
                        m_ItemsList->Append(it.first);
                    break;

                case Insertion_Descending:
                    {
                        wxArrayString labels;

                        for(auto it = m_KeyStrMap.begin(); it != m_KeyStrMap.end(); ++it)
                            labels.Insert(wxString(it->second.first), 0);

                        m_ItemsList->Insert(labels, 0);
                    }
                    break;

                case Alphabetical_Descending:
                    for(auto it = m_StrKeyMap.rbegin(); it != m_StrKeyMap.rend(); ++it)
                        m_ItemsList->Append(it->first);
                    break;

                default:
                    MA_ASSERT(false,
                              L"Le paramètre sort_type: " + std::to_wstring(sort_type) + L" est invalide.",
                              std::invalid_argument);
                    break;
            }

            m_CurrentOrder = sort_type;
        }
    }

    void ItemList::OnDelete(wxCommandEvent &event)
    {
        wxArrayInt selections;
        m_ItemsList->GetSelections(selections);

        std::vector<std::wstring> names;

        for(auto index : selections)
            names.push_back(m_ItemsList->GetString(index).ToStdWstring());

        for(auto name : names)
        {
            auto it_find = m_StrKeyMap.find(name);

            if(it_find != m_StrKeyMap.end())
                m_Var->Erase(it_find->second.first);
        }

        event.Skip();
    }

    void ItemList::OnInsertOrder(wxCommandEvent &event)
    {
        switch(m_CurrentOrder)
        {
            case Insertion_Ascending:
            case Alphabetical_Ascending:
                SetupOrder(Insertion_Ascending);
                break;

            case Insertion_Descending:
            case Alphabetical_Descending:
                SetupOrder(Insertion_Descending);
                break;

            default:
                MA_ASSERT(false,
                          L"m_CurrentOrder " + std::to_wstring(m_CurrentOrder) + L" est invalide.",
                          std::invalid_argument);
                break;
        }

        event.Skip();
    }

    void ItemList::OnAlphabeticalOrder(wxCommandEvent &event)
    {
        switch(m_CurrentOrder)
        {
            case Insertion_Ascending:
                SetupOrder(Alphabetical_Ascending);
                break;

            case Insertion_Descending:
                SetupOrder(Alphabetical_Descending);
                break;

            default:
                break;
        }

        event.Skip();
    }

    void ItemList::OnAscendingOrder(wxCommandEvent &event)
    {
        switch(m_CurrentOrder)
        {
            case Insertion_Descending:
                SetupOrder(Insertion_Ascending);
                break;

            case Alphabetical_Descending:
                SetupOrder(Alphabetical_Ascending);
                break;

            default:
                break;
        }

        event.Skip();
    }

    void ItemList::OnDescendingOrder(wxCommandEvent &event)
    {
        switch(m_CurrentOrder)
        {
            case Insertion_Ascending:
                SetupOrder(Insertion_Descending);
                break;

            case Alphabetical_Ascending:
                SetupOrder(Alphabetical_Descending);
                break;

            default:
                break;
        }

        event.Skip();
    }

    void ItemList::OnTextSearch(wxCommandEvent &event)
    {
        event.Skip();
    }

    void ItemList::OnTextEnterSearch(wxCommandEvent &event)
    {
        event.Skip();
    }

    void ItemList::OnItemsSelect(wxCommandEvent &event)
    {
        event.Skip();
    }

    void ItemList::OnItemsDClick(wxCommandEvent &event)
    {
        auto index = m_ItemsList->GetSelection();
        std::wstring label = m_ItemsList->GetString(index).ToStdWstring();

        auto it_find = m_StrKeyMap.find(label);

        if(it_find != m_StrKeyMap.end())
            MA_ROOT->m_Current->SetItem(ma::Item::GetItem(it_find->second.first));
    }

    const ma::TypeInfo &VarIInfo::GetwxVarIInfoTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxVarIInfoClassName(),
            L"Permet d'afficher dans un petit panel pour afficher les information d'instance de la variable.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(VarIInfo, wx, Observer)

    const ma::TypeInfo &VarIInfo2::GetwxVarIInfo2TypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxVarIInfo2ClassName(),
            L"Permet d'afficher dans un petit panel pour afficher les informations d'instance de la variable.",
            {}
        };
        // clang-format on
        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(VarIInfo2, wx, Observer)

    const ma::TypeInfo &AddIInfo::GetwxAddIInfoTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxAddIInfoClassName(),
            L"Permet d'ouvrir une fenêtre de dialogue pour ajouter une information à l'instance de la variable "
            L"désirée.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(AddIInfo, wx)

    const ma::TypeInfo &VarString::GetwxVarStringTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxVarStringClassName(),
            L"Permet d'afficher dans un petit panel des variables de tout type.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(VarString, wx, Observer)
    M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarString, Var, wx)

    const ma::TypeInfo &VarDirectory::GetwxVarDirectoryTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxVarDirectoryClassName(),
            L"Permet d'afficher dans un petit panel une variable de type, DirectoryVar.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(VarDirectory, wx, Observer)
    M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarDirectory, DirectoryVar, wx)

    const ma::TypeInfo &VarFileName::GetwxVarFileNameTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxVarFileNameClassName(),
            L"Permet d'afficher dans un petit panel une variable de type, ileNameVar.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(VarFileName, wx, Observer)
    M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarFileName, FileNameVar, wx)

    const ma::TypeInfo &VarItem::GetwxVarItemTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxVarItemClassName(),
            L"Permet d'afficher dans un petit panel représentant une variable pointant sur un item.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(VarItem, wx, Observer)
    M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(VarItem, ItemVar, wx)

    const ma::TypeInfo &ItemListDropTarget::GetwxItemListDropTargetTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxItemListDropTargetClassName(),
            L"Permet d'ajouter un ou plusieurs item via «Drag & Drop» à une variable de type ItemHashSetVar.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ItemListDropTarget, wx)

    const ma::TypeInfo &ItemList::GetwxItemListTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetwxItemListClassName(),
            L"Permet d'afficher dans un panel représentant une variable de type ItemHashSetVar.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ItemList, wx, Observer)
    M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(ItemList, ItemHashSetVar, wx)
} // namespace ma::wx