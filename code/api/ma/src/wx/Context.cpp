/// \file wx/Context.cpp
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/wx/Context.h"
#include "ma/Macros.h"

namespace ma::wx
{
    // Image//——————————————————————————————————————————————————————————————————————————————————————————————————————————
    Image::Image(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Data::Data(in_params, link, client),
    m_wxContext(std::dynamic_pointer_cast<ma::wx::Context>(link->GetContext())),
    m_Resource(std::dynamic_pointer_cast<ma::Resource>(client)),
    m_Image()
    {
        MA_ASSERT(m_wxContext, L"m_wxContext est nul.", std::invalid_argument);
        MA_ASSERT(m_Resource, L"m_Resource est nul.", std::invalid_argument);
        MA_ASSERT(m_wxContext->IsImage(m_Resource), L"m_Resource n'est pas une image.", std::invalid_argument);
    }

    Image::~Image() = default;

    bool Image::Load()
    {
        bool result = false;

        MA_ASSERT(m_Resource, L"m_Resource est nul.", std::invalid_argument);

        if(m_Resource && m_Resource->m_Path->Exist())
        {
            auto path = m_Resource->m_Path->toString();

            if(!m_Image.LoadFile(path))
                MA_WARNING(false, L"Une wxImage ayant pour fichier " + path + L" n'a peu être chargée.");
            else
                result = m_Image.IsOk();
        }

        return result;
    }

    bool Image::UnLoad()
    {
        if(m_Image.IsOk())
            m_Image.Destroy();

        return !m_Image.IsOk();
    }

    bool Image::Reload()
    {
        auto result = UnLoad();

        if(result)
            result = Load();

        return result;
    }
} // namespace ma::wx

// wx::ImageHandler/////////////////////////////////////////////////////////////

ma::wx::ImageHandler::ImageHandler(const ma::Item::ConstructorParameters &in_params, const wxImageHandler &handler):
ma::Item(in_params),
m_Handler(handler),
m_Name{AddReadOnlyMemberVar<ma::StringVar>(L"m_Name"s, m_Handler.GetName().ToStdWstring())},
m_Extensions{},
m_BitmapType{}
{
    ma::wx::ExtensionsVar::value_type extensions_set;

    wxArrayString extensions_array = m_Handler.GetAltExtensions();

    for(auto extension : extensions_array)
        extensions_set.insert(extension.MakeLower().ToStdWstring());

    auto extension = m_Handler.GetExtension();
    extensions_set.insert(extension.MakeLower().ToStdWstring());

    m_Extensions = AddReadOnlyMemberVar<ma::wx::ExtensionsVar>(L"m_Extensions"s, extensions_set);

    const auto handler_type = handler.GetType();
    m_BitmapType = AddReadOnlyMemberVar<ma::wx::BitmapTypeChoiceVar>(L"m_BitmapType"s, handler_type);
}

ma::wx::ImageHandler::~ImageHandler() = default;

std::shared_ptr<ma::wx::ImageHandler> ma::wx::ImageHandler::Clone(const ma::Item::key_type &key) const
{
    return ma::Item::CreateItem<ma::wx::ImageHandler>({GetParentKey(), true, m_CanBeDeleteOnLoad, key}, m_Handler);
}

const wxImageHandler &ma::wx::ImageHandler::GetHandler()
{
    return m_Handler;
}

// wx::BitmapTypes /////////////////////////////////////////////////////////////

#define M_BITMAP_TYPE_DEF(type)                                                                                        \
    {                                                                                                                  \
        std::wstring(ma::to_wstring(#type)), type                                                                      \
    }

ma::wx::BitmapTypesVar::value_type &ma::wx::BitmapTypes::Get()
{
    static ma::wx::BitmapTypesVar::value_type result{
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_INVALID),       M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_BMP),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_BMP_RESOURCE), // = wxBITMAP_TYPE_RESOURCE
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_ICO),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_ICO_RESOURCE),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_CUR),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_CUR_RESOURCE),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_XBM),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_XBM_DATA),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_XPM),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_XPM_DATA),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_TIFF), // = wxBITMAP_TYPE_TIF
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_TIFF_RESOURCE), // = wxBITMAP_TYPE_TIF_RESOURCE
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_GIF),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_GIF_RESOURCE),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_PNG),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_PNG_RESOURCE),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_JPEG),          M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_JPEG_RESOURCE),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_PNM),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_PCX),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_PCX_RESOURCE),  M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_PICT),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_PICT_RESOURCE), M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_ICON),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_ICON_RESOURCE), M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_ANI),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_IFF),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_TGA),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_MACCURSOR),     M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_MACCURSOR_RESOURCE),
        M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_MAX),           M_BITMAP_TYPE_DEF(wxBITMAP_TYPE_ANY)};

    return result;
}

// wx::Context /////////////////////////////////////////////////////////////////

const ma::Var::key_type &ma::wx::Context::Key::GetBitmapTypes()
{
    static const ma::Var::key_type key{L"m_BitmapType"};

    return key;
}

const ma::Item::key_type &ma::wx::Context::Key::GetContex()
{
    static const ma::Item::key_type key{L"13b376e7-a2bf-4551-8852-4be51fac50b9"};

    return key;
}

ma::wx::Context::Context(const ma::Item::ConstructorParameters &in_params, const std::wstring &name):
ma::Context({in_params, Key::GetContex(), true, false}, name, L"3af7f35a-abe5-4a63-a2b7-7c02973dd287"),
m_BitmapTypes{},
m_BitmapExtensions()
{
    Init();
}

void ma::wx::Context::Init()
{
    wxInitAllImageHandlers();
    const auto image_handlers = wxImage::GetHandlers();
    // const auto bitmap_handlers = wxBitmap::GetHandlers();

    ma::wx::ExtensionsVar::value_type extensions;

    auto key = GetKey();
    for(auto handler : image_handlers)
    {
        auto wx_image_handler = dynamic_cast<wxImageHandler *>(handler);

        // todo À voir si il faut les sauvegarder. Si oui il faudra leur attribuer une clef statique et définir la
        //  fonction « ma::ItemPtr CreateChildFromSave(const std::filesystem::directory_entry &dir_entry) override; »
        auto ma_image_handler = ma::Item::CreateItem<ma::wx::ImageHandler>({key, false, false}, *wx_image_handler);
        auto image_extensions = ma_image_handler->m_Extensions->Get();
        extensions.insert(image_extensions.begin(), image_extensions.end());
    }

    m_BitmapTypes = AddReadOnlyMemberVar<ma::wx::BitmapTypesVar>(Key::GetBitmapTypes(), ma::wx::BitmapTypes::Get());
    m_BitmapExtensions = AddReadOnlyMemberVar<ma::wx::ExtensionsVar>(L"m_BitmapExtensions"s, extensions);
}

ma::wx::Context::~Context() = default;

ma::ItemPtr ma::wx::Context::CreateChildFromSave(const std::filesystem::directory_entry &dir_entry)
{
    const auto class_name = GetTypeFromSave(dir_entry);
    const auto key = GetKeyFromSave(dir_entry);

    typedef ma::MatchType<ImageHandler> match_handlers;
    auto handlers = ma::Find<match_handlers>::GetDeque(GetKey(), match_handlers(), ma::Item::SearchMod::LOCAL);

    for(auto &handler : handlers)
    {
        if(handler->GetTypeInfo().m_ClassName == class_name)
        {
            auto clone = handler->Clone(key);
            RemoveChild(handler);
            return clone;
        }
    }

    return ma::Context::CreateChildFromSave(dir_entry);
}

bool ma::wx::Context::IsImage(ma::ResourcePtr resource)
{
    const auto res_extension = resource->m_Path->GetExt();
    const auto extensions = m_BitmapExtensions->Get();

    return extensions.find(res_extension) != extensions.end();
}

// ma::wx::ImagePtr ma::wx::Context::Get(ma::ResourcePtr resource)
//{
//    return ma::Item::CreateItem<ma::wx::Image>({GetKey()}, resource, GetPtr<ma::wx::Context>()));
//}

std::unordered_map<std::wstring, ma::wx::ImageHandlerPtr> ma::wx::Context::GetImageHandlers() const
{
    typedef MatchVar<ma::StringVar, ma::wx::ImageHandler> matcher;
    matcher match(Item::Key::Var::GetName());
    return ma::Find<matcher>::GetHashMap(GetKey(), match, ma::Item::SearchMod::LOCAL);
}

namespace ma
{
    const TypeInfo &wx::BitmapTypesVar::GetwxBitmapTypesVarTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxBitmapTypesVarClassName(),
            L"La classe « wx::BitmapTypesVar » est une variable représentant l'énumération des « wxBitmapType » défini"
            L" dans wxWidgets.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(BitmapTypesVar, wx, Var)

    const TypeInfo &wx::BitmapTypeChoiceVar::GetwxBitmapTypeChoiceVarTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxBitmapTypeChoiceVarClassName(),
            L"La classe «wx::BitmapTypeChoiceVar» variable représentant la valeur du type choisi parmi ceux définis "
            L"dans une variable ma::wx::BitmapTypesVar.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(BitmapTypeChoiceVar, wx, Var)

    const TypeInfo &wx::ExtensionsVar::GetwxExtensionsVarTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxExtensionsVarClassName(),
            L"La classe «wx::ExtensionsVar» variable représentent une série d'extensions de fichier.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ExtensionsVar, wx, Var)

    const TypeInfo &wx::Context::GetwxContextTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxContextClassName(),
            L"La classe «wx::Context» permet de définir le milieu dans lequel une ressource sera utilisée.",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        ContextLink::GetContextLinkClassName(),
                        ContextDataMaker::GetContextDataMakerClassName(),
                        wx::ImageHandler::GetwxImageHandlerClassName()
                    }
                },
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        ma::ContextManager::GetContextManagerClassName()
                    }
                }
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Context, wx, Context)
    // M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Context, wx)

    const TypeInfo &wx::Image::GetwxImageTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxImageClassName(), L"La classe «wx::Image» créer une wxImage à partir d'une ressource.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Image, wx, Data)

    const TypeInfo &wx::ImageHandler::GetwxImageHandlerTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxImageHandlerClassName(), L"La classe «wx::ImageHandler» fait le lien avec les wxImageHandler.",
            {}
        };
        // clang-format on
        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ImageHandler, wx, Item)
} // namespace ma
