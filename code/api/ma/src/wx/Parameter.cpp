/// \file wx/Parameter.cpp
/// \author Pontier Pierre
/// \date 2020-09-01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/Error.h"
#include "ma/Root.h"
#include "ma/wx/Parameter.h"
#include "ma/Find.h"
#include "ma/Variable.h"
#include "ma/ObservationManager.h"
#include "ma/ClassInfo.h"
#include "ma/wx/GuiAccess.h"

namespace ma::wx
{
    // ParameterManager //——————————————————————————————————————————————————————————————————————————————————————————————
    ParameterManager::ParameterManager(const ma::Item::ConstructorParameters &params):
    ma::Item::Item({params, ma::Item::Key::GetwxParameterManager(), true, false})
    {
        InitSystemColours();
    }

    void ParameterManager::InitSystemColours()
    {
        // clang-format off
        m_SysColourScrollBar =
        AddReadOnlyMemberVar<ma::wxColourVar>( L"m_SysColourScrollBar",
                                                wxSystemSettings::GetColour(wxSYS_COLOUR_SCROLLBAR));

        m_SysColourScrollBar->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                                  L"Valeurs de wxSYS_COLOUR_SCROLLBAR. "
                                                  L"La couleur grise des ascenseur.");

        m_SysColourDesktop = AddReadOnlyMemberVar<ma::wxColourVar>(L"m_SysColourDesktop",
                                                                    wxSystemSettings::GetColour(wxSYS_COLOUR_DESKTOP));

        m_SysColourDesktop->InsertIInfoKeyValue( ma::Var::Key::IInfo::ms_Doc,
                                                L"Valeurs de wxSYS_COLOUR_DESKTOP. La couleur du bureau.");

        m_SysColourActiveCaption =
        AddReadOnlyMemberVar<ma::wxColourVar>(L"m_SysColourActiveCaption",
                                               wxSystemSettings::GetColour(wxSYS_COLOUR_ACTIVECAPTION));

        m_SysColourActiveCaption->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                                      L"Valeurs de wxSYS_COLOUR_ACTIVECAPTION."
                                                      L"La couleur d'une zone de saisie active.");

        m_SysColourInactiveCaption =
        AddReadOnlyMemberVar<ma::wxColourVar>(L"m_SysColourInactiveCaption",
                                               wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVECAPTION));

        m_SysColourInactiveCaption->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                                        L"Valeurs de wxSYS_COLOUR_INACTIVECAPTION."
                                                        L" La couleur d'une zone de saisie inactive.");

        m_SysColourMenu = AddReadOnlyMemberVar<ma::wxColourVar>(L"m_SysColourMenu",
                                                                 wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

        m_SysColourMenu->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                             L"Valeurs de wxSYS_COLOUR_MENU. La couleur d'arrière plan des menus.");

        m_SysColourWindow = AddReadOnlyMemberVar<ma::wxColourVar>(L"m_SysColourWindow",
                                                                   wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));

        m_SysColourWindow->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                               L"Valeurs de wxSYS_COLOUR_WINDOW. La couleur d'arrière plan des menus.");

        m_SysColourWindowFrame =
        AddReadOnlyMemberVar<ma::wxColourVar>(L"m_SysColourWindowFrame",
                                               wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWFRAME));

        m_SysColourWindowFrame->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                                    L"Valeurs de wxSYS_COLOUR_WINDOWFRAME."
                                                    L" La couleur du cadre des fenêtres.");

        m_SysColourMenuText = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourMenuText", wxSystemSettings::GetColour(wxSYS_COLOUR_MENUTEXT));
        m_SysColourMenuText->InsertIInfoKeyValue( ma::Var::Key::IInfo::ms_Doc,
                                                 L"Valeurs de wxSYS_COLOUR_MENUTEXT. La couleur du texte des menus.");

        m_SysColourWindowMenu = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourWindowMenu", wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));
        m_SysColourWindowMenu->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_WINDOWTEXT. La couleur du texte des fenêtres.");

        m_SysColourCaptionText = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourCaptionText", wxSystemSettings::GetColour(wxSYS_COLOUR_CAPTIONTEXT));
        m_SysColourCaptionText->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc,
            L"Valeurs de wxSYS_COLOUR_CAPTIONTEXT. La couleur du texte des zones de saisies, des boîtes "
            L"redimensionnables(size box) et des cases en formes de flèches des ascenseurs .");

        m_SysColourActiveBorder = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourActiveBorder", wxSystemSettings::GetColour(wxSYS_COLOUR_ACTIVEBORDER));
        m_SysColourActiveBorder->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_ACTIVEBORDER. La couleur des bordures actives.");

        m_SysColourInactiveBorder = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourInactiveBorder", wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVEBORDER));
        m_SysColourInactiveBorder->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_INACTIVEBORDER. La couleur des bordures inactives.");

        m_SysColourAppWorkspace = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourAppWorkspace", wxSystemSettings::GetColour(wxSYS_COLOUR_APPWORKSPACE));
        m_SysColourAppWorkspace->InsertIInfoKeyValue( ma::Var::Key::IInfo::ms_Doc,
                                                     L"Valeurs de wxSYS_COLOUR_APPWORKSPACE. La couleur de l'espace de "
                                                     L"travail des fenêtres MDI (plusieurs fenêtres "
                                                     L"englobées dans une fenêtre parente).");

        m_SysColourHighLight = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourHighLight", wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHT));
        m_SysColourHighLight->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc,
            L"Valeurs de wxSYS_COLOUR_HIGHLIGHT. La couleur un ou plusieurs items sélectionnés.");

        m_SysColourHighLightText = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourHighLightText", wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHTTEXT));
        m_SysColourHighLightText->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc,
            L"Valeurs de wxSYS_COLOUR_HIGHLIGHTTEXT. La couleur du texte des items"
            L" sélectionnés.");

        m_SysColourBoutonFace = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourBoutonFace", wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
        m_SysColourBoutonFace->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_BTNFACE. La couleur de la face des boutons.");

        m_SysColourBoutonShadow = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourBoutonShadow", wxSystemSettings::GetColour(wxSYS_COLOUR_BTNSHADOW));
        m_SysColourBoutonShadow->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_BTNSHADOW. La couleur de l'ombrage du bouton.");

        m_SysColourGrayText = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourGrayText", wxSystemSettings::GetColour(wxSYS_COLOUR_GRAYTEXT));
        m_SysColourGrayText->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_GRAYTEXT. La couleur des textes désactivés.");

        m_SysColourBoutonText = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourBoutonText", wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
        m_SysColourBoutonText->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_BTNTEXT. La couleur du texte des boutons.");

        m_SysColourInactiveCaptionText = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourInactiveCaptionText", wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVECAPTIONTEXT));
        m_SysColourInactiveCaptionText->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc,
            L"Valeurs de wxSYS_COLOUR_INACTIVECAPTIONTEXT. La couleur du texte des zones de saisies inactives.");

        m_SysColourBoutonHighLight = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColourBoutonHighLight", wxSystemSettings::GetColour(wxSYS_COLOUR_BTNHIGHLIGHT));
        m_SysColourBoutonHighLight->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc,
            L"Valeurs de wxSYS_COLOUR_BTNHIGHLIGHT. La couleur de mise en "
            L"évidence des boutons. Elle est identiques à m_SysColour3DLight.");

        m_SysColour3DDarkShadow = AddReadOnlyMemberVar<ma::wxColourVar>(
            L"m_SysColour3DDarkShadow", wxSystemSettings::GetColour(wxSYS_COLOUR_3DDKSHADOW));
        m_SysColour3DDarkShadow->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc,
            L"Valeurs de wxSYS_COLOUR_3DDKSHADOW. La couleur des ombres sombres des éléments en trois dimensions.");

        m_SysColour3DLight = AddReadOnlyMemberVar<ma::wxColourVar>(L"m_SysColour3DLight",
                                                                    wxSystemSettings::GetColour(wxSYS_COLOUR_3DLIGHT));
        m_SysColour3DLight->InsertIInfoKeyValue(
             ma::Var::Key::IInfo::ms_Doc, L"Valeurs de wxSYS_COLOUR_3DLIGHT. La couleur des éléments en trois dimensions.");
        // clang-format on
    }

    ParameterManager::~ParameterManager() = default;

    std::wstring str_toupper(std::wstring s)
    {
        std::transform(s.begin(),
                       s.end(),
                       s.begin(),
                       [](unsigned char c)
                       {
                           return std::towupper(c);
                       });
        return s;
    }

    // https://cp-algorithms.com/string/string-hashing.html
    long long compute_hash(std::string const &s)
    {
        const int p = 31;
        const int m = 1e9 + 9;
        long long hash_value = 0;
        long long p_pow = 1;
        for(char c : s)
        {
            hash_value = (hash_value + (c - 'a' + 1) * p_pow) % m;
            p_pow = (p_pow * p) % m;
        }
        return std::abs(hash_value);
    }

    const ma::TypeInfo &ParameterManager::GetwxParameterManagerTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo result =
        {
            GetwxParameterManagerClassName(),
            L"Gestionnaire des items qui servent à éditer l'apparence des fenêtres de wxWidgets.",
            {
                {
                    ma::TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        Parameter::GetwxParameterClassName()
                    }
                }
            }
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ParameterManager, wx, Item)

    // Parameter //—————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Var::key_type &Parameter::Key::Var::GetwxClassName()
    {
        static const ma::Var::key_type key{L"m_wxClassName"};
        return key;
    }

    Parameter::Parameter(const ma::Item::ConstructorParameters &in_params, const std::wstring &class_name):
    ma::Item(in_params),
    m_ClassInfo{nullptr},
    m_wxClassName{}
    {
        MA_ASSERT(!class_name.empty(), L"Le nom de la classe est vide.", std::invalid_argument);
        wxString clas_name{class_name};
        m_ClassInfo = wxClassInfo::FindClass(clas_name);
        MA_ASSERT(m_ClassInfo,
                  L"Le nom de la classe « " + class_name + L" » ne correspond à aucune classe de la RTTI de wxWidgets.",
                  std::invalid_argument);
    }

    Parameter::~Parameter() = default;

    void Parameter::EndConstruction()
    {
        ma::Item::EndConstruction();
        m_wxClassName = AddReadOnlyMemberVar<StringVar>(Parameter::Key::Var::GetwxClassName(),
                                                        wxString(m_ClassInfo->GetClassName()).ToStdWstring());

        // clang-format off
        m_wxClassName->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                           L"Le nom du type de la fenêtre.\n"
                                           L"Pour wxWidgets cela correspond au nom retourné par la fonction "
                                           L"« wxClassInfo::GetClassName ».\n"
                                           L"Voir la documentation:\n"
                                           L"https://docs.wxwidgets.org/latest/classwx_class_info.html");
        // clang-format on
    }

    void Parameter::BeginObservation(const ma::ObservablePtr &observable)
    {}

    /// todo utiliser wxWindow::CallForEachChild avec wxWidgets 3.3.0
    template<typename T_Func>
    void CallForEachChild(wxWindow *window, const T_Func &fn)
    {
        MA_ASSERT(window, L"« window » est nul.", std::invalid_argument);
        fn(window);
        for(auto &child : window->GetChildren())
        {
            CallForEachChild(child, fn);
        }
    }

    void Parameter::UpdateObservation(const ma::ObservablePtr &observable, const ma::Observable::Data &data)
    {
        // On traite le cas que des variables ayant terminée leur mise à jour.
        // Ce qui simplifie l'utilisation, le nombre d'appels, mais réduit le champ
        // des possibles pour les classes enfants.
        const auto &actions = data.GetOrdered();
        for(const auto &action : actions)
        {
            if(action.first == ma::Var::Key::Obs::ms_EndSetValue)
            {
                if(ma::ClassInfoManager::IsInherit(observable, ma::Var::GetVarClassName()))
                {
                    ma::VarPtr var = std::dynamic_pointer_cast<ma::Var>(observable);

                    MA_ASSERT(var, L"La variable «var» est nulle. Cela ne devrait pas arriver.", std::invalid_argument);

                    auto gui_access = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

                    auto *frame = gui_access->GetMotherFrame();

                    // frame->IsKindOf
                    auto functor = [=](wxWindow *win)
                    {
                        if(win->GetClassInfo()->IsKindOf(m_ClassInfo))
                        {
                            UpdateVar(var, data, win);
                        }
                    };

                    CallForEachChild(frame, functor);
                }
            }
        }
    }

    void Parameter::EndObservation(const ma::ObservablePtr &observable)
    {}

    void Parameter::OnEnable()
    {
        Observer::OnEnable();
        Item::OnEnable();
    }

    void Parameter::OnDisable()
    {
        Observer::OnDisable();
        Item::OnDisable();
    }

    void Parameter::UpdateVar(ma::VarPtr var, const ma::Observable::Data &data, wxWindow *window)
    {}

    const ma::TypeInfo &Parameter::GetwxParameterTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo result =
        {
            GetwxParameterClassName(),
            L"Classe de base qui permet d'éditer l'apparence des fenêtres de wxWidgets.",
            {
                {
                    ma::TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        ParameterManager::GetwxParameterManagerClassName()
                    }
                }
            }
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Parameter, wx, Item, Observer)

    // WindowParameter //———————————————————————————————————————————————————————————————————————————————————————————————
    WindowParameter::WindowParameter(const ma::Item::ConstructorParameters &in_params):
    Parameter(in_params, wxWindow::ms_classInfo.GetClassName()) // L"wxWindow")
    {}

    void WindowParameter::EndConstruction()
    {
        Parameter::EndConstruction();
    }

    WindowParameter::~WindowParameter() = default;

    void WindowParameter::UpdateVar(ma::VarPtr var, const ma::Observable::Data &data, wxWindow *window)
    {
        Parameter::UpdateVar(var, data, window);
    }

    const ma::TypeInfo &WindowParameter::GetwxWindowParameterTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo result =
        {
            GetwxWindowParameterClassName(),
            L"Classe qui fait l'interface entre la fenêtre et l'item qui servira à modifier ses paramètres.",
            {}
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(WindowParameter, wx, Parameter::GetwxParameterClassHierarchy())
    // M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(WindowParameter, wx)

} // namespace ma::wx