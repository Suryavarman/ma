/// \file wx/Colour.cpp
/// \author Pontier Pierre
/// \date 2020-10-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/wx/Colour.h"

namespace ma::wx
{
    ColourPanel::ColourPanel(ma::wxColourVarPtr var,
                             wxWindow *parent,
                             wxWindowID win_id,
                             const wxPoint &pos,
                             const wxSize &in_size,
                             long style):
    Var<wxColourVar>(var, parent, true, win_id, pos, in_size, style),
    m_ObservationUpdatePhase(false),
    m_ColourPickerCtrl(nullptr)
    {
#ifdef wxCLRP_SHOW_ALPHA // Fonctionne seulement sur wxGTK et wxOSX
        m_ColourPickerCtrl = new wxColourPickerCtrl(
            this, wxID_ANY, var->Get(), wxDefaultPosition, wxSize(30, 30), wxCLRP_SHOW_ALPHA | wxCLRP_USE_TEXTCTRL);
#else
        m_ColourPickerCtrl =
            new wxColourPickerCtrl(this, wxID_ANY, var->Get(), wxDefaultPosition, wxSize(30, 30), wxCLRP_USE_TEXTCTRL);
#endif

        // m_BottomHorizontalSizer->Add(m_ColourPickerCtrl, 0, wxALL | wxALIGN_CENTER_VERTICAL, 3);
        m_BottomHorizontalSizer->Add(m_ColourPickerCtrl, 0, wxALL, 3);

        this->Layout();

        m_ColourPickerCtrl->Bind(wxEVT_COLOURPICKER_CHANGED, &ColourPanel::OnColourPickerChanged, this);

#if wxCHECK_VERSION(3, 1, 0)
        m_ColourPickerCtrl->Bind(wxEVT_COLOURPICKER_CURRENT_CHANGED, &ColourPanel::OnColourPickerCurrentChanged, this);

        m_ColourPickerCtrl->Bind(
            wxEVT_COLOURPICKER_DIALOG_CANCELLED, &ColourPanel::OnColourPickerDialogCancelled, this);
#endif
    }

    ColourPanel::~ColourPanel() = default;

    void ColourPanel::OnColourPickerChanged(wxColourPickerEvent &event)
    {
        if(!m_ObservationUpdatePhase)
        {
            m_Var->Set(event.GetColour());
        }
    }

    void ColourPanel::OnColourPickerCurrentChanged(wxColourPickerEvent &event)
    {
        if(!m_ObservationUpdatePhase)
        {
            m_Var->Set(event.GetColour());
        }
    }

    void ColourPanel::OnColourPickerDialogCancelled(wxColourPickerEvent &event)
    {
        if(!m_ObservationUpdatePhase)
        {
            m_Var->Set(m_ColourPickerCtrl->GetColour());
        }
    }

    void ColourPanel::SetVar(const ma::ObservablePtr &observable, bool end_observation)
    {
        Var<ma::wxColourVar>::SetVar(observable, end_observation);

        m_ObservationUpdatePhase = true;

        if(end_observation)
            m_ColourPickerCtrl->SetColour(*wxWHITE);
        else
            m_ColourPickerCtrl->SetColour(m_Var->Get());

        m_ObservationUpdatePhase = false;
    }

    const TypeInfo &ColourPanel::GetwxColourPanelTypeInfo()
    {
        static const TypeInfo info = {GetwxColourPanelClassName(),
                                      L"Permet d'afficher dans un petit panel une variable de type, wxColourVar.",
                                      {}};

        return info;
    }

    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ColourPanel, wx, Observer)
    M_CPP_MAKERWXVAR_TYPE_WITH_NAMESPACE(ColourPanel, wxColourVar, wx)
} // namespace ma::wx
