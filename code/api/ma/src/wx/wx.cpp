/// \file wx/wx.cpp
/// \author Pontier Pierre
/// \date 2020-11-22
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/wx/wx.h"

// std::wstring operator = (const wxString &str)
//{
//    #ifdef wxUSE_UNICODE
//        const std::wstring text = str.ToStdWstring();
//    #else
//        const std::wstring text = str.ToStdWstring();
//    #endif
//}

#if !defined(M_NOGUI)

    #include <wx/sysopt.h>
    #include <wx/recguard.h>
    #include <wx/apptrait.h>

namespace ma::wx
{
    wxTextCtrl *g_TextCtrl{nullptr};
    const wxColour g_TextErrorBackgroundColour{0, 0, 0}; // {119, 46, 44};
    const wxColour g_TextWarningBackgroundColour{0, 0, 0}; // {92, 79, 23};
    const wxColour g_TextDefaultBackgroundColour{0, 0, 0}; // {92, 79, 23};
    const wxColour g_TextErrorColour{247, 84, 100};
    const wxColour g_TextWarningColour{224, 187, 101};
    const wxColour g_TextDefaultColour{255, 255, 255};
    const wxFont g_TextFont{10,
                            wxFontFamily::wxFONTFAMILY_DEFAULT,
                            wxFontStyle::wxFONTSTYLE_NORMAL,
                            wxFontWeight::wxFONTWEIGHT_NORMAL};

    const wxTextAttr g_TextErrorAttr{g_TextErrorColour, g_TextErrorBackgroundColour, g_TextFont};
    const wxTextAttr g_TextWarningAttr{g_TextWarningColour, g_TextWarningBackgroundColour, g_TextFont};
    const wxTextAttr g_TextDefaultAttr{g_TextDefaultColour, g_TextDefaultBackgroundColour, g_TextFont};

    void AssertHandler(const wxString &file, int line, const wxString &func, const wxString &cond, const wxString &msg)
    {
        // If this option is set, we should abort immediately when assert happens.
        if(wxSystemOptions::GetOptionInt("exit-on-assert"))
            wxAbort();

        // FIXME MT-unsafe
        static int s_bInAssert = 0;

        wxRecursionGuard guard(s_bInAssert);
        if(guard.IsInside())
        {
            // can't use assert here to avoid infinite loops, so just trap
            wxTrap();

            return;
        }

        if(wxTheApp)
        {
            // let the app process it as it wants
            // Peux masquer les messages d'avertissements qui suits.
            // Si absent des erreurs pourraient ne pas être affichées.
            wxTheApp->OnAssertFailure(file.wc_str(), line, func.wc_str(), cond.wc_str(), msg.wc_str());
        }
    }
} // namespace ma::wx
#endif