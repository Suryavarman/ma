/// \file wx/AddItemDialog.cpp
/// \author Pontier Pierre
/// \date 2020-03-16
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

#include "ma/Macros.h"
#include "ma/Error.h"
#include "ma/Algorithm.h"
#include "ma/Translation.h"
#include "ma/Item.h"
#include "ma/Root.h"
#include "ma/RTTI.h"
#include "ma/wx/AddItemDialog.h"

namespace ma::wx
{
    AddItemDialog::AddItemDialog(const ma::Item::key_type &key_parent,
                                 wxWindow *parent,
                                 wxWindowID id,
                                 const wxString &title,
                                 const wxPoint &pos,
                                 const wxSize &size,
                                 long style):
    wxDialog(parent, id, title, pos, size, style),
    m_ItemParent(ma::Item::GetItem(key_parent))
    {
        wxArrayString class_check_list_choices = GetList();

        this->SetSizeHints(wxDefaultSize, wxDefaultSize);

        wxBoxSizer *vertical_sizer;
        vertical_sizer = new wxBoxSizer(wxVERTICAL);

        wxBoxSizer *horizontal_sizer_top;
        horizontal_sizer_top = new wxBoxSizer(wxHORIZONTAL);

        m_TypesListBox = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxSize(200, -1), 0, nullptr, wxLB_SINGLE);
        if(!class_check_list_choices.empty())
        {
            m_TypesListBox->InsertItems(class_check_list_choices, 0);
            if(!class_check_list_choices.empty())
            {
                m_TypesListBox->SetSelection(0);
            }
        }

        horizontal_sizer_top->Add(m_TypesListBox, 0, wxEXPAND | wxTOP | wxBOTTOM | wxLEFT, 5);

        m_HTMLWin = new wxHtmlWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHW_SCROLLBAR_AUTO);
        SetHtml();
        horizontal_sizer_top->Add(m_HTMLWin, 1, wxEXPAND | wxALL, 5);

        vertical_sizer->Add(horizontal_sizer_top, 1, wxEXPAND, 5);

        wxBoxSizer *horizontal_sizer_bottom;
        horizontal_sizer_bottom = new wxBoxSizer(wxHORIZONTAL);

        m_ValidateButton = new wxButton(this, wxID_ANY, M_Tr(L"Ajouter"), wxDefaultPosition, wxDefaultSize, 0);
        m_ValidateButton->SetToolTip(
            M_Tr(L"Valide le choix, créé une instance du type sélectionné et l'ajoute à l'item courant."));

        horizontal_sizer_bottom->Add(m_ValidateButton, 0, wxALL, 5);

        m_CancelButton = new wxButton(this, wxID_ANY, M_Tr(L"Annuler"), wxDefaultPosition, wxDefaultSize, 0);
        m_CancelButton->SetToolTip(M_Tr(L"Ferme la fenêtre sans ajouter de nouvel item."));

        horizontal_sizer_bottom->Add(m_CancelButton, 0, wxALL, 5);

        vertical_sizer->Add(horizontal_sizer_bottom, 0, wxEXPAND, 5);

        this->SetSizer(vertical_sizer);
        this->Layout();

        this->Centre(wxBOTH);

        // Connect Events
        if(!class_check_list_choices.empty())
        {
            m_TypesListBox->Connect(
                wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler(AddItemDialog::OnSelected), nullptr, this);

            m_ValidateButton->Connect(
                wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AddItemDialog::OnAdd), nullptr, this);
        }

        m_CancelButton->Connect(
            wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AddItemDialog::OnCancel), nullptr, this);
    }

    AddItemDialog::~AddItemDialog()
    {
        // Disconnect Events
        m_TypesListBox->Disconnect(
            wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler(AddItemDialog::OnSelected), nullptr, this);
        m_ValidateButton->Disconnect(
            wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AddItemDialog::OnAdd), nullptr, this);
        m_CancelButton->Disconnect(
            wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(AddItemDialog::OnCancel), nullptr, this);
    }

    void AddItemDialog::OnAdd(wxCommandEvent &event)
    {
        int selection = m_TypesListBox->GetSelection();

        MA_ASSERT(selection != wxNOT_FOUND, L"Il devrait avoir un type de sélectionné.", std::runtime_error);

        const std::wstring type = m_TypesListBox->GetString(selection).ToStdWstring();

        auto child = ma::root()->m_RttiManager->CreateItem(type, {ma::Item::Key::GetNoParent(), false});

        if(m_ItemParent->AcceptToAddChild(child))
        {
            m_ItemParent->AddChild(child);

            child->EndConstruction();

            ma::root()->m_Current->SetItem(child);
        }
        else
        {
            MA_MSG(L"L'item «" + m_ItemParent->GetKey() + L"» ne peut accepter 'ajouter en tant qu'enfant l'item «" +
                   child->GetKey() + L"».");
        }

        EndModal(0);
        event.Skip();
    }

    void AddItemDialog::OnCancel(wxCommandEvent &event)
    {
        EndModal(0);
        event.Skip();
    }

    void AddItemDialog::OnSelected(wxCommandEvent &event)
    {
        SetHtml();
        event.Skip();
    }

    wxArrayString AddItemDialog::GetList()
    {
        wxArrayString result;

        const auto &key = ma::MakerItem::Key::GetClassNameItemProperty();
        auto compare = ma::CompareItemsString(key);
        ma::ItemsOrder children = ma::Sort::SortItems(ma::Item::Key::GetRTTI(), compare, ma::Item::SearchMod::LOCAL);

        auto white_list = m_ItemParent->GetAuthorisedChildrenType();

        for(const auto &item : children)
        {
            // On ne veut pas les autres types de constructeurs, tel que m_BuilderClasseNameVar.
            if(auto opt = item->HasVarOpt(key))
            {
                const std::wstring class_name = opt.value()->toString();

                if(white_list.find(class_name) != white_list.end())
                {
                    // Pour chacune de ces classes, il faut pouvoir vérifier si ce parent est acceptable.
                    const auto authorised_type = ma::Item::GetAuthorisedParentsType(class_name);

                    if(authorised_type.count(m_ItemParent->GetTypeInfo().m_ClassName))
                        result.Add(class_name);
                }
            }
        }

        return result;
    }

    void AddItemDialog::SetHtml()
    {
        int selection = m_TypesListBox->GetSelection();

        if(selection != wxNOT_FOUND)
        {
            const std::wstring type = m_TypesListBox->GetString(selection).ToStdWstring();

            auto makers = ma::root()->m_RttiManager->GetRegisteredMakerItems();
            auto it_find = makers.find(type);

            MA_ASSERT(it_find != makers.end(),
                      L"Le nom de la classe «" + type + L"» ne correspond à aucune fabrique d'item enregistrée.",
                      std::runtime_error);

            ma::TypeInfo info = it_find->second->GetFactoryTypeInfo();

            std::wstringstream ss;

            ss << L"<h1>" << type << L"</h1>" << std::endl;

            std::vector<std::wstring> description = ma::Split(info.m_Description, '\n');
            ss << L"<p>" << std::endl;
            for(const auto &text : description)
            {
                ss << text << L"<br>" << std::endl;
            }
            ss << L"</p>" << std::endl;

            ss << L"<ul>" << std::endl;
            for(const auto &key_value : info.m_Data)
            {
                ss << L"<li>" << ma::toString(key_value) << L"</li>" << std::endl;
            }
            ss << L"</ul>" << std::endl;

            m_HTMLWin->SetPage(ss.str());
        }
    }
} // namespace ma::wx
