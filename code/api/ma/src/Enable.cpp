/// \file Enable.cpp
/// \author Pontier Pierre
/// \date 2023-05-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Enable.h"

namespace ma
{
    Enable::Enable(): m_Enable(false)
    {}

    Enable::~Enable() = default;

    bool Enable::IsEnable() const
    {
        return m_Enable;
    }

    void Enable::OnEnable()
    {
        m_Enable = true;
    }

    void Enable::OnDisable()
    {
        m_Enable = false;
    }
} // namespace ma