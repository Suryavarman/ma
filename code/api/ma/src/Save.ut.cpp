/// \file Save.ut.cpp
/// \author Pontier Pierre
/// \date 2022-03-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires de la classe Item et de sa partie sauvegarde.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

const auto backup_current_dir = std::filesystem::current_path();
const auto tests_path = backup_current_dir / L"tests"s;

template<const wchar_t* T_Name>
void save_setup()
{
    if(!std::filesystem::exists(tests_path))
        std::filesystem::create_directory(tests_path);

    const auto test_path = tests_path / T_Name;

    if(std::filesystem::exists(test_path))
        std::filesystem::remove_all(test_path);

    std::filesystem::create_directory(test_path);

    std::filesystem::current_path(test_path);

    ma::g_ConsoleVerbose = 2u;
    ma::Item::CreateRoot<ma::Item>();
    // Des items supprimés doivent pouvoir être réactivés.
    ma::Item::CreateItem<ma::Garbage>();

    // Pour les tests avec des observations
    ma::Item::CreateItem<ma::ClassInfoManager>();

    // Pour permettre d'instancier des types d'Items et de variables
    // dynamiquement.
    auto rtti = ma::Item::CreateItem<ma::RTTI>();

    ma::Item::CreateParameters params{rtti->GetKey()};

    ma::Item::CreateItem<ma::ItemMaker>(params);
    ma::Item::CreateItem<ma::FolderMaker>(params);
    //ma::Item::CreateItem<ma::wxGlobalParameterMaker>(params);

    // Déclarations des variables qui seront disponibles via la RTTI
    ma::Item::CreateItem<ma::DefineVarMaker>(params);
    ma::Item::CreateItem<ma::StringVarMaker>(params);
    ma::Item::CreateItem<ma::BoolVarMaker>(params);
    ma::Item::CreateItem<ma::StringHashSetVarMaker>(params);
    ma::Item::CreateItem<ma::ItemHashSetVarMaker>(params);
    ma::Item::CreateItem<ma::ItemVarMaker>(params);

    ma::Item::CreateItem<ma::IntVarMaker>(params);
    ma::Item::CreateItem<ma::LongVarMaker>(params);
    ma::Item::CreateItem<ma::LongLongVarMaker>(params);
    ma::Item::CreateItem<ma::UVarMaker>(params);
    ma::Item::CreateItem<ma::ULongVarMaker>(params);
    ma::Item::CreateItem<ma::ULongLongVarMaker>(params);
    ma::Item::CreateItem<ma::FloatVarMaker>(params);
    ma::Item::CreateItem<ma::DoubleVarMaker>(params);
    ma::Item::CreateItem<ma::LongDoubleVarMaker>(params);
    ma::Item::CreateItem<ma::EnumerationVarMaker>(params);
    ma::Item::CreateItem<ma::EnumerationChoiceVarMaker>(params);

    ma::Item::CreateItem<ma::PathVarMaker>(params);
    ma::Item::CreateItem<ma::DirectoryVarMaker>(params);
    ma::Item::CreateItem<ma::FilePathVarMaker>(params);
}

void save_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);

    std::filesystem::current_path(backup_current_dir);
    ma::g_ConsoleVerbose = 0u;
}

const wchar_t save_root_name[] = L"save_root";
Test(Save, save_root, .init = save_setup<save_root_name>, .fini = save_teardown)
{
    auto root = ma::Item::GetRoot();

    const auto root_path = std::filesystem::path(root->m_SavePath->toString());

    cr_assert(std::filesystem::equivalent(std::filesystem::current_path(), root_path.parent_path()));
    cr_assert_not(std::filesystem::exists(root_path));

    root->Save();

    cr_assert(std::filesystem::exists(root_path));
    cr_assert(std::filesystem::exists(root_path / L"item.cpp"s));

    auto garbage = ma::Item::GetItem(ma::Item::Key::GetGarbage());
    cr_assert_not(garbage->m_Savable);

    for(const std::filesystem::directory_entry& dir_entry : std::filesystem::directory_iterator{root_path})
    {
        if(dir_entry.is_directory())
        {
            cr_assert(std::filesystem::equivalent(root_path, dir_entry.path()));
        }
    }
}

const wchar_t save_child_name[] = L"save_child";
Test(Save, save_child, .init = save_setup<save_child_name>, .fini = save_teardown)
{
    auto root_item = ma::Item::GetRoot();
    const auto root_path = std::filesystem::path(root_item->m_SavePath->toString());

    cr_assert_not(std::filesystem::exists(root_path));

    auto child_item = ma::Item::CreateItem<ma::Item>();
    const auto child_path = std::filesystem::path(child_item->m_SavePath->toString());

    root_item->Save();

    cr_assert(std::filesystem::exists(root_path));
    cr_assert(std::filesystem::exists(root_path / L"item.cpp"s));

    cr_assert(std::filesystem::exists(child_path));
    cr_assert(std::filesystem::exists(child_path / L"item.cpp"s));

    cr_assert(std::filesystem::equivalent(root_path, child_path.parent_path()));
}

const wchar_t save_load_1_name[] = L"save_load_1";
Test(Save, save_load_1, .init = save_setup<save_load_1_name>, .fini = save_teardown)
{
    auto root_item = ma::Item::GetRoot();
    const auto root_path = std::filesystem::path(root_item->m_SavePath->toString());

    cr_assert_not(std::filesystem::exists(root_path));

    root_item->Save();

    cr_assert(std::filesystem::exists(root_path));
    cr_assert(std::filesystem::exists(root_path / L"item.cpp"s));

    auto child_item = ma::Item::CreateItem<ma::Item>();
    const auto child_path = std::filesystem::path(child_item->m_SavePath->toString());

    cr_assert_not(std::filesystem::exists(child_path));
    cr_assert_not(std::filesystem::exists(child_path / L"item.cpp"s));

    cr_assert(std::filesystem::equivalent(root_path, child_path.parent_path()));
    root_item->LoadItems();
    root_item->LoadVars();

    cr_assert_not(root_item->HasItem(child_item->GetKey(), ma::Item::SearchMod::LOCAL));
}

const wchar_t save_load_2_name[] = L"save_load_2";
Test(Save, save_load_2, .init = save_setup<save_load_2_name>, .fini = save_teardown)
{
    auto root_item = ma::Item::GetRoot();
    const auto root_path = std::filesystem::path(root_item->m_SavePath->toString());

    cr_assert_not(std::filesystem::exists(root_path));

    auto child_item = ma::Item::CreateItem<ma::Item>();
    const auto child_path = std::filesystem::path(child_item->m_SavePath->toString());

    root_item->Save();

    cr_assert(std::filesystem::exists(root_path));
    cr_assert(std::filesystem::exists(root_path / L"item.cpp"s));

    cr_assert(std::filesystem::exists(child_path));
    cr_assert(std::filesystem::exists(child_path / L"item.cpp"s));

    cr_assert(std::filesystem::equivalent(root_path, child_path.parent_path()));

    root_item->RemoveChild(child_item);

    root_item->LoadItems();
    root_item->LoadVars();

    cr_assert(root_item->HasItem(child_item->GetKey(), ma::Item::SearchMod::LOCAL));
}

const wchar_t save_load_save_var_item_1_name[] = L"save_load_save_var_item_1";
Test(Save, save_load_save_var_item_1, .init = save_setup<save_load_save_var_item_1_name>, .fini = save_teardown)
{
    auto root_item = ma::Item::GetRoot();

    auto item_1 = ma::Item::CreateItem<ma::Item>();
    auto item_2 = ma::Item::CreateItem<ma::Item>();
    auto item_3 = ma::Item::CreateItem<ma::Item>();

    auto var = item_2->AddVar<ma::ItemVar>(L"m_Var");

    var->SetItem(item_1);
    cr_assert_eq(var->GetItem(), item_1);

    root_item->Save();

    var->SetItem(item_3);
    cr_assert_eq(var->GetItem(), item_3);

    root_item->LoadItems();
    root_item->LoadVars();

    // La poubelle à item permet de retrouver les items portant la même clef.
    // std::wcout << L"item_1:         " << ma::toString(item_1) << std::endl;
    // std::wcout << L"item_2:         " << ma::toString(item_2) << std::endl;
    // std::wcout << L"item_3:         " << ma::toString(item_3) << std::endl;
    // std::wcout << L"var:            " << ma::toString(var) << std::endl;
    // std::wcout << L"var->GetItem(): " << ma::toString(var->GetItem()) << std::endl;
    cr_assert_eq(var->GetItem(), item_1);
}

const wchar_t save_load_save_var_item_2_name[] = L"save_load_save_var_item_2";
Test(Save, save_load_save_var_item_2, .init = save_setup<save_load_save_var_item_2_name>, .fini = save_teardown)
{
    // Le même test mais avec la présence d'un gestionnaire d'observation.
    auto root_item = ma::Item::GetRoot();

    ma::Item::CreateItem<ma::ObservationManager>();

    auto item_1 = ma::Item::CreateItem<ma::Item>();
    auto item_2 = ma::Item::CreateItem<ma::Item>();
    auto item_3 = ma::Item::CreateItem<ma::Item>();

    auto var = item_2->AddVar<ma::ItemVar>(L"m_Var");

    var->SetItem(item_1);
    cr_assert_eq(var->GetItem(), item_1);

    root_item->Save();

    var->SetItem(item_3);
    cr_assert_eq(var->GetItem(), item_3);

    root_item->LoadItems();
    root_item->LoadVars();

    // La poubelle à item permet de retrouver les items portant la même clef.
    // std::wcout << L"item_1:         " << ma::toString(item_1) << std::endl;
    // std::wcout << L"item_2:         " << ma::toString(item_2) << std::endl;
    // std::wcout << L"item_3:         " << ma::toString(item_3) << std::endl;
    // std::wcout << L"var:            " << ma::toString(var) << std::endl;
    // std::wcout << L"var->GetItem(): " << ma::toString(var->GetItem()) << std::endl;
    cr_assert_eq(var->GetItem(), item_1);
}


////////////////////////////////////////////////////////////////////////////////
// Tests d'observations.

/// Observe un item
class ObserverItem_Test : public ma::Observer
{
    public:
        /// Clef de l'observateur dans observer_item_map
        std::wstring m_Key;

        /// Le nombre d'items directement obtenu via GetItems().size()
        size_t m_CountItems;

        /// Le nombre d'item calculé via les opérations d'ajout et de
        /// suppression.
        size_t m_EstimateCountItems;

        /// Le nombre de variables directement obtenu via GetVars().size()
        size_t m_CountVars;

        /// Le nombre d'variables calculé via les opérations d'ajout et de
        /// suppression.
        size_t m_EstimateCountVars;

        /// L'item qui sera observé
        ma::ItemPtr m_Item;

        ObserverItem_Test(const std::wstring& key):
        ma::Observer(),
        m_Key(key),
        m_CountItems(0),
        m_EstimateCountItems(0),
        m_CountVars(0),
        m_EstimateCountVars(0)
        {}

        ~ObserverItem_Test()
        {}

        void BeginObservation(const ma::ObservablePtr& observable) override
        {
            //M_MSG("BeginObservation: Begin")
            //M_MSG("Observable clef: " + observable->GetKey())
            m_Item = ma::Item::GetItem(observable->GetKey());

            m_CountItems = m_Item->GetItems(ma::Item::SearchMod::LOCAL).size();
            m_EstimateCountItems = m_CountItems;
            m_CountVars = m_Item->GetVars().size();
            m_EstimateCountVars = m_CountVars;
            //M_MSG("BeginObservation: End")
        }

        void UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data) override
        {
            //M_MSG("UpdateObservation Begin")

            MA_ASSERT(m_Item,
                      L"m_Item n'a pas été initialiser.");

            MA_ASSERT(observable->GetKey() == m_Item->GetKey(),
                      L"Cet observeur observe un item à la fois.");

//            for(const auto& it: data)
//            {
//                M_MSG(it.first)
//            }

            //M_MSG("Observable clef: " + observable->GetKey())
            //M_MSG("Observable clef: " + m_Item->GetKey())

            m_EstimateCountItems += data.GetCount(ma::Item::Key::Obs::ms_AddItemBegin);
            m_EstimateCountItems -= data.GetCount(ma::Item::Key::Obs::ms_RemoveItemBegin);

            m_EstimateCountVars += data.GetCount(ma::Item::Key::Obs::ms_AddVar);
            m_EstimateCountVars -= data.GetCount(ma::Item::Key::Obs::ms_RemoveVarBegin);

            m_CountItems = m_Item->GetItems(ma::Item::SearchMod::LOCAL).size();
            m_CountVars = m_Item->GetVars().size();
            //M_MSG("UpdateObservation: End")
        }

        void EndObservation(const ma::ObservablePtr& observable) override
        {
            //M_MSG("EndObservation: Begin")

            MA_ASSERT(observable->GetKey() == m_Item->GetKey(),
                      L"Cet observateur observe un item à la fois.");

            //M_MSG("Observable clef: " + observable->GetKey())
            m_CountItems = 0;
            m_EstimateCountItems = 0;
            m_CountVars = 0;
            m_EstimateCountVars = 0;
            m_Item = nullptr;
            //M_MSG("EndObservation: End")
        }

        const ma::TypeInfo& GetTypeInfo() const override
        {
            static const ma::TypeInfo result
            {
                    L"ObserverItem_Test",
                    L"Pour tester la mécanique d'observations des items",
                    {}
            };

            return result;
        }
};
typedef ObserverItem_Test* ObserverItem_TestPtr;

typedef std::unordered_map<std::wstring, ObserverItem_TestPtr> ObserverItem_TestMap;

static ObserverItem_TestMap observer_item_map = {};

class ObserverItem_TestVar : public ma::ObserverVar
{
    protected:
        void SetFromItem(const std::wstring& str) override
        {
            auto it_find = observer_item_map.find(str);

            MA_ASSERT(it_find != observer_item_map.end(),
                      L"La clef ne représente aucun observateur.",
                      std::invalid_argument);

            m_Value = it_find->second;
        }

        ma::VarPtr Copy() const override
        {
            return std::shared_ptr<ObserverItem_TestVar>(new ObserverItem_TestVar(GetConstructorParameters(), m_Value));
        }

    public:
        using ma::ObserverVar::ObserverVar;

        /// Renvoie la clef de l'observateur dans observer_item_map
        std::wstring toString() const override
        {
            return dynamic_cast<ObserverItem_Test*>(m_Value)->m_Key;
        }

        std::wstring GetKeyValueToString() const override
        {
            return ma::toString(std::make_pair(m_Key, toString()));
        }

        /// À partir de la clef de l'observateur, il retrouve la référence à celui-ci.
        void fromString(const std::wstring& str) override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être modifiée.",
                      std::logic_error);

            SetFromItem(str);
        }

        /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
        /// \remarks à utiliser avant d'appeler fromString.
        bool IsValidString(const std::wstring& str)const override
        {
            return observer_item_map.count(str);
        }

        bool IsValid() const override
        {
            return observer_item_map.count(dynamic_cast<ObserverItem_Test*>(m_Value)->m_Key);
        }

        void Reset() override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être modifiée.",
                      std::logic_error);
        }
};
typedef std::shared_ptr<ObserverItem_TestVar> ObserverItem_TestVarPtr;

const wchar_t save_load_observer_item_1_name[] = L"save_load_observer_item_1";
Test(Save, save_load_observer_item_1, .init = save_setup<save_load_observer_item_1_name>, .fini = save_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();

    const std::wstring key_observer(L"obsvervateur_0");
    ObserverItem_TestPtr observer_ptr = new ObserverItem_Test(key_observer);

    auto item = ma::Item::CreateItem<ma::Item>();
    auto key = item->GetKey();
    auto child = ma::Item::CreateItem<ma::Item>({key});

    obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, item);

    cr_assert(obs_manager->HasObservation(observer_ptr, ma::Item::GetItem(key)));
    cr_assert_not(obs_manager->HasObservation(observer_ptr, child));
    cr_assert(obs_manager->HasObserver(observer_ptr));
    cr_assert(obs_manager->HasObservable(ma::Item::GetItem(key)));

    cr_assert_eq(observer_ptr->m_CountItems, 1);
    cr_assert_eq(observer_ptr->m_EstimateCountItems, 1);
    cr_assert_eq(observer_ptr->m_CountVars, item->GetVars().size());
    cr_assert_eq(observer_ptr->m_EstimateCountVars, item->GetVars().size());

    delete observer_ptr;
}
