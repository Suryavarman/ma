/// \file Version.cpp
/// \author Pontier Pierre
/// \date 2022-02-12
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Version.h"
#include "ma/Git.h"

#include <sstream>

namespace ma
{
    std::wstring GetBuildId()
    {
        std::wstringstream ss;
        // ss << MA_BUILD_YEAR <<  L"a" << MA_BUILD_MONTH << L"m" << MA_BUILD_DAY << L"j_";
        // ss << MA_BUILD_HOUR << L"h" << MA_BUILD_MINUTES << L"min" << MA_BUILD_SEC << L"s" << std::endl;
        ss << MA_BUILD_YEAR << MA_BUILD_MONTH << MA_BUILD_DAY << L"_";
        ss << MA_BUILD_HOUR << MA_BUILD_MINUTES << MA_BUILD_SEC << std::endl;
        return ss.str();
    }

    std::wstring GetRevisiondId()
    {
        auto full_hash = ma::to_wstring(VCS_FULL_HASH);
        auto short_hash = full_hash.substr(0, 8u);

        std::wstringstream ss;
        ss << VCS_BRANCH << L"_" << short_hash << std::endl;

        return ss.str();
    }

    std::wstring GetVersion()
    {
        std::wstring status = ma::to_wstring(MA_STATUS);

        if(!status.empty())
            status = L"_" + status;

        std::wstringstream ss;
        ss << MA_MAJOR << L"." << MA_MINOR << status << L"." << MA_BUILD << L"." << MA_REVISION << std::endl;

        return ss.str();
    }
} // namespace ma
