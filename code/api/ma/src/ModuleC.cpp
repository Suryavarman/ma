/// \file ModuleC.cpp
/// \author Pontier Pierre
/// \date 2023-11-15
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// La partie du code concernant le chargement dynamique des librairies est basé sur le code de OgreDynLib.cpp
/// Vous pouvez retrouvez le code original ici https://github.com/OGRECave/ogre/blob/master/OgreMain/src/OgreDynLib.cpp
///

#include "ma/ModuleC.h"
#include "ma/ClassInfo.h"
#include "ma/Config.h"

#if MA_PLATFORM == MA_PLATFORM_LINUX
    // https://linux.die.net/man/3/dlsym
    #define MA_DYNLIB_LOAD(a) dlopen(a, m_LinuxFlag); // | RTLD_DEEPBIND);//RTLD_LOCAL) // RTLD_LAZY
    #define MA_DYNLIB_GETSYM(a, b) dlsym(a, b)
    #define MA_DYNLIB_UNLOAD(a) dlclose(a)
#elif MA_PLATFORM == MA_PLATFORM_WIN32
// we can not use LOAD_WITH_ALTERED_SEARCH_PATH with relative paths
    #define MA_DYNLIB_LOAD(a) LoadLibraryEx(a, NULL, 0)
    #define MA_DYNLIB_GETSYM(a, b) GetProcAddress(a, b)
    #define MA_DYNLIB_UNLOAD(a) !FreeLibrary(a)
#elif MA_PLATFORM == MA_PLATFORM_APPLE
    #define MA_DYNLIB_LOAD(a) mac_loadDylib(a)
    #define FRAMEWORK_LOAD(a) mac_loadFramework(a)
    #define MA_DYNLIB_GETSYM(a, b) dlsym(a, b)
    #define MA_DYNLIB_UNLOAD(a) dlclose(a)
#endif

#if MA_PLATFORM == MA_PLATFORM_WIN32
    #define WIN32_LEAN_AND_MEAN
    #if !defined(NOMINMAX) && defined(_MSC_VER)
        #define NOMINMAX // required to stop windows.h messing up std::min
    #endif
    #include <windows.h>
#else
extern "C"
{
    #include <dlfcn.h>
}
#endif

#if MA_PLATFORM == MA_PLATFORM_APPLE
    #include "macUtils.h"
extern "C"
{
    #include <unistd.h>
    #include <sys/param.h>
    #include <CoreFoundation/CoreFoundation.h>
}
#endif

namespace ma
{
    // ModuleTypeC //———————————————————————————————————————————————————————————————————————————————————————————————————
    ModuleC::ModuleC(const Item::ConstructorParameters &in_params,
                     const ContextLinkPtr &link,
                     const ItemPtr &client,
                     ModuleCMakerPtr maker):
    Module(in_params, link, client, maker),
    m_ObservationManager(GetItem<ObservationManager>(Item::Key::GetObservationManager())),
    m_ClassInfoManager(GetItem<ClassInfoManager>(Item::Key::GetClassInfoManager())),
    m_PluginPath(),
    m_Handle(nullptr),
    m_MakerC(maker)
#if MA_PLATFORM == MA_PLATFORM_LINUX
    ,
    m_LinuxFlag(RTLD_NOW | RTLD_LOCAL)
#endif
    {
        MA_ASSERT(m_MakerC, L"m_MakerC est nul.", std::logic_error);
    }

    ModuleC::~ModuleC()
    {
        if(m_Handle)
        {
            typedef void (*MA_DLL_DELETE_PLUGIN)(void);
#ifdef __GNUC__
            __extension__
#endif
                auto func = reinterpret_cast<MA_DLL_DELETE_PLUGIN>(getSymbol(L"dllDeletePlugin"));
            MA_WARNING(func,
                       L"Il a été impossible de trouvé le symbole « dllDeletePlugin » dans le module « " +
                           m_Name->toString() + L" » Erreurs: " + DynLibError());

            // this must call uninstallPlugin
            if(func)
                func();

            // dllDeletePlugin s'assure que toutes les ressources du module sont bien désallouées.
            MA_DYNLIB_UNLOAD(m_Handle);

            m_Handle = nullptr;
        }
    }

    void ModuleC::EndConstruction()
    {
        m_EntryPoint = AddReadOnlyMemberVar<ResourceVar>(L"m_EntryPoint");

        auto &folder = m_Folder->GetItem();
        const std::filesystem::directory_entry folder_path{folder->m_Path->Get()};
        if(auto const opt = m_MakerC->Match(folder_path))
        {
            m_PluginPath = opt.value();
        }
        else
        {
            const auto &path = folder_path.path();
            MA_ASSERT(false,
                      L"Le dossier « " + path.wstring() +
                          L" » ne contient aucun fichier d'entré valide portant le nom « " + path.stem().wstring() +
                          L" ». ",
                      std::logic_error);
        }

        if(auto const opt = m_MakerC->HasConfig(folder_path))
        {
            m_ConfigPath = opt.value();
        }

        Module::EndConstruction();

        // On va observer le dossier.
        // Dès que celui-ci possède le fichier du point d'entrée, nous relancerons Load().
        // Nous le faisons après EndConstruction pour être sûr que l'élément soit présent dans son parent.
        // Cela permettra à ma::ModuleManager::GetModules de renvoyer ce module pour la construction des dépendances
        // des éléments construit dans le module.
        // \todo Load sera donc lancé deux fois inutilement. Veiller à ce qu'il soit lancé une seul fois.
        m_ObservationManager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, folder);
    }

    void ModuleC::BeginObservation(const ObservablePtr &observable)
    {
        Module::BeginObservation(observable);

        const auto &folder = m_Folder->GetItem();
        if(folder == observable)
        {
            const auto &resources = folder->GetResources();
            for(auto &&[filename, resource] : resources)
            {
                if(resource && resource->m_Path->Get() == m_PluginPath)
                {
                    SetVarFromValue<ResourceVar>(m_EntryPoint->GetKey(), resource);

                    if(auto opt_config = folder->HasResource(m_ConfigPath))
                        SetVarFromValue<ResourceVar>(m_Config->GetKey(), opt_config.value());

                    OnEnable();
                }
            }
        }
    }

    void ModuleC::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        Module::UpdateObservation(observable, data);

        const auto &folder = m_Folder->GetItem();
        if(folder == observable)
        {
            const auto &actions = data.GetOrdered();
            for(const auto &action : actions)
            {
                // La construction n'est pas fini, la ressource ne sera pas observable.
                // Nous attendrons donc l'appel à ms_ConstructionEnd
                if(action.first != ma::Item::Key::Obs::ms_AddItemBegin &&
                   action.first != ma::Item::Key::Obs::ms_AddItemEnd &&
                   action.first != ma::Item::Key::Obs::ms_ConstructionEnd)
                {
                    if(auto opt = folder->HasItemOpt<Resource>(action.second, SearchMod::LOCAL))
                    {
                        auto resource = opt.value();
                        if(resource && resource->m_Path->Get() == m_PluginPath)
                        {
                            if(action.first == ma::Folder::Key::Obs::ms_ConstructionEnd)
                            {
                                SetVarFromValue<ResourceVar>(m_EntryPoint->GetKey(), resource);

                                if(auto opt_config = folder->HasResource(m_ConfigPath))
                                    SetVarFromValue<ResourceVar>(m_Config->GetKey(), opt_config.value());

                                // Deuxième appel à Load mais cette fois-ci les variables pointes sur des éléments.
                                OnEnable();
                                //                                MA_WARNING(Load(),
                                //                                           L"Le chargement du module « " +
                                //                                           m_PluginPath.wstring() + L" » a échoué.");
                            }
                            else if(action.first == ma::Item::Key::Obs::ms_RemoveItemBegin)
                            {
                                OnDisable();
                                //                                MA_WARNING(UnLoad(),
                                //                                           L"Le chargement du module « " +
                                //                                           m_PluginPath.wstring() + L" » a échoué.");
                            }
                        }
                    }
                }
            }
        }
    }

    void ModuleC::EndObservation(const ObservablePtr &observable)
    {
        Module::EndObservation(observable);
    }

    bool ModuleC::Load()
    {
        MA_ASSERT(m_EntryPoint, L"m_EntryPoint est nul.", std::logic_error);
        MA_ASSERT(m_Config, L"m_Config est nul.", std::logic_error);

        const auto config_resource = m_Config->GetItem();
        if(config_resource)
        {
            const std::filesystem::path path = config_resource->m_Path->toString();
            auto config = ma::Config(path);
            config.Load();

            const auto &data = config.m_Datas;
            if(!data.empty())
            {
#if MA_PLATFORM == MA_PLATFORM_LINUX
                const auto it_find_linux = data.find(L"linux");
                if(it_find_linux != data.end())
                {
                    const auto &data_linux = it_find_linux->second;
                    const auto &it_find_tag = data_linux.find(L"flag");
                    if(it_find_tag != data_linux.end())
                    {
                        const auto &tags = it_find_tag->second;
                        const ma::Serialization::Parser parser{tags};
                        if(!parser.HasError())
                        {
                            typedef std::set<std::wstring> Tags;
                            if(parser.IsValid<Tags>())
                            {
                                const auto values = parser.GetValue<Tags>();
                                m_LinuxFlag = GetLinuxFlag(values);
                            }
                        }
                    }
                }
#elif MA_PLATFORM == MA_PLATFORM_WIN32
                const auto it_find_windows = data.find(L"windows");
                if(it_find_windows != data.end())
                {
                    const auto &data_windows = it_find_windows->second;
                }
#elif MA_PLATFORM == MA_PLATFORM_APPLE
                const auto it_find_mac = data.find(L"mac");
                if(it_find_mac != data.end())
                {
                    const auto &data_mac = it_find_mac->second;
                }
#endif
            }
        }

        const auto resource = m_EntryPoint->GetItem();

        // Le dossier est observé, dès que celui-ci se voit ajouter ses ressources, alors m_EntryPoint pointera sur la
        // ressource et cette fonction Load sera appelée.
        if(resource && Module::Load())
        {
            const auto &plugin_name = m_Name->toString();

            const std::filesystem::path path = resource->m_Path->toString();

            if(path.extension() == ".framework")
            {
#if MA_PLATFORM == MA_PLATFORM_APPLE
                m_Handle = (MA_DYNLIB_HANDLE)MA_FRAMEWORK_LOAD(path.string().c_str());
#else
                MA_ASSERT(false,
                          L"L'extension « .framework » ne fonctionne que sur les plateformes d'Apple.",
                          std::invalid_argument);
#endif
            }
            else
            {
                m_Handle = (MA_DYNLIB_HANDLE)MA_DYNLIB_LOAD(path.string().c_str());
            }

            MA_WARNING(m_Handle,
                       L"Impossible de charger le module « " + path.wstring() +
                           L" ».\n"
                           L"Les erreurs sont les suivantes: \n" +
                           DynLibError());

            if(m_Handle)
            {
                // Trouvé une manière plus récente pour faire ce travail de cast. Ex: std::bind.
                typedef void (*MA_DLL_START_PLUGIN)(const std::filesystem::path &module_path);

#ifdef __GNUC__
                __extension__
#endif
                    MA_DLL_START_PLUGIN func = reinterpret_cast<MA_DLL_START_PLUGIN>(getSymbol(L"dllStartPlugin"));

                MA_WARNING(func,
                           L"Il a été impossible de trouvé le symbole « dllStartPlugin » dans la librairie « " +
                               plugin_name + L" ».");

                // Cela doit appeler « installPlugin ».
                if(func)
                {
                    func(path.parent_path().string());
                    return true;
                }
            }
        }

        return false;
    }

    bool ModuleC::UnLoad()
    {
        if(m_Handle)
        {
            typedef void (*MA_DLL_STOP_PLUGIN)(void);
#ifdef __GNUC__
            __extension__
#endif
                auto func = reinterpret_cast<MA_DLL_STOP_PLUGIN>(getSymbol(L"dllStopPlugin"));
            MA_WARNING(func,
                       L"Il a été impossible de trouvé le symbole « dllStopPlugin » dans le module « " +
                           m_Name->toString() + L" » Erreurs: " + DynLibError());

            // Cela doit appeler dllStopPlugin
            if(func)
                func();

            if(!func)
                return false;
        }

        return Module::UnLoad();
    }

    void *ModuleC::getSymbol(const std::wstring &name) const noexcept
    {
        const auto str_symbole = ma::to_string(name);
        return (void *)MA_DYNLIB_GETSYM(m_Handle, str_symbole.c_str());
    }

#if MA_PLATFORM == MA_PLATFORM_LINUX
    int ModuleC::GetLinuxFlag(const std::set<std::wstring> &flags)
    {
        if(!flags.empty())
        {
            auto get_flag = [](const std::wstring &str) -> int
            {
                static const std::map<std::wstring, int> flags = {
                    {L"RTLD_LAZY", RTLD_LAZY},
                    {L"RTLD_NOW", RTLD_NOW},
                    {L"RTLD_GLOBAL", RTLD_GLOBAL},
                    {L"RTLD_LOCAL", RTLD_LOCAL},
                    {L"RTLD_NODELETE", RTLD_NODELETE},
                    {L"RTLD_NOLOAD", RTLD_NOLOAD},
                    {L"RTLD_DEEPBIND", RTLD_DEEPBIND},
                };

                const auto flag_find = flags.find(str);

                if(flag_find != flags.end())
                    return flag_find->second;
                else
                    return 0;
            };

            int value{get_flag(*flags.begin())};

            for(auto it = flags.begin()++; it != flags.end(); ++it)
            {
                value |= get_flag(*it);
            }

            if(value != 0)
                return value;
        }

        return RTLD_NOW | RTLD_LOCAL;
    }
#endif

    std::wstring ModuleC::DynLibError()
    {
        try
        {
#if MA_PLATFORM == MA_PLATFORM_WIN32
            LPTSTR lpMsgBuf;
            FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                          NULL,
                          GetLastError(),
                          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                          (LPTSTR)&lpMsgBuf,
                          0,
                          NULL);
            std::string ret = lpMsgBuf;
            // Free the buffer.
            LocalFree(lpMsgBuf);
            return ma::to_wstring(ret);
#elif MA_PLATFORM == MA_PLATFORM_LINUX || MA_PLATFORM == MA_PLATFORM_APPLE
            const auto error_result = dlerror();
            const std::string str_error = error_result != nullptr ? error_result : "null";
            return ma::to_wstring(str_error);
#else
            return {};
#endif
        }
        catch(const std::exception &e)
        {
            std::cout << e.what() << std::endl;
            return {};
        }
    }

    void ModuleC::MakerReplacement(ma::DataMakerPtr old_maker, ma::DataMakerPtr new_maker)
    {
        Module::MakerReplacement(old_maker, new_maker);

        if(m_MakerC == old_maker and ma::ClassInfoManager::IsInherit(new_maker, m_MakerC->GetTypeInfo().m_ClassName))
        {
            auto maker = std::dynamic_pointer_cast<ma::ModuleCMaker>(new_maker);
            MA_ASSERT(
                maker, L"new_maker n'a peut être converti en ModuleCMaker alors qu'il en hérite.", std::runtime_error);
            m_MakerC = std::move(maker);
        }
    }

    const TypeInfo &ModuleC::GetModuleCTypeInfo()
    {
        const auto module_typeinfo = Module::GetModuleTypeInfo();
        // clang-format off
        static const TypeInfo info =
        {
            GetModuleCClassName(),
            L"La classe « ModuleC » permet charger et décharger un module accessible via du code écrit en C.",
            module_typeinfo.MergeDatas(
            {
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        ModuleManager::GetModuleManagerClassName()
                    }
                }
            })
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ModuleC, Module)

    // ModuleCMaker // —————————————————————————————————————————————————————————————————————————————————————————————————
    ModuleCMaker::ModuleCMaker(const Item::ConstructorParameters &in_params):
    ModuleMaker(in_params, GetDataClassName(), GetClientClassName())
    {}

    ModuleCMaker::~ModuleCMaker() = default;

    DataMakerPtr ModuleCMaker::Clone(const Item::key_type &key) const
    {
        return Item::CreateItem<ModuleCMaker>({GetParentKey(), true, m_CanBeDeleteOnLoad, key});
    }

    const std::initializer_list<std::wstring> &ModuleCMaker::GetExtensions()
    {
#ifdef MA_PLATFORM_LINUX
        static const auto result = {L".so"s};
#elif MA_PLATFORM_APPLE
        static const auto result = {L".dylib"s, L".framework"s};
#elif MAMA_PLATFORM_WIN32
        static const auto result = {L".dll"s};
#else
        // je mets une erreur de domaine sans trop de conviction :p
        MA_ASSERT(false, L"Aucune extensions n'est disponible pour cette plateforme.", std::domain_error);
        static const auto result = {};
#endif
        return result;
    }

    // Le plus probable est en début de liste.
    const std::initializer_list<std::wstring> &ModuleCMaker::GetLibPrefixes()
    {
        static const auto result = {L"lib"s, L""s}; // L"lib64"s ?
        return result;
    }

    std::optional<std::filesystem::path> ModuleCMaker::Match(const std::filesystem::directory_entry &dir_entry) const
    {
        if(auto opt = m_ModuleManager->IsValidModuleFolderName(dir_entry))
        {
            // const auto &module_name = opt.value();
            const auto &folder_path = dir_entry.path();
            const auto folder_name = folder_path.stem().wstring();

            const auto plateforme_extensions = GetExtensions();
            const auto &prefixes = GetLibPrefixes();

            // Chemin le plus probable
            auto plugin_path = folder_path / (*prefixes.begin() + folder_name + *plateforme_extensions.begin());

            if(!std::filesystem::exists(plugin_path))
            {
                for(auto &&prefix : prefixes)
                    for(auto it = plateforme_extensions.begin();
                        it != plateforme_extensions.end() && !std::filesystem::exists(plugin_path);
                        ++it)
                    {
                        plugin_path = folder_path / (prefix + folder_name + *it);
                    }
            }

            if(std::filesystem::exists(plugin_path))
                return plugin_path;
        }

        return {};
    }

    ma::DataPtr
    ModuleCMaker::CreateData(ContextLinkPtr &link, ma::ItemPtr client, const ma::Item::CreateParameters &params) const
    {
        auto folder = std::dynamic_pointer_cast<ma::Folder>(client);
        MA_ASSERT(folder, L"folder est nul.", std::invalid_argument);

        const std::filesystem::directory_entry dir_entry{folder->m_Path->Get()};

        MA_ASSERT(m_ModuleManager->IsValidModuleFolderName(dir_entry).has_value(),
                  L"Ce dossier ne possède pas un nom valide. Normalement cela à lieu en amont via la fonction Match.",
                  std::logic_error);

        auto module_c_maker = GetPtr<ModuleCMaker>();

        MA_ASSERT(module_c_maker, L"« GetPtr<ModuleCMaker>() » renvoie un pointeur nul.", std::logic_error);

        return ma::Item::CreateItem<ma::ModuleC>(params, link, folder, module_c_maker);
    }

    const ma::TypeInfo &ModuleCMaker::GetFactoryTypeInfo() const
    {
        return ma::ModuleC::GetModuleCTypeInfo();
    }

    std::wstring ModuleCMaker::GetDataClassName()
    {
        return ma::ModuleC::GetModuleCClassName();
    }

    std::wstring ModuleCMaker::GetClientClassName()
    {
        return ma::Folder::GetFolderClassName();
    }

    const TypeInfo &ModuleCMaker::GetModuleCMakerTypeInfo()
    {
        const auto maker_typeinfo = ModuleMaker::GetModuleMakerTypeInfo();
        // clang-format off
        static const TypeInfo info =
        {
            GetModuleCMakerClassName(),
            L"La classe « ModuleCMaker » permet de créer un élément « ModuleC », se le dossier est um module avec une "
            L".dll, un .so ou une .dynlib",
            maker_typeinfo.MergeDatas({})
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ModuleCMaker, ModuleMaker)

} // namespace ma
