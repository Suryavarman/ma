/// \file Resource.ut.cpp
/// \author Pontier Pierre
/// \date 2020-05-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires des classes de gestion des ressources.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

void resource_setup()
{
    ma::g_ConsoleVerbose = 1u;
    ma::Item::CreateRoot<ma::Item>();
}

void resource_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(Resource, manager_creation, .init = resource_setup, .fini = resource_teardown)
{
    ma::ResourceManagerPtr resource_manager = ma::Item::CreateItem<ma::ResourceManager>();

    cr_assert_eq(resource_manager->GetKey(), ma::Item::Key::GetResourceManager());

    // cr_assert_null(resource_manager->m_FileSystemWatcher);
    cr_assert_eq(resource_manager->m_Resources->Get().size(), 0);

    auto white_list = resource_manager->GetWhiteListChildren();

    cr_assert_eq(white_list.size(), 2u);
    cr_assert_neq(white_list.find(ma::Resource::GetResourceClassName()), white_list.end());
    cr_assert_neq(white_list.find(ma::Folder::GetFolderClassName()), white_list.end());
}

Test(Resource, folder_creation, .init = resource_setup, .fini = resource_teardown)
{
    ma::ResourceManagerPtr resource_manager = ma::Item::CreateItem<ma::ResourceManager>();

    auto folder = ma::Item::CreateItem<ma::Folder>({ma::Item::Key::GetResourceManager()});
    bool should_exist = folder->m_HasToExist->Get();
    auto path = folder->m_Path->Get();
    bool recursive = folder->m_Recursive->Get();
    const std::wstring readable_size = folder->m_Size->Get();

    cr_assert_eq(should_exist, false);
    cr_assert_eq(path.wstring(), L"");
    cr_assert_eq(std::filesystem::exists(path), false);
    cr_assert_eq(recursive, true);
    cr_assert_eq(readable_size, L"");

    cr_assert_none_throw(ma::Item::CreateItem<ma::Folder>({ma::Item::Key::GetResourceManager()}, L""s, true, false));

    // Fait planter le test.
    //cr_assert_throw(ma::Item::CreateItem<ma::Folder>({ma::Item::ms_KeyResourceManager}, std::wstring(""), true, true), std::logic_error);
}

Test(Resource, real_folder_creation, .init = resource_setup, .fini = resource_teardown)
{
    ma::ResourceManagerPtr resource_manager = ma::Item::CreateItem<ma::ResourceManager>();

    #ifdef API_PATH
        std::filesystem::path current_dir{API_PATH};
    #else
        // On est dans le cas du projet CodeBlocks originel fait à la main.
        // wxGetCwd() renvoie donc API_PATH/bin/UnitTests_Release (ou Debuq)
        std::filesystem::path current_dir{wxGetCwd()};
    #endif // API_PATH

    cr_assert(std::filesystem::exists(current_dir));

    #ifndef API_PATH
    current_dir.RemoveLastDir();
    current_dir.RemoveLastDir();
    #endif
    current_dir = current_dir / L"tests" / L"resources_folder";

    cr_assert(std::filesystem::exists(current_dir));

    const std::wstring value = current_dir.wstring();

    auto folder = ma::Item::CreateItem<ma::Folder>({ma::Item::Key::GetResourceManager()}, value, true, true);

    bool should_exist = folder->m_HasToExist->Get();
    auto path = folder->m_Path->Get();
    bool recursive = folder->m_Recursive->Get();
    std::wstring readable_size = folder->m_Size->Get();

    cr_assert_eq(should_exist, true);
    cr_assert_eq(std::filesystem::exists(path), true);
    cr_assert_eq(recursive, true);
    cr_assert_neq(readable_size, L"");
}

