/// \file Colour.cpp
/// \author Pontier Pierre
/// \date 2020-10-25
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Colour.h"
#include "ma/Algorithm.h"
#include <regex>
#include <algorithm>
#include <string_view>

typedef std::pair<std::wstring, std::tuple<int, int, int, int>> ColorRGBAStruct;
typedef std::pair<std::wstring, std::tuple<int, int, int>> ColorRGBStruct;

namespace ma
{
    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params): wxColourVar::VarColourBase(params, *wxWHITE)
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params, const wxColour &color):
    wxColourVar::VarColourBase(params, color)
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params,
                             unsigned char red,
                             unsigned char green,
                             unsigned char blue,
                             unsigned char alpha):
    wxColourVar::VarColourBase(params, wxColour(red, green, blue, alpha))
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params,
                             unsigned int red,
                             unsigned int green,
                             unsigned int blue,
                             unsigned int alpha):
    wxColourVar::VarColourBase(params, wxColour(red, green, blue, alpha))
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params, int red, int green, int blue, int alpha):
    wxColourVar::VarColourBase(params, wxColour(red, green, blue, alpha))
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params,
                             unsigned int red,
                             unsigned int green,
                             unsigned int blue,
                             float alpha):
    wxColourVar::VarColourBase(params, wxColour(red, green, blue, alpha * 255))
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params, int red, int green, int blue, float alpha):
    wxColourVar::VarColourBase(params, wxColour(red, green, blue, alpha * 255))
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params, unsigned long colRGB):
    wxColourVar::VarColourBase(params, wxColour(colRGB))
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params, const wxString &color_name):
    wxColourVar::VarColourBase(params, wxColour(color_name))
    {}

    wxColourVar::wxColourVar(const ma::Var::ConstructorParameters &params, const std::wstring &color_name):
    wxColourVar::VarColourBase(params, wxColour(color_name))
    {}

    wxColourVar::~wxColourVar()
    {}

    void wxColourVar::SetFromItem(const std::wstring &str)
    {
        const auto value = ma::FromString<std::pair<std::wstring, std::vector<int>>>(str);
        wxColour colour(value.second[0],
                        value.second[1u],
                        value.second[2u],
                        value.second.size() == 4u ? value.second[3u] : wxALPHA_OPAQUE);

        if(colour != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = colour;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr wxColourVar::Copy() const
    {
        auto ptr = std::shared_ptr<wxColourVar>(new wxColourVar(GetConstructorParameters(), m_Value));

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    std::wstring wxColourVar::toString() const
    {
        const auto value_str = m_Value.GetAsString(wxC2S_CSS_SYNTAX).ToStdWstring();

        const bool is_rgba = value_str.find(L"rgba") != std::string::npos;

        if(is_rgba)
            return ma::toString(
                ColorRGBAStruct{L"rgba", {m_Value.Red(), m_Value.Green(), m_Value.Blue(), m_Value.Alpha()}});
        else
            return ma::toString(ColorRGBStruct{L"rgb", {m_Value.Red(), m_Value.Green(), m_Value.Blue()}});
    }

    std::wstring wxColourVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, this->toString()));
    }

    void wxColourVar::fromString(const std::wstring &str)
    {
        const bool is_rgba = str.find(L"rgba") != std::string::npos;

        if(is_rgba)
        {
            const auto value = ma::FromString<ColorRGBAStruct>(str);
            Set(wxColour(std::get<0>(value.second),
                         std::get<1>(value.second),
                         std::get<2>(value.second),
                         std::get<3>(value.second)));
        }
        else
        {
            const auto value = ma::FromString<ColorRGBStruct>(str);
            Set(wxColour(std::get<0>(value.second), std::get<1>(value.second), std::get<2>(value.second), 255));
        }
    }

    bool wxColourVar::IsValidString(const std::wstring &str) const
    {
        const bool is_rgba = str.find(L"rgba") != std::string::npos;
        const bool is_rgb = str.find(L"rgb") != std::string::npos;
        bool is_valid = is_rgba || is_rgb;

        if(is_valid)
        {
            if(is_rgba)
            {
                is_valid = ma::IsValidString<ColorRGBAStruct>(str);
                if(is_valid)
                {
                    const auto value = ma::FromString<ColorRGBAStruct>(str);
                    is_valid = 0 <= std::get<0>(value.second) && 256 > std::get<0>(value.second) &&
                               0 <= std::get<1>(value.second) && 256 > std::get<1>(value.second) &&
                               0 <= std::get<2>(value.second) && 256 > std::get<2>(value.second) &&
                               0 <= std::get<3>(value.second) && 256 > std::get<3>(value.second);
                }
            }
            else if(is_rgb)
            {
                is_valid = ma::IsValidString<ColorRGBStruct>(str);
                if(is_valid)
                {
                    const auto value = ma::FromString<ColorRGBStruct>(str);
                    is_valid = 0 <= std::get<0>(value.second) && 256 > std::get<0>(value.second) &&
                               0 <= std::get<1>(value.second) && 256 > std::get<1>(value.second) &&
                               0 <= std::get<2>(value.second) && 256 > std::get<2>(value.second);
                }
            }
        }

        return is_valid;
    }

    void wxColourVar::Set(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
    {
        Set(wxColour(red, green, blue, alpha));
    }

    void wxColourVar::Set(unsigned int red, unsigned int green, unsigned int blue, unsigned int alpha)
    {
        Set(wxColour(red, green, blue, alpha));
    }

    void wxColourVar::Set(int red, int green, int blue, int alpha)
    {
        Set(wxColour(red, green, blue, alpha));
    }

    void wxColourVar::Set(unsigned int red, unsigned int green, unsigned int blue, float alpha)
    {
        Set(wxColour(red, green, blue, alpha * 255));
    }

    void wxColourVar::Set(int red, int green, int blue, float alpha)
    {
        Set(wxColour(red, green, blue, alpha * 255));
    }

    void wxColourVar::Set(unsigned long colRGB)
    {
        Set(wxColour(colRGB));
    }

    void wxColourVar::Set(const wxString &colourName)
    {
        Set(wxColour(colourName));
    }

    void wxColourVar::Set(const std::wstring &colourName)
    {
        Set(wxColour(colourName));
    }

    void wxColourVar::Set(const wxColour &color)
    {
        wxColourVar::VarColourBase::Set(color);
    }

    const TypeInfo &wxColourVar::GetwxColourVarTypeInfo()
    {
        static const TypeInfo info = {GetwxColourVarClassName(), L"Variable d'item de type «wxColour».", {}};

        return info;
    }
} // namespace ma

M_CPP_CLASSHIERARCHY(wxColourVar, Var)
M_CPP_MAKERVAR_TYPE(wxColourVar)
