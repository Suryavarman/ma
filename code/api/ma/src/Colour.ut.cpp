/// \file Colour.ut.cpp
/// \author Pontier Pierre
/// \date 2020-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires des classes de type « Colour ».
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

void colour_setup()
{
    ma::Item::CreateRoot<ma::Item>();
}

void colour_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(Colour, colour_constructor_no_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));
    cr_assert_eq(var_colour->Get(), *wxWHITE);
}

Test(Colour, colour_constructor_colour_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    wxColour colour(0, 255, 0, 128);
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", colour));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_constructor_RGBi_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    wxColour colour(0, 255, 0);
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", 0, 255, 0));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_constructor_RGBu_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    wxColour colour(0, 255, 0);
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", 0u, 255u, 0u));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_constructor_RGBAu_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    wxColour colour(0, 255, 0, 128);
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", 0u, 255u, 0u, 128u));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_constructor_RGBAf_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    wxColour colour(0, 255, 0, 128);
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", 0, 255, 0, 128.f/255.f));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_constructor_RGBAuf_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    wxColour colour(0u, 255u, 0u, 128u);
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", 0u, 255u, 0u, 128.f/255.f));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_constructor_int32_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    wxColour colour(0, 255, 0, 128);
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", colour.GetRGBA()));
    cr_assert_eq(var_colour->Get(), wxColour(colour.GetRGBA()));
}

Test(Colour, colour_constructor_wxString_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", wxString(wxT("RED"))));
    cr_assert_eq(var_colour->Get(), *wxRED);
}

Test(Colour, colour_constructor_std_string_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour", std::wstring(L"RED")));
    cr_assert_eq(var_colour->Get(), *wxRED);
}

Test(Colour, colour_set_colour_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxColour colour(0, 255, 0, 128);
    cr_assert_none_throw(var_colour->Set(colour))
    cr_assert_eq(var_colour->Get(), colour);
}


Test(Colour, colour_set_RGBi_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxColour colour(255, 0, 128);
    cr_assert_none_throw(var_colour->Set(255, 0, 128));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_set_RGBu_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxColour colour(0u, 255u, 0u);
    cr_assert_none_throw(var_colour->Set(0u, 255u, 0u));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_set_RGBAu_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxColour colour(0u, 255u, 0u, 128u);
    cr_assert_none_throw(var_colour->Set(0u, 255u, 0u, 128u));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_set_RGBAf_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxColour colour(0, 255, 0, 128);
    cr_assert_none_throw(var_colour->Set(0, 255, 0, 128.f/255.f));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_set_RGBAuf_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxColour colour(0u, 255u, 0u, 128);
    cr_assert_none_throw(var_colour->Set(0u, 255u, 0u, 128.f/255.f));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_set_int32_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxColour colour(0, 255, 0, 128);
    cr_assert_none_throw(var_colour->Set(colour.GetRGBA()));
    cr_assert_eq(var_colour->Get(), wxColour(colour.GetRGBA()));
}

Test(Colour, colour_set_wxString_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    wxString red_str = "RED";
    wxColour colour(red_str);
    cr_assert_none_throw(var_colour->Set(red_str));
    cr_assert_eq(var_colour->Get(), colour);
}

Test(Colour, colour_set_std_string_value, .init = colour_setup, .fini = colour_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::wxColourVarPtr var_colour;
    cr_assert_none_throw(var_colour = item->AddVar<ma::wxColourVar>(L"m_Colour"));

    std::wstring red_str = L"RED";
    wxColour colour(red_str);
    cr_assert_none_throw(var_colour->Set(red_str));
    cr_assert_eq(var_colour->Get(), colour);
}

