/// \file Root.cpp
/// \author Pontier Pierre
/// \date 2019-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Root.h"
#include "ma/wx/Context.h"

namespace ma
{
    const ma::Var::key_type &Root::GetKeyCurrentVarProperty()
    {
        static const ma::Var::key_type key = L"m_Current";
        return key;
    }

    Root::Root(const Item::ConstructorParameters &in_params, const std::filesystem::path &in_config_path):
    Item({in_params, Key::GetRoot()}),
    m_Garbage{},
    m_ClassInfoManager{},
    m_RttiManager{},
    m_ObservationManager{},
    m_ResourceManager{},
    m_ContextsManager{},
    m_Config{},
    m_Current{},
#ifndef M_NOGUI
    m_wxGuiAccess{},
    m_Views{},
#endif
    m_ModulesManager{}
    {
        const auto root_path = std::filesystem::current_path() / GetKey();
        m_SavePath->fromString(root_path.wstring());

        m_Garbage = CreateItem<Garbage>({GetKey(), false});

        m_ClassInfoManager = CreateItem<ClassInfoManager>();
        m_RttiManager = CreateItem<RTTI>();
        m_ObservationManager = CreateItem<ObservationManager>();
        m_ResourceManager = CreateItem<ResourceManager>();
        m_ContextsManager = CreateItem<ContextManager>();

#ifndef M_NOGUI
        m_wxGuiAccess = CreateItem<ma::wx::GuiAccess>();
#endif

        m_Current = AddMemberVar<ItemVar>(Root::GetKeyCurrentVarProperty());

        m_Config = CreateItem<ma::ConfigItem>({Item::GetKey(), true, false, Key::GetConfig()}, in_config_path);
        m_Config->AddReadOnlyVar<ma::StringVar>(Key::Var::GetName(), L"Configurations");
    }

    Root::~Root() = default;

    void Root::EndConstruction()
    {
        Item::EndConstruction();

        m_Garbage->EndConstruction();

        m_Config->LoadConfig();
        m_ModulesManager = ma::Item::CreateItem<ma::ModuleManager>({m_ContextsManager->GetKey()});

#ifndef M_NOGUI
        m_Views = CreateItem<ma::wx::Views>(m_wxGuiAccess->GetKey());
#endif
    }

    void Root::SetSavePath(const std::filesystem::path &project_path)
    {
        MA_ASSERT(std::filesystem::exists(project_path),
                  L"Le dossier « " + project_path.wstring() + L" » n'existe pas.",
                  std::runtime_error);

        const auto folder_save_name = this->ma::Item::GetFolderSaveName(*this);
        const auto root_folder = project_path / folder_save_name;
        const auto root_folder_str = root_folder.wstring();

        SetVarFromString(m_LastSavePath->GetKey(), root_folder_str);
        m_SavePath->fromString(root_folder_str);
        m_CustomSavePath->fromString(ma::toString(true));
    }

    const TypeInfo &Root::GetRootTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetRootClassName(),
            L"La classe Root est le point d'entré de la structure de donnée. "
            L" Initialiser le nœud racine de cette manière: "
            L"        m_Root = ma::Item::CreateRoot<ma::Root>();"
            L"Puis utiliseR le nœud racine pour accéder à la rtti, à l'interface "
            L"wxWidgets etc."
            L"Exemple: "
            L"   ma::root()->wx.SetToolBar(m_AuiToolBar);"
            L"Pour supprimer le nœud racine appeler: "
            L"   ma::Item::EraseRoot(); "
            L" Puis pour le supprimer définitivement:"
            L"   m_Root.reset(nullptr); "
            L" ou en remplacer le par un nouveau nœud principal."
            L"   m_Root = ma::Item::CreateRoot<ma::Root>();",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        ma::Item::GetItemClassName()
                    }
                },
                {
                    TypeInfo::Key::GetBlackListChildrenData(),
                    {
                        ClassInfo::GetClassInfoClassName(),
                        Folder::GetFolderClassName(),
                        Resource::GetResourceClassName(),
                        Observation::GetObservationClassName(),
                        MakerItem::GetMakerItemClassName(),
                        MakerVar::GetMakerVarClassName(),
                        MakerwxVar::GetMakerwxVarClassName(),
                        Context::GetContextClassName()
#ifndef M_NOGUI
                                             ,
                        wx::Parameter::GetwxParameterClassName()
#endif
                    }
                }
            }
        };

        return info;
        // clang-format on
    }
    M_CPP_CLASSHIERARCHY(Root, Item)
} // namespace ma
