/// \file Enable.cpp
/// \author Pontier Pierre
/// \date 2019-11-18
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

// https://docs.wxwidgets.org/trunk/overview_log.html#overview_log_customize

#include "ma/Error.h"

#if !defined(M_NOGUI)
    #include <wx/app.h>
    #include <wx/debug.h>
    #include <wx/msgdlg.h>
    #include <wx/richmsgdlg.h>
    #include <wx/log.h>
    #include <wx/notifmsg.h>
    #include "ma/wx/wx.h"
#endif

namespace ma
{
    unsigned int g_ConsoleVerbose{2u};

    namespace wx
    {
        void Assert(const std::wstring &msg,
                    const std::wstring &function_name,
                    const std::wstring &filename,
                    const unsigned int &line)
        {
#if !defined(M_NOGUI)

            const bool wx_widgets_on = wxApp::GetInstance() != nullptr;
            if(wx_widgets_on)
            {
                const wxString message{L"Message: "s + msg};
                const wxString func{L"Fonction: " + function_name};
                const wxString cond{L""s};
                const wxString file{L"Fichier: "s + filename};

                // Peux masquer les messages d'avertissements qui suits.
                wxOnAssert(file, static_cast<int>(line), func, cond, msg);

                const wxString end_line{L"\n"s};

                const auto dlg_message{message + end_line + L"Ligne: "s + std::to_string(line) + end_line + func +
                                       end_line + file};

                wxRichMessageDialog dlg(
                    nullptr, dlg_message, L"Erreur", wxOK | wxCENTRE | wxICON_ERROR | wxSTAY_ON_TOP);
                dlg.ShowModal();
                // wxLogFatalError(dlg_message); // Ne s'affiche pas.
            }

#endif
        }

        M_DLL_EXPORT void AssertTextBegin()
        {
#if !defined(M_NOGUI)

            if(g_TextCtrl)
            {
                g_TextCtrl->SetDefaultStyle(g_TextErrorAttr);
            }

#endif
        }

        M_DLL_EXPORT void AssertTextEnd()
        {
#if !defined(M_NOGUI)
            if(g_TextCtrl)
            {
                g_TextCtrl->SetDefaultStyle(g_TextDefaultAttr);
            }
#endif
        }

        void Warning(const std::wstring &msg,
                     const std::wstring &function_name,
                     const std::wstring &filename,
                     const unsigned int &line)
        {
#if !defined(M_NOGUI)

            const bool wx_widgets_on = wxApp::GetInstance() != nullptr;
            if(wx_widgets_on)
            {
                const wxString message{L"Message: "s + msg};
                const wxString func{L"Fonction: " + function_name};
                const wxString cond{L""s};
                const wxString file{L"Fichier: "s + filename};

                const wxString end_line{L"\n"s};

                const wxString dlg_message{message + end_line + L"Ligne: "s + std::to_string(line) + end_line + func +
                                           end_line + file};

                /*
                // Belle représentation, mais s'il y a plusieurs messages à la suite, il ne faudra pas arrêter de
                // cliquer.
                wxRichMessageDialog dlg(nullptr,
                                        dlg_message,
                                        L"Avertissement",
                                        wxOK|wxCENTRE|wxICON_WARNING|wxSTAY_ON_TOP);
                dlg.ShowDetailedText(msg);
                dlg.ShowModal();
                /*/
                // Accumule les messages. Cela évite d'avoir des centaines de messages les un après les autres.
                // wxLogWarning(dlg_message);
                //
                // Sans accumulation cela peut vite saturer, surtout à la fermeture de l'application.
                // wxNotificationMessage(L"Avertissement", dlg_message, nullptr, wxICON_WARNING).Show();
                //*/
            }
#endif
        }
    } // namespace wx

    void Warning(bool test,
                 const std::wstring &msg,
                 const std::wstring &function_name,
                 const std::wstring &filename,
                 const unsigned int &line)
    {
        if(!test)
        {
#if !defined(M_NOGUI)
            ma::wx::Warning(msg, function_name, filename, line);

            if(wx::g_TextCtrl)
            {
                wx::g_TextCtrl->SetDefaultStyle(ma::wx::g_TextWarningAttr);
            }
#endif
            std::wstringstream ss;
            ss << L"Avertissement: " << '\n';
            ss << L"Message: " << msg << '\n';
            ss << L"Fonction: " << function_name << '\n';
            ss << L"Fichier: " << filename << '\n';
            ss << L"Ligne: " << line << std::endl;

            // Pour être sur d'avoir un message affiché.
            // todo A voir si il y a mieux que cette possible redondance ou moyen de faire une asse
            if(ma::g_ConsoleVerbose > 1u)
            {
                // J'utilise cout et non wcout pour avoir les messages qui
                // s'affichent correctement dans la console, mais logiquement
                // cette solution est hasardeuse.
                std::cout << ma::to_string(ss.str()) << std::endl;
                // std::wcout << ss.str() << std::endl;
            }

#if !defined(M_NOGUI)
            if(wx::g_TextCtrl)
            {
                wx::g_TextCtrl->SetDefaultStyle(ma::wx::g_TextDefaultAttr);
            }
#endif
        }
    }
} // namespace ma
