/// \file Observer.cpp
/// \author Pontier Pierre
/// \date 2020-01-05
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Observer.h"
#include "ma/Item.h"
#include "ma/ObservationManager.h"

namespace ma
{
    // const ma::Observable::Data::key_type ma::Observable::ms_KeyObsConstructionEnd("obs_construction_end");

    // Observable::Data //——————————————————————————————————————————————————————————————————————————————————————————————
    Observable::Data::Data(std::initializer_list<Observable::DataOrdered::value_type> init):
    m_Ordered(init),
    m_Unordered()
    {
        for(const auto &it : m_Ordered)
            m_Unordered[it.first].push_back(it.second);
    }

    Observable::Data::Data(const DataOrdered &data_ordered): m_Ordered(data_ordered), m_Unordered()
    {
        for(const auto &it : m_Ordered)
            m_Unordered[it.first].push_back(it.second);
    }

    Observable::Data::Data(): m_Ordered(), m_Unordered()
    {}

    Observable::Data::~Data() = default;

    const Observable::DataOrdered &Observable::Data::GetOrdered() const
    {
        return m_Ordered;
    }

    const Observable::DataUnordered &Observable::Data::GetUnordered() const
    {
        return m_Unordered;
    }

    void Observable::Data::SetAction(const Observable::Action &action)
    {
        m_Ordered.push_back(action);
        m_Unordered[action.first].push_back(action.second);
    }

    void Observable::Data::Clear()
    {
        m_Ordered.clear();
        m_Unordered.clear();
    }

    Observable::DataUnordered::size_type Observable::Data::GetSize() const
    {
        return m_Unordered.size();
    }

    void Observable::Data::Merge(const Data &data)
    {
        for(const auto &it : data.m_Ordered)
            m_Unordered[it.first].push_back(it.second);

        m_Ordered.insert(m_Ordered.end(), data.m_Ordered.begin(), data.m_Ordered.end());
    }

    bool Observable::Data::HasActions(const Action::first_type &key) const
    {
        const auto it_find = m_Unordered.find(key);
        return it_find != m_Unordered.end() && !it_find->second.empty();
    }

    Observable::Action::second_type Observable::Data::GetLastAction(const Observable::Data::key_type &key) const
    {
        static const std::wstring function_name =
            L"ma::Observable::Action::second_type ma::Observable::Data::GetLastAction(const "
            L"ma::Observable::Data::key_type& key)";

        const auto it_find = m_Unordered.find(key);

        MA_ASSERT(it_find != m_Unordered.end(),
                  L"La clef «" + key +
                      L"» ne correspond à aucune action. Utilisez HasActions avant d'appeler GetLastAction.",
                  std::invalid_argument);

        MA_ASSERT(!it_find->second.empty(),
                  L"La liste des identifiants des actions est vide. Cela ne devrait pas être le cas.",
                  std::runtime_error);

        return *it_find->second.rbegin();
    }

    Observable::Action::second_type Observable::Data::GetFirstAction(const Observable::Data::key_type &key) const
    {
        const auto it_find = m_Unordered.find(key);

        MA_ASSERT(it_find != m_Unordered.end(),
                  L"La clef «" + key +
                      L"» ne correspond à aucune action. Utilisez HasActions avant d'appeler GetFirstAction.",
                  std::invalid_argument);

        MA_ASSERT(!it_find->second.empty(),
                  L"La liste des identifiants des actions est vide. Cela ne devrait pas être le cas.",
                  std::runtime_error);

        return *it_find->second.begin();
    }

    Observable::DataUnordered::mapped_type Observable::Data::GetActions(const Observable::Data::key_type &key) const
    {
        const auto it_find = m_Unordered.find(key);

        MA_ASSERT(it_find != m_Unordered.end(),
                  L"La clef «" + key +
                      L"»ne correspond à aucune action. Utilisez HasActions avant d'appeler GetActions.",
                  std::invalid_argument);

        return it_find->second;
    }

    Observable::DataUnordered::mapped_type::size_type ma::Observable::Data::GetCount(const key_type &key) const
    {
        const auto it_find = m_Unordered.find(key);
        return it_find != m_Unordered.end() ? it_find->second.size() : 0;
    }

    std::wstring Observable::Data::toString() const
    {
        std::wstring str = L"{";

        for(const auto &it : m_Ordered)
            str += L"{" + it.first + L", " + it.second + L"}";

        return str + L"}";
    }

    // Observable //————————————————————————————————————————————————————————————————————————————————————————————————————
    Observable::Observable():
    // Lorsqu'un observable se construit, il faut attendre la fin de sa construction.
    // Pour éviter les problèmes d'observation avant la fin de la construction, par défaut l'observable accumule les
    // mises à jour.
    m_EndConstruction(false),
    m_BeginBatch(false),
    m_NeedUpdate(false),
    m_IsObserve(false),
    m_CurrentData()
    {}

    Observable::~Observable() = default;

    void Observable::SetObserve(bool value)
    {
        m_IsObserve = true;
    }

    bool Observable::IsObserve() const
    {
        return m_IsObserve;
    }

    void Observable::UpdateObservations(const Observable::Data &data)
    {
        if(IsObservable() && Item::HasItem(Item::Key::GetObservationManager()))
        {
            m_CurrentData.Merge(data);

            if(m_IsObserve && !GetBatchState() && m_EndConstruction)
            {
                auto obs_manager = Item::GetItem<ObservationManager>(Item::Key::GetObservationManager());

                // J'effectue une copie des données pour les vider de m_CurrenData.
                // De cette manière si UpdateObservations est rappelé durant cette
                // passe les données seront vides et cela évitera certains appels en
                // boucle.
                auto copy_data = m_CurrentData;
                m_CurrentData.Clear();
                obs_manager->UpdateObservations(GetPtr(), copy_data);

                SetNeedUpdate(false);
            }
            else
            {
                SetNeedUpdate(true);
            }

            // BeginObservation sera appelé. L'observateur analysera l'observable
            // dans son ensemble. Puis au  prochain UpdateObservations l'observateur
            // récupéra le m_CurrentData qui aura été initialiser après l'appel
            // à BeginObservation.
            // En résumé m_CurrentData accumule les données si il est observé et en
            // mode regroupement des observations.
            if(!m_IsObserve)
                m_CurrentData.Clear();
        }
    }

    void Observable::EndConstruction()
    {
        MA_ASSERT(
            !m_EndConstruction, L"La fonction «EndConstruction» ne doit être appelée qu'une fois", std::logic_error);

        m_EndConstruction = true;
        if(m_NeedUpdate)
            UpdateObservations({{}});
    }

    bool Observable::GetEndConstruction() const
    {
        return m_EndConstruction;
    }

    bool Observable::IsObservable() const
    {
        return m_EndConstruction;
    }

    void Observable::BeginBatch()
    {
        m_BeginBatch = true;
    }

    void Observable::EndBatch()
    {
        m_BeginBatch = false;
        if(m_NeedUpdate)
            UpdateObservations({{}});
    }

    bool Observable::GetBatchState()
    {
        return m_BeginBatch;
    }

    void Observable::SetNeedUpdate(bool value)
    {
        m_NeedUpdate = value;
    }

    const TypeInfo &Observable::GetObservableTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetObservableClassName(),
            L"Classe de base qui définit un objet observable.",
            {}
        };
        // clang-format on
        return info;
    }
    M_CPP_CLASSHIERARCHY(Observable)

    // Observer //——————————————————————————————————————————————————————————————————————————————————————————————————————
    Observer::Observer(): m_ObserverKey(), m_DestroyCall(false)
    {}

    Observer::~Observer()
    {
        m_DestroyCall = true;

        if(Item::HasItem(Item::Key::GetObservationManager()))
        {
            auto obs_manager = Item::GetItem<ObservationManager>(Item::Key::GetObservationManager());
            obs_manager->RemoveObserverFromObserverDestroy(this);
        }
    }

    InstanceInfo Observer::GetInstanceInfo() const
    {
        return {};
    }

    bool Observer::GetDestroyCall() const
    {
        return m_DestroyCall;
    }

    Observer::key_type Observer::GetObserverKey() const
    {
        return m_ObserverKey.GetKey();
    }

    void Observer::OnEnable()
    {
        Enable::OnEnable();
    }

    void Observer::OnDisable()
    {
        Enable::OnDisable();

        if(Item::HasItem(Item::Key::GetObservationManager()))
        {
            auto obs_manager = Item::GetItem<ObservationManager>(Item::Key::GetObservationManager());
            if(obs_manager->HasObserver(this))
                obs_manager->RemoveObserver(this);
        }
    }

    const TypeInfo &Observer::GetObserverTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetObserverClassName(),
            L"Classe abstraite qui définit un observateur.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(Observer)

} // namespace ma