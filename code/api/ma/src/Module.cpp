/// \file Module.cpp
/// \author Pontier Pierre
/// \date 2023-07-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <numeric>
#include "ma/Module.h"
#include "ma/ModuleC.h"
#include "ma/ClassInfo.h"
#include "ma/Config.h"

namespace ma
{
    // const Var::key_type var_key_folder = L"m_Folder"s;
    constexpr auto var_key_folder = L"m_Folder";
    // constexpr auto key_modules = L"3ecd94ee-cdb7-45f6-af52-bf1783a834d8";
    // constexpr auto key_makers = L"44466c10-1fab-4710-bc80-327edcad9e82";

    // module : français, anglais, néerlandais
    // modul : allemand, danois, hongrois, indonésien, norvégien, polonais, roumain, slovaque, slovène, suédois,
    //         tchèque, occitan
    // модул : bulgare
    // 模块 : chinois
    // 模組 : mou zou : cantonais
    // 모듈 : coréen
    // módulo : espagnole, portugais
    // moodul : estonien
    // moduuli : finnois
    // ενότητα : grec
    // modulo : italien
    // モジュール : japonais
    // modulis : leton, lituarien
    // модуль : russe, ukrainien
    // modül : turc
    // wahdat : وحدة : arabe
    // maapaank : मापांक : hindi
    // मॉड्यूल : sanscrit
    // moduli : swahili
    // mòdul : catalan
    // modulua : basque
    // modulu : corse
    // modil : haïtien
    // hébreu : מודול
    // eining : islandais
    // ម៉ូឌុល : khmer
    // โมดูล : thaï
    // mô-đun : vietnamien
    // modulenn : breton
    const auto prefixes_folder = StringSetVar::value_type{
        L"module", L"modul",   L"модул",    L"模块",   L"模組",       L"mouzou",  L"모듈",    L"módulo",
        L"moodul", L"moduuli", L"ενότητα",  L"modulo", L"モジュール", L"modulis", L"модуль",  L"modül",
        L"وحدة",   L"wahdat",  L"maapaank", L"मापांक",  L"moduli",     L"mòdul",   L"modulua", L"modulu",
        L"modil",  L"מודול",   L"eining",   L"ម៉ូឌុល",    L"โมดูล",       L"mô-đun",  L"modulenn"};

    // ModuleManager
    // —————————————————————————————————————————————————————————————————————————————————————————————————
    ModuleManager::ModuleManager(const Item::ConstructorParameters &in_params):
    Context({in_params.m_ParentKey, Key::GetModuleManager(), true, false},
            L"ModuleManager",
            L"5e6a2b36-632f-424c-8d7b-ce64348300a3"),
    m_ResourceManager(),
    m_PrefixesFolder(AddMemberVar<ma::StringSetVar>(L"m_PrefixesFolder", prefixes_folder))
    {}

    ModuleManager::~ModuleManager() = default;

    void ModuleManager::EndConstruction()
    {
        Context::EndConstruction();

        AddDataMaker<ma::ModuleCMaker>();
        // Ajouter le dossier au gestionnaire de ressources si celui n'est pas déjà présent.
        //        auto resource_manager = GetItem<ResourceManager>(Item::Key::ms_ResourceManager);
        //        const bool recursive = true;
        //        auto folder = resource_manager->AddFolder(dir_entry, recursive);
        //        SetVarFromValue<FolderVar>(m_Folder->GetKey(), folder);

        m_ResourceManager = GetItem<ResourceManager>(Key::GetResourceManager());
        m_ClassInfoManager = GetItem<ClassInfoManager>(Key::GetClassInfoManager());
        m_ObservationManager = GetItem<ObservationManager>(Key::GetObservationManager());
        m_ObservationManager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, m_ResourceManager);
    }

    std::optional<std::wstring> ModuleManager::IsValidModuleFolderName(const std::filesystem::directory_entry &entry)
    {
        bool result = entry.is_directory();

        if(result)
        {
            const auto name = entry.path().stem().wstring();

            const auto pos_delimiter = name.find(L'_');
            result = pos_delimiter != std::wstring::npos;
            if(result)
            {
                // https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
                const auto module_prefix = name.substr(0, pos_delimiter);
                auto module_name = name.substr(pos_delimiter + 1u);

                result = !module_prefix.empty() && !module_name.empty();
                if(result)
                {
                    const auto &prefixes = m_PrefixesFolder->Get();
                    bool find_prefix = false;
                    for(auto it = prefixes.begin(); it != prefixes.end() && !find_prefix; ++it)
                    {
                        find_prefix = *it == module_prefix;
                    }

                    result = find_prefix;

                    if(result)
                        return module_name;
                }
            }
        }

        return {};
    }

    std::optional<ma::ContextLinkPtr> ModuleManager::MakersCheckFolder(const ma::FolderPtr &folder)
    {
        // En commentaire car l'ajout des enfants du dossier n'est pas encore effectif à ce moment de la création de ce
        // dossier.
        //        static const ma::Folder::MatchFolder matcher({ma::Folder::ms_KeyPath});
        //
        //        auto folders = ma::Find<ma::Folder::MatchFolder>::GetDeque(input_older->GetKey(), matcher,
        //        ma::Item::SearchMod::RECURSIVE); folders.push_front(input_folder);
        //
        //        for(const auto& folder: folders)

        // Attention : Si le comportement de cette fonction est modifiée, il faut aussi modifier le CreateChildFromSave
        std::filesystem::directory_entry entry{folder->m_Path->Get()};

        if(std::filesystem::is_directory(entry))
        {
            // Avant de tester un dossier, on regarde si celui-ci possède des dépendances.
            // Si oui nous, les chargeons.
            auto opt = ModuleMaker::HasConfig(entry);
            auto dependencies =
                opt ? ModuleMaker::GetModuleDependencies(opt.value()) : ModuleMaker::ModuleDependencies{};
            std::unordered_set<Item::key_type> folders_added;

            auto resource_manager = Item::GetItem<ResourceManager>(Item::Key::GetResourceManager());

            for(auto &&[dep_key, dep_path] : dependencies)
            {
                if(!HasItem(dep_key))
                {
                    folders_added.insert(resource_manager->AddFolder(dep_path, true)->GetKey());
                }
            }

            auto makers = GetDataMakers<ModuleMaker>();

            for(const auto &maker : makers)
            {
                if(maker->Match(entry))
                {
                    // Le chargement d'un projet avec le système de dépendances peut provoquer des chargements en double
                    // à la fin du chargement lorsque les EndBatch sont appelées et que le système d'observation est
                    // exécuté.
                    if(!HasLink(folder))
                        return {ma::Item::CreateItem<ma::ContextLink>(GetKey(), folder)};
                }
            }

            for(auto &key : folders_added)
                Item::EraseItem(key);
        }

        return {};
    }

    ma::ItemPtr ModuleManager::CreateChildFromSave(const std::filesystem::directory_entry &dir_entry)
    {
        return Context::CreateChildFromSave(dir_entry);
    }

    void ModuleManager::BeginObservation(const ObservablePtr &observable)
    {
        if(m_ResourceManager == observable)
        {
            // On parcourt tous les dossiers du gestionnaire de ressources.
            const auto folders = ma::Find<ma::Folder::MatchFolder>::GetDeque(
                m_ResourceManager->GetKey(), ma::Folder::GetMatchFolder(), ma::Item::SearchMod::RECURSIVE);

            for(const auto &folder : folders)
            {
                MakersCheckFolder(folder);
                m_ObservationManager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, folder);
            }
        }
    }

    void ModuleManager::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(m_ResourceManager == observable ||
           m_ClassInfoManager->IsInherit(observable, ma::Folder::GetFolderClassName()))
        {
            auto item_obs = std::dynamic_pointer_cast<Item>(observable);
            MA_ASSERT(item_obs, L"item_obs est nul", std::runtime_error);

            const auto &actions = data.GetOrdered();
            for(const auto &action : actions)
            {
                if(action.first != ma::Item::Key::Obs::ms_AddItemBegin &&
                   // la construction n'est pas fini, nous attendrons l'appel à ms_ConstructionEnd
                   action.first != ma::Item::Key::Obs::ms_AddItemEnd &&
                   item_obs->HasItem(action.second, SearchMod::RECURSIVE)) // ← utile ?
                {
                    auto item = ma::Item::GetItem(action.second);

                    // Gestion de l'ajout ou de la suppression d'un dossier.
                    if(m_ClassInfoManager->IsInherit(item, ma::Folder::GetFolderClassName()))
                    {
                        auto folder = std::dynamic_pointer_cast<Folder>(item);
                        MA_ASSERT(folder, L"Impossible de convertir cet élément en dossier.", std::runtime_error);

                        if(action.first == ma::Item::Key::Obs::ms_ConstructionEnd)
                        {
                            m_ObservationManager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, folder);
                        }

                        if(action.first == ma::Folder::Key::Obs::ms_ConstructionEnd)
                        {
                            MakersCheckFolder(folder);
                        }
                    }
                }
            }
        }
    }

    void ModuleManager::EndObservation(const ObservablePtr &observable)
    {
        // Ne sert à rien puisqu'en théorie, les dossiers auront été traité avant que le gestionnaire de ressources
        // disparaisse.
        //        if(m_ResourceManager == observable)
        //        {
        //            auto modules = GetModules();
        //            for(auto &&[key, module] : modules)
        //                module->UnLoad();
        //        }
    }

    void ModuleManager::OnEnable()
    {
        ma::Context::OnEnable();
    }

    void ModuleManager::OnDisable()
    {
        ma::Context::OnDisable();

        RemoveChildren();
    }

    ma::DataPtr ModuleManager::BuildData(ma::ContextLinkPtr link, ma::DataPtr parent, ma::ItemPtr client)
    {
        ma::DataPtr result;

        if(link)
        {
            if(client)
            {
                auto folder = std::dynamic_pointer_cast<Folder>(client);

                if(folder)
                {
                    std::filesystem::directory_entry entry{folder->m_Path->Get()};

                    try
                    {
                        const auto key_parent = parent ? parent->GetKey() : link->GetKey();

                        auto data_makers = GetDataMakers<ModuleMaker>();

                        // for(auto &&[client_class_name, data_maker] : data_makers)
                        for(auto &data_maker : data_makers)
                        {
                            if(ma::ClassInfoManager::IsInherit(client, data_maker->m_ClientClassName->Get()))
                            {
                                if(data_maker->Match(entry))
                                    result = data_maker->CreateData(link, client, {key_parent});
                            }
                        }
                    }
                    catch(const std::exception &e)
                    {
                        MA_ASSERT(false,
                                  L"La création de l'item correspondant au client : \n"s + L"Clef: " +
                                      client->GetKey() + L" classe: " + client->GetItemTypeInfo().m_ClassName +
                                      L"\nn'a peu se faire à cause de l'erreur suivante:\n" + ma::to_wstring(e.what()),
                                  std::invalid_argument);
                    }
                }
            }
            else
                Context::BuildData(link, parent, client);
        }

        return result;
    }

    std::unordered_multimap<std::wstring, ma::ModulePtr> ModuleManager::GetModules()
    {
        typedef ma::MatchType<ma::ContextLink> MatchContextLink;
        auto links = ma::Find<MatchContextLink>::GetHashSet(this, {}, ma::Item::SearchMod::LOCAL);

        typedef ma::MatchVar<ma::StringVar, ma::Module> MatchModule;
        auto match = MatchModule(Key::Var::GetName());

        std::unordered_multimap<std::wstring, ma::ModulePtr> result;

        for(const auto &link : links)
        {
            auto modules = ma::Find<MatchModule>::GetHashMultiMap(link.get(), match, ma::Item::SearchMod::LOCAL);

#ifdef __cpp_lib_containers_ranges
            result.insert_range(modules);
#else
            result.insert(modules.begin(), modules.end());
#endif
        }

        return result;
    }

    const TypeInfo &ModuleManager::GetModuleManagerTypeInfo()
    {
        const auto context_typeinfo = Context::GetContextTypeInfo();
        // clang-format off
        static const TypeInfo info =
        {
            GetModuleManagerClassName(),
            L"La classe « ModuleManager » permet de définir une base commune pour charger et décharger un module.",
            context_typeinfo.MergeDatas(
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        Module::GetModuleClassName()
                    }
                }
            })
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ModuleManager, Context)

    // ModuleData // ———————————————————————————————————————————————————————————————————————————————————————————————————
    ModuleMaker::ModuleMaker(const Item::ConstructorParameters &in_params,
                             const std::wstring &data_class_name,
                             const std::wstring &client_class_name):
    DataMaker(in_params, data_class_name, client_class_name)
    {
        m_ModuleManager = ma::Item::GetItem<ma::ModuleManager>(Key::GetModuleManager());
    }

    ModuleMaker::~ModuleMaker() = default;

    std::optional<std::filesystem::path> ModuleMaker::HasConfig(const std::filesystem::directory_entry &dir_entry)
    {
        const auto &folder_path = dir_entry.path();
        const auto folder_name = folder_path.stem().wstring();

        auto config_path = folder_path / (folder_name + L".cfg");

        if(std::filesystem::exists(config_path))
            return config_path;
        else
            return {};
    }

    ModuleMaker::ModuleDependencies ModuleMaker::GetModuleDependencies(const std::filesystem::path &config_path)
    {
        MA_ASSERT(!std::filesystem::is_directory(config_path),
                  L"Le chemin du fichier de configuration est un répertoire : « " + config_path.wstring() + L" ».",
                  std::invalid_argument);
        MA_ASSERT(std::filesystem::exists(config_path),
                  L"Le chemin du fichier de configuration n'existe pas : « " + config_path.wstring() + L" ».",
                  std::invalid_argument);

        ModuleMaker::ModuleDependencies result;

        auto config = ma::Config(config_path);
        config.Load();

        const auto &data = config.m_Datas;
        if(!data.empty())
        {
            const auto it_find_deps = data.find(L"dependencies");
            if(it_find_deps != data.end())
            {
                const auto &data_deps = it_find_deps->second;
                const auto &it_find_tag = data_deps.find(L"modules");
                if(it_find_tag != data_deps.end())
                {
                    const auto &modules_str = it_find_tag->second;
                    const ma::Serialization::Parser parser{modules_str};
                    using ModulesDeps = std::vector<std::wstring>;
                    if(parser.HasError() || !parser.IsValid<ModulesDeps>())
                    {
                        const auto errors = parser.GetErrors<ModulesDeps>();
                        std::wstring errors_msg;

                        if(!errors.empty())
                        {
                            auto merge_errors =
                                [](std::wstring message, const ma::Serialization::Errors::value_type &error)
                            {
                                return std::move(message) + L"\nLigne " + toString(error.first) + L" « " +
                                       error.second + L" ».";
                            };

                            auto error_0 = errors.begin();
                            errors_msg = L"Ligne: " + toString(error_0->first);
                            errors_msg += L" « " + error_0->second + L" ».";

                            errors_msg =
                                std::accumulate(std::next(errors.begin()), errors.end(), errors_msg, merge_errors);
                        }

                        MA_ASSERT(false,
                                  L"Impossible de lire les dépendances définies dans le fichier de configuration « " +
                                      config_path.wstring() + L" ». Voici les erreurs:\n" + errors_msg,
                                  std::invalid_argument);
                    }
                    else
                    {
                        const auto modules_key_names = parser.GetValue<ModulesDeps>();

                        const auto current = std::filesystem::current_path();
                        std::filesystem::current_path(config_path.parent_path());

                        for(const auto &dep_relative_path : modules_key_names)
                        {
                            const std::filesystem::directory_entry dep_absolute_path{
                                std::filesystem::absolute(dep_relative_path)};
                            auto dep_key = ma::Module::GetGuid(dep_absolute_path);

                            result.emplace_back(dep_key, dep_absolute_path);
                        }

                        std::filesystem::current_path(current);
                    }
                }
            }
        }

        return result;
    }

    const TypeInfo &ModuleMaker::GetModuleMakerTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetModuleMakerClassName(),
            L"La classe « ModuleMaker » est une classe abstraite. Il faut définir le teste sur un dossier "
            L"pour savoir si ce dossier est un module que cette fabrique peut générer.",
            {
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        ModuleManager::GetModuleManagerClassName()
                    }
                }
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ModuleMaker, DataMaker)

    // Module // ———————————————————————————————————————————————————————————————————————————————————————————————————————
    Module::Module(const Item::ConstructorParameters &params,
                   const ContextLinkPtr &link,
                   const ItemPtr &client,
                   ModuleMakerPtr maker):
    ma::Data::Data({params,
                    params.m_Key.empty() ? GetGuid(std::filesystem::directory_entry(
                                               std::dynamic_pointer_cast<Folder>(client)->m_Path->Get())) :
                                           params.m_Key},
                   link,
                   client),
    ma::Observer(),
    m_Context{std::dynamic_pointer_cast<ma::ModuleManager>(link->GetContext())},
    m_Maker{std::move(maker)},
    m_Folder{AddReadOnlyMemberVar<FolderVar>(var_key_folder, std::dynamic_pointer_cast<Folder>(m_Client->GetItem()))},
    m_Name{AddMemberVar<StringVar>(Item::Key::Var::GetName(),
                                   m_Client->GetItem()->GetVar(Item::Key::Var::GetName())->toString())},
    m_Config{AddReadOnlyMemberVar<ResourceVar>(L"m_Config")}
    {}

    Module::~Module() = default;

    void Module::EndConstruction()
    {
        auto folder = m_Folder->GetItem();
        MA_ASSERT(m_Context, L"m_Context est nul.", std::invalid_argument);
        MA_ASSERT(folder, L"m_Folder pointe sur rien.", std::invalid_argument);

        ma::Data::EndConstruction();
    }

    void Module::SetObservation(const ma::ObservablePtr &observable)
    {
        // Test
        // MA_ASSERT(IsEnable(), L"Ne devrait pas être appelé si le nœud est inactif.");

        // \todo Si le dossier ou l'un des sous-dossiers ou fichiers ont été modifiés, il faut recharger le module.
        // \todo Voir si les ressources et les dossiers notifie leur modifications ou renommage.
    }

    void Module::BeginObservation(const ObservablePtr &observable)
    {
        SetObservation(observable);
    }

    void Module::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        SetObservation(observable);
    }

    void Module::EndObservation(const ObservablePtr &observable)
    {
        auto folder = m_Folder->GetItem();
        if(folder == observable)
        {
            m_Context->RemoveChild(m_KeyLink);

            const auto key = GetKey();

            std::function<void(ma::ItemPtr it)> remove_func;
            remove_func = [&](ma::ItemPtr item)
            {
                for(auto &child : item->GetItemsPostOrder(LOCAL))
                {
                    const auto dependencies = child->GetDependencies();
                    if(auto it_find = std::find(dependencies.begin(), dependencies.end(), key);
                       it_find != dependencies.end())
                        Item::EraseItem(child);
                    else
                        remove_func(child);
                }
            };

            remove_func(ma::Item::GetRoot());
        }
    }

    void Module::OnEnable()
    {
        Observer::OnEnable();
        Data::OnEnable();
    }

    void Module::OnDisable()
    {
        Observer::OnDisable();
        Data::OnDisable();
    }

    bool Module::Load()
    {
        MA_ASSERT(m_Config, L"m_Config est nul.", std::logic_error);

        const auto config_resource = m_Config->GetItem();
        if(config_resource)
        {
            const std::filesystem::path config_path = config_resource->m_Path->toString();

            const auto modules_dependencies = std::filesystem::exists(config_path) ?
                                                  ModuleMaker::GetModuleDependencies(config_path) :
                                                  ModuleMaker::ModuleDependencies{};

            auto resource_manager = Item::GetItem<ma::ResourceManager>(Item::Key::GetResourceManager());
            auto module_manager = Item::GetItem<ma::ModuleManager>(Item::Key::GetModuleManager());

            auto test_module_or_key = [&](const ma::FolderPtr &folder)
            {
                if(auto data_opt = module_manager->HasData(folder))
                {
                    const auto data_key = data_opt.value()->GetKey();
                    MA_ASSERT(false,
                              L"Le dossier « " + folder->m_Path->toString() +
                                  L" » dont dépend ce module est bien chargé, le module l'ai "
                                  L"bien lui aussi, mais celui-ci possède une clef différente "
                                  L"« " +
                                  data_key + L" » que celle définie dans son fichier « .guid » « " + folder->GetKey() +
                                  L" ».",
                              std::logic_error);
                }
                else
                    MA_ASSERT(false,
                              L"Le dossier « " + folder->m_Path->toString() +
                                  L" » dont dépend ce module est bien chargé mais le "
                                  L"module ne l'ai pas.",
                              std::logic_error);
            };

            std::vector<Item::key_type> dependencies;

            for(auto &&[dep_key, dep_path] : modules_dependencies)
            {
                if(!HasItem(dep_key))
                {
                    if(auto folder_opt = resource_manager->HasFolderOpt(dep_path.path().wstring()))
                    {
                        test_module_or_key(folder_opt.value());
                    }
                    else
                    {
                        auto folder = resource_manager->AddFolder(dep_path.path().wstring(), true);
                        if(!HasItem(dep_key))
                            test_module_or_key(folder_opt.value());
                        else
                        {
                            if(auto it_find = std::find(dependencies.begin(), dependencies.end(), dep_key);
                               it_find != dependencies.end())
                            {
                                dependencies.push_back(dep_key);
                            }
                        }
                    }
                }
                else
                {
                    dependencies.push_back(dep_key);
                }
            }

            AddDependencies(dependencies);
        }

        bool result = Data::Load();

        if(result)
        {
            auto folder = m_Folder->GetItem();
            std::filesystem::directory_entry entry{folder->m_Path->Get()};

            MA_ASSERT(m_Maker, L"m_Maker est nul.", std::logic_error);

            MA_ASSERT(result = m_Maker->Match(entry).has_value(),
                      L"Le module « " + m_Name->toString() + L" » pointe sur un dossier qu'il ne peut gérer.",
                      std::logic_error);
        }

        return result;
    }

    bool Module::UnLoad()
    {
        return Data::UnLoad();
    }

    bool Module::Reload()
    {
        return Data::Reload();
    }

    Item::key_type Module::GetGuid(const std::filesystem::directory_entry &module_directory)
    {
        MA_ASSERT(module_directory.exists(),
                  L"Ce dossier n'existe pas " + module_directory.path().wstring(),
                  std::invalid_argument);

        MA_ASSERT(module_directory.is_directory(),
                  L"Ce n'est pas un dossier " + module_directory.path().wstring(),
                  std::invalid_argument);

        const auto name = module_directory.path().stem().wstring();
        std::wstring guid_file_name = name + L".guid";

        std::filesystem::path guid_file_path = module_directory.path() / guid_file_name;

        std::wstring guid;
        {
            std::wifstream guid_file(guid_file_path, std::ios::in);

            MA_ASSERT(
                guid_file.is_open(), L"Impossible d'ouvrir le fichier " + guid_file_path.wstring(), std::runtime_error);

            MA_ASSERT(guid_file >> guid,
                      L"Impossible de lire le guid dans le fichier « " + guid_file_path.wstring() + L" ».",
                      std::invalid_argument);
        }
        const auto result = ma::toString<std::wstring>(guid);

        return result;
    }

    Item::key_type Module::GetGuid(const std::filesystem::path &plugin_file_name)
    {
        const std::filesystem::directory_entry entry{plugin_file_name.parent_path()};
        return GetGuid(entry);
    }

    void Module::MakerReplacement(ma::DataMakerPtr old_maker, ma::DataMakerPtr new_maker)
    {
        Data::MakerReplacement(old_maker, new_maker);

        if(m_Maker == old_maker and ma::ClassInfoManager::IsInherit(new_maker, m_Maker->GetTypeInfo().m_ClassName))
        {
            auto maker = std::dynamic_pointer_cast<ma::ModuleMaker>(new_maker);
            MA_ASSERT(
                maker, L"new_maker n'a peut être converti en ModuleMaker alors qu'il en hérite.", std::runtime_error);

            m_Maker = std::move(maker);
        }
    }

    const TypeInfo &Module::GetModuleTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetModuleClassName(),
            L"Classe abstraite normalisant et factorisant le chargement et déchargement d'un module.",
            {
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        ModuleManager::GetModuleManagerClassName()
                    }
                }
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(Module, Data)

} // namespace ma
