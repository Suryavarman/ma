/// \file Serialization.cpp
/// \author Pontier Pierre
/// \date 2021-16-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Serialization.h"
#include "ma/Algorithm.h"
#include <cmath>

using namespace std::literals;

namespace ma
{
    // \todo utiliser std::quoted https://en.cppreference.com/w/cpp/io/manip/quoted ?
    namespace Serialization
    {
        /////////////////////////////////
        // Node
        Node::Node(size_t index): m_Type{ma::Serialization::Node::Type::Invalid}, m_Index{index}, m_String{}, m_Nodes{}
        {}

        Node::Node(ma::Serialization::Node::Type type, size_t index, const std::wstring_view &str):
        m_Type{type},
        m_Index{index},
        m_String{str},
        m_Nodes{}
        {}

        Node::Node(ma::Serialization::Node::Type type,
                   size_t index,
                   const std::wstring_view &str,
                   const std::vector<ma::Serialization::NodePtr> &children):
        m_Type{type},
        m_Index{index},
        m_String{str},
        m_Nodes{children}
        {}

        bool Node::IsValid() const
        {
            if(m_Type == Type::Invalid)
                return false;
            else
                for(const auto &it : m_Nodes)
                {
                    if(!it->IsValid())
                        return false;
                }

            return true;
        }

        void Node::SetEndIndex(size_t index)
        {
            MA_ASSERT(index >= m_Index, L"index <= m_Index", std::invalid_argument);

            if(index + 1u < m_String.size())
                m_String.remove_suffix(m_String.size() - index - 1u);
        }

        std::wstring GetTypeStr(ma::Serialization::Node::Type type)
        {
            switch(type)
            {
                case ma::Serialization::Node::Type::Struct:
                    return L"Structure";

                case ma::Serialization::Node::Type::Value:
                    return L"Value";

                default:
                    return L"Invalid";
            }
        }

        /////////////////////////////////
        // Parser
        Parser::Parser(const std::wstring &str):
        m_Root{new Node{Begin::GetIndex(str)}},
        m_String{str},
        m_StructStack{},
        m_StructIncrement{0},
        m_StructDecrement{0},
        m_Str{m_String}
        {
            m_Root->m_String = m_String;
            m_Root->m_String.remove_prefix(m_Root->m_Index);

            if(!m_Root->m_String.empty())
            {
                // Ce n'est pas à l'analyse syntaxique de modifier la chaîne de
                // caractère. Une variable de type string peut nécessiter d'avoir
                // espaces au début. À la fin c'est plus discutable.
                // m_Root->m_String = ma::TrimStartEnd(m_Root->m_String);
                Begin{this}.Parse(m_Root);
            }
            else
            {
                m_Root->m_Type = Node::Type::Value;
            }
        }

        Parser::~Parser() = default;

        bool Parser::HasError() const
        {
            return m_StructIncrement != m_StructDecrement || !m_Root->IsValid();
        }

        NodePtr Parser::GetRoot() const
        {
            return m_Root;
        }

        void Parser::BeginStruct(const NodePtr &node)
        {
            m_StructStack.push(node);
            m_StructIncrement++;
            node->m_Type = ma::Serialization::Node::Type::Invalid;
        }

        NodePtr Parser::EndStruct(size_t str_end_index)
        {
            MA_ASSERT(str_end_index < m_String.size(),
                      L"str_end_index >= " + ma::toString(m_String.size()),
                      std::invalid_argument);

            if(m_StructStack.empty())
                return m_Root;
            else
            {
                auto node = m_StructStack.top();

                node->m_String.remove_suffix(m_String.size() - str_end_index - 1u);
                node->m_Type = ma::Serialization::Node::Type::Struct;

                m_StructStack.pop();
                m_StructDecrement++;

                return node;
            }
        }

        NodePtr Parser::GetCurrentStruct()
        {
            if(!m_StructStack.empty())
                return m_StructStack.top();
            else
                return m_Root;
        }

        /////////////////////////////////
        // Rule
        Rule::Rule(Parser *parser): m_Parser{parser}
        {
            MA_ASSERT(m_Parser, L"Le parseur est nulle. L'instance doit être non nulle.", std::invalid_argument);
        }

        Rule::~Rule() = default;

        bool Rule::CheckNode(NodePtr &node)
        {
            return node->m_Index < m_Parser->m_Str.size();
        }

        /////////////////////////////////
        // Fin
        //     c
        //   ┌───┐
        //  ─┤FIN│
        //   └───┘
        //
        End::End(Parser *parser): Rule(parser)
        {}

        End::~End() = default;

        void End::Parse(NodePtr &root)
        {
            if(root != m_Parser->GetRoot())
                root->m_Type = Node::Type::Invalid;
            else
            {
                size_t end_index = root->m_Index + root->m_String.size();
                if(root->m_Index != m_Parser->m_Str.size())
                {
                    if(CheckNode(root))
                    {
                        if(end_index < m_Parser->m_Str.size() - 1u && root->m_Type != Node::Type::Invalid)
                        {
                            std::wstring_view str =
                                m_Parser->m_Str.substr(end_index, m_Parser->m_Str.size() - end_index);
                            for(auto it = str.begin(); root->m_Type != Node::Type::Invalid && it != str.end(); ++it)
                                if(*it != ' ')
                                    root->m_Type = Node::Type::Invalid;
                        }
                    }
                }
            }
        }

        /////////////////////////////////
        // Begin
        //     a
        //   ┌───┐S+1
        //   │ { ├──►b
        //  ─┤   │Valeur (nombre ou chaîne de caractères)
        //   │ V ├──►c
        //   └───┘
        //
        Begin::Begin(Parser *parser): Rule(parser)
        {}

        Begin::~Begin() = default;

        void Begin::Parse(NodePtr &root)
        {
            if(!m_Parser->m_Str.empty())
            {
                if(CheckNode(root))
                {
                    std::wstring_view str = m_Parser->m_Str.substr(root->m_Index);
                    std::wstring_view trim = ma::TrimStart(str);

                    if(trim.front() == '{')
                    {
                        root->m_Type = Node::Type::Struct;
                        m_Parser->BeginStruct(root);
                        StructBegin{m_Parser}.Parse(root);
                    }
                    else
                    {
                        root->m_Type = Node::Type::Value;
                        End{m_Parser}.Parse(root);
                    }
                }
            }
            else
            {
                root->m_Type = Node::Type::Value;
            }
        }

        size_t Begin::GetIndex(const std::wstring &str)
        {
            size_t index = 0;

            if(!str.empty())
            {
                std::wstring trim = ma::TrimStart(str);

                if(trim.front() == '{')
                {
                    index = str.size() - trim.size();
                }
            }

            return index;
        }

        /////////////////////////////////
        // StructBegin
        //    b
        //  ┌───┐S+1
        //  │ { ├──►b
        //  │   │chaîne de caractères
        //  │ " ├──►d
        // ─┤   │nombre
        //  │ N ├──►e
        //  │   │S-1
        //  │ } ├──►f
        //  └───┘
        //
        StructBegin::StructBegin(Parser *parser): Rule(parser)
        {}

        StructBegin::~StructBegin() = default;

        void StructBegin::Parse(NodePtr &struct_node)
        {
            if(CheckNode(struct_node))
            {
                size_t next_index = struct_node->m_Index + 1u;

                if(next_index < m_Parser->m_Str.size())
                {
                    std::wstring_view str = m_Parser->m_Str.substr(next_index);
                    std::wstring_view trim = ma::TrimStart(str);

                    // Index de la première lettre de trim dans m_Parser->m_Str
                    next_index = next_index + str.size() - trim.size();

                    if(trim.front() == '{')
                    {
                        NodePtr node{new Node{Node::Type::Struct, next_index, trim}};
                        struct_node->m_Nodes.push_back(node);
                        m_Parser->BeginStruct(node);
                        StructBegin{m_Parser}.Parse(node);
                    }
                    else if(trim.front() == '}')
                    {
                        m_Parser->EndStruct(next_index);
                        NodePtr parent_struct_node = m_Parser->GetCurrentStruct();
                        StructEnd{m_Parser}.Parse(parent_struct_node, next_index + 1u);
                    }
                    else if(trim.front() == '"')
                    {
                        // On met index + 1u car la valeur ne comprend pas le
                        // guillemet.
                        trim.remove_prefix(1u);
                        NodePtr node{new Node{Node::Type::Value, next_index + 1u, trim}};
                        struct_node->m_Nodes.push_back(node);
                        StringNext{m_Parser}.Parse(node);
                    }
                    else
                    {
                        NodePtr node{new Node{Node::Type::Value, next_index, trim}};
                        struct_node->m_Nodes.push_back(node);
                        NumberNext{m_Parser}.Parse(node);
                    }
                }
                else
                    struct_node->m_Type = Node::Type::Invalid;
            }
        }

        /////////////////////////////////
        // StructEnd
        //   f
        //  ┌───┐
        //  │ , ├──►k
        //  │   │
        // ─┤Fin│
        //  │   │s-1
        //  │ } ├──►f
        //  └───┘
        StructEnd::StructEnd(Parser *parser): Rule(parser)
        {}

        StructEnd::~StructEnd() = default;

        void StructEnd::Parse(NodePtr &struct_node)
        {
            MA_ASSERT(false,
                      L"Il faut utiliser «StructEnd::Parse(NodePtr& struct_node, size_t in_next_index)»",
                      std::logic_error);
        }

        void StructEnd::Parse(NodePtr &struct_node, size_t in_next_index)
        {
            if(CheckNode(struct_node))
            {
                MA_ASSERT(in_next_index > struct_node->m_Index + 1u,
                          L"«in_next_index» est inférieur ou égale à «struct_node->m_Index + 1u.»",
                          std::invalid_argument);

                std::wstring_view str = m_Parser->m_Str.substr(in_next_index);
                std::wstring_view trim = ma::TrimStart(str);
                // Index de la première lettre de trim dans m_Parser->m_Str
                size_t next_index = in_next_index + str.size() - trim.size();

                // struct_node->m_String = m_Parser->m_Str.substr(struct_node->m_Index, next_index -
                // struct_node->m_Index);
                //
                if(trim.empty())
                {
                    End{m_Parser}.Parse(struct_node);
                }
                else if(trim.front() == ',')
                {
                    NodePtr parent_struct_node = m_Parser->GetCurrentStruct();
                    StructNext{m_Parser}.Parse(parent_struct_node, next_index + 1u);
                }
                else if(trim.front() == '}')
                {
                    m_Parser->EndStruct(next_index);
                    // pour s'assurer d'avoir le parent.
                    NodePtr parent_struct_node = m_Parser->GetCurrentStruct();
                    StructEnd{m_Parser}.Parse(parent_struct_node, next_index + 1u);
                }
                else
                {
                    struct_node->m_Type = Node::Type::Invalid;
                }
            }
        }

        /////////////////////////////////
        // StructNext
        //    k
        //  ┌───┐S+1
        //  │ { ├──►b
        //  │   │
        // ─┤ " ├──►d
        //  │   │
        //  │ N ├──►e
        //  └───┘
        //
        StructNext::StructNext(Parser *parser): Rule(parser)
        {}

        StructNext::~StructNext() = default;

        void StructNext::Parse(NodePtr &struct_node)
        {
            MA_ASSERT(false,
                      L"Il faut utiliser «StructNext::Parse(NodePtr& struct_node, size_t in_next_index)»",
                      std::logic_error);
        }

        void StructNext::Parse(NodePtr &struct_node, size_t in_next_index)
        {
            if(CheckNode(struct_node))
            {
                MA_ASSERT(!struct_node->m_Nodes.empty(),
                          L"StructNext devrait être appelé, seulement si m_Nodes à au moins un enfant.",
                          std::invalid_argument);

                NodePtr last_child = *struct_node->m_Nodes.rbegin();
                size_t last_index = last_child->m_Index + last_child->m_String.size() - 1u;

                MA_ASSERT(in_next_index > last_index + 1u,
                          L"«in_next_index» est inférieur ou égale à «last_index + 1u.»",
                          std::invalid_argument);

                std::wstring_view str = m_Parser->m_Str.substr(in_next_index);
                std::wstring_view trim = ma::TrimStart(str);
                // Index de la première lettre de trim dans m_Parser->m_Str
                size_t index = in_next_index + str.size() - trim.size();

                if(trim.front() == '{')
                {
                    NodePtr node{new Node{Node::Type::Struct, index, trim}};
                    struct_node->m_Nodes.push_back(node);
                    m_Parser->BeginStruct(node);
                    StructBegin{m_Parser}.Parse(node);
                }
                else if(trim.front() == '"')
                {
                    // On met index + 1u car la valeur ne comprend pas le guillemet.
                    trim.remove_prefix(1u);
                    NodePtr node{new Node{Node::Type::Value, index + 1u, trim}};
                    struct_node->m_Nodes.push_back(node);
                    StringNext{m_Parser}.Parse(node);
                }
                else
                {
                    NodePtr node{new Node{Node::Type::Value, index, trim}};
                    struct_node->m_Nodes.push_back(node);
                    NumberNext{m_Parser}.Parse(node);
                }
            }
        }

        /////////////////////////////////
        // NumberNext
        //    e
        //  ┌───┐S-1
        //  │ } ├──►f
        // ─┤   │
        //  │ , ├──►k
        //  └───┘
        //
        NumberNext::NumberNext(Parser *parser): Rule(parser)
        {}

        NumberNext::~NumberNext() = default;

        void NumberNext::Parse(NodePtr &number_node)
        {
            if(CheckNode(number_node))
            {
                const auto index_comma = m_Parser->m_Str.find_first_of(',', number_node->m_Index);
                const auto index_brace = m_Parser->m_Str.find_first_of('}', number_node->m_Index);

                if(index_comma != std::wstring_view::npos || index_brace != std::wstring_view::npos)
                {
                    bool is_comma = std::wstring_view::npos == index_brace || index_comma < index_brace;

                    size_t number_index_end{(is_comma ? index_comma : index_brace) - 1u};
                    number_node->m_String.remove_suffix(m_Parser->m_Str.size() - number_index_end - 1u);

                    // On retire les espaces après le nombre.
                    number_node->m_String = ma::TrimEnd(number_node->m_String);

                    if(is_comma) // ','
                    {
                        NodePtr struct_node = m_Parser->GetCurrentStruct();
                        StructNext{m_Parser}.Parse(struct_node, index_comma + 1u);
                    }
                    else // '}'
                    {
                        m_Parser->EndStruct(index_brace);
                        // pour s'assurer d'avoir le parent.
                        NodePtr parent_struct_node = m_Parser->GetCurrentStruct();
                        StructEnd{m_Parser}.Parse(parent_struct_node, index_brace + 1u);
                    }
                }
                else
                {
                    number_node->m_Type = Node::Type::Invalid;
                }
            }
        }

        /////////////////////////////////
        // StringNext
        //     d
        //  ┌─────┐
        //  │ " , ├──►k
        // ─┤     │S-1
        //  │ " } ├──►b
        //  └─────┘
        //
        StringNext::StringNext(Parser *parser): Rule(parser)
        {}

        StringNext::~StringNext() = default;

        void StringNext::Parse(NodePtr &string_node)
        {
            if(CheckNode(string_node))
            {
                auto index_quote = m_Parser->m_Str.find_first_of(L'"', string_node->m_Index);
                // il serait bien de trouver le dernier guillemet avant « } » ou « , ».

                if(index_quote != std::wstring_view::npos)
                {
                    const auto index_comma = m_Parser->m_Str.find_first_of(L',', index_quote);
                    const auto index_brace = m_Parser->m_Str.find_first_of(L'}', index_quote);

                    if(index_comma != std::wstring_view::npos || index_brace != std::wstring_view::npos)
                    {
                        bool is_comma = std::wstring_view::npos == index_brace || index_comma < index_brace;
                        auto index_next = is_comma ? index_comma : index_brace;

                        // {""", ""}
                        // 012345678 => index_quote = 2 après ce traitement, il sera égale à trois.
                        index_quote = m_Parser->m_Str.substr(index_quote, index_next - index_quote).find_last_of(L'"') +
                                      index_quote;

                        string_node->m_String =
                            m_Parser->m_Str.substr(string_node->m_Index, index_quote - string_node->m_Index);

                        if(is_comma) // ','
                        {
                            NodePtr node = m_Parser->GetCurrentStruct();
                            StructNext{m_Parser}.Parse(node, index_comma + 1u);
                        }
                        else // '}'
                        {
                            m_Parser->EndStruct(index_brace);
                            // pour s'assurer d'avoir le parent.
                            NodePtr parent_struct_node = m_Parser->GetCurrentStruct();
                            StructEnd{m_Parser}.Parse(parent_struct_node, index_brace + 1u);
                        }

                        // On teste entre les guillemets et la virgule la
                        // présence ou non d'autres caractères que les espaces, saut
                        //  de lignes, etc. Si oui le nœud sera invalidé.
                        if(!ma::TrimStart(m_Parser->m_Str.substr(index_quote + 1u, index_next - index_quote - 1u))
                                .empty())
                        {
                            string_node->m_Type = Node::Type::Invalid;
                        }
                    }
                    else
                    {
                        string_node->m_Type = Node::Type::Invalid;
                    }
                }
                else
                {
                    string_node->m_Type = Node::Type::Invalid;
                }
            }
        }
    } // namespace Serialization

    template<>
    MA_SFINAE_EXPORT_STRING_NUMBER(bool)
    toString<bool>(bool value)
    {
        std::wstringstream ss;
        ss << std::boolalpha << value;
        return ss.str();
    }

    template<>
    bool FromNode<bool>(const MA_SFINAE_EXPORT_NODE_NUMBER(bool) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        std::wistringstream is(std::wstring(node->m_String));
        bool boolean_value = false;
        is >> std::boolalpha >> boolean_value;

        return boolean_value;
    }

    template<>
    bool IsValidNode<bool>(const MA_SFINAE_EXPORT_NODE_NUMBER(bool) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            return node->m_String == L"false"sv || node->m_String == L"true"sv;
            //     ||
            //           value == "0" || value == "1" ||
            //           value == "faux" || value == "vrais" ||
            //           value == "off" || value == "on" ||
            //           value == "no"  || value == "yes";
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<bool>(const MA_SFINAE_EXPORT_NODE_NUMBER(bool) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"node->m_Nodes.size() : "s + ma::toString(node->m_Nodes.size()) + L" != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"(node->m_Type: " + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value");
        else if(node->m_String == L"false"sv || node->m_String == L"true"sv)
            errors.emplace_back(
                node->m_Index, L"(node->m_String: )" + std::wstring(node->m_String) + L"ne représente pas un booléen.");

        return errors;
    }

    template<>
    int FromNode<int>(const MA_SFINAE_EXPORT_NODE_NUMBER(int) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stoi(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<int>(const MA_SFINAE_EXPORT_NODE_NUMBER(int) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_IntRegType))
            {
                try
                {
                    std::stoi(str);
                    result = true;
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<int>(const MA_SFINAE_EXPORT_NODE_NUMBER(int) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"node->m_Nodes.size() : "s + ma::toString(node->m_Nodes.size()) + L" != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"(node->m_Type: " + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value");
        else
        {
            const std::wstring str{node->m_String};
            if(!std::regex_match(str, ma::g_IntRegType))
            {
                errors.emplace_back(node->m_Index, L"std::regex_match(str, ma::g_IntRegType) == false");
            }
            else
            {
                try
                {
                    std::stoi(str);
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(node->m_Index,
                                        LR"(std::stoi(")"s + str + LR"(") => )" + ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    long FromNode<long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stol(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_IntRegType))
            {
                try
                {
                    std::stol(str);
                    result = true;
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"node->m_Nodes.size() : "s + ma::toString(node->m_Nodes.size()) + L" != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"(node->m_Type: " + GetTypeStr(node->m_Type) + L") != ma::Serialization::Node::Value");
        else
        {
            const std::wstring str{node->m_String};
            if(!std::regex_match(str, ma::g_IntRegType))
            {
                errors.emplace_back(node->m_Index, L"std::regex_match(str, ma::g_IntRegType) == false");
            }
            else
            {
                try
                {
                    std::stol(str);
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(node->m_Index,
                                        LR"(std::stol(")"s + str + LR"(") => )" + ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    long long FromNode<long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long long) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stoll(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long long) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_IntRegType))
            {
                try
                {
                    std::stoll(str);
                    result = true;
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(long long) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(
                node->m_Index, L"[long long] node->m_Nodes.size() : "s + ma::toString(node->m_Nodes.size()) + L" != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[long long] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value");
        else
        {
            const std::wstring str{node->m_String};
            if(!std::regex_match(str, ma::g_IntRegType))
            {
                errors.emplace_back(node->m_Index, L"[long long] std::regex_match(str, ma::g_IntRegType) == false");
            }
            else
            {
                try
                {
                    std::stoll(str);
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(
                        node->m_Index, LR"([long long] std::stoll(")"s + str + LR"(") => )" + ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    unsigned FromNode<unsigned>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stoul(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<unsigned>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_UIntRegType))
            {
                try
                {
                    // Pas de stou
                    unsigned long x = std::stoul(str);
                    result = std::numeric_limits<unsigned>::max() >= x;
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<unsigned>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[unsigned] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L") != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[unsigned] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value}");
        else
        {
            const std::wstring str{node->m_String};
            if(!std::regex_match(str, ma::g_UIntRegType))
            {
                errors.emplace_back(node->m_Index, L"[unsigned] std::regex_match(str, ma::g_UIntRegType) == false");
            }
            else
            {
                try
                {
                    unsigned long x = std::stoul(str);

                    if(std::numeric_limits<unsigned>::max() < x)
                        errors.emplace_back(node->m_Index, L"[unsigned] std::numeric_limits<unsigned>::max() < x");
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(node->m_Index,
                                        LR"([unsigned] std::stoul(")"s + str + LR"(") => )" + ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    unsigned long FromNode<unsigned long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stoul(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<unsigned long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_UIntRegType))
            {
                try
                {
                    std::stoul(str);
                    result = true;
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<unsigned long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[unsigned long] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) +
                                    L") != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[unsigned long] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value");
        else
        {
            const std::wstring str{node->m_String};
            if(!std::regex_match(str, ma::g_UIntRegType))
            {
                errors.emplace_back(node->m_Index,
                                    L"[unsigned long] std::regex_match(str, ma::g_UIntRegType) == false");
            }
            else
            {
                try
                {
                    std::stoul(str);
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(node->m_Index,
                                        LR"([unsigned long] std::stoul(")"s + str + LR"(") => )" +
                                            ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    unsigned long long FromNode<unsigned long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long long) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stoull(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<unsigned long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long long) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_UIntRegType))
            {
                try
                {
                    std::stoull(str);
                    result = true;
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<unsigned long long>(const MA_SFINAE_EXPORT_NODE_NUMBER(unsigned long long) &
                                                            node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[unsigned long long] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) +
                                    L") != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[unsigned long long] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value");
        else
        {
            const std::wstring str{node->m_String};
            if(!std::regex_match(str, ma::g_UIntRegType))
            {
                errors.emplace_back(node->m_Index,
                                    L"[unsigned long long] std::regex_match(str, ma::g_UIntRegType) == false");
            }
            else
            {
                try
                {
                    std::stoull(str);
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(node->m_Index,
                                        LR"([unsigned long long] std::stoull(")"s + str + LR"(") => )" +
                                            ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    float FromNode<float>(const MA_SFINAE_EXPORT_NODE_NUMBER(float) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stof(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<float>(const MA_SFINAE_EXPORT_NODE_NUMBER(float) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_RealRegType))
            {
                try
                {
                    // auto trim = Trim(str);
                    // M_MSG("str: " + str)
                    // M_MSG("trim: " + trim)
                    //                std::wistringstream iss(ma::Trim(str));
                    //                float x;
                    //                iss >> x;
                    //                result = iss && iss.eof();
                    result = !std::isnan(std::stof(str));
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<float>(const MA_SFINAE_EXPORT_NODE_NUMBER(float) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[float] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L") != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[float] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value");
        else
        {
            const std::wstring str{node->m_String};
            if(!std::regex_match(str, ma::g_RealRegType))
            {
                errors.emplace_back(node->m_Index, L"[float] std::regex_match(str, ma::g_RealRegType) == false");
            }
            else
            {
                try
                {
                    float x = std::stof(str);
                    if(std::isnan(x))
                        errors.emplace_back(node->m_Index, L"[float] std::isnan(x) == true");
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(node->m_Index,
                                        LR"([float] std::stof(")"s + str + LR"(") => )" + ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    double FromNode<double>(const MA_SFINAE_EXPORT_NODE_NUMBER(double) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stod(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<double>(const MA_SFINAE_EXPORT_NODE_NUMBER(double) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;
            if(std::regex_match(str, g_RealRegType))
            {
                try
                {
                    // std::stod(str);
                    // https://stackoverflow.com/questions/392981/how-can-i-convert-string-to-double-in-c
                    // https://stackoverflow.com/questions/447206/c-isfloat-function
                    //                std::wistringstream iss(ma::Trim(str));
                    //                double x;
                    //                iss >> x;
                    //                result = iss && iss.eof();
                    //
                    result = !std::isnan(std::stod(str));
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<double>(const MA_SFINAE_EXPORT_NODE_NUMBER(double) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[double] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L") != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[double] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value");
        else
        {
            const std::wstring str{node->m_String};

            if(!std::regex_match(str, ma::g_RealRegType))
            {
                errors.emplace_back(node->m_Index, L"[double] std::regex_match(str, ma::g_RealRegType) == false");
            }
            else
            {
                try
                {
                    auto x = std::stod(str);
                    if(std::isnan(x))
                        errors.emplace_back(node->m_Index, L"[double] std::isnan(x) == true");
                }
                catch(const std::exception &e)
                {
                    errors.emplace_back(node->m_Index,
                                        LR"([double] std::stod(")"s + str + LR"(") => )" + ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    template<>
    long double FromNode<long double>(const MA_SFINAE_EXPORT_NODE_NUMBER(long double) & node)
    {
        MA_ASSERT(
            node->m_Type != ma::Serialization::Node::Invalid, L"Le nœud n'est pas valide.", std::invalid_argument);

        MA_ASSERT(
            node->m_Type == ma::Serialization::Node::Value, L"Le nœud n'est pas une valeur.", std::invalid_argument);

        MA_ASSERT(node->m_Nodes.empty(),
                  L"Le nœud est une valeur qui possède des enfants. Ce qui n'est pas dans la logique de ces nœuds.",
                  std::invalid_argument);

        return std::stold(std::wstring(node->m_String));
    }

    template<>
    bool IsValidNode<long double>(const MA_SFINAE_EXPORT_NODE_NUMBER(long double) & node)
    {
        if(node->m_Type == ma::Serialization::Node::Value)
        {
            const std::wstring str{node->m_String};
            bool result = false;

            if(std::regex_match(str, g_RealRegType))
            {
                try
                {
                    //                std::wistringstream iss(ma::Trim(str));
                    //                long double x;
                    //                iss >> x;
                    //                result = iss && iss.eof();

                    result = !std::isnan(std::stold(str));
                }
                catch(const std::exception &e)
                {
                    result = false;
                }
            }
            return result;
        }
        else
        {
            return false;
        }
    }

    template<>
    ma::Serialization::Errors GetErrors<long double>(const MA_SFINAE_EXPORT_NODE_NUMBER(long double) & node)
    {
        ma::Serialization::Errors errors;

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[long double] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) +
                                    L") != 0");

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[long double] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value.");
        else
        {
            const std::wstring str{node->m_String};

            if(!std::regex_match(str, ma::g_RealRegType))
            {
                errors.emplace_back(node->m_Index, L"[long double] std::regex_match(str, ma::g_RealRegType) == false");
            }
            else
            {
                try
                {
                    auto x = std::stold(str);
                    if(std::isnan(x))
                        errors.emplace_back(node->m_Index, L"[long double] std::isnan(x) == true");
                }
                catch(const std::exception &e)
                {
                    // \todo utiliser std::quoted(L"toto") à la place de LR"("toto")"
                    errors.emplace_back(node->m_Index,
                                        LR"([long double] std::stold(")"s + str + LR"(") => )" +
                                            ma::to_wstring(e.what()));
                }
            }
        }

        return errors;
    }

    // Serialization wstring////////////////////////////////////////////////////

    template<>
    bool IsRepresentedByString<std::wstring>()
    {
        return true;
    }

    template<>
    bool IsRepresentedByString<std::wstring &>()
    {
        return true;
    }

    template<>
    bool IsRepresentedByString<std::wstring &&>()
    {
        return true;
    }

    template<>
    bool IsRepresentedByString<const std::wstring>()
    {
        return true;
    }

    template<>
    bool IsRepresentedByString<const std::wstring &>()
    {
        return true;
    }

    template<>
    bool IsRepresentedByString<const std::wstring &&>()
    {
        return true;
    }

    // Serialization Item //////////////////////////////////////////////////////
    template<>
    bool IsRepresentedByString<ma::Item>()
    {
        return true;
    }

    template<>
    bool IsRepresentedByString<ma::ItemPtr>()
    {
        return true;
    }

    template<>
    bool IsValidString<ma::Item>(const MA_SFINAE_STRING_ITEM_T &str)
    {
        return str.empty() || ma::Item::HasItem(str);
    }

    template<>
    bool IsValidString<ma::ItemPtr>(const MA_SFINAE_STRING_ITEM_T &str)
    {
        return str.empty() || ma::Item::HasItem(str);
    }

    template<>
    bool IsValidNode<ma::Item>(const MA_SFINAE_NODE_ITEM_T &node)
    {
        bool result = node->m_Type == ma::Serialization::Node::Value && node->m_Nodes.empty();

        return result && (node->m_String.empty() || ma::Item::HasItem(std::wstring{node->m_String}));
    }

    template<>
    bool IsValidNode<ma::ItemPtr>(const MA_SFINAE_NODE_ITEM_T &node)
    {
        bool result = node->m_Type == ma::Serialization::Node::Value && node->m_Nodes.empty();

        return result && (node->m_String.empty() || ma::Item::HasItem(std::wstring{node->m_String}));
    }

    template<>
    ma::Serialization::Errors GetErrors<ma::Item>(const MA_SFINAE_NODE_ITEM_T &node)
    {
        ma::Serialization::Errors errors;

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[ma::Item] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value");

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[ma::Item] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) + L" != 0");

        if(!node->m_String.empty() && !ma::Id::IsValid(node->m_String))
            errors.emplace_back(node->m_Index, L"[ma::Item] ma::Id::IsValid(node->m_String) == false");

        if(!node->m_String.empty() && !ma::Item::HasItem(std::wstring{node->m_String}))
            errors.emplace_back(node->m_Index, L"[ma::Item] ma::Item::HasItem(std::wstring{node->m_String} == false");

        return errors;
    }

    template<>
    ma::Serialization::Errors GetErrors<ma::ItemPtr>(const MA_SFINAE_NODE_ITEM_T &node)
    {
        ma::Serialization::Errors errors;

        if(node->m_Type != ma::Serialization::Node::Value)
            errors.emplace_back(node->m_Index,
                                L"[ma::ItemPtr] (node->m_Type: " + GetTypeStr(node->m_Type) +
                                    L") != ma::Serialization::Node::Value");

        if(!node->m_Nodes.empty())
            errors.emplace_back(node->m_Index,
                                L"[ma::ItemPtr] node->m_Nodes.size(): "s + ma::toString(node->m_Nodes.size()) +
                                    L" != 0");

        if(!node->m_String.empty() && !ma::Id::IsValid(node->m_String))
            errors.emplace_back(node->m_Index, L"[ma::ItemPtr] ma::Id::IsValid(node->m_String) == false");

        if(!node->m_String.empty() && !ma::Item::HasItem(std::wstring{node->m_String}))
            errors.emplace_back(node->m_Index,
                                L"[ma::ItemPtr] ma::Item::HasItem(std::wstring{node->m_String}) == false");

        return errors;
    }
} // namespace ma
