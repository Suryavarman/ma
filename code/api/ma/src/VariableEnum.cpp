/// \file VariableEnum.cpp
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/VariableEnum.h"

namespace ma
{
    const TypeInfo &EnumerationVar::GetEnumerationVarTypeInfo()
    {
        static const TypeInfo info = {GetEnumerationVarClassName(),
                                      L"Cette variable d'item représente une énumération de chaînes de "
                                      L"caractères associées à une valeur entière non signée. La valeur sélectionnée "
                                      L"est représentée par une deuxième variable de type EnumerationChoiceVar." +
                                          LR"(Exemple: {{"deux", 2}, {"un", 1}})"s,
                                      {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(EnumerationVar, Var)

    const TypeInfo &EnumerationChoiceVar::GetEnumerationChoiceVarTypeInfo()
    {
        static const TypeInfo info = {
            GetEnumerationChoiceVarClassName(),
            L"Cette variable d'item représente la clef sélectionnée parmi les clefs d'une variable d'énumération de "
            L"type EnumerationVar." +
                LR"(Exemple: {"deux", {"eba90734-7441-4541-8516-f98f48101a2c", "m_Enumeration"}})"s,
            {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(EnumerationChoiceVar, Var)
} // namespace ma
