/// \file Info.ut.cpp
/// \author Pontier Pierre
/// \date 2020-04-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires de la classe TypeInfo.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

void info_setup()
{
    ma::g_ConsoleVerbose = 1u;
}

void info_teardown()
{

}

namespace ma
{
    class M_DLL_EXPORT TestInfo: public ma::Item
    {
        protected:
            using ma::Item::Item;

        public:
            M_HEADER_CLASSHIERARCHY(TestInfo)
    };
    typedef std::shared_ptr<TestInfo> TestInfoPtr;
}

namespace ma
{
    const TypeInfo& TestInfo::GetTestInfoTypeInfo()
    {
        static const TypeInfo info =
        {
            GetTestInfoClassName(),
            L"La classe «TestInfo» permet de définir le milieu dans lequel une" \
            L"ressource sera utilisée.",
            {}
        };

        return info;
    }
    M_CPP_CLASSHIERARCHY(TestInfo, Item)
}

Test(Info, first_call, .init = info_setup, .fini = info_teardown)
{
    auto hierarchies = ma::TypeInfo::GetClassHierarchies();

    auto test_classname = ma::TestInfo::GetTestInfoClassName();

    auto it_find = hierarchies.find(test_classname);

    cr_assert_neq(it_find, hierarchies.end());

    std::vector<std::wstring> class_names =
    {
        test_classname,
        ma::Item::GetItemClassName(),
        ma::Observable::GetObservableClassName()
    };

    auto hierarchy = it_find->second;

    cr_assert_eq(hierarchy.size(), 3u);

    size_t i = 0;
    for(auto it : hierarchy)
    {
        cr_assert_eq(it.size(), 1u);

        ma::TypeInfo type = *it.begin();

        if(i < class_names.size())
            cr_assert_eq(type.m_ClassName, class_names[i]);

        i++;
    }
}

Test(Info, second_call, .init = info_setup, .fini = info_teardown)
{
    auto hierarchies = ma::TypeInfo::GetClassHierarchies();

    auto test_classname = ma::TestInfo::GetTestInfoClassName();

    auto it_find = hierarchies.find(test_classname);

    cr_assert_neq(it_find, hierarchies.end());

    std::vector<std::wstring> class_names =
    {
        test_classname,
        ma::Item::GetItemClassName(),
        ma::Observable::GetObservableClassName()
    };

    auto hierarchy = it_find->second;

    cr_assert_eq(hierarchy.size(), 3u);

    size_t i = 0;
    for(auto it : hierarchy)
    {
        cr_assert_eq(it.size(), 1u);

        ma::TypeInfo type = *it.begin();

        if(i < class_names.size())
            cr_assert_eq(type.m_ClassName, class_names[i]);

        i++;
    }
}
