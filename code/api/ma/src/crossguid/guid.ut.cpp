/// \file crossguid/guid.ut.cpp
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Tests unitaires de la classe guid.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
/// Les tests sont ceux de crossguid transposé pour criterion.
/// (https://github.com/graeme-hill/crossguid/blob/master/test/Test.cpp)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

Test(guid, random, .description = "Génération automatique des GUID.")
{
	auto r1 = xg::newGuid();
	auto r2 = xg::newGuid();
	auto r3 = xg::newGuid();

    std::stringstream ss1;
	ss1 << r1;
	cr_assert_eq(ss1.str(), r1.str());

    std::stringstream ss2;
	ss2 << r2;
	cr_assert_eq(ss2.str(), r2.str());

    std::stringstream ss3;
	ss3 << r3;
	cr_assert_eq(ss3.str(), r3.str());

	cr_assert_neq(r1, r2);
	cr_assert_neq(r1, r3);
	cr_assert_neq(r3, r2);
}

Test(guid, manual, .description = "Génération manuelle des GUID.")
{
	xg::Guid s1("7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");
	xg::Guid s2("16d1bd03-09a5-47d3-944b-5e326fd52d27");
	xg::Guid s3("fdaba646-e07e-49de-9529-4499a5580c75");
	xg::Guid s4("7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");
	xg::Guid s5("7bcd757f-5b10-4f9b-af69-1a1f226f3b31");

    cr_assert_neq(s1, s2);
	cr_assert_neq(s1, s3);
	cr_assert_neq(s2, s3);
	cr_assert_neq(s4, s5);
	cr_assert_lt(s5, s4);

    std::stringstream ss1;
	ss1 << s1;
	cr_assert_eq(ss1.str(), "7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");
	cr_assert_eq(s1.str(), "7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");

	std::stringstream ss2;
	ss2 << s2;
	cr_assert_eq(ss2.str(), "16d1bd03-09a5-47d3-944b-5e326fd52d27");

	std::stringstream ss3;
	ss3 << s3;
	cr_assert_eq(ss3.str(), "fdaba646-e07e-49de-9529-4499a5580c75");
}

Test(guid, swap, .description = "Gestion des échanges de valeurs(«swap»).")
{
    auto swap1 = xg::newGuid();
	auto swap2 = xg::newGuid();
	auto swap3 = swap1;
	auto swap4 = swap2;

	cr_assert_neq(swap1, swap2);
	cr_assert_eq(swap1, swap3);
	cr_assert_eq(swap2, swap4);

	swap1.swap(swap2);

	cr_assert_neq(swap1, swap2);
	cr_assert_eq(swap1, swap4);
	cr_assert_eq(swap2, swap3);
}

Test(guid, hashing, .description = "Gestion du hachage.")
{
    xg::Guid s1("7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");
    xg::Guid s2("16d1bd03-09a5-47d3-944b-5e326fd52d27");
    xg::Guid s3("fdaba646-e07e-49de-9529-4499a5580c75");
    xg::Guid s4("7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");
    xg::Guid s5("7bcd757f-5b10-4f9b-af69-1a1f226f3b31");

    std::unordered_map<xg::Guid, int> m = {{s1, 1}, {s2, 2}};
    auto it1 = m.find(s1);
    auto it2 = m.find(s2);

    cr_assert_neq(it1, m.end());
    cr_assert_eq(it1->first, s1);
    cr_assert_eq(it1->second, 1);

    cr_assert_neq(it2, m.end());
    cr_assert_eq(it2->first, s2);
    cr_assert_eq(it2->second, 2);

    auto it3 = m.find(s3);
    cr_assert_eq(it3, m.end());
}

Test(guid, from_bytes, .description = "Création des GUID à partir d'un tableau de bytes.")
{
	std::array<unsigned char, 16> bytes =
	{{
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xdd
	}};

	xg::Guid guidFromBytes(bytes);
	xg::Guid guidFromString("0102030405060708090a0b0c0d0e0fdd");

	cr_assert_eq(guidFromBytes, guidFromString);
	cr_assert(std::equal(guidFromBytes.bytes().begin(), guidFromBytes.bytes().end(), bytes.begin()));
}

Test(guid, errors_handling, .description = "Gestion des erreurs.")
{
    xg::Guid empty_guid;

	xg::Guid twoTooFew("7bcd757f-5b10-4f9b-af69-1a1f226f3b");
	cr_assert_eq(twoTooFew, empty_guid);
	cr_assert_not(twoTooFew.isValid());

	xg::Guid oneTooFew("16d1bd03-09a5-47d3-944b-5e326fd52d2");
    cr_assert_eq(oneTooFew, empty_guid);
	cr_assert_not(oneTooFew.isValid());

	xg::Guid twoTooMany("7bcd757f-5b10-4f9b-af69-1a1f226f3beeff");
    cr_assert_eq(twoTooMany, empty_guid);
	cr_assert_not(twoTooMany.isValid());

	xg::Guid oneTooMany("16d1bd03-09a5-47d3-944b-5e326fd52d27a");
    cr_assert_eq(oneTooMany, empty_guid);
	cr_assert_not(oneTooMany.isValid());

	xg::Guid badString("!!bad-guid-string!!");
    cr_assert_eq(badString, empty_guid);
	cr_assert_not(badString.isValid());
}
