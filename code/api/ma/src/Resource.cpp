/// \file Resource.cpp
/// \author Pontier Pierre
/// \date 2020-03-29
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Macros.h"
#include "ma/Resource.h"
#include "ma/Config.h"
#include "ma/ObservationManager.h"
#include "ma/ClassInfo.h"

#include <wx/dir.h>

// https://docs.wxwidgets.org/trunk/classwx_file_system_watcher.html
// c++17 link filesystem : http://nanapro.org/en-us/forum/index.php?u=/topic/1217/ggfolderbox-and-c-17 -> utiliser gcc9

// #include <filesystem> // c++17
//  namespace fs = std::filesystem; // c++17

// #include <experimental/filesystem>
//  namespace fs = std::experimental::filesystem;

// namespace ma
//{
//    class FolderSmile
//    {
//        public:
//            FolderSmile(){}
//            virtual ~FolderSmile(){}
//
//            /// \return Vrais si le chemin existe, n'est pas un raccourcis et est un dossier.
//            /// \see https://en.cppreference.com/w/cpp/filesystem/exists
//            static bool Exist(const fs::path& path, const fs::file_status& status = fs::file_status{});
//    };
//}
//
// bool ma::FolderSmile::Exist(const fs::path& path, const fs::file_status& status)
//{
//    bool result = false;
//
//    if(fs::status_known(status) ? fs::exists(status) : fs::exists(path))
//        result = true;
//
//    return result;
//}

namespace ma
{
    const Var::key_type Resource::ms_KeyPath(L"m_FilePath");
    const Var::key_type Resource::ms_KeySize(L"m_Size");

    const Observable::Data::key_type Folder::Key::Obs::ms_ConstructionEnd(L"folder_end_construction");

    const Observable::Data::key_type FileWatcher::Key::Obs::ms_Add{L"file_watcher_add"};
    const Observable::Data::key_type FileWatcher::Key::Obs::ms_Delete{L"file_watcher_delete"};
    const Observable::Data::key_type FileWatcher::Key::Obs::ms_Modified{L"file_watcher_modified"};
    const Observable::Data::key_type FileWatcher::Key::Obs::ms_Created{L"file_watcher_created"};
    const Observable::Data::key_type FileWatcher::Key::Obs::ms_Rename{L"file_watcher_rename"};

    const Resource::MatchResource &Resource::GetMatchResource()
    {
        static const Resource::MatchResource matcher({Resource::ms_KeyPath});

        return matcher;
    }

    const Folder::MatchFolder &Folder::GetMatchFolder()
    {
        static const Folder::MatchFolder matcher({Folder::Key::Var::GetPath()});

        return matcher;
    }

    std::wstring Permissions(const std::filesystem::perms &p)
    {
        using std::filesystem::perms;
        std::wstringstream ss;

        auto show = [&](char op, const perms &perm)
        {
            ss << (perms::none == (perm & p) ? '-' : op);
        };

        show('r', perms::owner_read);
        show('w', perms::owner_write);
        show('x', perms::owner_exec);
        show('r', perms::group_read);
        show('w', perms::group_write);
        show('x', perms::group_exec);
        show('r', perms::others_read);
        show('w', perms::others_write);
        show('x', perms::others_exec);

        return ss.str();
    }

#if(MA_PLATFORM == MA_PLATFORM_WIN32)
    // Ce code est une copie de la fonction wxCheckWin32Permission de wxWidgets.
    // License: https://docs.wxwidgets.org/3.2.0/page_copyright_wxlicense.html
    // https://github.com/wxWidgets/wxWidgets/blob/master/src/common/filefn.cpp#L957
    bool CheckWin32Permission(const wxString& path, DWORD access)
    {
        // quoting the MSDN: "To obtain a handle to a directory, call the
        // CreateFile function with the FILE_FLAG_BACKUP_SEMANTICS flag"
        const DWORD dwAttr = ::GetFileAttributes(path.t_str());
        if ( dwAttr == INVALID_FILE_ATTRIBUTES )
        {
            // file probably doesn't exist at all
            return false;
        }

        const HANDLE h = ::CreateFile
        (
            path.t_str(),
            access,
            FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
            nullptr,
            OPEN_EXISTING,
            dwAttr & FILE_ATTRIBUTE_DIRECTORY
                ? FILE_FLAG_BACKUP_SEMANTICS
                : 0,
            nullptr
        );
        if ( h != INVALID_HANDLE_VALUE )
            CloseHandle(h);

        return h != INVALID_HANDLE_VALUE;
    }
#endif

    // \see https://github.com/wxWidgets/wxWidgets/blob/master/src/common/filefn.cpp#L1001
    bool IsReadable(const std::filesystem::path &path)
    {
#if defined(__UNIX__)
        const bool ok = access(path.c_str(), R_OK) == 0;
#elif(MA_PLATFORM == MA_PLATFORM_WIN32)
        const bool ok = CheckWin32Permission(path, GENERIC_READ);
#else
    #error "Cette plateforme n'est pas gérée"
#endif
        return ok;
    }

    // \see https://github.com/wxWidgets/wxWidgets/blob/master/src/common/filefn.cpp#L987
    bool IsWritable(const std::filesystem::path &path)
    {
#if defined(__UNIX__)
        const bool ok = access(path.c_str(), W_OK) == 0;
#elif(MA_PLATFORM == MA_PLATFORM_WIN32)
        const bool ok = CheckWin32Permission(path, GENERIC_WRITE);
#else
    #error "Cette plateforme n'est pas gérée"
#endif
        return ok;
    }

    // \see https://github.com/wxWidgets/wxWidgets/blob/master/src/common/filefn.cpp#L1015
    bool IsExecutable(const std::filesystem::path &path)
    {
#if defined(__UNIX__)
        const bool ok = access(path.c_str(), X_OK) == 0;
#elif(MA_PLATFORM == MA_PLATFORM_WIN32)
        const bool ok = CheckWin32Permission(path, GENERIC_EXECUTE);
#else
    #error "Cette plateforme n'est pas gérée"
#endif
        return ok;
    }

    inline void DirAssertValid(const std::filesystem::path &path,
                               const std::wstring &function_name = M_FUNCTION_NAME,
                               bool writable = false)
    {
        const std::wstring value = path.wstring();

        MA_ASSERT(
            std::filesystem::exists(path), L"Le chemin n'existe pas: " + value, function_name, std::invalid_argument);

        MA_ASSERT(std::filesystem::is_directory(path),
                  L"Le chemin est protégé en lecture: " + value,
                  function_name,
                  std::invalid_argument);

        const auto status = std::filesystem::status(path);

        const auto str_perms = Permissions(status.permissions());
        MA_ASSERT(IsReadable(path),
                  L"Le chemin est protégé en lecture: " + value + L" Les permissions sont : " + str_perms,
                  function_name,
                  std::invalid_argument);

        if(writable)
            MA_ASSERT(IsReadable(path),
                      L"Le chemin est protégé en lecture: " + value + L" Les permissions sont : " + str_perms,
                      function_name,
                      std::invalid_argument);
    }

    inline void Normalize(std::filesystem::path &out_path,
                          bool absolute = false,
                          const std::wstring &function_name = M_FUNCTION_NAME)
    {
        // DirAssertValid(out_path, function_name);

        // Remplacer les ${} par leurs valeurs
        out_path = ExpandEnv(out_path.wstring());

        // Si le chemin n'existe pas et qu'il est relatif, parcourir les paths et au premier exists(VENV_PATH + path),
        // ajouter le VENV_PATH
        auto const env_paths = GetEnvPaths();
        for(auto it = env_paths.cbegin(); it != env_paths.cend() && !std::filesystem::exists(out_path); ++it)
        {
            if(std::filesystem::exists(*it / out_path))
                out_path = *it / out_path;
        }

        // \todo Si c'est un raccourci, récupérer la vraie valeur du chemin. ... ou pas

        // Si besoin rendre absolu le chemin
        try
        {
            // lename.Normalize(wxPATH_NORM_ABSOLUTE | wxPATH_NORM_ENV_VARS | wxPATH_NORM_SHORTCUT);
            out_path = std::filesystem::absolute(out_path.lexically_normal());
        }
        catch(std::exception &e)
        {
            MA_ASSERT(false,
                      L"La normalization du chemin a échouée: « " + out_path.wstring() + L" ».",
                      function_name,
                      std::invalid_argument);
        }
    }

    inline void FileAssertValid(const std::filesystem::path &path, const std::wstring &function_name)
    {
        std::wstring const value = path.wstring();

        MA_ASSERT(std::filesystem::is_regular_file(path),
                  L"Le chemin du fichier n'est pas un fichier: " + value,
                  function_name,
                  std::invalid_argument);

        //    MA_ASSERT(filename.FileExists(),
        //              L"Le fichier n'existe pas: " + path,
        //              function_name,
        //              std::invalid_argument);

        // Obligé d'utilisé la fonction « Exists() » pour que les cas où le fichier est un raccourci qui pointe sur rien.
        MA_ASSERT(
            std::filesystem::exists(path), L"Le fichier n'existe pas: " + value, function_name, std::invalid_argument);

        //    wxFileName tmp = filename;
        //    tmp.DontFollowLink();// Doesn't work…
        //    MA_ASSERT(tmp.FileExists(),
        //              L"Le fichier n'existe pas: " + path,
        //              function_name,
        //              std::invalid_argument);

        //    MA_ASSERT(filename.IsFileReadable(),
        //              "Le fichier est protégé en écriture: "  + path,
        //              function_name,
        //              std::invalid_argument);
    }

    std::uintmax_t GetFileSize(const std::filesystem::path& path)
    {
        if(std::filesystem::exists(path))
        {
            if(std::filesystem::is_directory(path))
            {
                std::uintmax_t sum = 0;

                for(auto const& sub_path : std::filesystem::directory_iterator(path))
                    sum += GetFileSize(sub_path);

                return sum;
            }
            else
                return std::filesystem::file_size(path);
        }
        else
            return 0;
    }

    // https://stackoverflow.com/questions/63512258/how-can-i-print-a-human-readable-file-size-in-c-without-a-loop
    // https://en.cppreference.com/w/cpp/filesystem/file_size
    struct HumanReadable
    {
            std::uintmax_t size{};

            std::wstring operator()()
            {
                std::wstringstream ss;

                ss << *this;

                return ss.str();
            }

        private:
            friend std::wostream &operator<<(std::wostream &os, HumanReadable hr)
            {
                int o{};
                auto mantissa = static_cast<double>(hr.size);
                for(; mantissa >= 1024.; mantissa /= 1024., ++o)
                    ;
                os << std::ceil(mantissa * 10.) / 10. << L"BKMGTPE"[o];
                return o ? os << L"B (" << hr.size << L')' : os;
            }
    };

    // inline void FileNormalize(std::filesystem::path &out_path, const std::wstring &function_name)
    //{
    //     // Si le fichier est un raccourci pointant sur un fichier ou un dossier qui n'existe pas, « FileExists » ne doit pas
    //     // générer d'erreur, car le raccourci lui existe bien.
    //     // Dommage ça ne fonctionne pas si le raccourci pointe sur rien.
    //     // out_path.DontFollowLink();
    //     FileAssertValid(out_path, function_name);
    //
    //     out_path = ExpandEnv(out_path.wstring());
    //
    //     // Si le chemin n'existe pas et qu'il est relatif, parcourir les paths et au premier exists(VENV_PATH + path),
    //     // ajouter le VENV_PATH
    //     auto const env_paths = GetEnvPaths();
    //     for(auto it = env_paths.cbegin(); it != env_paths.cend() && !std::filesystem::exists(out_path); ++it)
    //     {
    //         if(std::filesystem::exists(*it / out_path))
    //             out_path = *it / out_path;
    //     }
    //
    //
    //     bool normalize_success = path.Normalize(wxPATH_NORM_ABSOLUTE | wxPATH_NORM_ENV_VARS); //|wxPATH_NORM_SHORTCUT);
    //
    //     MA_ASSERT(normalize_success, L"La normalization a échouée.", function_name, std::invalid_argument);
    // }

    std::wstring GetFileSize(const FilePathVarPtr &path)
    {
        return HumanReadable{std::filesystem::file_size(path->Get())}();
    }

    std::filesystem::path GetResFileName(const std::wstring &value)
    {
        std::filesystem::path path{value};

        if(std::filesystem::exists(path))
            Normalize(path);

        return path;
    }

    std::filesystem::path GetResFileName(const std::filesystem::path &value)
    {
        std::filesystem::path path{value};

        if(std::filesystem::exists(path))
            Normalize(path);

        return path;
    }

    // Resource //——————————————————————————————————————————————————————————————————————————————————————————————————————

    Resource::Resource(const Item::ConstructorParameters &in_params, const std::filesystem::path &path):
    Item(in_params),
    m_Path(AddReadOnlyMemberVar<FilePathVar>(ms_KeyPath, GetResFileName(path))),
    m_Size(AddReadOnlyMemberVar<StringVar>(ms_KeySize, ma::toString(GetFileSize(path)))),
    m_Name(AddReadOnlyMemberVar<StringVar>(Key::Var::GetName(), path.filename().wstring()))
    {}

    Resource::~Resource() = default;

    const TypeInfo &Resource::GetResourceTypeInfo()
    {
        static const TypeInfo info = {
            GetResourceClassName(),
            L"La classe Resource permet d'associer un fichier à un Item.",
            {{TypeInfo::Key::GetWhiteListParentsData(), {Folder::GetFolderClassName()}}}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(Resource, Item)

    // Folder //————————————————————————————————————————————————————————————————————————————————————————————————————————
    const Var::key_type &Folder::Key::Var::GetPath()
    {
        static const auto result{L"m_FilePath"s};
        return result;
    }

    const Var::key_type &Folder::Key::Var::GetRecursive()
    {
        static const auto result{L"m_Recursive"s};
        return result;
    }

    const Var::key_type &Folder::Key::Var::GetSize()
    {
        static const auto result{L"m_Size"s};
        return result;
    }

    const Var::key_type &Folder::Key::Var::GetHasToExist()
    {
        static const auto result{L"has_to_exist"s};
        return result;
    }

    std::wstring GetDirSize(const DirectoryVarPtr &var)
    {
        auto const &path = var->Get();
        if(std::filesystem::exists(path))
        {
            try
            {
                const auto file_size = GetFileSize(path);
                return HumanReadable{file_size}();
            }
            catch(std::filesystem::filesystem_error& error)
            {
                MA_ASSERT(false, ma::to_wstring(error.what()), std::invalid_argument);
                return L"0o";
            }
        }
        else
            return L"";
    }

    std::wstring GetFolderName(const DirectoryVarPtr &var)
    {
        const auto path = var->Get();

        return path.stem().wstring();
    }

    std::filesystem::path GetPath(const std::wstring &value)
    {
        std::filesystem::path path{value};

        if(std::filesystem::exists(path))
            Normalize(path);

        return path;
    }

    std::filesystem::path GetDirFileName(const std::filesystem::path &value)
    {
        std::filesystem::path path{value};

        if(std::filesystem::exists(path))
            Normalize(path);

        return path;
    }

    // \todo implémenter cette fonction sans utilisé wxWidgets
    std::filesystem::path GetDirFileName(const std::filesystem::directory_entry &dir_entry)
    {
        std::filesystem::path path = dir_entry.path();

        if(std::filesystem::exists(path))
            Normalize(path);

        return path;
    }

    // std::wstring Resource::GetFolderSaveName(const Item& item) const
    //{
    //    auto folder_name = item.GetTypeInfo().m_ClassName;
    //    if(item.HasVar(ms_KeyNameVar))
    //       folder_name += L"_"s + item.GetVar(ms_KeyNameVar)->toString();
    //    return folder_name;
    //}

    Folder::Folder(const Item::ConstructorParameters &in_params,
                       const std::wstring &path,
                       bool recursive,
                       bool has_to_exist):
    Item(in_params),
    m_HasToExist(AddMemberVar<BoolVar>(Key::Var::GetHasToExist(), has_to_exist)),
    m_Path(AddMemberVar<DirectoryVar>(Key::Var::GetPath(), GetPath(path))),
    m_Recursive(AddMemberVar<BoolVar>(Key::Var::GetRecursive(), recursive)),
    m_Size(AddReadOnlyMemberVar<StringVar>(Key::Var::GetSize(), GetDirSize(m_Path))),
    m_Name(AddReadOnlyMemberVar<StringVar>(Item::Key::Var::GetName(), GetFolderName(m_Path)))
    {}

    Folder::Folder(const Item::ConstructorParameters &in_params,
                       const std::filesystem::path &path,
                       bool recursive,
                       bool has_to_exist):
    Item(in_params),
    m_HasToExist(AddMemberVar<BoolVar>(Key::Var::GetHasToExist(), has_to_exist)),
    m_Path(AddMemberVar<DirectoryVar>(Key::Var::GetPath(), path)),
    m_Recursive(AddMemberVar<BoolVar>(Key::Var::GetRecursive(), recursive)),
    m_Size(AddReadOnlyMemberVar<StringVar>(Key::Var::GetSize(), GetDirSize(m_Path))),
    m_Name(AddReadOnlyMemberVar<StringVar>(Item::Key::Var::GetName(), GetFolderName(m_Path)))
    {}

    Folder::Folder(const Item::ConstructorParameters &in_params,
                       const std::filesystem::directory_entry &dir_entry,
                       bool recursive,
                       bool has_to_exist):
    Item(in_params),
    m_HasToExist(AddMemberVar<BoolVar>(Key::Var::GetHasToExist(), has_to_exist)),
    m_Path(AddMemberVar<DirectoryVar>(Key::Var::GetPath(), dir_entry.path())),
    m_Recursive(AddMemberVar<BoolVar>(Key::Var::GetRecursive(), recursive)),
    m_Size(AddReadOnlyMemberVar<StringVar>(Key::Var::GetSize(), GetDirSize(m_Path))),
    m_Name(AddReadOnlyMemberVar<StringVar>(Item::Key::Var::GetName(), GetFolderName(m_Path)))
    {}

    Folder::~Folder() = default;

    void Folder::EndConstruction()
    {
        Item::EndConstruction();

        if(auto opt = Item::HasItemOpt<ObservationManager>(Item::Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();

            observer_manager->AddObservation<ItemObserverVar, VarVar>(this, m_HasToExist);
            observer_manager->AddObservation<ItemObserverVar, VarVar>(this, m_Path);
            observer_manager->AddObservation<ItemObserverVar, VarVar>(this, m_Recursive);
            auto parent_key = GetParentKey();

            if(auto opt_parent = GetParentOpt())
                opt_parent.value()->UpdateObservations({{Key::Obs::ms_ConstructionEnd, GetKey()}});
        }
        else
        {
            Build();
        }
    }

    void Folder::Build()
    {
        auto path = m_Path->Get();

        if(m_HasToExist->Get())
        {
            MA_ASSERT(std::filesystem::exists(path),
                      L"Le chemin «" + path.wstring() +
                          L"»  n'existe pas et il est spécifié que le chemin doit pointer vers un dossier existant "
                          L"m_HasToExist est vrais.",
                      std::logic_error);
        }

        if(std::filesystem::exists(path))
        {
            Normalize(path);

            const auto parent_key = GetKey();

            const auto resources_manager = GetItem<ResourceManager>(Item::Key::GetResourceManager());
            const auto folders = Find<MatchFolder>::GetHashMap(parent_key, GetMatchFolder(), Item::SearchMod::LOCAL);
            const auto resources = Find<Resource::MatchResource>::GetHashMap(
                parent_key, Resource::GetMatchResource(), Item::SearchMod::LOCAL);

            //*
            for(const auto &entry : std::filesystem::directory_iterator{path})
            {
                if(entry.is_directory())
                {
                    // Nous n'acceptons pas les dossiers, seulement si m_Recursive est vrais.
                    if(m_Recursive->Get())
                    {
                        auto sub_path = entry.path();
                        if(IsReadable(sub_path) && IsWritable(sub_path) && folders.find(sub_path) == folders.end() &&
                           !resources_manager->HasFolder(sub_path.wstring()))
                        {
                            Normalize(sub_path);
                            Item::CreateItem<Folder>(
                                {parent_key, true}, sub_path, m_Recursive->Get(), std::filesystem::exists(sub_path));
                        }
                    }
                }
                else
                {
                    auto res_path = GetResFileName(entry.path());

                    if(resources.find(res_path) == resources.end() && std::filesystem::exists(res_path))
                    {
                        Normalize(res_path);
                        Item::CreateItem<Resource>({parent_key}, res_path);
                    }
                }
            }

            /*/
            wxDir dir(path.wstring());
            MA_ASSERT(dir.IsOpened(), L"dir n'est pas ouvert...", function_name, std::runtime_error);

            wxFileName normalize_path = filename;
            DirAssertValid(normalize_path);
            DirNormalize(normalize_path);

            auto folders = Find<MatchFolder>::GetHashMap(GetKey(), GetMatchFolder(), Item::SearchMod::LOCAL);
            auto resources = Find<Resource::MatchResource>::GetHashMap(
                GetKey(), Resource::GetMatchResource(), Item::SearchMod::LOCAL);

            wxString sub_file;
            bool cont = dir.GetFirst(&sub_file, wxEmptyString, wxDIR_DIRS | wxDIR_FILES | wxDIR_NO_FOLLOW);
            while(cont)
            {
                auto path = dir.GetNameWithSep() + sub_file;

                if(wxDir::Exists(path))
                {
                    // Nous n'acceptons les dossiers que si m_Recursive est vrais.
                    if(m_Recursive->Get())
                    {
                        wxFileName sub_filename = GetDirFileName(path);
                        auto resources_manager = GetItem<ResourceManager>(Item::Key::GetResourceManager());

                        if(sub_filename.IsDirReadable() && sub_filename.IsDirWritable() &&
                           folders.find(sub_filename) == folders.end() &&
                           !resources_manager->HasFolder(sub_filename.GetFullPath().ToStdWstring()))
                        {
                            DirNormalize(sub_filename);
                            Item::CreateItem<Folder>(
                                {GetKey(), true}, sub_filename, m_Recursive->Get(), filename.Exists());
                        }
                    }
                }
                else
                {
                    wxFileName sub_filename = GetResFileName(path);
                    // sub_filename.DontFollowLink(); // Ne fonctionne pas du tout.
                    // Du coup, j'ajoute à la condition suivante :
                    // - si le fichier existe.
                    // Je ne peux pas tester si c'est un raccourci ou pas avec wxWidgets.
                    // Alors, je vérifie si le fichier ou le fichier pointé existe.
                    // Cela évitera d'ajouter des raccourcis qui pointent sur rien et qui provoque des erreurs.

                    // if(sub_filename.IsFileReadable())
                    if(resources.find(sub_filename) == resources.end() && sub_filename.FileExists())
                    {
                        FileNormalize(sub_filename);
                        Item::CreateItem<Resource>({GetKey()}, sub_filename);
                    }
                }

                cont = dir.GetNext(&sub_file);
            }
            //*/
        }
    }

    void Folder::BeginObservation(const ObservablePtr &observable)
    {
        // Cela permet de s'assurer que l'on effectue une seul-fois l'appel à Build.
        if(m_Recursive == observable)
            Build();

        if(m_Path == observable)
            SetVarFromString(m_Name->GetKey(), GetFolderName(m_Path));
    }

    /*
    void Folder::UpdateObservation(const ObservablePtr& observable, const Observable::Data& data)
    {
        // \todo Revoir si il faut mettre des BeginBatch et EndBatch dans cette
        // fonction.
        bool own_batch = !GetBatchState();

        if(data.HasActions(Var::ms_KeyObsEndSetValue))
        {
            if(m_FilePath == observable && m_FilePath->GetPrevious() != m_FilePath->Get())
            {
                if(own_batch)
                    BeginBatch();

                SetVarFromString(m_Size->GetKey(), GetDirSize(m_FilePath));
                SetVarFromString(m_Name->GetKey(), GetFolderName(m_FilePath));

                Build();

                if(own_batch)
                    EndBatch();
            }

            if(m_Recursive == observable && m_Recursive->GetPrevious() != m_Recursive->Get())
            {
                if(m_Recursive->Get())
                {
                    if(own_batch)
                        BeginBatch();
                    Build();
                    if(own_batch)
                        EndBatch();
                }
                else
                {
                    // Si ce n'est pas récursif alors nous ne gardons que les
                    // fichiers locaux.
                    auto children = Find<MatchFolder>::GetDeque(GetKey(), GetMatchFolder(),
    Item::SearchMod::LOCAL);

                    if(own_batch)
                        BeginBatch();

                    for(auto child : children)
                        Item::EraseItem(child->GetKey());

                    if(own_batch)
                        EndBatch();
                }
            }

            if(m_HasToExist == observable && m_HasToExist->GetPrevious() != m_HasToExist->Get())
            {
                if(m_HasToExist->Get())
                {
                    wxFileName filename = m_FilePath->Get();
                    if(!filename.Exists())
                    {
                        // Le dossier n'existant pas on ne veux pas déclencher
                        // d'exception. Le chemin d'un vrais dossier devra d'abord
                        // être spécifié.
                        m_HasToExist->Set(false);
                    }
                }

            }
        }

        if(data.HasActions(Item::ms_KeyObsAddItemEnd))
        {
            SetVarFromString(m_Size->GetKey(), GetDirSize(m_FilePath));
        }
    }
    /*/
    void Folder::UpdateObservation(const ObservablePtr &observable, const Observable::Data &data)
    {
        // \todo Revoir si il faut mettre des BeginBatch et EndBatch dans cette
        // fonction.
        // bool own_batch = !GetBatchState();
        const auto &actions = data.GetOrdered();
        for(const auto &action : actions)
        {
            if(action.first == Var::Key::Obs::ms_EndSetValue)
            {
                if(m_Path == observable && m_Path->GetPrevious() != m_Path->Get())
                {
                    //                if(own_batch)
                    //                    BeginBatch();

                    SetVarFromString(m_Size->GetKey(), GetDirSize(m_Path));
                    SetVarFromString(m_Name->GetKey(), GetFolderName(m_Path));

                    Build();

                    //                if(own_batch)
                    //                    EndBatch();
                }

                if(m_Recursive == observable && m_Recursive->GetPrevious() != m_Recursive->Get())
                {
                    if(m_Recursive->Get())
                    {
                        //                    if(own_batch)
                        //                        BeginBatch();
                        Build();
                        //                    if(own_batch)
                        //                        EndBatch();
                    }
                    else
                    {
                        // Si ce n'est pas récursif alors, nous ne gardons que les fichiers locaux.
                        auto children =
                            Find<MatchFolder>::GetDeque(GetKey(), GetMatchFolder(), Item::SearchMod::LOCAL);
                        //
                        //                    if(own_batch)
                        //                        BeginBatch();

                        for(const auto &child : children)
                            Item::EraseItem(child->GetKey());
                        //
                        //                    if(own_batch)
                        //                        EndBatch();
                    }
                }

                if(m_HasToExist == observable && m_HasToExist->GetPrevious() != m_HasToExist->Get())
                {
                    if(m_HasToExist->Get())
                    {
                        auto const &path = m_Path->Get();
                        if(!std::filesystem::exists(path))
                        {
                            // Le dossier n'existant pas, on ne veut pas déclencher d'exceptions.
                            // Le chemin d'un vrai dossier devra d'abord être spécifié.
                            m_HasToExist->Set(false);
                        }
                    }
                }
            }

            if(action.first == Item::Key::Obs::ms_AddItemEnd)
            {
                SetVarFromString(m_Size->GetKey(), GetDirSize(m_Path));
            }
        }
    }
    //*/

    void Folder::EndObservation(const ObservablePtr &observable)
    {
        // La fin d'une observation indique que le dossier a changé de taille.
        SetVarFromString(m_Size->GetKey(), GetDirSize(m_Path));
    }

    bool Folder::IsValid(const std::wstring &value)
    {
        std::filesystem::path const path{value};
        return std::filesystem::is_directory(path) && IsReadable(path) && IsWritable(path);
    }

    void Folder::AssertValid(const std::filesystem::path &value)
    {
        DirAssertValid(value);
    }

    void Folder::Normalize(std::filesystem::path &out_path)
    {
        ma::Normalize(out_path);
    }

    Resources Folder::GetResources(Item::SearchMod search_mod) const
    {
        return Find<Resource::MatchResource>::GetHashMap(GetKey(), Resource::GetMatchResource(), search_mod);
    }

    std::optional<ResourcePtr> Folder::HasResource(const std::filesystem::path &path,
                                                           Item::SearchMod search_mod) const
    {
        const auto resources =
            Find<Resource::MatchResource>::GetHashMap(GetKey(), Resource::GetMatchResource(), search_mod);

        const auto res_path = GetResFileName(path.wstring());
        const auto &it_find = resources.find(res_path);

        if(it_find != resources.end())
            return it_find->second;
        else
            return {};
    }

    bool Folder::HasSubFolder(const std::wstring &folder_name)
    {
        const auto path = m_Path->Get() / folder_name;
        return HasSubFolder(path);
    }

    bool Folder::HasSubFolder(const std::filesystem::path &path)
    {
        auto normalize_path = path;

        AssertValid(normalize_path);
        Normalize(normalize_path);

        auto children = Find<MatchFolder>::GetHashMap(GetKey(), GetMatchFolder(), Item::SearchMod::RECURSIVE);
        return !children.empty() && children.find(normalize_path) != children.end();
    }

    FolderPtr Folder::AddSubFolder(const std::wstring &folder_name)
    {
        const std::filesystem::path path{folder_name};
        return AddSubFolder(path);
    }

    FolderPtr Folder::AddSubFolder(const std::filesystem::path &path)
    {
        auto children = Find<MatchFolder>::GetHashMap(GetKey(), GetMatchFolder(), Item::SearchMod::RECURSIVE);

        const std::wstring str_path = path.wstring();

        MA_ASSERT(children.find(path) == children.end(),
                  L"Le sous dossier existe déjà: «" + str_path + L"».",
                  std::invalid_argument);

        return Item::CreateItem<Folder>({GetKey()}, str_path, m_Recursive->Get(), true);
    }

    bool Folder::AcceptToAddChild(const ItemPtr &item) const
    {
        bool result = Item::AcceptToAddChild(item);

        if(result)
        {
            const bool is_inherited_from_folder =
                ClassInfoManager::IsInherit(item, Folder::GetFolderClassName());
            const bool is_inherited_from_resource =
                ClassInfoManager::IsInherit(item, Resource::GetResourceClassName());

            if(is_inherited_from_folder)
                result = !Item::HasItem(item->GetKey(), Item::SearchMod::RECURSIVE);

            if(is_inherited_from_resource)
                result = !Item::HasItem(item->GetKey(), Item::SearchMod::LOCAL);
        }

        return result;
    }

    ItemPtr Folder::CreateChildFromSave(const std::filesystem::directory_entry &dir_entry)
    {
        const auto key = GetKeyFromFolderSavePath(dir_entry);
        const auto class_name = GetTypeFromSave(dir_entry);
        const auto vars_from_save = GetVarsFromSave(dir_entry);
        const auto it_find = vars_from_save.find(Item::Key::Var::GetName());
        const auto name = it_find != vars_from_save.end() ? std::get<1u>(it_find->second) : L""s;

        const auto item_header = toString(std::make_tuple(key, class_name, name));

        MA_ASSERT(
            !HasItem(key),
            L"L'item existe déjà. Cette fonction est destinée à créer un item enfant de cette instance dans le cas "
            L"où le type n'est pas géré par la RTTI." +
                item_header,
            std::invalid_argument);

        ItemPtr result;

        // Seulement les ressources du type « Resource » seront gérées.
        // Les dossiers peuvent être créés via la « RTTI » cette fonction ne sera donc pas appelée.
        if(class_name == Resource::GetResourceClassName())
        {
            auto resources = Find<Resource::MatchResource>::GetHashMap(
                GetKey(), Resource::GetMatchResource(), Item::SearchMod::LOCAL);

            const auto it_find_res = vars_from_save.find(Resource::ms_KeyPath);
            if(it_find != vars_from_save.end())
            {
                const auto path_str = std::get<1u>(it_find_res->second);

                auto sub_path = GetResFileName(path_str);
                // sub_filename.DontFollowLink(); // Ne fonctionne pas du tout.
                // Du coup, j'ajoute à la condition suivante : si le fichier existe.
                // Je ne peux pas tester si c'est un raccourci ou pas avec wxWidgets.
                // Alors, je vérifie si le fichier ou le fichier pointé existe.
                // Cela évitera d'ajouter des raccourcis qui pointent sur rien et qui provoque des erreurs.

                // Nous sommes en plein chargement des items.
                // Il peut localement déjà exister un item représentant la même ressource.
                // Si c'est le cas la ressource existante est supprimée.
                auto it_find_sub = resources.find(sub_path);
                if(it_find_sub != resources.end())
                {
                    RemoveChild(it_find_sub->second->GetKey());
                }

                // if(sub_filename.IsFileReadable())
                if(std::filesystem::exists(sub_path))
                {
                    Normalize(sub_path);
                    result = Item::CreateItem<Resource>({GetKey(), true, true, key}, sub_path);
                }
            }
            else
            {
                MA_WARNING(
                    false,
                    L"La variable " + Folder::Key::Var::GetPath() +
                        L" du chemin de la ressource n'a peu être identifiée. Le fichier item.cpp de la sauvegarde "
                        L"de est mal formé. " +
                        item_header);
            }
        }
        else
        {
            MA_ASSERT(false, L"Ce type" + class_name + L"n'est géré par cette fonction.");
        }

        return result;
    }

    void Folder::OnEnable()
    {
        Observer::OnEnable();
        Item::OnEnable();
    }

    void Folder::OnDisable()
    {
        Observer::OnDisable();
        Item::OnDisable();
    }

    const TypeInfo &Folder::GetFolderTypeInfo()
    {
        // clang-format off
            static const TypeInfo info =
            {
                GetFolderClassName(),
                L"La classe Folder permet d'associer un dossier à un Item.",
                {
                    {
                        TypeInfo::Key::GetWhiteListChildrenData(),
                        {
                            Folder::GetFolderClassName(), Resource::GetResourceClassName()
                        }
                    },
                    {
                        TypeInfo::Key::GetWhiteListParentsData(),
                        {
                            Folder::GetFolderClassName(), ResourceManager::GetResourceManagerClassName()
                        }
                    }
                }
            };

            return info;
        // clang-format on
    }
    M_CPP_CLASSHIERARCHY(Folder, Item, Observer)
    M_CPP_ITEM_TYPE_VAR(Folder)

    // FileWatcher //———————————————————————————————————————————————————————————————————————————————————————————————————————

    FileWatcher::FileWatcher(const Item::ConstructorParameters &in_params):
    Item(in_params)
    {}

    FileWatcher::~FileWatcher() = default;

    void FileWatcher::EndConstruction()
    {
        Item::EndConstruction();
    }

    void FileWatcher::BeginObservation(const ObservablePtr &observable)
    {}

    void FileWatcher::UpdateObservation(const ObservablePtr &observable, const Observable::Data &data)
    {}

    void FileWatcher::EndObservation(const ObservablePtr &)
    {}

    void FileWatcher::OnEnable()
    {}

    void FileWatcher::OnDisable()
    {}

    void FileWatcher::OnFileSystemEvent(const Observable::Data::key_type &event, const std::filesystem::path& path)
    {
        if((event == Key::Obs::ms_Delete) || (event == Key::Obs::ms_Rename) || (event == Key::Obs::ms_Created))
        {
            if(std::filesystem::is_directory(path))
            {
                auto folders = Find<Folder::MatchFolder>::GetHashMap(Item::Key::GetResourceManager(), Folder::GetMatchFolder(), Item::SearchMod::RECURSIVE);

                MA_ASSERT(!folders.empty(), L"Aucun dossier n'ai surveillé.", std::invalid_argument);

                if(event == Key::Obs::ms_Delete || event == Key::Obs::ms_Rename)
                {
                    auto it_find = folders.find(path);

                    MA_ASSERT(it_find != folders.end(),
                              L"Aucun dossier surveillé n'est associé à ce chemin : «" + path.wstring() + L"».",
                              std::invalid_argument);

                    FolderPtr folder = it_find->second;

                    if(event == Key::Obs::ms_Delete)
                        Item::EraseItem(folder->GetKey());
                    else if(event == Key::Obs::ms_Rename)
                        folder->m_Path->Set(path);
                }
                else if(event == Key::Obs::ms_Created)
                {
                    auto it_find_parent = folders.find(path);

                    MA_ASSERT(it_find_parent != folders.end(),
                              L"Aucun dossier surveillé n'est associé à ce chemin: «" + path.wstring() + L"».",
                              std::invalid_argument);

                    FolderPtr parent_folder = it_find_parent->second;
                    parent_folder->AddSubFolder(path);
                }
            }
            else
            {
                auto resources = Find<Resource::MatchResource>::GetHashMap(
                    Item::Key::GetResourceManager(), Resource::GetMatchResource(), Item::SearchMod::RECURSIVE);

                if(event == Key::Obs::ms_Delete || event == Key::Obs::ms_Rename)
                {
                    auto it_find = resources.find(path);

                    MA_ASSERT(it_find != resources.end(),
                              L"Aucun ressources surveillée n'est associée à ce chemin: «" + path.wstring() + L"».",
                              std::invalid_argument);

                    ResourcePtr resource = it_find->second;

                    if(event == Key::Obs::ms_Delete)
                        Item::EraseItem(resource->GetKey());
                    else if(event == Key::Obs::ms_Rename)
                        resource->m_Path->Set(path);
                }
                else if(event == Key::Obs::ms_Created)
                {
                    auto folders = Find<Folder::MatchFolder>::GetHashMap(
                        Item::Key::GetResourceManager(), Folder::GetMatchFolder(), Item::SearchMod::RECURSIVE);

                    MA_ASSERT(!folders.empty(), L"Aucun dossier n'ai surveillé.", std::invalid_argument);

                    auto it_find_parent = folders.find(path);

                    MA_ASSERT(it_find_parent != folders.end(),
                              L"Aucun dossier surveillé n'est associé à ce chemin : «" + path.wstring() + L"».",
                              std::invalid_argument);

                    FolderPtr folder = it_find_parent->second;
                    Item::CreateItem<Resource>({folder->GetKey()}, path);
                }
            }
        }

    }

    // ResourceManager //———————————————————————————————————————————————————————————————————————————————————————————————————

    const Var::key_type ResourceManager::ms_KeyResources(L"ms_KeyResources");

    ResourceManager::ResourceManager(const Item::ConstructorParameters &in_params):
    Item({in_params, Key::GetResourceManager(), true, false}),
    m_Resources(AddReadOnlyMemberVar<StringHashSetVar>(ms_KeyResources))
    {}

    ResourceManager::~ResourceManager() = default;

    void ResourceManager::EndConstruction()
    {
        Item::EndConstruction();

        if(Item::HasItem(Key::GetObservationManager()))
        {
            auto observer_manager = Item::GetItem<ObservationManager>(Key::GetObservationManager());

            auto resource_manager_item = Item::GetItem(GetKey());
            // Permet de savoir quand l'un de ses enfants a fini d'être construit et ainsi qu'il soit observable.
            // Ce qui permet de l'ajouter et de l'observer.
            observer_manager->AddObservation<ItemObserverVar, ItemVar>(this, resource_manager_item);
        }
    }

    FolderPtr ResourceManager::AddFolder(const std::wstring &value, bool recursive)
    {
        if(auto folder_opt = HasFolderOpt(value); !folder_opt.has_value())
        {
            std::filesystem::path path{value};

            Normalize(path);

            return Item::CreateItem<Folder>({GetKey()}, path.wstring(), recursive);
        }
        else
            return folder_opt.value();
    }

    FolderPtr ResourceManager::AddFolder(const std::filesystem::directory_entry &dir_entry, bool recursive)
    {
        auto path = dir_entry.path();
        if(auto folder_opt = HasFolderOpt(path.wstring()); !folder_opt.has_value())
        {
            Normalize(path);

            return Item::CreateItem<Folder>({GetKey()}, path.wstring(), recursive);
        }
        else
        {
            return folder_opt.value();
        }
    }

    void ResourceManager::RemoveFolder(const std::wstring &value)
    {
        const std::filesystem::path path{value};

        DirAssertValid(path);

        auto children = Find<Folder::MatchFolder>::GetHashMap(
            Key::GetResourceManager(), Folder::GetMatchFolder(), Item::SearchMod::RECURSIVE);

        MA_ASSERT(!children.empty(), L"Aucun dossier n'ai surveiller.", std::invalid_argument);

        auto it_find = children.find(path);

        MA_ASSERT(
            it_find != children.end(), L"Aucun dossier surveiller n'est associé à ce chemin.", std::invalid_argument);

        Item::EraseItem(it_find->second->GetKey());
    }

    bool ResourceManager::HasFolder(const std::wstring &value)
    {
        const std::filesystem::path path{value};
        DirAssertValid(path);

        auto children = Find<Folder::MatchFolder>::GetHashMap(
            Key::GetResourceManager(), Folder::GetMatchFolder(), Item::SearchMod::RECURSIVE);

        return !children.empty() && children.find(path) != children.end();
    }

    std::optional<FolderPtr> ResourceManager::HasFolderOpt(const std::wstring &value)
    {
        // Ne fonctionne pas
        // path   L"/home/gandi/travail/Ma/code/.build/clion_gcc_debug/modules/ma/module_sample_cpp"
        // le dossier est déjà présent mais le find ne fonctionne pas.
        const std::filesystem::path path{value};
        DirAssertValid(path);

        auto children = Find<Folder::MatchFolder>::GetHashMap(
            Key::GetResourceManager(), Folder::GetMatchFolder(), Item::SearchMod::RECURSIVE);

        if(!children.empty())
        {
            if(auto it_find = children.find(path); it_find != children.end())
                return {it_find->second};
        }

        return {};
    }

    FolderPtr ResourceManager::GetFolder(const std::wstring &value)
    {
        const std::filesystem::path path{value};
        DirAssertValid(path);

        auto children = Find<Folder::MatchFolder>::GetHashMap(
            Key::GetResourceManager(), Folder::GetMatchFolder(), Item::SearchMod::RECURSIVE);

        MA_ASSERT(!children.empty(), L"Aucun dossier n'ai surveiller.", std::invalid_argument);

        auto it_find = children.find(path);

        MA_ASSERT(
            it_find != children.end(), L"Aucun dossier surveiller n'est associé à ce chemin.", std::invalid_argument);

        return it_find->second;
    }

    void ResourceManager::BeginObservation(const ObservablePtr &observable)
    {
        if(ClassInfoManager::IsInherit(observable, Folder::GetFolderClassName()))
        {
            FolderPtr folder = std::dynamic_pointer_cast<Folder>(observable);

            MA_ASSERT(folder, L"Impossible de convertir l'observable en dossier.", std::runtime_error);

//            if(m_FileSystemWatcherVar)
//            {
//                auto path = folder->m_Path->Get();
//                m_FileSystemWatcherVar->Get()->Add(path);
//            }
        }
    }

    void ResourceManager::UpdateObservation(const ObservablePtr &observable, const Observable::Data &data)
    {
        if(data.HasActions(Key::Obs::ms_AddItemEnd))
        {
            if(observable->IsObservable() &&
               ClassInfoManager::IsInherit(observable, Folder::GetFolderClassName()))
            {
                FolderPtr folder = std::dynamic_pointer_cast<Folder>(observable);

                MA_ASSERT(folder, L"Impossible de convertir l'observable en dossier.", std::runtime_error);

                auto observer_manager = Item::GetItem<ObservationManager>(Key::GetObservationManager());
                observer_manager->AddObservation<ItemObserverVar, ItemVar>(this, folder);
            }
        }
    }

    void ResourceManager::EndObservation(const ObservablePtr &observable)
    {
        if(ClassInfoManager::IsInherit(observable, Folder::GetFolderClassName()))
        {
            FolderPtr folder = std::dynamic_pointer_cast<Folder>(observable);

            MA_ASSERT(folder, L"Impossible de convertir l'observable en dossier.", std::runtime_error);

//            if(m_FileSystemWatcherVar)
//            {
//                auto path = folder->m_Path->Get();
//                m_FileSystemWatcherVar->Get()->Remove(path);
//            }
        }
    }

    void ResourceManager::AddChild(ItemPtr item)
    {
        Item::AddChild(item);

        if(item->IsObservable() && ClassInfoManager::IsInherit(item, Folder::GetFolderClassName()))
        {
            auto observer_manager = Item::GetItem<ObservationManager>(Key::GetObservationManager());
            observer_manager->AddObservation<ItemObserverVar, ItemVar>(this, item);
        }
    }

    bool ResourceManager::AcceptToAddChild(const ItemPtr &item) const
    {
        bool result = Item::AcceptToAddChild(item);

        if(result && ClassInfoManager::IsInherit(item, Folder::GetFolderClassName()))
            result = !Item::HasItem(item->GetKey(), Item::SearchMod::RECURSIVE);

        return result;
    }

    void ResourceManager::OnEnable()
    {
        Observer::OnEnable();
        Item::OnEnable();
    }

    void ResourceManager::OnDisable()
    {
        Observer::OnDisable();
        Item::OnDisable();
    }

    // ItemPtr ResourceManager::GetItemFromFolderPath(const std::filesystem::directory_entry& dir_entry) const
    //{
    //    ItemPtr result;
    //    auto const vars_from_save = GetVarsFromSave(dir_entry);
    //
    //    const auto it_find = vars_from_save.find(Folder::GetPath());
    //    if(it_find != vars_from_save.end())
    //    {
    //        const auto path_str = std::get<1u>(it_find->second);
    //        const wxFileName path_wx = GetDirFileName(path_str);
    //
    //        typedef MatchVar<DirectoryVar, Folder> MatchFolder;
    //        MatchFolder matcher({Folder::GetPath()});
    //        const auto folders = Find<MatchFolder>::GetHashMap(GetKey(), matcher, Item::SearchMod::LOCAL);
    //        const auto it_find_folder = folders.find(path_wx);
    //        if(it_find_folder != folders.end())
    //        {
    //            result = it_find_folder->second;
    //        }
    //    }
    //
    //    // Au cas où. Je ne pense pas que cela sera utile.
    //    if(!result)
    //        result = Item::GetItemFromFolderPath(dir_entry);
    //
    //    return result;
    //}
    //
    // Item::key_type ResourceManager::GetKeyFromFolderSavePath(const std::filesystem::directory_entry& dir_entry) const
    //{
    //    // La clef n'est pas écrite dans le nom du dossier mais seulement dans le
    //    // fichier de sauvegarde de l'item: item.cpp.
    //    return this->GetKeyFromSave(dir_entry);
    //}
    //
    // std::wstring ResourceManager::GetClassNameFromFolderSavePath(const std::filesystem::directory_entry& dir_entry) const
    //{
    //    const auto folder_name = dir_entry.path().stem().wstring();
    //    const auto type_name = Split(folder_name, L'_');
    //
    //    if(type_name.size())
    //        return type_name[0];
    //    else
    //        return L"";
    //}
    //
    // std::wstring ResourceManager::GetNameFromFolderSavePath(const std::filesystem::directory_entry& dir_entry) const
    //{
    //    const auto folder_name = dir_entry.path().stem().wstring();
    //    const auto type_name = Split(folder_name, L'_');
    //
    //    if(type_name.size() > 1u)
    //        return type_name[1u];
    //    else
    //        return L"";
    //}

    const TypeInfo &ResourceManager::GetResourceManagerTypeInfo()
    {
        // clang-format off
            static const TypeInfo info =
            {
                GetResourceManagerClassName(),
                L"La classe ResourceManager administre la gestion des ressources.",
                {{
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        Folder::GetFolderClassName(), Resource::GetResourceClassName()
                    }
                }}
            };
        // clang-format on
        return info;
    }

    M_CPP_CLASSHIERARCHY(ResourceManager, Item, Observer)

    // ResourceVar //———————————————————————————————————————————————————————————————————————————————————————————————————
    const TypeInfo &ResourceVar::GetResourceVarTypeInfo()
    {
        static const TypeInfo info = {GetResourceVarClassName(),
                                      L"Permet de faire référence à une ressource qui ne soit pas enfant de l'item "
                                      L"possédant la variable « ResourceVar ».",
                                      {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(ResourceVar, ObservableVar)

    // ResourceVectorVar //—————————————————————————————————————————————————————————————————————————————————————————————
    const TypeInfo &ResourceVectorVar::GetResourceVectorVarTypeInfo()
    {
        static const TypeInfo info = {
            GetResourceVectorVarClassName(),
            L"Cette variable représente un conteneur d'éléments ordonné et non associatif. "
            L"Concrètement c'est un std::vector<ma::ResourcePtr>.",
            {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(ResourceVectorVar, VectorVar)

    // FolderVectorVar //———————————————————————————————————————————————————————————————————————————————————————————————
    const TypeInfo &FolderVectorVar::GetFolderVectorVarTypeInfo()
    {
        static const TypeInfo info = {
            GetFolderVectorVarClassName(),
            L"Cette variable représente un conteneur d'éléments ordonné et non associatif. "
            L"Concrètement c'est un std::vector<ma::FolderPtr>.",
            {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(FolderVectorVar, VectorVar)
} // namespace ma
