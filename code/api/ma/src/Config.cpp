/// \file Config.cpp
/// \author Pontier Pierre
/// \date 2023-03-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <fstream>

#include "ma/Config.h"
#include "ma/Error.h"
#include "ma/String.h"
#include "ma/Algorithm.h"

namespace ma
{
    // Config //————————————————————————————————————————————————————————————————————————————————————————————————————————
    Config::Config(std::filesystem::path path): m_FilePath(std::move(path)), m_Datas()
    {
        ma::SetGlobalLocal();
    }

    Config::~Config() = default;

    void Config::Load()
    {
        if(!std::filesystem::exists(m_FilePath))
        {
            MA_WARNING(false, L"Le fichier de configuration n'existe pas: « " + m_FilePath.wstring() + L" ».");
            return;
        }

        if(std::filesystem::is_directory(m_FilePath))
        {
            MA_WARNING(false,
                       L"Le fichier de configuration « " + m_FilePath.wstring() +
                           L" » n'est pas un fichier mais un dossier.");
            return;
        }

        std::wstringstream input;
        if(std::wifstream file{m_FilePath})
        {
            input << file.rdbuf();
        } // permet de fermer le flux du fichier au plus vite.

        if(!input.str().empty())
        {
            m_Datas.clear();

            std::wstring current_group;
            for(std::wstring line; std::getline(input, line);)
            {
                const auto trim_line = ma::TrimStartEnd(line);

                if(trim_line.size() > 2u && trim_line[0] == L'[' && trim_line[trim_line.size() - 1u] == L']')
                {
                    current_group = ma::TrimStartEnd(trim_line.substr(1u, trim_line.size() - 2u));
                    m_Datas[current_group] = {};
                }
                else if(!current_group.empty())
                {
                    const auto find_equal = trim_line.find(L'=');

                    if(trim_line.size() > 3u && find_equal != std::wstring::npos && find_equal > 1u &&
                       find_equal < trim_line.size() - 1u)
                    {
                        const auto key = ma::TrimStartEnd(line.substr(0u, find_equal));
                        const auto value =
                            ma::TrimStartEnd(line.substr(find_equal + 1u)); // , trim_line.size() - find_equal - 1u
                        m_Datas[current_group][key] = value;
                    }
                }
            }
        }
        else
        {
            MA_WARNING(false, L"Le fichier « " + m_FilePath.wstring() + L" » n'a peu être ouvert en lecture.");
        }
    }

    void Config::Save()
    {
        std::wstringstream ss;

        for(const auto &data : m_Datas)
        {
            // Nom du groupe
            ss << L'[' << data.first << L"]\n";

            // Ensemble de clefs et de valeurs du groupe.
            for(auto &&[key, value] : data.second)
            {
                ss << key << L'=' << value << L'\n';
            }

            ss << std::endl;
        }

        if(std::wofstream out_file{m_FilePath})
            out_file << ss.str();
        else
            MA_WARNING(false, L"Le fichier « " + m_FilePath.wstring() + L" » n'a peu être ouvert en écriture.");
    }

    // ConfigGroup //———————————————————————————————————————————————————————————————————————————————————————————————————
    // ConfigGroup::ConfigGroup(const ma::Item::ConstructorParameters& params):
    // Item(params),
    // m_VarFromSuperClass(),
    // m_Name(AddReadOnlyMemberVar<ma::StringVar>(ma::Item::ms_KeyNameVar, L""))
    // {}

    ConfigGroup::ConfigGroup(const Item::ConstructorParameters &params, const std::wstring &name):
    Item(params),
    m_VarFromSuperClass(),
    // Si le nom est vide alors, il faut laisser la possibilité à l'utilisateur de le modifier.
    // Si le nom est vide cela veut s'en doute dire que le ConfigGroup a été créé dynamiquement.
    m_Name(name.empty() ? AddMemberVar<ma::StringVar>(Key::Var::GetName()) :
                          AddReadOnlyMemberVar<ma::StringVar>(Key::Var::GetName(), name))
    {}

    ConfigGroup::~ConfigGroup() = default;

    void ConfigGroup::LoadConfig()
    {
        const auto name = m_Name->toString();

        if(!name.empty())
            if(auto parent = GetParentOpt<ConfigItem>())
            {
                auto &datas = parent.value()->m_Config.m_Datas;
                auto it_find_group = datas.find(name);
                if(it_find_group != datas.end())
                {
                    for(auto &&[key, value] : it_find_group->second)
                    {
                        if(HasVar(key))
                        {
                            SetVarFromString(key, value);
                        }
                    }
                }
            }
    }

    void ConfigGroup::LoadVars()
    {
        Item::LoadVars();

        LoadConfig();
    }

    const TypeInfo &ConfigGroup::GetConfigGroupTypeInfo()
    {
        // clang-format off
        static const TypeInfo info = {GetConfigGroupClassName(),
                                      L"La classe ConfigGroup permet de définir un groupe dans un fichier de "
                                      L"configuration.",
                                      {
                                          {TypeInfo::Key::GetWhiteListParentsData(), {ma::ConfigItem::GetConfigItemClassName()}}
                                      }};

        // clang-format on
        return info;
    }
    M_CPP_CLASSHIERARCHY(ConfigGroup, Item)
    M_CPP_MAKERITEM_TYPE(ConfigGroup)
    M_CPP_ITEM_TYPE_VAR(ConfigGroup)

    // ConfigItem //————————————————————————————————————————————————————————————————————————————————————————————————————
    ConfigItem::ConfigItem(const ma::Item::ConstructorParameters &params, const std::filesystem::path &path):
    Item(params),
    m_Config(path),
    m_ConfigFilePath(AddMemberVar<FilePathVar>(L"m_ConfigFilePath", path.wstring()))
    {}

    ConfigItem::~ConfigItem() = default;

    const Config &ConfigItem::GetConfig()
    {
        return m_Config;
    }

    std::optional<ma::ConfigGroupPtr> ConfigItem::GetGroup(const std::wstring &group_name)
    {
        typedef ma::MatchVar<ma::StringVar, ma::ConfigGroup> MatchGroup;

        MatchGroup matcher({Key::Var::GetName()});

        auto groups = ma::Find<MatchGroup>::GetHashMap(GetKey(), matcher, ma::Item::SearchMod::LOCAL);

        auto it_find = groups.find(group_name);

        if(it_find != groups.end())
            return it_find->second;
        else
            return {};
    }

    Item::key_errors ConfigItem::LoadItems()
    {
        LoadConfig();

        return Item::LoadItems();
    }

    void ConfigItem::LoadConfig()
    {
        m_Config.m_FilePath = m_ConfigFilePath->toString();

        if(!m_Config.m_FilePath.empty() && std::filesystem::exists(m_Config.m_FilePath))
            m_Config.Load();
    }

    void ConfigItem::Save()
    {
        Item::Save();

        m_Config.m_FilePath = m_ConfigFilePath->toString();

        if(!m_Config.m_FilePath.empty() && m_ConfigFilePath)
        {
            typedef ma::MatchVar<ma::StringVar, ma::ConfigGroup> MatchGroup;
            MatchGroup matcher({Key::Var::GetName()});

            const auto groups = ma::Find<MatchGroup>::GetHashMap(GetKey(), matcher, ma::Item::SearchMod::LOCAL);

            auto &datas = m_Config.m_Datas;
            datas.clear();

            for(auto &&[name, group] : groups)
            {
                for(auto &&[key, var] : group->GetVars())
                {
                    if(var != group->m_SavePath && var != group->m_LastSavePath && var != group->m_CustomSavePath &&
                       key != Key::Var::GetName())
                    {
                        datas[name][key] = var->toString();
                    }
                }
            }

            m_Config.Save();
        }
    }

    const TypeInfo &ConfigItem::GetConfigItemTypeInfo()
    {
        // clang-format off
        static const TypeInfo info = {GetConfigItemClassName(),
                      L"La classe ConfigItem permet de définir lier un fichier de configuration à un "
                      L"ensemble d'Items.",
                      {
                          {TypeInfo::Key::GetWhiteListChildrenData(), {ma::ConfigGroup::GetConfigGroupClassName()}}
                      }};
        // clang-format on
        return info;
    }

    M_CPP_CLASSHIERARCHY(ConfigItem, Item)
    M_CPP_MAKERITEM_TYPE(ConfigItem)
    M_CPP_ITEM_TYPE_VAR(ConfigItem)

} // namespace ma
