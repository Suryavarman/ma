/// \file Serialization.ut.cpp
/// \author Pontier Pierre
/// \date 2021-21-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires du fichier Serialization.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"
#include <sstream>
#include <map>
#include <list>
#include <array>
#include <forward_list>
#include <set>

void serialization_setup()
{
    ma::g_ConsoleVerbose = 1u;
    ma::Item::CreateRoot<ma::Item>();
}

void serialization_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(Serialization, node_empty)
{
    std::wstring empty_str{L""};
    const auto empty_str_ansi = ma::to_string(empty_str);
    const char* empty_chars = empty_str_ansi.c_str();

    cr_assert_none_throw(ma::Serialization::Parser{empty_str});
    cr_assert_none_throw(ma::Serialization::Parser{L""});

    const ma::Serialization::Parser parser{empty_str};
    cr_assert_not(parser.HasError());
    auto root = parser.GetRoot();

    cr_assert_eq(root->m_Type, ma::Serialization::Node::Type::Value);
    cr_assert_eq(root->m_Index, 0);

    cr_assert_eq(root->m_String, empty_str,
                 "(root->m_String = %s) != (empty_str = %s)",
                 ma::to_string(std::wstring(root->m_String)).c_str(),
                 empty_chars);

    cr_assert_eq(root->m_Nodes.size(), 0);
}

#define test_node_number(number) \
{ \
    std::wstring number_str{number}; \
    std::wstring number_result_str{number}; \
    const auto number_str_ansi = ma::to_string(number); \
    const char* number_result_str_chars = number_str_ansi.c_str(); \
 \
    const ma::Serialization::Parser parser{number_str}; \
    cr_assert_not(parser.HasError()); \
    auto root = parser.GetRoot(); \
 \
    cr_assert_eq(root->m_Type, ma::Serialization::Node::Type::Value); \
    cr_assert_eq(root->m_Index, 0); \
 \
    cr_assert_eq(root->m_String, number_result_str, \
                 "(root->m_String = %s) != (number_result_str = %s)", \
                 ma::to_string(std::wstring(root->m_String)).c_str(), \
                 number_result_str_chars); \
 \
    cr_assert_eq(root->m_Nodes.size(), 0); \
}

#define test_node_number_series(number) \
    test_node_number(L"number"); \
    test_node_number(L" number"); \
    test_node_number(L"number "); \
    test_node_number(L" number "); \
    test_node_number(L"  number  ");

Test(Serialization, node_number)
{
    {
        std::wstring number_str{L"1"};
        cr_assert_none_throw(ma::Serialization::Parser{number_str});
        cr_assert_none_throw(ma::Serialization::Parser{L"1"});
    }

    test_node_number_series(1)
    test_node_number_series(1.)
    test_node_number_series(1.0)
    test_node_number_series(-1)
    test_node_number_series(-1.)
    test_node_number_series(-1.0)
    test_node_number_series(2)
    test_node_number_series(2.)
    test_node_number_series(2.0)
    test_node_number_series(-2)
    test_node_number_series(-2.)
    test_node_number_series(-2.0)

    test_node_number_series(999)
    test_node_number_series(999.999)
    test_node_number_series(-999)
    test_node_number_series(-999.999)
    test_node_number_series(9999999)
    test_node_number_series(9999999.999)
    test_node_number_series(-9999999)
    test_node_number_series(-9999999.999)
}

#define test_node_string(str) \
{ \
    const auto str_ansi = ma::to_string(str); \
    const char* str_chars = str_ansi.c_str(); \
 \
    const ma::Serialization::Parser parser{str}; \
    cr_assert_not(parser.HasError()); \
    auto root = parser.GetRoot(); \
 \
    cr_assert_eq(root->m_Type, ma::Serialization::Node::Type::Value); \
    cr_assert_eq(root->m_Index, 0); \
 \
    cr_assert_eq(root->m_String, str, \
                 "(root->m_String = %s) != (str = %s)", \
                 ma::to_string(std::wstring(root->m_String)).c_str(), \
                 str_chars); \
 \
    cr_assert_eq(root->m_Nodes.size(), 0); \
}

Test(Serialization, node_string)
{
    {
        std::wstring string_str{L"toto"};
        cr_assert_none_throw(ma::Serialization::Parser{string_str});
        cr_assert_none_throw(ma::Serialization::Parser{L"toto"});
    }

    test_node_string(L"")
    test_node_string(L"toto")
    test_node_string(L"    toto")
    test_node_string(L"    toto      ")
    test_node_string(L"    to        to      ")
    test_node_string(L"    to        to      ")
    test_node_string(LR"#("toto")#")
}

const char* GetTypeChar(ma::Serialization::Node::Type type)
{
    switch(type)
    {
        case ma::Serialization::Node::Type::Struct:
            return "Structure";
            break;

        case ma::Serialization::Node::Type::Value:
            return "Value";
            break;

        default:
            return "Invalid";
            break;
    }
}

void CompareNodes(ma::Serialization::NodePtr node_auto, ma::Serialization::NodePtr node_manual, std::deque<size_t> depth)
{
    std::string depth_str = "[";

    for(auto it = depth.begin(); it != depth.end(); ++it)
        depth_str += "/" + ma::toString(*it);

    depth_str += "]";

    const char* depth_chars = depth_str.c_str();

    cr_assert_eq(node_auto->m_Type, node_manual->m_Type,
                 R"#(%s (node_auto->m_Type = «%s») != (node_manual->m_Type = «%s»))#",
                 depth_chars,
                 GetTypeChar(node_auto->m_Type),
                 GetTypeChar(node_manual->m_Type));

    cr_assert_eq(node_auto->m_Index, node_manual->m_Index,
                 "%s (node_auto->m_Index = %zu) != (node_manual->m_Index = %zu)",
                 depth_chars,
                 node_auto->m_Index,
                 node_manual->m_Index);

    cr_assert_eq(node_auto->m_String, node_manual->m_String,
                 R"#(%s (node_auto->m_String = «%s») != (node_manual.m_String = «%s»))#",
                 depth_chars,
                 ma::to_string(std::wstring(node_auto->m_String)).c_str(),
                 ma::to_string(std::wstring(node_manual->m_String)).c_str());

    cr_assert_eq(node_auto->m_Nodes.size(), node_manual->m_Nodes.size(),
                 "%s (node_auto->m_Nodes.size() = %zu) != (node_manual->m_Nodes.size() = %zu)",
                 depth_chars,
                 node_auto->m_Nodes.size(),
                 node_manual->m_Nodes.size());

    size_t index = 0;
    auto it_auto = node_auto->m_Nodes.begin();
    for(auto it_manu = node_manual->m_Nodes.begin(); it_manu != node_manual->m_Nodes.end(); ++it_auto, ++it_manu, index++)
    {
        depth.push_back(index);
        CompareNodes(*it_auto, *it_manu, depth);
        depth.pop_back();
    }
}

void CompareRoots(const std::wstring &str, const ma::Serialization::Node &manual_root, bool has_errors = false)
{
    ma::Serialization::Parser parser{str};

    if(has_errors)
        cr_assert(parser.HasError());
    else
        cr_assert_not(parser.HasError());

    auto root_auto = parser.GetRoot();

    cr_assert_eq(root_auto->m_Type, manual_root.m_Type,
                 R"#([/0] (root_auto->m_Type = «%s») != (manual_root.m_Type = «%s»))#",
                 GetTypeChar(root_auto->m_Type),
                 GetTypeChar(manual_root.m_Type));

    cr_assert_eq(root_auto->m_Index, manual_root.m_Index,
                 R"#([/0] (root_auto->m_Index = %zu) != (manual_root.m_Index = %zu))#",
                 root_auto->m_Index,
                 manual_root.m_Index);

    cr_assert_eq(root_auto->m_String, manual_root.m_String,
                 R"#([/0] (root_auto->m_String = «%s») != (manual_root.m_String = «%s»))#",
                 ma::to_string(std::wstring(root_auto->m_String)).c_str(),
                 ma::to_string(std::wstring(manual_root.m_String)).c_str());

    cr_assert_eq(root_auto->m_Nodes.size(), manual_root.m_Nodes.size(),
                 R"#([/0] (root_auto->m_Nodes.size() = %zu) != (manual_root.m_Nodes.size() = %zu))#",
                 root_auto->m_Nodes.size(),
                 manual_root.m_Nodes.size());

    std::deque<size_t> depth = {0};
    size_t index = 0;
    auto it_auto = root_auto->m_Nodes.begin();
    for(auto it_manu = manual_root.m_Nodes.begin(); it_manu != manual_root.m_Nodes.end(); ++it_auto, ++it_manu, index++)
    {
        depth.push_back(index);
        CompareNodes(*it_auto, *it_manu, depth);
        depth.pop_back();
    }
}

#define MA_PARAMSTR(str_view, index_0, index_1) \
    index_0, str_view.substr(index_0, index_1 - index_0 + 1u)

using namespace ma::Serialization;

Test(Serialization, node_struct_empty_1)
{
    std::wstring str{L"{}"};
    cr_assert_none_throw(ma::Serialization::Parser{str});
    cr_assert_none_throw(ma::Serialization::Parser{L"{}"});
}

Test(Serialization, node_struct_empty_2)
{
    std::wstring str{L"{}"};
    //                 01

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0, 1u) // /0
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_empty_3)
{
    const std::wstring str{L" { } "};
    //                       01234

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 1, 3u) // /0
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_multi_struct_empty_1)
{
    const std::wstring str{L"{{}}"};
    //                       0123

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0, 3u), // /0
        {
            std::make_shared<Node>(Node::Type::Struct, MA_PARAMSTR(str_view, 1u, 2u)) // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_multi_struct_empty_2)
{
    const std::wstring str{L"{{}, {}}"};
    //                       01234567

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0, 7u), // /0
        {
            std::make_shared<Node>(Node::Type::Struct, MA_PARAMSTR(str_view, 1u, 2u)), // /0/0
            std::make_shared<Node>(Node::Type::Struct, MA_PARAMSTR(str_view, 5u, 6u))  // /0/1
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_multi_struct_empty_3)
{
    const std::wstring str{L"{{{}}}"};
    //                       012345

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0, 5u), // /0
        {
            std::make_shared<Node>(Node
            {
                Node::Type::Struct, MA_PARAMSTR(str_view, 1u, 4u), // /0/0
                {
                   std::make_shared<Node>(Node::Type::Struct, MA_PARAMSTR(str_view,  2u,  3u)) // /0/0/0
                }
            })
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_number_1)
{
    std::wstring str{L"{1}"};
    //                 013

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0, 3u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 1u, 1u)) // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_number_2)
{
    std::wstring str{L" { 1 } "};
    //                 0123456

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 1u, 5u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 3u, 3u)) // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_number_3)
{
    std::wstring str{L"{1,2,3,4}"};
    //                 012345678

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 8u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 1u, 1u)), // /0/0
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 3u, 3u)), // /0/1
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 5u, 5u)), // /0/2
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 7u, 7u))  // /0/4
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_number_4)
{
    std::wstring str{L"{1.1 + 3, 2.02 - 55, -0.3 / 8.0, 0.004 * 2.002}"};
    //                 01234567890123456789012345678901234567890123456
    //                 0         1         2         3         4

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 46u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view,  1u,  7u)), // /0/0
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 10u, 18u)), // /0/1
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 21u, 30u)), // /0/2
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 33u, 45u))  // /0/4
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_number_5)
{
    std::wstring str{L"{,,,}"};
    //                 01234

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 4u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, 1u, L""sv), // /0/0
            std::make_shared<Node>(Node::Type::Value, 2u, L""sv), // /0/1
            std::make_shared<Node>(Node::Type::Value, 3u, L""sv), // /0/2
            std::make_shared<Node>(Node::Type::Value, 4u, L""sv)  // /0/4
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_string_1)
{
    std::wstring str{LR"#({"toto_1"})#"};
    //                    0123456789

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 9u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 2u, 7u)), // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_string_2)
{
    std::wstring str{LR"({" "})"};
    //                   01234

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 4u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 2u, 2u)), // /0/0
        }
    };

    CompareRoots(str, manual_root);
}


Test(Serialization, node_struct_string_3)
{
    std::wstring str{LR"({""})"};
    //                   0123

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 3u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, 2u, L""sv), // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_string_4)
{
    std::wstring str{LR"#({"toto_1", "toto_2", "toto_3", "toto_4"})#"};
    //                    0123456789012345678901234567890123456789
    //                    0         1         2         3

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 39u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view,  2u,  7u)), // /0/0
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 12u, 17u)), // /0/1
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 22u, 27u)), // /0/2
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 32u, 37u))  // /0/4
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_string_5)
{
    std::wstring str{LR"#({"", "", "", ""})#"};
    //                    0123456789012345
    //                    0         1

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 15u), // /0
        {
            std::make_shared<Node>(Node::Type::Value,  2u, L""sv), // /0/0
            std::make_shared<Node>(Node::Type::Value,  6u, L""sv), // /0/1
            std::make_shared<Node>(Node::Type::Value, 10u, L""sv), // /0/2
            std::make_shared<Node>(Node::Type::Value, 14u, L""sv)  // /0/4
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_complex_1)
{
    //                            /0/0          /0/1             /0/2                 /0/2/2
    const std::wstring str{LR"#( {{1, 2, 3, 4}, {5, 6, 7, 8, 9}, {"toto_1", "toto_2", {10.00, 11 + 1500, - 0.12, 2.0*13  }}} )#"};
    //                          012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
    //                          0         1         2         3         4         5         6         7         8         9

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 1u, 91u), // racine /0
        {
            std::make_shared<Node>(Node
            {
                Node::Type::Struct, MA_PARAMSTR(str_view, 2u, 13u), // nœud 1 /0/0
                {
                   std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view,  3u,  3u)), // 1 /0/0/0
                   std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view,  6u,  6u)), // 2 /0/0/1
                   std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view,  9u,  9u)), // 3 /0/0/2
                   std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 12u, 12u))  // 4 /0/0/3
                }
            }),
            std::make_shared<Node>(Node
            {
                Node::Type::Struct, MA_PARAMSTR(str_view, 16u, 30u),// nœud 2 /0/1
                {
                    std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 17u, 17u)), // 5 /0/1/0
                    std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 20u, 20u)), // 6 /0/1/1
                    std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 23u, 23u)), // 7 /0/1/2
                    std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 26u, 26u)), // 8 /0/1/3
                    std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 29u, 29u))  // 9 /0/1/4
                }
            }),
            std::make_shared<Node>(Node
            {
                Node::Type::Struct, MA_PARAMSTR(str_view, 33u, 90u), // nœud 3 /0/2
                {
                   std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 35u, 40u)), // toto_1 /0/2/0
                   std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 45u, 50u)), // toto_2 /0/2/1
                   std::make_shared<Node>(Node
                   {
                       Node::Type::Struct, MA_PARAMSTR(str_view, 54u, 89u), // nœud 3 1 /0/2/2
                       {
                           std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 55u, 59u)), // 10 /0/2/2/0
                           std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 62u, 70u)), // 11 /0/2/2/1
                           std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 73u, 78u)), // 12 /0/2/2/2
                           std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 81u, 86u))  // 13 /0/2/2/3
                       }
                   })
                }
            })
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_1)
{
    std::wstring str{LR"({""})"};
    //                   0123

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 3u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, 2, L""sv), // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_2)
{
    std::wstring str{LR"({"""})"};
    //                   01234

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 4u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 2u, 2u)), // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_3)
{
    std::wstring str{LR"({""""})"};
    //                   012345

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 5u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 2u, 3u)), // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_4)
{
    std::wstring str{LR"({"""""})"};
    //                   0123456

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 6u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, MA_PARAMSTR(str_view, 2u, 4u)), // /0/0
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_5)
{
    const std::wstring str{LR"({""", ""})"};
    //                         012345678

    std::wstring_view str_view{str};
    Node manual_root
    {
        Node::Type::Struct, MA_PARAMSTR(str_view, 0u, 9u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, 2u, LR"(")"sv), // /0/0
            std::make_shared<Node>(Node::Type::Value, 7u, L""sv), // /0/1
        }
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_6)
{
    const std::wstring str{LR"(})"};
    //                         0

    Node manual_root
    {
        Node::Type::Value, 0u, L"}"sv, // /0
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_7)
{
    const std::wstring str{LR"(,)"};
    //                         0

    Node manual_root
    {
        Node::Type::Value, 0u, L","sv, // /0
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_struct_border_line_8)
{
    const std::wstring str{LR"(, {})"};
    //                         0

    Node manual_root
    {
        Node::Type::Value, 0u, L", {}"sv, // /0
    };

    CompareRoots(str, manual_root);
}

Test(Serialization, node_invalid_struct_1)
{
    const std::wstring str{LR"({"", "})"};
    //                         0123456

    std::wstring_view str_view{str};
    Node manual_root
    {
        // L'accolade est interprétée comme faisant partie de la chaîne de
        // caractères du deuxième enfant. La structure de la racine n'est donc
        // pas complète.
        Node::Type::Invalid, MA_PARAMSTR(str_view, 0u, 6u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, 2u, L""sv), // /0/0
            std::make_shared<Node>(Node::Type::Invalid, 6u, L"}"sv), // /0/1
        }
    };

    CompareRoots(str, manual_root, true);
}

Test(Serialization, node_invalid_struct_2)
{
    const std::wstring str{LR"({"",  )"};
    //                         012345

    std::wstring_view str_view{str};
    Node manual_root
    {
        // Il n'y a pas d'accolade pour clore la structure.
        Node::Type::Invalid, MA_PARAMSTR(str_view, 0u, 5u), // /0
        {
            std::make_shared<Node>(Node::Type::Value, 2u, L""sv), // /0/0

            // L'index commence après les espaces formant le préfixe, dans le
            // cas présent les espaces vont jusqu'à la fin de chaîne de
            // caractères. La valeur est donc après ces espaces.
            std::make_shared<Node>(Node::Type::Value, 6u, L""sv), // /0/1
        }
    };

    CompareRoots(str, manual_root, true);
}

Test(Serialization, node_invalid_struct_3)
{
    const std::wstring str{LR"({)"};
    //                         0

    Node manual_root
    {
        // Il n'y a pas d'accolade pour clore la structure.
        Node::Type::Invalid, 0u, L"{"sv, // /0
    };

    CompareRoots(str, manual_root, true);
}

Test(Serialization, wstring_0)
{
    const std::wstring toto = L"toto";

    cr_assert(ma::IsValidString<std::wstring>(toto));
    cr_assert_eq(toto, ma::toString<std::wstring>(toto));
    cr_assert_eq(toto, ma::FromString<std::wstring>(toto));
}

Test(Serialization, wstring_1)
{
    const std::wstring str = L"{}";

    cr_assert(ma::IsValidString<std::wstring>(str));
    cr_assert_eq(str, ma::toString<std::wstring>(str));
    cr_assert_eq(str, ma::FromString<std::wstring>(str));
}

Test(Serialization, wstring_no_errors)
{
    const std::wstring str = L"toto";

    const auto result = ma::GetErrors<std::wstring>(str);

    cr_assert_eq(result.size(), 0,
                 "(result.size() = %zu) != 0",
                 result.size());
}

Test(Serialization, wstring_errors)
{
    const std::wstring str = L"{}";

    const auto result = ma::GetErrors<std::wstring>(str);

    cr_assert_eq(result.size(), 0u,
                 "(result.size() = %zu) != 0u",
                 result.size());
}

Test(Serialization, struct_wstring)
{
    const std::wstring str = LR"({""})";

    typedef std::vector<std::wstring> struct_str;
    struct_str value = {L""};

    cr_assert(ma::IsValidString<struct_str>(str));
    cr_assert_eq(str, ma::toString(value));
    cr_assert_eq(value, ma::FromString<struct_str>(str));
}

Test(Serialization, struct_wstring_no_errors)
{
    const std::wstring str = LR"({""})";

    const auto result = ma::GetErrors<std::wstring>(str);

    cr_assert_eq(result.size(), 0,
                 "(result.size() = %zu) != 0",
                 result.size());
}

#define variable_valid3(in_number, number_str, number_to_str) \
{ \
    const T number = static_cast<T>(in_number); \
    const char* type_chars = typeid(number).name(); \
    const auto number_str_ansi = ma::to_string(number_str); \
    const char* number_str_chars = number_str_ansi.c_str(); \
    \
    cr_assert(ma::IsValidString<T>(number_str), \
              "ma::IsValidString<%s>(number_str = %s)", \
              type_chars, \
              number_str_chars); \
 \
    const auto number_to_str_1_ansi = ma::to_string(ma::toString<T>(number)); \
    const char* number_chars = number_to_str_1_ansi.c_str(); \
    const auto number_to_str_2_ansi = ma::to_string(number_to_str); \
    const char* number_to_str_chars = number_to_str_2_ansi.c_str(); \
    const char* to_str_chars = number_to_str_2_ansi.c_str(); \
    const auto number_str_from_to_ansi = ma::to_string(ma::toString<T>(ma::FromString<T>(number_str))); \
    const char* from_str_chars = number_str_from_to_ansi.c_str(); \
 \
    cr_assert_eq(number_to_str, ma::toString<T>(number), \
                 "(number_to_str = %s) != (ma::toString<%s>(number = %s) = %s)", \
                 number_to_str_chars, \
                 type_chars, \
                 number_chars, \
                 to_str_chars); \
 \
    cr_assert_eq(number, ma::FromString<T>(number_str), \
                 "(number = %s) != (ma::FromString<%s>(number_str = %s) = %s)", \
                 number_chars, \
                 type_chars, \
                 number_str_chars, \
                 from_str_chars); \
}

#define variable_valid2(in_number, number_str) \
{ \
    const T number = static_cast<T>(in_number); \
    const char* type_chars = typeid(number).name(); \
    const auto number_str_ansi = ma::to_string(number_str); \
    const char* number_str_chars = number_str_ansi.c_str(); \
 \
    cr_assert(ma::IsValidString<T>(number_str),  \
              "ma::IsValidString<%s>(number_str = %s)", \
              type_chars, \
              number_str_chars); \
  \
    const auto number_to_str_ansi = ma::to_string(ma::toString<T>(number)); \
    const char* number_chars = number_to_str_ansi.c_str(); \
    const char* to_str_chars = number_to_str_ansi.c_str(); \
    const auto number_str_from_to_ansi = ma::to_string(ma::toString<T>(ma::FromString<T>(number_str))); \
    const char* from_str_chars = number_str_from_to_ansi.c_str(); \
 \
    cr_assert_eq(number_str, ma::toString<T>(number), \
                 "(number_str = %s) != (ma::toString<%s>(number = %s) = %s)", \
                 number_str_chars, \
                 type_chars, \
                 number_chars, \
                 to_str_chars); \
 \
    cr_assert_eq(number, ma::FromString<T>(number_str), \
                 "(number = %s) != (ma::FromString<%s>(number_str = %s) = %s)", \
                 number_chars, \
                 type_chars, \
                 number_str_chars, \
                 from_str_chars); \
}

#define _GET_variable_valid_OVERRIDE(_1, _2, _3, NAME, ...) NAME \

#define variable_valid(...) _GET_variable_valid_OVERRIDE(__VA_ARGS__, \
    variable_valid3, variable_valid2)(__VA_ARGS__)

// Pour des raisons d'optimisation l'écriture d'un nombre n'est pas autant vérifier que IsValidString.
//cr_assert_any_throw(ma::FromString<T>(number_str));
#define variable_invalid(number_str) \
{ \
    cr_assert_not(ma::IsValidString<T>(number_str), "ma::IsValidString<%s>(number_str = %s)", typeid(zero).name(), ma::to_string(number_str).c_str()); \
}

template <class T>
void serialization_init_number()
{
    const T zero = 0;
    const T one = static_cast<T>(1);

    variable_valid(zero, L"0");
    variable_valid(one, L"1");
    variable_valid(one, L"01", L"1");
    variable_valid(one, L"001", L"1");
    variable_valid(one, L"  1  ", L"1");

    variable_invalid(L"toto");
    variable_invalid(L"1a");
    variable_invalid(L"a1");
    variable_invalid(L"a1a");
    variable_invalid(L"1  a");
    variable_invalid(L"a  1");
    variable_invalid(L"a  1  a");

    if(std::is_same<T, int>::value ||
       std::is_same<T, long>::value ||
       std::is_same<T, long long>::value)
    {
        variable_invalid(L"1.");
        variable_invalid(L"1.0");

        variable_valid(-1, L"-1");
        variable_valid(10, L"10");
        variable_valid(-10, L"-10");
    }

    if(std::is_same<T, unsigned>::value ||
       std::is_same<T, unsigned long>::value ||
       std::is_same<T, unsigned long long>::value)
    {
        variable_invalid(L"1.");
        variable_invalid(L"1.0");
        variable_invalid(L"-1.");
        variable_valid(10, L"10");
        variable_invalid(L"-10");
    }

    if(std::is_same<T, float>::value ||
       std::is_same<T, double>::value ||
       std::is_same<T, long double>::value)
    {
        variable_invalid(L"");
        variable_invalid(L".");
        variable_invalid(L"1..");
        variable_invalid(L".1.");
        variable_invalid(L"..1");

        // Pour que les long double puisse avoir une valeur correcte. Il faut
        // utiliser le littéral pour longue double et s'assuré de l'exactitude
        // de la valeur.
        variable_valid(1.l, L"1.", L"1");
        variable_valid(2.l, L"2.", L"2");
        variable_valid(3.l, L"3.", L"3");
        variable_valid(4.l, L"4.", L"4");
        variable_valid(-1.l, L"-1.", L"-1");
        variable_valid(-2.l, L"-2.", L"-2");
        variable_valid(-3.l, L"-3.", L"-3");
        variable_valid(-4.l, L"-4.", L"-4");
        variable_valid(.1l, L".1", L"0.1");
        variable_valid(.2l, L".2", L"0.2");
        variable_valid(.3l, L".3", L"0.3");
        variable_valid(.4l, L".4", L"0.4");
        variable_valid(-.1l, L"-.1", L"-0.1");
        variable_valid(-.2l, L"-.2", L"-0.2");
        variable_valid(-.3l, L"-.3", L"-0.3");
        variable_valid(-.4l, L"-.4", L"-0.4");
        variable_valid(.1l, L"+.1", L"0.1");
        variable_valid(.2l, L"+.2", L"0.2");
        variable_valid(.3l, L"+.3", L"0.3");
        variable_valid(.4l, L"+.4", L"0.4");
        variable_valid(1.0l, L"1.0", L"1");
        variable_valid(3.0l, L"3.0", L"3");
        variable_valid(4.0l, L"4.0", L"4");
        variable_valid(5.0l, L"5.0", L"5");
        variable_valid(-1.0l, L"-1.0", L"-1");
        variable_valid(-2.0l, L"-2.0", L"-2");
        variable_valid(-3.0l, L"-3.0", L"-3");
        variable_valid(-4.0l, L"-4.0", L"-4");
        variable_valid(-5.0l, L"-5.0", L"-5");
        variable_valid(5.0123l, L"5.0123");
        variable_valid(-5.0123l, L"-5.0123");
        variable_valid(-1, L"-1");
        variable_valid(10, L"10");
        variable_valid(-10, L"-10");
        variable_valid(10.0l, L"10.0", L"10");
        variable_valid(-10.0l, L"-10.0", L"-10");
        variable_valid(-10.01234l, L"-10.01234");
        variable_valid(10.01234l, L"10.01234");

        // https://stackoverflow.com/questions/392981/how-can-i-convert-string-to-double-in-c
        variable_valid(0, L"0.", L"0");
        variable_valid(0, L"0.0", L"0");
        variable_valid(0, L"0.00", L"0");
        variable_valid(0, L"0.0e0", L"0");
        variable_valid(0, L"0.0e-0", L"0");
        variable_valid(0, L"0.0e+0", L"0");
        variable_valid(0, L"+0", L"0");
        variable_valid(0, L"+0.", L"0");
        variable_valid(0, L"+0.0", L"0");
        variable_valid(0, L"+0.00", L"0");
        variable_valid(0, L"+0.0e0", L"0");
        variable_valid(0, L"+0.0e-0", L"0");
        variable_valid(0, L"+0.0e+0", L"0");
        variable_valid(0, L"-0", L"0");
        variable_valid(0, L"-0.", L"0");
        variable_valid(0, L"-0.0", L"0");
        variable_valid(0, L"-0.00", L"0");
        variable_valid(0, L"-0.0e0", L"0");
        variable_valid(0, L"-0.0e-0", L"0");
        variable_valid(0, L"-0.0e+0", L"0");
    }

    T value_min = std::numeric_limits<T>::min(),
      value_max = std::numeric_limits<T>::max();

    std::wstringstream ss_min;
    ss_min.precision(std::numeric_limits<T>::digits10 + 1);
    ss_min << value_min;

    std::wstringstream ss_max;
    ss_max.precision(std::numeric_limits<T>::digits10 + 1);

    std::wstring value_min_str = ss_min.str();
    std::wstring value_max_str;

    if(std::is_same<T, float>::value ||
       std::is_same<T, double>::value ||
       std::is_same<T, long double>::value)
    {
        ss_max << std::fixed << value_max;

        value_max_str = ss_max.str();

        // http://cpp.sh/4lbgq
        // http://cpp.sh/9pj65y
        while(value_max_str.find('.') != std::string::npos && value_max_str.back() == '0')
            value_max_str.pop_back();

        if(value_max_str.back() == '.')
            value_max_str.pop_back();

        // sur mon gcc (8)
        //variable_valid(value_min, L"0"); // 3.362103143112093506e-4932, L"0" => value_min != 0…
        variable_valid(value_max, value_max_str);
    }
    else
    {
        ss_max << value_max;

        value_max_str = ss_max.str();

        variable_valid(value_min, value_min_str);
        variable_valid(value_max, value_max_str);
    }

    // Les nombres entiers ont pour limite basse 0. La valeur -1 est déjà testée.
    // Les nombres réels devraient ne pas valider une valeur plus petite que le
    // min.
    if(!std::is_same<T, unsigned>::value &&
       !std::is_same<T, unsigned long>::value &&
       !std::is_same<T, unsigned long long>::value &&
       !std::is_same<T, float>::value &&
       !std::is_same<T, double>::value &&
       !std::is_same<T, long double>::value)
    {
        variable_invalid(value_min_str + L"01");
    }

    variable_invalid(value_max_str + L"01");

    // no errors
    {
        const auto str = L"0"s;
        const auto result = ma::GetErrors<T>(str);

        cr_assert_eq(result.size(), 0,
                     "(result.size() = %zu) != 0",
                     result.size());
    }

    // errors
    {
        const auto str = L"{}"s;
        const auto result = ma::GetErrors<T>(str);

        const std::string type_name{ma::TypeName<T>()};
        const char* type_str_char = type_name.c_str();

        // 1- La racine est une structure au lieu d'être une valeur.
        // Aucune autre erreur sur le type de la string ne dois apparaitre, car
        // nous savons déjà que le type représenté n'est pas le bon.
        cr_assert_eq(result.size(), 1u,
                     "[%s] (result.size() = %zu) == 1u",
                     type_str_char,
                     result.size());
    }
}

Test(Serialization,        int_serialization, .init = serialization_init_number<int>){}
Test(Serialization,       long_serialization, .init = serialization_init_number<long>){}
Test(Serialization,   longlong_serialization, .init = serialization_init_number<long long>){}
Test(Serialization,          u_serialization, .init = serialization_init_number<unsigned>){}
Test(Serialization,      ulong_serialization, .init = serialization_init_number<unsigned long>){}
Test(Serialization,  ulonglong_serialization, .init = serialization_init_number<unsigned long long>){}
Test(Serialization,      float_serialization, .init = serialization_init_number<float>){}
Test(Serialization,     double_serialization, .init = serialization_init_number<double>){}
Test(Serialization, longdouble_serialization, .init = serialization_init_number<long double>){}

Test(Serialization, pair_serialization)
{
    {
        std::pair<int, int> int_int = {0, 1};
        std::wstring int_int_str = L"{0, 1}";
        cr_assert_eq(ma::toString(int_int), int_int_str);
    }

    {
        std::pair<std::wstring, int> str_int = {L"toto", 1};
        std::wstring str_int_str = LR"#({"toto", 1})#";
        cr_assert_eq(ma::toString(str_int), str_int_str);
    }
}

#define serialization_init_sequence_container_sub(T_Container, T_Number) \
{ \
    { \
        const T_Container<T_Number> numbers = {0, 1L, 2L, 3L}; \
        const std::wstring numbers_str = L"{0, 1, 2, 3}"; \
        const std::wstring numbers_to_str = ma::toString(numbers); \
 \
        const auto numbers_str_ansi = ma::to_string(numbers_str); \
        const char* numbers_str_chars = numbers_str_ansi.c_str(); \
        const auto numbers_to_str_ansi = ma::to_string(numbers_to_str); \
        const char* numbers_to_str_chars = numbers_to_str_ansi.c_str(); \
 \
        cr_assert_eq(numbers_to_str, numbers_str, \
                     "(ma::toString(%s) = %s) != %s", \
                     numbers_str_chars, \
                     numbers_to_str_chars, \
                     numbers_str_chars); \
    } \
 \
    { \
        const T_Container<std::pair<T_Number, T_Number>> pairs = {{0, 1L}, {2L, 3L}, {4L, 5L}}; \
        const std::wstring pairs_str = L"{{0, 1}, {2, 3}, {4, 5}}"; \
        cr_assert_eq(ma::toString(pairs), pairs_str); \
    } \
 \
    { \
        const T_Container<std::pair<T_Number, std::wstring>> pairs = {{0, L"toto"}, {2L, L"tata"}}; \
        const std::wstring pairs_str = LR"#({{0, "toto"}, {2, "tata"}})#"; \
        cr_assert_eq(ma::toString(pairs), pairs_str); \
    } \
}

#define serialization_init_sequence_container(T_Container) \
{ \
    serialization_init_sequence_container_sub(T_Container, int) \
    serialization_init_sequence_container_sub(T_Container, long) \
    serialization_init_sequence_container_sub(T_Container, long long) \
    serialization_init_sequence_container_sub(T_Container, unsigned) \
    serialization_init_sequence_container_sub(T_Container, unsigned long) \
    serialization_init_sequence_container_sub(T_Container, unsigned long long) \
    serialization_init_sequence_container_sub(T_Container, float) \
    serialization_init_sequence_container_sub(T_Container, double) \
    serialization_init_sequence_container_sub(T_Container, long double) \
}

Test(Serialization, vector_serialization){serialization_init_sequence_container(std::vector)}
Test(Serialization,  deque_serialization){serialization_init_sequence_container(std::deque)}
Test(Serialization,   list_serialization){serialization_init_sequence_container(std::list)}
Test(Serialization,    set_serialization){serialization_init_sequence_container(std::set)}
//Test(Serialization, array_serialization){serialization_init_sequence_container(std::array)} // taille fixe
//Test(Serialization, forward_list_serialization){serialization_init_sequence_container(std::forward_list)} // pas de fonction size()

Test(Serialization, map_serialization)
{
    {
        const std::map<int, std::wstring> key_values = {{0, L"toto"}, {1, L"tata"}};
        const std::wstring key_values_str = LR"#({{0, "toto"}, {1, "tata"}})#";
        cr_assert_eq(ma::toString(key_values), key_values_str);
    }
}

Test(Serialization, boolean_from_string_0)
{
    const std::wstring str = L"true";
    const bool value = true;
    ma::Serialization::Parser parser{str};

    cr_assert_eq(parser.GetValue<bool>(), value);
}

Test(Serialization, boolean_from_string_1)
{
    const std::wstring str = L"false";
    const bool value = false;
    ma::Serialization::Parser parser{str};

    cr_assert_eq(parser.GetValue<bool>(), value);
}

Test(Serialization, number_from_string_0)
{
    ma::Serialization::Parser parser{L"0"};

    cr_assert(parser.IsValid<int>());
    cr_assert(parser.IsValid<long>());
    cr_assert(parser.IsValid<long long>());
    cr_assert(parser.IsValid<unsigned>());
    cr_assert(parser.IsValid<unsigned long>());
    cr_assert(parser.IsValid<unsigned long long>());
    cr_assert(parser.IsValid<float>());
    cr_assert(parser.IsValid<double>());
    cr_assert(parser.IsValid<long double>());

    cr_assert_eq(parser.GetValue<int>(), 0);
    cr_assert_eq(parser.GetValue<long>(), 0);
    cr_assert_eq(parser.GetValue<long long>(), 0);
    cr_assert_eq(parser.GetValue<unsigned>(), 0);
    cr_assert_eq(parser.GetValue<unsigned long>(), 0);
    cr_assert_eq(parser.GetValue<unsigned long long>(), 0);
    cr_assert_eq(parser.GetValue<float>(), 0);
    cr_assert_eq(parser.GetValue<double>(), 0);
    cr_assert_eq(parser.GetValue<long double>(), 0);
}

Test(Serialization, number_from_string_1)
{
    ma::Serialization::Parser parser{L"1"};

    cr_assert(parser.IsValid<int>());
    cr_assert(parser.IsValid<long>());
    cr_assert(parser.IsValid<long long>());
    cr_assert(parser.IsValid<unsigned>());
    cr_assert(parser.IsValid<unsigned long>());
    cr_assert(parser.IsValid<unsigned long long>());
    cr_assert(parser.IsValid<float>());
    cr_assert(parser.IsValid<double>());
    cr_assert(parser.IsValid<long double>());

    cr_assert_eq(parser.GetValue<int>(), 1);
    cr_assert_eq(parser.GetValue<long>(), 1);
    cr_assert_eq(parser.GetValue<long long>(), 1);
    cr_assert_eq(parser.GetValue<unsigned>(), 1);
    cr_assert_eq(parser.GetValue<unsigned long>(), 1);
    cr_assert_eq(parser.GetValue<unsigned long long>(), 1);
    cr_assert_eq(parser.GetValue<float>(), 1);
    cr_assert_eq(parser.GetValue<double>(), 1);
    cr_assert_eq(parser.GetValue<long double>(), 1);
}

Test(Serialization, number_from_string_1_plus)
{
    ma::Serialization::Parser parser{L"+1"};

    cr_assert(parser.IsValid<int>());
    cr_assert(parser.IsValid<long>());
    cr_assert(parser.IsValid<long long>());
    cr_assert(parser.IsValid<unsigned>());
    cr_assert(parser.IsValid<unsigned long>());
    cr_assert(parser.IsValid<unsigned long long>());
    cr_assert(parser.IsValid<float>());
    cr_assert(parser.IsValid<double>());
    cr_assert(parser.IsValid<long double>());

    cr_assert_eq(parser.GetValue<int>(), 1);
    cr_assert_eq(parser.GetValue<long>(), 1);
    cr_assert_eq(parser.GetValue<long long>(), 1);
    cr_assert_eq(parser.GetValue<unsigned>(), 1);
    cr_assert_eq(parser.GetValue<unsigned long>(), 1);
    cr_assert_eq(parser.GetValue<unsigned long long>(), 1);
    cr_assert_eq(parser.GetValue<float>(), 1);
    cr_assert_eq(parser.GetValue<double>(), 1);
    cr_assert_eq(parser.GetValue<long double>(), 1);
}

Test(Serialization, number_from_string_1_neg)
{
    ma::Serialization::Parser parser{L"-1"};

    cr_assert(parser.IsValid<int>());
    cr_assert(parser.IsValid<long>());
    cr_assert(parser.IsValid<long long>());
    cr_assert_not(parser.IsValid<unsigned>());
    cr_assert_not(parser.IsValid<unsigned long>());
    cr_assert_not(parser.IsValid<unsigned long long>());
    cr_assert(parser.IsValid<float>());
    cr_assert(parser.IsValid<double>());
    cr_assert(parser.IsValid<long double>());

    cr_assert_eq(parser.GetValue<int>(), -1);
    cr_assert_eq(parser.GetValue<long>(), -1);
    cr_assert_eq(parser.GetValue<long long>(), -1);
    cr_assert_eq(parser.GetValue<float>(), -1);
    cr_assert_eq(parser.GetValue<double>(), -1);
    cr_assert_eq(parser.GetValue<long double>(), -1);
}

Test(Serialization, real_from_string_0)
{
    ma::Serialization::Parser parser{L"0.0000"};

    cr_assert_not(parser.IsValid<int>());
    cr_assert_not(parser.IsValid<long>());
    cr_assert_not(parser.IsValid<long long>());
    cr_assert_not(parser.IsValid<unsigned>());
    cr_assert_not(parser.IsValid<unsigned long>());
    cr_assert_not(parser.IsValid<unsigned long long>());
    cr_assert(parser.IsValid<float>());
    cr_assert(parser.IsValid<double>());
    cr_assert(parser.IsValid<long double>());

    cr_assert_eq(parser.GetValue<float>(), 0);
    cr_assert_eq(parser.GetValue<double>(), 0);
    cr_assert_eq(parser.GetValue<long double>(), 0);
}

Test(Serialization, real_from_string_1)
{
    ma::Serialization::Parser parser{L"0.0001"};
    long double value = 0.0001l;

    cr_assert_eq(parser.GetValue<float>(), static_cast<float>(value),
                 "(parser.GetValue<float>() = %f) != (value =  %f)",
                 parser.GetValue<float>(),
                 static_cast<float>(value));

    cr_assert_eq(parser.GetValue<double>(), static_cast<double>(value));
    cr_assert_eq(parser.GetValue<long double>(), value);
}

Test(Serialization, string_from_string_0)
{
    std::wstring str = L"toto";
    ma::Serialization::Parser parser{str};

    // Bien vérifier la gestion d'erreurs et l'accès à la valeur.
    // Les deux pourraient avoir une implémentation différente.
    cr_assert(parser.GetErrors<std::wstring>().empty());
    cr_assert_eq(parser.GetValue<std::wstring>(), str);
}

Test(Serialization, string_from_string_1)
{
    std::wstring str = LR"({""})";
    ma::Serialization::Parser parser{str};

    cr_assert(parser.GetErrors<std::wstring>().empty());

    ma::g_ConsoleVerbose = 0;
    // Peu importe le type du nœud, nous devons pouvoir interpréter ça comme une chaîne de caractère puisque c'est une
    // de caractères.
    cr_assert_no_throw(parser.GetValue<std::wstring>(), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Serialization, string_is_representing_by_string_1)
{
    cr_assert(ma::IsRepresentedByString<std::wstring>());
}

Test(Serialization, string_is_representing_by_const_string_1)
{
    cr_assert(ma::IsRepresentedByString<const std::wstring>());
}

Test(Serialization, string_is_representing_by_string_2)
{
    auto str{ma::Quoted(L"Toto")};

    cr_assert(ma::IsRepresentedByString<decltype(str)>());
}

Test(Serialization, string_is_representing_by_const_string_2)
{
    const auto str{ma::Quoted(L"Toto")};

    cr_assert(ma::IsRepresentedByString<decltype(str)>());
}

Test(Serialization, struct_empty_from_string_0)
{
    ma::Serialization::Parser parser{L"{}"};

    cr_assert_eq(parser.GetValue<std::vector<int>>(), std::vector<int>{});
    cr_assert_eq(parser.GetValue<std::deque<int>>(), std::deque<int>{});
    cr_assert_eq(parser.GetValue<std::set<int>>(), std::set<int>{});
    cr_assert_eq(parser.GetValue<std::unordered_set<int>>(), std::unordered_set<int>{});
    cr_assert_eq(parser.GetValue<std::list<int>>(), std::list<int>{});

    // std::queue
    {
        auto value_from_str = parser.GetValue<std::queue<int>>();
        auto deq = std::deque<int>{};
        std::queue<int> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::priority_queue
    {
        auto value_from_str = parser.GetValue<std::priority_queue<int>>();
        const auto vec = std::vector<int>{};
        std::priority_queue<int> value(vec.begin(), vec.end());

        while(!value.empty())
        {
            const int value_a = value.top();
            value.pop();

            const int value_b = value_from_str.top();
            value_from_str.pop();

            cr_assert_eq(value_a, value_b);
        }
    }

    // std::stack
    {
        auto value_from_str = parser.GetValue<std::stack<int>>();
        auto deq = std::deque<int>{};
        std::stack<int> value(deq);
        cr_assert_eq(value_from_str, value);
    }
}

Test(Serialization, struct_empty_to_string_0)
{
    std::wstring result = L"{}";

    cr_assert_eq(result, ma::toString(std::vector<int>{}));
    cr_assert_eq(result, ma::toString(std::deque<int>{}));
    cr_assert_eq(result, ma::toString(std::set<int>{}));
    cr_assert_eq(result, ma::toString(std::unordered_set<int>{}));
    cr_assert_eq(result, ma::toString(std::list<int>{}));

    // std::queue
    {
        auto deq = std::deque<int>{};
        std::queue<int> value(deq);
        std::wstring str_from_value = ma::toString(value);
        cr_assert_eq(result, str_from_value);
    }

    // std::priority_queue
    {
        const auto vec = std::vector<int>{};
        std::priority_queue<int> value(vec.begin(), vec.end());
        cr_assert_eq(result, ma::toString(value));
    }

    // std::stack
    {
        auto deq = std::deque<int>{};
        std::stack<int> value(deq);
        cr_assert_eq(result, ma::toString(value));
    }
}

Test(Serialization, struct_empty_from_string_1)
{
    std::wstring str = L"toto";
    ma::Serialization::Parser parser{L"{" + str + L"}"};

    cr_assert_eq(parser.GetValue<std::vector<std::wstring>>(), std::vector<std::wstring>{str});
    cr_assert_eq(parser.GetValue<std::deque<std::wstring>>(), std::deque<std::wstring>{str});
    cr_assert_eq(parser.GetValue<std::set<std::wstring>>(), std::set<std::wstring>{str});
    cr_assert_eq(parser.GetValue<std::unordered_set<std::wstring>>(), std::unordered_set<std::wstring>{str});
    cr_assert_eq(parser.GetValue<std::list<std::wstring>>(), std::list<std::wstring>{str});

    // std::queue
    {
        auto value_from_str = parser.GetValue<std::queue<std::wstring>>();
        auto deq = std::deque<std::wstring>{str};
        std::queue<std::wstring> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::priority_queue
    {
        auto value_from_str = parser.GetValue<std::priority_queue<std::wstring>>();
        const auto vec = std::vector<std::wstring>{str};
        std::priority_queue<std::wstring> value(vec.begin(), vec.end());

        while(!value.empty())
        {
            const std::wstring value_a = value.top();
            value.pop();

            const std::wstring value_b = value_from_str.top();
            value_from_str.pop();

            cr_assert_eq(value_a, value_b);
        }
    }

    // std::stack
    {
        auto value_from_str = parser.GetValue<std::stack<std::wstring>>();
        auto deq = std::deque<std::wstring>{str};
        std::stack<std::wstring> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::array
    {
        auto value_from_str = parser.GetValue<std::array<std::wstring, 1u>>();
        auto value = std::array<std::wstring, 1u>{ {str} };
        cr_assert_eq(value_from_str, value);
    }
}

Test(Serialization, struct_empty_to_string_1)
{
    const auto str = L"toto"s;
    const auto result = LR"({"toto"})"s;

    cr_assert_eq(result, ma::toString(std::vector<std::wstring>{str}));
    cr_assert_eq(result, ma::toString(std::deque<std::wstring>{str}));
    cr_assert_eq(result, ma::toString(std::set<std::wstring>{str}));
    cr_assert_eq(result, ma::toString(std::unordered_set<std::wstring>{str}));
    cr_assert_eq(result, ma::toString(std::list<std::wstring>{str}));

    // std::queue
    {
        auto deq = std::deque<std::wstring>{str};
        std::queue<std::wstring> value(deq);
        std::wstring str_from_value = ma::toString(value);
        cr_assert_eq(result, str_from_value);
    }

    // std::priority_queue
    {
        const auto vec = std::vector<std::wstring>{str};
        std::priority_queue<std::wstring> value(vec.begin(), vec.end());
        cr_assert_eq(result, ma::toString(value));
    }

    // std::stack
    {
        auto deq = std::deque<std::wstring>{str};
        std::stack<std::wstring> value(deq);
        cr_assert_eq(result, ma::toString(value));
    }

    // std::array
    {
        auto value = std::array<std::wstring, 1u>{ {str} };
        const auto str_from_value = ma::toString(value);
        cr_assert_eq(result, str_from_value);
    }
}

Test(Serialization, struct_empty_from_string_2)
{
    ma::Serialization::Parser parser{L"{1}"};

    cr_assert_eq(parser.GetValue<std::vector<int>>(), std::vector<int>{1});
    cr_assert_eq(parser.GetValue<std::deque<int>>(), std::deque<int>{1});
    cr_assert_eq(parser.GetValue<std::set<int>>(), std::set<int>{1});
    cr_assert_eq(parser.GetValue<std::unordered_set<int>>(), std::unordered_set<int>{1});
    cr_assert_eq(parser.GetValue<std::list<int>>(), std::list<int>{1});

    // std::queue
    {
        auto value_from_str = parser.GetValue<std::queue<int>>();
        auto deq = std::deque<int>{1};
        std::queue<int> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::priority_queue
    {
        auto value_from_str = parser.GetValue<std::priority_queue<int>>();
        const auto vec = std::vector<int>{1};
        std::priority_queue<int> value(vec.begin(), vec.end());

        while(!value.empty())
        {
            const int value_a = value.top();
            value.pop();

            const int value_b = value_from_str.top();
            value_from_str.pop();

            cr_assert_eq(value_a, value_b);
        }
    }

    // std::stack
    {
        auto value_from_str = parser.GetValue<std::stack<int>>();
        auto deq = std::deque<int>{1};
        std::stack<int> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::array
    {
        auto value_from_str = parser.GetValue<std::array<int, 1u>>();
        auto value = std::array<int, 1u>{{1}};
        cr_assert_eq(value_from_str, value);
    }
}

Test(Serialization, struct_empty_to_string_2)
{
    const auto result = L"{1}"s;

    cr_assert_eq(result, ma::toString(std::vector<int>{1}));
    cr_assert_eq(result, ma::toString(std::deque<int>{1}));
    cr_assert_eq(result, ma::toString(std::set<int>{1}));
    cr_assert_eq(result, ma::toString(std::unordered_set<int>{1}));
    cr_assert_eq(result, ma::toString(std::list<int>{1}));

    // std::queue
    {
        auto deq = std::deque<int>{1};
        std::queue<int> value(deq);
        std::wstring str_from_value = ma::toString(value);
        cr_assert_eq(result, str_from_value);
    }

    // std::priority_queue
    {
        const auto vec = std::vector<int>{1};
        std::priority_queue<int> value(vec.begin(), vec.end());
        cr_assert_eq(result, ma::toString(value));
    }

    // std::stack
    {
        auto deq = std::deque<int>{1};
        std::stack<int> value(deq);
        cr_assert_eq(result, ma::toString(value));
    }

    // std::array
    {
        auto value = std::array<int, 1u>{ {1} };
        cr_assert_eq(result, ma::toString(value));
    }
}

Test(Serialization, struct_empty_from_string_3)
{
    ma::Serialization::Parser parser{L"{1, 2, 3, 4}"};

    cr_assert_eq(parser.GetValue<std::vector<int>>(), (std::vector<int>{1, 2, 3, 4}));
    cr_assert_eq(parser.GetValue<std::deque<int>>(), (std::deque<int>{1, 2, 3, 4}));
    cr_assert_eq(parser.GetValue<std::set<int>>(), (std::set<int>{1, 2, 3, 4}));
    cr_assert_eq(parser.GetValue<std::list<int>>(), (std::list<int>{1, 2, 3, 4}));

    // std::queue
    {
        auto value_from_str = parser.GetValue<std::queue<int>>();
        auto deq = std::deque<int>{1, 2, 3, 4};
        std::queue<int> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::priority_queue
    {
        auto value_from_str = parser.GetValue<std::priority_queue<int>>();
        const auto vec = std::vector<int>{1, 2, 3, 4};
        std::priority_queue<int> value(vec.begin(), vec.end());

        while(!value.empty())
        {
            const int value_a = value.top();
            value.pop();

            const int value_b = value_from_str.top();
            value_from_str.pop();

            cr_assert_eq(value_a, value_b);
        }
    }

    // std::stack
    {
        auto value_from_str = parser.GetValue<std::stack<int>>();
        auto deq = std::deque<int>{1, 2, 3, 4};
        std::stack<int> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::array
    {
        auto value_from_str = parser.GetValue<std::array<int, 4u>>();
        auto value = std::array<int, 4u>{ {1, 2, 3, 4} };
        cr_assert_eq(value_from_str, value);
    }
}

Test(Serialization, struct_empty_to_string_3)
{
    const auto result = L"{1, 2, 3, 4}"s;

    cr_assert_eq(result, ma::toString(std::vector<int>{1, 2, 3, 4}));
    cr_assert_eq(result, ma::toString(std::deque<int>{1, 2, 3, 4}));
    cr_assert_eq(result, ma::toString(std::set<int>{1, 2, 3, 4}));
    //cr_assert_eq(result, ma::toString(std::unordered_set<int>{1, 2, 3, 4}));
    cr_assert_eq(result, ma::toString(std::list<int>{1, 2, 3, 4}));

    // std::queue
    {
        auto deq = std::deque<int>{1, 2, 3, 4};
        std::queue<int> value(deq);
        std::wstring str_from_value = ma::toString(value);
        cr_assert_eq(result, str_from_value);
    }

    // std::priority_queue
    {
        const auto vec = std::vector<int>{1, 2, 3, 4};
        std::priority_queue<int, std::vector<int>, std::greater<int>> value(vec.begin(), vec.end());
        cr_assert_eq(result, ma::toString(value));
    }

    // std::stack
    {
        auto deq = std::deque<int>{1, 2, 3, 4};
        std::stack<int> value(deq);
        cr_assert_eq(result, ma::toString(value));
    }

    // std::array
    {
        auto value = std::array<int, 4u>{ {1, 2, 3, 4} };
        cr_assert_eq(result, ma::toString(value));
    }
}

Test(Serialization, struct_empty_from_string_4)
{
    std::wstring str_1 = L"toto_1",
                 str_2 = L"toto_2",
                 str_3 = L"toto_3",
                 str_4 = L"toto_4";

    ma::Serialization::Parser parser{L"{" + str_1 + L", " + str_2 + L", " + str_3 + L", " + str_4 + L"}"};

    cr_assert_eq(parser.GetValue<std::vector<std::wstring>>(), (std::vector<std::wstring>{str_1, str_2, str_3, str_4}));
    cr_assert_eq(parser.GetValue<std::deque<std::wstring>>(), (std::deque<std::wstring>{str_1, str_2, str_3, str_4}));
    cr_assert_eq(parser.GetValue<std::set<std::wstring>>(), (std::set<std::wstring>{str_1, str_2, str_3, str_4}));
    cr_assert_eq(parser.GetValue<std::list<std::wstring>>(), (std::list<std::wstring>{str_1, str_2, str_3, str_4}));

    // std::queue
    {
        auto value_from_str = parser.GetValue<std::queue<std::wstring>>();
        auto deq = std::deque<std::wstring>{str_1, str_2, str_3, str_4};
        std::queue<std::wstring> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::priority_queue
    {
        auto value_from_str = parser.GetValue<std::priority_queue<std::wstring>>();
        const auto vec = std::vector<std::wstring>{str_1, str_2, str_3, str_4};
        std::priority_queue<std::wstring> value(vec.begin(), vec.end());

        while(!value.empty())
        {
            const std::wstring value_a = value.top();
            value.pop();

            const std::wstring value_b = value_from_str.top();
            value_from_str.pop();

            cr_assert_eq(value_a, value_b);
        }
    }

    // std::stack
    {
        auto value_from_str = parser.GetValue<std::stack<std::wstring>>();
        auto deq = std::deque<std::wstring>{str_1, str_2, str_3, str_4};
        std::stack<std::wstring> value(deq);
        cr_assert_eq(value_from_str, value);
    }

    // std::array
    {
        auto value_from_str = parser.GetValue<std::array<std::wstring, 4u>>();
        auto value = std::array<std::wstring, 4u>{{str_1, str_2, str_3, str_4}};
        cr_assert_eq(value_from_str, value);
    }
}

Test(Serialization, struct_empty_to_string_4)
{
    const std::wstring str_1 = L"toto_1",
                       str_2 = L"toto_2",
                       str_3 = L"toto_3",
                       str_4 = L"toto_4";

    const auto result = LR"({"toto_1", "toto_2", "toto_3", "toto_4"})"s;

    cr_assert_eq(result, ma::toString(std::vector<std::wstring>{str_1, str_2, str_3, str_4}));
    cr_assert_eq(result, ma::toString(std::deque<std::wstring>{str_1, str_2, str_3, str_4}));
    cr_assert_eq(result, ma::toString(std::set<std::wstring>{str_1, str_2, str_3, str_4}));
    //cr_assert_eq(result, ma::toString(std::unordered_set<std::wstring>{1, 2, 3, 4}));
    cr_assert_eq(result, ma::toString(std::list<std::wstring>{str_1, str_2, str_3, str_4}));

    // std::queue
    {
        auto deq = std::deque<std::wstring>{str_1, str_2, str_3, str_4};
        std::queue<std::wstring> value(deq);
        std::wstring str_from_value = ma::toString(value);
        cr_assert_eq(result, str_from_value);
    }

    // std::priority_queue
    {
        const auto vec = std::vector<std::wstring>{str_1, str_2, str_3, str_4};
        std::priority_queue<std::wstring, std::vector<std::wstring>, std::greater<std::wstring>> value(vec.begin(), vec.end());
        cr_assert_eq(result, ma::toString(value));
    }

    // std::stack
    {
        auto deq = std::deque<std::wstring>{str_1, str_2, str_3, str_4};
        std::stack<std::wstring> value(deq);
        cr_assert_eq(result, ma::toString(value));
    }

    // std::array
    {
        auto value = std::array<std::wstring, 4u>{ {str_1, str_2, str_3, str_4} };
        cr_assert_eq(result, ma::toString(value));
    }
}

Test(Serialization, struct_empty_from_string_5)
{
    ma::Serialization::Parser parser{L"0"};

    cr_assert_not(parser.IsValid<std::vector<int>>());
    cr_assert_not(parser.IsValid<std::deque<int>>());
    cr_assert_not(parser.IsValid<std::set<int>>());
    cr_assert_not(parser.IsValid<std::list<int>>());
    cr_assert_not(parser.IsValid<std::queue<int>>());
    cr_assert_not(parser.IsValid<std::priority_queue<int>>());
    cr_assert_not(parser.IsValid<std::stack<int>>());
}

Test(Serialization, struct_empty_from_string_6)
{
    ma::Serialization::Parser parser{L"{0}"};

    cr_assert_eq(parser.GetErrors<std::vector<int>>().size(), 0);
    cr_assert_eq(parser.GetErrors<std::deque<int>>().size(), 0);
    cr_assert_eq(parser.GetErrors<std::set<int>>().size(), 0);
    cr_assert_eq(parser.GetErrors<std::list<int>>().size(), 0);
    cr_assert_eq(parser.GetErrors<std::queue<int>>().size(), 0);
    cr_assert_eq(parser.GetErrors<std::priority_queue<int>>().size(), 0);
    cr_assert_eq(parser.GetErrors<std::stack<int>>().size(), 0);
}

Test(Serialization, tuple_to_string_0)
{
    typedef std::tuple<std::wstring, int, float> tuple;

    const auto tuple_str = LR"({"toto", 1, -2.1})"s;
    const auto tuple_value = tuple{L"toto", 1, -2.1f};

    const auto tuple_str_from_value = ma::toString(tuple_value);

    cr_assert_eq(tuple_str, tuple_str_from_value,
                 "(tuple_str: %s) != (tuple_str_from_value: %s)",
                 ma::to_string(tuple_str).c_str(),
                 ma::to_string(tuple_str_from_value).c_str());
}

Test(Serialization, tuple_from_string_0)
{
    typedef std::tuple<std::wstring, int, float> tuple;

    const auto tuple_str = LR"({"toto", 1, -2.1})"s;
    const auto tuple_value = tuple{L"toto", 1, -2.1f};

    const auto tuple_value_from_str = ma::FromString<tuple>(tuple_str);

    cr_assert_eq(tuple_value, tuple_value_from_str);
}

Test(Serialization, tuple_valid_string_0)
{
    typedef std::tuple<std::wstring, int, float> tuple;

    const auto tuple_str = LR"({"toto", 1, -2.1})"s;
    const bool result = ma::IsValidString<tuple>(tuple_str);

    cr_assert(result);
}

Test(Serialization, tuple_not_valid_string_0)
{
    typedef std::tuple<std::wstring, int, float> tuple;

    const auto tuple_str = LR"({"toto", "toto", -2.1})"s;
    const bool result = ma::IsValidString<tuple>(tuple_str);

    cr_assert_not(result);
}

Test(Serialization, tuple_no_errors_0)
{
    typedef std::tuple<std::wstring, int, float> tuple;

    const auto tuple_str = LR"({"toto_1", 1, -2.1})"s;
    //                         01234567890123456789010123
    //                         0         1         2
    const auto result = ma::GetErrors<tuple>(tuple_str);

    cr_assert_eq(result.size(), 0,
                 "(result.size() = %zu) != 0",
                 result.size());
}

Test(Serialization, tuple_errors_0)
{
    typedef std::tuple<std::wstring, int, float> tuple;

    const auto tuple_str = LR"({"toto_1", "toto_2", -2.1})"s;
    //                         01234567890123456789010123
    //                         0         1         2
    const auto result = ma::GetErrors<tuple>(tuple_str);

    cr_assert_eq(result.size(), 1u,
                 "(result.size() = %zu) != 1u",
                 result.size());

    // L'erreur se situe sur le nœud de toto_2, celui débute à l'index 12, c'est
    // à dire juste après les premiers guillemets.
    cr_assert_eq(result[0].first, 12u,
                 "(result[0].first: %zu) != 12u",
                 result[0].first);
}

Test(Serialization, struct_tuple_0)
{
    const std::wstring str = LR"({{1, "A"}, {2, "B"}})";

    typedef std::vector<std::tuple<int, std::wstring>> struct_tuple_type;
    const struct_tuple_type value{{1, L"A"}, {2, L"B"}};

    cr_assert(ma::IsValidString<struct_tuple_type>(str));
    const auto errors = ma::GetErrors<struct_tuple_type>(str);
    cr_assert_eq(0, errors.size());

    cr_assert_eq(str, ma::toString(value));
    cr_assert_eq(value, ma::FromString<struct_tuple_type>(str));
}

Test(Serialization, struct_tuple_1)
{
    const std::wstring str = LR"({{1, "A"}, {2, "B"}})";

    typedef std::pair<int, std::wstring> int_str_type;
    typedef std::tuple<int_str_type, int_str_type> struct_tuple_type;
    const struct_tuple_type value{{1, L"A"}, {2, L"B"}};

    cr_assert(ma::IsValidString<struct_tuple_type>(str));
    cr_assert_eq(str, ma::toString(value));
    cr_assert_eq(value, ma::FromString<struct_tuple_type>(str));
}

Test(Serialization, struct_tuple_2)
{
    const std::wstring str = LR"({{1, "A"}, {2, "B"}})";

    typedef std::tuple<int, std::wstring> int_str_type;
    typedef std::tuple<int_str_type, int_str_type> struct_tuple_type;
    const struct_tuple_type value{{1, L"A"}, {2, L"B"}};

    cr_assert(ma::IsValidString<struct_tuple_type>(str));
    cr_assert_eq(str, ma::toString(value));
    cr_assert_eq(value, ma::FromString<struct_tuple_type>(str));
}

Test(Serialization, struct_tuple_3)
{
    const std::wstring str = LR"({{1, "A"}, {2, "B"}})";

    typedef std::tuple<int, std::wstring> int_str_type;
    typedef std::pair<int_str_type, int_str_type> struct_tuple_type;
    const struct_tuple_type value{{1, L"A"}, {2, L"B"}};

    cr_assert(ma::IsValidString<struct_tuple_type>(str));
    const auto errors = ma::GetErrors<struct_tuple_type>(str);
    cr_assert_eq(0, errors.size());

    cr_assert_eq(str, ma::toString(value));
    cr_assert_eq(value, ma::FromString<struct_tuple_type>(str));
}

Test(Serialization, struct_tuple_4)
{
    const std::wstring str = LR"({{1, {"A1", "A2"}}, {2, {"B1", "B2"}}})";

    typedef std::vector<std::tuple<int, std::pair<std::wstring, std::wstring>>> struct_tuple_type;
    const struct_tuple_type value{{1, {L"A1", L"A2"}}, {2, {L"B1", L"B2"}}};

    cr_assert(ma::IsValidString<struct_tuple_type>(str));
    const auto errors = ma::GetErrors<struct_tuple_type>(str);
    cr_assert_eq(0, errors.size());

    cr_assert_eq(str, ma::toString(value));
    cr_assert_eq(value, ma::FromString<struct_tuple_type>(str));
}

Test(Serialization, struct_tuple_5)
{
    const std::wstring str = LR"({{{"key", "m_Name"}, {"type", "StringVar"}, {"value", "print_HelloWorld.py"}}, {{"key", "m_CustomSavePath"}, {"type", "BoolVar"}, {"value", "false"}}})";

    typedef std::pair<std::wstring, std::wstring> pair_str;
    typedef std::tuple<pair_str, pair_str, pair_str> var_save_type;
    typedef std::vector<var_save_type> body_save_type;
    const body_save_type value
    {
        {{L"key", L"m_Name"},{L"type", L"StringVar"},{L"value", L"print_HelloWorld.py"}},
        {{L"key", L"m_CustomSavePath"},{L"type", L"BoolVar"},{L"value", L"false"}}
    };

    // std::wcout << ma::toString(value) << std::endl;

    cr_assert(ma::IsValidString<body_save_type>(str));
    cr_assert_eq(str, ma::toString(value));
    cr_assert_eq(value, ma::FromString<body_save_type>(str));
    // ne peut être lié
    //const auto errors = ma::GetErrors<body_save_type>(str);
    //cr_assert_eq(0, errors.size());
}

// Erreurs trouvées et à corriger :
Test(Serialization, serialization_error_end_is_begin)
{
    typedef std::pair<std::wstring, std::tuple<int, int, int, int>> test_type;
    const std::wstring value_str = LR"({"toto", {1, 1, 1})"; // mauvaise structure
    ma::Serialization::Errors errors;
    cr_assert_none_throw(errors = ma::GetErrors<test_type>(value_str));
}

Test(Serialization, serialization_error_double_end_braces)
{
    using test_type = std::wstring;
    const std::wstring value_str = L"{}}"s;
    ma::Serialization::Errors errors;
    cr_assert_none_throw(errors = ma::GetErrors<test_type>(value_str));
}

Test(Serialization, array_from_string_1)
{
    typedef std::array<std::wstring, 1u> array;

    const std::wstring str = L"toto";
    auto array_str = L"{"s + str + L"}"s;

    auto value = array{ {str} };
    auto result = ma::FromString<array>(array_str);

    cr_assert_eq(result, value);
}

Test(Serialization, array_from_string_2)
{
    typedef std::array<std::wstring, 2u> array;

    const std::wstring str_1 = L"toto_1";
    const std::wstring str_2 = L"toto_2";
    auto array_str = L"{"s + str_1 + L", " + str_2 + L"}"s;

    auto value = array{ {str_1, str_2} };
    auto result = ma::FromString<array>(array_str);

    cr_assert_eq(result, value);
}

Test(Serialization, array_from_string_3)
{
    typedef std::array<std::wstring, 2u> array;

    const std::wstring str_1 = L"toto_1";
    auto array_str = L"{"s + str_1 + L"}"s;

    auto value = array{ {str_1, L""} };
    auto result = ma::FromString<array>(array_str);

    cr_assert_eq(result, value);
}

Test(Serialization, array_from_string_4)
{
    typedef std::array<std::wstring, 2u> array;

    const std::wstring str_1 = L"toto_1";
    const std::wstring str_2 = L"toto_2";
    const auto array_str = L"{"s + str_1 + L", " + str_2 + L"}"s;

    cr_assert_no_throw(ma::FromString<array>(array_str), std::invalid_argument);

    const auto result = ma::FromString<array>(array_str);
    const auto value = array{ {str_1, str_2} };

    cr_assert_eq(result, value);
}

Test(Serialization, eigen_matrix_from_string_1)
{
    typedef Eigen::Matrix<float, 3, 3> matrix;

    const auto matrix_str = L"{0, 1, 2, 3, 4, 6, 7, 8, 9}"s;

    matrix value{{0, 1, 2},
                 {3, 4, 6},
                 {7, 8, 9}};

    auto result = ma::FromString<matrix>(matrix_str);

    cr_assert_eq(result, value);
}

Test(Serialization, eigen_matrix_from_string_2)
{
    typedef Eigen::Matrix<float, 3, 3> matrix;

    const auto matrix_str = L"{{0, 1, 2}, {3, 4, 6}, {7, 8, 9}}"s;

    matrix value{{0, 1, 2},
                 {3, 4, 6},
                 {7, 8, 9}};

    auto result = ma::FromString<matrix>(matrix_str);

    cr_assert_eq(result, value);
}

Test(Serialization, eigen_matrix_to_string_1)
{
    typedef Eigen::Matrix<int, 3, 3> matrix;

    const auto matrix_str = L"{{0, 1, 2}, {3, 4, 6}, {7, 8, 9}}"s;

    matrix value{{0, 1, 2},
                 {3, 4, 6},
                 {7, 8, 9}};

    const auto result = ma::toString<matrix>(value);
    cr_assert_eq(result, matrix_str);
}

Test(Serialization, eigen_matrix_to_string_2)
{
    typedef Eigen::Matrix<int, 1, 3> matrix;

    const auto matrix_str = L"{0, 1, 2}"s;

    matrix value{0, 1, 2};

    const auto result = ma::toString<matrix>(value);
    cr_assert_eq(result, matrix_str);
}

Test(Serialization, eigen_matrix_to_string_3)
{
    typedef Eigen::Matrix<int, 1, 3> matrix;

    const auto matrix_str = L"{0, 1, 2}"s;

    matrix value{{0, 1, 2}};

    const auto result = ma::toString<matrix>(value);
    cr_assert_eq(result, matrix_str);
}

Test(Serialization, eigen_matrix_to_string_4)
{
    typedef Eigen::Matrix<int, 2, 2, 0, 2, 2> matrix;

    const auto matrix_str = L"{{0, 1}, {2, 3}}"s;

    matrix value{{0, 1},
                 {2, 3}};

    const auto result = ma::toString<matrix>(value);
    cr_assert_eq(result, matrix_str);
}

Test(Serialization, eigen_quaterion_to_string_1)
{
    typedef Eigen::Quaternion<double> quaternion;

    const auto quaternion_str = L"{1, 0, 0, 0}"s;

    quaternion value{1, 0, 0, 0};

    const auto result = ma::toString<quaternion>(value);
    cr_assert_eq(result, quaternion_str);
}

Test(Serialization, eigen_quaterion_from_string_1)
{
    typedef Eigen::Quaternion<double> quaternion;

    const auto quaternion_str = L"{1, 0, 0, 0}"s;

    quaternion value{1, 0, 0, 0};

    const auto result = ma::FromString<quaternion>(quaternion_str);
    cr_assert_eq(result, value);
}

Test(Serialization, eigen_transform_to_string_1)
{
    typedef ma::Eigen::Transformd transform_d;

    const auto transform_str = L"{{0, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 11}, {12, 13, 14, 15}}"s;

    const transform_d::MatrixType matrix{{0, 1, 2, 3},
                                         {4, 5, 6, 7},
                                         {8, 9, 10, 11},
                                         {12, 13, 14, 15}};

    const transform_d value{matrix};

    const auto result = ma::toString<transform_d>(value);
    cr_assert_eq(result, transform_str);
}

Test(Serialization, eigen_transform_from_string_1)
{
    typedef ma::Eigen::Transformd transform_d;

    const auto transform_str = L"{{0, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 11}, {12, 13, 14, 15}}"s;

    const transform_d::MatrixType matrix{{ 0,  1,  2,  3},
                                         { 4,  5,  6,  7},
                                         { 8,  9, 10, 11},
                                         {12, 13, 14, 15}};

    const transform_d value{matrix};

    const auto result = ma::FromString<transform_d>(transform_str);
    cr_assert_eq(result.matrix(), value.matrix());
}

Test(Serialization, pair_empty_from_string_0)
{
    ma::Serialization::Parser parser{LR"#({2, "toto"})#"};
    typedef std::pair<int, std::wstring> int_str;

    cr_assert(parser.IsValid<int_str>());

    const int_str value{2, L"toto"};
    const int_str result = parser.GetValue<int_str>();
    cr_assert_eq(result, value);
}

Test(Serialization, unordered_set_0)
{
    typedef std::unordered_set<std::wstring> set_str;

    const set_str values{L"toto_1", L"toto_2"};
    const std::wstring values_str = LR"({"toto_1", "toto_2"})";

    ma::Serialization::Parser parser{values_str};

    cr_assert(parser.IsValid<set_str>());
    const set_str result = parser.GetValue<set_str>();

    cr_assert_eq(values.size(), result.size());

    for(auto str : values)
        cr_assert_neq(result.find(str), result.end());
}

Test(Serialization, unordered_map_0)
{
    typedef std::unordered_map<int, bool> map_int_bool;

    const map_int_bool values{{1, true}, {2, false}};
    const std::wstring values_str = L"{{1, true}, {2, false}}";

    ma::Serialization::Parser parser{values_str};

    cr_assert(parser.IsValid<map_int_bool>());
    const map_int_bool result = parser.GetValue<map_int_bool>();

    cr_assert_eq(values.size(), result.size());

    for(auto value : values)
    {
        auto it_find = result.find(value.first);
        cr_assert_neq(it_find, result.end());
        cr_assert_eq(value.second, it_find->second);
    }
}

Test(Serialization, unordered_map_1)
{
    typedef std::unordered_map<std::wstring, std::wstring> map_str_str;

    const map_str_str values{{L"toto", L"test"}, {L"tata", L"test2"}};
    std::wstring values_str = L"{";

    // = LR"({{"toto", "test"}, {"tata", "test2"}})";
    std::wstringstream ss;
    auto index = values.size();
    for(auto &&[key, value] : values)
    {
        ss << L"{" << std::quoted(key) << L", " << std::quoted(value) << L"}";
        if(index > 1)
            ss << L", ";

        index--;
    }

    values_str += ss.str();
    values_str += L"}";

    ma::Serialization::Parser parser{values_str};

    cr_assert(parser.IsValid<map_str_str>());
    const map_str_str result = parser.GetValue<map_str_str>();

    cr_assert_eq(values.size(), result.size());

    for(auto value : values)
    {
        auto it_find = result.find(value.first);
        cr_assert_neq(it_find, result.end());
        cr_assert_eq(value.second, it_find->second);
    }

    const auto values_str2 = ma::toString(values);
    cr_assert_eq(values_str, values_str2);
}

Test(Serialization, filesystem_path_0)
{
    const std::wstring values_str = L"/home/toto";
    const auto value = std::filesystem::path(values_str);

    ma::Serialization::Parser parser{values_str};

    cr_assert(parser.IsValid<std::filesystem::path>());
    const auto result = parser.GetValue<std::filesystem::path>();

    cr_assert_eq(result, value);

    cr_assert_eq(ma::toString(value), values_str);
}

void serialization_item_setup()
{
    ma::g_ConsoleVerbose = 1;
    ma::Item::CreateRoot<ma::Item>();
}

void serialization_item_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(Item, serialization_item_1, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    std::wstring item_str;

    cr_assert_none_throw(item_str = ma::toString(item));
    cr_assert_eq(item->GetKey(), item_str);

    ma::ItemPtr item_from_str;
    cr_assert_none_throw(item_from_str = ma::FromString<ma::Item>(item_str));
    cr_assert_eq(item_from_str, item);
}

Test(Item, serialization_item_2, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item_1 = ma::Item::CreateItem<ma::Item>();
    auto item_2 = ma::Item::CreateItem<ma::Item>();
    typedef std::vector<ma::ItemPtr> Items;
    Items items{item_1, item_2},
          items_from_str;

    std::wstring items_str_manual{LR"({")" + item_1->GetKey() + LR"(", ")" + item_2->GetKey() + LR"("})"};

    std::wstring items_str_auto;

    cr_assert_none_throw(items_str_auto = ma::toString(items));
    cr_assert_eq(items_str_auto, items_str_manual);

    cr_assert_none_throw(items_from_str = ma::FromString<Items>(items_str_auto));
    cr_assert_eq(items_from_str, items);
}

Test(Item, serialization_item_3, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    ma::ItemPtr item_null;

    std::wstring item_str;

    cr_assert_none_throw(item_str = ma::toString(item_null));
    cr_assert(item_str.empty());
}

Test(Item, serialization_item_4, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    ma::ItemPtr item_null;

    std::wstring item_str;

    cr_assert_none_throw(item_str = ma::toString(item_null));
    cr_assert(item_str.empty());
}

Test(Item, serialization_item_5, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    typedef std::deque<ma::ItemPtr> Items;
    Items items{ma::ItemPtr{}, ma::ItemPtr{}};

    const std::wstring items_str = LR"({"", ""})";

    cr_assert_eq(items_str, ma::toString(items));
}

Test(Item, serialization_varvar_to_string, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 2);
    auto varvar = item->AddVar<ma::VarVar>(L"m_Variable"s, var);

    const std::wstring var_str = LR"({")"s + item->GetKey() + LR"(", "m_Value"})"s;

    cr_assert_eq(var_str, ma::toString(var),
                 "(var_str: %s) != (ma::toString(var): %s)",
                 ma::to_string(var_str).c_str(),
                 ma::to_string(ma::toString(var)).c_str());

    cr_assert_eq(var_str, varvar->toString(),
                 "(var_str: %s) != (varvar->toString(): %s)",
                 ma::to_string(var_str).c_str(),
                 ma::to_string(varvar->toString()).c_str());
}

Test(Item, serialization_varvar_valid_string, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 0);

    const std::wstring var_str = LR"({")"s + item->GetKey() + LR"(", "m_Value"})"s;

    cr_assert(ma::IsValidString<ma::Var>(var_str));
    cr_assert(ma::IsValidString<ma::VarPtr>(var_str));
}

Test(Item, serialization_varvar_not_valid_string, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 0);

    cr_assert_not(ma::IsValidString<ma::Var>(L"{"s + item->GetKey() + LR"(, "m_NoValue"})"s));
    cr_assert_not(ma::IsValidString<ma::VarPtr>(L"{"s + item->GetKey() + LR"(, "m_NoValue"})"s));

    cr_assert_not(ma::IsValidString<ma::Var>(LR"({")"s + item->GetKey() + LR"(", "m_NoValue"})"s));
    cr_assert_not(ma::IsValidString<ma::VarPtr>(LR"({")"s + item->GetKey() + LR"(", "m_NoValue"})"s));

    cr_assert_not(ma::IsValidString<ma::Var>(LR"({")"s + ma::Item::Key::GetNoParent() + LR"(", "m_Value"})"s));
    cr_assert_not(ma::IsValidString<ma::VarPtr>(LR"({")"s + ma::Item::Key::GetNoParent() + LR"(", "m_Value"})"s));
}

Test(Item, serialization_varvar_from_string, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 0);

    const std::wstring var_str = LR"({")"s + item->GetKey() + LR"(", "m_Value"})"s;

    cr_assert_eq(var, ma::FromString<ma::VarPtr>(var_str));
}

Test(Item, serialization_varvar_no_error, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 0);

    const std::wstring var_str = LR"({")"s + item->GetKey() + LR"(", "m_Value"})"s;

    cr_assert_eq(0, ma::GetErrors<ma::VarPtr>(var_str).size());
}

//Test(Item, serialization_varvar_error_2, .init = serialization_item_setup, .fini = serialization_item_teardown)
//{
//    auto item = ma::Item::CreateItem<ma::Item>();
//    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 0);
//
//    const std::wstring var_str = L"{"s + item->GetKey() + LR"(, "m_Value"})"s;
//    const ma::Serialization::Errors errors = ma::GetErrors<ma::VarPtr>(var_str);
//    std::cout << ma::toString(errors) << std::endl;
//    cr_assert_eq(1u, errors.size());
//    cr_assert_eq(1u, errors[0].first);
//
//}

Test(Item, serialization_varvar_error_2, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 0);

    const std::wstring var_str = LR"({")"s + ma::Item::Key::GetNoParent() + LR"(", "m_Value"})"s;
    const ma::Serialization::Errors errors = ma::GetErrors<ma::VarPtr>(var_str);

    cr_assert_eq(1u, errors.size());
    cr_assert_eq(2u, errors[0].first);
}

Test(Item, serialization_varvar_error_3, .init = serialization_item_setup, .fini = serialization_item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::IntVar>(L"m_Value"s, 0);

    // {"2798396d-d211-40b9-8b8f-55a631d74032", "m_NoValue"}
    // 01234567890123456789012345678901234567890123456789012
    // 0         1         2         3         4         5
    const std::wstring var_str = LR"({")"s + item->GetKey() + LR"(", "m_NoValue"})"s;
    const ma::Serialization::Errors errors = ma::GetErrors<ma::VarPtr>(var_str);

    cr_assert_eq(1u, errors.size());
    cr_assert_eq(42u, errors[0].first);
}
