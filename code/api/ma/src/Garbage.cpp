/// \file Garbage.cpp
/// \author Pontier Pierre
/// \date 2022-02-11
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Garbage.h"
#include "ma/ObservationManager.h"

namespace ma
{
    Trash::Trash(const Item::ConstructorParameters &in_params, ma::ItemPtr item):
    Item(in_params),
    m_Item(std::move(item)),
    m_ItemKey(AddReadOnlyMemberVar<ma::StringVar>(L"m_ItemKey", m_Item->GetKey())),
    m_ItemClassName(AddReadOnlyMemberVar<ma::StringVar>(L"m_ItemClassName", m_Item->GetTypeInfo().m_ClassName))
    {
        if(m_Item->HasVar(Key::Var::GetName()))
            AddReadOnlyMemberVar<ma::StringVar>(L"m_ItemName", m_Item->GetVar(Key::Var::GetName())->toString());
    }

    Trash::~Trash() = default;

    bool Trash::IsObservable() const
    {
        return false;
    }

    const TypeInfo &Trash::GetTrashTypeInfo()
    {
        static const TypeInfo info = {
            GetTrashClassName(),
            L"La classe Trash permet de stocker un item  ayant été retirer de Item::ms_Map.",
            {{TypeInfo::Key::GetWhiteListParentsData(), {ma::Garbage::GetGarbageClassName()}}}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(Trash, Item)

    Garbage::Garbage(const Item::ConstructorParameters &in_params):
    Item({in_params, Key::GetGarbage(), false}),
    m_Links{},
    m_Root(HasItemOpt(Item::Key::GetRoot()).value_or(nullptr))
    {}

    Garbage::~Garbage() = default;

    void Garbage::EndConstruction()
    {
        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Item::Key::GetObservationManager());
           m_Root && opt.has_value())
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, m_Root);
        }

        Item::EndConstruction();
    }

    void Garbage::BeginObservation(const ObservablePtr &observable)
    {}

    void Garbage::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(m_Root == observable)
        {
            for(const auto &action : data.GetOrdered())
            {
                const auto &key = action.second;
                if(action.first == Item::Key::Obs::ms_GlobalAddItemBegin && Id::IsValid(key))
                {
                    if(auto opt = HasTrashOpt(key))
                    {
                        auto trash = opt.value();
                        if(trash && trash->m_Item.use_count() == 1u)
                        {
                            RemoveChild(trash);
                        }

                        // Ultime tentative
                        if(trash && trash->m_Item.use_count() > 0)
                        {
                            CleanTrashes();
                        }
                    }

                    if(auto opt = HasTrashOpt(key))
                    {
                        MA_ASSERT(false,
                                  L"L'item « " + key + L" » est encore référencé « " +
                                      ma::toString(opt.value().use_count()) +
                                      L" », en cet instant, il ne peut être détruit.",
                                  std::logic_error);
                    }
                }
            }
        }
    }

    void Garbage::EndObservation(const ObservablePtr &observable)
    {}

    void Garbage::OnEnable()
    {
        Item::OnEnable();
    }

    void Garbage::OnDisable()
    {
        Item::OnDisable();
    }

    bool Garbage::HasTrash(const Item::key_type &key) const
    {
        return m_Links.find(key) != m_Links.end();
    }

    std::optional<TrashPtr> Garbage::HasTrashOpt(const Item::key_type &key) const
    {
        auto it_find = m_Links.find(key);

        if(it_find != m_Links.end())
            return std::dynamic_pointer_cast<Trash>(GetItem(it_find->second, SearchMod::LOCAL));
        else
            return {};
    }

    TrashPtr Garbage::AddTrash(const ItemPtr &item)
    {
        const auto item_key = item->GetKey();
        MA_ASSERT(!HasTrash(item_key), L"L'item fait déjà parti de cette poubelle.", std::invalid_argument);

        auto trash = Item::CreateItem<Trash>({GetKey()}, item);
        m_Links.insert({item_key, trash->GetKey()});

        return trash;
    }

    TrashPtr Garbage::GetTrash(const Item::key_type &key)
    {
        auto it_find = m_Links.find(key);
        MA_ASSERT(it_find != m_Links.end(), L"L'item ne fait pas parti de cette poubelle.", std::invalid_argument);

        return std::dynamic_pointer_cast<Trash>(GetItem(it_find->second, SearchMod::LOCAL));
    }

    void Garbage::RemoveTrash(const Item::key_type &key)
    {
        auto trash = GetTrash(key);

        RemoveChild(trash);
    }

    void Garbage::RemoveChild(ma::ItemPtr item)
    {
        auto trash = std::dynamic_pointer_cast<Trash>(item);
        if(trash)
            m_Links.erase(trash->m_Item->GetKey());
        Item::RemoveChild(trash);
    }

    void Garbage::CleanTrashes()
    {
        // L'élément est peut-être lié à d'autre élément dans la poubelle.
        // Nettoyons la poubelle jusqu'à ce que le nombre d'éléments ne varie plus.
        const auto max_iteration = GetCount(Item::SearchMod::LOCAL);
        size_t count_trashes = 0;
        size_t previous_count_trashes = max_iteration;
        for(auto i = max_iteration; i > 0 && count_trashes != previous_count_trashes; i--)
        {
            const auto children = GetItems(Item::SearchMod::LOCAL);
            for(auto &&[key, child] : children)
            {
                auto trash = std::dynamic_pointer_cast<Trash>(child);
                if(trash && trash->m_Item.use_count() == 1u)
                {
                    RemoveChild(child);
                }
            }

            previous_count_trashes = count_trashes;
            count_trashes = m_Links.size();
        }

        MA_WARNING(m_Links.empty(),
                   L"Après avoir tenté de vider la poubelle, il reste « " + ma::toString(m_Links.size()) +
                       L" » débris. Soit les éléments restant s'auto référence, soit il y a les éléments actifs "
                       L"continue de les référencer.");
    }

    const TypeInfo &Garbage::GetGarbageTypeInfo()
    {
        static const TypeInfo info = {GetGarbageClassName(),
                                      L"La classe « Garbage » permet de stocker et retrouver les items ayant été "
                                      L"retirer de Item::ms_Map. Si un Item n'a plus qu'une référence, c'est à dire "
                                      L"dans la poubelle, il sera supprimer au prochain appel à « CleanTrashes ».",
                                      {{TypeInfo::Key::GetWhiteListChildrenData(), {ma::Trash::GetTrashClassName()}}}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(Garbage, Item)
} // namespace ma
