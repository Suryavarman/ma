/// \file ObservationManager.cpp
/// \author Pontier Pierre
/// \date 2020-01-08
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/ObservationManager.h"
#include "ma/ClassInfo.h"
#include "ma/Error.h"

namespace ma
{
    // Observation //———————————————————————————————————————————————————————————————————————————————————————————————————
    Observation::Observation(const Item::ConstructorParameters &in_params):
    Item::Item(in_params),
    m_DestructionBegin(false),
    m_DestructionEnd(false),
    m_ObserverVar(),
    m_ObservableVar()
    {}

    Observation::~Observation() = default;

    bool Observation::IsObservable() const
    {
        return false;
    }

    const TypeInfo &ma::Observation::GetObservationTypeInfo()
    {
        static const TypeInfo result = {
            GetObservationClassName(), L"C'est le lien entre un observable et un observateur.", {}};

        return result;
    };
    M_CPP_CLASSHIERARCHY(Observation, Item)

    // ObservationManager //————————————————————————————————————————————————————————————————————————————————————————————
    ObservationManager::ObservationManager(const Item::ConstructorParameters &in_params):
    Item::Item({in_params, Key::GetObservationManager(), false}),
    m_ObsaMap(),
    m_ObseMap(),
    m_ObservationsDestruction()
    {}

    ObservationManager::~ObservationManager() = default;

    void ObservationManager::RemoveChild(ItemPtr item)
    {
        MA_ASSERT(item, L"La variable item est nulle.", std::invalid_argument);

        ma::ObservationPtr observation;

        auto classinfo = ma::Item::GetItem<ma::ClassInfoManager>(Key::GetClassInfoManager());
        if(classinfo->IsInherit(item, ma::Observation::GetObservationClassName()))
        {
            observation = std::dynamic_pointer_cast<ma::Observation>(item);

            MA_ASSERT(observation, L"Impossible de convertir item en ma::Observation", std::invalid_argument);

            auto observer = observation->m_ObserverVar->Get();
            auto observer_key = observer->GetObserverKey();
            auto observable = observation->m_ObservableVar->Get();

            m_ObseMap.at(observer_key).erase(observable);
            m_ObsaMap.at(observable).erase(observer_key);

            if(m_ObseMap.at(observer_key).empty())
                m_ObseMap.erase(observer_key);

            if(m_ObsaMap.at(observable).empty())
                m_ObsaMap.erase(observable);

            if(!m_ObsaMap.count(observable))
                observable->SetObserve(false);
        }

        ma::Item::RemoveChild(item);
    }

    bool ObservationManager::HasObservation(ObserverPtr observer, ObservablePtr observable) const
    {
        return HasObservation(observer, observable, false);
    }

    bool ObservationManager::HasObservation(ObserverPtr observer,
                                            ObservablePtr observable,
                                            bool with_destruction_progress) const
    {
        auto it_find = m_ObsaMap.find(observable);

        if(it_find != m_ObsaMap.end())
        {
            auto it_find_obse = it_find->second.find(observer->GetObserverKey());

            if(it_find_obse != it_find->second.end())
            {
                auto key = it_find_obse->second;
                return (m_ObservationsDestruction.find(key) == m_ObservationsDestruction.end() ||
                        with_destruction_progress) &&
                       ma::Item::HasItem(key);
            }
        }

        return false;
    }

    bool ObservationManager::HasObservable(ObservablePtr observable) const
    {
        return m_ObsaMap.find(observable) != m_ObsaMap.end();
    }

    bool ObservationManager::HasObserver(ObserverPtr observer) const
    {
        return m_ObseMap.find(observer->GetObserverKey()) != m_ObseMap.end();
    }

    ObservationManager::key_type ObservationManager::GetObservationKey(ObserverPtr observer,
                                                                       ObservablePtr observable) const
    {
        return GetObservationKey(observer, observable, false);
    }

    ObservationManager::key_type ObservationManager::GetObservationKey(ObserverPtr observer,
                                                                       ObservablePtr observable,
                                                                       bool with_destruction_progress) const
    {
        auto it_find_obsa = m_ObsaMap.find(observable);

        MA_ASSERT(
            it_find_obsa != m_ObsaMap.end(), L"Aucune observation associée n'a été trouvée.", std::invalid_argument);

        auto it_find_obse = it_find_obsa->second.find(observer->GetObserverKey());

        MA_ASSERT(it_find_obse != it_find_obsa->second.end(),
                  L"Aucune observation associée n'a été trouvée.",
                  std::invalid_argument);

        auto key = it_find_obse->second;

        MA_ASSERT(m_ObservationsDestruction.find(key) == m_ObservationsDestruction.end() || with_destruction_progress,
                  L"Le paramètre «with_destruction_progress» est à faux et l'observation est cours de destruction.",
                  std::invalid_argument);

        return key;
    }

    ObservationPtr ObservationManager::GetObservation(ObserverPtr observer, ObservablePtr observable) const
    {
        return GetObservation(observer, observable, false);
    }

    ObservationPtr ObservationManager::GetObservation(ObserverPtr observer,
                                                      ObservablePtr observable,
                                                      bool with_destruction_progress) const
    {
        auto key = GetObservationKey(observer, observable, with_destruction_progress);
        auto item = GetItem(key, ma::Item::SearchMod::LOCAL);

        auto observation = std::dynamic_pointer_cast<ma::Observation>(item);
        MA_ASSERT(observation, L"L'item référencé dans it_find_obse n'est pas une observation", std::runtime_error);

        return observation;
    }

    Observations ObservationManager::GetObservations(ObserverPtr observer) const
    {
        return GetObservations(observer, false);
    }

    Observations ObservationManager::GetObservations(ObserverPtr observer, bool with_destruction_progress) const
    {
        ma::Observations result;

        auto it_find = m_ObseMap.find(observer->GetObserverKey());

        if(it_find != m_ObseMap.end())
            for(auto it : it_find->second)
            {
                auto item = GetItem(it.second, ma::Item::SearchMod::LOCAL);

                MA_ASSERT(item, L"item est nulle.", std::runtime_error);

                auto classinfo = ma::Item::GetItem<ma::ClassInfoManager>(Key::GetClassInfoManager());
                if(classinfo->IsInherit(item, ma::Observation::GetObservationClassName()))
                {
                    auto observation = std::dynamic_pointer_cast<ma::Observation>(item);
                    MA_ASSERT(observation, L"item n'est pas une observation.", std::runtime_error);

                    if(!observation->m_DestructionBegin || with_destruction_progress)
                        result[it.second] = observation;
                }
            }

        return result;
    }

    Observations ObservationManager::GetObservations(ObservablePtr observable) const
    {
        return GetObservations(observable, false);
    }

    Observations ObservationManager::GetObservations(ObservablePtr observable, bool with_destruction_progress) const
    {
        ma::Observations result;

        auto it_find = m_ObsaMap.find(observable);

        if(it_find != m_ObsaMap.end())
            for(auto it : it_find->second)
            {
                auto key = it.second;
                auto item = GetItem(key, ma::Item::SearchMod::LOCAL);

                auto classinfo = ma::Item::GetItem<ma::ClassInfoManager>(Key::GetClassInfoManager());
                if(classinfo->IsInherit(item, ma::Observation::GetObservationClassName()))
                {
                    auto observation = std::dynamic_pointer_cast<ma::Observation>(item);
                    MA_ASSERT(observation, L"observation est nulle.", std::runtime_error);

                    if(!observation->m_DestructionBegin || with_destruction_progress)
                        result[key] = observation;
                }
            }

        return result;
    }

    void ObservationManager::UpdateObservations(ObservablePtr observable, const Observable::Data &data)
    {
        MA_ASSERT(observable->IsObservable(),
                  L"Les mécanismes d'observation ne peuvent fonctionner qu'avec des observables observable.",
                  std::runtime_error);

        auto observations = GetObservations(observable);

        for(const auto &it : observations)
        {
            auto observation = it.second;

            if(observation->m_ObserverVar->IsValid())
            {
                auto observer = observation->m_ObserverVar->Get();

                MA_ASSERT(observer, L"L'observateur est nulle.", std::runtime_error);

                if(!observer->GetDestroyCall())
                    observer->UpdateObservation(observable, data);
                else
                {
                    MA_WARNING(false, L"Appel en pleine destruction de l'observateur de la fonction.");
                }
            }
            else
            {
                MA_WARNING(false, L"L'observateur n'est plus valide.");
            }
        }
    }

    void ObservationManager::UpdateObservations(const Observable::Data &data)
    {
        ma::Item::UpdateObservations(data);
    }

    void ObservationManager::EndObservationBlock(ObservationPtr observation)
    {
        observation->m_DestructionBegin = true;
        m_ObservationsDestruction[observation->GetKey()] = observation;

        auto observer = observation->m_ObserverVar->Get();
        auto observable = observation->m_ObservableVar->Get();
        observer->EndObservation(observable);

        observation->m_DestructionEnd = true;
        m_ObservationsDestruction.erase(observation->GetKey());

        if(ma::Item::HasItem(observation->GetKey()))
            ma::Item::EraseItem(observation->GetKey());
    }

    void ObservationManager::RemoveObservation(ObserverPtr observer, ObservablePtr observable)
    {
        MA_ASSERT(
            HasObservation(observer, observable, true), L"Cette observation n'existe pas.", std::invalid_argument);

        auto key = GetObservationKey(observer, observable, true);
        if(m_ObservationsDestruction.find(key) == m_ObservationsDestruction.end())
        {
            auto observation = GetObservation(observer, observable, true);

            // La destruction de l'observation a déjà député.
            // Elle sera donc correctement libérée par la fonction appelante.
            if(!observation->m_DestructionBegin)
            {
                auto observer_key = observer->GetObserverKey();

                MA_ASSERT(observer == observation->m_ObserverVar->Get(),
                          L"L'observateur de l'observation n'est pas la même instance que celle de l'observateur passé "
                          L"en paramètre.",
                          std::runtime_error);

                MA_ASSERT(observer_key == observation->m_ObserverVar->Get()->GetObserverKey(),
                          L"L'observateur de l'observation ne renvoie pas la même clef que celle de l'observateur "
                          L"passé en paramètre.",
                          std::runtime_error);

                EndObservationBlock(observation);

                //            // L'observateur étant un pointeur et non un shared_ptr, la fin de son
                //            // observation peut provoquer la destruction de celui-ci et rendre toute
                //            // référence à celui-ci caduque.
                //            // Il se peut aussi que l'observateur eu déjà nettoyé toutes ses observations lors de sa
                //            // destruction.
                //            if(observable->IsObserve())
                //            {
                //                m_ObseMap.at(observer_key).erase(observable);
                //                m_ObsaMap.at(observable).erase(observer_key);
                //
                //                if(!m_ObseMap.at(observer_key).size())
                //                    m_ObseMap.erase(observer_key);
                //
                //                if(!m_ObsaMap.at(observable).size())
                //                    m_ObsaMap.erase(observable);
                //
                //                if(!m_ObsaMap.count(observable))
                //                    observable->SetObserve(false);
                //            }

                // À faire après le nettoyage de m_ObseMap et de m_ObsaMap :
                // Car si on insère dans le root->current le gestionnaire d'observation, il peut se passer que
                // GetItem renvoie nullptr depuis la fonction GetObservations.
                // Est-ce une erreur, je ne sais pas à voir.
                //            ma::Item::EraseItem(observation->GetKey());
            }
        }
    }

    void ObservationManager::RemoveObservable(ObservablePtr observable)
    {
        auto observations = GetObservations(observable);

        for(auto &&[key, observation] : observations)
        {
            auto observer = observation->m_ObserverVar->Get();
            auto observer_key = observer->GetObserverKey();

            MA_ASSERT(observable == observation->m_ObservableVar->Get(),
                      L"observable est différent de celui stocker dans l'observation",
                      std::runtime_error);

            EndObservationBlock(observation);

            //        ma::Item::EraseItem(observation->GetKey());

            //        m_ObseMap.at(observer_key).erase(observable);
            //
            //        if(!m_ObseMap.at(observer_key).size())
            //            m_ObseMap.erase(observer_key);
        }

        //    m_ObsaMap.erase(observable);

        //    observable->SetObserve(false);
    }

    void ObservationManager::RemoveObserver(ObserverPtr observer)
    {
        MA_ASSERT(!observer->GetDestroyCall(),
                  L"L'observateur est en cours de destruction. La fonction « RemoveObserverFromObserverDestroy » a "
                  L"normalement déjà été appelée.",
                  std::logic_error);

        auto observations = GetObservations(observer);
        auto observer_key = observer->GetObserverKey();

        for(auto it : observations)
        {
            auto observation = it.second;
            auto observable = observation->m_ObservableVar->Get();

            MA_ASSERT(observer == observation->m_ObserverVar->Get(),
                      L"L'observateur de l'observation n'est pas la même instance que celle de l'observateur passé en "
                      L"paramètre.",
                      std::runtime_error);

            MA_ASSERT(observer_key == observation->m_ObserverVar->Get()->GetObserverKey(),
                      L"L'observateur de l'observation ne renvoie pas la même clef que celle de l'observateur passé en "
                      L"paramètre.",
                      std::runtime_error);

            EndObservationBlock(observation);

            //        ma::Item::EraseItem(observation->GetKey());
            //        m_ObsaMap.at(observable).erase(observer_key);
            //
            //        if(!m_ObsaMap.at(observable).size())
            //            m_ObsaMap.erase(observable);
            //
            //        if(!m_ObsaMap.count(observable))
            //            observable->SetObserve(false);
        }

        //    m_ObseMap.erase(observer_key);
    }

    void ObservationManager::RemoveObserverFromObserverDestroy(ObserverPtr observer)
    {
        auto observations = GetObservations(observer);
        auto observer_key = observer->GetObserverKey();

        for(auto it : observations)
        {
            auto observation = it.second;

            if(!observation->m_DestructionBegin)
            {
                auto observable = observation->m_ObservableVar->Get();

                ma::Item::EraseItem(observation->GetKey());

                //            // Pour éviter des bogues. Mais c'est pas bien car cela révèle que
                //            // le fonctionnement est incertain.
                //            if(m_ObsaMap.find(observable) != m_ObsaMap.end())
                //            {
                //                m_ObsaMap[observable].erase(observer_key);
                //                //m_ObsaMap.at(observable).erase(observer_key);
                //
                //                if(!m_ObsaMap.at(observable).size())
                //                    m_ObsaMap.erase(observable);
                //            }
                //
                //            if(!m_ObsaMap.count(observable))
                //                observable->SetObserve(false);
            }
        }

        //    m_ObseMap.erase(observer_key);
    }

    const TypeInfo &ObservationManager::GetObservationManagerTypeInfo()
    {
        // clang-format off
        static const TypeInfo result =
        {
            GetObservationManagerClassName(),
            L"C'est le gestionnaire d'observation qui crée, met à jour et détruit les liens d'observations."
            L"C'est à dire les instances de ma::Observation.",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        ma::Observation::GetObservationClassName()
                    }
                }
            }
        };
        // clang-format on
        return result;
    }
    M_CPP_CLASSHIERARCHY(ObservationManager, Item)
} // namespace ma
