/// \file RTTI.ut.cpp
/// \author Pontier Pierre
/// \date 2019-12-10
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires des classes RTTI et Maker.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

void rtti_setup()
{
    ma::g_ConsoleVerbose = 1u;
    ma::Item::CreateRoot<ma::Item>();
}

void rtti_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(RTTI, item_maker, .init = rtti_setup, .fini = rtti_teardown)
{
    ma::RTTIPtr rtti = ma::Item::CreateItem<ma::RTTI>();
    auto root = ma::Item::GetItem(ma::Item::Key::GetRoot());
    cr_assert(root->HasItem(rtti->GetKey(), ma::Item::SearchMod::LOCAL));

    ma::ItemMakerPtr item_maker = ma::Item::CreateItem<ma::ItemMaker>({rtti->GetKey()});
    cr_assert(rtti->HasItem(item_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    cr_assert_eq(ma::ItemMaker::GetClassName(), ma::Item::GetItemClassName());
    cr_assert(item_maker->HasVar(ma::ItemMaker::Key::GetClassNameItemProperty()));
    cr_assert_eq(item_maker->GetVar(ma::ItemMaker::Key::GetClassNameItemProperty())->toString(), ma::Item::GetItemClassName());

    auto makers = rtti->GetRegisteredMakerItems();
    cr_assert_eq(makers.size(), 1u);
    cr_assert_neq(makers.find(L"Item"), makers.end());

    ma::ItemPtr item;
    cr_assert_none_throw(item = rtti->CreateItem(ma::ItemMaker::GetClassName(), {ma::Item::Key::GetRoot()}));
    cr_assert_neq(item, nullptr);
}

Test(RTTI, var_makers, .init = rtti_setup, .fini = rtti_teardown)
{
    ma::RTTIPtr rtti = ma::Item::CreateItem<ma::RTTI>();
    auto root = ma::Item::GetItem(ma::Item::Key::GetRoot());
    cr_assert(root->HasItem(rtti->GetKey(), ma::Item::SearchMod::LOCAL));

    auto def_maker = ma::Item::CreateItem<ma::DefineVarMaker>({rtti->GetKey()}); // 1
    cr_assert(rtti->HasItem(def_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto str_maker = ma::Item::CreateItem<ma::StringVarMaker>({rtti->GetKey()}); // 2
    cr_assert(rtti->HasItem(str_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto bool_maker = ma::Item::CreateItem<ma::BoolVarMaker>({rtti->GetKey()}); // 3
    cr_assert(rtti->HasItem(bool_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto str_set_maker = ma::Item::CreateItem<ma::StringHashSetVarMaker>({rtti->GetKey()}); // 4
    cr_assert(rtti->HasItem(str_set_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto int_maker = ma::Item::CreateItem<ma::IntVarMaker>({rtti->GetKey()}); // 5
    cr_assert(rtti->HasItem(int_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto long_maker = ma::Item::CreateItem<ma::LongVarMaker>({rtti->GetKey()}); // 6
    cr_assert(rtti->HasItem(long_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto longlong_maker = ma::Item::CreateItem<ma::LongLongVarMaker>({rtti->GetKey()}); // 7
    cr_assert(rtti->HasItem(longlong_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto u_maker = ma::Item::CreateItem<ma::UVarMaker>({rtti->GetKey()}); // 8
    cr_assert(rtti->HasItem(u_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto ulong_maker = ma::Item::CreateItem<ma::ULongVarMaker>({rtti->GetKey()}); // 9
    cr_assert(rtti->HasItem(ulong_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto ulonglong_maker = ma::Item::CreateItem<ma::ULongLongVarMaker>({rtti->GetKey()}); // 10
    cr_assert(rtti->HasItem(ulonglong_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto float_maker = ma::Item::CreateItem<ma::FloatVarMaker>({rtti->GetKey()}); // 11
    cr_assert(rtti->HasItem(float_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto double_maker = ma::Item::CreateItem<ma::DoubleVarMaker>({rtti->GetKey()}); // 12
    cr_assert(rtti->HasItem(double_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto longdouble_maker = ma::Item::CreateItem<ma::LongDoubleVarMaker>({rtti->GetKey()}); // 13
    cr_assert(rtti->HasItem(longdouble_maker->GetKey(), ma::Item::SearchMod::LOCAL));

    auto makers = rtti->GetRegisteredMakerVars();
    cr_assert_eq(makers.size(), 13u);
    cr_assert_neq(makers.find(L"DefineVar"), makers.end());
    cr_assert_neq(makers.find(L"StringVar"), makers.end());
    cr_assert_neq(makers.find(L"BoolVar"), makers.end());
    cr_assert_neq(makers.find(L"StringHashSetVar"), makers.end());
    cr_assert_neq(makers.find(L"IntVar"), makers.end());
    cr_assert_neq(makers.find(L"UVar"), makers.end());
    cr_assert_neq(makers.find(L"ULongVar"), makers.end());
    cr_assert_neq(makers.find(L"ULongLongVar"), makers.end());
    cr_assert_neq(makers.find(L"FloatVar"), makers.end());
    cr_assert_neq(makers.find(L"DoubleVar"), makers.end());
    cr_assert_neq(makers.find(L"LongDoubleVar"), makers.end());
}

template<typename T_Maker>
void rtti_number_init()
{
    rtti_setup();

    ma::RTTIPtr rtti = ma::Item::CreateItem<ma::RTTI>();

    auto maker = ma::Item::CreateItem<T_Maker>({rtti->GetKey()});
    cr_assert(rtti->HasItem(maker->GetKey(), ma::Item::SearchMod::LOCAL));
    cr_assert(maker->HasVar(ma::MakerVar::Key::GetClassNameVarProperty()));

    //ma::StringVarPtr class_name_var = std::dynamic_pointer_cast<ma::StringVar>(maker->GetVar(ma::MakerVar::ms_KeyClassNameVarProperty));
    //ma::StringVarPtr class_name_var = static_cast<ma::Item*>(maker.get())->GetVar<ma::StringVar>(ma::MakerVar::ms_KeyClassNameVarProperty);
    ma::ItemPtr item_maker = std::static_pointer_cast<ma::Item>(maker); //Pourquoi faut-il convertir le maker en item?
    ma::StringVarPtr class_name_var = item_maker->GetVar<ma::StringVar>(ma::MakerVar::Key::GetClassNameVarProperty());
    cr_assert_eq(class_name_var->Get(), T_Maker::GetClassName());
    cr_assert_eq(class_name_var->toString(), T_Maker::GetClassName());

    auto item = ma::Item::CreateItem<ma::Item>();

    std::shared_ptr<typename T_Maker::value_type> var;
    cr_assert_none_throw(var = std::dynamic_pointer_cast<typename T_Maker::value_type>(rtti->CreateVar(T_Maker::GetClassName(), L"m_Var", item->GetKey())));

    cr_assert_eq(var->GetTypeInfo().m_ClassName, T_Maker::GetClassName());
    cr_assert_eq(var->Get(), 0);
}

Test(RTTI, var_int_maker, .init = rtti_number_init<ma::IntVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_long_maker, .init = rtti_number_init<ma::LongVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_longlong_maker, .init = rtti_number_init<ma::LongLongVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_u_maker, .init = rtti_number_init<ma::UVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_ulong_maker, .init = rtti_number_init<ma::ULongVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_ulonglong_maker, .init = rtti_number_init<ma::ULongLongVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_float_maker, .init = rtti_number_init<ma::FloatVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_double_maker, .init = rtti_number_init<ma::DoubleVarMaker>, .fini = rtti_teardown){}
Test(RTTI, var_longdouble_maker, .init = rtti_number_init<ma::LongDoubleVarMaker>, .fini = rtti_teardown){}
