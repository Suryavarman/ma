/// \file Item.ut.cpp
/// \author Pontier Pierre
/// \date 2019-11-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires de la classe Item.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

Test(Item, id_no_parent)
{
    ma::Id::ClearKeys();

    ma::Id id(ma::Item::Key::GetNoParent());
    cr_assert_eq(id, ma::Item::Key::GetNoParent());
    cr_assert_neq(id, ma::Item::Key::GetRoot());

    ma::Id::ClearKeys();
}

Test(Item, id_root)
{
    ma::Id::ClearKeys();

    ma::Id id(ma::Item::Key::GetRoot());
    cr_assert_eq(id, ma::Item::Key::GetRoot());
    cr_assert_neq(id, ma::Item::Key::GetNoParent());

    // Double parenthèses pour éviter que Clang considère ma::Item::Key::ms_Root
    // comme une nouvelle déclaration de celui-ci.

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw((ma::Id(ma::Item::Key::GetRoot())), std::logic_error);
    ma::g_ConsoleVerbose = 1u;

    ma::Id::ClearKeys();
}

Test(Item, root)
{
    ma::Id::ClearKeys();

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(ma::Item::GetRoot(), std::logic_error);
    ma::g_ConsoleVerbose = 1u;

    {
        ma::ItemPtr root;
        cr_assert_none_throw(root = ma::Item::CreateRoot<ma::Item>());

        ma::g_ConsoleVerbose = 0;
        cr_assert_any_throw(ma::Item::CreateRoot<ma::Item>());
        ma::g_ConsoleVerbose = 1u;

        cr_assert_eq(root->GetKey(), ma::Item::Key::GetRoot());
        cr_assert_eq(ma::Item::GetCount(), 1u);
        cr_assert_eq(ma::Item::GetItems()[ma::Item::Key::GetRoot()], root);
    }
    cr_assert_eq(ma::Item::GetCount(), 1u);
    cr_assert_neq(ma::Item::GetItems()[ma::Item::Key::GetRoot()], nullptr);

    ma::g_ConsoleVerbose = 0;
    cr_assert_any_throw(ma::Item::EraseItem(ma::Item::Key::GetRoot()));
    ma::g_ConsoleVerbose = 1u;

    cr_assert_none_throw(ma::Item::EraseRoot());

    cr_assert_eq(ma::Item::GetCount(), 0u);

    ma::Id::ClearKeys();
}

void item_setup()
{
    ma::g_ConsoleVerbose = 1u;
    ma::Item::CreateRoot<ma::Item>();
}

void item_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(Item, create_item_with_no_parent, .init = item_setup, .fini = item_teardown)
{
    cr_assert_eq(ma::Item::GetCount(), 1u);
    ma::ItemPtr item = ma::Item::CreateItem<ma::Item>({ma::Item::Key::GetNoParent()});
    cr_assert_eq(ma::Item::GetCount(), 1u);

    auto root = ma::Item::GetRoot();
    root->AddChild(item);
    cr_assert_eq(ma::Item::GetCount(), 2u);
    cr_assert_eq(root->GetCount(ma::Item::SearchMod::LOCAL), 1u);
}

Test(Item, default_constructor_and_destructor, .init = item_setup, .fini = item_teardown)
{
    auto root = ma::Item::GetRoot();

    ma::Item::key_type key;
    cr_assert_eq(ma::Item::GetCount(), 1u);
    {
        auto item = ma::Item::CreateItem<ma::Item>();
        key = item->GetKey();

        cr_assert_eq(ma::Item::GetCount(), 2u);
        cr_assert(ma::Item::HasItem(key));
        cr_assert(root->HasItem(key, ma::Item::SearchMod::LOCAL));
        cr_assert_eq(root->GetItem(key, ma::Item::SearchMod::LOCAL), item);
        cr_assert_eq(item->GetParentKey(), ma::Item::Key::GetRoot());
    }

    cr_assert_eq(ma::Item::GetCount(), 2u);
    cr_assert(ma::Item::HasItem(key));
    cr_assert_neq(root->GetItem(key, ma::Item::SearchMod::LOCAL), nullptr);

    ma::Item::EraseItem(key);

    //cr_assert_none_throw(ma::Item::EraseItem(key));
    ma::g_ConsoleVerbose = 0;
    cr_assert_any_throw(ma::Item::EraseItem(key));
    ma::g_ConsoleVerbose = 1u;
    cr_assert_eq(ma::Item::GetCount(), 1u);
}

Test(Item, constructors, .init = item_setup, .fini = item_teardown)
{
    auto parent = ma::Item::CreateItem<ma::Item>();
    const auto key = parent->GetKey();
    const auto call_construction_end = true;
    const auto can_be_delete_on_load = true;
    const auto child_key = L"9ea58d38-17e7-4c67-b25f-9e3b16620550"s;

    auto child = ma::Item::CreateItem<ma::Item>({key});
    cr_assert_eq(child->GetParentKey(), key);
    cr_assert_eq(child->GetEndConstruction(), true);
    cr_assert_eq(child->m_CanBeDeleteOnLoad, true);

    child = ma::Item::CreateItem<ma::Item>({key, call_construction_end});
    cr_assert_eq(child->GetParentKey(), key);
    cr_assert_eq(child->GetEndConstruction(), call_construction_end);
    cr_assert_eq(child->m_CanBeDeleteOnLoad, true);

    child = ma::Item::CreateItem<ma::Item>({key, call_construction_end, can_be_delete_on_load});
    cr_assert_eq(child->GetParentKey(), key);
    cr_assert_eq(child->GetEndConstruction(), call_construction_end);
    cr_assert_eq(child->m_CanBeDeleteOnLoad, can_be_delete_on_load);

    child = ma::Item::CreateItem<ma::Item>({key, call_construction_end, can_be_delete_on_load, child_key});
    cr_assert_eq(child->GetParentKey(), key);
    cr_assert_eq(child->GetEndConstruction(), call_construction_end);
    cr_assert_eq(child->m_CanBeDeleteOnLoad, can_be_delete_on_load);
    cr_assert_eq(child->GetKey(), child_key);

    ma::Item::EraseItem(key);
}

Test(Item, items_map_management, .init = item_setup, .fini = item_teardown)
{
    auto root = ma::Item::GetRoot();

    ma::Item::key_type key;
    {
        auto item = ma::Item::CreateItem<ma::Item>();
        key = item->GetKey();
        cr_assert(!ma::Id::IsUnique(key));
        cr_assert_eq(ma::Item::GetCount(), 2u);
        cr_assert_eq(item->GetParentKey(), ma::Item::Key::GetRoot());

        ma::ItemPtr item_bis = ma::Item::GetItem(key);
        cr_assert_eq(item, item_bis);
        cr_assert_eq(ma::Item::GetCount(), 2u);
    }
    cr_assert_eq(ma::Item::GetCount(), 2u);

    {
        ma::ItemPtr item = ma::Item::GetItem(key);
        cr_assert_not_null(item);
    }

    cr_assert_eq(ma::Item::GetCount(), 2u);

    cr_assert_none_throw(ma::Item::EraseItem(key));
    cr_assert_eq(ma::Item::GetCount(), 1u);
    cr_assert_eq(ma::Item::GetRoot(), root);

    cr_assert_not(ma::Item::HasItem(key));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(ma::Item::GetItem(key), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Item, has_item, .init = item_setup, .fini = item_teardown)
{
    auto root = ma::Item::GetRoot();

    ma::ItemPtr* itemPtr = nullptr;
    {
        ma::ItemPtr item = ma::Item::CreateItem<ma::Item>();
        itemPtr = new ma::ItemPtr(item);

        auto key = item->GetKey();
        auto child = ma::Item::CreateItem<ma::Item>(key);

        cr_assert(item->HasItem(child->GetKey(), ma::Item::SearchMod::LOCAL));
        cr_assert(item->HasItem(child->GetKey(), ma::Item::SearchMod::RECURSIVE));
        cr_assert(item->HasItem(child->GetKey(), ma::Item::SearchMod::GLOBAL));

        cr_assert(root->HasItem(key, ma::Item::SearchMod::LOCAL));

        cr_assert(!root->HasItem(child->GetKey(), ma::Item::SearchMod::LOCAL));
        cr_assert(root->HasItem(child->GetKey(), ma::Item::SearchMod::RECURSIVE));
        cr_assert(root->HasItem(child->GetKey(), ma::Item::SearchMod::GLOBAL));

        cr_assert_eq(ma::Item::GetCount(), 3u);

        cr_assert_none_throw(ma::Item::EraseItem(key));

        cr_assert_eq(ma::Item::GetCount(), 1u);
    }
    cr_assert_none_throw(delete itemPtr);
}

Test(Item, branch, .init = item_setup, .fini = item_teardown)
{
    auto item1_lvl1 = ma::Item::CreateItem<ma::Item>();
    auto item1_lvl2 = ma::Item::CreateItem<ma::Item>({item1_lvl1->GetKey()});
    auto item2_lvl2 = ma::Item::CreateItem<ma::Item>({item1_lvl1->GetKey()});
    auto item1_lvl3 = ma::Item::CreateItem<ma::Item>({item1_lvl2->GetKey()});
    auto item2_lvl3 = ma::Item::CreateItem<ma::Item>({item1_lvl2->GetKey()});
    auto item1_lvl4 = ma::Item::CreateItem<ma::Item>({item1_lvl3->GetKey()});

    cr_assert_eq(item1_lvl1->GetItems(ma::Item::SearchMod::LOCAL).size(), 2u);
    cr_assert_eq(item1_lvl2->GetItems(ma::Item::SearchMod::LOCAL).size(), 2u);
    cr_assert_eq(item1_lvl3->GetItems(ma::Item::SearchMod::LOCAL).size(), 1u);
    cr_assert_eq(item2_lvl3->GetItems(ma::Item::SearchMod::LOCAL).size(), 0);
    cr_assert_eq(item1_lvl4->GetItems(ma::Item::SearchMod::LOCAL).size(), 0);

    cr_assert(item1_lvl1->IsObservable());
    cr_assert(item1_lvl2->IsObservable());
    cr_assert(item2_lvl2->IsObservable());
    cr_assert(item1_lvl3->IsObservable());
    cr_assert(item2_lvl3->IsObservable());
    cr_assert(item1_lvl4->IsObservable());

    cr_assert_eq(item1_lvl1->GetItems(ma::Item::SearchMod::LOCAL).size(), 2u);

    ma::Item::EraseItem(item1_lvl2->GetKey());

    cr_assert(item1_lvl1->IsObservable());
    cr_assert_not(item1_lvl2->IsObservable());
    cr_assert(item2_lvl2->IsObservable());
    cr_assert_not(item1_lvl3->IsObservable());
    cr_assert_not(item2_lvl3->IsObservable());
    cr_assert_not(item1_lvl4->IsObservable());

    auto items = item1_lvl1->GetItems(ma::Item::SearchMod::LOCAL);
    cr_assert_eq(items.size(), 1u);
    cr_assert_eq(items.begin()->second, item2_lvl2);
}

Test(Item, parenting, .init = item_setup, .fini = item_teardown)
{
    auto root = ma::Item::GetRoot();

    ma::Item::key_type key,
                        parent_key,
                        grand_parent_key;
    ma::ItemPtr* itemPtr = nullptr;
    cr_assert_eq(ma::Item::GetCount(), 1u);
    {
        {
            auto parent_item = ma::Item::CreateItem<ma::Item>();
            cr_assert_eq(parent_item->GetParentKey(), ma::Item::Key::GetRoot());
            parent_key = parent_item->GetKey();

            auto item = ma::Item::CreateItem<ma::Item>({parent_key});
            itemPtr = new ma::ItemPtr(item);
            cr_assert_neq(item, nullptr);
            cr_assert_eq(ma::Item::GetCount(), 3u);

            key = item->GetKey();
            cr_assert_eq(item->GetParentKey(), parent_key);
            cr_assert(parent_item->HasItem(key, ma::Item::SearchMod::LOCAL));

            {
                ma::ItemPtr item_bis = ma::Item::GetItem(key);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 3u);
            }
            cr_assert_eq(ma::Item::GetCount(), 3u);

            {
                ma::ItemPtr item_bis = parent_item->GetItem(key);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 3u);
            }
            cr_assert_eq(ma::Item::GetCount(), 3u);

            {
                ma::ItemPtr item_bis = parent_item->GetItem(key, ma::Item::SearchMod::GLOBAL);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 3u);
            }
            cr_assert_eq(ma::Item::GetCount(), 3u);

            {
                ma::ItemPtr item_bis = parent_item->GetItem(key, ma::Item::SearchMod::LOCAL);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 3u);
            }
            cr_assert_eq(ma::Item::GetCount(), 3u);

            {
                ma::ItemPtr item_bis = parent_item->GetItem(key, ma::Item::SearchMod::RECURSIVE);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 3u);
            }
            cr_assert_eq(ma::Item::GetCount(), 3u);

            auto grand_parent_item = ma::Item::CreateItem<ma::Item>();
            grand_parent_key = grand_parent_item->GetKey();
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = ma::Item::GetItem(key);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = grand_parent_item->GetItem(key);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = grand_parent_item->GetItem(key, ma::Item::SearchMod::GLOBAL);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                cr_assert_not(grand_parent_item->HasItem(key, ma::Item::SearchMod::LOCAL));

                ma::g_ConsoleVerbose = 0;
                cr_assert_throw(grand_parent_item->GetItem(key, ma::Item::SearchMod::LOCAL), std::invalid_argument);
                ma::g_ConsoleVerbose = 1u;

                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                cr_assert_not(grand_parent_item->HasItem(key, ma::Item::SearchMod::RECURSIVE));

                ma::g_ConsoleVerbose = 0;
                cr_assert_throw(grand_parent_item->GetItem(key, ma::Item::SearchMod::RECURSIVE), std::invalid_argument);
                ma::g_ConsoleVerbose = 1u;

                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            grand_parent_item->AddChild(parent_key);
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = ma::Item::GetItem(key);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = grand_parent_item->GetItem(key);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = grand_parent_item->GetItem(key, ma::Item::SearchMod::GLOBAL);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                cr_assert_not(grand_parent_item->HasItem(key, ma::Item::SearchMod::LOCAL));

                ma::g_ConsoleVerbose = 0;
                cr_assert_throw(grand_parent_item->GetItem(key, ma::Item::SearchMod::LOCAL), std::invalid_argument);
                ma::g_ConsoleVerbose = 1u;

                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = grand_parent_item->GetItem(key, ma::Item::SearchMod::RECURSIVE);
                cr_assert_eq(item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);


            {
                ma::ItemPtr item_bis = grand_parent_item->GetItem(parent_key, ma::Item::SearchMod::LOCAL);
                cr_assert_eq(parent_item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                ma::ItemPtr item_bis = grand_parent_item->GetItem(parent_key, ma::Item::SearchMod::RECURSIVE);
                cr_assert_eq(parent_item, item_bis);
                cr_assert_eq(ma::Item::GetCount(), 4u);
            }
            cr_assert_eq(ma::Item::GetCount(), 4u);

            {
                auto items = item->GetItems(ma::Item::SearchMod::RECURSIVE);

                cr_assert_eq(items.size(), 0u);
            }

            {
                auto items = parent_item->GetItems(ma::Item::SearchMod::RECURSIVE);

                cr_assert_eq(items.size(), 1u);
                cr_assert_neq(items.find(key), items.end());
                cr_assert_eq(items.find(parent_key), items.end());
                cr_assert_eq(items.find(grand_parent_key), items.end());
                cr_assert_eq(items.find(ma::Item::Key::GetRoot()), items.end());
            }

            {
                auto items = grand_parent_item->GetItems(ma::Item::SearchMod::RECURSIVE);

                cr_assert_eq(items.size(), 2u);
                cr_assert_neq(items.find(key), items.end());
                cr_assert_neq(items.find(parent_key), items.end());
                cr_assert_eq(items.find(grand_parent_key), items.end());
                cr_assert_eq(items.find(ma::Item::Key::GetRoot()), items.end());
            }

            {
                auto items = root->GetItems(ma::Item::SearchMod::RECURSIVE);

                cr_assert_eq(items.size(), 3u);
                cr_assert_neq(items.find(key), items.end());
                cr_assert_neq(items.find(parent_key), items.end());
                cr_assert_neq(items.find(grand_parent_key), items.end());
                cr_assert_eq(items.find(ma::Item::Key::GetRoot()), items.end());
            }

            cr_assert(parent_item->HasItem(key, ma::Item::SearchMod::LOCAL));
            cr_assert(grand_parent_item->HasItem(parent_key, ma::Item::SearchMod::LOCAL));
            cr_assert(grand_parent_item->HasItem(key, ma::Item::SearchMod::RECURSIVE));
            cr_assert(root->HasItem(key, ma::Item::SearchMod::RECURSIVE));
            cr_assert(root->HasItem(parent_key, ma::Item::SearchMod::RECURSIVE));
            cr_assert(root->HasItem(grand_parent_key, ma::Item::SearchMod::LOCAL));

            // L'item ayant pour identifiant key est possédé par item, itemPtr parent_item et ms_Map.
            // EraseItem l'effacera de ms_Map et parent_item.
            // Il ne sera donc pas détruit, car il sera encore possédé par item et itemPtr.
            cr_assert_none_throw(ma::Item::EraseItem(key));
            cr_assert_eq(ma::Item::GetCount(), 3u);
            cr_assert_not(parent_item->HasItem(key, ma::Item::SearchMod::LOCAL));
            cr_assert_not(parent_item->HasItem(key, ma::Item::SearchMod::RECURSIVE));
            cr_assert_not(parent_item->HasItem(key, ma::Item::SearchMod::GLOBAL));
            cr_assert_not(grand_parent_item->HasItem(key, ma::Item::SearchMod::RECURSIVE));
            cr_assert_not(root->HasItem(key, ma::Item::SearchMod::RECURSIVE));

        } // porté de parent_item

        cr_assert_none_throw(ma::Item::EraseItem(grand_parent_key));
        cr_assert_eq(ma::Item::GetCount(), 1u);

        auto items = ma::Item::GetItems();
        cr_assert_eq(items.find(parent_key), items.end());
    }

    cr_assert_neq(itemPtr, nullptr);
    delete itemPtr;
    cr_assert_eq(ma::Item::GetCount(), 1u);
}

class Test_1 : public ma::Item
{
    public:
        static const ma::Var::key_type ms_KeyVar_ADD;
        static const ma::Var::key_type ms_KeyVar_REMOVE;

        using ma::Item::Item;

        bool AcceptToAddChild(const ma::ItemPtr& item) const override
        {
            return item->HasVar(ms_KeyVar_ADD);
        }

        bool AcceptToRemoveChild(const key_type& key) const override
        {
            auto item = GetItem(key, ma::Item::SearchMod::LOCAL);
            return item->HasVar(ms_KeyVar_REMOVE);
        }
};

const ma::Var::key_type Test_1::ms_KeyVar_ADD = L"ADD";
const ma::Var::key_type Test_1::ms_KeyVar_REMOVE = L"REMOVE";

class Test_1_AcceptedChild : public ma::Item
{
    protected:
        using ma::Item::Item;

    public:
        ma::DefineVarPtr m_Var_ADD,
                          m_Var_REMOVE;

        explicit Test_1_AcceptedChild(const ma::Item::ConstructorParameters& params):
        Item(params),
        m_Var_ADD(AddMemberVar<ma::DefineVar>(Test_1::ms_KeyVar_ADD)),
        m_Var_REMOVE(AddMemberVar<ma::DefineVar>(Test_1::ms_KeyVar_REMOVE))
        {}

        virtual ~Test_1_AcceptedChild()
        {}
};

Test(Item, allow_or_not_to_add_and_remove_item, .init = item_setup, .fini = item_teardown)
{
    auto root = ma::Item::GetRoot();

    auto parent = ma::Item::CreateItem<Test_1>();
    auto child1_Yes = ma::Item::CreateItem<Test_1_AcceptedChild>();

    // Une bonne pratique est de ne pas lier un item à un parent, si celui-ci va être ajouté à un autre parent.
    auto child2_Yes = ma::Item::CreateItem<ma::Item>({ma::Item::Key::GetNoParent()});
    child2_Yes->AddVar<ma::DefineVar>(Test_1::ms_KeyVar_ADD);
    auto child_No = ma::Item::CreateItem<ma::Item>();

    cr_assert(parent->AcceptToAddChild(child1_Yes));
    cr_assert(parent->AcceptToAddChild(child2_Yes));
    cr_assert_not(parent->AcceptToAddChild(child_No));

    cr_assert_none_throw(parent->AddChild(child1_Yes));
    cr_assert_none_throw(parent->AddChild(child2_Yes));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(parent->AddChild(child_No), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    child_No->AddVar<ma::DefineVar>(Test_1::ms_KeyVar_ADD);
    cr_assert_none_throw(parent->AddChild(child_No));

    cr_assert(parent->HasItem(child1_Yes->GetKey(), ma::Item::SearchMod::LOCAL));
    cr_assert(parent->HasItem(child2_Yes->GetKey(), ma::Item::SearchMod::LOCAL));
    cr_assert(parent->HasItem(child_No->GetKey(), ma::Item::SearchMod::LOCAL));

    child2_Yes->AddVar<ma::DefineVar>(Test_1::ms_KeyVar_REMOVE);
    cr_assert(parent->AcceptToRemoveChild(child1_Yes->GetKey()));
    cr_assert(parent->AcceptToRemoveChild(child2_Yes->GetKey()));
    cr_assert_not(parent->AcceptToRemoveChild(child_No->GetKey()));

    cr_assert_none_throw(parent->RemoveChild(child1_Yes->GetKey()));
    cr_assert_none_throw(parent->RemoveChild(child2_Yes->GetKey()));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(parent->RemoveChild(child_No->GetKey()), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    cr_assert_not(parent->HasItem(child1_Yes->GetKey(), ma::Item::SearchMod::LOCAL));
    cr_assert_not(parent->HasItem(child2_Yes->GetKey(), ma::Item::SearchMod::LOCAL));
    cr_assert(parent->HasItem(child_No->GetKey(), ma::Item::SearchMod::LOCAL));
}

Test(Item, member_and_dynamic_var, .init = item_setup, .fini = item_teardown)
{
    auto child = ma::Item::CreateItem<Test_1_AcceptedChild>();
    cr_assert_not(child->m_Var_ADD->m_Dynamic);

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(child->EraseVar(child->m_Var_ADD->m_Key), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    cr_assert(child->HasVar(child->m_Var_ADD->m_Key));

    child->AddVar<ma::DefineVar>(L"m_DynamicVar");
    cr_assert(child->GetVar(L"m_DynamicVar")->m_Dynamic);
    cr_assert_none_throw(child->EraseVar(L"m_DynamicVar"));
}

Test(Item, on_enable_disable, .init = item_setup, .fini = item_teardown)
{
    class test_on_enable:
    public ma::Item
    {
        public:
        bool m_TestValue;
        protected:
        void OnEnable() override
        {
            ma::Item::OnEnable();
            m_TestValue = true;
        }

        void OnDisable() override
        {
            ma::Item::OnDisable();
            m_TestValue = false;
        }

        public:
        explicit test_on_enable(const Item::ConstructorParameters &in_params):
        ma::Item(in_params),
        m_TestValue(false)
        {}

        ~test_on_enable() override = default;
    };

    auto item = ma::Item::CreateItem<test_on_enable>();
    cr_assert(item->IsEnable());
    auto parent = ma::Item::GetItem(item->GetParentKey());
    parent->RemoveChild(item);
    cr_assert_not(item->m_TestValue);
    cr_assert_not(item->IsEnable());
    parent->AddChild(item);
    cr_assert(item->IsEnable());

    auto child = ma::Item::CreateItem<ma::Item>(item->GetKey());
    cr_assert(child->IsEnable());
    parent->RemoveChild(item);
    cr_assert_not(child->IsEnable());
    parent->AddChild(item);
    cr_assert(child->IsEnable());
}

Test(Item, remove_non_observable_item, .init = item_setup, .fini = item_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto child = ma::Item::CreateItem<ma::Item>(item->GetKey());

    auto root = ma::Item::GetRoot();
    root->RemoveChild(item);
    cr_assert_not(item->IsObservable());
    cr_assert_not(child->IsObservable());

    item->RemoveChild(child);
    cr_assert_eq(child->GetParentKey(), ma::Item::Key::GetNoParent());
}

Test(Item, get_parent_item, .init = item_setup, .fini = item_teardown)
{
    auto parent = ma::Item::CreateItem<ma::Item>();
    auto child = ma::Item::CreateItem<ma::Item>(parent->GetKey());

    cr_assert_eq(child->GetParent(), parent);

    auto root = ma::Item::GetRoot();
    cr_assert_any_throw(root->GetParent());
}