/// \file Enable.ut.cpp
/// \author Pontier Pierre
/// \date 2019-11-18
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires de la classe « Error ».
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

Test(Error, calling_throw)
{
    std::wstring function_name(L"Error::calling_throw");

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(MA_ASSERT(false, L"calling_throw1"), std::logic_error);
    cr_assert_throw(MA_ASSERT(false, L"calling_throw2", std::logic_error), std::logic_error);
    cr_assert_throw(MA_ASSERT(false, L"calling_throw3", std::invalid_argument), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Error, message)
{
    std::wstring message(L"message"),
                 function_name(L"function_name"),
                 filename(L"Error.ut.cpp"),
                 line(std::to_wstring(__LINE__ + 7u));

    std::wstring what;
    MA_MSG(L"=================================================="s);
    MA_MSG(L"Message d'erreur voulu: Le message va être tester."s);
    try
    {
        MA_ASSERT(false, message, function_name, std::logic_error);
    }
    catch(std::exception& e)
    {
        what = ma::to_wstring(e.what());
    }
    MA_MSG(L"Fin du message d'erreur."s);
    MA_MSG(L"=================================================="s);

    std::wstringstream stream(what);
    std::vector<std::wstring> lines;
    std::wstring str;
    while(std::getline(stream, str))
    {
        lines.push_back(str);
    }

    cr_assert_eq(lines.size(), 5u);

    auto line_1 = lines[1];
    cr_assert_neq(line_1.find(message), std::wstring::npos);

    auto line_2 = lines[2];
    cr_assert_neq(line_2.find(function_name), std::wstring::npos);

    auto line_3 = lines[3];
    cr_assert_neq(line_3.find(filename), std::wstring::npos);

    auto line_4 = lines[4];
    cr_assert_neq(line_4.find(line), std::wstring::npos);
}
