/// \file Algorithm.cpp
/// \author Pontier Pierre
/// \date 2020-03-22
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Algorithm.h"

#include <sstream>
#include <algorithm>
#include <cwctype>

namespace ma
{
    std::vector<std::string> Split(const std::string &s, char delimiter)
    {
        std::vector<std::string> tokens;
        std::string token;
        std::istringstream tokenStream(s);
        while(std::getline(tokenStream, token, delimiter))
        {
            tokens.push_back(token);
        }
        return tokens;
    }

    // https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
    std::vector<std::wstring> Split(const std::wstring &s, wchar_t delimiter)
    {
        std::vector<std::wstring> tokens;
        std::wstring token;
        std::wistringstream tokenStream(s);
        while(std::getline(tokenStream, token, delimiter))
        {
            tokens.push_back(token);
        }
        return tokens;
    }

    //    std::vector<std::string> Split(const std::string& s, char delimiter)
    //    {
    //       std::vector<std::string> tokens;
    //       std::string token;
    //       std::istringstream tokenStream(s);
    //       while (std::getline(tokenStream, token, delimiter))
    //       {
    //          tokens.push_back(token);
    //       }
    //       return tokens;
    //    }

    // L'\u2003' ?
    const std::wstring only_blank_chars{L" \t"};
    const std::wstring not_only_blank_chars{L" \t\n\f\r\v"};

    // La fonction « trim » quitte tous les espaces.
    // https://stackoverflow.com/questions/83439/remove-spaces-from-stdstring-in-c
    // https://en.cppreference.com/w/cpp/algorithm/remove
    std::wstring Trim(const std::wstring &str, bool only_blank)
    {
        std::wstring trim = str;

        if(only_blank)
            trim.erase(std::remove_if(trim.begin(),
                                      trim.end(),
                                      [](std::wstring::value_type x)
                                      {
                                          return std::iswblank(x);
                                      }),
                       trim.end());
        else
            trim.erase(std::remove_if(trim.begin(),
                                      trim.end(),
                                      [](std::wstring::value_type x)
                                      {
                                          return std::iswspace(x);
                                      }),
                       trim.end());

        return trim;
    }

    std::wstring TrimStart(const std::wstring &str, bool only_blank)
    {
        std::wstring_view trim_begin{str};
        const std::wstring filter{only_blank ? only_blank_chars : not_only_blank_chars};

        trim_begin.remove_prefix(std::min(trim_begin.find_first_not_of(filter), trim_begin.size()));

        return std::wstring{trim_begin};
    }

    std::wstring TrimEnd(const std::wstring &str, bool only_blank)
    {
        std::wstring_view trim_end{str};
        const std::wstring filter{only_blank ? only_blank_chars : not_only_blank_chars};

        trim_end.remove_suffix(
            trim_end.size() -
            std::min(std::max(static_cast<size_t>(0u), trim_end.find_last_not_of(filter) + 1u), trim_end.size()));

        return std::wstring{trim_end};
    }

    std::wstring TrimStartEnd(const std::wstring &str, bool only_blank)
    {
        std::wstring_view trim = str;
        const std::wstring filter{only_blank ? only_blank_chars : not_only_blank_chars};

        trim.remove_prefix(std::min(trim.find_first_not_of(filter), trim.size()));
        trim.remove_suffix(
            trim.size() - std::min(std::max(static_cast<size_t>(0u), trim.find_last_not_of(filter) + 1u), trim.size()));

        return std::wstring{trim};
    }

    std::wstring_view TrimStart(const std::wstring_view &str, bool only_blank)
    {
        std::wstring_view trim_begin = str;
        const std::wstring filter{only_blank ? only_blank_chars : not_only_blank_chars};

        trim_begin.remove_prefix(std::min(trim_begin.find_first_not_of(filter), trim_begin.size()));

        return trim_begin;
    }

    std::wstring_view TrimEnd(const std::wstring_view &str, bool only_blank)
    {
        std::wstring_view trim_end = str;
        const std::wstring filter{only_blank ? only_blank_chars : not_only_blank_chars};

        trim_end.remove_suffix(
            trim_end.size() -
            std::min(std::max(static_cast<size_t>(0u), trim_end.find_last_not_of(filter) + 1u), trim_end.size()));

        return trim_end;
    }

    std::wstring_view TrimStartEnd(const std::wstring_view &str, bool only_blank)
    {
        std::wstring_view trim = str;
        const std::wstring filter{only_blank ? only_blank_chars : not_only_blank_chars};

        trim.remove_prefix(std::min(trim.find_first_not_of(filter), trim.size()));
        trim.remove_suffix(
            trim.size() - std::min(std::max(static_cast<size_t>(0u), trim.find_last_not_of(filter) + 1u), trim.size()));

        return trim;
    }

    // https://en.cppreference.com/w/cpp/string/basic_string/replace
    std::size_t ReplaceAll(std::wstring &inout, std::wstring_view what, std::wstring_view with)
    {
        std::size_t count{};
        for(std::wstring::size_type pos{}; std::wstring::npos != (pos = inout.find(what.data(), pos, what.length()));
            pos += with.length(), ++count)
        {
            inout.replace(pos, what.length(), with.data(), with.length());
        }
        return count;
    }

    // https://en.cppreference.com/w/cpp/string/basic_string/replace
    std::size_t RemoveAll(std::wstring &inout, std::wstring_view what)
    {
        return ma::ReplaceAll(inout, what, L"");
    }
} // namespace ma
