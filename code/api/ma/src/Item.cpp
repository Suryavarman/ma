/// \file Item.cpp
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Item.h"
#include "ma/Variable.h"
#include "ma/ObservationManager.h"
#include "ma/RTTI.h"
#include "ma/ClassInfo.h"
#include "ma/Error.h"
#include "ma/Macros.h"
#include "ma/Garbage.h"

#include <sstream>
#include <algorithm>
#include <vector>

#define MA_STATIC_GETTER(in_M_type, in_M_name)                                                                         \
    {                                                                                                                  \
        static const in_M_type result{ma::to_wstring(in_M_name)};                                                      \
        return result;                                                                                                 \
    }

using namespace std::literals;

namespace ma
{
    ItemsMap Item::ms_Map;

    Item::mutex Item::ms_MapMutex;

    // Clefs réservées des items //—————————————————————————————————————————————————————————————————————————————————————
    // clang-format off

    // Pour permettre à la description du type « Item » d'accéder aux clefs, même si elles ne sont pas encore assignées.
    const Item::key_type& Item::Key::GetNoParent()           MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000001")
    const Item::key_type& Item::Key::GetRoot()               MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000002")
    const Item::key_type& Item::Key::GetObservationManager() MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000003")
    const Item::key_type& Item::Key::GetClassInfoManager()   MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000004")
    const Item::key_type& Item::Key::GetRTTI()               MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000005")
    const Item::key_type& Item::Key::GetResourceManager()    MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000006")
    const Item::key_type& Item::Key::GetContextManager()     MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000007")
    const Item::key_type& Item::Key::GetGuiAccess()          MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000008")
    const Item::key_type& Item::Key::GetwxParameterManager() MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000009")
    const Item::key_type& Item::Key::GetGarbage()            MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000010")
    const Item::key_type& Item::Key::GetConfig()             MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000011")
    const Item::key_type& Item::Key::GetModuleManager()      MA_STATIC_GETTER(Item::key_type, "00000000-0000-0000-0000-000000000012")

    // Clefs réservées des variables d'items //—————————————————————————————————————————————————————————————————————————
    const Var::key_type &Item::Key::Var::GetName()           MA_STATIC_GETTER(ma::VarsMap::key_type, "m_Name")
    const Var::key_type &Item::Key::Var::GetDoc()            MA_STATIC_GETTER(ma::VarsMap::key_type, "__doc__")
    const Var::key_type &Item::Key::Var::GetSavePath()       MA_STATIC_GETTER(ma::VarsMap::key_type, "m_SavePath")
    const Var::key_type &Item::Key::Var::GetLastSavePath()   MA_STATIC_GETTER(ma::VarsMap::key_type, "m_LastSavePath")
    const Var::key_type &Item::Key::Var::GetCustomSavePath() MA_STATIC_GETTER(ma::VarsMap::key_type, "m_CustomSavePath")
    const Var::key_type &Item::Key::Var::GetDependencies()   MA_STATIC_GETTER(ma::VarsMap::key_type, "m_Dependencies")

    // Clefs réservées des observations //——————————————————————————————————————————————————————————————————————————————
    const Observable::Data::key_type Item::Key::Obs::ms_AddItemBegin(L"add_item_begin");
    const Observable::Data::key_type Item::Key::Obs::ms_AddItemEnd(L"add_item_end");
    const Observable::Data::key_type Item::Key::Obs::ms_RemoveItemBegin(L"rm_item_begin");
    const Observable::Data::key_type Item::Key::Obs::ms_RemoveItemEnd(L"rm_item_end");
    const Observable::Data::key_type Item::Key::Obs::ms_AddVar(L"add_var");
    const Observable::Data::key_type Item::Key::Obs::ms_RemoveVarBegin(L"rm_var_begin");
    const Observable::Data::key_type Item::Key::Obs::ms_RemoveVarEnd(L"rm_var_end");
    const Observable::Data::key_type Item::Key::Obs::ms_ConstructionEnd(L"item_end_construction");
    const Observable::Data::key_type Item::Key::Obs::ms_GlobalAddItemBegin(L"global_add_item_begin");
    const Observable::Data::key_type Item::Key::Obs::ms_GlobalAddItemEnd(L"global_add_item_end");
    const Observable::Data::key_type Item::Key::Obs::ms_GlobalRemoveItemBegin(L"global_rm_item_begin");
    const Observable::Data::key_type Item::Key::Obs::ms_GlobalRemoveItemEnd(L"global_rm_item_end");

    // clang-format on

    // ConstructorParameters //—————————————————————————————————————————————————————————————————————————————————————————
    // #if MA_COMPILER == MA_COMPILER_CLANG
    //     Item::ConstructorParameters::ConstructorParameters(const Item::key_type& parent_key,
    //                                                        const Item::key_type& key,
    //                                                        bool savable,
    //                                                        bool can_be_delete_on_load):
    //     m_ParentKey{parent_key},
    //     m_Key{key},
    //     m_Savable{savable},
    //     m_CanBeDeleteOnLoad{can_be_delete_on_load}
    //     {}
    //
    //     Item::ConstructorParameters::ConstructorParameters(const ConstructorParameters &params,
    //                                                        const Item::key_type& key,
    //                                                        bool savable,
    //                                                        bool can_be_delete_on_load):
    //     m_ParentKey{params.m_ParentKey},
    //     m_Key{key},
    //     m_Savable{savable},
    //     m_CanBeDeleteOnLoad{can_be_delete_on_load}
    //     {}
    //
    // #else

    Item::ConstructorParameters::ConstructorParameters(Item::key_type parent_key,
                                                       Item::key_type key,
                                                       bool savable,
                                                       bool can_be_delete_on_load):
    m_ParentKey{std::move(parent_key)},
    m_Key{std::move(key)},
    m_Savable{savable},
    m_CanBeDeleteOnLoad{can_be_delete_on_load}
    {}

    Item::ConstructorParameters::ConstructorParameters(const ConstructorParameters &params,
                                                       Item::key_type key,
                                                       bool savable,
                                                       bool can_be_delete_on_load):
    m_ParentKey{params.m_ParentKey},
    m_Key{std::move(key)},
    m_Savable{savable},
    m_CanBeDeleteOnLoad{can_be_delete_on_load}
    {}

    // #endif

    Item::ConstructorParameters::ConstructorParameters(const ConstructorParameters &params,
                                                       bool savable,
                                                       bool can_be_delete_on_load):
    m_ParentKey{params.m_ParentKey},
    m_Key{params.m_Key},
    m_Savable{savable},
    m_CanBeDeleteOnLoad{can_be_delete_on_load}
    {}

    // CreateParameters //——————————————————————————————————————————————————————————————————————————————————————————————
    Item::CreateParameters::CreateParameters(Item::key_type parent_key,
                                             bool call_end_construction,
                                             bool can_be_delete_on_load,
                                             Item::key_type key):
    m_ParentKey(std::move(parent_key)),
    m_CallEndConstruction(call_end_construction),
    m_CanBeDeleteOnLoad(can_be_delete_on_load),
    m_Key(std::move(key))
    {}

    // Item //——————————————————————————————————————————————————————————————————————————————————————————————————————————
    void Item::EraseRoot()
    {
        ItemPtr item = GetItem(Key::GetRoot());

        MA_ASSERT(item->GetParentKey() == Key::GetNoParent(), L"La racine ne peut avoir de parent.");

        // On commence par avertir les observateurs avant de supprimer l'item
        // racine. Ceux-ci pourraient avoir besoin d'accéder à cet item pour
        // effectuer leurs traitements.
        if(HasItem(Key::GetObservationManager()))
        {
            auto obs_manager = GetItem<ObservationManager>(Key::GetObservationManager());
            auto items = item->GetItemsPostOrder(Item::SearchMod::RECURSIVE);

            for(const auto &it : items)
                obs_manager->RemoveObservable(it);
        }

        lock_guard mutex(ms_MapMutex);
        ms_Map.clear();
    }

    ItemPtr Item::GetRoot()
    {
        return GetItem(Key::GetRoot());
    }

    void Item::EraseItem(const key_type &key)
    {
        MA_ASSERT(key != Key::GetRoot(), L"La clef « " + key + L" » est celle de la racine.");

        MA_ASSERT(key != Key::GetNoParent(), L"La clef « " + key + L" » est égale à ms_KeyNoParent.");

        ItemPtr item = GetItem(key);

        EraseItem(item);
    }

    void Item::EraseItem(const std::shared_ptr<Item> item)
    {
        MA_ASSERT(item, L"« item » est nul.", std::invalid_argument);

        const auto key = item->GetKey();
        const auto parent_key = item->GetParentKey();

        if(parent_key != Key::GetNoParent())
        {
            GetItem(parent_key)->RemoveChild(item);
        }
        else
        {
            const bool is_enable = item->IsEnable();
            auto items = item->GetItemsPostOrder(Item::SearchMod::RECURSIVE);

            // On commence par avertir les observateurs. Car ils pourraient avoir
            // besoin d'accéder aux items pour effectuer leurs traitements.
            if(HasItem(Key::GetObservationManager()))
            {
                auto obs_manager = GetItem<ObservationManager>(Key::GetObservationManager());
                for(const auto &it : items)
                    obs_manager->RemoveObservable(it);

                obs_manager->RemoveObservable(item);
            }

            // Portée du mutex
            {
                lock_guard mutex(ms_MapMutex);
                for(const auto &it : items)
                {
                    auto child_key = it->GetKey();
                    auto it_find = ms_Map.find(child_key);
                    MA_ASSERT(it_find != ms_Map.end(),
                              L"L'item enfant «" + child_key + L"» n'est pas présent dans ms_Map.");
                    ms_Map.erase(it_find);
                }
                ms_Map.erase(key);
            }

            // Via AddTrash, nous ajoutons un Item.
            // Du coup, il faut bien s'assurer d'être en dehors de la portée du mutex de ms_Map.
            if(HasItem(Key::GetGarbage()))
            {
                if(parent_key != Key::GetGarbage())
                {
                    auto garbage = GetItem<Garbage>(Key::GetGarbage());

                    garbage->AddTrash(item);

                    for(const auto &it : items)
                        garbage->AddTrash(it);
                }
            }
            else
            {
                MA_WARNING(false,
                           L"La poubelle à « Item » n'est pas disponible. Cela impactera le chargement qui peut-être "
                           L"essaiera de créer des éléments avec un GUID toujours existant car il n'aura pas accès à "
                           L"la poubelle à « Item » pour vérifier si un « Item » possède déjà le GUID demandé.");
            }

            if(is_enable)
            {
                item->OnDisable();
                for(const auto &it : items)
                {
                    it->OnDisable();
                }
            }
        }
    }

    ItemsMap::size_type Item::GetCount()
    {
        lock_guard mutex(ms_MapMutex);
        return ms_Map.size();
    }

    Id GetNewId(const Item::key_type &key)
    {
        const auto new_key = key.empty() ? Id::GenerateKey() : key;

        if(auto opt = Item::HasItemOpt(Item::Key::GetRoot()))
        {
            auto root = opt.value();
            root->UpdateObservations({{Item::Key::Obs::ms_GlobalAddItemBegin, new_key}});
        }

        return Id(new_key);
    }

    Item::Item(const Item::ConstructorParameters &in_params):
    Observable(),
    std::enable_shared_from_this<Item>(),
    m_DeleteProcessing{false},
    m_Children{},
    m_Id(GetNewId(in_params.m_Key)),
    m_ParentKey(Key::GetNoParent()),
    m_Vars{},
    m_SavePath{nullptr},
    m_LastSavePath{nullptr},
    m_CustomSavePath{nullptr},
    m_Savable{in_params.m_Savable},
    m_CanBeDeleteOnLoad{in_params.m_CanBeDeleteOnLoad}
    {
        const auto key = m_Id.GetKey();
        const auto parent_key = in_params.m_ParentKey;
        if(parent_key != Key::GetNoParent() || key == Key::GetRoot())
        {
            { // début du contexte du mutex
                lock_guard mutex(ms_MapMutex);

                MA_ASSERT(ms_Map.find(key) == ms_Map.end(),
                          L"La clef «" + key + L"» de l'item est déjà présente dans Item::ms_Map.");

                MA_ASSERT(key != m_ParentKey,
                          L"La clef de l'item généré automatiquement est la même que m_ParentKey."
                          L"Cela ne devrait pas arriver."
                          L"Il y a un problème avec le module « crossguid ».",
                          std::runtime_error);

                ms_Map[key] = ItemPtr(this,
                                      [](auto item)
                                      {
                                          delete item;
                                      });

            } // fin du contexte du mutex
        }

        if(m_Savable)
            InitPathVars();

        // Notez bien que la clef du parent n'est pas assigné à la variable
        // m_ParentKey.
        // L'item doit être construit pour que AcceptChild fonctionne.
        // La fonction qui créé l'item doit s'assurer d'appeler AddChild du parent.
        // CreatItem fait le travail.
        // todo C'est un problème pour les items dérivant d'Item qui ont besoin de la clef du parent.
        //      Ils doivent pour le moment utiliser « params.m_ParentKey ».
        //      Ce qui est pas bien fait. Cela pourrait amener à des erreurs.
    }

    void Item::EndConstruction()
    {
        InitDependencyVar();

        // todo Normalement c'est géré dans le chargement.
        if(m_Savable)
        {
            // Nous mettons à jour les chemins qui ont été définis au début de la construction de l'item. Le nom de
            // classe contenu dans le chemin est donc Item et non le type enfant d'Item. Il faut alors mettre à jour les
            // chemins une fois la construction terminée.
            const auto save_path = GetFolderPath().wstring();

            if(m_SavePath->toString().empty())
                SetVarFromString(m_SavePath->GetKey(), save_path);

            if(m_LastSavePath->toString().empty())
                SetVarFromString(m_LastSavePath->GetKey(), save_path);
        }

        Observable::EndConstruction();

        auto parent_key = GetParentKey();

        if(Item::HasItem(parent_key))
        {
            GetItem(parent_key)->UpdateObservations({{Key::Obs::ms_ConstructionEnd, GetKey()}});
            if(!IsEnable())
                OnEnable();
        }

        if(auto opt = Item::HasItemOpt(Item::Key::GetRoot()))
        {
            auto root = opt.value();
            root->UpdateObservations({{Item::Key::Obs::ms_GlobalAddItemEnd, GetKey()}});
        }
    }

    Item::~Item()
    {
        MA_ASSERT(!m_DeleteProcessing,
                  L"L'item est déjà en cours de destruction. Auriez vous créé un shared_ptr sans garder le lien avec "
                  L"ceux de ms_Map?");

        m_DeleteProcessing = true;

        auto key = this->GetKey();

        auto children_copy = m_Children;
        // On commence par vider la liste des enfants afin d'éviter tout appel
        // en boucle.
        m_Children.clear();

        for(const auto &it : children_copy)
        {
            MA_ASSERT(it.second,
                      L"L'item enfant associé à la clef « " + it.first + L" » est nulle. Cela ne devrait pas arriver.",
                      std::runtime_error);

            it.second->m_ParentKey = Key::GetNoParent();
        }

        // Puis, on détache l'item du parent.
        if(m_ParentKey != Key::GetNoParent())
        {
            std::wstringstream ss;
            ss << L"La clef du parent n'est pas valide:\n"
               << L"ma::Item::ms_KeyNoParent = " << Key::GetNoParent() << "\n"
               << L"m_ParentKey = " << m_ParentKey << "\n"
               << L"Il est possible que « ma::Item::ms_KeyNoParent » est été altérée par un débordement mémoire."
               << std::endl;

            MA_ASSERT(Id::IsValid(m_ParentKey), ss.str(), std::runtime_error);

            // La fonction « RemoveChild » n'est pas utilisée, car elle utilise « ms_Map » et cela va influencer le
            // compteur de références de « ma::ItemPtr » qui contient « this ».
            GetItem(m_ParentKey)->m_Children.erase(key);
            this->m_ParentKey = Key::GetNoParent();
        }
    }

    ItemPtr Item::GetItem(const Item::key_type &key, SearchMod mod) const
    {
        MA_ASSERT(key != Key::GetNoParent(),
                  L"Aucun item ne peut être associé à cette clef «" + key +
                      L"» car il s'agit de la clef du parent des items n'ayant pas de parent.",
                  std::invalid_argument);

        if(key == m_Id.GetKey())
            return GetItem(key);

        if(mod == Item::SearchMod::GLOBAL)
            return GetItem(key);

        auto itFind = m_Children.find(key);
        if(itFind != m_Children.end())
        {
            auto item = itFind->second;

            MA_ASSERT(
                item, L"Aucun item correspondant à la clef « " + key + L" » n'a été trouvé", std::invalid_argument);

            return item;
        }

        if(mod == Item::SearchMod::RECURSIVE)
        {
            for(const auto &it : m_Children)
            {
                MA_ASSERT(it.second,
                          L"L'item enfant associé à la clef « " + it.first +
                              L" » est nulle."
                              L"Cela ne devrait pas arriver.",
                          std::invalid_argument);

                auto sub_child = it.second->GetItem(key, mod);

                if(sub_child)
                    return sub_child;
            }
        }

        MA_ASSERT(false, L"Aucun item correspondant à la clef " + key + L" n'a été trouvé", std::invalid_argument);

        return nullptr;
    }

    ItemPtr Item::GetItem(const Item::key_type &key)
    {
        MA_ASSERT(key != Key::GetNoParent(),
                  L"La clef à pour valeur ms_KeyNoParent « " + Key::GetNoParent() +
                      L" », cette clef ne peut faire référence à un item de ms_Map.",
                  std::invalid_argument);

        ItemPtr result;
        // portée du mutex
        {
            lock_guard mutex(ms_MapMutex);
            auto it_find = ms_Map.find(key);
            result = it_find != ms_Map.end() ? it_find->second : nullptr;
        }

        MA_ASSERT(
            result, L"La clef « " + key + L" » ne fait référence à aucun items de ms_Map.", std::invalid_argument);

        return result;
    }

    ItemsMap Item::GetItems()
    {
        lock_guard mutex(ms_MapMutex);
        return Item::ms_Map;
    }

    ItemsOrder Item::GetItemsPostOrder()
    {
        if(HasItem(Key::GetRoot()))
        {
            auto root = GetItem(Key::GetRoot());
            auto result = root->GetItemsPostOrder(SearchMod::RECURSIVE);
            result.push_back(root);
            return result;
        }
        else
            return {};
    }

    ItemsMap Item::GetItems(Item::SearchMod mod) const
    {
        ItemsMap result;

        switch(mod)
        {
            case Item::SearchMod::GLOBAL:
                result = GetItems();
                break;

            case Item::SearchMod::LOCAL:
                result = m_Children;
                break;

            case Item::SearchMod::RECURSIVE:
                result = m_Children;
                for(const auto &it : result)
                {
                    const ItemsMap items = it.second->GetItems(mod);
                    result.insert(items.begin(), items.end());
                    // todo c++17 : result.merge(it.second->GetItems(mod)); ?
                }
                break;

            default:
                MA_ASSERT(false,
                          L"Le mode de recherche des items n'est pas valide: " + std::to_wstring(mod),
                          std::invalid_argument);
                break;
        }

        return result;
    }

    ItemsOrder Item::GetItemsPostOrder(Item::SearchMod mod) const
    {
        ItemsOrder result;

        switch(mod)
        {
            case Item::SearchMod::GLOBAL:
                result = GetItemsPostOrder();
                break;

            case Item::SearchMod::LOCAL:
                // for(const auto& [key, child]: m_Children) // c++ 17
                // todo utiliser std::transform https://en.cppreference.com/w/cpp/algorithm/transform
                for(const auto &it : m_Children)
                    result.push_back(it.second);
                // std::for_each(m_Children.begin(), m_Children.end(), [](const auto& it){ result.push_back(it.second);
                // });
                break;

            case Item::SearchMod::RECURSIVE:
                // for(const auto& [key, child] : m_Children) // c++ 17
                for(const auto &it : m_Children)
                {
                    const ItemsOrder child_items = it.second->GetItemsPostOrder(mod);
                    result.insert(result.end(), child_items.begin(), child_items.end());
                    result.push_back(it.second);
                }
                break;

            default:
                MA_ASSERT(false,
                          L"Le mode de recherche des items n'est pas valide: " + std::to_wstring(mod),
                          std::invalid_argument);
                break;
        }

        return result;
    }

    bool Item::HasItem(const Item::key_type &key)
    {
        lock_guard mutex(ms_MapMutex);
        return ms_Map.find(key) != ms_Map.end();
    }

    bool Item::HasItem(const Item::key_type &key, Item::SearchMod mod) const
    {
        bool result = false;

        if(mod == Item::SearchMod::GLOBAL)
            result = HasItem(key);
        else
        {
            Item::key_type parent_key = GetKey();
            result = m_Children.find(key) != m_Children.end();

            if(!result && mod == Item::SearchMod::RECURSIVE)
            {
                auto children = GetItems(Item::SearchMod::RECURSIVE);

                auto it_find = children.find(key);
                if(it_find != children.end())
                {
                    result = true;
                    parent_key = it_find->first;
                }
            }
        }

        return result;
    }

    std::optional<ma::ItemPtr> Item::HasItemOpt(const key_type &key)
    {
        lock_guard mutex(ms_MapMutex);
        auto it_find = ms_Map.find(key);
        if(it_find != ms_Map.end())
            return it_find->second;
        else
            return {};
    }

    std::optional<ma::ItemPtr> Item::HasItemOpt(const key_type &key, SearchMod mod) const
    {
        std::optional<ma::ItemPtr> result;

        if(mod == Item::SearchMod::GLOBAL)
            result = HasItemOpt(key);
        else
        {
            auto it_find = m_Children.find(key);
            if(it_find != m_Children.end())
            {
                result = it_find->second;
            }
            else if(mod == Item::SearchMod::RECURSIVE)
            {
                auto children = GetItems(Item::SearchMod::RECURSIVE);

                auto it_find2 = children.find(key);
                if(it_find2 != children.end())
                    result = it_find2->second;
            }
        }

        return result;
    }

    ItemsMap::size_type Item::GetCount(SearchMod mod) const
    {
        ItemsMap::size_type result = 0;

        if(mod == Item::SearchMod::GLOBAL)
            result = GetCount();
        else if(mod == Item::SearchMod::LOCAL)
        {
            result = m_Children.size();
        }
        else
        {
            result = m_Children.size();
            for(const auto &it : m_Children)
                result += it.second->GetCount(Item::SearchMod::RECURSIVE);
        }

        return result;
    }

    bool Item::Empty() const
    {
        return m_Children.empty();
    }

    ItemsMap::size_type Item::GetParentingDepth() const
    {
        ItemsMap::size_type depth = 0;
        if(auto opt = GetParentOpt())
        {
            // Pour détecter d'éventuelles boucles récursives infinies.
            // todo Voir si cela est bien utile.
            MA_ASSERT(opt.value().get() != this, L"Le parent retourné est l'item lui-même.", std::runtime_error);

            depth = opt.value()->GetParentingDepth();
        }
        return depth;
    }

    Item::key_type Item::GetKey() const
    {
        return m_Id.GetKey();
    }

    ObservablePtr Item::GetPtr() const
    {
        return GetItem(GetKey());
    }

    Item::key_type Item::GetParentKey() const
    {
        return m_ParentKey;
    }

    std::optional<ItemPtr> Item::GetParentOpt() const
    {
        if(m_ParentKey != Key::GetNoParent() && HasItem(m_ParentKey))
        {
            auto parent = GetItem(m_ParentKey);

            MA_ASSERT(parent->HasItem(GetKey(), SearchMod::LOCAL),
                      L"La clef du parent pointe sur un item ne possédant pas cet enfant.",
                      std::logic_error);

            return parent;
        }

        return {};
    }

    std::shared_ptr<Item> ma::Item::GetParent() const
    {
        MA_ASSERT(m_ParentKey != Key::GetNoParent(),
                  L"La clef à pour valeur ms_KeyNoParent « " + Key::GetNoParent() +
                      L" », cette clef ne peut faire référence à un item de ms_Map.",
                  std::invalid_argument);

        ItemPtr result;
        // portée du mutex
        {
            lock_guard mutex(ms_MapMutex);
            auto it_find = ms_Map.find(m_ParentKey);
            result = it_find != ms_Map.end() ? it_find->second : nullptr;
        }

        MA_ASSERT(result,
                  L"La clef « " + m_ParentKey + L" » ne fait référence à aucun items de ms_Map.",
                  std::invalid_argument);

        return result;
    }

    void Item::AddChild(const key_type &key)
    {
        MA_ASSERT(Id::IsValid(key), L"La clef «" + key + L"» n'est pas un GUID valide.", std::invalid_argument);

        MA_ASSERT(key != Key::GetRoot(), L"L'item «" + key + L"» à ajouter est la racine.", std::invalid_argument);

        MA_ASSERT(key != Key::GetNoParent(),
                  L"La clef, pour désigner l'item enfant à  ajouter, est la clef pour désigner un item sans parent.",
                  std::invalid_argument);

        AddChild(GetItem(key));
    }

    void Item::AddChild(ItemPtr item)
    {
        MA_ASSERT(item, L"L'item enfant à ajouter est nulle.", std::invalid_argument);

        MA_ASSERT(item->GetKey() != GetKey(), L"L'item tente de s'ajouter à lui même.", std::invalid_argument);

        Item::key_type key = item->GetKey(), old_parent_key = item->m_ParentKey, new_parent_key = this->GetKey();

        MA_ASSERT(AcceptToAddChild(item),
                  L"Le parent «" + GetKey() + L"» de type « " + GetTypeInfo().m_ClassName +
                      L"» n'accepte pas d'ajouter l'item « " + key + L" » de type «" + item->GetTypeInfo().m_ClassName +
                      L"». Veuillez vérifier la possibilité d'ajouter un item avant d'essayer de l'ajouter. Pour ce "
                      L"faire utiliser la fonction AcceptToAddChild.",
                  std::invalid_argument);

        MA_ASSERT(key != Key::GetRoot(), L"L'item «" + key + L"» à ajouter est la racine.", std::invalid_argument);

        UpdateObservations({{Key::Obs::ms_AddItemBegin, key}});

        if(old_parent_key != Key::GetNoParent() && old_parent_key != new_parent_key)
        {
            auto old_parent = GetItem(old_parent_key);
            MA_ASSERT(old_parent,
                      L"L'item que vous tentez d'ajouter est déjà associé à un item parent. Pour le retirer de son "
                      L"précédent parent il nous faut retrouver cet ancien parent. Hors la clef de celui-ci n'est "
                      L"associée à aucun item existant.",
                      std::runtime_error);

            old_parent->RemoveChild(key);
        }

        if(HasItem(Key::GetGarbage()))
        {
            auto garbage = GetItem<ma::Garbage>(Key::GetGarbage());
            if(garbage->HasTrash(key))
            {
                garbage->RemoveTrash(key);
            }
        }

        auto items = item->GetItems(Item::SearchMod::RECURSIVE);

        // Portée du mutex
        {
            lock_guard mutex(ms_MapMutex);
            ms_Map[key] = item;
            for(const auto &it : items)
            {
                ms_Map[it.first] = it.second;
            }

            m_Children[key] = item;
            item->m_ParentKey = new_parent_key;
        }

        if(GetBatchState())
            item->BeginBatch();

        // L'item est en cours de création, c'est donc CreateItem ou la fonction appelante qui se chargera d'appeler
        // UpdateObservations et ensuite EndConstruction.
        if(item->GetEndConstruction())
        {
            UpdateObservations({{Key::Obs::ms_AddItemEnd, key}});
            if(!item->IsEnable() && Item::HasItem(item->m_ParentKey))
            {
                item->OnEnable();
                for(const auto &it : items)
                {
                    it.second->OnEnable();
                }
            }
        }
    }

    void Item::RemoveChild(const key_type &key)
    {
        MA_ASSERT(Id::IsValid(key), L"La clef «" + key + L"» n'est pas un GUID valide.", std::invalid_argument);

        MA_ASSERT(key != Key::GetNoParent(),
                  L"La clef «" + key +
                      L"» pour désigner l'item enfant à  retirer, est la clef pour désigner un item sans parent.",
                  std::invalid_argument);

        RemoveChild(GetItem(key, SearchMod::LOCAL));
    }

    void Item::RemoveChild(ItemPtr item)
    {
        auto key = item->GetKey();

        MA_ASSERT(key != Key::GetRoot(),
                  L"La clef «" + key + L"» pour désigner l'item enfant à retirer, est la clef pour désigner la racine.",
                  std::invalid_argument);

        MA_ASSERT(AcceptToRemoveChild(key),
                  L"Le parent «" + GetKey() + L"» n'accepte pas de retirer l'item «" + key +
                      L"». Veuillez vérifier la possibilité d'ajouter un item avant d'essayer de l'ajouter. Pour ce "
                      L"faire utiliser la fonction AcceptToRemoveChild.",
                  std::invalid_argument);

        auto items = item->GetItemsPostOrder(Item::SearchMod::RECURSIVE);

        // On commence d'abord par avertir les observateurs, car ils peuvent avoir
        // besoin d'accéder aux items via Item avant qu'ils ne soient
        // supprimés.
        // On commence par avertir l'observateur du parent. Ce n'est qu'une
        // convention.
        UpdateObservations({{Key::Obs::ms_RemoveItemBegin, key}});

        // Nous désactivons l'item avant qu'il ne soit retiré de son parent.
        // Cela permet d'avoir la hiérarchie intacte pour faire les opérations adéquates.
        if(item->IsEnable())
        {
            item->OnDisable();
        }

        for(const auto &it : items)
        {
            if(it->IsEnable())
                it->OnDisable();
        }

        if(HasItem(Key::GetObservationManager()))
        {
            auto obs_manager = GetItem<ObservationManager>(Key::GetObservationManager());

            for(const auto &it : items)
            {
                obs_manager->RemoveObservable(it);
                // Observer::OnDisable devrait s'en occupé.
                //                auto *item_observer = dynamic_cast<ma::ObserverPtr>(item.get());
                //                if(item_observer && obs_manager->HasObserver(item_observer))
                //                    obs_manager->RemoveObserver(item_observer);
            }

            obs_manager->RemoveObservable(item);

            // Observer::OnDisable devrait s'en occupé.
            //            auto *item_observer = dynamic_cast<ma::ObserverPtr>(item.get());
            //            if(item_observer && obs_manager->HasObserver(item_observer))
            //                obs_manager->RemoveObserver(item_observer);
        }

        bool is_a_trash = true;
        // Portée du mutex
        {
            lock_guard mutex(ms_MapMutex);

            m_Children.erase(key);
            item->m_ParentKey = Key::GetNoParent();

            for(const auto &it : items)
            {
                auto child_key = it->GetKey();
                auto it_find = ms_Map.find(child_key);
                // Nous pouvons vouloir supprimer un item qui ne soit plus observable/actif/dans ms_Map.
                //                MA_ASSERT(it_find != ms_Map.end(),
                //                          L"L'item enfant «" + child_key + L"» n'est pas présent dans ms_Map.",
                //                          std::runtime_error);
                if(it_find != ms_Map.end())
                    ms_Map.erase(it_find);
                else
                    is_a_trash = false;
            }
            ms_Map.erase(key);
        }

        if(is_a_trash)
        {
            // Via AddTrash, nous ajoutons un Item.
            // Du coup, il faut bien s'assurer d'être en dehors de la portée du mutex de ms_Map.
            if(HasItem(Key::GetGarbage()))
            {
                if(GetKey() != Key::GetGarbage())
                {
                    auto garbage = GetItem<Garbage>(Key::GetGarbage());

                    garbage->AddTrash(item);

                    for(const auto &it : items)
                        garbage->AddTrash(it);
                }
            }
            else
            {
                MA_WARNING(false,
                           L"La poubelle à « Item » n'est pas disponible."
                           L"Cela impactera le chargement qui peut-être essaiera de créer des éléments avec un GUID "
                           L"toujours existant car il n'aura pas accès à la poubelle à « Item » pour vérifier si un "
                           L"« Item » possède déjà le GUID demandé.");
            }
        }

        UpdateObservations({{Key::Obs::ms_RemoveItemEnd, key}});
    }

    void Item::RemoveChildren()
    {
        auto children = GetItemsPostOrder(Item::SearchMod::LOCAL);
        for(const auto &child : children)
            RemoveChild(child);
    }

    void Item::Remove()
    {
        auto parent = GetItem(GetParentKey());
        parent->RemoveChild(GetKey());
    }

    bool Item::AcceptToAddChild(const ItemPtr &item) const
    {
        if(item)
        {
            if(HasItem(item->GetKey(), Item::SearchMod::LOCAL))
            {
                return false;
            }
            else
            {
                auto const authorised = GetAuthorisedChildrenType();
                return authorised.find(item->GetTypeInfo().m_ClassName) != authorised.end();
            }
        }
        else
            return false;
    }

    bool Item::AcceptToRemoveChild(const key_type &key) const
    {
        return HasItem(key, Item::SearchMod::LOCAL);
    }

    bool Item::AcceptParent(const ma::ItemPtr &parent) const
    {
        if(parent)
        {
            // Un item ne peut avoir pour parent un de ses enfants.
            if(HasItem(parent->GetKey(), Item::SearchMod::RECURSIVE))
            {
                return false;
            }
            else
            {
                auto const authorised = GetAuthorisedParentsType();
                return authorised.find(parent->GetTypeInfo().m_ClassName) != authorised.end();
            }
        }
        else
            return false;
    }

    ClassNames GetDataFirstList(const std::wstring &class_name, const ma::TypeInfo::Data::key_type &key)
    {
        const auto datas = ma::TypeInfo::GetDatas(class_name, key);

        for(const auto &data : datas)
        {
            if(data)
            {
                // La première liste de classes sera utilisée.
                const auto &data_class_names = std::get<2u>(*data);
                if(!data_class_names.empty())
                {
                    ClassNames result{};
                    for(const auto &data_class_name : data_class_names)
                        result.insert(data_class_name);
                    return result;
                }
            }
        }

        return {};
    }

    ClassNames Item::GetWhiteListChildren(const std::wstring &class_name)
    {
        return GetDataFirstList(class_name, ma::TypeInfo::Key::GetWhiteListChildrenData());
    }

    ClassNames Item::GetBlackListChildren(const std::wstring &class_name)
    {
        return GetDataFirstList(class_name, ma::TypeInfo::Key::GetBlackListChildrenData());
    }

    ClassNames Item::GetWhiteListParents(const std::wstring &class_name)
    {
        return GetDataFirstList(class_name, ma::TypeInfo::Key::GetWhiteListParentsData());
    }

    ClassNames Item::GetBlackListParents(const std::wstring &class_name)
    {
        return GetDataFirstList(class_name, ma::TypeInfo::Key::GetBlackListParentsData());
    }

    ClassNames Item::GetWhiteListChildren() const
    {
        return GetDataFirstList(GetTypeInfo().m_ClassName, ma::TypeInfo::Key::GetWhiteListChildrenData());
    }

    ClassNames Item::GetBlackListChildren() const
    {
        return GetDataFirstList(GetTypeInfo().m_ClassName, ma::TypeInfo::Key::GetBlackListChildrenData());
    }

    ClassNames Item::GetWhiteListParents() const
    {
        return GetDataFirstList(GetTypeInfo().m_ClassName, ma::TypeInfo::Key::GetWhiteListParentsData());
    }

    ClassNames Item::GetBlackListParents() const
    {
        return GetDataFirstList(GetTypeInfo().m_ClassName, ma::TypeInfo::Key::GetBlackListParentsData());
    }

    ClassNames GetAuthorisedItemTypes(const ClassNames &white_list, const ClassNames &black_list)
    {
        ClassNames result;

        const TypeInfo::ClassHierarchies hierarchies = ClassInfoManager::GetClassHierarchies();

        for(const auto &it_hierarchies : hierarchies)
        {
            bool accepted = true;
            const std::wstring class_name = it_hierarchies.first;

            if(!white_list.empty())
            {
                accepted = white_list.count(class_name) > 0;

                if(!accepted)
                    for(auto it = white_list.cbegin(); !accepted && it != white_list.cend(); ++it)
                        accepted = ClassInfoManager::IsInherit(class_name, *it);
            }

            if(!black_list.empty())
            {
                accepted = accepted && black_list.count(class_name) == 0;

                if(accepted)
                    for(auto it = black_list.cbegin(); accepted && it != black_list.cend(); ++it)
                        accepted = !ClassInfoManager::IsInherit(class_name, *it);
            }

            if(accepted)
                result.insert(class_name);
        }

        return result;
    }

    ClassNames GetProhibitedItemTypes(const ClassNames &white_list, const ClassNames &black_list)
    {
        ClassNames result;

        const TypeInfo::ClassHierarchies hierarchies = ClassInfoManager::GetClassHierarchies();

        for(const auto &it_hierarchies : hierarchies)
        {
            bool rejected = true;
            const std::wstring class_name = it_hierarchies.first;

            if(!white_list.empty())
            {
                rejected = white_list.count(class_name) == 0;

                if(rejected)
                    for(auto it = white_list.cbegin(); rejected && it != white_list.cend(); ++it)
                        rejected = !ClassInfoManager::IsInherit(class_name, *it);
            }

            if(!black_list.empty())
            {
                rejected = rejected && black_list.count(class_name) > 0;

                if(!rejected)
                    for(auto it = black_list.cbegin(); !rejected && it != black_list.cend(); ++it)
                        rejected = ClassInfoManager::IsInherit(class_name, *it);
            }

            if(rejected)
                result.insert(class_name);
        }

        return result;
    }

    ClassNames Item::GetAuthorisedChildrenType() const
    {
        return GetAuthorisedItemTypes(GetWhiteListChildren(), GetBlackListChildren());
    }

    ClassNames Item::GetProhibitedChildrenType() const
    {
        return GetProhibitedItemTypes(GetWhiteListChildren(), GetBlackListChildren());
    }

    ClassNames Item::GetAuthorisedParentsType() const
    {
        return GetAuthorisedItemTypes(GetWhiteListParents(), GetBlackListParents());
    }

    ClassNames Item::GetProhibitedParentsType() const
    {
        return GetProhibitedItemTypes(GetWhiteListParents(), GetBlackListParents());
    }

    ClassNames Item::GetAuthorisedChildrenType(const std::wstring &class_name)
    {
        return GetAuthorisedItemTypes(GetWhiteListChildren(class_name), GetBlackListChildren(class_name));
    }

    ClassNames Item::GetProhibitedChildrenType(const std::wstring &class_name)
    {
        return GetProhibitedItemTypes(GetWhiteListChildren(class_name), GetBlackListChildren(class_name));
    }

    ClassNames Item::GetAuthorisedParentsType(const std::wstring &class_name)
    {
        return GetAuthorisedItemTypes(GetWhiteListParents(class_name), GetBlackListParents(class_name));
    }

    ClassNames Item::GetProhibitedParentsType(const std::wstring &class_name)
    {
        return GetProhibitedItemTypes(GetWhiteListParents(class_name), GetBlackListParents(class_name));
    }

    Item::dependencies_type Item::GetDependencies() const
    {
        if(auto opt = HasVarOpt(Key::Var::GetDependencies()))
        {
            auto vector = std::dynamic_pointer_cast<ma::ItemVectorVar>(opt.value());
            MA_WARNING(vector,
                       L"La variable « " + Key::Var::GetDependencies() + L" » de l'item « " + GetKey() +
                           L" » doit être du type « " + ma::TypeNameW<decltype(vector)>() + L" ».");

            return vector->GetKeys();
        }
        else
            return {};
    }

    void Item::AddDependencies(const std::vector<ma::Item::key_type> &item_keys)
    {
        const auto &dep_key = TypeInfo::Key::GetDependenciesData();

        // La variable des dépendances existe.
        if(auto opt = HasVarOpt<ItemVectorVar>(dep_key))
        {
            ItemVectorVarPtr var = opt.value();
            const auto items = var->Get();

            if(items.empty())
            {
                for(auto &item_key : item_keys)
                {
                    auto item = GetItem(item_key);
                    if(var->HasItem(item))
                        var->PushBack(item);
                }
            }
            else
            {
                for(auto &item_key : item_keys)
                {
                    auto item = GetItem(item_key);
                    if(auto it_find = std::find(items.begin(), items.end(), item); it_find == items.end())
                    {
                        var->PushBack(item);
                    }
                }
            }
        }
        // La variable des dépendances n'existe pas.
        else
        {
            ItemVectorVar::value_type items;
            for(auto &item_key : item_keys)
                items.push_back(GetItem(item_key));

            auto var = AddVar<ItemVectorVar>(dep_key, items);
            var->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                     L"Permet de référencer les dépendances statiques et dynamiques de l'élément.");
        }
    }

    void Item::SetDependencies(const std::vector<ma::Item::key_type> &item_keys)
    {
        const auto &dep_key = TypeInfo::Key::GetDependenciesData();

        // La variable des dépendances existe.
        if(auto opt = HasVarOpt<ItemVectorVar>(dep_key))
        {
            ItemVectorVarPtr var = opt.value();
            const auto items = var->Get();

            if(items.empty())
            {
                for(auto &item_key : item_keys)
                {
                    auto item = GetItem(item_key);
                    if(var->HasItem(item))
                        var->PushBack(item);
                }
            }
            else
            {
                /// on supprime les items n'étant pas dans items_keys
                for(auto &item : items)
                {
                    if(std::find(item_keys.begin(), item_keys.end(), item->GetKey()) == item_keys.end())
                        var->Erase(item);
                }

                /// on insère les nouveaux.
                size_t index = 0;
                for(auto &item_key : item_keys)
                {
                    auto item = GetItem(item_key);
                    if(auto it_find = std::find(items.begin(), items.end(), item); it_find == items.end())
                    {
                        var->Insert(index, item);
                    }

                    index++;
                }
            }
        }
        // La variable des dépendances n'existe pas.
        else
        {
            ItemVectorVar::value_type items;
            for(auto &item_key : item_keys)
                items.push_back(GetItem(item_key));

            auto var = AddVar<ItemVectorVar>(dep_key, items);
            var->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                     L"Permet de référencer les dépendances statiques et dynamiques de l'élément.");
        }
    }

    void Item::AddVar(const VarPtr &var)
    {
        MA_ASSERT(var->m_KeyItem == Key::GetNoParent(),
                  L"La variable est déjà associée à l'item «" + var->m_KeyItem + L"»",
                  std::invalid_argument);

        MA_ASSERT(
            m_Vars.find(var->m_Key) == m_Vars.end(), L"La variable est déjà associée à l'item.", std::invalid_argument);

        m_Vars[var->m_Key] = var;
        var->m_KeyItem = GetKey();

        if(GetBatchState())
            var->BeginBatch();

        UpdateObservations({{Key::Obs::ms_AddVar, var->m_Key}});
    }

    VarPtr Item::CopyVar(const VarPtr &var)
    {
        VarPtr result = var->Copy();

        MA_ASSERT(m_Vars.find(result->m_Key) == m_Vars.end(),
                  L"La variable est déjà associée à l'item.",
                  std::invalid_argument);

        m_Vars[result->m_Key] = result;
        result->m_KeyItem = GetKey();

        if(GetBatchState())
            result->BeginBatch();

        result->EndConstruction();

        return result;
    }

    void Item::EraseVar(const Var::key_type &key)
    {
        auto it_find = m_Vars.find(key);
        MA_ASSERT(it_find != m_Vars.end(), L"La variable n'est pas associée à l'item.", std::invalid_argument);

        auto var = it_find->second;

        MA_ASSERT(var->m_Dynamic,
                  L"La variable est membre de la classe " + GetTypeInfo().m_ClassName +
                      L" seul une variable dynamiquement ajoutée peut être enlever de l'item. Veuillez vérifier si la "
                      L"variable est membre ou dynamique. Pour se faire utiliser var->m_Dynamic.",
                  std::invalid_argument);

        // On commence d'abord par avertir les observateurs, car ils peuvent avoir
        // besoin de d'accéder aux variables de l'item avant qu'elles ne soient
        // supprimées.
        // On commence par avertir l'observateur de l'item. Ce n'est qu'une
        // convention.
        UpdateObservations({{Key::Obs::ms_RemoveVarBegin, key}});

        if(HasItem(Key::GetObservationManager()))
        {
            auto obs_manager = GetItem<ObservationManager>(Key::GetObservationManager());
            obs_manager->RemoveObservable(var);
        }

        var->m_KeyItem = Key::GetNoParent();
        m_Vars.erase(it_find);

        UpdateObservations({{Key::Obs::ms_RemoveVarEnd, key}});
    }

    VarPtr Item::GetVar(const Var::key_type &key) const
    {
        auto it_find = m_Vars.find(key);
        MA_ASSERT(it_find != m_Vars.end(),
                  L"La clef «" + key + L"» n'est associée à aucune variable de l'item",
                  std::invalid_argument);

        return it_find->second;
    }

    bool Item::HasVar(const Var::key_type &key) const
    {
        return m_Vars.find(key) != m_Vars.end();
    }

    std::optional<ma::VarPtr> Item::HasVarOpt(const Var::key_type &key) const
    {
        auto it_find = m_Vars.find(key);
        if(it_find != m_Vars.end())
            return it_find->second;
        else
            return {};
    }

    void Item::SetVarFromString(const VarsMap::key_type &key, const std::wstring &value)
    {
        VarPtr var = GetVar(key);
        var->SetFromItem(value);
    }

    VarsMap Item::GetVars() const
    {
        return m_Vars;
    }

    bool Item::IsObservable() const
    {
        return GetEndConstruction() && ms_Map.count(GetKey());
    }

    void Item::BeginBatch()
    {
        if(!GetBatchState())
        {
            for(const auto &item : m_Children)
                item.second->BeginBatch();

            for(const auto &var : m_Vars)
                var.second->BeginBatch();
        }

        Observable::BeginBatch();
    }

    void Item::EndBatch()
    {
        bool batch_state = GetBatchState();

        Observable::EndBatch();

        if(batch_state)
        {
            for(const auto &var : m_Vars)
                var.second->EndBatch();

            for(const auto &item : m_Children)
                item.second->EndBatch();
        }
    }

    void Item::InitPathVars()
    {
        m_CustomSavePath = this->AddMemberVar<ma::BoolVar>(Key::Var::GetCustomSavePath(), false);
        m_CustomSavePath->InsertIInfoKeyValue(
            ma::Var::Key::IInfo::ms_Doc,
            L"Si vrais alors « m_SavePath » sera utilisé sinon le chemin sera déduit de son parent.");

        // La variable « m_CustomSavePath » étant à faux, nous pouvons appeler GetFolderPath et éviter ainsi de
        // dupliquer le code pour déduire le chemin.
        // const auto save_path = GetFolderPath();
        // Nous mettons des chaînes de caractères vides, cela permettra à la fonction « EndConstruction » de détecter
        // s'il doit modifier ou non les variables « m_SavePath » et « m_LastSavePath ».

        const std::filesystem::path empty_path{};

        m_SavePath = this->AddMemberVar<ma::DirectoryVar>(Key::Var::GetSavePath(), empty_path);
        m_SavePath->InsertIInfoKeyValue(
            ma::Var::Key::IInfo::ms_Doc,
            L"Le chemin du dossier où sera sauvegarder l'item. \n"
            L"Remarques: Par défaut il sera défini en fonction de l'item parent."
            L"Sa modification ne supprimera pas la sauvegarde actuelle. "
            L"Il faudra lancer l'action de sauvegarde pour supprimer le dossier actuel de sauvegarde et créer le "
            L"nouveau.");

        m_LastSavePath = this->AddReadOnlyMemberVar<ma::DirectoryVar>(Key::Var::GetLastSavePath(), empty_path);
        m_LastSavePath->InsertIInfoKeyValue(
            ma::Var::Key::IInfo::ms_Doc,
            L"Le chemin du dossier où est sauvegarder l'item. "
            L"Permet de comparer avec « m_SavePath » pour savoir s'il faut supprimer ou non le dossier actuel de la "
            L"sauvegarde.");
    }

    void Item::InitDependencyVar()
    {
        auto dependency_keys = GetTypeInfo().GetDependencies();
        ItemVectorVar::value_type dependencies;

        for(const auto &dependency_key : dependency_keys)
        {
            if(auto option = HasItemOpt(dependency_key))
            {
                dependencies.push_back(*option);
            }
            else
            {
                MA_ASSERT(false,
                          L"Cet élément dépend d'un autre élément « " + dependency_key + L" » qui n'est pas présent.",
                          std::invalid_argument);
            }
        }

        if(!dependencies.empty())
        {
            auto var = this->AddMemberVar<ItemVectorVar>(Key::Var::GetDependencies(), dependencies);
            var->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                     L"Permet de référencer les dépendances statiques et dynamiques de l'élément.");
        }
    }

    std::wstring Item::GetFolderSaveName(const Item &item) const
    {
        auto folder_name = item.GetKey() + L"_"s + item.GetTypeInfo().m_ClassName;

        if(item.HasVar(Key::Var::GetName()))
            folder_name += L"_"s + item.GetVar(Key::Var::GetName())->toString();

        ReplaceAll(folder_name, L"::", L"_");
        ReplaceAll(folder_name, L" ", L"_");

        folder_name = ma::Trim(folder_name);

        // POSIX defines a Portable Filename Character Set
        static const std::set<std::wstring::value_type> portable_filename_char{
            L'A', L'B', L'C', L'D', L'E', L'F', L'G', L'H', L'I', L'J', L'K', L'L', L'M', L'N', L'O', L'P', L'Q',
            L'R', L'S', L'T', L'U', L'V', L'W', L'X', L'Y', L'Z', L'a', L'b', L'c', L'd', L'e', L'f', L'g', L'h',
            L'i', L'j', L'k', L'l', L'm', L'n', L'o', L'p', L'q', L'r', L's', L't', L'u', L'v', L'w', L'x', L'y',
            L'z', L'0', L'1', L'2', L'3', L'4', L'5', L'6', L'7', L'8', L'9', L'.', L'_', L'-'};

        std::wstring result{};
        for(const auto character : folder_name)
        {
            if(portable_filename_char.count(character) > 0)
                result += character;
        }

        return result;
    }

    std::filesystem::path Item::GetFolderPath(bool vars_loading) const
    {
        if(m_Savable)
        {
            auto is_custom = std::dynamic_pointer_cast<ma::BoolVar>(m_CustomSavePath);
            if(vars_loading)
            {
                return std::filesystem::path{m_LastSavePath->toString()};
            }
            else if(!is_custom->Get())
            {
                std::filesystem::path save_path;
                ma::ItemPtr parent;

                if(m_ParentKey != Key::GetNoParent() && (parent = GetItem(m_ParentKey)))
                    save_path = parent->GetFolderPath();
                else
                    save_path = std::filesystem::current_path();

                auto folder_name = GetFolderSaveName(*this);

                try
                {
                    save_path /= folder_name;
                }
                catch(const std::exception &e)
                {
                    // https://stackoverflow.com/q
                    // uestions/1976007/what-characters-are-forbidden-in-windows-and-linux-directory-names
                    MA_ASSERT(
                        false,
                        L"Cette erreur apparait certainement parce-que le nom du dossier déduit du nom et du type de "
                        L"l'item est composé de caractère spéciaux invalide pour un nom de dossier.\n folder_name : " +
                            folder_name + L"\n save_path : " + save_path.wstring() + L"\n Erreur attrapée: " +
                            ma::to_wstring(e.what()),
                        std::invalid_argument);
                }

                return save_path;
            }
            else
                return std::filesystem::path{m_SavePath->toString()};
        }
        else
            return {};
    }

    // En fonction d'un dossier renvoie la clef de l'item contenu dans le nom
    // du dossier.
    Item::key_type Item::GetKeyFromFolderSavePath(const std::filesystem::directory_entry &dir_entry) const
    {
        const auto folder_name = dir_entry.path().stem().wstring();
        const auto key_size = Id::GetStrLength();

        if(folder_name.size() >= key_size)
            return folder_name.substr(0, key_size);
        else
            return Key::GetNoParent();
    }

    /*
    std::wstring Item::GetClassNameFromFolderSavePath(const std::filesystem::directory_entry& dir_entry) const
    {
        const auto folder_name = dir_entry.path().stem().wstring();
        const auto key_type_name = ma::Split(folder_name, L'_');

        if(key_type_name.size() > 1u)
            return key_type_name[1u];
        else
            return L"";
    }

    std::wstring Item::GetNameFromFolderSavePath(const std::filesystem::directory_entry& dir_entry) const
    {
        const auto folder_name = dir_entry.path().stem().wstring();
        const auto key_type_name = ma::Split(folder_name, L'_');

        if(key_type_name.size() > 2u)
            return key_type_name[2u];
        else
            return L"";
    }
    */

    ma::ItemPtr Item::GetItemFromFolderPath(const std::filesystem::directory_entry &dir_entry) const
    {
        // GetFolderSaveName(*GetItem(key).get()) != path.stem().wstring()))

        ma::ItemPtr result;
        const auto key = GetKeyFromFolderSavePath(dir_entry);
        // On ne peut pas compter sur cette fonction, car les noms de classe
        // peuvent contenir l'espace de non formé par « :: » ce qui est un
        // caractère non autorisé pour définir un nom de dossier.
        // const auto class_name = GetClassNameFromFolderSavePath(dir_entry);

        if(dir_entry.is_directory() && key != Key::GetNoParent())
        {
            // ma::ItemPtr item;
            if(this->HasItem(key, Item::SearchMod::LOCAL))
            {
                result = GetItem(key);
            }
            else
            {
                // Si la poubelle à item existe, nous regardons dedans si l'item continue d'y exister.
                if(Item::HasItem(Key::GetGarbage()))
                {
                    auto garbage = GetItem<Garbage>(Key::GetGarbage());
                    if(garbage->HasTrash(key))
                        result = garbage->GetTrash(key)->m_Item;
                }
            }

            // if(item) //  && item->GetTypeInfo().m_ClassName == class_name)
            //    result = item;
        }

        return result;
    }

    void Item::Save()
    {
        if(!m_Savable)
            return;

        const auto directory_path = GetFolderPath();
        const auto last_directory_path = std::filesystem::path(m_LastSavePath->toString());

        if(directory_path != last_directory_path)
        {
            DeleteSaveFolder();
            SetVarFromString(m_LastSavePath->GetKey(), directory_path.wstring());
        }

        if(!std::filesystem::exists(directory_path))
            std::filesystem::create_directory(directory_path);

        MA_ASSERT(std::filesystem::exists(directory_path),
                  L"Le dossier parent" + directory_path.wstring() + L"n'existe pas.",
                  std::runtime_error);

        MA_ASSERT(std::filesystem::is_directory(directory_path),
                  L"Le dossier parent" + directory_path.wstring() + L"n'est pas un dossier mais un fichier.",
                  std::logic_error);

        // exemple:
        // L'idéal pour fichier python:
        // std::tuple<str, int, float> toto = {"toto_1", 1, -2.1}
        // toto: tuple[str, int, float] = {"toto_1", 1, -2.1}
        // L'idéal pour un fichier python qui serait parsé via un interpréteur:
        // item.AddFloatVar("m_FloatVar", 0.1)
        // L'idéal pour un fichier cpp:
        // item->AddVar<ma::FloatVar>(L"m_FloatVar", 0.1f);
        // Le plus simple pour le moment:
        // item.cpp:
        // {
        //      {"m_FloatVar", "FloatVar", 0.1}
        //      {"m_Toto", "StringHashSetVar", {"toto", "tutu"}}
        // }    {"m_Item", "ItemVar", "00000000-0000-0000-0000-000000000009"}

        const auto vars_cpp_file = directory_path / L"item.cpp";

        // numéro de version de la sérialisation
        const auto save_version = std::make_pair(L"version"s, 1u);

        const auto item_header = std::tuple{
            std::make_pair(L"key"s, m_Id.GetKey()),
            std::make_pair(L"type"s, GetTypeInfo().m_ClassName),
        };

        std::wstringstream ss;
        // La portée du fichier est locale, car en cas d'erreur, ça sera plus facile de la localiser et surtout cela
        // permet de ne pas garder le flux ouvert pendant que les enfants s'enregistrent eux aussi.
        if(std::ofstream outfile{vars_cpp_file})
        {
            ss << L"{\n";

            ss << L"\t" << ma::toString(save_version) << ",\n";
            ss << L"\t" << ma::toString(item_header) << ",\n";
            ss << L"\t{\n";

            // On ne doit pas sauvegarder les variables m_SavePath et
            // m_LastSavePath. Sinon, elles vont écraser les chemins des items et
            // dans certains cas, ils seront faux.
            // Sauf si le chemin de l'item est personnalisé.
            auto vars = m_Vars;
            if(!std::dynamic_pointer_cast<BoolVar>(m_CustomSavePath)->Get())
            {
                std::set<Var::key_type> save_vars{Key::Var::GetSavePath(), Key::Var::GetLastSavePath()};

                for(const auto &save_key_var : save_vars)
                {
                    auto it_find = vars.find(save_key_var);
                    if(it_find != vars.end())
                        vars.erase(it_find);
                }
            }

            // Nous retirons les variables qui ne doivent pas être sauvegardées.
            for(const auto &it : m_Vars)
            {
                const auto var = it.second;
                if(!var->m_Savable && vars.find(it.first) != vars.end())
                {
                    vars.erase(it.first);
                }
            }

            auto index = vars.size();
            for(const auto &it_var : vars)
            {
                const auto key = it_var.first;
                const auto var = it_var.second;

                const auto type = var->GetTypeInfo().m_ClassName;
                /// \todo utiliser ma::IsRepresentedByString<decltype(value)>()
                /// et virer la fonction toStringRepresentation
                const auto value_str = var->toStringRepresentation();

                ss << L"\t\t{";
                ss << ma::toString(std::make_pair(L"key"s, key)) << L", ";
                ss << ma::toString(std::make_pair(L"type"s, type)) << L", ";
                ss << LR"({"value", )"s << value_str << L"}, ";
                /*
                // La clef n'est pas entre guillemets.
                const auto iinfos_str = ma::toString(var->GetIInfo());
                // MA_MSG(L"var->GetIInfo(): "s + iinfos_str)
                ss << LR"({"infos", )"s << iinfos_str << L"}";
                /*/
                ss << LR"({"infos", {)"s;
                size_t index_iinfo = var->GetIInfo().size();
                for(const auto &it_iinfo : var->GetIInfo())
                {
                    ss << LR"({")"s << it_iinfo.first << LR"(", )"s;
                    ss << LR"(")"s << it_iinfo.second << LR"("})"s;

                    index_iinfo--;

                    if(index_iinfo > 0)
                        ss << L","s;
                }
                ss << L"}}";
                //*/

                ss << L"}";

                if(index > 1u)
                    ss << L",";

                ss << L"\n";

                index--;
            }

            ss << L"\t}\n";
            ss << L"}\n";

            outfile << ma::to_string(ss.str());
        }

        // On supprime les dossiers n'appartenant pas à la liste des enfants
        for(const std::filesystem::directory_entry &dir_entry : std::filesystem::directory_iterator{directory_path})
        {
            // pour ne pas supprimer des fichiers
            if(dir_entry.is_directory())
            {
                const auto key = GetKeyFromFolderSavePath(dir_entry);

                if( // pour éviter de supprimer des dossiers qui ne font pas partie
                    // de cette sauvegarde.
                    ma::Id::IsValid(key) && !GetItemFromFolderPath(dir_entry))
                {
                    try
                    {
                        std::filesystem::remove_all(dir_entry);
                        std::wcout << L"Suppression du  dossier \n" << dir_entry << std::endl;
                    }
                    catch(const std::filesystem::filesystem_error &ex)
                    {
                        std::wcout << L"La suppression d'un dossier a échouée." << L'\n' << L"what():  " << ex.what()
                                   << L'\n' << L"path1(): " << ex.path1() << L'\n' << L"path2(): " << ex.path2()
                                   << L'\n' << L"code().value(): " << ex.code().value() << L'\n'
                                   << L"code().message(): " << ex.code().message() << L'\n' << L"code().category(): "
                                   << ex.code().category().name() << std::endl;
                    }
                }
            }
        }

        // comment supprimer les dossiers des items supprimés et qui sauvegardent
        // leurs données dans un dossier indépendant du parent ?

        for(const auto &it : m_Children)
            it.second->Save();
    }

    void Item::FillParentingMap(parenting_map_type &dependencies_map, const ItemPtr &item, const Item::key_type &key)
    {
        if(auto opt = item->GetParentOpt())
        {
            const auto parent_key = opt.value()->GetKey();
            dependencies_map[key].push_front(parent_key);
            if(auto it_find = dependencies_map.find(parent_key); it_find != dependencies_map.end())
            {
                const auto &grand_parents = it_find->second;
                auto &parents = dependencies_map[key];
                parents.insert(parents.end(), grand_parents.rbegin(), grand_parents.rend());
            }

            FillParentingMap(dependencies_map, opt.value(), key);
        }
    }

    void Item::FillParentingMap(parenting_map_type &dependencies_map,
                                const std::filesystem::directory_entry &directory_path,
                                const Item::key_type &parent_key)
    {
        if(std::filesystem::exists(directory_path) && std::filesystem::is_directory(directory_path))
        {
            const auto key = GetKeyFromFolderSavePath(directory_path);

            dependencies_map[key].push_front(parent_key);
            if(auto it_find = dependencies_map.find(parent_key); it_find != dependencies_map.end())
            {
                const auto &grand_parents = it_find->second;
                auto &parents = dependencies_map[key];
                parents.insert(parents.end(), grand_parents.rbegin(), grand_parents.rend());
            }

            for(const std::filesystem::directory_entry &dir_entry : std::filesystem::directory_iterator{directory_path})
            {
                if(dir_entry.is_directory())
                {
                    FillParentingMap(dependencies_map, dir_entry, key);
                }
            }
        }
    }

    static const auto error_separator{L"===========================================\n"s};

    Item::key_errors Item::LoadItems()
    {
        /// {clef de l'item, {clefs root, ..., clef parent + 1, clef parent}}
        static parenting_map_type parenting_map{};

        key_errors items_not_loaded;

        /// Une fois ce compteur à zéro dependencies_map sera vidé.
        static int counter{0};
        counter++;

        if(!m_Savable)
            return {};

        auto directory_path = GetFolderPath();

        if(!std::filesystem::exists(directory_path))
        {
            MA_WARNING(!m_CanBeDeleteOnLoad,
                       L"Le dossier de l'item « " + directory_path.wstring() + L" » n'existe pas.");
            return {};
        }

        if(!std::filesystem::is_directory(directory_path))
        {
            MA_WARNING(false,
                       L"Le dossier de l'item « " + directory_path.wstring() +
                           L" » n'est pas un dossier mais un fichier.");
            return {};
        }

        // Dans ce block, nous allons remplir toute la hiérarchie des données sans charger le projet.
        // Cela va permettre à la gestion des dépendances de charger des éléments en premiers dont d'autres dépendents
        // pour se créer.
        if(parenting_map.empty())
        {
            for(auto &&[key, item] : GetItems())
                FillParentingMap(parenting_map, item, key);

            auto root = GetRoot();
            const std::filesystem::directory_entry folder_path{root->GetFolderPath()};
            FillParentingMap(parenting_map, folder_path, root->GetParentKey());
        }

        // Nous créons et/ou nous mettons à jour les variables à partir du fichier « item.cpp ».
        const auto vars_cpp_file = directory_path / L"item.cpp";

        // todo voir pourquoi ce n'est pas un wifstream (de l'utf8) ?
        // visual ne le gère pas, visiblement gcc non plus.
        // J'ai essayé avec infile.imbue(loc) comme sur ce lien :
        // https://askcodez.com/ecrit-dans-un-fichier-en-utilisant-stdwofstream-le-fichier-reste-vide.html
        // ça ne fonctionne pas plus
        if(std::ifstream infile{vars_cpp_file, std::ios::ate})
        {
            const auto stream_size = infile.tellg();
            std::string ansi_text(stream_size, '\0');
            infile.seekg(0);
            if(!infile.read(ansi_text.data(), stream_size))
            {
                MA_WARNING(false, L"Attention le fichier « " + vars_cpp_file.wstring() + L" » n'a peu être lu.");
            }
            else
            {
                const auto utf8_text = ma::to_wstring(ansi_text);
                ma::Serialization::Parser parser{Trim(utf8_text)};

                const auto root = parser.GetRoot();

                std::wstringstream errors_ss;

                unsigned int version = 0;

                if(root->m_Nodes.empty())
                {
                    errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n'
                              << L"Il n'y a pas de numéro de version du système de sauvegarde utilisé.";
                }
                else
                    version = ma::FromNode<std::pair<std::wstring, unsigned int>>(root->m_Nodes[0]).second;

                // Le numéro de version maximal
                unsigned int max_version = 1u;

                if(version == 1u)
                {
                    const bool has_rtti = Item::HasItem(Key::GetRTTI());
                    MA_WARNING(has_rtti,
                               L"Il est impossible de recréer les items à charger sans gestionnaire de types dynamique "
                               L"(RTTI).");

                    RTTIPtr rtti = has_rtti ? GetItem<RTTI>(Key::GetRTTI()) : nullptr;

                    // Ce conteneur n'est pas constant, car elle pourra être mise à jour.
                    auto item_type_registered = rtti ? rtti->GetRegisteredMakerItems() : ma::MakerItemsMap{};

                    if(root->m_Nodes.size() == 1u)
                    {
                        errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n' << L"Il n'y a pas d'en-tête.";
                    }
                    else
                    {
                        const auto header = root->m_Nodes[1u];
                        const auto errors = GetErrors<header_save_type>(header);

                        for(const auto &error : errors)
                            errors_ss << toString(error) << L'\n';
                        // errors_ss << L"Texte nombre de chr: « " << error[0] << L"» Erreurs: " << error[1] << L'\n';
                    }

                    if(root->m_Nodes.size() < 3u)
                    {
                        errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n' << L"Il n'y a pas de corps.";
                    }

                    // On crée les items n'existant pas et on supprime ceux qui n'appartiennent pas à la sauvegarde.

                    // La clef des items qui appartiennent à la sauvegarde.
                    auto item_saves = std::unordered_set<ma::Item::key_type>{};

                    // Permet de finaliser la construction des item au moment opportun.
                    std::unordered_set<ma::ItemPtr> items_call_end_construction{};

                    // On crée et on met à jour les enfants à partir de la liste des dossiers.
                    // On parcourt les dossiers pour s'assurer de la création des enfants avant de les charger un par
                    // un.
                    // Pour aller vite, on utilise le nom des dossiers et non les item.cpp.
                    for(const auto &dir_entry : std::filesystem::directory_iterator{directory_path})
                    {
                        if(dir_entry.is_directory())
                        {
                            auto item = GetItemFromFolderPath(dir_entry);

                            const auto &path = dir_entry.path();
                            const auto key = item ? item->GetKey() : GetKeyFromFolderSavePath(dir_entry);
                            const auto class_name = item ? item->GetTypeInfo().m_ClassName : GetTypeFromSave(dir_entry);
                            const auto dependencies =
                                item ? item->GetDependencies() : GetDependenciesFromSave(dir_entry);

                            // On vérifie si le dossier représente un item.
                            if((key != Key::GetNoParent() && ma::Id::IsValid(key)) || item)
                            {
                                item_saves.insert(key);

                                // Si la poubelle à item existe, nous regardons dedans si l'item continue d'y exister.
                                if(Item::HasItem(Key::GetGarbage()))
                                {
                                    auto garbage = GetItem<Garbage>(Key::GetGarbage());
                                    if(garbage->HasTrash(key))
                                    {
                                        item = garbage->GetTrash(key)->m_Item;

                                        MA_ASSERT(item, L"L'item récupérer dans le débris est nul", std::runtime_error);
                                    }
                                }

                                if(rtti && !item)
                                {
                                    // L'élément dont dépend celui-ci sera chargé. Mais le système des
                                    // observations ne s'active cas la fin du chargement des éléments
                                    // et des variables.
                                    // Il y a donc deux manières de faire soit bien tout prévoir et
                                    // bien marqué les dépendances, soit relancer le chargement
                                    // jusqu'à ce que le nombre d'éléments non créés, soit constant.
                                    //

                                    // Gestion des dépendances :
                                    // Les éléments dont dépend cet élément seront chargé avant lui.
                                    if(!dependencies.empty())
                                    {
                                        for(const auto &dependency_key : dependencies)
                                        {
                                            // Si la dépendance n'est pas déjà chargée.
                                            if(!Item::HasItem(dependency_key))
                                            {
                                                // Nous retrouvons la dépendance dans la structure préchargée.
                                                const auto it_find = parenting_map.find(dependency_key);
                                                if(it_find != parenting_map.end())
                                                {
                                                    // Nous cherchons sont plus proche parent qui a déjà été chargé.
                                                    Item::key_type last_key;
                                                    for(auto it = it_find->second.cbegin();
                                                        last_key.empty() && it != it_find->second.cend();
                                                        ++it)
                                                    {
                                                        const auto &dependency_parent_key = *it;
                                                        if(Item::HasItem(dependency_parent_key))
                                                        {
                                                            last_key = dependency_parent_key;
                                                        }
                                                    }

                                                    if(!last_key.empty())
                                                    {
                                                        std::wstring error_message;
                                                        if(last_key == Key::GetRoot())
                                                        {
                                                            error_message =
                                                                L"Le plus proche parent qui est déjà chargé est "
                                                                L"la racine. L'algorithme du chargement des "
                                                                L"dépendances ne permet pas actuellement de "
                                                                L"charger un enfant spécifique.";
                                                        }
                                                        else if(last_key == GetParentKey())
                                                        {
                                                            error_message =
                                                                L"Le plus proche parent qui est déjà chargé est  le "
                                                                L"parent cet élément. L'algorithme du chargement des "
                                                                L"dépendances ne permet pas actuellement de charger un "
                                                                L"enfant spécifique.";
                                                        }

                                                        if(error_message.empty())
                                                        {
                                                            auto dep = Item::GetItem(last_key);
                                                            const auto deps_not_loaded = dep->LoadItems();
                                                            items_not_loaded.insert(deps_not_loaded.begin(),
                                                                                    deps_not_loaded.end());

                                                            if(rtti)
                                                                item_type_registered = rtti->GetRegisteredMakerItems();

                                                            if(auto opt = Item::HasItemOpt(key))
                                                                item = opt.value();
                                                        }
                                                        else
                                                        {
                                                            MA_WARNING(false, error_message);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MA_WARNING(false,
                                                                   L"Cet item « " + dir_entry.path().wstring() +
                                                                       L" » dépend d'un item « " + dependency_key +
                                                                       L" » dont aucun parent n'a été chargé.");
                                                    }
                                                }
                                                else
                                                {
                                                    MA_WARNING(
                                                        false,
                                                        L"Cet item « " + dir_entry.path().wstring() +
                                                            L" » dépend d'un item « " + dependency_key +
                                                            L" » qui ne fait ni partie de la sauvegarde et ni partie "
                                                            L"des éléments par défauts de l'application.");
                                                }
                                            }
                                        }
                                    }

                                    if(!item)
                                    {
                                        // - On vérifie si l'item existe déjà.
                                        // - Vérifier s'il est dans la poubelle.
                                        // - Vérifier si celui qui existe à bien le même parent.
                                        // - Vérifier si celui qui existe est du même type.
                                        // Si cela arrive alors erreur, car avec la destruction de celui existant
                                        // pourrait ne pas se faire, parce qu'une variable peut toujours pointer sur
                                        // lui.
                                        if(item_type_registered.count(class_name))
                                        {
                                            item = rtti->CreateItem(class_name, {this->GetKey(), true, true, key});
                                            // items_call_end_construction.insert(item);
                                        }
                                        else
                                        {
                                            item = CreateChildFromSave(dir_entry);

                                            if(!item)
                                            {
                                                const std::wstring message{
                                                    error_separator + L"Ligne: " + ma::toString(M_LINE) +
                                                    L"\nLe type « " + class_name +
                                                    L" » de classe d'item n'appartient pas au gestionnaire de type "
                                                    L"dynamique et " +
                                                    GetTypeInfo().m_ClassName +
                                                    L"::CreateChildFromSave ne permet pas de créer ce type.\n"};

                                                items_not_loaded[key] = message;
                                                errors_ss << message;
                                            }
                                        }
                                    }
                                }

                                if(item)
                                {
                                    const auto item_type_name = item->GetTypeInfo().m_ClassName;
                                    if(class_name != item_type_name)
                                    {
                                        errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n' << L"Le type « "
                                                  << class_name << L" » est différent de l'item associé." << L'\n';
                                    }

                                    if(item->GetParentKey() != GetKey())
                                    {
                                        AddChild(item);
                                    }

                                    // GetItemFromFolderPath peut renvoyer un item même si le chemin ne correspondent au
                                    // chemin défini dans l'item.
                                    // Les items peuvent surcharger GetItemFromFolderPath et veiller à ce que la
                                    // correspondance entre dossier et item avec d'autres règles que celles de bases.
                                    if(item->m_Savable)
                                    {
                                        const auto path_str = path.wstring();
                                        if(item->GetFolderPath() != path)
                                        {
                                            item->m_CustomSavePath->fromString(ma::toString(true));
                                            item->m_SavePath->fromString(path_str);
                                        }

                                        item->SetVarFromString(Key::Var::GetLastSavePath(), path_str);

                                        const auto item_directory_path = item->GetFolderPath(true);

                                        MA_ASSERT(std::filesystem::equivalent(item_directory_path, path),
                                                  L"Le dossier « " + item_directory_path.wstring() +
                                                      L" » de l'item « " + ma::toString(GetItem(GetKey())) +
                                                      L" » n'est pas le même que celui chargé « " + path.wstring() +
                                                      L" ».",
                                                  std::logic_error);

                                        const auto test_key_form_path = GetKeyFromFolderSavePath(dir_entry);
                                        MA_ASSERT(item->GetKey() == test_key_form_path,
                                                  L"L'item chargé n'a pas la même clef « " +
                                                      ma::toString(GetItem(GetKey())) + L" » que celle contenue « " +
                                                      test_key_form_path +
                                                      L" » dans le chemin de son dossier d'enregistrement.",
                                                  std::logic_error);
                                    }
                                    else
                                    {
                                        std::wcout << L"Non sauvegardable : " << ma::toString(item) << std::endl;
                                    }
                                }
                            }
                        }
                    }

                    // On supprime les enfants n'appartenant pas à la sauvegarde.
                    auto erase_items_set = std::unordered_set<ma::Item::key_type>{};
                    for(const auto &it : m_Children)
                    {
                        const auto key = it.first;
                        if(item_saves.find(key) == item_saves.end())
                        {
                            auto item = GetItem(key);
                            if(item->m_Savable && item->m_CanBeDeleteOnLoad)
                                erase_items_set.insert(key);
                        }
                    }

                    // std::wcout << (toString(erase_items_set)) << std::endl;

                    // this->BeginBatch();
                    for(const auto &key : erase_items_set)
                        ma::Item::EraseItem(key);

                    // Plantage ici wxTreeItem tente d'accéder à l'item supprimé.
                    // this->EndBatch();

                    const std::wstring errors_str = L"Attention le fichier suivant est peut-être mal formé.\n"s +
                                                    vars_cpp_file.wstring() +
                                                    L"\n"
                                                    L"L'erreur peut aussi venir de l'un des sous-dossiers.\n"
                                                    L"Voici les erreurs:\n"s +
                                                    errors_ss.str() + L"\n"s + utf8_text;

                    MA_WARNING(!parser.HasError() && errors_ss.str().empty(), errors_str);

                    // for(const auto item : items_call_end_construction)
                    // {
                    //     UpdateObservations({{ms_KeyObsAddItemEnd, item->GetKey()}});
                    //     item->EndConstruction();
                    // }

                    // On charge les enfants
                    for(const auto &it : m_Children)
                    {
                        const auto child_items_not_loaded = it.second->LoadItems();
                        items_not_loaded.insert(child_items_not_loaded.begin(), child_items_not_loaded.end());
                    }
                }
                else
                {
                    MA_WARNING(max_version >= version,
                               L"Le numéro de version du système de sauvegarde est plus récent que celui de cette "
                               L"version." +
                                   ma::toString(version));

                    MA_WARNING(version > 0,
                               L"Le numéro de version du système de sauvegarde est invalide." + ma::toString(version));
                }
            }
        }
        else
        {
            MA_WARNING(false, L"Impossible d'ouvrir le fichier « " + vars_cpp_file.wstring() + L" »");
        }
        // On supprime les variables n'appartenant pas à item.cpp

        counter--;
        if(counter < 1)
            parenting_map.clear();

        return items_not_loaded;
    }

    void Item::LoadVars()
    {
        if(!m_Savable)
            return;

        auto directory_path = GetFolderPath(true);

        if(!std::filesystem::exists(directory_path))
        {
            MA_WARNING(
                !m_CanBeDeleteOnLoad,
                L"Le dossier « " + directory_path.wstring() + L" » de l'item « " + ma::toString(GetItem(GetKey())) +
                    L" » n'existe pas.\n"
                    L"Cela peut venir du faite que la fonction BeginBatch de l'item n'est pas été appelée avant le "
                    L"chargement.\n"
                    L"Certains items génèrent automatiquement leur enfants via le mécanisme d'observation.\n"
                    L"Pour éviter cela il faut désactiver temporairement le mécanisme d'observation.");
            return;
        }

        if(!std::filesystem::is_directory(directory_path))
        {
            MA_WARNING(false,
                       L"Le dossier « " + directory_path.wstring() + L" » de l'item « " +
                           ma::toString(GetItem(GetKey())) + L" » n'est pas un dossier mais un fichier.");
            return;
        }

        // On crée et/ou on met à jour les variables à partir du fichier « item.cpp ».
        const auto item_cpp_file = directory_path / L"item.cpp";
        if(std::ifstream infile{item_cpp_file, std::ios::ate})
        {
            const auto stream_size = infile.tellg();
            std::string ansi_text(stream_size, '\0');
            infile.seekg(0);
            if(!infile.read(ansi_text.data(), stream_size))
            {
                MA_WARNING(false, L"Attention le fichier « " + item_cpp_file.wstring() + L" » n'a peu être lu.");
            }
            else
            {
                auto utf8_text = ma::to_wstring(ansi_text);
                // Trim
                ma::Serialization::Parser parser{utf8_text};

                const auto root = parser.GetRoot();

                std::wstringstream errors_ss;

                unsigned int version = 0;

                if(root->m_Nodes.empty())
                    errors_ss << L"Il n'y a pas de numéro de version du système de sauvegarde utilisé.";
                else
                    version = ma::FromNode<std::pair<std::wstring, unsigned int>>(root->m_Nodes[0]).second;

                if(version == 1u)
                {
                    const bool has_rtti = Item::HasItem(Key::GetRTTI());
                    MA_WARNING(has_rtti,
                               L"Il est impossible de recréer les variables à charger sans gestionnaire de types "
                               L"dynamique (RTTI).");

                    RTTIPtr rtti = has_rtti ? GetItem<RTTI>(Key::GetRTTI()) : nullptr;
                    const ma::MakerVarsMap var_type_registered =
                        rtti ? rtti->GetRegisteredMakerVars() : ma::MakerVarsMap{};

                    if(root->m_Nodes.size() == 1u)
                        errors_ss << L"Il n'y a pas d'en-tête.";
                    else
                    {
                        const auto header = root->m_Nodes[1u];
                        const auto errors = GetErrors<header_save_type>(header);

                        for(const auto &error : errors)
                            errors_ss << toString(error) << L'\n';
                        // errors_ss << L"Texte nombre de chr: « " << error[0] << L"» Erreurs: " << error[1] << L'\n';
                    }

                    if(root->m_Nodes.size() < 3u)
                        // if(3u <= root->m_Nodes.size())
                        errors_ss << L"Il n'y a pas de corps.";
                    else
                    {
                        const auto body = root->m_Nodes[2u];

                        // On Charge les variables de la sauvegarde
                        for(const auto &var_node : body->m_Nodes)
                        {
                            const auto &errors = GetErrors<var_save_type>(var_node);
                            for(const auto &error : errors)
                                errors_ss << toString(error) << L'\n';

                            if(var_node->m_Nodes.size() >= std::tuple_size<var_save_type>{})
                            {
                                typedef std::pair<std::wstring, key_type> key_node_type;
                                const auto key_errors = ma::GetErrors<key_node_type>(var_node->m_Nodes[0]);
                                for(const auto &error : key_errors)
                                    errors_ss << toString(error) << L'\n';

                                typedef std::pair<std::wstring, std::wstring> class_name_node_type;
                                const auto class_name_errors =
                                    ma::GetErrors<class_name_node_type>(var_node->m_Nodes[1u]);
                                for(const auto &error : class_name_errors)
                                    errors_ss << toString(error) << L'\n';

                                if(key_errors.empty() && class_name_errors.empty())
                                {
                                    const auto key = ma::FromNode<key_node_type>(var_node->m_Nodes[0]).second;
                                    const auto type = ma::FromNode<class_name_node_type>(var_node->m_Nodes[1u]).second;
                                    auto value_node = var_node->m_Nodes[2u];
                                    auto iinfo_node = var_node->m_Nodes[3u];

                                    if(value_node->m_Nodes.size() >= 2u)
                                    {
                                        ma::VarPtr var{};

                                        // On va créer la variable et tester si le nœud de la valeur est sans erreurs.
                                        if(HasVar(key))
                                        {
                                            var = GetVar(key);
                                        }
                                        else // La variable n'existe pas, il faut la créer.
                                        {
                                            if(rtti)
                                            {
                                                if(var_type_registered.find(type) != var_type_registered.end())
                                                    var = rtti->CreateVar(type, key, GetKey());
                                                else
                                                {
                                                    errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n'
                                                              << L"Le type « " << type
                                                              << L" » de classe de variable n'appartient "
                                                              << L"pas au gestionnaire de type dynamique." << L'\n';
                                                }
                                            }
                                            else
                                            {
                                                errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n'
                                                          << L"Le gestionnaire de types dynamique n'étant pas présent"
                                                          << L"la variable « " << key << L" » et de type « " << type
                                                          << L" » n'a peu être instanciée.";
                                            }
                                        }

                                        if(var)
                                        {
                                            const auto var_desc = L"variable: {"s + GetKey() + L" <"s +
                                                                  GetTypeInfo().m_ClassName + L"> {"s + var->GetKey() +
                                                                  L" <"s + var->GetTypeInfo().m_ClassName + L">}}"s;

                                            const auto add_var_errors = [&](const std::wstring &message,
                                                                            const ma::Serialization::NodePtr &node = {})
                                            {
                                                errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n'
                                                          << message << L'\n';

                                                if(node)
                                                {
                                                    for(const auto &it : ma::GetErrors<ma::InstanceInfo>(node))
                                                        errors_ss << L"Index: "s << it.first << L" "s << it.second
                                                                  << L'\n';

                                                    errors_ss << L"«" << node->m_String << L" » : " << L'\n';
                                                }
                                                errors_ss << var_desc << L'\n';
                                            };

                                            if(var->GetTypeInfo().m_ClassName == type)
                                            {
                                                auto node = value_node->m_Nodes[1u];
                                                const std::wstring value_str{node->m_String};
                                                if(var->IsValidString(value_str))
                                                {
                                                    var->SetFromItem(value_str);

                                                    // Erreur de conception de la variable ?
                                                    // Pour les set, map…, pour le reformatage des chaines de caractères
                                                    // ça va déclencher des avertissements inutiles. if(var->toString()
                                                    // != value_str)
                                                    // {
                                                    //     errors_ss << L"La valeur affectée à la variable n'est pas
                                                    //     identique à ce que la variable renvoie après affectation." <<
                                                    //     L'\n'
                                                    //     << L"Valeur assignée                : " << value_str << L'\n'
                                                    //     << L"Valeur renvoyée par la variable: " << var->toString() <<
                                                    //     L'\n'
                                                    //     << var_desc
                                                    //     << L'\n';
                                                    // }
                                                }
                                                else
                                                    add_var_errors(L"Le nœud ne possède pas une chaîne de caractère "s +
                                                                       L"valide pour la variable."s,
                                                                   node);
                                            }
                                            else
                                                add_var_errors(L"Le type de la variable ne correspond pas à la "s +
                                                               L"variable déjà présente. \n « "s + type + L" » != « "s +
                                                               var->GetTypeInfo().m_ClassName + L" »."s);

                                            if(iinfo_node->m_Nodes.size() >= 2u)
                                            {
                                                auto node = iinfo_node->m_Nodes[1u];
                                                if(ma::IsValidNode<ma::InstanceInfo>(node))
                                                {
                                                    // Le chargement quitte les sauts de lignes et les espaces.
                                                    // auto iinfos = ma::FromNode<ma::InstanceInfo>(node);
                                                    ma::InstanceInfo iinfos;

                                                    for(const auto &it : node->m_Nodes)
                                                    {
                                                        const auto nodes = it->m_Nodes;
                                                        if(nodes.size() >= 2u)
                                                        {
                                                            // std::wcout << error_separator << std::endl;
                                                            // std::cout <<
                                                            // ma::to_string(std::wstring(nodes[1u]->m_String)) <<
                                                            // std::endl; std::wcout << nodes[1u]->m_String <<
                                                            // std::endl;
                                                            iinfos[std::wstring(nodes[0]->m_String)] =
                                                                std::wstring(nodes[1u]->m_String);
                                                        }
                                                    }

                                                    var->InsertIInfo(iinfos);
                                                }
                                                else
                                                    add_var_errors(L"Le nœud d'informations d'instance est invalide."s,
                                                                   node);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n' << L"Le nœud "
                                                  << std::wstring(value_node->m_String) << L" contient"
                                                  << L" seulement un enfant alors qu'il est censé représenter "
                                                  << L"une paire de valeurs." << L'\n';
                                    }
                                }
                            }
                            else
                            {
                                errors_ss << error_separator << L"Ligne: " << __LINE__ << L'\n'
                                          << L"Le nœud suivant est mal formé. Il devrait posséder au moins trois "
                                          << L"enfants \n";
                            }
                        }
                    }

                    // On charge les variables des enfants
                    for(const auto &it : m_Children)
                        it.second->LoadVars();

                    const std::wstring errors_str = L"Attention le fichier « " + item_cpp_file.wstring() +
                                                    L" » est mal formé.\n Voici les erreurs: \n" + errors_ss.str() +
                                                    std::wstring(parser.m_Str);

                    MA_WARNING(!parser.HasError() && errors_ss.str().empty(), errors_str);
                }
                else
                {
                    MA_WARNING(false, L"La version du système de sauvegarde ne correspond à aucune connue.");
                }
            }
        }
        else
        {
            MA_WARNING(false, L"Impossible d'ouvrir le fichier « " + item_cpp_file.wstring() + L" »");
        }
        // On supprime les variables n'appartenant pas à item.cpp
    }

    std::wstring Item::GetTextFromSave(const std::filesystem::directory_entry &dir_entry) const
    {
        std::wstring result;

        if(m_Savable)
        {
            const auto &directory_path = dir_entry.path();

            if(!std::filesystem::exists(directory_path))
            {
                MA_WARNING(false, L"Le dossier de l'item « "s + directory_path.wstring() + L" » n'existe pas."s);
            }
            else if(!std::filesystem::is_directory(directory_path))
            {
                MA_WARNING(false,
                           L"Le dossier de l'item « "s + directory_path.wstring() + L" » n'est pas un dossier mais "s +
                               L"un fichier."s);
            }
            else
            {
                const auto item_cpp_file = directory_path / L"item.cpp";

                if(std::ifstream infile{item_cpp_file, std::ios::ate})
                {
                    const auto stream_size = infile.tellg();
                    std::string ansi_text(stream_size, '\0');
                    infile.seekg(0);
                    if(!infile.read(ansi_text.data(), stream_size))
                    {
                        MA_WARNING(false,
                                   L"Attention le fichier « " + item_cpp_file.wstring() + L" » n'a peu être lu.");
                    }
                    else
                    {
                        auto utf8_text = ma::to_wstring(ansi_text);
                        result = utf8_text;
                    }
                }
            }
        }

        return result;
    }

    Item::header_save_type Item::GetHeaderFromSave(const std::filesystem::directory_entry &dir_entry) const
    {
        Item::header_save_type result;

        const auto text = GetTextFromSave(dir_entry);
        ma::Serialization::Parser parser{Trim(text)};

        const auto root = parser.GetRoot();

        std::wstringstream errors_ss;

        unsigned int version = 0;

        if(root->m_Nodes.empty())
            errors_ss << L"Il n'y a pas de numéro de version du système de sauvegarde utilisé.";
        else
            version = ma::FromNode<std::pair<std::wstring, unsigned int>>(root->m_Nodes[0]).second;

        if(version == 1u)
        {
            if(root->m_Nodes.size() == 1u)
                errors_ss << L"Il n'y a pas d'en-tête.";
            else
            {
                const auto header = root->m_Nodes[1u];
                const auto errors = GetErrors<Item::header_save_type>(header);

                for(const auto &error : errors)
                    errors_ss << toString(error) << L'\n';
                // errors_ss << L"Texte nombre de chr: « " << error[0] << L"» Erreurs: " << error[1] << L'\n';

                if(errors.empty())
                    result = FromNode<Item::header_save_type>(header);
            }

            const std::wstring errors_str = L"Attention le fichier « "s + dir_entry.path().wstring() +
                                            L" » est mal formé. \n Voici les erreurs: « "s + errors_ss.str() +
                                            L" ». "s + std::wstring(parser.m_Str);

            MA_WARNING(!parser.HasError() && errors_ss.str().empty(), errors_str);
        }
        else
        {
            MA_WARNING(false, L"La version du système de sauvegarde ne correspond à aucun connu.");
        }

        return result;
    }

    Item::key_type Item::GetKeyFromSave(const std::filesystem::directory_entry &dir_entry) const
    {
        const auto header = GetHeaderFromSave(dir_entry);
        return std::get<0>(header).second;
    }

    std::wstring Item::GetTypeFromSave(const std::filesystem::directory_entry &dir_entry) const
    {
        const auto header = GetHeaderFromSave(dir_entry);
        return std::get<1u>(header).second;
    }

    Item::body_save_type Item::GetBodyFromSave(const std::filesystem::directory_entry &dir_entry) const
    {
        Item::body_save_type result;

        const auto text = GetTextFromSave(dir_entry);
        ma::Serialization::Parser parser{Trim(text)};

        const auto root = parser.GetRoot();

        std::wstringstream errors_ss;

        unsigned int version = 0;

        if(root->m_Nodes.empty())
            errors_ss << L"Il n'y a pas de numéro de version du système de sauvegarde utilisé.";
        else
            version = ma::FromNode<std::pair<std::wstring, unsigned int>>(root->m_Nodes[0]).second;

        if(version == 1u)
        {
            if(root->m_Nodes.size() == 1u)
                errors_ss << L"Il n'y a pas d'en-tête.";

            if(root->m_Nodes.size() < 3u)
            {
                // if(3u <= root->m_Nodes.size())
                errors_ss << L"Il n'y a pas de corps.";
            }
            else
            {
                const auto body_node = root->m_Nodes[2u];

                const auto errors = ma::GetErrors<Item::body_save_type>(body_node);
                for(const auto &error : errors)
                    errors_ss << toString(error) << L'\n';

                if(errors.empty())
                    result = ma::FromNode<Item::body_save_type>(body_node);
            }

            const std::wstring errors_str = L"Attention le fichier « "s + dir_entry.path().wstring() +
                                            L" » est mal formé.\n"
                                            L"Voici les erreurs: « "s +
                                            errors_ss.str() + L" »."s + std::wstring(parser.m_Str);

            MA_WARNING(!parser.HasError() && errors_ss.str().empty(), errors_str);
        }
        else
        {
            MA_WARNING(false, L"La version du système de sauvegarde ne correspond à aucun connu.");
        }

        return result;
    }

    Item::vars_save_type Item::GetVarsFromSave(const std::filesystem::directory_entry &dir_entry) const
    {
        vars_save_type result;
        const auto body = GetBodyFromSave(dir_entry);

        for(const auto &var : body)
        {
            const auto key = std::get<0>(var).second;
            const auto class_name = std::get<1u>(var).second;
            const auto value = std::get<2u>(var).second;
            const auto iinfos = std::get<3u>(var).second;
            result[key] = {class_name, value, iinfos};
        }

        return result;
    }

    Item::dependencies_type Item::GetDependenciesFromSave(const std::filesystem::directory_entry &dir_entry) const
    {
        Item::dependencies_type dependencies;
        std::wstringstream errors_ss;

        auto vars = GetVarsFromSave(dir_entry);
        auto it_find = vars.find(Key::Var::GetDependencies());
        if(it_find != vars.end())
        {
            const auto keys = std::get<1u>(it_find->second);

            for(const auto &key : ma::FromString<std::vector<key_type>>(keys))
            {
                // test
                // std::wcout << L"key: " << key << L" : value_str" << value_str << std::endl;
                dependencies.push_back(key);
            }
        }

        return dependencies;
    }

    ma::ItemPtr Item::CreateChildFromSave(const std::filesystem::directory_entry &dir_entry)
    {
        return {nullptr};
    }

    void Item::DeleteSaveFolder()
    {
        // suppression du dossier actuel de la sauvegarde de cet item.
        std::filesystem::remove_all(m_LastSavePath->toString());
    }

    void Item::OnEnable()
    {
        Observable::OnEnable();
    }

    void Item::OnDisable()
    {
        Observable::OnDisable();
    }

    const TypeInfo &Item::GetItemTypeInfo()
    {
        // clang-format off
        static const TypeInfo result =
        {
            GetItemClassName(),
            L"La classe «Item» est la classe de base qui définie l'organisation des données de l'API."
            L"Un item peut posséder une liste d'enfants de type « Item » et un parent de type « Item »."
            L"Chaque item est identifié par un GUID unique."
            L"Chaque item actif est stocker dans Item::ms_Map, s'il ne l'ai pas alors il est considéré comme inactif.\n"
            L"\n"
            L"Les différentes clefs prédéfinies:\n"
            L"Pas de parent « " + Key::GetNoParent() + " »:\n"
            L"Un item n'ayant pas de parent utilise cette clef pour définir son parent.\n"
            L"\n"
            L"La racine « " + Key::GetRoot() + " »:\n"
            L"L'item racine est le parent de tous des les items."
            L"Il n'a pas de parent et par conséquent c'est le seul item actif sans parent.\n"
            L"\n"
            L"Le gestionnaire d'observations « " + Key::GetObservationManager() + " »:\n"
            L"   L'item qui gère les observations.\n"
            L"\n"
            L"Le dictionnaire des classes de l'api « " + Key::GetClassInfoManager() + " »:\n"
            L"   L'item qui stocke la description des classes et de leur hiérarchie.\n"
            L"\n"
            L"RTTI « " + Key::GetRTTI() + " »:\n"
            L"   L'item répertoriant les classes qui peuvent être instanciées via une string.",
            {}
        };
        // clang-format on

        return result;
    }

    M_CPP_CLASSHIERARCHY(Item, Observable)
} // namespace ma
