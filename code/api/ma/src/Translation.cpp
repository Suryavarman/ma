/// \file Translation.cpp
/// \author Pontier Pierre
/// \date 2020-11-05
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/wx/wx.h"
#include "ma/Error.h"
#include "ma/Translation.h"

std::wstring ma::Tr(const std::wstring &str)
{
#ifdef wxUSE_UNICODE
    return wxGetTranslation(str).ToStdWstring();
#else
    return wxGetTranslation(str).ToStdWstring();
#endif
}
