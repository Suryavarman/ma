/// \file RTTI.cpp
/// \author Pontier Pierre
/// \date 2019-12-10
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/RTTI.h"
#include "ma/ClassInfo.h"
// http://www.cs.technion.ac.il/users/yechiel/c++-faq/static-init-order.html

namespace ma
{

    // RTTI //——————————————————————————————————————————————————————————————————————————————————————————————————————————
    RTTI::RTTI(const Item::ConstructorParameters &in_params): ma::Item({in_params, Key::GetRTTI(), false})
    {}

    RTTI::~RTTI() = default;

    ItemPtr RTTI::CreateItem(const std::wstring &class_name, const Item::CreateParameters &params) const
    {
        auto makers = GetRegisteredMakerItems();

        auto it_find = makers.find(class_name);
        MA_ASSERT(
            it_find != makers.end(), L"Le type « " + class_name + L" » n'a pas été enregistré.", std::invalid_argument);

        return it_find->second->CreateItem(params);
    }

    VarPtr RTTI::CreateVar(const std::wstring &class_name, const Var::key_type &var_key, const key_type &item_key) const
    {
        auto makers = GetRegisteredMakerVars();

        auto it_find = makers.find(class_name);
        MA_ASSERT(
            it_find != makers.end(), L"Le type « " + class_name + L" » n'a pas été enregistré.", std::invalid_argument);

        return it_find->second->CreateVar(var_key, item_key);
    }

    wxWindow *RTTI::CreatewxVar(const Var::key_type &var_key, const key_type &item_key, wxWindow *parent) const
    {
        auto item = ma::Item::GetItem(item_key);
        auto var = item->GetVar(var_key);

        auto typeinfo = var->GetTypeInfo();
        const auto &class_hierarchy = typeinfo.GetHierarchy();
        const auto &class_name = typeinfo.m_ClassName;
        auto makers = GetRegisteredMakerwxVars();

        auto it_find = makers.find(class_name);

        // Nous parcourons les classes dans l'ordre des enfants vers les parents.
        for(auto it_types = class_hierarchy.cbegin(); it_find == makers.cend() && it_types != class_hierarchy.cend();
            ++it_types)
        {
            const auto &types = (*it_types);
            for(auto it_type = types.cbegin(); it_find == makers.cend() && it_type != types.cend(); ++it_type)
            {
                const ma::TypeInfo &type = *it_type;
                it_find = makers.find(type.m_ClassName);
            }
        }

        MA_ASSERT(it_find != makers.end(),
                  L"Il n'est pas possible de trouver une GUI pour le type « " + class_name + L" ».",
                  std::invalid_argument);

        return it_find->second->CreatewxVar(var_key, item_key, parent);
    }

    MakerItemsMap RTTI::GetRegisteredMakerItems() const
    {
        MakerItemsMap result;

        ItemsMap items = static_cast<const Item *>(this)->GetItems(Item::SearchMod::LOCAL);
        for(const auto &it : items)
        {
            const auto item = it.second;
            const auto &class_name_item_property = MakerItem::Key::GetClassNameItemProperty();
            if(item->HasVar(class_name_item_property))
            {
                const std::wstring class_name = item->GetVar(class_name_item_property)->toString();
                auto it_find = result.find(class_name);
                MA_ASSERT(it_find == result.end(),
                          L"Le «Maker» pour le type " + class_name +
                              L" a été enregistré plus d'une fois. Cela ne devrait pas être le cas.",
                          std::logic_error);

                MakerItemPtr maker = std::dynamic_pointer_cast<MakerItem>(item);
                if(maker)
                {
                    result[class_name] = maker;
                }
            }
        }

        return result;
    }

    MakerVarsMap RTTI::GetRegisteredMakerVars() const
    {
        MakerVarsMap result;

        ItemsMap items = static_cast<const Item *>(this)->GetItems(Item::SearchMod::LOCAL);
        for(const auto &it : items)
        {
            const auto item = it.second;
            const auto &class_name_var_property = MakerVar::Key::GetClassNameVarProperty();
            if(item->HasVar(class_name_var_property))
            {
                const std::wstring class_name = item->GetVar(class_name_var_property)->toString();

                MA_ASSERT(result.find(class_name) == result.end(),
                          L"Le «Maker» pour le type " + class_name +
                              L" a été enregistré plus d'une fois. Cela ne devrait pas être le cas.",
                          std::logic_error);

                MakerVarPtr maker = std::dynamic_pointer_cast<MakerVar>(item);
                if(maker)
                {
                    result[class_name] = maker;
                }
            }
        }

        return result;
    }

    MakerwxVarsMap RTTI::GetRegisteredMakerwxVars() const
    {
        MakerwxVarsMap result;

        ItemsMap items = static_cast<const Item *>(this)->GetItems(Item::SearchMod::LOCAL);
        for(const auto &it : items)
        {
            const auto item = it.second;
            const auto &class_name_wx_var_property = MakerwxVar::Key::GetClassNamewxVarProperty();
            if(item->HasVar(class_name_wx_var_property))
            {
                const std::wstring class_name = item->GetVar(class_name_wx_var_property)->toString();
                auto it_find = result.find(class_name);
                MA_ASSERT(it_find == result.end(),
                          L"Le «Maker» pour le type " + class_name +
                              L" a été enregistré plus d'une fois. Cela ne devrait pas être le cas.",
                          std::logic_error);

                MakerwxVarPtr maker = std::dynamic_pointer_cast<MakerwxVar>(item);
                if(maker)
                {
                    result[maker->GetFabricVarClassName()] = maker;
                }
            }
        }

        return result;
    }

    MakerItemsMap RTTI::GetRegisteredMakerItems(const std::wstring &input_class_name) const
    {
        ma::MakerItemsMap result;
        auto makers = GetRegisteredMakerItems();
        for(auto &&[class_name, maker] : makers)
        {
            if(ma::ClassInfoManager::IsInherit(class_name, input_class_name))
                result[class_name] = maker;
        }

        return result;
    }

    MakerVarsMap RTTI::GetRegisteredMakerVars(const std::wstring &input_class_name) const
    {
        ma::MakerVarsMap result;
        auto makers = GetRegisteredMakerVars();
        for(auto &&[class_name, maker] : makers)
        {
            if(ma::ClassInfoManager::IsInherit(class_name, input_class_name))
                result[class_name] = maker;
        }

        return result;
    }

    MakerwxVarsMap RTTI::GetRegisteredMakerwxVars(const std::wstring &input_class_name) const
    {
        ma::MakerwxVarsMap result;
        auto makers = GetRegisteredMakerwxVars();
        for(auto &&[class_name, maker] : makers)
        {
            if(ma::ClassInfoManager::IsInherit(class_name, input_class_name))
                result[class_name] = maker;
        }

        return result;
    }

    const TypeInfo &RTTI::GetRTTITypeInfo()
    {
        // clang-format off
        static const TypeInfo result =
        {
            GetRTTIClassName(),
            L"La classe RTTI permet de créer un type d'Item via une chaîne de "
            L"caratères. "
            L"Pour initialiser la RTTI il vous faudra choisir les types acceptés par "
            L"l'application. Exemple: "
            L"   void App::InitRTTI() "
            L"   {"
            L"       ma::Item::CreateItem<ma::ItemMaker>({m_Root->rtti->GetKey()});"
            L"   }"
            L"Pour qu'une fabrique d'item (ma::Maker) soit pris en compte il doit "
            L"être enfant de ma::root()->rtti. "
            L"Pour créer un Item à partir d'une string il vous faut appeler la rtti "
            L"de cette manière (le type de l'item étant «Exemple»): "
            L"   ma::ItemPtr item = rtti->CreateItem(ma::ExempleMaker::GetClassName(), {ma::Item::ms_KeyRoot});",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        MakerItem::GetMakerItemClassName(),
                        MakerVar::GetMakerVarClassName(),
                        MakerwxVar::GetMakerwxVarClassName()
                    }
                },
                {
                    TypeInfo::Key::GetVarOrderChildrenDisplay(),
                    {
                        Item::Key::Var::GetName()
                    }
                }
            }
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY(RTTI, Item)

    // MakerItem //—————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Var::key_type &MakerItem::Key::GetClassNameItemProperty()
    {
        static const ma::Var::key_type key{L"m_BuilderClasseNameItem"};
        return key;
    }

    MakerItem::MakerItem(const Item::ConstructorParameters &in_params, const std::wstring &class_name):
    Item(in_params),
    m_ClassName(AddReadOnlyMemberVar<ma::StringVar>({MakerItem::Key::GetClassNameItemProperty()}, class_name))
    {
        MA_ASSERT(m_ClassName,
                  L"Le pointeur partagé «m_ClassName» possède une référence nulle."
                  L"Cela ne devrait pas être le cas."
                  L"Il devrait posséder une variable."
                  L"C'est une erreur interne à l'API.",
                  std::runtime_error);
    }

    MakerItem::~MakerItem() = default;

    const TypeInfo &MakerItem::GetMakerItemTypeInfo()
    {
        // clang-format off
        static const TypeInfo result =
        {
            GetMakerItemClassName(),
            L"Classe abstraite pour fabriquer des items à partir d'une chaîne de caractères.",
            {}
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY(MakerItem, Item)

    // MakerVar //——————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Var::key_type &MakerVar::Key::GetClassNameVarProperty()
    {
        static const ma::Var::key_type key{L"m_BuilderClasseNameVar"};
        return key;
    }

    MakerVar::MakerVar(const Item::ConstructorParameters &in_params, const std::wstring &class_name):
    Item(in_params),
    m_ClassName(AddReadOnlyMemberVar<ma::StringVar>(MakerVar::Key::GetClassNameVarProperty(), class_name))
    {
        MA_ASSERT(m_ClassName,
                  L"Le pointeur partagé «m_ClassName» possède une référence nulle."
                  L"Cela ne devrait pas être le cas."
                  L"Il devrait posséder une variable."
                  L"C'est une erreur interne à l'API.",
                  std::runtime_error);
    }

    MakerVar::~MakerVar() = default;

    const TypeInfo &MakerVar::GetMakerVarTypeInfo()
    {
        // clang-format off
        static const TypeInfo result =
        {
            GetMakerVarClassName(),
            L"Classe abstraite pour fabriquer des variables à partir d'une chaîne de caractères.",
            {}
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY(MakerVar, Item)

    // MakerwxVar //————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Var::key_type &MakerwxVar::Key::GetClassNamewxVarProperty()
    {
        static const ma::Var::key_type key{L"m_BuilderClasseNamewxVar"};
        return key;
    }

    MakerwxVar::MakerwxVar(const Item::ConstructorParameters &in_params, const std::wstring &class_name):
    Item(in_params),
    m_ClassName(AddReadOnlyMemberVar<ma::StringVar>(MakerwxVar::Key::GetClassNamewxVarProperty(), class_name))
    {
        MA_ASSERT(m_ClassName,
                  L"Le pointeur partagé « m_ClassName » possède une référence nulle."
                  L"Cela ne devrait pas être le cas."
                  L"Il devrait posséder une variable."
                  L"C'est une erreur interne à l'API.",
                  std::runtime_error);
    }

    MakerwxVar::~MakerwxVar() = default;

    const TypeInfo &MakerwxVar::GetMakerwxVarTypeInfo()
    {
        // clang-format off
        static const TypeInfo result =
        {
            GetMakerwxVarClassName(),
            L"Classe abstraite pour fabriquer des wxVar à partir d'une chaîne de caractères.",
            {}
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY(MakerwxVar, Item)

    // Déclaration des fabriques d'éléments //——————————————————————————————————————————————————————————————————————————
    // todo les placer si possible à côté de leur déclaration.
    M_CPP_MAKERITEM_TYPE(Item)
    M_CPP_MAKERITEM_TYPE(Folder)

    // Déclaration des fabriques de variables //————————————————————————————————————————————————————————————————————————
    // todo les placer si possible à côté de leur déclaration.
    M_CPP_MAKERVAR_TYPE(DefineVar)
    M_CPP_MAKERVAR_TYPE(StringVar)
    M_CPP_MAKERVAR_TYPE(PathVar)
    M_CPP_MAKERVAR_TYPE(DirectoryVar)
    M_CPP_MAKERVAR_TYPE(FilePathVar)
    M_CPP_MAKERVAR_TYPE(BoolVar)
    M_CPP_MAKERVAR_TYPE(StringSetVar)
    M_CPP_MAKERVAR_TYPE(StringHashSetVar)
    M_CPP_MAKERVAR_TYPE(ItemHashSetVar)
    M_CPP_MAKERVAR_TYPE(ItemVectorVar)
    M_CPP_MAKERVAR_TYPE(ItemVar)
    M_CPP_MAKERVAR_TYPE(IntVar)
    M_CPP_MAKERVAR_TYPE(LongVar)
    M_CPP_MAKERVAR_TYPE(LongLongVar)
    M_CPP_MAKERVAR_TYPE(UVar)
    M_CPP_MAKERVAR_TYPE(ULongVar)
    M_CPP_MAKERVAR_TYPE(ULongLongVar)
    M_CPP_MAKERVAR_TYPE(FloatVar)
    M_CPP_MAKERVAR_TYPE(DoubleVar)
    M_CPP_MAKERVAR_TYPE(LongDoubleVar)
    M_CPP_MAKERVAR_TYPE(EnumerationVar)
    M_CPP_MAKERVAR_TYPE(EnumerationChoiceVar)
} // namespace ma
