/// \file Algorithm.ut.cpp
/// \author Pontier Pierre
/// \date 2021-03-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires du fichier « Algorithm ».
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

using namespace std::literals;

Test(Algorithm, algorithm_trim_all_1)
{
    std::wstring str{L"  t   o t o    "},
                 str_result{L"toto"};

    cr_assert_eq(ma::Trim(str), str_result);
}

Test(Algorithm, algorithm_trim_all_2)
{
    std::wstring str{L"  t\t   \no t o    "},
                 str_result{L"t\noto"};

    cr_assert_eq(ma::Trim(str, true), str_result);
}

Test(Algorithm, algorithm_trim_all_3)
{
    std::wstring str{L"\t{\n\t\t1,\n\t\t2,\n\t\t3\n\t}"},
                 str_result{L"{\n1,\n2,\n3\n}"},
                 trim_result = ma::Trim(str, true);

    cr_assert_eq(trim_result, str_result,
                 "(trim_result : «%s») != (str_result :«%s»)",
                 ma::to_string(std::wstring(trim_result)).c_str(),
                 ma::to_string(std::wstring(str_result)).c_str());
}

Test(Algorithm, algorithm_trim_all_4)
{
    std::wstring str{LR"(  t\t   \no t o    )"},
                 str_result{LR"(t\t\noto)"};

    cr_assert_eq(ma::Trim(str), str_result);
}

Test(Algorithm, algorithm_trim_start_1)
{
    std::wstring_view str{L"  toto"},
                      str_result{L"toto"};

    cr_assert_eq(ma::TrimStart(str), str_result);
    cr_assert_eq(ma::TrimStart(L""sv), L""sv);
}

Test(Algorithm, algorithm_trim_start_2)
{
    std::wstring_view str{L"\t\n\f\r\v  toto"},
                      str_result{L"toto"};

    cr_assert_eq(ma::TrimStart(str), str_result);
}

Test(Algorithm, algorithm_trim_start_3)
{
    std::wstring_view str{L"\t\n\f\r\v  toto"},
                      str_result{L"\n\f\r\v  toto"};

    auto trim_result = ma::TrimStart(str, true);

    cr_assert_eq(trim_result, str_result,
                 R"((trim_result : «%s») != (\n\f\r\v  toto)»)",
                 ma::to_string(std::wstring(trim_result)).c_str());
}

Test(Algorithm, algorithm_trim_start_4)
{
    std::wstring_view str{LR"(\t\n\f\r\v  toto)"},
                      str_result{LR"(\t\n\f\r\v  toto)"};

    cr_assert_eq(ma::TrimStart(str), str_result);
}

Test(Algorithm, algorithm_trim_end_1)
{
    std::wstring_view str{L"toto   "},
                      str_result{L"toto"};

    cr_assert_eq(ma::TrimEnd(str), str_result);
    cr_assert_eq(ma::TrimEnd(L""sv), L""sv);
}

Test(Algorithm, algorithm_trim_end_2)
{
    std::wstring_view str{L"toto   \t\n\f\r\v"},
                      str_result{L"toto"};

    cr_assert_eq(ma::TrimEnd(str), str_result);
}

Test(Algorithm, algorithm_trim_end_3)
{
    std::wstring_view str{L" toto   \t\n\f\r\v \t"},
                      str_result{L" toto   \t\n\f\r\v"};

    cr_assert_eq(ma::TrimEnd(str, true), str_result);
}

Test(Algorithm, algorithm_trim_end_4)
{
    std::wstring_view str{LR"(toto   \t\n\f\r\v  )"},
                      str_result{LR"(toto   \t\n\f\r\v)"};

    cr_assert_eq(ma::TrimEnd(str), str_result);
}

Test(Algorithm, algorithm_trim_startend_1)
{
    std::wstring_view str{L"   toto   "},
                      str_result{L"toto"};

    cr_assert_eq(ma::TrimStartEnd(str), str_result);
    cr_assert_eq(ma::TrimStartEnd(L""sv), L""sv);
}

Test(Algorithm, algorithm_trim_startend_2)
{
    std::wstring_view str{L" \f\r\v  \t toto   \t\n\f\r\v"},
                      str_result{L"toto"};

    cr_assert_eq(ma::TrimStartEnd(str), str_result);
}

Test(Algorithm, algorithm_trim_startend_3)
{
    std::wstring_view str{L" \f\r\v  \t toto   \t\n\f\r\v\t"},
                      str_result{L"\f\r\v  \t toto   \t\n\f\r\v"};

    cr_assert_eq(ma::TrimStartEnd(str, true), str_result);
}

Test(Algorithm, algorithm_trim_startend_4)
{
    std::wstring_view str{LR"( \f\r\v  \t toto   \t\n\f\r\v\t )"},
                      str_result{LR"(\f\r\v  \t toto   \t\n\f\r\v\t)"};

    cr_assert_eq(ma::TrimStartEnd(str, true), str_result);
}
