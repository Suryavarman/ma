/// \file Id.ut.cpp
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires de la classe Id.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

Test(Id, operators)
{
    ma::g_ConsoleVerbose = 1u;
    ma::Id::ClearKeys();

    ma::Id id3(L"16d1bd03-09a5-47d3-944b-5e326fd52d27"),
            id4(L"7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");

    cr_assert_neq(id3, id4);
    cr_assert(!(id3 == id4));

    cr_assert_neq(id3, id4.GetKey());
    cr_assert(!(id3 == id4.GetKey()));

    cr_assert_neq(id3.GetKey(), id4.GetKey());
    cr_assert(!(id3.GetKey() == id4.GetKey()));

    ma::Id::ClearKeys();
}

Test(Id, uniqueness, .description = "Test l'unicité des clefs à la création des Id.")
{
    ma::Id::ClearKeys();

    ma::Id id1,
            id2;

    cr_assert_neq(id1, id2);

    ma::Id::key_type key3(L"7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");
    ma::Id id3(key3),
            id4(L"16d1bd03-09a5-47d3-944b-5e326fd52d27");

    cr_assert_eq(id3.GetKey(), key3);
    cr_assert_neq(id3, id4);

    // Bien nettoyer les Id pour que les tests sur les GUID ne plante pas.
    ma::Id::ClearKeys();
}

Test(Id, uniqueness_caught)
{
    ma::Id::ClearKeys();

    ma::Id::key_type key3(L"7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");
    ma::Id id3(key3);

    cr_assert(!ma::Id::IsUnique(key3));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(ma::Id id4(key3), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    ma::Id::ClearKeys();
}

Test(Id, is_valid)
{
    ma::Id::ClearKeys();

    cr_assert_none_throw(ma::Id(L"7bcd757f-5b10-4f9b-af69-1a1f226f3b3e"));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(ma::Id(L"toto"), std::invalid_argument);
    cr_assert_throw(ma::Id(L"00000000-0000-0000-0000-000000000000"), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    cr_assert_none_throw(ma::Id(L"00000000-0000-0000-0000-000000000001"));
    cr_assert_eq(ma::Id(L"00000000-0000-0000-0000-000000000002").GetKey().size(), ma::Id::GetStrLength());

    ma::Id::ClearKeys();
}

Test(Id, erase_key_of_destroyed_id)
{
    ma::Id::ClearKeys();

    ma::Id::key_type key(L"7bcd757f-5b10-4f9b-af69-1a1f226f3b3e");

    {
        ma::Id id(key);
        cr_assert(!ma::Id::IsUnique(key));
    }

    cr_assert(ma::Id::IsUnique(key));

    {
        ma::Id id(key);
        cr_assert(!ma::Id::IsUnique(key));
    }
}
