/// \file eigen/Transform.cpp
/// \author Pontier Pierre
/// \date 2022-04-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/eigen/Transform.h"

// EIGEN_VAR_TRANSFORM_CPP(i)
// EIGEN_VAR_TRANSFORM_CPP(u)
// EIGEN_VAR_TRANSFORM_CPP(l)
// EIGEN_VAR_TRANSFORM_CPP(ll)

EIGEN_VAR_TRANSFORM_CPP(f)
EIGEN_VAR_TRANSFORM_CPP(d)
EIGEN_VAR_TRANSFORM_CPP(ld)
