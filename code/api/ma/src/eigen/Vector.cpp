/// \file eigen/Vector.cpp
/// \author Pontier Pierre
/// \date 2022-04-01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/eigen/Vector.h"

EIGEN_VAR_VECTOR_CPP(Vector2i)
EIGEN_VAR_VECTOR_CPP(Vector2u)
EIGEN_VAR_VECTOR_CPP(Vector2l)
EIGEN_VAR_VECTOR_CPP(Vector2ll)
EIGEN_VAR_VECTOR_CPP(Vector2f)
EIGEN_VAR_VECTOR_CPP(Vector2d)
EIGEN_VAR_VECTOR_CPP(Vector2ld)

EIGEN_VAR_VECTOR_CPP(Vector3i)
EIGEN_VAR_VECTOR_CPP(Vector3u)
EIGEN_VAR_VECTOR_CPP(Vector3l)
EIGEN_VAR_VECTOR_CPP(Vector3ll)
EIGEN_VAR_VECTOR_CPP(Vector3f)
EIGEN_VAR_VECTOR_CPP(Vector3d)
EIGEN_VAR_VECTOR_CPP(Vector3ld)

EIGEN_VAR_VECTOR_CPP(Vector4i)
EIGEN_VAR_VECTOR_CPP(Vector4u)
EIGEN_VAR_VECTOR_CPP(Vector4l)
EIGEN_VAR_VECTOR_CPP(Vector4ll)
EIGEN_VAR_VECTOR_CPP(Vector4f)
EIGEN_VAR_VECTOR_CPP(Vector4d)
EIGEN_VAR_VECTOR_CPP(Vector4ld)
