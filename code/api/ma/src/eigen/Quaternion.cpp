/// \file eigen/Quaternion.cpp
/// \author Pontier Pierre
/// \date 2022-04-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/eigen/Quaternion.h"

// EIGEN_VAR_QUATERNION_CPP(Quaternioni)
// EIGEN_VAR_QUATERNION_CPP(Quaternionu)
// EIGEN_VAR_QUATERNION_CPP(Quaternionl)
// EIGEN_VAR_QUATERNION_CPP(Quaternionll)

EIGEN_VAR_QUATERNION_CPP(Quaternionf)
EIGEN_VAR_QUATERNION_CPP(Quaterniond)
EIGEN_VAR_QUATERNION_CPP(Quaternionld)
