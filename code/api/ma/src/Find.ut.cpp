/// \file Find.ut.cpp
/// \author Pontier Pierre
/// \date 2020-03-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires des classes de recherches.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

void find_setup()
{
    ma::Item::CreateRoot<ma::Item>();
}

void find_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}
