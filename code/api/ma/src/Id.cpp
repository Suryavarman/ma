/// \file Id.cpp
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/crossguid/guid.h"
#include "ma/Id.h"
#include "ma/Error.h"
#include "ma/String.h"

namespace ma
{
    class IdSmile
    {
        public:
            IdSmile();
            explicit IdSmile(const Id::key_type &guid);
            virtual ~IdSmile();

            /// \see https://guidgenerator.com/
            /// 128-bits is big enough and the generation algorithm is unique enough that if 1.000.000.000 GUIDs per
            /// second were generated for 1 year the probability of a duplicate would be only 50%. Or if every human on
            /// Earth generated 600.000.000 GUIDs there would only be a 50% probability of a duplicate.
            xg::Guid m_Guid;
    };

    Id::Keys Id::ms_Keys;

    IdSmile::IdSmile(): m_Guid(xg::newGuid())
    {}

    IdSmile::IdSmile(const Id::key_type &guid): m_Guid(ma::to_string(guid))
    {}

    IdSmile::~IdSmile() = default;

    Id::Id(): Noncopyable(), m_IdSmile(new IdSmile()), m_Key{ma::to_wstring(m_IdSmile->m_Guid.str())}
    {
        MA_ASSERT(Id::IsUnique(m_Key),
                  L"Le guid a été généré automatiquement. Malgré cela le guid existe déjà! Ce qui ne devrait pas être "
                  L"possible. xg::newGuid() ne fonctionne donc pas correctement.",
                  std::runtime_error);

        MA_ASSERT(Id::IsValid(m_Key),
                  L"L'id généré automatiquement n'est pas valide. Cela ne devrait pas arriver. Il y a un problème avec "
                  L"le module « crossguid ».",
                  std::runtime_error);

        ms_Keys.insert(m_Key);
    }

    Id::Id(const Id::key_type &guid):
    Noncopyable(),
    m_IdSmile(new IdSmile(guid)),
    m_Key{ma::to_wstring(m_IdSmile->m_Guid.str())}
    {
        MA_ASSERT(Id::IsUnique(m_Key),
                  L"Le guid existe déjà « " + m_Key +
                      L" » Utilisé ma::Id::IsUnique(key) avant d'attribuer un guid/clef à un nouveau Id.",
                  std::invalid_argument);

        MA_ASSERT(Id::IsValid(m_Key),
                  L"Le guid « " + m_Key + L" » passé en paramètre n'est pas valide.",
                  std::invalid_argument);

        ms_Keys.insert(m_Key);
    }

    Id::~Id()
    {
        const auto guid = ma::to_wstring(m_IdSmile->m_Guid.str());
        ms_Keys.erase(guid);
        // std::wcout << L"Id::~Id() : " << guid << std::endl;
    }

    Id::key_type Id::GetKey() const
    {
        return m_Key;
    }

    bool Id::IsValid() const
    {
        return m_IdSmile->m_Guid.isValid();
    }

    bool Id::IsValid(const key_type &key)
    {
        return xg::Guid(ma::to_string(key)).isValid();
    }

    bool Id::IsValid(const std::wstring_view &key)
    {
        return xg::Guid(ma::to_string(std::wstring{key})).isValid();
    }

    bool Id::IsUnique(const key_type &key)
    {
        return ms_Keys.find(key) == ms_Keys.end();
    }

    void Id::ClearKeys()
    {
        ms_Keys.clear();
    }

    bool Id::operator==(const Id &other) const
    {
        return this->m_IdSmile->m_Guid.str() == other.m_IdSmile->m_Guid.str();
    }

    bool Id::operator!=(const Id &other) const
    {
        return this->m_IdSmile->m_Guid.str() != other.m_IdSmile->m_Guid.str();
    }

    bool Id::operator==(const Id::key_type &key) const
    {
        return this->m_IdSmile->m_Guid.str() == ma::to_string(key);
    }

    bool Id::operator!=(const Id::key_type &key) const
    {
        return this->m_IdSmile->m_Guid.str() != ma::to_string(key);
    }

    std::string::size_type Id::GetStrLength()
    {
        return 36u;
    }

    Id::key_type Id::GenerateKey()
    {
        return ma::to_wstring(xg::newGuid());
    }
} // namespace ma
