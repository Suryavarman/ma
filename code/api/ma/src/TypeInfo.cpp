/// \file TypeInfo.cpp
/// \author Pontier Pierre
/// \date 2020-03-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/TypeInfo.h"

namespace ma
{
    const TypeInfo::Data::key_type &TypeInfo::Key::GetWhiteListChildrenData()
    {
        static const TypeInfo::Data::key_type result = L"white_list_children";
        return result;
    }

    const TypeInfo::Data::key_type &TypeInfo::Key::GetBlackListChildrenData()
    {
        static const TypeInfo::Data::key_type result = L"black_list_children";
        return result;
    }

    const TypeInfo::Data::key_type &TypeInfo::Key::GetWhiteListParentsData()
    {
        static const TypeInfo::Data::key_type result = L"white_list_parents";
        return result;
    }

    const TypeInfo::Data::key_type &TypeInfo::Key::GetBlackListParentsData()
    {
        static const TypeInfo::Data::key_type result = L"black_list_parents";
        return result;
    }

    const TypeInfo::Data::key_type &TypeInfo::Key::GetVarOrderChildrenDisplay()
    {
        static const TypeInfo::Data::key_type result = L"var_order";
        return result;
    }

    const TypeInfo::Data::key_type &TypeInfo::Key::GetDependenciesData()
    {
        static const TypeInfo::Data::key_type result = L"dependencies";
        return result;
    }

    TypeInfo::Data::mapped_type TypeInfo::GetDependencies() const
    {
        return GetDependencies(m_ClassName);
    }

    TypeInfo::Data::mapped_type TypeInfo::GetDependencies(const std::wstring &class_name)
    {
        TypeInfo::Data::mapped_type result;
        const auto datas = ma::TypeInfo::GetDatas(class_name, TypeInfo::Key::GetDependenciesData());

        for(const auto &data : datas)
        {
            if(data)
            {
                // La première liste de classes sera utilisée.
                const auto &keys = std::get<2u>(*data);

#ifdef __cpp_lib_containers_ranges
                result.insert_range(result.end(), keys);
#else
                result.insert(result.end(), keys.cbegin(), keys.cend());
#endif
            }
        }

        return result;
    }

    QuickAccessHierarchies &GetQuickAccessHierarchyRef()
    {
        static QuickAccessHierarchies quick_access_hierarchy{};

        return quick_access_hierarchy;
    }

    // Un peu barbare :p
    void UpdateQuickAccessHierarchy(QuickAccessHierarchies &quick_access_hierarchy,
                                    const TypeInfo::ClassHierarchies &hierarchies)
    {
        for(const auto &[class_name, hierarchy] : hierarchies)
        {
            QuickAccessHierarchies::mapped_type parents_class_names{};

            for(const auto &parents_level : hierarchy)
                for(const auto &parent_type_info : parents_level)
                    parents_class_names.insert(parent_type_info.m_ClassName);

            quick_access_hierarchy.insert({class_name, parents_class_names});
        }
    }

    const TypeInfo::ClassHierarchies &TypeInfo::AddClassHierarchies(const ClassHierarchy &hierarchy)
    {
        // https://en.cppreference.com/w/cpp/language/storage_duration#Static_local_variables
        static TypeInfo::ClassHierarchies hierarchies{};

        if(!hierarchy.empty())
        {
            MA_ASSERT(hierarchy[0].size() == 1u,
                      L"La classe de base doit être décrite et uniquement elle.",
                      std::invalid_argument);

            hierarchies[hierarchy[0][0].m_ClassName] = hierarchy;

            UpdateQuickAccessHierarchy(GetQuickAccessHierarchyRef(), hierarchies);
        }

        return hierarchies;
    }

    const TypeInfo::ClassHierarchies &TypeInfo::GetClassHierarchies()
    {
        return AddClassHierarchies({});
    }

    const QuickAccessHierarchies &TypeInfo::GetQuickAccessHierarchies()
    {
        auto &quick_access_hierarchy = GetQuickAccessHierarchyRef();

        if(quick_access_hierarchy.empty())
        {
            const auto &hierarchies = TypeInfo::GetClassHierarchies();
            for(const auto &[class_name, hierarchy] : hierarchies)
            {
                QuickAccessHierarchies::mapped_type parents_class_names{};

                for(const auto &parents_level : hierarchy)
                    for(const auto &parent_type_info : parents_level)
                        parents_class_names.insert(parent_type_info.m_ClassName);

                quick_access_hierarchy.insert({class_name, parents_class_names});
            }
        }

        return quick_access_hierarchy;
    }

    const QuickAccessHierarchies::mapped_type &TypeInfo::GetQuickAccessHierarchy() const
    {
        const auto &quick_access_hierarchies = GetQuickAccessHierarchies();

        auto it_find = quick_access_hierarchies.find(m_ClassName);

        MA_ASSERT(it_find != quick_access_hierarchies.end(),
                  L"Ce type «" + m_ClassName + L"» n'a pas été enregistré dans les hiérarchies de classe.",
                  std::runtime_error);

        return it_find->second;
    }

    const ClassHierarchy &TypeInfo::GetHierarchy() const
    {
        const auto &hierarchies = GetClassHierarchies();
        auto it_find = hierarchies.find(m_ClassName);

        MA_ASSERT(it_find != hierarchies.end(),
                  L"Ce type «" + m_ClassName + L"» n'a pas été enregistré dans les hiérarchies de classe.",
                  std::runtime_error);

        return it_find->second;
    }

    TypeInfo::datas_type TypeInfo::GetDatas(const std::wstring &class_name, const TypeInfo::Data::key_type &key)
    {
        TypeInfo::datas_type result{};

        const auto &hierarchies = GetClassHierarchies();

        auto it_find = hierarchies.find(class_name);

        MA_ASSERT(it_find != hierarchies.end(),
                  L"Ce type «" + class_name + L"» n'a pas été enregistré dans les  hiérarchies de classe.",
                  std::runtime_error);

        // La profondeur de la hiérarchie.
        size_t level{0};
        for(const auto &type_infos_level : it_find->second)
        {
            for(const auto &class_type_info : type_infos_level)
            {
                const auto it_find_data = class_type_info.m_Data.find(key);
                result.push_back(
                    {{class_type_info.m_ClassName,
                      level,
                      it_find_data != class_type_info.m_Data.end() ? it_find_data->second :
                                                                     TypeInfo::Data::value_type::second_type{}}});
            }

            level++;
        }

        return result;
    }

    TypeInfo::datas_type TypeInfo::GetDatas(const Data::key_type &key) const
    {
        TypeInfo::datas_type result{};

        const auto &hierarchies = GetClassHierarchies();

        auto it_find = hierarchies.find(m_ClassName);

        MA_ASSERT(it_find != hierarchies.end(),
                  L"Ce type «" + m_ClassName + L"» n'a pas été enregistré dans les hiérarchies de classe.",
                  std::runtime_error);

        // La profondeur de la hiérarchie.
        size_t level{0};
        for(const auto &type_infos_level : it_find->second)
        {
            for(const auto &class_type_info : type_infos_level)
            {
                const auto it_find_data = class_type_info.m_Data.find(key);
                result.push_back(
                    {{class_type_info.m_ClassName,
                      level,
                      it_find_data != class_type_info.m_Data.end() ? it_find_data->second :
                                                                     TypeInfo::Data::value_type::second_type{}}});
            }

            level++;
        }

        return result;
    }

    const TypeInfo &TypeInfo::GetTypeInfo(const std::wstring &class_name)
    {
        const auto &hierarchies = GetClassHierarchies();

        auto it_find = hierarchies.find(class_name);

        MA_ASSERT(it_find != hierarchies.end(),
                  L"Ce type «" + class_name + L"» n'a pas été enregistré dans les hiérarchies de classe.",
                  std::runtime_error);

        return it_find->second[0][0];
    }

    bool TypeInfo::HasTypeInfo(const std::wstring &class_name)
    {
        return GetClassHierarchies().count(class_name) > 0;
    }

    TypeInfo::Data TypeInfo::MergeDatas(const TypeInfo::Data &in_data) const
    {
        Data result = m_Data;

        for(auto &&[key, tail] : in_data)
        {
#ifdef __cpp_lib_containers_ranges
            result[key].append_range(tail);
#else
            auto &head = result[key];
            head.insert(head.end(), tail.cbegin(), tail.cend());
#endif
        }

        return result;
    }
} // namespace ma