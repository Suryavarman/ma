/// \file Sort.ut.cpp
/// \author Pontier Pierre
/// \date 2020-03-16
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires des classes de tris.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

void sort_setup()
{
    ma::Item::CreateRoot<ma::Item>();
}

void sort_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(Sort, sort_var_name, .init = sort_setup, .fini = sort_teardown)
{
    auto item_parent = ma::Item::CreateItem<ma::Item>();
    auto key_parent = item_parent->GetKey();

    ma::ItemPtr item1(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item2(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item3(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item4(ma::Item::CreateItem<ma::Item>({key_parent}));

    ma::Var::key_type key(L"m_Name");

    ma::StringVarPtr var1(item1->AddVar<ma::StringVar>(key, L"d")),
                      var2(item2->AddVar<ma::StringVar>(key, L"c")),
                      var3(item3->AddVar<ma::StringVar>(key, L"a")),
                      var4(item4->AddVar<ma::StringVar>(key, L"b"));

    auto compare = ma::CompareItemsString(key);

    // a, b, c, d => var3, var4, var2, var1
    ma::ItemsOrder result = ma::Sort::SortItems(key_parent, compare, ma::Item::SearchMod::LOCAL);

    cr_assert_eq(result.size(), 4u);

    cr_assert_eq(result[0]->GetVar(key), var3);
    cr_assert_eq(var3->Get(), L"a");

    cr_assert_eq(result[1u]->GetVar(key), var4);
    cr_assert_eq(var4->Get(), L"b");

    cr_assert_eq(result[2u]->GetVar(key), var2);
    cr_assert_eq(var2->Get(), L"c");

    cr_assert_eq(result[3u]->GetVar(key), var1);
    cr_assert_eq(var1->Get(), L"d");
}

Test(Sort, sort_var_int, .init = sort_setup, .fini = sort_teardown)
{
    auto item_parent = ma::Item::CreateItem<ma::Item>();
    auto key_parent = item_parent->GetKey();

    ma::ItemPtr item1(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item2(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item3(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item4(ma::Item::CreateItem<ma::Item>({key_parent}));

    ma::Var::key_type key(L"m_Int");

    ma::IntVarPtr var1(item1->AddVar<ma::IntVar>(key, 10)),
                   var2(item2->AddVar<ma::IntVar>(key, 2)),
                   var3(item3->AddVar<ma::IntVar>(key, -10)),
                   var4(item4->AddVar<ma::IntVar>(key, 1));

    auto compare = ma::CompareItemsInt(key);

    // -10, 1, 2, 10 => var3, var4, var2, var1
    ma::ItemsOrder result = ma::Sort::SortItems(key_parent, compare, ma::Item::SearchMod::LOCAL);

    cr_assert_eq(result.size(), 4u);

    cr_assert_eq(result[0]->GetVar(key), var3);
    cr_assert_eq(var3->Get(), -10);

    cr_assert_eq(result[1u]->GetVar(key), var4);
    cr_assert_eq(var4->Get(), 1);

    cr_assert_eq(result[2u]->GetVar(key), var2);
    cr_assert_eq(var2->Get(), 2);

    cr_assert_eq(result[3u]->GetVar(key), var1);
    cr_assert_eq(var1->Get(), 10);
}

Test(Sort, sort_var_int2, .init = sort_setup, .fini = sort_teardown)
{
    auto item_parent = ma::Item::CreateItem<ma::Item>();
    auto key_parent = item_parent->GetKey();

    ma::ItemPtr item1(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item2(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item3(ma::Item::CreateItem<ma::Item>({key_parent})),
                 item4(ma::Item::CreateItem<ma::Item>({key_parent}));

    ma::Var::key_type key(L"m_Int");

    ma::IntVarPtr var1(item1->AddVar<ma::IntVar>(key, 10)),
                  var2(item2->AddVar<ma::IntVar>(key, 2)),
                  var3(item3->AddVar<ma::IntVar>(key, -10)),
                  var4(item4->AddVar<ma::IntVar>(L"toto", 1));

    auto compare = ma::CompareItemsInt(key);

    // -10, 1, 2, 10 => var3, var4, var2, var1
    ma::ItemsOrder result = ma::Sort::SortItems(item_parent, compare, ma::Item::SearchMod::LOCAL);

    cr_assert_eq(result.size(), 4u);

    cr_assert_eq(result[0]->GetVar(key), var3);
    cr_assert_eq(var3->Get(), -10);

    cr_assert_eq(result[1u]->GetVar(key), var2);
    cr_assert_eq(var2->Get(), 2);

    cr_assert_eq(result[2u]->GetVar(key), var1);
    cr_assert_eq(var1->Get(), 10);

    cr_assert_eq(result[3u]->GetVar(L"toto"), var4);
    cr_assert_eq(var4->Get(), 1);
}
