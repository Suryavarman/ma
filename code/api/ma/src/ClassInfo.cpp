/// \file ClassInfo.cpp
/// \author Pontier Pierre
/// \date 2020-02-11
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <utility>

#include "ma/ClassInfo.h"
#include "ma/Module.h"

const ma::Var::key_type ma::ClassInfo::ms_KeyDescriptionVar{L"m_Description"};
const ma::Var::key_type ma::ClassInfo::ms_KeyParentClassesVar{L"m_ParentClasses"};

namespace ma
{
    // ClassInfo //—————————————————————————————————————————————————————————————————————————————————————————————————————
    ClassInfo::ClassInfo(const Item::ConstructorParameters &in_params, const ClassHierarchy &hierarchy):
    Item(in_params),
    m_Name{},
    m_Description{},
    m_ParentClasses{}
    {
        MA_ASSERT(!hierarchy.empty(), L"La hiérarchie est vide.", std::invalid_argument);

        MA_ASSERT(hierarchy[0].size() == 1u,
                  L"La classe de base doit être décrite et uniquement elle.",
                  std::invalid_argument);

        const auto &info_type = hierarchy[0][0];
        m_Name = AddReadOnlyMemberVar<StringVar>(Key::Var::GetName(), info_type.m_ClassName);
        m_Description = AddReadOnlyMemberVar<StringVar>(ms_KeyDescriptionVar, info_type.m_Description);

        // m_ParentClasses
        {
            auto classes = info_type.GetQuickAccessHierarchy();
            // Le nom de la classe est inclus par défaut.
            // Il faut donc la quitter pour bien représenter les parents.
            classes.erase(info_type.m_ClassName);
            m_ParentClasses = AddReadOnlyMemberVar<StringSetVar>(
                ms_KeyParentClassesVar, StringSetVar::value_type{classes.cbegin(), classes.cend()});
        }

        auto add_list = [this, &info_type](const ma::TypeInfo::Data::key_type &key)
        {
            if(const auto it_find = info_type.m_Data.find(key); it_find != info_type.m_Data.end())
            {
                const auto &elements = it_find->second;
                this->AddReadOnlyMemberVar<StringSetVar>(key,
                                                         StringSetVar::value_type{elements.cbegin(), elements.cend()});
            }
        };

        add_list(ma::TypeInfo::Key::GetWhiteListChildrenData());
        add_list(ma::TypeInfo::Key::GetBlackListChildrenData());
        add_list(ma::TypeInfo::Key::GetWhiteListParentsData());
        add_list(ma::TypeInfo::Key::GetBlackListParentsData());
        add_list(ma::TypeInfo::Key::GetDependenciesData());

        const auto &dep_key = ma::TypeInfo::Key::GetDependenciesData();
        if(const auto it_find = info_type.m_Data.find(dep_key); it_find != info_type.m_Data.end())
        {
            const auto &dependency_keys = it_find->second;

            ItemVectorVar::value_type dependencies;

            for(const auto &dependency_key : dependency_keys)
            {
                if(auto option = HasItemOpt(dependency_key))
                    dependencies.push_back(*option);
                else
                    MA_ASSERT(false,
                              L"Le ClassInfo « " + m_Name->Get() + L" » dépend d'un élément « " + dependency_key +
                                  L" »qui n'est pas présent.",
                              std::invalid_argument);
            }

            this->AddReadOnlyMemberVar<ItemVectorVar>(Key::Var::GetDependencies(), dependencies);
        }
    }

    ClassInfo::~ClassInfo() = default;

    const TypeInfo &ClassInfo::GetClassInfoTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetClassInfoClassName(),
            L"La classe ClassInfo permet de référencer la hiérarchie d'une classe, d'un item ou d'un observateur.",
            {}
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ClassInfo, Item)

    // ClassInfoManager //——————————————————————————————————————————————————————————————————————————————————————————————
    ClassInfoManager::ClassInfoManager(const Item::ConstructorParameters &in_params):
    Item({in_params, Item::Key::GetClassInfoManager(), false})
    {
        BuildHierarchies();
    }

    ClassInfoManager::~ClassInfoManager() = default;

    void ClassInfoManager::BuildHierarchies()
    {
        auto hierarchies = TypeInfo::GetClassHierarchies();
        for(const auto &it : hierarchies)
        {
#if MA_COMPILER == MA_COMPILER_CLANG
            Item::CreateItem<ClassInfo, ClassHierarchy>({GetKey(), true}, it.second);
#else
            Item::CreateItem<ClassInfo>({GetKey(), true}, it.second);
#endif
        }
    }

    // https://stackoverflow.com/questions/14322299/c-stdfind-with-a-custom-comparator
    struct TypeInfoCompare //: public std::unary_function<TypeInfo, bool> // ← déprécier
    {
            std::wstring m_ClassName;

            explicit TypeInfoCompare(std::wstring class_name): m_ClassName(std::move(class_name))
            {}

            bool operator()(const TypeInfo &value) const
            {
                return value.m_ClassName == m_ClassName;
            }
    };

    bool ClassInfoManager::IsInheritDS(const std::wstring &child_class_name, const std::wstring &parent_class_name)
    {
        return IsInheritS(child_class_name, parent_class_name);
    }

    bool ClassInfoManager::IsInheritS(const std::wstring &child_class_name, const std::wstring &parent_class_name)
    {
        //*
        const auto &quick_access_hierarchy = ma::TypeInfo::GetQuickAccessHierarchies();

        auto it_find = quick_access_hierarchy.find(child_class_name);

        MA_ASSERT(it_find != quick_access_hierarchy.end(),
                  L"Le nom de la classe «" + child_class_name + L"» n'est pas référencée ou n'est pas correct.",
                  std::invalid_argument);

        return it_find->second.count(parent_class_name) > 0;
        /*/
        auto hierarchies = TypeInfo::GetClassHierarchies();

        auto it_find = hierarchies.find(child_class_name);

        MA_ASSERT(it_find != hierarchies.end(),
                  L"Le nom de la classe «" + child_class_name + "» n'est pas" \
                  L" référencée ou n'est pas correct.",
                  std::invalid_argument);

        bool result = it_find->first == parent_class_name;

        for(auto it = it_find->second.cbegin(); it != it_find->second.cend() && !result; ++it)
        {
            result = std::find_if(it->cbegin(), it->cend(), TypeInfoCompare(parent_class_name)) != it->cend();
        }

        return result;
        //*/
    }

    bool ClassInfoManager::IsInherit(const ObservablePtr &obs, const std::wstring &parent_class_name)
    {
        MA_ASSERT(obs, L"L'observateur est nulle.", std::invalid_argument);
        return IsInherit(obs->GetTypeInfo().m_ClassName, parent_class_name);
    }

    bool ClassInfoManager::IsInherit(const std::wstring &child_class_name, const std::wstring &parent_class_name)
    {
        if(HasItem(Item::Key::GetClassInfoManager()))
        {
            auto cl_mgr = GetItem<ClassInfoManager>(Item::Key::GetClassInfoManager());
            return cl_mgr->IsInheritDS(child_class_name, parent_class_name);
        }
        else
        {
            return IsInheritS(child_class_name, parent_class_name);
        }
    }

    TypeInfo::ClassHierarchies ClassInfoManager::GetClassHierarchies()
    {
        if(HasItem(Item::Key::GetClassInfoManager()))
        {
            // À implémenter pour la partie dynamique
            return TypeInfo::GetClassHierarchies();
        }
        else
        {
            return TypeInfo::GetClassHierarchies();
        }
    }

    const TypeInfo &ClassInfoManager::GetClassInfoManagerTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetClassInfoManagerClassName(),
            L"La classe ClassInfoManager permet de référencer les hiérarchie des observables et des observateurs "
            L"(Item, Var, wxVar…)",
            {
                {
                    TypeInfo::Key::GetWhiteListChildrenData(),
                    {
                        ClassInfo::GetClassInfoClassName()
                    }
                },
                {
                    TypeInfo::Key::GetVarOrderChildrenDisplay(),
                    {
                        Item::Key::Var::GetName()
                    }
                }
            }
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(ClassInfoManager, Item)
} // namespace ma
