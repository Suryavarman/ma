/// \file Variable.ut.cpp
/// \author Pontier Pierre
/// \date 2019-12-17
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires des classes Variable.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"
#include <sstream>

void variable_setup()
{
    ma::g_ConsoleVerbose = 1u;
    ma::Item::CreateRoot<ma::Item>();
}

void variable_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

class Test_Variable_1 : public ma::Item
{
    private:
        void InitVars()
        {
            m_DefineVar = AddVar<ma::DefineVar>(L"m_DefineVar");
            m_StringVar = AddVar<ma::StringVar>(L"m_StringVar", L"TOTO");
            m_BoolVar = AddVar<ma::BoolVar>(L"m_BoolVar", true);
        }

    public:
        ma::DefineVarPtr m_DefineVar;
        ma::StringVarPtr m_StringVar;
        ma::BoolVarPtr m_BoolVar;

        explicit Test_Variable_1(const ma::Item::ConstructorParameters& params):
        ma::Item(params),
        m_DefineVar(),
        m_StringVar(),
        m_BoolVar()
        {
            InitVars();
        }

        virtual ~Test_Variable_1()
        {}
};
typedef std::shared_ptr<Test_Variable_1> Test_1Ptr;

Test(Variable, has_var, .init = variable_setup, .fini = variable_teardown)
{
    auto default_item = ma::Item::CreateItem<ma::Item>();
    const auto default_count_vars = default_item->GetVars().size();

    Test_1Ptr item = ma::Item::CreateItem<Test_Variable_1>();
    cr_assert_eq(item->GetVars().size(), default_count_vars + 3u);

    cr_assert(item->HasVar(L"m_DefineVar"));
    cr_assert(item->HasVar(L"m_StringVar"));
    cr_assert(item->HasVar(L"m_BoolVar"));
    cr_assert(!item->HasVar(L"m_Toto"));
}

Test(Variable, default_value, .init = variable_setup, .fini = variable_teardown)
{
    auto default_item = ma::Item::CreateItem<ma::Item>();
    const auto default_count_vars = default_item->GetVars().size();

    Test_1Ptr item = ma::Item::CreateItem<Test_Variable_1>();
    cr_assert_eq(item->GetVars().size(), default_count_vars + 3u);

    ma::DefineVarPtr var_define;
    cr_assert_none_throw(var_define = item->GetVar<ma::DefineVar>(L"m_DefineVar"));

    ma::StringVarPtr var_string;
    cr_assert_none_throw(var_string = item->GetVar<ma::StringVar>(L"m_StringVar"));

    ma::BoolVarPtr var_bool;
    cr_assert_none_throw(var_bool = item->GetVar<ma::BoolVar>(L"m_BoolVar"));

    cr_assert_eq(var_define->toString(), L"");
    cr_assert_eq(var_string->Get(), L"TOTO");
    cr_assert_eq(var_string->toString(), L"TOTO");
    cr_assert_eq(var_bool->Get(), true);
    cr_assert_eq(var_bool->toString(), L"true");
}

Test(Variable, copy_var, .init = variable_setup, .fini = variable_teardown)
{
    auto item_1 = ma::Item::CreateItem<ma::Item>();
    auto item_2 = ma::Item::CreateItem<ma::Item>();

    auto var_1 = item_1->AddVar<ma::BoolVar>(L"m_BoolVar_1", true);
    auto var_2 = item_1->AddVar<ma::BoolVar>(L"m_BoolVar_2", false);

    auto var_1_copy = item_2->CopyVar<ma::BoolVar>(var_1);
    auto var_2_copy = item_2->CopyVar<ma::BoolVar>(var_2);

    cr_assert_neq(var_1_copy, var_1);
    cr_assert_eq(var_1->Get(), true);
    cr_assert_eq(var_1_copy->Get(), true);
    cr_assert_eq(var_1->GetItemKey(), item_1->GetKey());
    cr_assert_eq(var_1_copy->GetItemKey(), item_2->GetKey());

    var_1_copy->Set(false);
    cr_assert_eq(var_1->Get(), true);
    cr_assert_eq(var_1_copy->Get(), false);

    var_1->Set(false);
    var_1_copy->Set(true);

    cr_assert_eq(var_1->Get(), false);
    cr_assert_eq(var_1_copy->Get(), true);

    cr_assert_eq(var_2->Get(), false);
    cr_assert_eq(var_2_copy->Get(), false);
}

Test(Variable, boolean_value, .init = variable_setup, .fini = variable_teardown)
{
    auto default_item = ma::Item::CreateItem<ma::Item>();
    const auto default_count_vars = default_item->GetVars().size();

    Test_1Ptr item = ma::Item::CreateItem<Test_Variable_1>();
    cr_assert_eq(item->GetVars().size(), default_count_vars + 3u);

    ma::BoolVarPtr var_bool;
    cr_assert_none_throw(var_bool = item->GetVar<ma::BoolVar>(L"m_BoolVar"));

    cr_assert_eq(var_bool->Get(), true);
    cr_assert_eq(var_bool->toString(), L"true");

    var_bool->Set(false);
    cr_assert_eq(var_bool->Get(), false);
    cr_assert_eq(var_bool->toString(), L"false");

    cr_assert_none_throw(var_bool->fromString(L"true"));
    cr_assert_eq(var_bool->Get(), true);

    cr_assert_none_throw(var_bool->fromString(L"false"));
    cr_assert_eq(var_bool->Get(), false);
}

Test(Variable, string_value, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::StringVarPtr var_string;
    cr_assert_none_throw(var_string = item->AddVar<ma::StringVar>(L"m_StrVar"));

    cr_assert_eq(var_string->Get(), L"");
    cr_assert_eq(var_string->toString(), L"");

    // https://en.cppreference.com/w/cpp/language/string_literal
    const std::wstring value = LR"(\o/)";
    var_string->Set(value);
    cr_assert_eq(var_string->Get(), value);
    cr_assert_eq(var_string->toString(), value);

    cr_assert_none_throw(var_string->fromString(L"toto1"));
    cr_assert_eq(var_string->Get(), "toto1");

    cr_assert_none_throw(var_string->fromString(L"toto2"));
    cr_assert_eq(var_string->Get(), L"toto2");
}

Test(Variable, string_hash_set_var_1, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::StringHashSetVarPtr var;
    cr_assert_none_throw(var = item->AddVar<ma::StringHashSetVar>(L"m_Var"));

    cr_assert_eq(var->Get().size(), 0);

    const auto str = LR"({"TOTO"})"s;
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_2, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const auto str = LR"({"12345", "TOTO1", "TOTO2", "TOTO3"})"s;
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 4u);
    cr_assert_eq(var->Get().count(L"12345"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO1"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO2"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO3"), 1u);
}

Test(Variable, string_hash_set_var_3, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({"TOTO0","TOTO1","TOTO2","TOTO3"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 4u);
    cr_assert_eq(var->Get().count(L"TOTO0"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO1"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO2"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO3"), 1u);
}

Test(Variable, string_hash_set_var_4, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({"TOTO0", "TOTO1", "TOTO2", "TOTO3"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 4u);
    cr_assert_eq(var->Get().count(L"TOTO0"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO1"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO2"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO3"), 1u);
}

Test(Variable, string_hash_set_var_5, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({"TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 4u);
    cr_assert_eq(var->Get().count(L"TOTO0"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO1"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO2}"), 1u);
    cr_assert_eq(var->Get().count(L"{TO,TO,{3"), 1u);
}

Test(Variable, string_hash_set_var_6, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({   "TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3"  })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 4u);
    cr_assert_eq(var->Get().count(L"TOTO0"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO1"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO2}"), 1u);
    cr_assert_eq(var->Get().count(L"{TO,TO,{3"), 1u);
}

Test(Variable, string_hash_set_var_7, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TOTO"  })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_8, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"(          {"TOTO"}                    )";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_9, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TOTO"}     )";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_10, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({""})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L""), 1u);
}

Test(Variable, string_hash_set_var_11, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({""}   )";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L""), 1u);
}

Test(Variable, string_hash_set_var_12, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({" "})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L" "), 1u);
}

Test(Variable, string_hash_set_var_13, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({" TOTO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L" TOTO"), 1u);
}

Test(Variable, string_hash_set_var_14, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TOTO "})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO "), 1u);
}

Test(Variable, string_hash_set_var_15, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({" TOTO "})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L" TOTO "), 1u);
}

Test(Variable, string_hash_set_var_16, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({ " TOTO " , " TOTO2" })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(L" TOTO "), 1u);
    cr_assert_eq(var->Get().count(L" TOTO2"), 1u);
}

Test(Variable, string_hash_set_var_17, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({" TOTO ","TOTO2"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(L" TOTO "), 1u);
    cr_assert_eq(var->Get().count(L"TOTO2"), 1u);
}

Test(Variable, string_hash_set_var_18, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    // Les cas bizarres s
    const std::wstring str = LR"({""TRUC1", "LOLO1"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(LR"("TRUC1)"), 1u);
    cr_assert_eq(var->Get().count(L"LOLO1"), 1u);
}

Test(Variable, string_hash_set_var_19, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"}"TRUC2", "LOLO2"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(LR"(}"TRUC2)"), 1u);
    cr_assert_eq(var->Get().count(L"LOLO2"), 1u);
}

Test(Variable, string_hash_set_var_20, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    // L'expression régulière ne permet pas de récupérer les bonnes valeurs.
    // Du coup « IsValidString » renverra, elle aussi faux.
    const std::wstring str = LR"({"},",{"", "LOLO2"})";
    //cr_assert(var->IsValidString(str));
    //cr_assert_none_throw(var->fromString(str));
    //cr_assert_eq(var->Get().size(), 2u);
    //cr_assert_eq(var->Get().count(R"(},",{")"), 1u);
    //cr_assert_eq(var->Get().count(L"LOLO2"), 1u);
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, string_hash_set_var_21, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    // Pour indiquer le comportement admis dans ces cas bizarres
    const std::wstring str = LR"({"","TOTO"}"})";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    //cr_assert(var->IsValidString(str));
    //cr_assert_none_throw(var->fromString(str));
    //cr_assert_eq(var->Get().size(), 1u);
    //cr_assert_eq(var->Get().count(R"(","TOTO"})"), 1u);
}

Test(Variable, string_hash_set_var_22, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TOTO", "TOTO", "TOTO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_23, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TOTO", "TOTO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_24, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TOTO","TOTO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_25, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TO TO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TO TO"), 1u);
}

Test(Variable, string_hash_set_var_26, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TO TO "})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TO TO "), 1u);
}

Test(Variable, string_hash_set_var_27, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TO TO HA HA HA "})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TO TO HA HA HA "), 1u);
}

Test(Variable, string_hash_set_var_28, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({ "TO TO", "LOL O" })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(L"TO TO"), 1u);
    cr_assert_eq(var->Get().count(L"LOL O"), 1u);
}

Test(Variable, string_hash_set_var_29, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TO TO HA HA HA ", "TOTO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(L"TO TO HA HA HA "), 1u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
}

Test(Variable, string_hash_set_var_30, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TO,TO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TO,TO"), 1u);
}

Test(Variable, string_hash_set_var_31, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({ "TO,TO" })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TO,TO"), 1u);
}

Test(Variable, string_hash_set_var_32, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"( { "TO,TO" } )";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(L"TO,TO"), 1u);
}

Test(Variable, string_hash_set_var_33, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({"TOTO"; "Toto" })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(LR"(TOTO"; "Toto)"), 1u);
}

Test(Variable, string_hash_set_var_34, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    const std::wstring str = LR"({""TOTO"; "Toto"" })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str));
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(LR"("TOTO"; "Toto")"), 1u);
}

Test(Variable, string_hash_set_var_35, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");
    // Les mauvais :
    const std::wstring str = LR"({"TRUC", "})";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, string_hash_set_var_36, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"()";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, string_hash_set_var_37, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    // La structure de chaîne de caractère est bonne
    // Les chaînes vides et les valeurs vides entre virgules représentent une chaîne de caractères vide.
    // Attention s'il n'y a pas de guillemets pour une valeur, celle-ci sera considérée comme une valeur simple.
    // S'il y a un espace entre les deux virgules alors l'espace sera pris en compte.
    const std::wstring str = LR"({"TOTO_1", "", ,, "TOTO_2"})";
    // résultat => «», « », «TOTO_1», «TOTO_2»
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str))
    cr_assert_eq(var->Get().size(), 3u);
    cr_assert_eq(var->Get().count(L"TOTO_1"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO_2"), 1u);
    cr_assert_eq(var->Get().count(L""), 1u);
}

Test(Variable, string_hash_set_var_38, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({"TOTO", })";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str))
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(L"TOTO"), 1u);
    cr_assert_eq(var->Get().count(L""), 1u);
}

Test(Variable, string_hash_set_var_39, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({TOTO"})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str))
    cr_assert_eq(var->Get().size(), 1u);
    cr_assert_eq(var->Get().count(LR"(TOTO")"), 1u);
}

Test(Variable, string_hash_set_var_40, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({"})";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, string_hash_set_var_41, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({"TOTO})";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, string_hash_set_var_42, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({TOTO_1, TOTO_2})";
    cr_assert(var->IsValidString(str));
    cr_assert_none_throw(var->fromString(str))
    cr_assert_eq(var->Get().size(), 2u);
    cr_assert_eq(var->Get().count(L"TOTO_1"), 1u);
    cr_assert_eq(var->Get().count(L"TOTO_2"), 1u);

    // Si on retransforme en chaîne de caractères la structure analysée, les chaînes de caractères seront cette fois-ci
    // délimiter par des guillemets.
    std::wstring fixed_str = L"{";

    size_t i = 0;
    for(auto str_value :var->Get())
    {
        fixed_str += LR"(")" + str_value + (i < var->Get().size() - 1 ? LR"(", )" : LR"(")");
        i++;
    }

    fixed_str += L"}";

    const std::wstring result_str = var->toString();

    cr_assert_eq(result_str, fixed_str,
                 "(var->toString(): %s) != (fixed_str: %s)",
                 ma::to_string(result_str).c_str(),
                 ma::to_string(fixed_str).c_str());
}

Test(Variable, string_hash_set_var_43, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"("TOTO",)";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, string_hash_set_var_44, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({"TOTO0", "TOTO1",    "TOTO2}" , "{TO,TO,{3  })";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, string_hash_set_var_45, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto var = item->AddVar<ma::StringHashSetVar>(L"m_Var");

    const std::wstring str = LR"({   "TOTO0", "TOTO1",    "TOTO2}" , {"TO,TO,{3"  })";
    cr_assert_not(var->IsValidString(str));

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var->fromString(str), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;
}

Test(Variable, item_hash_set_var, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();
    auto item1 = ma::Item::CreateItem<ma::Item>();
    auto item2 = ma::Item::CreateItem<ma::Item>();
    auto item3 = ma::Item::CreateItem<ma::Item>();

    ma::ItemHashSetVarPtr var;
    cr_assert_none_throw(var = item->AddVar<ma::ItemHashSetVar>(L"m_Var"));

    cr_assert_eq(var->Get().size(), 0);

    const std::wstring begin_str = LR"({")";
    const std::wstring end_str = LR"("})";
    const std::wstring separator_str = LR"(", ")";

    {
        const std::wstring str = begin_str + item1->GetKey() + end_str;

        cr_assert(var->IsValidString(str));
        cr_assert_none_throw(var->fromString(str));

        auto items = var->Get();
        cr_assert_eq(items.size(), 1u);
        cr_assert_eq(items.count(item1), 1u);
    }

    {
        const std::wstring str = begin_str + item1->GetKey() + separator_str +
                                             item2->GetKey() + separator_str +
                                             item3->GetKey() + end_str;

        cr_assert(var->IsValidString(str));
        cr_assert_none_throw(var->fromString(str));
        auto items = var->Get();
        cr_assert_eq(items.size(), 3u);
        cr_assert_eq(items.count(item1), 1u);
        cr_assert_eq(items.count(item2), 1u);
        cr_assert_eq(items.count(item3), 1u);
    }
}

Test(Variable, item_hash_set_var_empty, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::ItemHashSetVarPtr var;
    cr_assert_none_throw(var = item->AddVar<ma::ItemHashSetVar>(L"m_Var"));

    cr_assert_eq(var->Get().size(), 0);

    const ma::ItemPtr item_null;

    {
        const std::wstring str = LR"({""})";

        cr_assert(var->IsValidString(str));
        auto toto = ma::FromString<ma::ItemHashSetVar::value_type>(str);

        cr_assert_none_throw(var->fromString(str));

        auto items = var->Get();
        cr_assert_eq(items.size(), 1u);
        cr_assert_eq(items.count(item_null), 1u);
    }

    {
        const std::wstring str = LR"({"", "", ""})";

        cr_assert(var->IsValidString(str));
        cr_assert_none_throw(var->fromString(str));

        auto items = var->Get();

        // C'est un set, les trois item ont la même clef, il ne restera donc
        // qu'un seul item.
        cr_assert_eq(items.size(), 1u,
                     "(items.size(): %zu) != 1u",
                     items.size());
        cr_assert_eq(items.count(item_null), 1u);
    }
}

Test(Variable, item_hash_set_var_insert, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::ItemHashSetVarPtr var;
    cr_assert_none_throw(var = item->AddVar<ma::ItemHashSetVar>(L"m_Var"));

    auto item_0 = ma::Item::CreateItem<ma::Item>();

    cr_assert_none_throw(var->Insert(item_0));
    cr_assert(var->HasItem(item_0));
    cr_assert_none_throw(var->Erase(item_0));
    cr_assert_not(var->HasItem(item_0));
}

Test(Variable, dynamic_var, .init = variable_setup, .fini = variable_teardown)
{
    auto item = ma::Item::CreateItem<ma::Item>();

    ma::DefineVarPtr var_define = item->AddVar<ma::DefineVar>(L"m_DefineVar");
    cr_assert(item->HasVar(L"m_DefineVar"));
    cr_assert_eq(var_define->toString(), L"");

    cr_assert_none_throw(var_define = item->GetVar<ma::DefineVar>(L"m_DefineVar"));
    cr_assert_eq(var_define->toString(), "");
}

template <class T>
void variable_init_number()
{
    variable_setup();
    auto item = ma::Item::CreateItem<ma::Item>();

    auto var_read_only = item->AddReadOnlyVar<T>(L"m_VarReadOnlyNumber");
    cr_assert(var_read_only->m_ReadOnly);
    cr_assert_eq(var_read_only->Get(), 0);

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var_read_only->Set(1), std::logic_error);
    ma::g_ConsoleVerbose = 1u;

    cr_assert_eq(var_read_only->Get(), 0);

    ma::g_ConsoleVerbose = 0;
    cr_assert_throw(var_read_only->Reset(), std::logic_error);
    ma::g_ConsoleVerbose = 1u;

    cr_assert_eq(var_read_only->Get(), 0);

    auto var = item->AddVar<T>(L"m_VarNumber");
    cr_assert(item->HasVar(L"m_VarNumber"));
    cr_assert_eq(var->Get(), 0);
    cr_assert_eq(var->toString(), L"0");

    cr_assert(var->IsValidString(L"0"));
    cr_assert(var->IsValidString(L"1"));
    cr_assert(var->IsValidString(L"01"));
    cr_assert(var->IsValidString(L"001"));
    cr_assert(var->IsValidString(L"  1  "));
    cr_assert_not(var->IsValidString(L"toto"));
    cr_assert_not(var->IsValidString(L"1a"));
    cr_assert_not(var->IsValidString(L"a1"));
    cr_assert_not(var->IsValidString(L"a1a"));
    cr_assert_not(var->IsValidString(L"1  a"));
    cr_assert_not(var->IsValidString(L"a  1"));
    cr_assert_not(var->IsValidString(L"a  1  a"));
    cr_assert_none_throw(var->fromString(L"001"));
    cr_assert_eq(var->Get(), 1);

    cr_assert_none_throw(var->fromString(L"  1  "))
    cr_assert_eq(var->Get(), 1);

    if(var->GetTypeInfo().m_ClassName == ma::IntVar::GetIntVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::LongVar::GetLongVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::LongLongVar::GetLongLongVarClassName())
    {
        cr_assert_not(var->IsValidString(L"1."));
        cr_assert_not(var->IsValidString(L"1.0"));
        cr_assert_not(var->IsValidString(L"1."));
        cr_assert(var->IsValidString(L"-1"));
        cr_assert(var->IsValidString(L"10"));
        cr_assert(var->IsValidString(L"-10"));
    }

    if(var->GetTypeInfo().m_ClassName == ma::UVar::GetUVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::ULongVar::GetULongVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::ULongLongVar::GetULongLongVarClassName())
    {
        cr_assert_not(var->IsValidString(L"1."));
        cr_assert_not(var->IsValidString(L"1.0"));
        cr_assert_not(var->IsValidString(L"1."));
        cr_assert_not(var->IsValidString(L"-1"));
        cr_assert(var->IsValidString(L"10"));
        cr_assert_not(var->IsValidString(L"-10"));
    }

    if(var->GetTypeInfo().m_ClassName == ma::FloatVar::GetFloatVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::DoubleVar::GetDoubleVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::LongDoubleVar::GetLongDoubleVarClassName())
    {
        cr_assert_not(var->IsValidString(L""));
        cr_assert_not(var->IsValidString(L"."));
        cr_assert_not(var->IsValidString(L"1.."));
        cr_assert_not(var->IsValidString(L".1."));
        cr_assert_not(var->IsValidString(L"..1"));
        cr_assert(var->IsValidString(L"1."));
        cr_assert(var->IsValidString(L"2."));
        cr_assert(var->IsValidString(L"3."));
        cr_assert(var->IsValidString(L"4."));
        cr_assert(var->IsValidString(L"-1."));
        cr_assert(var->IsValidString(L"-2."));
        cr_assert(var->IsValidString(L"-3."));
        cr_assert(var->IsValidString(L"-4."));
        cr_assert(var->IsValidString(L".1"));
        cr_assert(var->IsValidString(L".2"));
        cr_assert(var->IsValidString(L".3"));
        cr_assert(var->IsValidString(L".4"));
        cr_assert(var->IsValidString(L"-.1"));
        cr_assert(var->IsValidString(L"-.2"));
        cr_assert(var->IsValidString(L"-.3"));
        cr_assert(var->IsValidString(L"-.4"));
        cr_assert(var->IsValidString(L"+.1"));
        cr_assert(var->IsValidString(L"+.2"));
        cr_assert(var->IsValidString(L"+.3"));
        cr_assert(var->IsValidString(L"+.4"));
        cr_assert(var->IsValidString(L"1.0"));
        cr_assert(var->IsValidString(L"3.0"));
        cr_assert(var->IsValidString(L"4.0"));
        cr_assert(var->IsValidString(L"5.0"));
        cr_assert(var->IsValidString(L"-1.0"));
        cr_assert(var->IsValidString(L"-2.0"));
        cr_assert(var->IsValidString(L"-3.0"));
        cr_assert(var->IsValidString(L"-4.0"));
        cr_assert(var->IsValidString(L"-5.0"));
        cr_assert(var->IsValidString(L"5.0123456"));
        cr_assert(var->IsValidString(L"-5.0123456789"));
        cr_assert(var->IsValidString(L"-1"));
        cr_assert(var->IsValidString(L"10"));
        cr_assert(var->IsValidString(L"-10"));
        cr_assert(var->IsValidString(L"10.0"));
        cr_assert(var->IsValidString(L"-10.0"));
        cr_assert(var->IsValidString(L"-10.01234"));
        cr_assert(var->IsValidString(L"10.01234"));

        // https://stackoverflow.com/questions/392981/how-can-i-convert-string-to-double-in-c
        cr_assert(var->IsValidString(L"0"      ));
        cr_assert(var->IsValidString(L"0."     ));
        cr_assert(var->IsValidString(L"0.0"    ));
        cr_assert(var->IsValidString(L"0.00"   ));
        cr_assert(var->IsValidString(L"0.0e0"  ));
        cr_assert(var->IsValidString(L"0.0e-0" ));
        cr_assert(var->IsValidString(L"0.0e+0" ));
        cr_assert(var->IsValidString(L"+0"     ));
        cr_assert(var->IsValidString(L"+0."    ));
        cr_assert(var->IsValidString(L"+0.0"   ));
        cr_assert(var->IsValidString(L"+0.00"  ));
        cr_assert(var->IsValidString(L"+0.0e0" ));
        cr_assert(var->IsValidString(L"+0.0e-0"));
        cr_assert(var->IsValidString(L"+0.0e+0"));
        cr_assert(var->IsValidString(L"-0"     ));
        cr_assert(var->IsValidString(L"-0."    ));
        cr_assert(var->IsValidString(L"-0.0"   ));
        cr_assert(var->IsValidString(L"-0.00"  ));
        cr_assert(var->IsValidString(L"-0.0e0" ));
        cr_assert(var->IsValidString(L"-0.0e-0"));
        cr_assert(var->IsValidString(L"-0.0e+0"));

        var->fromString(L"0"      ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"0."     ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"0.0"    ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"0.00"   ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"0.0e0"  ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"0.0e-0" ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"0.0e+0" ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"+0"     ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"+0."    ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"+0.0"   ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"+0.00"  ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"+0.0e0" ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"+0.0e-0"); cr_assert_eq(var->Get(), 0);
        var->fromString(L"+0.0e+0"); cr_assert_eq(var->Get(), 0);
        var->fromString(L"-0"     ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"-0."    ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"-0.0"   ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"-0.00"  ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"-0.0e0" ); cr_assert_eq(var->Get(), 0);
        var->fromString(L"-0.0e-0"); cr_assert_eq(var->Get(), 0);
        var->fromString(L"-0.0e+0"); cr_assert_eq(var->Get(), 0);
    }

    using number_type = typename T::value_type;

    number_type value_1 = static_cast<number_type>(1),
                value_min = std::numeric_limits<number_type>::min(),
                value_max = std::numeric_limits<number_type>::max();

    cr_assert_none_throw(var->Set(value_1));
    cr_assert_eq(var->Get(), value_1);

    auto var_bis = item->GetVar<T>(L"m_VarNumber");
    cr_assert_eq(var_bis->Get(), value_1);

    cr_assert_none_throw(var->Set(value_min));
    cr_assert_eq(var_bis->Get(), value_min);

    cr_assert_none_throw(var->Set(value_max));
    cr_assert_eq(var_bis->Get(), value_max);

    // Les réels
    if(var->GetTypeInfo().m_ClassName == ma::FloatVar::GetFloatVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::DoubleVar::GetDoubleVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::LongDoubleVar::GetLongDoubleVarClassName())
    {

    }
    // Les entiers
    else
    {

    }

    std::wstringstream ss_min;
    ss_min.precision(std::numeric_limits<number_type>::digits10);
    ss_min << value_min;

    std::wstringstream ss_max;
    ss_max.precision(std::numeric_limits<number_type>::digits10);
    ss_max << value_max;

    std::wstring value_max_str = ss_max.str();
    std::wstring value_min_str = ss_min.str();

//    M_MSG("var->GetTypeInfo().m_ClassName: " + var->GetTypeInfo().m_ClassName)
//    var->Set(value_min);
//    M_MSG("min    : " + value_min_str)
//    M_MSG("var min: " + var->toString())
//
//    var->Set(value_max);
//    M_MSG("max    : " + value_max_str)
//    M_MSG("var max: " + var->toString())

    cr_assert(var->IsValidString(value_min_str));

    if(var->GetTypeInfo().m_ClassName == ma::FloatVar::GetFloatVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::DoubleVar::GetDoubleVarClassName() ||
       var->GetTypeInfo().m_ClassName == ma::LongDoubleVar::GetLongDoubleVarClassName())
    {
        // sur mon gcc (8)
        // var->IsValidString(value_max_str) renvoie faux
    }
    else
    {
        cr_assert(var->IsValidString(value_max_str));
    }

    cr_assert_none_throw(var->Set(value_max));
    //cr_assert_eq(var->toString(), value_max_str);

    cr_assert_none_throw(var->Set(value_min));
    //cr_assert_eq(var->toString(), value_min_str);

    // Les nombres entiers ont pour limite basse 0. La valeur -1 est déjà testée.
    // Les nombres réels devraient ne pas valider une valeur plus petite que le
    // min.
    if(var->GetTypeInfo().m_ClassName != ma::UVar::GetUVarClassName() &&
       var->GetTypeInfo().m_ClassName != ma::ULongVar::GetULongVarClassName() &&
       var->GetTypeInfo().m_ClassName != ma::ULongLongVar::GetULongLongVarClassName() &&

       var->GetTypeInfo().m_ClassName != ma::FloatVar::GetFloatVarClassName() &&
       var->GetTypeInfo().m_ClassName != ma::DoubleVar::GetDoubleVarClassName() &&
       var->GetTypeInfo().m_ClassName != ma::LongDoubleVar::GetLongDoubleVarClassName())
    {
        cr_assert_not(var->IsValidString(value_min_str + L"01"));
    }

    cr_assert_not(var->IsValidString(value_max_str + L"01"));
}

Test(Variable,        int_var, .init = variable_init_number<ma::IntVar>,        .fini = variable_teardown){}
Test(Variable,       long_var, .init = variable_init_number<ma::LongVar>,       .fini = variable_teardown){}
Test(Variable,   longlong_var, .init = variable_init_number<ma::LongLongVar>,   .fini = variable_teardown){}
Test(Variable,          u_var, .init = variable_init_number<ma::UVar>,          .fini = variable_teardown){}
Test(Variable,      ulong_var, .init = variable_init_number<ma::ULongVar>,      .fini = variable_teardown){}
Test(Variable,  ulonglong_var, .init = variable_init_number<ma::ULongLongVar>,  .fini = variable_teardown){}
Test(Variable,      float_var, .init = variable_init_number<ma::FloatVar>,      .fini = variable_teardown){}
Test(Variable,     double_var, .init = variable_init_number<ma::DoubleVar>,     .fini = variable_teardown){}
Test(Variable, longdouble_var, .init = variable_init_number<ma::LongDoubleVar>, .fini = variable_teardown){}
