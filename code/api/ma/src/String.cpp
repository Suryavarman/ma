/// \file String.cpp
/// \author Pontier Pierre
/// \date 2020-23-12
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/String.h"
#include <string>
#include <codecvt>
#include <locale>
#include <iostream>

// https://en.cppreference.com/w/cpp/locale/wstring_convert/to_bytes
using codec_utf8 = std::codecvt_utf8<wchar_t>;

namespace ma
{
    void SetGlobalLocal()
    {
        // https://en.cppreference.com/w/cpp/locale/locale/locale
        std::locale loc(std::locale::classic(), new codec_utf8);
        std::wcout.imbue(loc);
        std::locale::global(loc);
    }

    std::string to_string(const std::wstring &str)
    {
        std::wstring_convert<codec_utf8, wchar_t> wstring_converter;
        return wstring_converter.to_bytes(str);
    }

    std::wstring to_wstring(const std::string &str)
    {
        std::wstring_convert<codec_utf8, wchar_t> wstring_converter;
        return wstring_converter.from_bytes(str);
    }
} // namespace ma
