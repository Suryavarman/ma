/// \file engine/Context.cpp
/// \author Pontier Pierre
/// \date 2022-11-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/engine/Context.h"
namespace ma::Engine
{
    Context::Context(const Item::ConstructorParameters &in_params, const std::wstring &name):
    ma::Context(in_params, name)
    {}

    Context::~Context() = default;

    const ma::TypeInfo &Context::GetEngineContextTypeInfo()
    {
        const auto context_typeinfo = ma::Context::GetContextTypeInfo();
        // clang-format off
        static const TypeInfo info =
        {
            GetEngineContextClassName(),
            L"La classe « ma::Engine::Context » permet de définir une base commune pour les contexte des moteur 3D/2D.",
            context_typeinfo.MergeDatas({})
        };
        // clang-format on

        return info;
    }
} // namespace ma::Engine
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Context, Engine, ma::Context::GetContextClassHierarchy())
