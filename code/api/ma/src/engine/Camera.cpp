/// \file engine/Camera.cpp
/// \author Pontier Pierre
/// \date 2022-03-28
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/engine/Camera.h"

ENGINE_CAMERA_CPP(f, ma::FloatVar)
ENGINE_CAMERA_CPP(d, ma::DoubleVar)
ENGINE_CAMERA_CPP(ld, ma::LongDoubleVar)
