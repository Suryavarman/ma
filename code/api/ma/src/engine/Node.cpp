/// \file engine/Node.cpp
/// \author Pontier Pierre
/// \date 2022-03-28
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/engine/Node.h"

// ENGINE_NODE_CPP(i)
// ENGINE_NODE_CPP(u)
// ENGINE_NODE_CPP(l)
// ENGINE_NODE_CPP(ll)

ENGINE_NODE_CPP(f)
ENGINE_NODE_CPP(d)
ENGINE_NODE_CPP(ld)
