/// \file engine/Scene.cpp
/// \author Pontier Pierre
/// \date 2022-03-26
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/engine/Scene.h"

// ENGINE_SCENE_CPP(f)
ENGINE_SCENE_CPP(d)
ENGINE_SCENE_CPP(ld)

ma::Engine::Scenef::Scenef(const ma::Item::ConstructorParameters &in_params, const std::wstring &in_name):
ma::Engine::Nodef{in_params, in_name}
{}

ma::Engine::Scenef::~Scenef()
{}

const ma::TypeInfo &ma::Engine::Scenef::GetEngineScenefTypeInfo()
{
    static const TypeInfo info = {GetEngineScenefClassName(),
                                  L"Définition d'une scène 3D ayant pour scalaire le type: «"s +
                                      TypeNameW<scalar_type>() + L"».\n"s,
                                  {{TypeInfo::Key::GetWhiteListChildrenData(), {GetEngineNodefClassName()}},
                                   {TypeInfo::Key::GetWhiteListParentsData(), {ma::Item::GetItemClassName()}}}};

    return info;
}

M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Scenef, Engine, ma::Engine::Nodef::GetEngineNodefClassHierarchy())
M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Scenef, Engine)
