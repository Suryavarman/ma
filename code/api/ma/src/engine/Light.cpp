/// \file engine/Light.cpp
/// \author Pontier Pierre
/// \date 2022-03-28
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/engine/Light.h"

ENGINE_LIGHT_CPP(f, ma::FloatVar)
ENGINE_LIGHT_CPP(d, ma::DoubleVar)
ENGINE_LIGHT_CPP(ld, ma::LongDoubleVar)
