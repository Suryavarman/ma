/// \file engine/Primitives.cpp
/// \author Pontier Pierre
/// \date 2022-03-28
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#include "ma/engine/Primitive.h"

ENGINE_CUBE_CPP(f, ma::FloatVar)
ENGINE_CUBE_CPP(d, ma::DoubleVar)
ENGINE_CUBE_CPP(ld, ma::LongDoubleVar)
