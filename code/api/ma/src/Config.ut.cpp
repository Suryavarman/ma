/// \file Config.ut.cpp
/// \author Pontier Pierre
/// \date 2023-03-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Tests unitaires des classes de type « Config ».
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>
#include "ma/Ma.h"

const auto config_backup_current_dir = std::filesystem::current_path();
const auto config_tests_path = config_backup_current_dir / L"tests"s;

template<const wchar_t* T_Name>
void config_setup()
{
    ma::SetGlobalLocal();

    if (!std::filesystem::exists(config_tests_path))
        std::filesystem::create_directory(config_tests_path);

    const auto test_path = config_tests_path / T_Name;

    if (std::filesystem::exists(test_path))
        std::filesystem::remove_all(test_path);

    std::filesystem::create_directory(test_path);

    std::filesystem::current_path(test_path);
}

void config_teardown()
{
    std::filesystem::current_path(config_backup_current_dir);
}

const wchar_t config_load_test_name[] = L"config_load";
Test(Config, config_load, .init = config_setup<config_load_test_name>, .fini = config_teardown)
{
    std::wstringstream ss;
    ss << L"[toto]\n";
    ss << L"a=0\n";
    ss << L"path=\\home\\toto\n";
    ss << L"[tata]\n";
    ss << LR"#(pair={L"première", 2})#" << L'\n';
    ss << std::endl;

    const auto config_file_path = std::filesystem::current_path() / L"config_load.cfg";
    if(std::wofstream out_file{config_file_path})
    {
        // Pour que le fichier ne soit pas vide.
        // https://www.developpez.net/forums/d2147261/c-cpp/cpp/pourquoi-wifstream-n-arrive-lire-fichier-generer-wofstream
        // https://www.codeproject.com/articles/38242/reading-utf-8-with-c-streams
        out_file << ss.str();

        cr_assert(out_file.good());
    }

    cr_assert(std::filesystem::exists(config_file_path));

    ma::Config config{config_file_path};
    config.Load();

    const auto& datas = config.m_Datas;

    const auto tata_key = L"tata"s;
    const auto toto_key = L"toto"s;

    cr_assert_eq(datas.size(), 2u,
                 R"#((datas.size() = %zu) != 2u)#",
                 datas.size());

    const auto& toto_it_find = datas.find(tata_key);
    const auto& tata_it_find = datas.find(tata_key);

    cr_assert_neq(toto_it_find, datas.end());
    cr_assert_neq(tata_it_find, datas.end());
}

const wchar_t config_save_test_name[] = L"config_save";
Test(Config, config_save, .init = config_setup<config_save_test_name>, .fini = config_teardown)
{
    const auto config_file_path = std::filesystem::current_path()  / L"config_load.cfg";

    ma::Config config(config_file_path);

    const int a = 0;
    const std::filesystem::path path{L"\\home\\toto"};
    const std::pair<std::wstring, int> pair{L"première", 2};

    config.m_Datas =
    {
        {
            L"toto",
            {
                {L"a", ma::toString(a)},
                {L"path", path.wstring()}
            }
        },

        {
            L"tata",
            {
                {L"pair", ma::toString(pair)}
            }
        },
    };

    config.Save();
}