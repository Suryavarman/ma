/// \file Variable.cpp
/// \author Pontier Pierre
/// \date 2019-11-03
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Variable.h"
#include "ma/Algorithm.h"
#include <memory>
#include <regex>
#include <wx/dir.h>

namespace ma
{
    const Observable::Data::key_type Var::Key::Obs::ms_BeginSetValue(L"var_begin_set_value");
    const Observable::Data::key_type Var::Key::Obs::ms_EndSetValue(L"var_end_set_value");

    const Observable::Data::key_type Var::Key::Obs::ms_BeginAddIInfo(L"var_iinfo_begin_add");
    const Observable::Data::key_type Var::Key::Obs::ms_EndAddIInfo(L"var_iinfo_end_add");

    const Observable::Data::key_type Var::Key::Obs::ms_BeginRemoveIInfo(L"var_iinfo_begin_remove");
    const Observable::Data::key_type Var::Key::Obs::ms_EndRemoveIInfo(L"var_iinfo_end_remove");

    const Observable::Data::key_type Var::Key::Obs::ms_BeginSetIInfo(L"var_iinfo_begin_set");
    const Observable::Data::key_type Var::Key::Obs::ms_EndSetIInfo(L"var_iinfo_end_set");

    const Observable::Data::key_type Var::Key::Obs::ms_BeginSetIInfoValue(L"var_iinfo_begin_set_value");
    const Observable::Data::key_type Var::Key::Obs::ms_EndSetIInfoValue(L"var_iinfo_end_set_value");

    const InstanceInfo::key_type Var::Key::IInfo::ms_Doc(ma::Item::Key::Var::GetDoc());

    const Observable::Data::key_type Var::ms_KeyGUI(L"gui");
    const Observable::Data::key_type Var::ms_Sep(L",");

    const Observable::Data::key_type HashSetVar::Key::Obs::ms_BeginInsertValue(L"var_begin_insert_value");
    const Observable::Data::key_type HashSetVar::Key::Obs::ms_EndInsertValue(L"var_end_insert_value");

    const Observable::Data::key_type HashSetVar::Key::Obs::ms_BeginEraseValue(L"var_begin_erase_value");
    const Observable::Data::key_type HashSetVar::Key::Obs::ms_EndEraseValue(L"var_end_erase_value");

    const Observable::Data::key_type SetVar::Key::Obs::ms_BeginInsertValue(L"var_begin_insert_value");
    const Observable::Data::key_type SetVar::Key::Obs::ms_EndInsertValue(L"var_end_insert_value");

    const Observable::Data::key_type SetVar::Key::Obs::ms_BeginEraseValue(L"var_begin_erase_value");
    const Observable::Data::key_type SetVar::Key::Obs::ms_EndEraseValue(L"var_end_erase_value");

    const std::wstring PathVar::ms_KeyMustExist(L"must_exist");

    const std::wstring FilePathVar::ms_KeyExtension(L"extension");
    const char FilePathVar::ms_ExtensionDelimiter(',');

    Var::ConstructorParameters::ConstructorParameters(const Var::key_type &key,
                                                      bool dynamic,
                                                      bool read_only,
                                                      bool savable):
    m_Key(key),
    m_Dynamic(dynamic),
    m_ReadOnly(read_only),
    m_Savable(savable)
    {}

    Var::ConstructorParameters::~ConstructorParameters() = default;

    Var::Var(const Var::ConstructorParameters &params):
    std::enable_shared_from_this<Var>(),
    Observable(),
    m_KeyItem(Item::Key::GetNoParent()),
    m_Key(params.m_Key),
    m_Dynamic(params.m_Dynamic),
    m_ReadOnly(params.m_ReadOnly),
    m_Savable(params.m_Savable)
    {}

    // Pas besoin de faire un EraseVar car si la variable est détruite cela veut
    // dire qu'il n'y a plus de pointeur intelligent qui pointe sur elle.
    Var::~Var() = default;

    std::pair<std::wstring, std::wstring> Var::GetKeyValue(const Var::Action::second_type &str)
    {
        static const std::wstring function_name =
            L"std::pair<std::wstring, std::wstring> Var::GetKeyValue(const Var::Action::second_type& str)";

        auto first_end_pos = str.find_first_of(Var::ms_Sep);

        MA_ASSERT(std::wstring::npos != first_end_pos,
                  L"Aucun séparateur n'a été trouvé dans la chaîne de caractère "
                  L"«str».",
                  function_name,
                  std::invalid_argument);

        auto second_begin_pos = first_end_pos + 1u;

        if(str.size() - second_begin_pos > 0)
            return std::make_pair(str.substr(0, first_end_pos), str.substr(second_begin_pos, str.size()));
        else
            return std::make_pair(str.substr(0, first_end_pos), L"");
    }

    ObservablePtr Var::GetPtr() const
    {
        return Item::GetItem(m_KeyItem)->GetVar(m_Key);
    }

    Item::key_type Var::GetItemKey() const
    {
        return m_KeyItem;
    }

    InstanceInfo Var::GetIInfo() const
    {
        return m_InstanceInfo;
    }

    void Var::InsertIInfoKeyValue(const InstanceInfo::key_type &key, const InstanceInfo::mapped_type &value)
    {
        bool new_key = m_InstanceInfo.find(key) == m_InstanceInfo.end();

        if(new_key)
            UpdateObservations({{Key::Obs::ms_BeginSetIInfoValue, GetKey() + ms_Sep + key}});
        else
            UpdateObservations({{Key::Obs::ms_BeginAddIInfo, GetKey() + ms_Sep + key}});

        m_InstanceInfo[key] = value;

        if(new_key)
            UpdateObservations({{Key::Obs::ms_EndSetIInfoValue, GetKey() + ms_Sep + key}});
        else
            UpdateObservations({{Key::Obs::ms_EndAddIInfo, GetKey() + ms_Sep + key}});
    }

    void Var::InsertIInfo(const InstanceInfo &instance_info)
    {
        if(!instance_info.empty())
        {
            InstanceInfo new_infos;
            InstanceInfo::key_type keys;
            std::vector<Data::key_type> key_finds;
            std::vector<Data::key_type> key_news;

            // On place dans new_infos les couples clef valeurs qui ne vont pas garder la même valeur et les nouveaux
            // couples.
            for(const auto &it : instance_info)
            {
                const auto key = it.first;
                const auto value = it.second;
                const auto &it_find = m_InstanceInfo.find(key);
                if(it_find != m_InstanceInfo.end())
                {
                    if(it.second != it_find->second)
                    {
                        new_infos[key] = value;
                        key_finds.push_back(key);
                        keys += ms_Sep + key;
                    }
                }
                else
                {
                    new_infos[key] = value;
                    key_news.push_back(key);
                    keys += ms_Sep + key;
                }
            }

            UpdateObservations({{Key::Obs::ms_BeginSetIInfo, GetKey() + keys}});

            const auto key_sep{GetKey() + ms_Sep};
            for(const auto &key_find : key_finds)
                UpdateObservations({{Key::Obs::ms_BeginSetIInfoValue, key_sep + key_find}});

            for(const auto &key_new : key_news)
                UpdateObservations({{Key::Obs::ms_BeginAddIInfo, key_sep + key_new}});

            for(const auto &it : new_infos)
                m_InstanceInfo[it.first] = it.second;

            for(const auto &key_find : key_finds)
                UpdateObservations({{Key::Obs::ms_EndSetIInfoValue, key_sep + key_find}});

            for(const auto &key_new : key_news)
                UpdateObservations({{Key::Obs::ms_EndAddIInfo, key_sep + key_new}});

            UpdateObservations({{Key::Obs::ms_EndSetIInfo, GetKey() + keys}});
        }
    }

    InstanceInfo::mapped_type Var::GetIInfoValue(const InstanceInfo::key_type &key)
    {
        const auto &it_find = m_InstanceInfo.find(key);
        MA_ASSERT(it_find != m_InstanceInfo.end(),
                  L"La clef « " + key + L" » n'est pas référencée dans m_InstanceInfo.");

        return it_find->second;
    }

    bool Var::HasIInfoKey(const InstanceInfo::key_type &key)
    {
        return m_InstanceInfo.find(key) != m_InstanceInfo.end();
    }

    void Var::RemoveIInfoKeyValue(const InstanceInfo::key_type &key)
    {
        const auto &it_find = m_InstanceInfo.find(key);
        MA_ASSERT(it_find != m_InstanceInfo.end(),
                  L"La clef « " + key + L" » n'est pas référencée dans m_InstanceInfo.");

        UpdateObservations({{Key::Obs::ms_BeginRemoveIInfo, GetKey() + ms_Sep + key}});
        m_InstanceInfo.erase(it_find);
        UpdateObservations({{Key::Obs::ms_EndRemoveIInfo, GetKey() + ms_Sep + key}});
    }

    size_t Var::GetIInfoCount()
    {
        return m_InstanceInfo.size();
    }

    Var::key_type Var::GetKey() const
    {
        return m_Key;
    }

    bool Var::IsObservable() const
    {
        return GetEndConstruction() && Item::HasItem(m_KeyItem) && Item::GetItem(m_KeyItem)->HasVar(m_Key);
    }

    std::wstring Var::toStringRepresentation() const
    {
        return this->toString();
    }

    Var::ConstructorParameters Var::GetConstructorParameters() const
    {
        return {m_Key, m_Dynamic, m_ReadOnly, m_Savable};
    }

    const TypeInfo &Var::GetVarTypeInfo()
    {
        static const TypeInfo info = {GetVarClassName(), L"Classe de base des variables.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(Var, Observable)

    // T_Var<std::wstring> //———————————————————————————————————————————————————————————————————————————————————————
    T_Var<std::wstring>::T_Var(const Var::ConstructorParameters &params, const value_type &value):
    Var(params),
    m_Value{value},
    m_Previous{},
    m_Default{value}
    {}

    T_Var<std::wstring>::T_Var(const Var::ConstructorParameters &params):
    Var(params),
    m_Value{},
    m_Previous{},
    m_Default{}
    {}

    T_Var<std::wstring>::~T_Var() = default;

    std::wstring T_Var<std::wstring>::toString() const
    {
        return ma::toString(m_Value);
    }

    std::wstring T_Var<std::wstring>::toStringRepresentation() const
    {
        return L'"' + m_Value + L'"';
    }

    std::wstring T_Var<std::wstring>::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    void T_Var<std::wstring>::SetFromItem(const std::wstring &str)
    {
        if(str != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = str;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void T_Var<std::wstring>::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    VarPtr T_Var<std::wstring>::Copy() const
    {
        auto ptr = std::make_shared<T_Var<std::wstring>>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool T_Var<std::wstring>::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<std::wstring>(value);
    }

    void T_Var<std::wstring>::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Previous = m_Value;
        m_Value = m_Default;
        UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    void T_Var<std::wstring>::Set(const value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    const typename T_Var<std::wstring>::value_type &T_Var<std::wstring>::Get() const
    {
        return m_Value;
    }

    const typename T_Var<std::wstring>::value_type &T_Var<std::wstring>::GetPrevious() const
    {
        return m_Previous;
    }

    T_Var<std::wstring> &T_Var<std::wstring>::operator=(const typename T_Var<std::wstring>::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    // StringVar //—————————————————————————————————————————————————————————————————————————————————————————————————

    StringVar::StringVar(const Var::ConstructorParameters &params, const std::wstring &value):
    Var(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}

    StringVar::StringVar(const Var::ConstructorParameters &params, const value_type::value_type *value):
    Var(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}

    StringVar::StringVar(const Var::ConstructorParameters &params, const wxString &value):
    Var(params),
    m_Value(value.ToStdWstring()),
    m_Previous(),
    m_Default(value.ToStdWstring())
    {}

    StringVar::StringVar(const Var::ConstructorParameters &params): Var(params), m_Value(), m_Previous(), m_Default()
    {}

    StringVar::~StringVar() = default;

    std::wstring StringVar::toString() const
    {
        return m_Value;
    }

    std::wstring StringVar::toStringRepresentation() const
    {
        return L'"' + m_Value + L'"';
    }

    std::wstring StringVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    void StringVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    void StringVar::SetFromItem(const std::wstring &value)
    {
        if(m_Value != value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr StringVar::Copy() const
    {
        auto ptr = std::make_shared<StringVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool StringVar::IsValidString(const std::wstring &value) const
    {
        return true;
    }

    void StringVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        if(m_Previous != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = m_Default;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void StringVar::Set(const StringVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    const StringVar::value_type &StringVar::Get() const
    {
        return m_Value;
    }

    const StringVar::value_type &StringVar::GetPrevious() const
    {
        return m_Previous;
    }

    StringVar &StringVar::operator=(const StringVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    bool StringVar::operator!=(const StringVar::value_type &value)
    {
        return m_Value != value;
    }

    bool StringVar::operator==(const StringVar::value_type &value)
    {
        return m_Value == value;
    }

    StringVar &StringVar::operator=(const wxString &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value.ToStdWstring());
        return *this;
    }

    bool StringVar::operator!=(const wxString &value)
    {
        return m_Value != value.ToStdWstring();
    }

    bool StringVar::operator==(const wxString &value)
    {
        return m_Value == value.ToStdWstring();
    }

    const TypeInfo &StringVar::GetStringVarTypeInfo()
    {
        static const TypeInfo info = {
            GetStringVarClassName(), L"C'est variable d'item représente une chaîne de caractère.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(StringVar, Var)
    /*
    EnumVar::EnumVar(const Var::ConstructorParameters& params, const value_type& value, const enum_type&
    enumeration): Var(params), m_Value(value), m_Previous(), m_Default(value)
    {}

    EnumVar::EnumVar(const Var::ConstructorParameters& params, const value_type::value_type* value):
    Var(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}
    */
    /*
    EnumVar::EnumVar(const Var::ConstructorParameters& params, const wxString& value):
    Var(params),
    m_Value(value.ToStdWstring()),
    m_Previous(),
    m_Default(value.ToStdWstring())
    {}

    EnumVar::EnumVar(const Var::ConstructorParameters& params):
    Var(params),
    m_Value(),
    m_Previous(),
    m_Default()
    {}

    EnumVar::~EnumVar()
    {}

    std::wstring EnumVar::toString() const
    {
        return m_Value;
    }

    void EnumVar::fromString(const std::wstring& value)
    {
        MA_ASSERT(!m_ReadOnly,
                  L"La variable est en lecture seule, elle ne peut être modifiée.",
                  std::logic_error);

        SetFromItem(value);
    }

    void EnumVar::SetFromItem(const std::wstring& value)
    {
        if(m_Value != value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr EnumVar::Copy() const
    {
         auto ptr = std::shared_ptr<EnumVar>(new EnumVar(GetConstructorParameters(), m_Value));

         ptr->m_Previous = m_Previous;
         ptr->m_Default = m_Default;

         return ptr;
    }

    bool EnumVar::IsValidString(const std::wstring& value) const
    {
        return true;
    }

    void EnumVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly,
                  L"La variable est en lecture seule, elle ne peut être modifiée.",
                  std::logic_error);

        if(m_Previous != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = m_Default;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void EnumVar::Set(const EnumVar::value_type& value)
    {
        MA_ASSERT(!m_ReadOnly,
                  L"La variable est en lecture seule, elle ne peut être modifiée.",
                  std::logic_error);

        SetFromItem(value);
    }

    const EnumVar::value_type& EnumVar::Get() const
    {
        return m_Value;
    }

    const EnumVar::value_type& EnumVar::GetPrevious() const
    {
        return m_Previous;
    }
    */

    std::filesystem::path Path(const std::wstring &value)
    {
        return PathVar::toPath(value);
    }

    // PathVar //———————————————————————————————————————————————————————————————————————————————————————————————————————
    PathVar::PathVar(const Var::ConstructorParameters &params, const PathVar::value_type &value):
    Var(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}

    PathVar::PathVar(const Var::ConstructorParameters &params, const std::wstring &value):
    PathVar::PathVar(params, toPath(value))
    {}

    PathVar::PathVar(const Var::ConstructorParameters &params):
    Var(params),
    m_Value(),
    m_Previous(),
    m_Default()
    {}

    PathVar::~PathVar() = default;

    std::wstring PathVar::toString() const
    {
        return m_Value.wstring();
    }

    PathVar::value_type PathVar::toPath(const std::wstring &value)
    {
        PathVar::value_type path{value};

        // Les chemins peuvent avoir un nom de dossier vide.
        if(path.stem().wstring().empty() && path.has_parent_path())
            path = path.parent_path();

        return path;
    }

    std::wstring PathVar::toStringRepresentation() const
    {
        return L'"' + this->toString() + L'"';
    }

    std::wstring PathVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value.wstring()));
    }

    void PathVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    bool PathVar::IsValidString(const std::wstring &value) const
    {
        std::filesystem::path path{toPath(value)};
        return IsValidValue(path);
    }

    bool PathVar::IsValidValue(const value_type &path) const
    {
        return !MustExist() || std::filesystem::exists(path);
    }

    void PathVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(m_Default);
    }

    void PathVar::Set(const PathVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    const PathVar::value_type &PathVar::Get() const
    {
        return m_Value;
    }

    const PathVar::value_type &PathVar::GetPrevious() const
    {
        return m_Previous;
    }

    PathVar &PathVar::operator=(const PathVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    PathVar &PathVar::operator=(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    bool PathVar::operator!=(const PathVar::value_type &value)
    {
        return m_Value != value;
    }

    bool PathVar::operator==(const PathVar::value_type &value)
    {
        return m_Value == value;
    }

    bool PathVar::operator!=(const std::wstring &value)
    {
        return m_Value != toPath(value);
    }

    bool PathVar::operator==(const std::wstring &value)
    {
        return m_Value == toPath(value);
    }

    void PathVar::SetFromItem(const std::wstring &value)
    {
        MA_ASSERT(IsValidString(value), L"Le chemin «" + value + L"» n'est pas valide", std::invalid_argument);

        auto path = toPath(value);

        if(path != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = path;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void PathVar::SetFromItem(const PathVar::value_type &value)
    {
        MA_ASSERT(IsValidValue(value),
                  L"La valeur «" + value.wstring() + L"» de type " + ma::TypeNameW<PathVar::value_type>() +
                      L" n'est pas valide",
                  std::invalid_argument);

        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr PathVar::Copy() const
    {
        auto ptr = std::make_shared<PathVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool PathVar::MustExist() const
    {
        bool result = false;
        const auto it_find = m_InstanceInfo.find(ms_KeyMustExist);

        if(it_find != m_InstanceInfo.end())
        {
            const std::wstring str = it_find->second;

            if(BoolVar::IsBool(str))
                result = BoolVar::ToBool(str);
        }

        return result;
    }

    const TypeInfo &PathVar::GetPathVarTypeInfo()
    {
        static const TypeInfo info = {
            GetPathVarClassName(), L"Cette variable d'item représente le chemin d'un fichier ou d'un dossier.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(PathVar, Var)

    // DirectoryVar //——————————————————————————————————————————————————————————————————————————————————————————————————
    bool DirectoryVar::IsValidString(const std::wstring &value) const
    {
        return IsValidValue(toPath(value));
    }

    bool DirectoryVar::IsValidValue(const value_type &value) const
    {
        return !MustExist() || (std::filesystem::exists(value) && std::filesystem::is_directory(value));
    }

    std::filesystem::directory_entry DirectoryVar::toEntry() const
    {
        return std::filesystem::directory_entry(m_Value);
    }

    VarPtr DirectoryVar::Copy() const
    {
        auto ptr = std::make_shared<DirectoryVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    const TypeInfo &DirectoryVar::GetDirectoryVarTypeInfo()
    {
        // @clang-format off
        static const TypeInfo info = {
            GetDirectoryVarClassName(), L"Cette variable d'item représente le chemin d'un dossier.", {}};
        // @clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(DirectoryVar, PathVar)

    // FilePathVar //———————————————————————————————————————————————————————————————————————————————————————————————————
    bool FilePathVar::IsValidString(const std::wstring &value) const
    {
        const auto extensions = GetExtensions();

        bool result = std::filesystem::exists(value);

        if(result)
        {
            auto path = toPath(value);
            result = (!MustExist() || std::filesystem::exists(path)) && std::filesystem::is_regular_file(path);

            if(result)
                result = extensions.empty() || extensions.find(path.extension().wstring()) != extensions.end();
        }

        return result;
    }

    bool FilePathVar::IsValidValue(const value_type &value) const
    {
        bool result = (!MustExist() || std::filesystem::exists(value)) && std::filesystem::is_regular_file(value);

        if(result)
        {
            auto extensions = GetExtensions();
            result = extensions.empty() || extensions.find(value.extension().wstring()) != extensions.end();
        }

        return result;
    }

    std::wstring FilePathVar::GetExt() const
    {
        return m_Value.extension().wstring();
    }

    FilePathVar::Extensions FilePathVar::GetExtensions() const
    {
        FilePathVar::Extensions result;
        auto it_find = m_InstanceInfo.find(ms_KeyExtension);
        if(it_find != m_InstanceInfo.end())
        {
            std::wstring extensions = it_find->second;
            if(!extensions.empty())
            {
                extensions = ma::Trim(extensions);

                if(!extensions.empty())
                {
                    auto split_result = ma::Split(extensions, ms_ExtensionDelimiter);

                    // https://stackoverflow.com/questions/20052674/how-to-convert-vector-to-set
                    // https://techiedelight.com/compiler/
                    std::copy(std::make_move_iterator(split_result.begin()),
                              std::make_move_iterator(split_result.end()),
                              std::inserter(result, result.end()));
                }
            }
        }

        return result;
    }

    bool FilePathVar::Exist() const
    {
        return std::filesystem::exists(m_Value);
    }

    VarPtr FilePathVar::Copy() const
    {
        auto ptr = std::make_shared<FilePathVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    const TypeInfo &FilePathVar::GetFilePathVarTypeInfo()
    {
        // @clang-format off
        static const TypeInfo info = {
            GetFilePathVarClassName(), L"Cette variable d'item représente le chemin d'un fichier.", {}};
        // @clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY(FilePathVar, PathVar)

    // DefineVar //—————————————————————————————————————————————————————————————————————————————————————————————————————
    DefineVar::DefineVar(const Var::ConstructorParameters &params): Var(params)
    {}

    DefineVar::~DefineVar() = default;

    std::wstring DefineVar::toString() const
    {
        return L""s;
    }

    std::wstring DefineVar::toStringRepresentation() const
    {
        return LR"("")"s;
    }

    std::wstring DefineVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, L""s));
    }

    void DefineVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }

    bool DefineVar::IsValidString(const std::wstring &value) const
    {
        return true;
    }

    void DefineVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }

    void DefineVar::SetFromItem(const std::wstring &value)
    {}

    VarPtr DefineVar::Copy() const
    {
        return std::make_shared<DefineVar>(GetConstructorParameters());
    }

    const TypeInfo &DefineVar::GetDefineVarTypeInfo()
    {
        static const TypeInfo info = {GetDefineVarClassName(),
                                      L"Variable d'item de type «define». La présence ou non de cette variable dans un "
                                      L"item est la condition recherchée.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(DefineVar, Var)

    // BoolVar //———————————————————————————————————————————————————————————————————————————————————————————————————————
    BoolVar::BoolVar(const Var::ConstructorParameters &params, bool value):
    Var(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}

    BoolVar::BoolVar(const Var::ConstructorParameters &params): Var(params), m_Value(), m_Previous(), m_Default()
    {}

    BoolVar::~BoolVar() = default;

    std::wstring BoolVar::toString() const
    {
        return ma::toString<bool>(m_Value);
    }

    std::wstring BoolVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    void BoolVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    void BoolVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(m_Default);
    }

    bool BoolVar::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<bool>(value);
    }

    void BoolVar::Set(BoolVar::value_type value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    BoolVar::value_type BoolVar::Get() const
    {
        return m_Value;
    }

    BoolVar::value_type BoolVar::GetPrevious() const
    {
        return m_Previous;
    }

    BoolVar &BoolVar::operator=(BoolVar::value_type value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    void BoolVar::SetFromItem(const std::wstring &value)
    {
        std::wistringstream is(value);
        bool boolean_value = false;
        is >> std::boolalpha >> boolean_value;

        if(boolean_value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = boolean_value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void BoolVar::SetFromItem(const BoolVar::value_type &value)
    {
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr BoolVar::Copy() const
    {
        auto ptr = std::make_shared<BoolVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool BoolVar::IsBool(const std::wstring &value)
    {
        return value == "false" || value == "true";
        //     ||
        //           value == "0" || value == "1" ||
        //           value == "faux" || value == "vrais" ||
        //           value == "off" || value == "on" ||
        //           value == "no"  || value == "yes";
    }

    bool BoolVar::ToBool(const std::wstring &value)
    {
        return ma::FromString<bool>(value);
    }

    std::wstring BoolVar::ToStr(bool value)
    {
        return ma::toString<bool>(value);
    }

    const TypeInfo &BoolVar::GetBoolVarTypeInfo()
    {
        static const TypeInfo info = {GetBoolVarClassName(), L"Cette variable d'item représente un booléen.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(BoolVar, Var)

    // HashSetVar //————————————————————————————————————————————————————————————————————————————————————————————————————
    HashSetVar::HashSetVar(const Var::ConstructorParameters &params): Var(params)
    {}

    HashSetVar::~HashSetVar() = default;

    const TypeInfo &HashSetVar::GetHashSetVarTypeInfo()
    {
        static const TypeInfo info = {GetHashSetVarClassName(),
                                      L"Cette variable d'item représente un conteneur unique non associatif. "
                                      L"Concrètement c'est un std::unordered_set<T>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(HashSetVar, Var)

    // SetVar //————————————————————————————————————————————————————————————————————————————————————————————————————————
    SetVar::SetVar(const Var::ConstructorParameters &params): Var(params)
    {}

    SetVar::~SetVar() = default;

    const TypeInfo &SetVar::GetSetVarTypeInfo()
    {
        static const TypeInfo info = {GetSetVarClassName(),
                                      L"Cette variable d'item représente un conteneur unique non associatif. "
                                      L"Concrètement c'est un std::set<T>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(SetVar, Var)

    // VectorVar //—————————————————————————————————————————————————————————————————————————————————————————————————————
    const Observable::Data::key_type VectorVar::Key::Obs::ms_BeginPushBackValue(L"var_begin_push_back_value");
    const Observable::Data::key_type VectorVar::Key::Obs::ms_EndPushBackValue(L"var_end_push_back_value");

    const Observable::Data::key_type VectorVar::Key::Obs::ms_BeginInsertValue(L"var_begin_insert_value");
    const Observable::Data::key_type VectorVar::Key::Obs::ms_EndInsertValue(L"var_end_insert_value");

    const Observable::Data::key_type VectorVar::Key::Obs::ms_BeginEraseValue(L"var_begin_erase_value");
    const Observable::Data::key_type VectorVar::Key::Obs::ms_EndEraseValue(L"var_end_erase_value");

    VectorVar::VectorVar(const Var::ConstructorParameters &params): Var(params)
    {}

    VectorVar::~VectorVar() = default;

    const TypeInfo &VectorVar::GetVectorVarTypeInfo()
    {
        static const TypeInfo info = {GetVectorVarClassName(),
                                      L"Cette variable d'item représente un conteneur ordonné et non associatif. "
                                      L"Concrètement c'est un std::vector<T>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(VectorVar, Var)

    // StringHashSetVar //——————————————————————————————————————————————————————————————————————————————————————————————
    StringHashSetVar::StringHashSetVar(const Var::ConstructorParameters &params,
                                       const StringHashSetVar::value_type &value):
    HashSetVar(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}

    StringHashSetVar::StringHashSetVar(const Var::ConstructorParameters &params):
    HashSetVar(params),
    m_Value(),
    m_Previous(),
    m_Default()
    {}

    StringHashSetVar::~StringHashSetVar() = default;

    std::wstring StringHashSetVar::toString() const
    {
        return ma::toString(m_Value);
    }

    std::wstring StringHashSetVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    void StringHashSetVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    void StringHashSetVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(m_Default);
    }

    bool StringHashSetVar::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<StringHashSetVar::value_type>(value);
    }

    void StringHashSetVar::Set(const StringHashSetVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    const StringHashSetVar::value_type &StringHashSetVar::Get() const
    {
        return m_Value;
    }

    const StringHashSetVar::value_type &StringHashSetVar::GetPrevious() const
    {
        return m_Previous;
    }

    StringHashSetVar &StringHashSetVar::operator=(const StringHashSetVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    void StringHashSetVar::SetFromItem(const std::wstring &value)
    {
        MA_ASSERT(!value.empty(), L"La chaîne de caractères est vide.", std::invalid_argument);

        MA_ASSERT(value.size() > 1u, L"La chaîne de caractères est invalide.", std::invalid_argument);

        MA_ASSERT(IsValidString(value), L"La chaîne de caractères est invalide.", std::invalid_argument);

        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

        //*/
        m_Value = ma::FromString<StringHashSetVar::value_type>(value);
        /*/
        std::wsmatch sm;
        if(regex_search(value, sm, ms_ReExtractSet))
        {
            m_Previous = m_Value;
            m_Value.clear();

            std::wstring text = value;
            while(regex_search(text, sm, ms_ReExtractSet))
            {
                if(text.size())
                {
                    m_Value.insert(sm.str(1u));
                    text = sm.suffix();
                }
            }
        }
        //*/

        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    void StringHashSetVar::SetFromItem(const StringHashSetVar::value_type &value)
    {
        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Previous = m_Value;
        m_Value = value;
        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    VarPtr StringHashSetVar::Copy() const
    {
        auto ptr = std::make_shared<StringHashSetVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    const TypeInfo &StringHashSetVar::GetStringHashSetVarTypeInfo()
    {
        static const TypeInfo info = {GetStringHashSetVarClassName(),
                                      L"Cette variable d'item représente un conteneur de chaîne de caractère où "
                                      L"l'unicité de chacune d'elle est assurée."
                                      L"Concrètement c'est un std::unordered_set<std::wstring>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(StringHashSetVar, HashSetVar)

    // StringSetVar //——————————————————————————————————————————————————————————————————————————————————————————————————
    StringSetVar::StringSetVar(const Var::ConstructorParameters &params, const StringSetVar::value_type &value):
    SetVar(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}

    StringSetVar::StringSetVar(const Var::ConstructorParameters &params):
    SetVar(params),
    m_Value(),
    m_Previous(),
    m_Default()
    {}

    StringSetVar::~StringSetVar() = default;

    std::wstring StringSetVar::toString() const
    {
        return ma::toString(m_Value);
    }

    std::wstring StringSetVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    void StringSetVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    void StringSetVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(m_Default);
    }

    bool StringSetVar::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<StringSetVar::value_type>(value);
    }

    void StringSetVar::Set(const StringSetVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    const StringSetVar::value_type &StringSetVar::Get() const
    {
        return m_Value;
    }

    const StringSetVar::value_type &StringSetVar::GetPrevious() const
    {
        return m_Previous;
    }

    StringSetVar &StringSetVar::operator=(const StringSetVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    void StringSetVar::SetFromItem(const std::wstring &value)
    {
        MA_ASSERT(!value.empty(), L"La chaîne de caractères est vide.", std::invalid_argument);

        MA_ASSERT(value.size() > 1u, L"La chaîne de caractères est invalide.", std::invalid_argument);

        MA_ASSERT(IsValidString(value), L"La chaîne de caractères est invalide.", std::invalid_argument);

        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

        m_Value = ma::FromString<StringSetVar::value_type>(value);

        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    void StringSetVar::SetFromItem(const StringSetVar::value_type &value)
    {
        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Previous = m_Value;
        m_Value = value;
        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    VarPtr StringSetVar::Copy() const
    {
        auto ptr = std::make_shared<StringSetVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    const TypeInfo &StringSetVar::GetStringSetVarTypeInfo()
    {
        static const TypeInfo info = {GetStringSetVarClassName(),
                                      L"Cette variable d'item représente un conteneur de chaîne de caractère où "
                                      L"l'unicité de chacune d'elle est assurée."
                                      L"Concrètement c'est un std::set<std::wstring>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(StringSetVar, SetVar)

    // StringVectorVar //———————————————————————————————————————————————————————————————————————————————————————————————
    StringVectorVar::StringVectorVar(const Var::ConstructorParameters &params,
                                     const StringVectorVar::value_type &value):
    VectorVar(params),
    m_Value(value),
    m_Previous(),
    m_Default(value)
    {}

    StringVectorVar::StringVectorVar(const Var::ConstructorParameters &params):
    VectorVar(params),
    m_Value(),
    m_Previous(),
    m_Default()
    {}

    StringVectorVar::~StringVectorVar() = default;

    std::wstring StringVectorVar::toString() const
    {
        return ma::toString(m_Value);
    }

    std::wstring StringVectorVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Value));
    }

    void StringVectorVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    void StringVectorVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(m_Default);
    }

    bool StringVectorVar::IsValidString(const std::wstring &value) const
    {
        return ma::IsValidString<StringVectorVar::value_type>(value);
    }

    void StringVectorVar::Set(const StringVectorVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    const StringVectorVar::value_type &StringVectorVar::Get() const
    {
        return m_Value;
    }

    const StringVectorVar::value_type &StringVectorVar::GetPrevious() const
    {
        return m_Previous;
    }

    StringVectorVar &StringVectorVar::operator=(const StringVectorVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    void StringVectorVar::SetFromItem(const std::wstring &value)
    {
        MA_ASSERT(!value.empty(), L"La chaîne de caractères est vide.", std::invalid_argument);

        MA_ASSERT(value.size() > 1u, L"La chaîne de caractères est invalide.", std::invalid_argument);

        MA_ASSERT(IsValidString(value), L"La chaîne de caractères est invalide.", std::invalid_argument);

        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

        m_Value = ma::FromString<StringVectorVar::value_type>(value);

        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    void StringVectorVar::SetFromItem(const StringVectorVar::value_type &value)
    {
        UpdateObservations({{Var::Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
        m_Previous = m_Value;
        m_Value = value;
        UpdateObservations({{Var::Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
    }

    VarPtr StringVectorVar::Copy() const
    {
        auto ptr = std::make_shared<StringVectorVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    const TypeInfo &StringVectorVar::GetStringVectorVarTypeInfo()
    {
        static const TypeInfo info = {GetStringVectorVarClassName(),
                                      L"Cette variable d'item représente un conteneur ordonné, non associatif et de"
                                      L"chaînes de caractères."
                                      L"Concrètement c'est un std::set<std::wstring>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(StringVectorVar, VectorVar)

    // ObservableVar //—————————————————————————————————————————————————————————————————————————————————————————————————
    ObservableVar::ObservableVar(const Var::ConstructorParameters &params): Var(params)
    {}

    ObservableVar::~ObservableVar() = default;

    VarVar::VarVar(const Var::ConstructorParameters &params, const ObservablePtr &var):
    ObservableVar(params),
    m_Var(std::static_pointer_cast<Var>(var))
    {}

    const TypeInfo &ObservableVar::GetObservableVarTypeInfo()
    {
        static const TypeInfo info = {GetObservableVarClassName(),
                                      L"Classe abstraite qui permet de référencer une variable de type"
                                      L" ma::ObservablePtr.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(ObservableVar, Var)

    // VarVar //————————————————————————————————————————————————————————————————————————————————————————————————————————
    VarVar::VarVar(const Var::ConstructorParameters &params): ObservableVar(params), m_Var()
    {}

    VarVar::~VarVar() = default;

    std::wstring VarVar::toString() const
    {
        if(m_Var)
        {
            return ma::toString(m_Var);
        }
        else
        {
            return L""s;
        }
    }

    std::wstring VarVar::toStringRepresentation() const
    {
        return L'"' + this->toString() + L'"';
    }

    std::wstring VarVar::GetKeyValueToString() const
    {
        if(m_Var)
            return ma::toString(std::make_pair(m_Key, m_Var));
        else
            return ma::toString(std::make_pair(m_Key, L""s));
    }

    void VarVar::fromString(const std::wstring &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    bool VarVar::IsValidString(const std::wstring &value) const
    {
        //    bool result = true;
        //
        //    result = result && *value.begin() == ms_Begin;
        //    result = result && *value.rbegin() == ms_End;
        //
        //    std::wstring trim = ma::Trim(value);
        //
        //    //  1 + 36 + 1 +  1 si la longueur de la chaîne le caractère est
        //    // supérieur à 39 alors elle est possiblement valide.
        //    // "{00000000-0000-0000-0000-000000000001,toto}"
        //    result = result && trim.size() > ma::Id::GetStrLength() + 3u;
        //
        //    const std::wstring item_key = trim.substr(1u, ma::Id::GetStrLength() + 1u);
        //    result = result && ma::Id::IsValid(item_key);
        //    result = result && ma::Item::HasItem(item_key);
        //
        //    if(result)
        //    {
        //
        //        const ma::ItemPtr item = ma::Item::GetItem(item_key);
        //        const std::wstring var_key = trim.substr(ma::Id::GetStrLength() + 2u, trim.size() - 1u);
        //        result = result && item->HasVar(var_key);
        //    }
        //
        //    return result;

        return ma::IsValidString<ptr_type>(value);
    }

    void VarVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }

    void VarVar::SetVar(const VarPtr &var)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        if(m_Var != var)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Var = var;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void VarVar::Set(const ObservablePtr &observable)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(observable);
    }

    ObservablePtr VarVar::Get() const
    {
        return m_Var;
    }

    const VarPtr &VarVar::GetVar() const
    {
        return m_Var;
    }

    VarVar &VarVar::operator=(const VarPtr &var)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        if(m_Var != var)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Var = var;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
        return *this;
    }

    ObservableVar &VarVar::operator=(const ObservablePtr &observable)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        if(m_Var != observable)
        {
            VarPtr var = std::dynamic_pointer_cast<Var>(observable);
            MA_ASSERT(
                var,
                L"«observable» n'est pas une variable pointant sur une variable. «observable» doit être du type Var.",
                std::runtime_error);

            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Var = var;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
        return *this;
    }

    void VarVar::SetFromItem(const std::wstring &value)
    {
        auto var = value.empty() ? nullptr : ma::FromString<ptr_type>(value);

        if(var != m_Var)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Var = var;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void VarVar::SetFromItem(const ObservablePtr &observable)
    {
        if(m_Var != observable)
        {
            VarPtr var = std::dynamic_pointer_cast<Var>(observable);
            MA_ASSERT(
                var,
                L"«observable» n'est pas une variable pointant sur une variable. «observable» doit être du type Var.",
                std::runtime_error);
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Var = var;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr VarVar::Copy() const
    {
        return std::make_shared<VarVar>(GetConstructorParameters(), m_Var);
    }

    const TypeInfo &VarVar::GetVarVarTypeInfo()
    {
        static const TypeInfo info = {
            GetVarVarClassName(), L"Cette variable permet de référencer une autre variable.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(VarVar, ObservableVar)

    // ObserverVar //———————————————————————————————————————————————————————————————————————————————————————————————————
    ObserverVar::ObserverVar(const Var::ConstructorParameters &params, const ObserverVar::value_type &value):
    Var(params),
    m_Value(value)
    {}

    ObserverVar::ObserverVar(const Var::ConstructorParameters &params): Var(params), m_Value(nullptr)
    {}

    ObserverVar::~ObserverVar() = default;

    void ObserverVar::Set(const ObserverVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
    }

    const ObserverVar::value_type &ObserverVar::Get() const
    {
        return m_Value;
    }

    ObserverVar &ObserverVar::operator=(const ObserverVar::value_type &value)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(value);
        return *this;
    }

    void ObserverVar::SetFromItem(const ObserverVar::value_type &value)
    {
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    const TypeInfo &ObserverVar::GetObserverVarTypeInfo()
    {
        static const TypeInfo info = {GetObserverVarClassName(),
                                      L"Classe abstraite qui permet de référencer une variable de type "
                                      L"ma::ObserverPtr.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(ObserverVar, Var)

    // IntVar //————————————————————————————————————————————————————————————————————————————————————————————————————————
    void IntVar::SetFromItem(const std::wstring &str)
    {
        int value = std::stoi(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr IntVar::Copy() const
    {
        auto ptr = std::make_shared<IntVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool IntVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_IntRegType))
        {
            try
            {
                std::stoi(value);
                result = true;
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    const TypeInfo &IntVar::GetIntVarTypeInfo()
    {
        static const TypeInfo info = {GetIntVarClassName(), L"Variable d'item de type «int».", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(IntVar, Var)

    // LongVar //——————————————————————————————————————————————————————————————————————————————————————————————————————
    void LongVar::SetFromItem(const std::wstring &str)
    {
        // https://stackoverflow.com/questions/11598990/is-stdstoi-actually-safe-to-use
        long value = std::stol(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr LongVar::Copy() const
    {
        auto ptr = std::make_shared<LongVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool LongVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_IntRegType))
        {
            try
            {
                std::stol(value);
                result = true;
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }
    const TypeInfo &LongVar::GetLongVarTypeInfo()
    {
        static const TypeInfo info = {GetLongVarClassName(), L"Variable d'item de type «long».", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(LongVar, Var)

    // LongLongVar //———————————————————————————————————————————————————————————————————————————————————————————————————
    void LongLongVar::SetFromItem(const std::wstring &str)
    {
        long long value = std::stoll(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr LongLongVar::Copy() const
    {
        auto ptr = std::make_shared<LongLongVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool LongLongVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_IntRegType))
        {
            try
            {
                std::stoll(value);
                result = true;
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    // UVar //——————————————————————————————————————————————————————————————————————————————————————————————————————————
    void UVar::SetFromItem(const std::wstring &str)
    {
        unsigned value = std::stoul(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr UVar::Copy() const
    {
        auto ptr = std::make_shared<UVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool UVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_UIntRegType))
        {
            try
            {
                // Pas de stou
                unsigned long x = std::stoul(value);
                result = std::numeric_limits<unsigned>::max() >= x;
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    const TypeInfo &UVar::GetUVarTypeInfo()
    {
        static const TypeInfo info = {GetUVarClassName(), L"Variable d'item de type «unsigned».", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(UVar, Var)

    // ULongVar //——————————————————————————————————————————————————————————————————————————————————————————————————————
    void ULongVar::SetFromItem(const std::wstring &str)
    {
        unsigned long value = std::stoul(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr ULongVar::Copy() const
    {
        auto ptr = std::make_shared<ULongVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool ULongVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_UIntRegType))
        {
            try
            {
                std::stoul(value);
                result = true;
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    const TypeInfo &ULongVar::GetULongVarTypeInfo()
    {
        static const TypeInfo info = {GetULongVarClassName(), L"Variable d'item de type «unsigned long»", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(ULongVar, Var)

    // ULongLongVar //——————————————————————————————————————————————————————————————————————————————————————————————————
    void ULongLongVar::SetFromItem(const std::wstring &str)
    {
        unsigned long long value = std::stoull(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr ULongLongVar::Copy() const
    {
        auto ptr = std::make_shared<ULongLongVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool ULongLongVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_UIntRegType))
        {
            try
            {
                std::stoull(value);
                result = true;
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    const TypeInfo &ULongLongVar::GetULongLongVarTypeInfo()
    {
        static const TypeInfo info = {GetULongLongVarClassName(), L"Variable d'item de type «unsigned long long»", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(ULongLongVar, Var)

    // FloatVar //——————————————————————————————————————————————————————————————————————————————————————————————————————
    void FloatVar::SetFromItem(const std::wstring &str)
    {
        float value = std::stof(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr FloatVar::Copy() const
    {
        auto ptr = std::make_shared<FloatVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    // https://stackoverflow.com/questions/447206/c-isfloat-function
    bool FloatVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_RealRegType))
        {
            try
            {
                //            auto trim = Trim(value);
                //            M_MSG("value: " + value)
                //            M_MSG("trim: " + trim)
                std::wistringstream iss(Trim(value));
                float x;
                iss >> x;
                result = iss && iss.eof();
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    const TypeInfo &FloatVar::GetFloatVarTypeInfo()
    {
        static const TypeInfo info = {GetFloatVarClassName(), L"Variable d'item de type «float»", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(FloatVar, Var)

    // DoubleVar //—————————————————————————————————————————————————————————————————————————————————————————————————————
    void DoubleVar::SetFromItem(const std::wstring &str)
    {
        double value = std::stod(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr DoubleVar::Copy() const
    {
        auto ptr = std::make_shared<DoubleVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool DoubleVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_RealRegType))
        {
            try
            {
                // std::stod(value);
                // https://stackoverflow.com/questions/392981/how-can-i-convert-string-to-double-in-c
                // https://stackoverflow.com/questions/447206/c-isfloat-function
                std::wistringstream iss(Trim(value));
                double x;
                iss >> x;
                result = iss && iss.eof();
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    const TypeInfo &DoubleVar::GetDoubleVarTypeInfo()
    {
        static const TypeInfo info = {GetDoubleVarClassName(), L"Variable d'item de type «double»", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(DoubleVar, Var)

    // LongDouble //————————————————————————————————————————————————————————————————————————————————————————————————————
    void LongDoubleVar::SetFromItem(const std::wstring &str)
    {
        long double value = std::stold(str);
        if(value != m_Value)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Previous = m_Value;
            m_Value = value;
            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr LongDoubleVar::Copy() const
    {
        auto ptr = std::make_shared<LongDoubleVar>(GetConstructorParameters(), m_Value);

        ptr->m_Previous = m_Previous;
        ptr->m_Default = m_Default;

        return ptr;
    }

    bool LongDoubleVar::IsValidString(const std::wstring &value) const
    {
        bool result = false;
        if(std::regex_match(value, g_RealRegType))
        {
            try
            {
                std::wistringstream iss(Trim(value));
                long double x;
                iss >> x;
                result = iss && iss.eof();
            }
            catch(const std::exception &e)
            {
                result = false;
            }
        }
        return result;
    }

    const TypeInfo &LongDoubleVar::GetLongDoubleVarTypeInfo()
    {
        static const TypeInfo info = {GetLongDoubleVarClassName(), L"Variable d'item de type «long double»", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(LongDoubleVar, Var)

    // ItemObserverVar //———————————————————————————————————————————————————————————————————————————————————————————————
    void ItemObserverVar::SetFromItem(const ItemObserverVar::value_type &value)
    {
        bool different = value != nullptr;
        if(m_Item)
            different = m_Item.get() != dynamic_cast<Item *>(value);

        if(different)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

            m_Value = value;

            if(m_Value)
            {
                auto *item = dynamic_cast<Item *>(value);
                MA_ASSERT(item, L"Impossible de convertir l'observateur en ma::Item.", std::invalid_argument);

                MA_ASSERT(Item::HasItem(item->GetKey()), L"L'item reçu n'est plus actif.", std::invalid_argument);

                m_Item = Item::GetItem(item->GetKey());
            }
            else
                m_Item = nullptr;

            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void ItemObserverVar::SetFromItem(const std::wstring &str)
    {
        static const std::wstring function_name(L"void ma::ItemObserverVar::SetFromItem(const std::wstring& str)");

        bool different = Item::HasItem(str);
        if(m_Item)
            different = m_Item->GetKey() != str;

        if(different)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Item = Item::GetItem(str);
            if(m_Item)
            {
                m_Value = dynamic_cast<ObserverPtr>(m_Item.get());
                MA_ASSERT(m_Value, L"Impossible de convertir l'item en observateur.", std::invalid_argument);
            }
            else
                m_Value = nullptr;

            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr ItemObserverVar::Copy() const
    {
        return std::make_shared<ItemObserverVar>(GetConstructorParameters(), m_Value);
    }

    std::wstring ItemObserverVar::toString() const
    {
        return m_Item ? m_Item->GetKey() : L""s;
    }

    std::wstring ItemObserverVar::toStringRepresentation() const
    {
        return L'"' + (m_Item ? m_Item->GetKey() : L""s) + L'"';
    }

    std::wstring ItemObserverVar::GetKeyValueToString() const
    {
        if(m_Item)
            return ma::toString(std::make_pair(m_Key, m_Item->GetKey()));
        else
            return ma::toString(std::make_pair(m_Key, L""s));
    }

    void ItemObserverVar::fromString(const std::wstring &str)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(str);
    }

    bool ItemObserverVar::IsValidString(const std::wstring &str) const
    {
        return str.empty() || Item::HasItem(str);
    }

    bool ItemObserverVar::IsValid() const
    {
        return !m_Item || Item::HasItem(m_Item->GetKey());
    }

    void ItemObserverVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }

    const TypeInfo &ItemObserverVar::GetItemObserverVarTypeInfo()
    {
        static const TypeInfo info = {
            GetItemObserverVarClassName(), L"Cette variable permet de référencer un item observateur.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(ItemObserverVar, ObservableVar)

    // VarObserverVar //————————————————————————————————————————————————————————————————————————————————————————————————
    void VarObserverVar::SetFromItem(const VarObserverVar::value_type &value)
    {
        bool different = value != nullptr;
        if(m_Var)
            different = m_Var.get() != dynamic_cast<Var *>(value);

        if(different)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});

            m_Value = value;

            if(m_Value)
            {
                auto *var = dynamic_cast<Var *>(value);
                MA_ASSERT(var, L"Impossible de convertir l'observateur en Var.", std::invalid_argument);

                MA_ASSERT(Item::HasItem(var->GetItemKey()),
                          L"La variable appartient à un item inactif.",
                          std::invalid_argument);

                auto item = Item::GetItem(var->GetItemKey());

                MA_ASSERT(item->HasVar(var->GetKey()),
                          L"La variable n'appartient pas à l'item auquel elle est censée appartenir.",
                          std::runtime_error);
            }
            else
                m_Var = nullptr;

            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    void VarObserverVar::SetFromItem(const std::wstring &str)
    {
        static const std::wstring function_name(L"void VarObserverVar::SetFromItem(const std::wstring& str)");

        auto var = ma::FromString<VarPtr>(str);

        if(m_Var != var)
        {
            UpdateObservations({{Key::Obs::ms_BeginSetValue, GetKey() + ms_Sep + this->toString()}});
            m_Var = var;
            if(m_Var)
            {
                m_Value = dynamic_cast<ObserverPtr>(m_Var.get());
                MA_ASSERT(m_Value, L"Impossible de convertir la variable en observateur.", std::invalid_argument);
            }
            else
                m_Value = nullptr;

            UpdateObservations({{Key::Obs::ms_EndSetValue, GetKey() + ms_Sep + this->toString()}});
        }
    }

    VarPtr VarObserverVar::Copy() const
    {
        return std::make_shared<VarObserverVar>(GetConstructorParameters(), m_Value);
    }

    std::wstring VarObserverVar::toString() const
    {
        return ma::toString(m_Var);
    }

    std::wstring VarObserverVar::GetKeyValueToString() const
    {
        return ma::toString(std::make_pair(m_Key, m_Var));
    }

    void VarObserverVar::fromString(const std::wstring &str)
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);

        SetFromItem(str);
    }

    bool VarObserverVar::IsValidString(const std::wstring &str) const
    {
        return ma::IsValidString<Var>(str);
    }

    bool VarObserverVar::IsValid() const
    {
        return !m_Var ||
               (Item::HasItem(m_Var->GetItemKey()) && Item::GetItem(m_Var->GetItemKey())->HasVar(m_Var->GetKey()));
    }

    void VarObserverVar::Reset()
    {
        MA_ASSERT(!m_ReadOnly, L"La variable est en lecture seule, elle ne peut être modifiée.", std::logic_error);
    }

    const TypeInfo &VarObserverVar::GetVarObserverVarTypeInfo()
    {
        static const TypeInfo info = {
            GetVarObserverVarClassName(), L"Cette variable permet de référencer une variable observatrice.", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(VarObserverVar, ObservableVar)

    // LongLongVar //———————————————————————————————————————————————————————————————————————————————————————————————————
    const TypeInfo &LongLongVar::GetLongLongVarTypeInfo()
    {
        static const TypeInfo info = {GetLongLongVarClassName(), L"Variable d'item de type «long long».", {}};

        return info;
    }
    M_CPP_CLASSHIERARCHY(LongLongVar, Var)

    /*
    // EnumVar //———————————————————————————————————————————————————————————————————————————————————————————————————————
    const TypeInfo& EnumVar::GetEnumVarTypeInfo()
    {
        static const TypeInfo info =
        {
            GetEnumVarClassName(),
            L"Cette variable d'item représente une valeur sélectionnée "s +
            L"d'une énumération."s,
            {}
        };

        return info;
    }
    M_CPP_CLASSHIERARCHY(EnumVar, Var)
    */

    // ItemHashSetVar //————————————————————————————————————————————————————————————————————————————————————————————————
    const TypeInfo &ItemHashSetVar::GetItemHashSetVarTypeInfo()
    {
        static const TypeInfo info = {GetItemHashSetVarClassName(),
                                      L"Cette variable d'item représente un conteneur d'items. L'unicité de chacun "
                                      L"des items est assurée. Concrètement c'est un std::unordered_set<ma::ItemPtr>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(ItemHashSetVar, HashSetVar)

    // ItemVectorVar //—————————————————————————————————————————————————————————————————————————————————————————————————
    const TypeInfo &ItemVectorVar::GetItemVectorVarTypeInfo()
    {
        static const TypeInfo info = {GetItemVectorVarClassName(),
                                      L"Cette variable représente un conteneur d'éléments ordonné et non associatif. "
                                      L"Concrètement c'est un std::vector<ma::ItemPtr>.",
                                      {}};
        return info;
    }
    M_CPP_CLASSHIERARCHY(ItemVectorVar, VectorVar)

    // ItemVar //———————————————————————————————————————————————————————————————————————————————————————————————————————
    M_CPP_ITEM_TYPE_VAR(Item)
} // namespace ma
