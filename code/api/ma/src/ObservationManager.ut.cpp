/// \file ObservationManager.ut.cpp
/// \author Pontier Pierre
/// \date 2020-01-08
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Test unitaires des classes Observable, Observer, Observation, ObservationManager.
/// Le système de tests unitaires utilisé est Criterion (https://github.com/Snaipe/Criterion).
/// Documentation: (https://buildmedia.readthedocs.org/media/pdf/criterion/latest/criterion.pdf)
///

#include <criterion/criterion.h>

#include "ma/Ma.h"

void observer_setup()
{
    ma::g_ConsoleVerbose = 1u;
    ma::Item::CreateRoot<ma::Item>();
    ma::Item::CreateItem<ma::ClassInfoManager>();
}

void observer_teardown()
{
    cr_assert_none_throw(ma::Item::EraseRoot());
    cr_assert_eq(ma::Item::GetCount(), 0);
}

Test(Observer, default_obs_manager, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();

    cr_assert_eq(obs_manager->GetKey(), ma::Item::Key::GetObservationManager());
}

////////////////////////////////////////////////////////////////////////////////

/// Observe un item
class ObserverItem_Test : public ma::Observer
{
    public:
        /// Clef de l'observateur dans observer_item_map
        std::wstring m_Key;

        /// Le nombre d'items directement obtenu via GetItems().size()
        size_t m_CountItems;

        /// Le nombre d'item calculé via les opérations d'ajout et de
        /// suppression.
        size_t m_EstimateCountItems;

        /// Le nombre de variables directement obtenu via GetVars().size()
        size_t m_CountVars;

        /// Le nombre de variables calculé via les opérations d'ajout et de suppressions.
        size_t m_EstimateCountVars;

        /// L'item qui sera observé
        ma::ItemPtr m_Item;

        ObserverItem_Test(const std::wstring& key):
        ma::Observer(),
        m_Key(key),
        m_CountItems(0),
        m_EstimateCountItems(0),
        m_CountVars(0),
        m_EstimateCountVars(0)
        {}

        ~ObserverItem_Test()
        {}

        void BeginObservation(const ma::ObservablePtr& observable) override
        {
            //M_MSG("BeginObservation: Begin")
            //M_MSG("Observable clef: " + observable->GetKey())
            m_Item = ma::Item::GetItem(observable->GetKey());

            m_CountItems = m_Item->GetItems(ma::Item::SearchMod::LOCAL).size();
            m_EstimateCountItems = m_CountItems;
            m_CountVars = m_Item->GetVars().size();
            m_EstimateCountVars = m_CountVars;
            //M_MSG("BeginObservation: End")
        }

        void UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data) override
        {
            //M_MSG("UpdateObservation Begin")

            MA_ASSERT(m_Item,
                      L"m_Item n'a pas été initialiser.");

            MA_ASSERT(observable->GetKey() == m_Item->GetKey(),
                      L"Cet observeur observe un item à la fois.");

//            for(const auto& it: data)
//            {
//                M_MSG(it.first)
//            }

            //M_MSG("Observable clef: " + observable->GetKey())
            //M_MSG("Observable clef: " + m_Item->GetKey())

            m_EstimateCountItems += data.GetCount(ma::Item::Key::Obs::ms_AddItemBegin);
            m_EstimateCountItems -= data.GetCount(ma::Item::Key::Obs::ms_RemoveItemBegin);

            m_EstimateCountVars += data.GetCount(ma::Item::Key::Obs::ms_AddVar);
            m_EstimateCountVars -= data.GetCount(ma::Item::Key::Obs::ms_RemoveVarBegin);

            m_CountItems = m_Item->GetItems(ma::Item::SearchMod::LOCAL).size();
            m_CountVars = m_Item->GetVars().size();
            //M_MSG("UpdateObservation: End")
        }

        void EndObservation(const ma::ObservablePtr& observable) override
        {
            //M_MSG("EndObservation: Begin")

            MA_ASSERT(observable->GetKey() == m_Item->GetKey(),
                      L"Cet observateur observe un item à la fois.");

            //M_MSG("Observable clef: " + observable->GetKey())
            m_CountItems = 0;
            m_EstimateCountItems = 0;
            m_CountVars = 0;
            m_EstimateCountVars = 0;
            m_Item = nullptr;
            //M_MSG("EndObservation: End")
        }

        const ma::TypeInfo& GetTypeInfo() const override
        {
            static const ma::TypeInfo result
            {
                L"ObserverItem_Test",
                L"Pour tester la mécanique d'observations des items",
                {}
            };

            return result;
        }
};
typedef ObserverItem_Test* ObserverItem_TestPtr;

typedef std::unordered_map<std::wstring, ObserverItem_TestPtr> ObserverItem_TestMap;

static ObserverItem_TestMap observer_item_map = {};

class ObserverItem_TestVar : public ma::ObserverVar
{
    protected:
        void SetFromItem(const std::wstring& str) override
        {
            auto it_find = observer_item_map.find(str);

            MA_ASSERT(it_find != observer_item_map.end(),
                      L"La clef ne représente aucun observateur.",
                      std::invalid_argument);

            m_Value = it_find->second;
        }

        ma::VarPtr Copy() const override
        {
            return std::shared_ptr<ObserverItem_TestVar>(new ObserverItem_TestVar(GetConstructorParameters(), m_Value));
        }

    public:
        using ma::ObserverVar::ObserverVar;

        /// Renvoie la clef de l'observateur dans observer_item_map
        std::wstring toString() const override
        {
            return dynamic_cast<ObserverItem_Test*>(m_Value)->m_Key;
        }

        std::wstring GetKeyValueToString() const override
        {
            return ma::toString(std::make_pair(m_Key, toString()));
        }

        /// A partir de la clef de l'observateur il retrouve la référence à
        /// celui-ci.
        void fromString(const std::wstring& str) override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être modifiée.",
                      std::logic_error);

            SetFromItem(str);
        }

        /// Permet de tester si la chaîne de caractère est syntaxiquement
        /// correct.
        /// \remarks à utiliser avant d'appeler fromString.
        bool IsValidString(const std::wstring& str)const override
        {
            return observer_item_map.count(str);
        }

        virtual bool IsValid() const override
        {
            return observer_item_map.count(dynamic_cast<ObserverItem_Test*>(m_Value)->m_Key);
        }

        void Reset() override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être modifiée.",
                      std::logic_error);
        }
};
typedef std::shared_ptr<ObserverItem_TestVar> ObserverItem_TestVarPtr;

Test(Observer, default_observer, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();

    const std::wstring key_observer(L"obsvervateur_0");
    ObserverItem_TestPtr observer_ptr = new ObserverItem_Test(key_observer);

    auto item = ma::Item::CreateItem<ma::Item>();
    auto key = item->GetKey();
    auto child = ma::Item::CreateItem<ma::Item>({key});

    obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, item);

    cr_assert(obs_manager->HasObservation(observer_ptr, ma::Item::GetItem(key)));
    cr_assert_not(obs_manager->HasObservation(observer_ptr, child));
    cr_assert(obs_manager->HasObserver(observer_ptr));
    cr_assert(obs_manager->HasObservable(ma::Item::GetItem(key)));

    cr_assert_eq(observer_ptr->m_CountItems, 1);
    cr_assert_eq(observer_ptr->m_EstimateCountItems, 1);
    cr_assert_eq(observer_ptr->m_CountVars, item->GetVars().size());
    cr_assert_eq(observer_ptr->m_EstimateCountVars, item->GetVars().size());

    delete observer_ptr;
}

Test(Observer, get_observations, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetItem(ma::Item::Key::GetRoot());

    const std::wstring key_observer(L"obsvervateur_0");
    ObserverItem_TestPtr observer = new ObserverItem_Test(key_observer);
    obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer, root);

    auto observations = obs_manager->GetObservations(observer);
    auto observer_key = observer->GetObserverKey();

    for(auto it : observations)
    {
        auto observation = it.second;
        auto observable = observation->m_ObservableVar->Get();

        cr_assert_eq(observer, observation->m_ObserverVar->Get());
        cr_assert_eq(root, observable);
        cr_assert_eq(observer_key, observation->m_ObserverVar->Get()->GetObserverKey());
    }
}

Test(Observer, remove_observer, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetItem(ma::Item::Key::GetRoot());

    const std::wstring key_observer(L"obsvervateur_0");
    ObserverItem_TestPtr observer_ptr = new ObserverItem_Test(key_observer);

    cr_assert_not(obs_manager->HasObservable(root));

    obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, root);

    cr_assert(obs_manager->HasObservation(observer_ptr, root));
    cr_assert(obs_manager->HasObserver(observer_ptr));
    cr_assert(obs_manager->HasObservable(root));

    obs_manager->RemoveObserver(observer_ptr);

    cr_assert_not(obs_manager->HasObservation(observer_ptr, root));
    cr_assert_not(obs_manager->HasObserver(observer_ptr));
    cr_assert_not(obs_manager->HasObservable(root));

    delete observer_ptr;
}

Test(Observer, remove_observable, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetItem(ma::Item::Key::GetRoot());

    const std::wstring key_observer(L"obsvervateur_0");
    ObserverItem_TestPtr observer_ptr = new ObserverItem_Test(key_observer);

    cr_assert_not(obs_manager->HasObservable(root));

    obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, root);

    cr_assert(obs_manager->HasObservation(observer_ptr, root));
    cr_assert(obs_manager->HasObserver(observer_ptr));
    cr_assert(obs_manager->HasObservable(root));

    obs_manager->RemoveObservable(root);

    cr_assert_not(obs_manager->HasObservation(observer_ptr, root));
    cr_assert_not(obs_manager->HasObserver(observer_ptr));
    cr_assert_not(obs_manager->HasObservable(root));

    delete observer_ptr;
}

Test(Observer, remove_observation, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetItem(ma::Item::Key::GetRoot());

    const std::wstring key_observer(L"obsvervateur_0");
    ObserverItem_TestPtr observer_ptr = new ObserverItem_Test(key_observer);

    cr_assert_not(obs_manager->HasObservable(root));

    obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, root);

    cr_assert(obs_manager->HasObservation(observer_ptr, root));
    cr_assert(obs_manager->HasObserver(observer_ptr));
    cr_assert(obs_manager->HasObservable(root));

    obs_manager->RemoveObservation(observer_ptr, root);

    cr_assert_not(obs_manager->HasObservation(observer_ptr, root));
    cr_assert_not(obs_manager->HasObserver(observer_ptr));
    cr_assert_not(obs_manager->HasObservable(root));

    delete observer_ptr;
}

Test(Observer, observateur_item, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();

    auto item = ma::Item::CreateItem<ma::Item>();
    const auto init_count_vars = item->GetVars().size();
    auto item_key = item->GetKey();

    const std::wstring key_observer(L"obsvervateur_0");
    ObserverItem_TestPtr observer_ptr = new ObserverItem_Test(key_observer);

    observer_item_map[key_observer] = observer_ptr;

    ObserverItem_TestVarPtr observervar_ptr = item->AddVar<ObserverItem_TestVar>(L"m_Observer", observer_ptr);

    auto observation = obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, item);

    cr_assert(observation);
    {
        cr_assert_eq(observer_ptr->m_CountItems, 0);
        cr_assert_eq(observer_ptr->m_EstimateCountItems, 0);
        cr_assert_eq(observer_ptr->m_CountVars, init_count_vars + 1u);
        cr_assert_eq(observer_ptr->m_EstimateCountVars, init_count_vars + 1u);

        auto child = ma::Item::CreateItem<ma::Item>({item_key});

        cr_assert_eq(observer_ptr->m_CountItems, 1);
        cr_assert_eq(observer_ptr->m_EstimateCountItems, 1);
        cr_assert_eq(observer_ptr->m_CountVars, init_count_vars + 1u);
        cr_assert_eq(observer_ptr->m_EstimateCountVars, init_count_vars+ 1u);

        ma::Item::EraseItem(child->GetKey());

        cr_assert_eq(observer_ptr->m_CountItems, 0);
        cr_assert_eq(observer_ptr->m_EstimateCountItems, 0);
        cr_assert_eq(observer_ptr->m_CountVars, init_count_vars + 1u);
        cr_assert_eq(observer_ptr->m_EstimateCountVars, init_count_vars+ 1u);

        auto var = item->AddVar<ma::DefineVar>(L"m_DefineVar");

        cr_assert_eq(observer_ptr->m_CountItems, 0);
        cr_assert_eq(observer_ptr->m_EstimateCountItems, 0);
        cr_assert_eq(observer_ptr->m_CountVars, init_count_vars + 2u);
        cr_assert_eq(observer_ptr->m_EstimateCountVars, init_count_vars + 2u);

        item->EraseVar(var->m_Key);

        cr_assert_eq(observer_ptr->m_CountItems, 0);
        cr_assert_eq(observer_ptr->m_EstimateCountItems, 0);
        cr_assert_eq(observer_ptr->m_CountVars, init_count_vars + 1u);
        cr_assert_eq(observer_ptr->m_EstimateCountVars, init_count_vars + 1u);

        ma::Item::EraseItem(item_key);
    }
    cr_assert_not(obs_manager->HasItem(observation->GetKey(), ma::Item::SearchMod::LOCAL));
    observation = nullptr;

    // L'item n'est plus valide, car il n'est plus dans « ma::Item::ms_Map ».
    // Par conséquent l'item n'est plus observable.
    // Une exception sera donc levée.
    ma::g_ConsoleVerbose = 0;
    cr_assert_throw((obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, item)), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    item = ma::Item::CreateItem<ma::Item>();
    item_key = item->GetKey();
    observation = obs_manager->AddObservation<ObserverItem_TestVar, ma::ItemVar>(observer_ptr, item);

    cr_assert(obs_manager->HasItem(observation->GetKey(), ma::Item::SearchMod::LOCAL));
    delete observer_ptr;
    cr_assert_not(obs_manager->HasItem(observation->GetKey(), ma::Item::SearchMod::LOCAL));
}

////////////////////////////////////////////////////////////////////////////////

/// Observe une variable
class ObserverVar_Test : public ma::Observer
{
    public:
        /// Clef de l'observateur dans observer_item_map
        std::wstring m_Key;

        /// La Valeur courrante de la variable.
        std::wstring m_Value;

        /// La variable qui sera observée
        ma::VarPtr m_Var;

        ObserverVar_Test(const std::wstring& key):
        ma::Observer(),
        m_Key(key),
        m_Value(),
        m_Var()
        {}

        ~ObserverVar_Test()
        {}

        void BeginObservation(const ma::ObservablePtr& observable) override
        {
            //M_MSG("BeginObservation: Begin")
            //M_MSG("Observable clef: " + observable->GetKey())
            m_Var = std::dynamic_pointer_cast<ma::Var>(observable);
            m_Value = m_Var->toString();
            //M_MSG("BeginObservation: End")
        }

        void UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data) override
        {
            //M_MSG("UpdateObservation Begin")

            MA_ASSERT(m_Var,
                      L"m_Var n'a pas été initialiser.");

            MA_ASSERT(m_Var->GetKey() == m_Var->GetKey(),
                      L"Cet observateur observe une variable à la fois.");

            m_Value = m_Var->toString();

            //M_MSG("UpdateObservation: End")
        }

        void EndObservation(const ma::ObservablePtr& observable) override
        {
            //M_MSG("EndObservation: Begin")

            MA_ASSERT(observable->GetKey() == m_Var->GetKey(),
                      L"Cet observateur observe une variable à la fois.");

            m_Var = nullptr;
            m_Value = L"";
            //M_MSG("EndObservation: End")
        }

        const ma::TypeInfo& GetTypeInfo() const override
        {
            static const ma::TypeInfo result
            {
                L"ObserverVar_Test",
                L"Pour tester la mécanique d'observations des variables.",
                {}
            };

            return result;
        }
};
typedef ObserverVar_Test* ObserverVar_TestPtr;

typedef std::unordered_map<std::wstring, ObserverVar_TestPtr> ObserverVar_TestMap;

static ObserverVar_TestMap observer_var_map = {};

class ObserverVar_TestVar : public ma::ObserverVar
{
    protected:
        void SetFromItem(const std::wstring& str) override
        {
            auto it_find = observer_var_map.find(str);

            MA_ASSERT(it_find != observer_var_map.end(),
                      L"La clef ne représente aucun observateur.",
                      std::invalid_argument);

            m_Value = it_find->second;
        }

        ma::VarPtr Copy() const override
        {
            return std::shared_ptr<ObserverVar_TestVar>(new ObserverVar_TestVar(GetConstructorParameters(), m_Value));
        }


    public:
        using ma::ObserverVar::ObserverVar;

        /// Renvoie la clef de l'observateur dans observer_item_map
        std::wstring toString() const override
        {
            std::wstring result = L"";

            if(m_Value)
            {
                auto observer = dynamic_cast<ObserverVar_Test*>(m_Value);
                if(observer)
                    result = observer->m_Key;
            }

            return result;
        }

        std::wstring GetKeyValueToString() const override
        {
            return ma::toString(std::make_pair(m_Key, toString()));
        }

        /// À partir de la clef de l'observateur, il retrouve la référence à celui-ci.
        void fromString(const std::wstring& str) override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être modifiée.",
                      std::logic_error);

            SetFromItem(str);
        }

        /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
        /// \remarks À utiliser avant d'appeler fromString.
        bool IsValidString(const std::wstring& str)const override
        {
            return observer_var_map.count(str);
        }

        virtual bool IsValid() const override
        {
            bool result = false;
            if(m_Value)
            {
                auto observer = dynamic_cast<ObserverVar_Test*>(m_Value);
                if(observer)
                    result = observer_var_map.count(observer->m_Key);
            }

            return result;
        }

        void Reset() override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être modifiée.",
                      std::logic_error);
        }
};
typedef std::shared_ptr<ObserverVar_TestVar> ObserverVar_TestVarPtr;


Test(Observer, observateur_var, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();

    auto item = ma::Item::CreateItem<ma::Item>();
    auto item_key = item->GetKey();

    auto string_var = item->AddVar<ma::StringVar>(L"m_StringVar", L"value_0");

    std::wstring key_observer(L"obsvervateur_0");
    ObserverVar_TestPtr observer_ptr = new ObserverVar_Test(key_observer);
    observer_var_map[key_observer] = observer_ptr;

    ObserverVar_TestVarPtr observervar_ptr = item->AddVar<ObserverVar_TestVar>(L"m_Observer", observer_ptr);

    auto observation = obs_manager->AddObservation<ObserverVar_TestVar, ma::VarVar>(observer_ptr, string_var);

    cr_assert(observation);
    {
        cr_assert_eq(observer_ptr->m_Var, string_var);
        cr_assert_eq(observer_ptr->m_Value, L"value_0");

        string_var->Set(L"value_1");
        cr_assert_eq(observer_ptr->m_Value, L"value_1");

        item->EraseVar(string_var->m_Key);

        cr_assert_eq(observer_ptr->m_Var, nullptr);
        cr_assert_eq(observer_ptr->m_Value, L"");
    }
    cr_assert_not(obs_manager->HasItem(observation->GetKey(), ma::Item::SearchMod::LOCAL));

    // La variable n'est plus valide, car elle n'est plus associé à l'item.
    // Par conséquent, la variable n'est plus observable.
    // Une exception sera donc levée.
    ma::g_ConsoleVerbose = 0;
    cr_assert_throw((obs_manager->AddObservation<ObserverItem_TestVar, ma::VarVar>(observer_ptr, string_var)), std::invalid_argument);
    ma::g_ConsoleVerbose = 1u;

    string_var = item->AddVar<ma::StringVar>(L"m_StringVar", L"value_0");
    observation = obs_manager->AddObservation<ObserverItem_TestVar, ma::VarVar>(observer_ptr, string_var);

    cr_assert(obs_manager->HasItem(observation->GetKey(), ma::Item::SearchMod::LOCAL));
    delete observer_ptr;
    cr_assert_not(obs_manager->HasItem(observation->GetKey(), ma::Item::SearchMod::LOCAL));

    observer_var_map.clear();
}

////////////////////////////////////////////////////////////////////////////////

class ObserverItemVar_TestVar;

/// Observe une variable de type ItemVar
class ObserverItemVar_Test : public ma::Observer
{
    public:
        /// Clef de l'observateur dans observer_item_var_map
        std::wstring m_Key;

        /// La variable qui sera observée
        ma::ItemVarPtr m_CurrentItemVar;
        ma::ObservationManagerPtr m_ObsMgr;

        ObserverItemVar_Test(ma::ItemVarPtr current, const std::wstring& key):
        ma::Observer(),
        m_Key(key),
        m_CurrentItemVar(current),
        m_ObsMgr(ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager()))
        {
            m_ObsMgr->AddObservation<ObserverItemVar_TestVar, ma::VarVar>(this, m_CurrentItemVar);
        }

        ~ObserverItemVar_Test()
        {}

        void AddObservation(ma::ItemPtr item)
        {
            if(item && item->IsObservable() && ma::Item::HasItem(item->GetKey()))
            {
                m_ObsMgr->AddObservation<ObserverItemVar_TestVar, ma::ItemVar>(this, item);
            }
        }

        void BeginObservation(const ma::ObservablePtr& observable) override
        {
            if(observable == m_CurrentItemVar)
                AddObservation(m_CurrentItemVar->GetItem());
        }

        void UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data) override
        {
            auto item = m_CurrentItemVar->GetItem();

            // La variable contenant l'item courant vient d'être mise à jour.
            if(observable == m_CurrentItemVar && data.HasActions(ma::Var::Key::Obs::ms_BeginSetValue))
            {
                if(item && m_ObsMgr->HasObservation(this, item))
                    m_ObsMgr->RemoveObservation(this, item);
            }
            else if(observable == m_CurrentItemVar &&
                    data.HasActions(ma::Var::Key::Obs::ms_EndSetValue))
            {
                AddObservation(item);
            }
        }

        void EndObservation(const ma::ObservablePtr& observable) override
        {
            if(ma::ClassInfoManager::IsInherit(observable, ma::Item::GetItemClassName()))
            {
                auto item = std::dynamic_pointer_cast<ma::Item>(observable);

                MA_ASSERT(item,
                          L"L'item est nulle.",
                          std::runtime_error);

                if(m_CurrentItemVar->GetItem() == item)
                {
                    auto parent_key = item->GetParentKey();

                    if(ma::Item::HasItem(parent_key))
                        m_CurrentItemVar->SetItem(ma::Item::GetItem(parent_key));

                }
            }
        }

        const ma::TypeInfo& GetTypeInfo() const override
        {
            static const ma::TypeInfo result
            {
                L"ObserverItemVar_Test",
                L"Pour tester la mécanique d'observations des variables.",
                {}
            };

            return result;
        }
};
typedef ObserverItemVar_Test* ObserverItemVar_TestPtr;

typedef std::unordered_map<std::wstring, ObserverItemVar_TestPtr> ObserverItemVar_TestMap;

static ObserverItemVar_TestMap observer_item_var_map = {};

class ObserverItemVar_TestVar : public ma::ObserverVar
{
    protected:
        void SetFromItem(const std::wstring& str) override
        {
            auto it_find = observer_item_var_map.find(str);

            MA_ASSERT(it_find != observer_item_var_map.end(),
                      L"La clef ne représente aucun observateur.",
                      std::invalid_argument);

            m_Value = it_find->second;
        }

        ma::VarPtr Copy() const override
        {
            return std::shared_ptr<ObserverItemVar_TestVar>(new ObserverItemVar_TestVar(GetConstructorParameters(), m_Value));
        }


    public:
        using ma::ObserverVar::ObserverVar;

        /// Renvoie la clef de l'observateur dans observer_item_map.
        std::wstring toString() const override
        {
            return dynamic_cast<ObserverItemVar_Test*>(m_Value)->m_Key;
        }

        std::wstring GetKeyValueToString() const override
        {
            return ma::toString(std::make_pair(m_Key, toString()));
        }


        /// À partir de la clef de l'observateur, il retrouve la référence à celui-ci.
        void fromString(const std::wstring& str) override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être modifiée.",
                      std::logic_error);

            SetFromItem(str);
        }

        /// Permet de tester si la chaîne de caractère est syntaxiquement correct.
        /// \remarks à utiliser avant d'appeler fromString.
        bool IsValidString(const std::wstring& str)const override
        {
            return observer_item_var_map.count(str);
        }

        virtual bool IsValid() const override
        {
            return observer_item_var_map.count(dynamic_cast<ObserverItemVar_Test*>(m_Value)->m_Key);
        }

        void Reset() override
        {
            MA_ASSERT(!m_ReadOnly,
                      L"La variable est en lecture seule, elle ne peut être " \
                      L"modifiée.",
                      std::logic_error);
        }
};
typedef std::shared_ptr<ObserverItemVar_TestVar> ObserverItemVar_TestVarPtr;

Test(Observer, guarden_loop, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();

    ma::ItemVarPtr item_var = root->AddVar<ma::ItemVar>(L"m_CurrentItemVar");

    auto grand_parent = ma::Item::CreateItem<ma::Item>({root->GetKey()});
    auto parent = ma::Item::CreateItem<ma::Item>({grand_parent->GetKey()});
    auto child_A = ma::Item::CreateItem<ma::Item>({parent->GetKey()});
    auto child_B = ma::Item::CreateItem<ma::Item>({parent->GetKey()});
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert(ma::Item::HasItem(child_B->GetKey()));

    item_var->SetItem(child_A);

    std::wstring key_observer_A(L"observer_A");
    ObserverItemVar_TestPtr observer_A = new ObserverItemVar_Test(item_var, key_observer_A);
    observer_item_var_map[key_observer_A] = observer_A;

    cr_assert_eq(item_var->GetItem(), child_A);
    cr_assert_eq(observer_A->m_CurrentItemVar->GetItem(), child_A);
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert(ma::Item::HasItem(child_B->GetKey()));

    std::wstring key_observer_B(L"observer_B");
    ObserverItemVar_TestPtr observer_B = new ObserverItemVar_Test(item_var, key_observer_B);
    observer_item_var_map[key_observer_B] = observer_B;

    cr_assert_eq(item_var->GetItem(), child_A);
    cr_assert_eq(observer_B->m_CurrentItemVar->GetItem(), child_A);
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert(ma::Item::HasItem(child_B->GetKey()));

    item_var->SetItem(child_B);
    cr_assert_eq(item_var->GetItem(), child_B);
    cr_assert_eq(observer_A->m_CurrentItemVar->GetItem(), child_B);
    cr_assert_eq(observer_B->m_CurrentItemVar->GetItem(), child_B);
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert(ma::Item::HasItem(child_B->GetKey()));

    ma::Item::EraseItem(child_B->GetKey());
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert_not(ma::Item::HasItem(child_B->GetKey()));

    cr_assert_eq(item_var->GetItem(), parent);
    cr_assert_eq(observer_A->m_CurrentItemVar->GetItem(), parent);
    cr_assert_eq(observer_B->m_CurrentItemVar->GetItem(), parent);

    delete observer_A;
    delete observer_B;

    observer_item_var_map.clear();
}

Test(Observer, guarden_loop2, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();

    ma::ItemVarPtr item_var = root->AddVar<ma::ItemVar>(L"m_CurrentItemVar");

    auto grand_parent = ma::Item::CreateItem<ma::Item>({root->GetKey()});
    auto parent = ma::Item::CreateItem<ma::Item>({grand_parent->GetKey()});
    auto child_A = ma::Item::CreateItem<ma::Item>({parent->GetKey()});
    auto child_B = ma::Item::CreateItem<ma::Item>({parent->GetKey()});

    item_var->SetItem(child_A);

    std::wstring key_observer_A(L"observer_A");
    ObserverItemVar_TestPtr observer_A = new ObserverItemVar_Test(item_var, key_observer_A);
    observer_item_var_map[key_observer_A] = observer_A;

    std::wstring key_observer_B(L"observer_B");
    ObserverItemVar_TestPtr observer_B = new ObserverItemVar_Test(item_var, key_observer_B);
    observer_item_var_map[key_observer_B] = observer_B;

    item_var->SetItem(child_B);

    ma::Item::EraseItem(child_B->GetKey());
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert_not(ma::Item::HasItem(child_B->GetKey()));

    cr_assert_eq(item_var->GetItem(), parent);
    cr_assert_eq(observer_A->m_CurrentItemVar->GetItem(), parent);
    cr_assert_eq(observer_B->m_CurrentItemVar->GetItem(), parent);

    parent->AddChild(child_B);
    cr_assert(parent->HasItem(child_B->GetKey(), ma::Item::SearchMod::LOCAL));

    item_var->SetItem(child_B);
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert(ma::Item::HasItem(child_B->GetKey()));

    cr_assert_eq(item_var->GetItem(), child_B);
    cr_assert_eq(observer_A->m_CurrentItemVar->GetItem(), child_B);
    cr_assert_eq(observer_B->m_CurrentItemVar->GetItem(), child_B);

    ma::Item::EraseItem(child_B->GetKey());
    cr_assert(ma::Item::HasItem(child_A->GetKey()));
    cr_assert_not(ma::Item::HasItem(child_B->GetKey()));

    cr_assert_eq(item_var->GetItem(), parent);
    cr_assert_eq(observer_A->m_CurrentItemVar->GetItem(), parent);
    cr_assert_eq(observer_B->m_CurrentItemVar->GetItem(), parent);

    delete observer_A;
    delete observer_B;

    observer_item_var_map.clear();
}


/// Observe une variable de type BoolVar
class ObserverBoolVar_Test : public ma::Observer
{
    public:
        /// La variable qui sera observée
        ma::BoolVarPtr m_Var;

        /// Copie de la valeur de m_Var.
        bool m_Value;

        ObserverBoolVar_Test(ma::BoolVarPtr var):
        ma::Observer(),
        m_Var(var)
        {
            auto obs_mgr = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
            obs_mgr->AddObservation<ma::VarObserverVar, ma::VarVar>(this, m_Var);
        }

        ~ObserverBoolVar_Test()
        {}

        void BeginObservation(const ma::ObservablePtr& observable) override
        {
            if(observable == m_Var)
                m_Value = m_Var->Get();
        }

        void UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data) override
        {
            if(observable == m_Var && data.HasActions(ma::Var::Key::Obs::ms_EndSetValue))
               m_Value = m_Var->Get();
        }

        void EndObservation(const ma::ObservablePtr& observable) override
        {
            m_Var = nullptr;
        }

        const ma::TypeInfo& GetTypeInfo() const override
        {
            static const ma::TypeInfo result
            {
                L"ObserverBoolVar_Test",
                L"Pour tester la mécanique d'observations des variables.",
                {}
            };

            return result;
        }
};
typedef ObserverBoolVar_Test* ObserverBoolVar_TestPtr;

Test(Observer, batch_1, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();

    ma::BoolVarPtr var = root->AddVar<ma::BoolVar>({L"m_BoolVar", false});

    ObserverBoolVar_Test observer(var);

    cr_assert_eq(observer.m_Value, false);
    cr_assert_eq(observer.m_Var, var);

    root->BeginBatch();
    var->Set(true);
    root->EndBatch();

    cr_assert_eq(observer.m_Value, true);
    cr_assert_eq(observer.m_Var, var);
}

Test(Observer, no_batch_2, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();

    ma::BoolVarPtr var = root->AddVar<ma::BoolVar>({L"m_BoolVar", false});

    ObserverBoolVar_Test observer(var);

    cr_assert_eq(observer.m_Value, false);
    cr_assert_eq(observer.m_Var, var);

    {
        var->Set(true);
        root->EraseVar(var->GetKey());

        // La fin de l'observation est appelée.
        cr_assert_eq(observer.m_Var, nullptr);
    }

    cr_assert_eq(observer.m_Value, true);
}

Test(Observer, batch_2, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();

    ma::BoolVarPtr var = root->AddVar<ma::BoolVar>({L"m_BoolVar", false});

    ObserverBoolVar_Test observer(var);

    cr_assert_eq(observer.m_Value, false);
    cr_assert_eq(observer.m_Var, var);

    root->BeginBatch();
    {
        var->Set(true);
        root->EraseVar(var->GetKey());

        // La fin de l'observation est appelée malgré le faite d'être dans la
        // portée de l'arrêt des observations.
        cr_assert_eq(observer.m_Var, nullptr);
    }
    root->EndBatch();

    // A la fin du EndBatch la variable a été retirée. L'observateur ne sera
    // donc pas mis à jour.
    cr_assert_eq(observer.m_Value, false);
}

/// Observe un item
class ObserverItem_Test2 : public ma::Observer
{
    public:
        /// L'item qui sera observé
        ma::ItemPtr m_Item;

        /// Le nombre d'enfants
        size_t m_CountChildren;

        /// Compte le nombre d'appels à AddChild
        size_t m_CountAddChild;

        ObserverItem_Test2(const ma::ItemPtr& item):
        ma::Observer(),
        m_Item(item),
        m_CountChildren(0),
        m_CountAddChild(0)
        {
            auto obs_mgr = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
            obs_mgr->AddObservation<ma::VarObserverVar, ma::VarVar>(this, m_Item);
        }

        ~ObserverItem_Test2()
        {}

        void BeginObservation(const ma::ObservablePtr& observable) override
        {
            if(m_Item == observable)
            {
                m_CountChildren = m_Item->GetCount(ma::Item::SearchMod::LOCAL);
                m_CountAddChild = m_CountChildren;
            }
        }

        void UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data) override
        {
            if(m_Item == observable)
            {
                m_CountChildren = m_Item->GetCount(ma::Item::SearchMod::LOCAL);
                m_CountAddChild += data.GetCount(ma::Item::Key::Obs::ms_AddItemBegin);
            }
        }

        void EndObservation(const ma::ObservablePtr& observable) override
        {}

        const ma::TypeInfo& GetTypeInfo() const override
        {
            static const ma::TypeInfo result
            {
                L"ObserverItem_Test2",
                L"Pour tester la mécanique d'observations des items",
                {}
            };

            return result;
        }
};

Test(Observer, batch_3, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();
    auto parent = ma::Item::CreateItem<ma::Item>();

    ObserverItem_Test2 observer(parent);

    cr_assert_eq(observer.m_Item, parent);
    cr_assert_eq(observer.m_CountChildren, 0);
    cr_assert_eq(observer.m_CountChildren, parent->GetCount(ma::Item::SearchMod::LOCAL));
    cr_assert_eq(observer.m_CountAddChild, 0);

    root->BeginBatch();
    {
        auto child_1 = ma::Item::CreateItem<ma::Item>(parent->GetKey());
        cr_assert_eq(observer.m_CountChildren, 0);
        cr_assert_eq(parent->GetCount(ma::Item::SearchMod::LOCAL), 1u);
        cr_assert_eq(observer.m_CountAddChild, 0);
    }
    root->EndBatch();

    cr_assert_eq(observer.m_CountChildren, 1u);
    cr_assert_eq(parent->GetCount(ma::Item::SearchMod::LOCAL), 1u);
    cr_assert_eq(observer.m_CountAddChild, 1u);
}
Test(Observer, no_batch_4, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();
    auto parent = ma::Item::CreateItem<ma::Item>();

    ObserverItem_Test2 observer(parent);

    cr_assert_eq(observer.m_Item, parent);
    cr_assert_eq(observer.m_CountChildren, 0);
    cr_assert_eq(observer.m_CountChildren, parent->GetCount(ma::Item::SearchMod::LOCAL));
    cr_assert_eq(observer.m_CountAddChild, 0);

    {
        auto child = ma::Item::CreateItem<ma::Item>(parent->GetKey());
        cr_assert_eq(observer.m_CountChildren, 1u);
        cr_assert_eq(parent->GetCount(ma::Item::SearchMod::LOCAL), 1u);
        cr_assert_eq(observer.m_CountAddChild, 1u);

        ma::Item::EraseItem(child->GetKey());
        cr_assert_eq(observer.m_CountChildren, 0);
        cr_assert_eq(parent->GetCount(ma::Item::SearchMod::LOCAL), 0);
        cr_assert_eq(observer.m_CountAddChild, 1u);
    }
}

Test(Observer, batch_4, .init = observer_setup, .fini = observer_teardown)
{
    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
    auto root = ma::Item::GetRoot();
    auto parent = ma::Item::CreateItem<ma::Item>();

    ObserverItem_Test2 observer(parent);

    cr_assert_eq(observer.m_Item, parent);
    cr_assert_eq(observer.m_CountChildren, 0);
    cr_assert_eq(observer.m_CountChildren, parent->GetCount(ma::Item::SearchMod::LOCAL));
    cr_assert_eq(observer.m_CountAddChild, 0);

    root->BeginBatch();
    {
        auto child = ma::Item::CreateItem<ma::Item>(parent->GetKey());
        cr_assert_eq(observer.m_CountChildren, 0);
        cr_assert_eq(parent->GetCount(ma::Item::SearchMod::LOCAL), 1u);
        cr_assert_eq(observer.m_CountAddChild, 0);

        ma::Item::EraseItem(child->GetKey());
        cr_assert_eq(observer.m_CountChildren, 0);
        cr_assert_eq(parent->GetCount(ma::Item::SearchMod::LOCAL), 0);
        cr_assert_eq(observer.m_CountAddChild, 0);
    }
    root->EndBatch();

    cr_assert_eq(observer.m_CountChildren, 0);
    cr_assert_eq(parent->GetCount(ma::Item::SearchMod::LOCAL), 0);
    cr_assert_eq(observer.m_CountAddChild, 1u);
}

//Test(Observer, observation_end, .init = observer_setup, .fini = observer_teardown)
//{
//    auto obs_manager = ma::Item::CreateItem<ma::ObservationManager>();
//    auto root = ma::Item::GetRoot();
//    auto item = ma::Item::CreateItem<ma::Item>();
//
//    auto var = item->AddVar<ma::BoolVar>(L"m_Bool", false);
//
//    ObserverItem_Test2 observer(item);
//    auto obs_mgr = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::ms_ObservationManager);
//    auto observation = obs_manager->AddObservation<ma::VarObserverVar, ma::VarVar>(&observer, var);
//}

