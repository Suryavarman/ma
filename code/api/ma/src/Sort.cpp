/// \file Sort.cpp
/// \author Pontier Pierre
/// \date 2020-03-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "ma/Sort.h"
// #include "ma/Find.h"

namespace ma
{
    // CompareItems //——————————————————————————————————————————————————————————————————————————————————————————————————
    bool CompareItems::operator()(ma::ItemPtr &) const
    {
        return true;
    }

    CompareItems::~CompareItems() = default;

    // Sort //——————————————————————————————————————————————————————————————————————————————————————————————————————————
    //    ItemsOrder Sort::AutoTypeSortItems(const ma::Item::key_type &key_parent, comparisons_type type,
    //    ma::Var::key_type key_var, ma::Item::SearchMod search_mod)
    //    {
    //        auto item_parent = ma::Item::GetItem(key_parent);
    //        return ma::Sort::AutoTypeSortItems(item_parent, type, key_var, search_mod);
    //    }
    //
    //    ItemsOrder Sort::AutoTypeSortItems(const ma::ItemPtr &item_parent, comparisons_type type, ma::Var::key_type
    //    key_var, ma::Item::SearchMod search_mod)
    //    {
    //        typedef ma::MatchVar<ma::Var, ma::Item> Matcher;
    //        Matcher matcher({key_var});
    //        auto children = ma::Find<Matcher>::GetHashMap(item_parent->GetKey(), matcher, ma::Item::SearchMod::LOCAL);
    //
    //
    //
    ////        std::unique_ptr<CompareItems> compare;
    ////        for(auto& it = result.begin(); it != result.end() && !compare; ++it)
    ////        {
    ////
    ////        }
    //
    //        return {};
    //    }
} // namespace ma