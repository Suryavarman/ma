import ma

def test_batch_item_add_erase():
    root = ma.Item.fs_GetRoot()
    parent = ma.Item.fs_CreateItem()
    root.BeginBatch()
    child = ma.Item.fs_CreateItem(parent.GetKey())
    ma.Item.fs_EraseItem(child.GetKey())
    root.EndBatch()
    ma.Item.fs_EraseItem(parent.GetKey())
    
test_batch_item_add_erase()
