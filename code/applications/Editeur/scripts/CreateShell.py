import wx
import wx.aui
from wx.py import shell, version
import ma


class ShellPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.SUNKEN_BORDER)

        intro = 'Bienvenu sur PyCrust %s - La moins fiable (falbkiest?!) des consoles python.' % version.VERSION

        py_crust = shell.Shell(self, -1, introText=intro)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(py_crust, 1, wx.EXPAND | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)

        self.SetSizer(sizer)


auiNotebookCenter = ma.wx.get_center()
auiNotebookCenter.AddPage(ShellPanel(auiNotebookCenter), "Console Python")

