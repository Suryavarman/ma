import wx
import wx.aui
from wx.py import editor
import ma

# http://athena-mylog.blogspot.com/2013/10/wxluaide-add-python-lexer.html
# https://pythonhosted.org/wxPython/wx.stc.StyledTextCtrl.html
# https://wiki.wxpython.org/StyledTextCtrl


class EditorPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.SUNKEN_BORDER)

        pyAlaCarte = editor.EditorFrame(self, -1)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pyAlaCarte, 1, wx.EXPAND | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)

        self.SetSizer(sizer)


auiNotebookCenter = ma.wx.get_center()
auiNotebookCenter.AddPage(EditorPanel(auiNotebookCenter), "Editeur")
