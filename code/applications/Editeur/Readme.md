[![License](https://img.shields.io/badge/license-MIT-green)](https://framagit.org/Suryavarman/ma/-/blob/master/LICENSE)

![Lanceur de Ma](images/ma_gui_969x256.png)

# L'interface graphique 

![Gui image 1](images/gui_01.png)

# Feuille de route
## Premier cycle 8/9
- [x] ~~Initialiser la structe de donnéer de Ma.~~
- [x] ~~Créer un feuillet gauche pour l'arbre de la structure des données de l'API.~~_
- [x] ~~Créer le feuillet bas pour la journalisation et l'explorateur de ressources.~~
- [x] ~~Créer le feuillet central pour la console python et les vues 3D.~~
- [x] ~~Créer le feuillet droit pour l'affichage les variables de l'item sélectionné.~~
- [x] ~~Créer le menu principal.~~
- [x] ~~Préparer l'environnement python pour charger des scripts python~~
- [x] ~~A l'aide de [PyCrust](https://www.pythonstudio.us/wxpython/pycrust-sets-the-standard-for-a-python-shell.html) créer une console.~~
- [ ] Traduire les textes de l'application.