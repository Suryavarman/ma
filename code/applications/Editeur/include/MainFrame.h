/// \file MainFrame.h
/// \brief Defines Application Frame
/// \author Pierre Pontier
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <wx/aui/aui.h>

#include "ma/Ma.h"
#include "Garbage.h"
#include <memory>

namespace ma::Editor
{
    class MainFrame final: public wxFrame
    {
        public:
            MainFrame(wxFrame* frame,
                      const wxString& inTitle,
                      const wxPoint& inPos,
                      const wxSize& inSize);

            ~MainFrame() final;

            void Load();
            void Save();

//            bool CreateFileWatcherIfNecessary();

            /// Ajoute un menu qui se chargera d'afficher ou de cacher une fenêtre.
            wxMenu* AddMenu(const std::wstring& name, wxWindow* window);

        private:
            static const long ID_MenuAddResourcePath;

            void OnAbout(wxCommandEvent& inEvent);
            void OnAddDirRes(wxCommandEvent& inEvent);
            void OnClose(wxCloseEvent& inEvent);
            void OnLoad(wxCommandEvent& inEvent);
            void OnOpenProjectFolder(wxCommandEvent& inEvent);
            void OnNew(wxCommandEvent& inEvent);
            void OnSave(wxCommandEvent& inEvent);
            void OnSaveAs(wxCommandEvent& inEvent);

        private:
            Editor::Garbage::Timer m_GarbageTimer;

            wxAuiManager m_AuiManager;
            wxMenuBar *m_MenuBar;
            wxAuiToolBar *m_AuiToolBar;
            wxAuiNotebook *m_AuiNotebookBottom,
                          *m_AuiNotebookLeft,
                          *m_AuiNotebookRight,
                          *m_AuiNotebookCenter;

            wxMenu *m_Views;

            /// \todo Créer une fonction permettant d'ajouter un wxMenuItem avec une case à cocher.
            ///       La valeur de cette case sera gérer par une fonction appeler lors de la création de ce wxMenuItem
            ///       et une autre ou la même qui sera appelée lors de la destruction de la vue.
            ///       A voir si c'est bien:
            ///       Lors de l'ajout d'un wxMenuItem, vérifier si le sous-menu est affiché sinon l'afficher.
            ///       Lors du retrait d'un wxMenuItem, vérifier si le sous-menu contient d'autres wxMenuItem, si il n'en
            ///       contient pas masque le sous-menu.

            std::unique_ptr<wxStreamToTextRedirector> m_StreamToTextRedirector;

            // Le chemin de l'application
            const std::filesystem::path m_ExecutablePath;

            // L'icône de l'application.
            const wxIcon m_AppIcon;

            void CreateMenu();
            void CreateToolbar();
            void CreateBottomNoteBook();
            void CreateLeftNoteBook();
            void CreateRightNoteBook();
            void CreateCenterNoteBook();

//            void OnFileSystemEvent(wxFileSystemWatcherEvent& event);

//            void CreateFileWatcher();

            wxAuiNotebook* CreateNoteBooks(const wxString &in_pane_id_name, wxAuiPaneInfo in_aui_pane_info);
    };
}
