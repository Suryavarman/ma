/// \file Views.h
/// \brief Permet d'observer les contextes et d'agir en fonctions de ceux qui sont présents.
/// \author Pierre Pontier
/// \date 2023-11-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <ma/Ma.h>

namespace ma::Editor
{
    class MenuItem: public ::wxMenuItem
    {
        public:
            const ma::wx::ViewPtr m_View;

            explicit MenuItem(wxMenu *parentMenu,
                                ma::wx::ViewPtr  view,
                                int id = wxID_SEPARATOR,
                                const wxString& text = wxEmptyString,
                                const wxString& help = wxEmptyString,
                                wxItemKind kind = wxITEM_CHECK,
                                wxMenu *subMenu = nullptr);
            ~MenuItem() override;
    };

    /// Le gestionnaire des vues
    /// En cours de transformation:
    /// Maintenant: elle observait la RTTI pour savoir quand un Contexte était ajouté.
    /// Futur: elle observera le conteneur de vues. Et en fonction des étiquettes et le type de celle-ci, elle saura
    /// dans quel menu et dans quel feuillet l'associer. L'item du menu qui lui sera associé permettra de l'afficher ou
    /// de la masquer.
    class ViewsMenu final : public wxMenu, public ma::Observer
    {
        private:
            /// Raccourcis pour accéder à la RTTI.
            ma::RTTIPtr m_RTTI;

            /// Raccourcis pour accéder au conteneur stockant les vues.
            ma::wx::ViewsPtr m_Views;

            /// Menu contenant les menus pour rendre les scènes 3D.
            wxMenu *m_Menu3D;
            ::wxMenuItem *m_MenuItem3D;

            /// Tout ce qui ne rentre pas dens le Menu3D.
            wxMenu *m_MenuOthers;
            ::wxMenuItem *m_MenuItemOthers;

            /// Exécute la mise à jour en fonction de l'état de la RTTI.
            void UpdateRTTIObservation();

            /// Exécute la mise à jour en fonction de l'état des vues.
            void UpdateViewsObservation();

            /// \return l'item du menu associé à l'id.
            /// \exception std::invalid_argument L'id ne pointe sur aucun élément ou l'élément n'est pas un
            ///            ma::Editor::wxMenuItem.
            MenuItem* GetMenuItem(int id, wxMenu **menu = nullptr) const;

            /// Initialise les menus :
            /// - m_Menu3D
            /// - m_MenuOthers
            void InitMenus();

            /// Défini où seront placé les vues.
            static wxAuiNotebook *GetBook(const ma::wx::ViewPtr& view);

            /// Ajoute une page.
            void AddPage(const ma::wx::ViewPtr& view, wxAuiNotebook* book, int menu_id);

            /// Ajoute un élément au menu, l'action associée créera un élément de type class_name héritant de
            /// ma::Engine::Context. Il sera placé dans un onglet du feuillet centrale.
            /// \param class_name Nom de la classe héritant de ma::Engine::Context.
            /// \param description Description de l'élément à ajouter au menu.
            void AddView3D(const std::wstring &class_name, const std::wstring &description);

            void AddView(const ma::wx::ViewPtr& view, const std::wstring &description);

            /// Cette fonction créera à partir de l'élément du menu sélectionné la vue du contexte.
            void OnCreateView3D(wxCommandEvent &event);

            void OnCreateView(wxCommandEvent &event);

        public:
            explicit ViewsMenu(const wxString& title, long style = 0);
            explicit ViewsMenu(long style = 0);
            ~ViewsMenu() override;

            // Concrétisation de ma::Observer
            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ViewsMenu, Editor)
    };
    typedef ma::wxObserverVar<ViewsMenu> ViewsMenuVar;
}
