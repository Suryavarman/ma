/// \file App.h
/// \brief Defines Application App
/// \author Pierre Pontier
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

/// \mainpage Maçon de l'espace - GUI documentation
/// \image html http://www.suryavarman.fr/wp-content/uploads/2019/04/Capture_GUI_01.jpeg
/// \section intro_sec Introduction
/// GUI définit une application graphique et permet de charger des scripts python qui la manipuleront ou pas.
/// \section ma Maçon de l'espace
/// GUI est l'un des trois modules de la solution <a href="https://sourceforge.net/projects/wxcpppy/"
/// target="_blank">Maçon de l'espace</a>.
///
/// L'aide pour installer ce projet :
/// - <a href="https://sourceforge.net/p/wxcpppy/code/ci/default/tree/README.md" target="_blank">README.md</a>
///
/// Les documentations générées par <a href="http://www.doxygen.nl/index.html" rel="noopener noreferrer"
/// target="_blank">Doxygen</a>:
/// - <a href="http://www.suryavarman.fr/stockage/wxcpppy/doc/api/" rel="noopener noreferrer" target="_blank">API</a>
/// - <a href="http://www.suryavarman.fr/stockage/wxcpppy/doc/api_py/" rel="noopener noreferrer"
/// target="_blank">API_Py</a>
/// - <a href="http://www.suryavarman.fr/stockage/wxcpppy/doc/gui/" rel="noopener noreferrer" target="_blank">GUI</a>
///
/// Information sur la license MIT.
/// - <a href="https://sourceforge.net/p/wxcpppy/code/ci/default/tree/LICENSE" target="_blank">LICENSE</a>
///

#pragma once

#include <wx/app.h>
#include <wx/fileconf.h>
#include <memory>
#include <filesystem>

#include <ma/Ma.h>

namespace ma::Editor
{
    ///  [project]
    ///  path=.ma/gui/projects/tata/
    /// \todo Observer le chemin de sauvegarde du nœud racine.
    class ConfigProject: public ma::ConfigGroup
    {
        public:
            /// Le dossier par défaut où seront enregistrer des projets créés par défaut.
            ma::DirectoryVarPtr m_DefaultProjectsFolder;

            /// Le chemin du projet
            ma::DirectoryVarPtr m_Path;

            static const ma::Item::key_type& GetConfigProjectKey();

            /// \brief Constructeur de ConfigProject.
            /// \param params Les paramètres de construction d'un item.
            /// \param projects_folder Le dossier où sont stockés les projets par défauts. Si celui-ci n'existe alors il sera
            /// créé.
            /// \exception std::invalid_argument Le dossier parent de « projects_folder » n'existe pas.
            ConfigProject(const Item::ConstructorParameters& params,
                          const std::filesystem::path& projects_folder);

            ConfigProject() = delete;

            /// \brief Destructeur de ConfigProject
            ~ConfigProject() override = default;

            void LoadVars() override;

            /// Renvoie le chemin d'un nouveau dossier enfant de projects_folder_path. Le dossier renvoyé n'existe pas.
            /// \exception std::invalid_argument Le dossier « projects_folder_path » n'existe pas.
            static std::filesystem::path GetNewProjectPath(const std::filesystem::path& projects_folder_path);

            /// Permet d'obtenir le nom du dossier représentant le nœud racine de la structure de données.
            std::wstring GetRootFolderSaveName() const;

            /// Permet de s'assurer que le chemin de la racine soit correctement initialisé.
            /// \param project_path Le chemin du projet. Le parent du dossier où la racine sera sauvegardé.
            /// Exemple:
            /// %user%/.ma/gui/projects/project_0
            static void SetSavePath(const std::filesystem::path &project_path);

            /// \brief Pour appeler SetSavePath.
            void EndConstruction() override;
    };
    typedef std::shared_ptr<ConfigProject> ConfigProjectPtr;

    /// ––––––––––––––––––––––––––––––-––--––-------––––[Configuration de Ma]–––––-–-––------–––––-––––––––––––––-––––––
    ///
    ///  L'éditeur de Ma.
    /// ––––––––––––––––––––––––––––––––––––––
    ///  .ma/gui/
    /// ––––––––––––––––––––––––––––––––––––––
    ///  Hiérarchie des dossiers dans le dossier « ~/.ma/gui/ »:
    ///  /config.cfg
    ///  /apps/7cbb1a57-61c9-4401-b1e6-7709cd0b7aae/config.cfg
    ///  /apps/e0ccf043-d254-4c5c-a317-ae0022b9502c/config.cfg
    ///
    /// ––––––––––––––––––––––––––––––––––––––
    ///  .ma/gui/config.cfg
    /// ––––––––––––––––––––––––––––––––––––––
    ///  Clef du groupe contenant les variables de l'environnement python. Elle est utilisée dans le fichier de config.
    ///  Exemple:
    ///  \code
    ///  [app_0]
    ///  path=/home/toto/blabla/Ma/code/.build/clion_clang_debug/applications/Editeur/MaEditor
    ///  guid=7cbb1a57-61c9-4401-b1e6-7709cd0b7aae
    ///
    ///  [app_1]
    ///  path=/home/toto/blabla/Ma/code/.build/Codeblocks_gcc/applications/Editeur/MaEditor
    ///  guid=e0ccf043-d254-4c5c-a317-ae0022b9502c
    ///  \endcode
    ///
    /// ––––––––––––––––––––––––––––––––––––––
    ///  .ma/gui/apps/7cbb1a57-61c9-4401-b1e6-7709cd0b7aae/config.cfg
    /// ––––––––––––––––––––––––––––––––––––––
    ///  \code
    ///  [project]
    ///  path=.ma/gui/projects/tata/
    ///  [python]
    ///  path=/home/toto/blabla/Ma/dependencies/venv
    ///  paths=/home/toto/blabla/Ma/dependencies/venv/lib64/python311.zip:/home/toto/blabla/Ma/dependencies/venv/lib64/python3.11
    ///  [ogre]
    ///  plugin_cfg_path=
    ///  config_cfg_path=
    ///  log_path=
    ///  resources_cfg_path=
    ///  [panda]
    ///  [cycle]
    ///  \endcode
    /// \todo Remplir python/paths avec « ma::toString({std::filesystem::path("/home/toto/python/truc"), ...}) ».
    /// \todo Créer une classe AppRoot et observer m_SavePath pour mettre à jour le fichier de configuration.
    /// \todo Gérer les fichiers de configuration dans AppRoot.
    /// \todo Créer une classe plugin ou module avec une configuration (get_group_key) (ini_values).
    class AppRoot: public ma::Root
    {
        public:
            /// .ma/gui/config.cfg
            std::unique_ptr<ma::Config> m_ConfigGui;

            /// \brief L'identifient de l'application. Il permet d'associer un dossier de configuration à l'application.
            /// La liste des GUID des applications graphiques de Ma est définies dans le fichier :
            /// %user%/.ma/gui/config.cfg
            /// Le fichier de configuration est défini ainsi :
            /// %user%/.ma/gui/apps/{app_guid}/config.cfg
            ///
            /// Il est défini de la manière suivante :
            /// 1. Si .ma/gui/config.cfg existe et s'il possède l'app avec le bon chemin du GUID associé sera utilisé comme
            /// clef.
            /// 2. Sinon l'application généra elle-même son GUID et l'enregistra dans .ma/gui/config.cfg et créera le
            /// dossier /apps/e0ccf043-d254-4c5c-a317-ae0022b9502c/config.cfg
            ma::StringVarPtr m_AppGuid;

            /// La configuration du projet courant.
            ConfigProjectPtr m_Project;

        private:
            /// \brief Cette fonction sert à obtenir le chemin du fichier de configuration pour la création du nœud
            /// racine de l'application.
            ///
            /// Dans cette implémentation, la fonction fait plusieurs choses :
            ///
            /// 1. Elle obtient le chemin du fichier de configuration qui associe chaque application héritant de « ma »
            ///    à un Guid. Le chemin est sur linux « ~/.ma/gui/config.cfg » .
            /// 2. Elle crée une nouvelle instance de la configuration en utilisant ce chemin.
            /// 3. Si ce chemin existe, la configuration est chargée depuis ce fichier.
            /// 4. Ensuite, elle obtient l'identifiant unique de l'application.
            ///    Exemple: 7cbb1a57-61c9-4401-b1e6-7709cd0b7aae
            /// 5. Cet identifiant unique est ensuite utilisé pour mettre à jour « ~.ma/gui/config.cfg » et pour
            ///    obtenir le chemin du fichier de configuration spécifique à cette application.
            ///    Exemple: ~/.ma/gui/apps/7cbb1a57-61c9-4401-b1e6-7709cd0b7aae/config.cfg
            ///
            /// \return Retourne le chemin du fichier de configuration pour l'application.
            /// \remarks Est utilisé seulement pour la création du nœud racine.
            static std::filesystem::path GetConfigAppFileNameForRootCreation();

            /// \brief Charge les paramètres du fichier de config
            /// .ma/gui/config.cfg
            /// S'il n'existe pas il sera généré avec les paramètres par défaut de l'application.
            static ma::Config* CreateConfigGui();

            /// \brief Permet de retrouver et si nécessaire de créer l'id de l'application.
            /// 1. Si .ma/gui/config.cfg existe et s'il possède l'app avec le bon chemin du GUID associé sera utilisé comme
            /// clef.
            /// 2. Sinon l'application généra elle-même son GUID et l'enregistra dans .ma/gui/config.cfg et créera le
            /// dossier .ma/gui/apps/{app_guid}
            /// Cette fonction s'assure que ConfigGui possède le bon GUID de l'application.
            /// \return L'identifiant de l'application.
            static std::wstring GetAppGUID(ma::Config& gui_config);

            /// \brief Permet d'obtenir le fichier de configuration de l'application.
            /// \param create_parents_folders Si vrais les dossiers parents seront créés s'ils n'existent pas.
            /// Seuls les dossiers parents locaux aux dossiers utilisateurs seront créés.
            static std::filesystem::path GetConfigGuiFileName(bool create_parents_folders = false);

            /// \brief Permet d'obtenir le chemin du fichier de configuration spécifique à cet executable.
            /// Chaque executable a en fonction de son chemin sur le disque son propre fichier de configuration.
            /// Exemple:
            ///     /apps/e0ccf043-d254-4c5c-a317-ae0022b9502c/config.cfg
            /// \remarks Elle n'est pas marquée constante, car elle fait appel à « CreateConfigGui ».
            static std::filesystem::path GetConfigAppFileName(const std::wstring& app_guid);

            /// \brief Initialise toutes les fabriques d'items.
            void InitRTTI();

        public:
            /// \brief Constructeur de AppRoot.
            /// \param in_params Les paramètres d'initialisation de tous les items. Ils seront définis par la fonction
            /// CreateRoot.
            /// \param app_guid @copydoc AppRoot::m_AppGuid
            explicit AppRoot(const ma::Item::ConstructorParameters &in_params);
            AppRoot() = delete;
            ~AppRoot() override;

            /// Chargement du projet.
            static void Load();

            /// Nous surchargeons la fonction de sauvegarde pour pouvoir sauvegarder les changements des configurations.
            void Save() override;

            /// \brief Pour appeler les EndConstruction des ConfigGroup
            void EndConstruction() override;
    };
    typedef std::shared_ptr<AppRoot> AppRootPtr;

    class App: public wxApp
    {
        private:
            AppRootPtr m_Root;

        public:
            bool OnInit() override;
            int OnExit() override;
            void OnInitCmdLine(wxCmdLineParser &parser) override;
            bool OnCmdLineParsed(wxCmdLineParser &parser) override;
            void OnEventLoopEnter(wxEventLoopBase* loop) override;
    };
}