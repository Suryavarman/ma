/// \file RTTI.h
/// \brief Initialisation de la RTTI de l'éditeur.
/// \author Pierre Pontier
/// \date 2023-10-22
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <ma/Item.h>

namespace ma::Editor::RTTI
{
    /// Initialise les types d'objets qui pourront être créer post-compilation.
    /// \param params Doit contenir la clef du gestionnaire de la RTTI.
    /// Exemple:
    /// \code{.cpp}
    /// ma::Item::CreateParameters params{m_RttiManager->GetKey()};
    /// Setup(params);
    /// \endcode
    void Setup(const ma::Item::CreateParameters& params);
}
