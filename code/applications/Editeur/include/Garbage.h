/// \file Garbage.h
/// \brief Gestion de la poubelle à « Item ».
/// \author Pierre Pontier
/// \date 2022-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <wx/app.h>
#include <wx/timer.h>

#include <ma/Ma.h>

namespace ma::Editor::Garbage
{
    class Timer : public wxTimer
    {
        protected:
            ma::GarbagePtr m_Garbage;
            ma::UVarPtr m_TimerCount;

        public:
            Timer();
            void Notify() final;
            void start();
    };
}

