/// \file RTTI.cpp
/// \brief Initialisation de la RTTI de l'éditeur.
/// \author Pierre Pontier
/// \date 2023-10-23
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "../include/RTTI.h"
#include <ma/Ma.h>

template<typename T_Maker>
void print(std::shared_ptr<T_Maker> maker)
{
    std::wcout << " - ma::" << maker->m_ClassName->Get() << std::endl;
}

namespace ma::Editor::RTTI
{
    void Setup(const ma::Item::CreateParameters& params)
    {
        std::wcout << L"===============================================" << '\n'
                   << _(L"Début de l'initialisation de la RTTI.") << std::endl;

        // Déclarations des items qui seront disponibles via la RTTI
        std::wcout << _(L"\nDéclarations des éléments héritant de « ma::Item ».") << '\n';
        print(ma::Item::CreateItem<ma::ItemMaker>(params));
        print(ma::Item::CreateItem<ma::FolderMaker>(params));
        // ma::Item::CreateItem<ma::wxGlobalParameterMaker>(params));
        print(ma::Item::CreateItem<ma::ConfigGroupMaker>(params));
        print(ma::Item::CreateItem<ma::ConfigItemMaker>(params));

        // Déclarations des variables qui seront disponibles via la RTTI
        std::wcout << _(L"\nDéclarations des variables héritant de « ma::Var ».") << '\n';
        print(ma::Item::CreateItem<ma::DefineVarMaker>(params));
        print(ma::Item::CreateItem<ma::StringVarMaker>(params));
        print(ma::Item::CreateItem<ma::BoolVarMaker>(params));
        print(ma::Item::CreateItem<ma::ItemVarMaker>(params));
        print(ma::Item::CreateItem<ma::IntVarMaker>(params));
        print(ma::Item::CreateItem<ma::LongVarMaker>(params));
        print(ma::Item::CreateItem<ma::LongLongVarMaker>(params));
        print(ma::Item::CreateItem<ma::UVarMaker>(params));
        print(ma::Item::CreateItem<ma::ULongVarMaker>(params));
        print(ma::Item::CreateItem<ma::ULongLongVarMaker>(params));
        print(ma::Item::CreateItem<ma::FloatVarMaker>(params));
        print(ma::Item::CreateItem<ma::DoubleVarMaker>(params));
        print(ma::Item::CreateItem<ma::LongDoubleVarMaker>(params));
        print(ma::Item::CreateItem<ma::EnumerationVarMaker>(params));
        print(ma::Item::CreateItem<ma::EnumerationChoiceVarMaker>(params));
        print(ma::Item::CreateItem<ma::ContextLinkMaker>(params));
        print(ma::Item::CreateItem<ma::wxColourVarMaker>(params));

        std::wcout << _(L"\nDéclarations des variables représentant des conteneurs.") << '\n';
        print(ma::Item::CreateItem<ma::StringHashSetVarMaker>(params));
        print(ma::Item::CreateItem<ma::StringSetVarMaker>(params));
        print(ma::Item::CreateItem<ma::ItemHashSetVarMaker>(params));
        print(ma::Item::CreateItem<ma::ItemVectorVarMaker>(params));

        std::wcout << _(L"\nDéclarations des variables liés au système de fichiers.") << '\n';
        print(ma::Item::CreateItem<ma::PathVarMaker>(params));
        print(ma::Item::CreateItem<ma::DirectoryVarMaker>(params));
        print(ma::Item::CreateItem<ma::FilePathVarMaker>(params));

        std::wcout << _(L"\nDéclarations des éléments héritant de « ma::View ».") << '\n';
        print(ma::Item::CreateItem<ma::wx::PanelResourcesExplorerViewMaker>(params));
        print(ma::Item::CreateItem<ma::wx::PanelResourcesExplorerParameterMaker>(params));

        print(ma::Item::CreateItem<ma::wx::TopMaker>(params));
        print(ma::Item::CreateItem<ma::wx::BottomMaker>(params));
        print(ma::Item::CreateItem<ma::wx::LeftMaker>(params));
        print(ma::Item::CreateItem<ma::wx::RightMaker>(params));
        print(ma::Item::CreateItem<ma::wx::CenterMaker>(params));

        print(ma::Item::CreateItem<ma::wx::XMaker>(params));
        print(ma::Item::CreateItem<ma::wx::YMaker>(params));
        print(ma::Item::CreateItem<ma::wx::XYMaker>(params));

        print(ma::Item::CreateItem<ma::wx::TextMaker>(params));
        print(ma::Item::CreateItem<ma::wx::WidgetsMaker>(params));
        print(ma::Item::CreateItem<ma::wx::RenderingMaker>(params));
        print(ma::Item::CreateItem<ma::wx::RealTimeMaker>(params));
        print(ma::Item::CreateItem<ma::wx::PrecomputeMaker>(params));

        std::wcout << _(L"\nDéclarations des éléments de « ma::Engine ».") << '\n';
        // ma::Item::CreateItem<ma::Engine::NodeiMaker>(params));
        // ma::Item::CreateItem<ma::Engine::NodeuMaker>(params));
        // ma::Item::CreateItem<ma::Engine::NodelMaker>(params));
        // ma::Item::CreateItem<ma::Engine::NodellMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::NodefMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::NodedMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::NodeldMaker>(params));

        print(ma::Item::CreateItem<ma::Engine::ScenefMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::ScenedMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::SceneldMaker>(params));

        print(ma::Item::CreateItem<ma::Engine::CamerafMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::CameradMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::CameraldMaker>(params));

        print(ma::Item::CreateItem<ma::Engine::CubefMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::CubedMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::CubeldMaker>(params));

        print(ma::Item::CreateItem<ma::Engine::LightfMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::LightdMaker>(params));
        print(ma::Item::CreateItem<ma::Engine::LightldMaker>(params));

        // Déclarations des wxVar (des observateurs de variables) qui seront
        // disponibles via la RTTI

        // todo corriger wxVarItem::SetVar m_Var != v)a
        // ma::Item::CreateItem<ma::wx::VarItemMaker>(params));
        std::wcout << _(L"\nDéclarations des éléments de « ma::wx ».") << '\n';
        print(ma::Item::CreateItem<ma::wx::VarStringMaker>(params));
        print(ma::Item::CreateItem<ma::wx::VarDirectoryMaker>(params));
        print(ma::Item::CreateItem<ma::wx::VarFileNameMaker>(params));
        print(ma::Item::CreateItem<ma::wx::ColourPanelMaker>(params));
        print(ma::Item::CreateItem<ma::wx::ItemListMaker>(params));

        // Eigen:
        std::wcout << _(L"\nDéclarations des éléments de « ma::Eigen ».") << '\n';
        print(ma::Item::CreateItem<ma::Eigen::Vector2iVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector2uVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector2lVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector2llVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector2fVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector2dVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector2ldVarMaker>(params));

        print(ma::Item::CreateItem<ma::Eigen::Vector3iVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector3uVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector3lVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector3llVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector3fVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector3dVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector3ldVarMaker>(params));

        print(ma::Item::CreateItem<ma::Eigen::Vector4iVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector4uVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector4lVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector4llVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector4fVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector4dVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Vector4ldVarMaker>(params));

        // ma::Item::CreateItem<ma::Eigen::QuaternioniVarMaker>(params));
        // ma::Item::CreateItem<ma::Eigen::QuaternionuVarMaker>(params));
        // ma::Item::CreateItem<ma::Eigen::QuaternionlVarMaker>(params));
        // ma::Item::CreateItem<ma::Eigen::QuaternionllVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::QuaternionfVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::QuaterniondVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::QuaternionldVarMaker>(params));

        print(ma::Item::CreateItem<ma::Eigen::Matrix22iVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix22uVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix22lVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix22llVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix22fVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix22dVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix22ldVarMaker>(params));

        print(ma::Item::CreateItem<ma::Eigen::Matrix33iVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix33uVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix33lVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix33llVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix33fVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix33dVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix33ldVarMaker>(params));

        print(ma::Item::CreateItem<ma::Eigen::Matrix44iVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix44uVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix44lVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix44llVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix44fVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix44dVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::Matrix44ldVarMaker>(params));

        // ma::Item::CreateItem<ma::Eigen::TransformiVarMaker>(params);
        // ma::Item::CreateItem<ma::Eigen::TransformuVarMaker>(params);
        // ma::Item::CreateItem<ma::Eigen::TransformlVarMaker>(params);
        // ma::Item::CreateItem<ma::Eigen::TransformllVarMaker>(params);
        print(ma::Item::CreateItem<ma::Eigen::TransformfVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::TransformdVarMaker>(params));
        print(ma::Item::CreateItem<ma::Eigen::TransformldVarMaker>(params));

        std::wcout << _(L"\nFin de l'initialisation de la RTTI.") << '\n'
                   << L"===============================================" << std::endl;
    }
}