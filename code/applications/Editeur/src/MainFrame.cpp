/// \file MainFrame.cpp
/// \brief Defines Application Frame
/// \author Pierre Pontier
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#if !wxUSE_MENUS
    #error "L'utilisitaion des menus est requise veuillez recompiler wxWidgets avec la variable wxUSE_MENUS à 1."
#endif

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include <wx/stdpaths.h>
#include <wx/artprov.h>
#include <wx/aboutdlg.h>

#include <memory>
#include <sstream>
#include <list>

#include <fstream>
#include <iostream>

#include <ma/Version.h>

#include "../include/MainFrame.h"
#include "../include/Views.h"
#include "../include/App.h"

//helper functions
enum wxbuildinfoformat
{
    short_f, long_f
};

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wx_build(wxVERSION_STRING);

    if (format == long_f )
    {
        #if defined(__WXMSW__)
            wxbuild << "-Windows";
        #elif defined(__WXMAC__)
            wxbuild << "-Mac";
        #elif defined(__UNIX__)
            wx_build << "-Linux";
        #endif

        #if wxUSE_UNICODE
            wx_build << "-Unicode build";
        #else
            wxbuild << "-ANSI build";
        #endif // wxUSE_UNICODE
    }

    return wx_build;
}

namespace ma::Editor
{
    const long MainFrame::ID_MenuAddResourcePath = wxNewId();

    // Constructeur et destructeur //———————————————————————————————————————————————————————————————————————————————————————

    MainFrame::MainFrame(wxFrame *inFrame, const wxString &inTitle, const wxPoint &inPos, const wxSize &inSize):
    wxFrame(inFrame, wxID_ANY, inTitle, inPos, inSize, wxDEFAULT_FRAME_STYLE | wxNO_FULL_REPAINT_ON_RESIZE),
    m_AuiManager(this),
    m_MenuBar(nullptr),
    m_AuiToolBar(nullptr),
    m_AuiNotebookBottom(nullptr),
    m_AuiNotebookLeft(nullptr),
    m_AuiNotebookRight(nullptr),
    m_AuiNotebookCenter(nullptr),
    m_Views(nullptr),
    m_ExecutablePath{wxStandardPaths::Get().GetExecutablePath().ToStdString()},
    m_AppIcon(wxIcon((m_ExecutablePath.parent_path() / "images" / "ma.ico").string()))
    {
        m_GarbageTimer.start();

        CreateMenu();
        CreateToolbar();

        #if wxUSE_STATUSBAR
            // create a status bar with some information about the used wxWidgets version
            CreateStatusBar(2);
            SetStatusText(_(L"Chargement de l'éditeur réussi."),0);
            SetStatusText(wxbuildinfo(short_f), 1);
        #endif // wxUSE_STATUSBAR

        CreateLeftNoteBook();
        CreateBottomNoteBook();
        CreateRightNoteBook();
        CreateCenterNoteBook();

        // Tien, tien, dans la sortie de la fenêtre de log les accents ne sont pas bien interprété et/ou encodé.
        // std::wcout << _(L"Le répertoire de Python défini dans python : ") << wxString(Py_GetPythonHome()) << std::endl;

        //Bind(wxEVT_MENU, &MainFrame::OnOpenScript, this, wxID_FILE);
        Bind(wxEVT_MENU, &MainFrame::OnNew, this, wxID_NEW);
        Bind(wxEVT_MENU, &MainFrame::OnLoad, this, wxID_REVERT_TO_SAVED);
        Bind(wxEVT_MENU, &MainFrame::OnOpenProjectFolder, this, wxID_OPEN);
        Bind(wxEVT_MENU, &MainFrame::OnSave, this, wxID_SAVE);
        Bind(wxEVT_MENU, &MainFrame::OnSaveAs, this, wxID_SAVEAS);
        Bind(wxEVT_MENU, &MainFrame::OnAddDirRes, this, static_cast<int>(ID_MenuAddResourcePath));
        Bind(wxEVT_MENU, &MainFrame::OnAbout, this, wxID_ABOUT);
        Bind(wxEVT_MENU, [=](wxCommandEvent&){Close(true);}, wxID_EXIT);
//        Bind(wxEVT_FSWATCHER, &MainFrame::OnFileSystemEvent, this);
        Bind(wxEVT_CLOSE_WINDOW, &MainFrame::OnClose, this);

        Maximize();
    }

    MainFrame::~MainFrame() = default;

//    bool MainFrame::CreateFileWatcherIfNecessary()
//    {
//        if (ma::root()->m_ResourceManager->m_FileSystemWatcher)
//            return false;
//
//        CreateFileWatcher();
//        Bind(wxEVT_FSWATCHER, &MainFrame::OnFileSystemEvent, this);
//
//        return true;
//    }
//
//    void MainFrame::CreateFileWatcher()
//    {
//        auto& watcher = ma::root()->m_ResourceManager->m_FileSystemWatcher;
//
//        MA_ASSERT(!watcher,
//                  L"root->m_ResourceManager->m_FileSystemWatcher a déjà été initialisé.",
//                  std::logic_error);
//
//        // https://github.com/wxWidgets/wxWidgets/blob/master/samples/fswatcher/fswatcher.cpp
//        watcher = std::make_unique<wxFileSystemWatcher>();
//        watcher->SetOwner(this);
//    }


    // Évènements //————————————————————————————————————————————————————————————————————————————————————————————————————————

    void MainFrame::OnAbout(wxCommandEvent &inEvent)
    {
        wxAboutDialogInfo info;

        info.SetName(wxT("Maçon de l'espace"));

        std::wstringstream ss_ma;
        ss_ma << L"Version: " << MA_VERSION_STRING << "\n";
        ss_ma << L"Status: " << MA_STATUS_VERBOSE << "\n";
        //ss_ma << L"Numéro de compilation: " << MA_BUILDS_COUNT << "\n";
        ss_ma << L"Date de compilation: " << MA_BUILD_DAY << L"/" << MA_BUILD_MONTH << L"/" << MA_BUILD_YEAR << "\n";
        info.SetVersion(ss_ma.str());

        info.SetCopyright(wxT("(C) 2018-") + ma::toString(MA_BUILD_YEAR));
        info.SetWebSite(wxT("https://framagit.org/Suryavarman/ma/"), wxT("Dépôt des sources"));

        const wxString license_str = _(
            L"Licence MIT \n"
            L"L'autorisation est accordée, gracieusement, à toute \n"
            L"personne acquérant une copie de cette bibliothèque et des fichiers \n"
            L"de documentation associés (la «Bibliothèque»), de commercialiser la \n"
            L"Bibliothèque sans restriction, notamment les droits d'utiliser, de \n"
            L"copier, de modifier, de fusionner, de publier, de distribuer, de \n"
            L"sous-licencier et / ou de vendre des copies de la Bibliothèque, \n"
            L"ainsi que d'autoriser les personnes auxquelles la Bibliothèque est \n"
            L"fournie à le faire, sous réserve des conditions suivantes : \n"
            L"La déclaration de copyright ci-dessus et la présente \n"
            L"autorisation doivent être incluses dans toutes copies ou parties \n"
            L"substantielles de la Bibliothèque.\n"
            L"\n"
            L"LA BIBLIOTHÈQUE EST FOURNIE «TELLE QUELLE», SANS \n"
            L"GARANTIE D'AUCUNE SORTE, EXPLICITE OU IMPLICITE, NOTAMMENT SANS \n"
            L"GARANTIE DE QUALITÉ MARCHANDE, D’ADÉQUATION À UN USAGE PARTICULIER ET \n"
            L"D'ABSENCE DE CONTREFAÇON. EN AUCUN CAS, LES AUTEURS OU TITULAIRES DU \n"
            L"DROIT D'AUTEUR NE SERONT RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU \n"
            L"AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT\n"
            L" OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA \n"
            L"BIBLIOTHÈQUE OU SON UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA \n"
            L"BIBLIOTHÈQUE.\n");

        info.SetLicence(license_str);

        info.AddDeveloper(wxT("Suryavarman"));

        info.SetIcon(m_AppIcon);
        //info.SetIcon(this->GetIcon());

    #if __cplusplus < 20110
        const std::wstring cpp_version = L"c99";
    #elif __cplusplus < 201402
        const std::wstring cpp_version = L"11";
    #elif __cplusplus < 201703
        const std::wstring cpp_version = L"14";
    #elif __cplusplus < 202002
        const std::wstring cpp_version = L"17";
    #elif __cplusplus < 202300
        const std::wstring cpp_version = L"20";
    #elif __cplusplus <= 202302
        const std::wstring cpp_version = L"23";
    #else
        const std::wstring cpp_version = L"26";
    #endif // __cplusplus

        std::wstringstream ss_desc;
        ss_desc << _(L"Éditeur pour les maçons de l'espace. \n");
        ss_desc << _(L"Version de c++ utilisée: ") << cpp_version << L"\n";

    #if(MA_COMPILER == MA_COMPILER_GCCE)
        ss_desc << _(L"Application compiler avec GCCE: ") << MA_COMP_VER << L"\n";
    #endif // MA_COMPILER_GCCE

    #if(MA_COMPILER == MA_COMPILER_WINSCW)
        ss_desc << _(L"Application compiler avec WINSCW: ") << MA_COMP_VER << L"\n";
    #endif // MA_COMPILER_WINSCW

    #if(MA_COMPILER == MA_COMPILER_MSVC)
        ss_desc << _(L"Application compiler avec MSVC: ") << MA_COMP_VER << L"\n";
    #endif // MA_COMPILER_MSVC

    #if(MA_COMPILER == MA_COMPILER_CLANG)
        ss_desc << _("LApplication compiler avec Clang: ")
                << __clang_major__ << L"."
                << __clang_minor__ << L"."
                << __clang_patchlevel__ << L"\n";
    #endif // MA_COMPILER_CLANG

    #if(MA_COMPILER == MA_COMPILER_GNUC)
        ss_desc << _(L"Application compiler avec GCC: ")
                << __GNUC__ << L"."
                << __GNUC_MINOR__ << L"."
                << __GNUC_PATCHLEVEL__ << L"\n";
    #endif // MA_COMPILER_GNUC

    #if(MA_COMPILER == MA_COMPILER_BORL)
        ss_desc << _(L"Application compiler avec Borland: " << MA_COMP_VER << L"\n";
    #endif // MA_COMPILER_BORL

        ss_desc << _(L"Versions de wxWidgets: ") << wxbuildinfo(long_f) << L"\n";
        // ss_desc << _(L"Versions de Python: ") << ma::to_wstring(Py_GetVersion()) << L"\n";

        info.SetDescription(ss_desc.str());

        wxAboutBox(info);
    }

    void MainFrame::OnAddDirRes(wxCommandEvent &inEvent)
    {
        wxDirDialog dlg(this,
                        _(L"Choisissez un dossier"),
                        m_ExecutablePath.parent_path().string(),
                        wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST);

        if(dlg.ShowModal() == wxID_OK)
        {
            std::wstring path = dlg.GetPath().ToStdWstring();

            SetStatusText(_(L"Fichier : ") + path, 0);

            auto root = ma::root();

            if(!root->m_ResourceManager->HasFolder(path))
            {
                const bool recursive = true;
                try
                {
                    wxBusyCursor wait;
                    auto folder = root->m_ResourceManager->AddFolder(path, recursive);
                    MA_ASSERT(ma::Item::HasItem(folder->GetKey()),
                              L"Erreur l'item représente un dossier qui n'est pas dans la hiérarchie des items.",
                              std::logic_error);

                    root->m_Current->SetItem(folder);
                    wxString message = _(L"Début de la scrutation des ressources du dossier : ") + path;
                    SetStatusText(message, 0);
                }
                catch(const std::exception& e)
                {
                    SetStatusText(e.what(), 0);
                }
            }
            else
            {
                SetStatusText(_(L"Ce dossier est déjà surveillé: ") + path, 0);
            }
        }
    }

    void MainFrame::OnClose(wxCloseEvent& inEvent)
    {
        m_GarbageTimer.Stop();
        inEvent.Skip();
    }

//    void MainFrame::OnFileSystemEvent(wxFileSystemWatcherEvent& event)
//    {
//        ma::root()->m_ResourceManager->OnFileSystemEvent(event);
//    }

    void MainFrame::OnLoad(wxCommandEvent &inEvent)
    {
        Load();
    }

    void MainFrame::OnOpenProjectFolder(wxCommandEvent &inEvent)
    {
        wxDirDialog dlg(this,
                        _(L"Choisissez un dossier"),
                        m_ExecutablePath.parent_path().string(),
                        wxDD_DEFAULT_STYLE | wxDD_SHOW_HIDDEN);

        if(dlg.ShowModal() == wxID_OK)
        {
            const std::filesystem::path path = dlg.GetPath().ToStdWstring();
            const auto path_str = path.wstring();

            if(std::filesystem::exists(path))
            {
                SetStatusText(_(L"Dossier : ") + path_str, 0);

                auto root = ma::root();

                root->m_SavePath->fromString(path_str);

                // Si le dossier est vide, nous créons le projet.
                if(std::filesystem::is_empty(path))
                {
                    Save();
                }
                // S'il ne l'est pas, nous tentons de le charger.
                else
                {
                    Load();
                }
            }
            else
            {
                SetStatusText(_(L"Le Dossier n'existe pas: ") + path_str, 0);
            }
        }
    }

    void MainFrame::OnNew(wxCommandEvent& inEvent)
    {
        MA_WARNING(false,
                   L"N'est pas encore implémentée.");
    }

    void MainFrame::OnSave(wxCommandEvent &inEvent)
    {
        Save();
    }

    void MainFrame::OnSaveAs(wxCommandEvent &inEvent)
    {
        wxDirDialog dlg(this,
                        _(L"Choisissez un dossier où sauvegarder le projet"),
                        m_ExecutablePath.parent_path().string(),
                        wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST | wxDD_CHANGE_DIR);

        if(dlg.ShowModal() == wxID_OK)
        {
            auto root = ma::root();
            const auto path_str = (std::filesystem::path(dlg.GetPath().ToStdWstring()) / root->GetKey()).wstring();

            SetStatusText(_(L"Dossier : ") + path_str, 0);

            root->m_CustomSavePath->fromString(ma::toString(true));
            root->m_SavePath->fromString(path_str);

            OnSave(inEvent);
        }
    }

    // Créations de composants graphiques //————————————————————————————————————————————————————————————————————————————

    void MainFrame::CreateBottomNoteBook()
    {
        wxASSERT(!m_AuiNotebookBottom);
        m_AuiNotebookBottom = CreateNoteBooks(L"AuiNotebookBottom", wxAuiPaneInfo().Bottom().MinSize(600, 300));
        ma::root()->m_wxGuiAccess->SetBottomNotebook(m_AuiNotebookBottom);

        // https://docs.wxwidgets.org/trunk/classwx_text_ctrl.html
        auto *text_ctrl = new wxTextCtrl(m_AuiNotebookBottom,
                                         wxID_ANY,
                                         wxEmptyString,
                                         wxDefaultPosition,
                                         wxDefaultSize,
                                         wxTE_MULTILINE | wxTE_READONLY);

        ma::wx::g_TextCtrl = text_ctrl;

        wxSetAssertHandler(ma::wx::AssertHandler);

        m_StreamToTextRedirector = std::make_unique<wxStreamToTextRedirector>(text_ctrl, &std::cout);

        m_AuiNotebookBottom->AddPage(text_ctrl,
                                     L"Logs",
                                     false,
                                     wxArtProvider::GetBitmap(wxART_WARNING, wxART_FRAME_ICON));

//        title = _(L"Explorateur de ressources.");
//        auto* resource_explorer = new ma::wx::PanelResourcesExplorer(m_AuiNotebookBottom, title);
//
//        m_AuiNotebookBottom->AddPage(resource_explorer,
//                                     title,
//                                     false,
//                                     wxArtProvider::GetBitmap(wxART_FOLDER, wxART_FRAME_ICON));

        if(!ma::Item::HasItem(ma::wx::PanelResourcesExplorerView::Key::Get()))
            ma::Item::CreateItem<ma::wx::PanelResourcesExplorerView>({ma::wx::Views::Key::Get()});
    }

    void MainFrame::CreateCenterNoteBook()
    {
        wxASSERT(!m_AuiNotebookCenter);
        m_AuiNotebookCenter = CreateNoteBooks(_(L"AuiNotebookCenter"), wxAuiPaneInfo().CenterPane());
        ma::root()->m_wxGuiAccess->SetCenterNoteBook(m_AuiNotebookCenter);

        wxString title(_(L"Présentation du feuillet du centre."));

        auto *text_ctrl = new wxTextCtrl(m_AuiNotebookCenter,
                                         wxID_ANY,
                                         wxEmptyString,
                                         wxDefaultPosition,
                                         wxDefaultSize,
                                         wxTE_MULTILINE);

        const auto comment_colour = wxColour(95, 130, 107);
        const auto key_word_colour = wxColour(207, 142, 109);
        const auto code_colour = wxColour(188, 190, 196);
        const auto operator_colour = wxColour(188, 190, 196);

        const auto comment_text_attr = wxTextAttr(comment_colour, *wxBLACK);
        const auto key_word_text_attr = wxTextAttr(key_word_colour, *wxBLACK);
        const auto code_text_attr = wxTextAttr(code_colour, *wxBLACK);
        const auto operator_text_attr = wxTextAttr(operator_colour, *wxBLACK);

        text_ctrl->SetDefaultStyle(comment_text_attr);
        text_ctrl->AppendText(_(L"# Zone te texte du feuillet du centre.\n"));
        text_ctrl->AppendText(_(L"# Voici un exemple pour accéder au feuillet via python.\n\n"));
        text_ctrl->SetDefaultStyle(key_word_text_attr);
        text_ctrl->AppendText(L"import");
        text_ctrl->SetDefaultStyle(code_text_attr);
        text_ctrl->AppendText(L" wx\n");
        text_ctrl->SetDefaultStyle(key_word_text_attr);
        text_ctrl->AppendText(L"import");
        text_ctrl->SetDefaultStyle(code_text_attr);
        text_ctrl->AppendText(L" wx");
        text_ctrl->SetDefaultStyle(operator_text_attr);
        text_ctrl->AppendText(L".");
        text_ctrl->SetDefaultStyle(code_text_attr);
        text_ctrl->AppendText(L"aui\n");
        text_ctrl->SetDefaultStyle(key_word_text_attr);
        text_ctrl->AppendText(L"import");
        text_ctrl->SetDefaultStyle(code_text_attr);
        text_ctrl->AppendText(L" ma\n\nauiNotebookCenter ");
        text_ctrl->SetDefaultStyle(operator_text_attr);
        text_ctrl->AppendText(L"=");
        text_ctrl->SetDefaultStyle(code_text_attr);
        text_ctrl->AppendText(L" ma");
        text_ctrl->SetDefaultStyle(operator_text_attr);
        text_ctrl->AppendText(L".");
        text_ctrl->SetDefaultStyle(code_text_attr);
        text_ctrl->AppendText(L"wx");
        text_ctrl->SetDefaultStyle(operator_text_attr);
        text_ctrl->AppendText(L".");
        text_ctrl->SetDefaultStyle(code_text_attr);
        text_ctrl->AppendText(L"get_center()\n");

        m_AuiNotebookCenter->AddPage(text_ctrl,
                                     title,
                                     false,
                                     wxArtProvider::GetBitmap(wxART_INFORMATION, wxART_FRAME_ICON));
    }

    void MainFrame::CreateLeftNoteBook()
    {
        wxASSERT(!m_AuiNotebookLeft);
        m_AuiNotebookLeft = CreateNoteBooks(L"AuiNotebookLeft", wxAuiPaneInfo().Left().MinSize(350, 600));
        ma::root()->m_wxGuiAccess->SetLeftNoteBook(m_AuiNotebookLeft);

        auto* tree = new ma::wx::TreeItems(m_AuiNotebookLeft);
        m_AuiNotebookLeft->AddPage(tree,
                                   _(L"Arbre des « ma::Item »."),
                                   false,
                                   wxArtProvider::GetBitmap(wxART_LIST_VIEW, wxART_FRAME_ICON));
    }

    void MainFrame::CreateMenu()
    {
        wxASSERT(!m_MenuBar);
        // create a menu bar
        m_MenuBar = new wxMenuBar();

        auto* ma_menu = new wxMenu();
        ma_menu->Append(wxID_ABOUT, _(L"&A propos\tF1"), _(L"Affiche les informations à propos de cette application."));
        ma_menu->Append(wxID_EXIT, _(L"&Quit\tAlt-F4"), _(L"Quitter l'application"));

        auto* project_menu = new wxMenu();
        project_menu->Append(wxID_OPEN, _(L"&Ouvrir projet\tCtrl+O"), _(L"Charger un autre projet."));
        project_menu->Append(wxID_NEW, _(L"&Nouveau\tCtrl+N"), _(L"Partir d'un nouveau projet."));
        project_menu->Append(wxID_SAVE, _(L"&Sauvegarder\tCtrl+S"), _(L"Sauvegarder le projet."));
        project_menu->Append(wxID_REVERT_TO_SAVED, _(L"&Recharger\tAlt+S"), _(L"Recharger le projet depuis la sauvegarde."));
        project_menu->Append(wxID_SAVEAS, _(L"Sauvegarder dans"), _(L"Sauvegarder le projet dans un autre dossier."));

        auto* resources_menu = new wxMenu();
        auto* add_res_item = new ::wxMenuItem(resources_menu,
                                              static_cast<int>(ID_MenuAddResourcePath),
                                              _(L"&Ajouter chemin de ressources\tCtrl+1"),
                                              _(L"Ajouter un chemin de dossier pour y scruter les ressources "
                                              L"qu'il contient."));
        add_res_item->SetBitmap(wxArtProvider::GetBitmap(wxART_NEW_DIR, wxART_MENU));
        resources_menu->Append(add_res_item);

        m_Views = new ma::Editor::ViewsMenu();

    //    auto* scripts_menu = new wxMenu();
    //    auto* python_exec_item = new wxMenuItem(scripts_menu, wxID_FILE, _(L"&Exécuter\tCtrl+E"), _(L"Exécuter un script python."));
    //    python_exec_item->SetBitmap(wxArtProvider::GetBitmap(wxART_EXECUTABLE_FILE, wxART_MENU));
    //    scripts_menu->Append(python_exec_item);

        m_MenuBar->Append(ma_menu, _(L"&Ma"));
        m_MenuBar->Append(project_menu, _(L"&Projet"));
        m_MenuBar->Append(resources_menu, _(L"&Ressources"));
    //    m_MenuBar->Append(scripts_menu, _(L"&Scripts"));
        m_MenuBar->Append(m_Views, _(L"&Vues"));

        SetMenuBar(m_MenuBar);

        SetIcon(m_AppIcon);

        ma::root()->m_wxGuiAccess->SetMenuBar(m_MenuBar);
    }

    wxAuiNotebook* MainFrame::CreateNoteBooks(const wxString &in_pane_id_name, wxAuiPaneInfo in_aui_pane_info)
    {
        // create the notebook off-window to avoid flicker
        wxSize client_size = GetClientSize();

        auto *noteBook = new wxAuiNotebook(this, wxID_ANY, wxPoint(client_size.x, client_size.y), wxSize(430, 200));

        auto auiPaneInfo = in_aui_pane_info.Name(in_pane_id_name)
                                           .PaneBorder(false)
                                           .CloseButton(false)
                                           .DestroyOnClose(true);

        m_AuiManager.AddPane(noteBook, auiPaneInfo);
        m_AuiManager.Update();

        return noteBook;
    }

    void MainFrame::CreateRightNoteBook()
    {
        wxASSERT(!m_AuiNotebookRight);
        m_AuiNotebookRight = CreateNoteBooks(_("AuiNotebookRight"), wxAuiPaneInfo().Right().MinSize(350, 600));
        ma::root()->m_wxGuiAccess->SetRightNoteBook(m_AuiNotebookRight);

        auto* panel_item = new ma::wx::PanelItem(m_AuiNotebookRight);
        m_AuiNotebookRight->AddPage(panel_item,
                                    _(L"Propriétés de l'item courant."),
                                    false,
                                    wxArtProvider::GetBitmap(wxART_LIST_VIEW, wxART_FRAME_ICON));
    }

    void MainFrame::CreateToolbar()
    {
        wxASSERT(!m_AuiToolBar);

        m_AuiToolBar = new wxAuiToolBar(this,
                                        wxID_ANY,
                                        wxDefaultPosition,
                                        wxDefaultSize,
                                        wxAUI_TB_DEFAULT_STYLE);// | wxAUI_TB_OVERFLOW);

        m_AuiToolBar->SetToolBitmapSize(wxSize(48, 48));

        m_AuiToolBar->AddTool(wxID_OPEN,
                              _(L"&Ouvrir projet\tCtrl+O"),
                              wxArtProvider::GetBitmap(wxART_FILE_OPEN, wxART_TOOLBAR),
                              _(L"Charger un autre projet."));

        m_AuiToolBar->AddTool(static_cast<int>(ID_MenuAddResourcePath),
                              _(L"&Ajouter un dossier de ressources\tCtrl+1"),
                              wxArtProvider::GetBitmap(wxART_NEW_DIR, wxART_TOOLBAR),
                              _(L"Ajouter un dossier de ressources."));

    //    m_AuiToolBar->AddTool(wxID_FILE,
    //                          _(L"&Exécuter\tCtrl+E"),
    //                          wxArtProvider::GetBitmap(wxART_EXECUTABLE_FILE, wxART_TOOLBAR),
    //                          _(L"Exécuter un script python."));

        m_AuiManager.AddPane(m_AuiToolBar,
                             wxAuiPaneInfo().Name(_("Toolbar")).Caption(wxEmptyString).ToolbarPane().Top());

        m_AuiManager.Update();

        ma::root()->m_wxGuiAccess->SetToolBar(m_AuiToolBar);
    }

    // Fonctions de chargement //———————————————————————————————————————————————————————————————————————————————————————
    void MainFrame::Load()
    {
        ma::Editor::AppRoot::Load();
        wxString message = _(L"Le projet a été chargé depuis le dossier: ") + ma::root()->m_SavePath->toString();
        SetStatusText(message, 0);
    }

    void MainFrame::Save()
    {
        auto root = ma::root();

        root->Save();

        wxString message = _(L"Le projet a été sauvegardé dans le dossier: ") + root->m_SavePath->toString();
        SetStatusText(message, 0);
    }
}