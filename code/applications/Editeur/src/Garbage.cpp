/// \file Garbage.cpp
/// \brief Gestion de la poubelle à « Item ».
/// \author Pierre Pontier
/// \date 2022-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "../include/Garbage.h"

namespace ma::Editor::Garbage
{
    Timer::Timer():
    wxTimer(),
    m_Garbage(ma::root()->m_Garbage),
    m_TimerCount{nullptr}
    {
        const auto timer_var_key = L"m_Timer"s;

        if(m_Garbage)
        {
            m_TimerCount = m_Garbage->AddVar<ma::UVar>(timer_var_key, 10);
            m_TimerCount->InsertIInfoKeyValue(ma::Var::Key::IInfo::ms_Doc,
                                              L"Défini le tempo de vidange de cette poubelle."
                                              L"Attention le temps se mettra à jour à la prochaine vidange.");
        }
    }

    void Timer::Notify()
    {
        m_Garbage->CleanTrashes();

        if(GetInterval() != static_cast<int>(m_TimerCount->Get()) * 1000)
        {
            Stop();
            start();
        }
    }

    void Timer::start()
    {
        if(m_Garbage)
        {
            const int seconds = m_TimerCount ? static_cast<int>(m_TimerCount->Get()) : 10;
            wxTimer::Start(1000 * seconds);
        }
    }
}