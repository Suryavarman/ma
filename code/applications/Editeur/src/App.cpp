/// \file App.cpp
/// \brief Defines Application App
/// \author Pierre Pontier
/// \date 2019-07-24
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

// Inclus avant pour éviter que la classe wxWindowIDRef soit incomplète et ainsi
// avoir l'erreur suivante:
// wx/windowid.h:28:24: error: variable ‘WXDLLIMPEXP_CORE wxWindowIDRef’ has
// initializer but incomplete type
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <wx/windowid.h>

#include <wx/utils.h> // GetEnv
#include <wx/cmdline.h>
#include <wx/stdpaths.h>
#include <wx/wfstream.h> // wxFileStream

#include <sstream>
#include <tuple>
#include <list>

#include "../include/App.h"
#include "../include/MainFrame.h"
#include "../include/RTTI.h"

// https://wiki.wxwidgets.org/Command-Line_Arguments
// clang-format off
static const wxCmdLineEntryDesc g_cmdLineDesc [] =
{
    { wxCMD_LINE_SWITCH, "h", "help", _(L"Affiche l'aide des lignes de commandes de l'application."), wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
    { wxCMD_LINE_OPTION, "e", "env", _(L"Le chemin de l'environnement python. Exemple: « /home/exemple/venv »"), wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
    { wxCMD_LINE_NONE }
};
// clang-format on

const std::wstring print_line = L"===============================================";

IMPLEMENT_APP(ma::Editor::App)
//wxIMPLEMENT_APP(App);

namespace ma::Editor
{
    // Project //———————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Item::key_type &ConfigProject::GetConfigProjectKey()
    {
        // https://guidgenerator.com/online-guid-generator.aspx
        static const auto key = L"59c61a17-f30e-4105-ba1a-1825d3f5afa8"s;
        return key;
    }

    ConfigProject::ConfigProject(const Item::ConstructorParameters &params, const std::filesystem::path& projects_folder):
    ma::ConfigGroup(params, L"project"),
    m_DefaultProjectsFolder(AddReadOnlyMemberVar<ma::DirectoryVar>(L"m_DefaultProjectsFolder", projects_folder.wstring())),
    m_Path(AddMemberVar<ma::DirectoryVar>(L"m_Path", GetNewProjectPath(projects_folder).wstring()))
    {
        if(!std::filesystem::exists(projects_folder))
        {
            auto const parent = projects_folder.parent_path();
            MA_ASSERT(std::filesystem::exists(parent),
                      L"Le dossier parent de « projects_folder» : « " + parent.wstring() + L" » n'existe pas.",
                      std::invalid_argument);

            std::error_code code;
            MA_ASSERT(std::filesystem::create_directory(projects_folder, code),
                      L"Impossible de créer le dossier « " + projects_folder.wstring() +
                      L" ». Le Message d'erreur est : « " + ma::to_wstring(code.message()) + L"»."s,
                      std::runtime_error);
        }
    }

    std::filesystem::path ConfigProject::GetNewProjectPath(const std::filesystem::path& projects_folder_path)
    {
        MA_ASSERT(std::filesystem::exists(projects_folder_path),
                  L"Le dossier « projects_folder» : « " + projects_folder_path.wstring() + L" » n'existe pas.",
                  std::invalid_argument);

        auto project_index = 0u;
        auto path = projects_folder_path / (L"project_" + ma::toString(project_index));

        while(std::filesystem::exists(path) && !std::filesystem::is_empty(path))
        {
            project_index++;
            path = projects_folder_path / (L"project_" + ma::toString(project_index));
        }

        return path;
    }

    void ConfigProject::EndConstruction()
    {
        Item::EndConstruction();
        SetSavePath(m_Path->toString());
    }

    void ConfigProject::LoadVars()
    {
        ma::ConfigGroup::LoadVars();
        SetSavePath(m_Path->toString());
    }

    std::wstring ConfigProject::GetRootFolderSaveName() const
    {
        return this->ma::Item::GetFolderSaveName(*ma::root());
    }

    void ConfigProject::SetSavePath(const std::filesystem::path &project_path)
    {
        if(!std::filesystem::exists(project_path))
        {
            std::error_code code;
            MA_ASSERT(std::filesystem::create_directory(project_path, code),
                      L"Impossible de créer le dossier « " + project_path.wstring() + L" »."
                      L"Le Message d'erreur est : « " + ma::to_wstring(code.message()) + L"»."s);
        }

        ma::root()->SetSavePath(project_path);
    }

    // AppRoot //———————————————————————————————————————————————————————————————————————————————————————————————————————
    std::filesystem::path AppRoot::GetConfigAppFileNameForRootCreation()
    {
        const auto config_gui_path = GetConfigGuiFileName(true);
        auto config_gui = ma::Config(config_gui_path);

        if(std::filesystem::exists(config_gui_path))
            config_gui.Load();

        const std::wstring app_guid = GetAppGUID(config_gui);

        // GetAppGUID met à jour config_gui. Pour que la suite du processus est le même GUID et pour éviter de refaire le
        // même travail, nous sauvegardons ce résultat.
        config_gui.Save();

        return GetConfigAppFileName(app_guid);
    }

    AppRoot::AppRoot(const ma::Item::ConstructorParameters &params):
    ma::Root::Root(params, GetConfigAppFileNameForRootCreation()),// GetConfigAppFileName()), // .ma/gui/apps/{GUID}/config.cfg
    m_ConfigGui(CreateConfigGui()), // Doit être appelé avant GetAppGUID() et donc de GetConfigAppFileName() // utiliser les chemins en paramètre ( GetConfigGuiPath et en locale créer un config)
    m_AppGuid(AddReadOnlyMemberVar<ma::StringVar>(L"m_AppGuid", GetAppGUID(*m_ConfigGui)))
    {
        // 1. Génération des dossiers

        // .ma/gui
        const auto gui_config_folder_path = GetConfigGuiFileName().parent_path();

        // .ma/gui/projects
        const auto projects_folder_path = gui_config_folder_path / L"projects";
        if(!std::filesystem::exists(projects_folder_path))
        {
            std::error_code code;
            MA_ASSERT(std::filesystem::create_directory(projects_folder_path, code),
                      L"Impossible de créer le dossier « " + projects_folder_path.wstring() + L" »." +
                      L"Le Message d'erreur est : « " + ma::to_wstring(code.message()) + L" ».");
        }

        InitRTTI();

        m_Project = CreateItem<ConfigProject>({
                                                Key::GetConfig(),
                                                false,
                                                false,
                                                ConfigProject::GetConfigProjectKey()
                                              },
                                              projects_folder_path);

        m_AppGuid->InsertIInfoKeyValue(
            ma::Var::Key::IInfo::ms_Doc,
            L"L'identifiant de l'application courante."
            L"Elle permet d'associer un fichier de configuration à l'application."
            L"\n"
            L"Exemple: \n"
            L"%user%/.ma/gui/apps/b1028725-aed7-47b0-bd8a-89f06c1cb336/config.cfg \n"
            L"A noter que les applications graphiques de Ma sont référencées dans un fichier de configuration. \n"
            L"Exemple: \n"
            L"%user%/.ma/gui/");
    }

    AppRoot::~AppRoot() = default;

    void AppRoot::EndConstruction()
    {
        Root::EndConstruction();

        m_Project->LoadConfig();
        m_Project->EndConstruction();
    }

    std::wstring AppRoot::GetAppGUID(ma::Config& gui_config)
    {
        // Préfixe des groupes définissant la configuration d'une application.
        const std::wstring prefix = L"app_";

        // Chemin de l'application courante.
        const std::filesystem::path current_app_path = wxStandardPaths::Get().GetExecutablePath().ToStdWstring();

        // Guid de l'application courante.
        auto current_app_guid = L""s;

        // Index de l'application courante.
        unsigned int current_app_index;

        MA_ASSERT(std::filesystem::exists(current_app_path),
                  L"Le chemin de cette application renvoyée par wxWidgets, n'existe pas.",
                  std::runtime_error);

        // key......... : Index de l'application obtenu à partir du nom du groupe : app_{index}.
        // value.first. : Guid de l'application.
        // value.second : Chemin de l'application.
        std::unordered_map<unsigned int, std::pair<std::wstring, std::filesystem::path>> apps;

        // Remplissage de la variable « apps ».
        for(const auto &group_it : gui_config.m_Datas)
        {
            const auto group_name = group_it.first;
    #ifdef __cpp_lib_starts_ends_with
            // c++20
            if(group_name.starts_with(prefix))
    #else
            // c++17
            if(group_name.size() > prefix.size() && group_name.substr(0, prefix.size()) == prefix)
    #endif
            {
                const auto &guid_it_find = group_it.second.find(L"guid");
                if(guid_it_find != group_it.second.end())
                {
                    const auto app_guid = guid_it_find->second;
                    const auto app_index = ma::FromString<unsigned int>(group_name.substr(prefix.size()));
                    std::filesystem::path app_path;

                    if(!app_guid.empty())
                    {
                        const auto &path_it_find = group_it.second.find(L"path");
                        if(path_it_find != group_it.second.end())
                        {
                            app_path = path_it_find->second;

                            if(app_path == current_app_path)
                            {
                                current_app_index = app_index;
                                current_app_guid = app_guid;
                            }
                        }
                    }

                    apps[app_index] = {app_guid, app_path};
                }
            }
        }

        std::wstring id_str;

        // Si le fichier de configuration ne contient pas le guid de l'application courante.
        if(current_app_guid.empty())
        {
            const auto id = ma::Id();

            // Nous en profitons pour mettre à jour le fichier de configuration.
            current_app_index = 0;
            while(apps.find(current_app_index) != apps.end())
                current_app_index++;

            const auto app_group = prefix + ma::toString(current_app_index);

            id_str = id.GetKey();

            gui_config.m_Datas[app_group][L"path"] = current_app_path.wstring();
            gui_config.m_Datas[app_group][L"guid"] = id_str;
        }
        else
        // Sinon c'est que le fichier de configuration contient le guid de l'application courante.
        {
            id_str = ma::Id(current_app_guid).GetKey();
        }

        return id_str;
    }

    ma::Config* AppRoot::CreateConfigGui()
    {
        const auto config_file_path = GetConfigGuiFileName(true);

        auto *config = new ma::Config(config_file_path);

        if(std::filesystem::exists(config->m_FilePath))
            config->Load();

        // Juste pour être sûr que le fichier de configuration, soit enregistré.
        config->Save();

        return config;
    }

    void AppRoot::InitRTTI()
    {
        ma::Item::CreateParameters params{m_RttiManager->GetKey()};
        RTTI::Setup(params);
    }

    std::filesystem::path AppRoot::GetConfigGuiFileName(bool create_parents_folders)
    {
        const auto user_config_dir = std::filesystem::path(wxStandardPaths::Get().GetUserConfigDir().wc_str());
        const auto ma_dir = user_config_dir / L".ma";
        const auto gui_dir = ma_dir / L"gui";
        auto gui_config_file_path = gui_dir / L"config.cfg";

        if(create_parents_folders)
        {
            if(!std::filesystem::exists(ma_dir))
            {
                std::error_code code;
                MA_ASSERT(std::filesystem::create_directory(ma_dir, code),
                          L"Impossible de créer le dossier « " + ma_dir.wstring() +
                          L" ». Le Message d'erreur est : « " + ma::to_wstring(code.message()) + L" ».");
            }

            if(!std::filesystem::exists(gui_dir))
            {
                std::error_code code;
                MA_ASSERT(std::filesystem::create_directory(gui_dir, code),
                          L"Impossible de créer le dossier « " + gui_dir.wstring() +
                          L" ». Le Message d'erreur est : « " + ma::to_wstring(code.message()) + L"».");
            }
        }

        return gui_config_file_path;
    }

    std::filesystem::path AppRoot::GetConfigAppFileName(const std::wstring& app_guid)
    {
        MA_ASSERT(!app_guid.empty(),
                  L"La variable app_guid est vide.",
                  std::invalid_argument);

        // Exemple:
        // .ma/gui/apps/e0ccf043-d254-4c5c-a317-ae0022b9502c/config.cfg

        // .ma/gui
        const auto gui_config_directory_path = GetConfigGuiFileName(true).parent_path();

        // .ma/gui/apps
        const auto apps_config_directory_path = gui_config_directory_path / L"apps";

        // .ma/gui/apps/{app_guid}
        const auto app_config_directory_path = apps_config_directory_path / app_guid;

        // .ma/gui/apps/{app_guid}/config.cfg
        auto app_config_file_path = app_config_directory_path / L"config.cfg";

        if(!std::filesystem::exists(apps_config_directory_path))
        {
            std::error_code code;
            MA_ASSERT(std::filesystem::create_directory(apps_config_directory_path, code),
                      L"Impossible de créer le dossier « " + apps_config_directory_path.wstring() +L" ».\n"
                      L"Le Message d'erreur est : « " + ma::to_wstring(code.message()) + L"».");
        }

        if(!std::filesystem::exists(app_config_directory_path))
        {
            std::error_code code;
            MA_ASSERT(std::filesystem::create_directory(app_config_directory_path, code),
                      L"Impossible de créer le dossier « " + app_config_directory_path.wstring() + L" ».\n"
                      L"Le Message d'erreur est : « " + ma::to_wstring(code.message()) + L"».");
        }

        return app_config_file_path;
    }

    void AppRoot::Load()
    {
        auto root = ma::root();

        root->BeginBatch();
        auto items_errors = root->LoadItems();

        root->LoadVars();
        root->EndBatch();

        for(auto && [key, error] : items_errors)
        {
            MA_WARNING(false,
                       L"L'élément ayant pour clef: « " + key + L"» n'a peu être chargé.\n"
                       L"En voici les raisons:\n" + error);
        }

    }

    void AppRoot::Save()
    {
        // todo Faire une observation des changements de valeur de la variable m_SavePath.
        m_Project->m_Path->fromString(std::filesystem::path(m_SavePath->toString()).parent_path().wstring());

        ma::Root::Save();
        m_ConfigGui->Save();
    }

    // App //————————————————————————————————————————————————————————————————————————————————————————————————————————
    bool App::OnCmdLineParsed(wxCmdLineParser &parser)
    {
        return true;

        // todo cette fonction est appelée alors m_Root n'est pas alloué.
        // todo revoir le mécanisme pour retrouver le chemin de l'environnement python: faire une cible de construction
        //      avec CMake qui construira un venv dans le dossier de construction.
        /*
        if(!m_Root->m_ConfigGui || !m_Root->GetConfigPythonEnvPath(m_Root->m_PythonEnvPath))
        {
            wxString python_env_path;
            if(!parser.Found("e", &python_env_path) && !parser.Found("env", &python_env_path))
            {
                if(!wxGetEnv("PYTHONENV_PATH", &python_env_path))
                {
    #ifdef MA_PYTHONENV_PATH
                    python_env_path = MA_PYTHONENV_PATH;
    #else
                    python_env_path = "";
                    std::wcout << L"App::OnCmdLineParsed "
                               << L"L'application a été lancée sans paramètre pour définir "
                                  L"m_PythonEnvPath. La valeur sera donc soit celle définie "
                                  L"dans le fichier de configuration: «"
                               << GetConfigFileName().GetFullPath() << L"»." << std::endl;
    #endif
                }
                else
                {
                    std::wcout << L"m_PythonEnvPath: " << m_Root->m_PythonEnvPath << std::endl;
                }
            }

            m_Root->m_PythonEnvPath = python_env_path.ToStdWstring();
        }

        return true;
        */
    }

    int App::OnExit()
    {
        // Suppression du nœud racine.
        ma::Item::EraseRoot();

        // Le compteur de référence de m_Root devrait passer à zéro et ainsi, on peut voir la destruction du root s'opérer
        // ici.
        m_Root.reset();

        // Pour éviter certains plantages avec la journalisation.
        // ne sert à rien ici ou ne fait rien. Devrait être dans le wxEntry de la fonction « main ».
        //    wxLog::FlushActive();
        //    wxLog::EnableLogging(false);
        //    wxLogNull no_log;

        return 0;
    }

    bool App::OnInit()
    {
        wxApp::OnInit();

        wxHandleFatalExceptions(true);

        // À effectuer avant la lecture de la première lecture des fichiers.
        ma::SetGlobalLocal();

        m_Root = ma::Item::CreateRoot<AppRoot>();

        const auto frame_name = _(L"Éditeur de Ma");
        auto *frame = new ma::Editor::MainFrame(nullptr, frame_name, wxDefaultPosition, wxSize(700, 600));

        m_Root->m_wxGuiAccess->SetMotherFrame(frame);

        frame->Show();

        const auto save_path = std::filesystem::path(m_Root->m_SavePath->toString());
        if(std::filesystem::exists(save_path) && !std::filesystem::is_empty(save_path))
            frame->Load();

        // Évite que chaque lancement de nouvelle application pointe sur un même dossier vide.
        // Voir InitConfigApp() la partie du code qui cherche à attribuer un dossier de projet vide ou nouveau.
        // Exemple :
        // /home/toto/.ma/gui/projects/project_0/
        if(!std::filesystem::exists(m_Root->m_SavePath->toString()))
            m_Root->Save();

        return true;
    }

    void App::OnInitCmdLine(wxCmdLineParser &parser)
    {
        parser.SetDesc(g_cmdLineDesc);
        // must refuse '/' as parameter starter or cannot use "/path" style paths
        parser.SetSwitchChars("-");
    }

    // https://github.com/wxWidgets/wxWidgets/blob/master/samples/fswatcher/fswatcher.cpp#L293
    void App::OnEventLoopEnter(wxEventLoopBase *loop)
    {
//        auto* frame = dynamic_cast<MainFrame*>(m_Root->m_wxGuiAccess->GetMotherFrame());
//        if( frame && frame->CreateFileWatcherIfNecessary())
//        {}
    }
}