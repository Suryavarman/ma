/// \file Views.cpp
/// \author Pierre Pontier
/// \date 2023-11-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <memory>
#include <utility>

#include "../include/Views.h"

namespace ma::Editor
{
    // MenuItem //————————————————————————————————————————————————————————————————————————————————————————————————————
    MenuItem::MenuItem(wxMenu *parentMenu,
                       ma::wx::ViewPtr view,
                       int id,
                       const wxString& text,
                       const wxString& help,
                       wxItemKind kind,
                       wxMenu *subMenu):
    ::wxMenuItem::wxMenuItem(parentMenu, id, text, help, kind, subMenu),
    m_View{std::move(view)}
    {
        MA_ASSERT(m_View, L"« m_View » est nul.", std::invalid_argument);
    }

    MenuItem::~MenuItem() = default;


    // ViewsMenu //—————————————————————————————————————————————————————————————————————————————————————————————————————
    ViewsMenu::ViewsMenu(const wxString& title, long style):
    wxMenu(title, style),
    ma::Observer(),
    m_RTTI{ma::Item::GetItem<ma::RTTI>(ma::Item::Key::GetRTTI())},
    m_Views{ma::Item::GetItem<ma::wx::Views>(ma::wx::Views::Key::Get())},
    m_Menu3D{nullptr},
    m_MenuItem3D{nullptr},
    m_MenuOthers{nullptr},
    m_MenuItemOthers{nullptr}
    {
        MA_ASSERT(m_RTTI, L"m_RTTI est nul", std::logic_error);
        MA_ASSERT(m_Views, L"m_Views est nul", std::logic_error);

        InitMenus();

        auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
        /*
        obs_manager->AddObservation<ma::wxObserverVar<ViewsMenu>, ma::ItemVar>(this, m_RTTI);
        obs_manager->AddObservation<ma::wxObserverVar<ViewsMenu>, ma::ItemVar>(this, m_Views);
        /*/ // Pour les tests
        obs_manager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, m_RTTI);
        obs_manager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, m_Views);
        //*/
    }

    ViewsMenu::ViewsMenu(long style):
    wxMenu(style),
    ma::Observer(),
    m_RTTI{ma::Item::GetItem<ma::RTTI>(ma::Item::Key::GetRTTI())},
    m_Views{ma::Item::GetItem<ma::wx::Views>(ma::wx::Views::Key::Get())},
    m_Menu3D{nullptr},
    m_MenuItem3D{nullptr},
    m_MenuOthers{nullptr},
    m_MenuItemOthers{nullptr}
    {
        MA_ASSERT(m_RTTI, L"m_RTTI est nul", std::logic_error);
        MA_ASSERT(m_Views, L"m_Views est nul", std::logic_error);

        InitMenus();

        auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
        /*
        obs_manager->AddObservation<ma::wxObserverVar<ViewsMenu>, ma::ItemVar>(this, m_RTTI);
        obs_manager->AddObservation<ma::wxObserverVar<ViewsMenu>, ma::ItemVar>(this, m_Views);
        /*/ // Pour les tests
        obs_manager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, m_RTTI);
        obs_manager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, m_Views);
        //*/
    }

    void ViewsMenu::InitMenus()
    {
        m_Menu3D = new wxMenu();
        m_MenuItem3D = new ::wxMenuItem(this,
                                        wxID_ANY,
                                        _(L"Vues 3D"),
                                        _(L"Affiche les contextes 3D permettant ainsi de créer une vue 3D."),
                                        wxITEM_NORMAL,
                                        m_Menu3D);
        Append(m_MenuItem3D);
        m_MenuItem3D->Enable(false);

        m_MenuOthers = new wxMenu();
        m_MenuItemOthers = new ::wxMenuItem(this,
                                            wxID_ANY,
                                            _(L"NoteBooks"),
                                            _(L"Permet de gérer les différentes vue de l'éditeur."),
                                            wxITEM_NORMAL,
                                            m_MenuOthers);
        Append(m_MenuItemOthers);
        m_MenuItemOthers->Enable(false);
    }

    ViewsMenu::~ViewsMenu() = default;

    void ViewsMenu::UpdateRTTIObservation()
    {
        auto makers = m_RTTI->GetRegisteredMakerItems(ma::Engine::Context::GetEngineContextClassName());
        std::set<std::wstring> menu_items_labels;

        for(auto* menu_item : m_Menu3D->GetMenuItems())
        {
            const auto class_name = menu_item->GetItemLabel().ToStdWstring();
            if(makers.find(class_name) == makers.end())
                Destroy(menu_item);
            else
                menu_items_labels.insert(class_name);
        }

        for(auto &&[class_name, maker] : makers)
        {
            if(menu_items_labels.find(class_name) == menu_items_labels.end())
                AddView3D(class_name, L"Ouvrir une fenêtre pour rendre une scène avec le rendu " + class_name);
        }
    }

    void ViewsMenu::UpdateViewsObservation()
    {
        auto views = m_Views->GetViews();

        std::set<std::wstring, ma::wx::ViewPtr> views_3d, views_others;
        std::set<std::wstring> menu_items_labels;

        for(auto* menu_item : m_MenuOthers->GetMenuItems())
        {
            const auto class_name = menu_item->GetItemLabel().ToStdWstring();
            auto test_class_name = [&](const ma::wx::ViewPtr &view){ return view->GetTypeInfo().m_ClassName == class_name;};
            if(std::find_if(views.begin(), views.end(), test_class_name) == views.end())
                Destroy(menu_item);
            else
                menu_items_labels.insert(class_name);
        }

        for(auto& view : views)
        {
            std::wstring doc;
            if(auto opt = view->HasVarOpt<StringVar>(ma::Item::Key::Var::GetDoc()))
                doc = opt.value()->toString();

            const auto class_name = view->GetTypeInfo().m_ClassName;
            if(menu_items_labels.find(class_name) == menu_items_labels.end())
                AddView(view, doc);
        }
    }

    void ViewsMenu::AddView(const ma::wx::ViewPtr& view, const std::wstring &description)
    {
        const auto class_name = view->GetTypeInfo().m_ClassName;
        const auto id = static_cast<int>(wxNewId());
        Bind(wxEVT_MENU, &ViewsMenu::OnCreateView, this, id);
        m_MenuItemOthers->Enable(true);
        m_MenuOthers->Append(new MenuItem(this, view, id, class_name, description));

        auto obs_manager = ma::Item::GetItem<ma::ObservationManager>(ma::Item::Key::GetObservationManager());
        obs_manager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, view);
    }

    void ViewsMenu::AddView3D(const std::wstring &class_name, const std::wstring &description)
    {
        const auto id = static_cast<int>(wxNewId());
        Bind(wxEVT_MENU, &ViewsMenu::OnCreateView3D, this, id);
        m_MenuItem3D->Enable(true);
        m_Menu3D->Append(id, class_name, description);
    }

    MenuItem* ViewsMenu::GetMenuItem(int id, wxMenu **menu) const
    {
        auto* item = m_MenuOthers->FindItem(id, menu);
        if(!item)
            item = m_Menu3D->FindItem(id, menu);

        MA_ASSERT(item,
                  L"Aucun élément du menu correspond à cet identifiant « " + std::to_wstring(id) + L" ».",
                  std::invalid_argument);

        auto* result = dynamic_cast<MenuItem*>(item);
        MA_ASSERT(result,
                  L"L'élément du menu correspondant à cet identifiant « " + std::to_wstring(id) + L" » n'est pas un "
                  L"« ma::Editor::MenuItem ».",
                  std::invalid_argument);

        return result;
    }

    void ViewsMenu::OnCreateView3D(wxCommandEvent &event)
    {
        auto label = m_Menu3D->GetLabel(event.GetId());
        MA_ASSERT(!label.empty(),
                  L"L'intitulé de l'élément du menu des Vues est vide.",
                  std::invalid_argument);

        const auto context_manager_key = ma::Item::Key::GetContextManager();

        auto context = std::dynamic_pointer_cast<ma::Engine::Context>(m_RTTI->CreateItem(label.ToStdWstring(), {context_manager_key}));
        MA_ASSERT(context,
                  L"L'item n'est pas du type « ma::Engine::Context ».",
                  std::logic_error);

        ma::Engine::BuildContextLink<ma::Engine::Context>(context);

        auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

        MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

        auto *center_book = gui->GetCenterNoteBook();
        const auto page_index = center_book->GetPageIndex(context->GetView());
        center_book->SetSelection(page_index);
    }

     wxAuiNotebook *ViewsMenu::GetBook(const ma::wx::ViewPtr& view)
     {
        auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

        MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

        auto tags = view->GetTags();

        // Par défaut, nous ajouterons la vue au centre.
        auto *book = gui->GetCenterNoteBook();

        if(tags.count(ma::wx::Bottom::GetwxBottomClassName()))
            book = gui->GetBottomNotebook();
        else if(tags.count(ma::wx::Top::GetwxTopClassName()))
            book = gui->GetCenterNoteBook();
        else if(tags.count(ma::wx::Left::GetwxLeftClassName()))
            book = gui->GetLeftNoteBook();
        else if(tags.count(ma::wx::Right::GetwxRightClassName()))
            book = gui->GetRightNoteBook();
        else if(tags.count(ma::wx::Center::GetwxCenterClassName()))
            book = gui->GetCenterNoteBook();
        else if(tags.count(ma::wx::X::GetwxXClassName()))
            book = gui->GetBottomNotebook();
        else if(tags.count(ma::wx::Y::GetwxYClassName()))
            book = gui->GetLeftNoteBook();
        else if(tags.count(ma::wx::XY::GetwxXYClassName()))
            book = gui->GetCenterNoteBook();
        else if(tags.count(ma::wx::Text::GetwxTextClassName()))
            book = gui->GetCenterNoteBook();
        else if(tags.count(ma::wx::Widgets::GetwxWidgetsClassName()))
            book = gui->GetCenterNoteBook();
        else if(tags.count(ma::wx::RealTime::GetwxRealTimeClassName()))
            book = gui->GetCenterNoteBook();
        else if(tags.count(ma::wx::Precompute::GetwxPrecomputeClassName()))
            book = gui->GetCenterNoteBook();

        return book;
     }

    void ViewsMenu::AddPage(const ma::wx::ViewPtr& view, wxAuiNotebook* book, int menu_id)
    {
        MA_ASSERT(view, L"« view » est nul.", std::invalid_argument);
        MA_ASSERT(book, L"« book » est nul.", std::invalid_argument);

        auto* window = view->GetView();

        std::wstring label;
        if(auto opt = view->HasVarOpt(ma::Item::Key::Var::GetName()))
            label = opt.value()->toString();
        else
            label = view->GetTypeInfo().m_ClassName;

        book->AddPage(window, label);

        book->Bind(wxEVT_AUINOTEBOOK_PAGE_CLOSE, [=](wxAuiNotebookEvent &event)
        {
            view->ViewDestroyed();

            if(menu_id > -1)
            {
                MenuItem* menu_item = GetMenuItem(menu_id);
                menu_item->Check(false);
            }
        });
    }

    void ViewsMenu::OnCreateView(wxCommandEvent &event)
    {
        const auto id = event.GetId();
        MenuItem* menu_item = GetMenuItem(id);

        MA_ASSERT(menu_item, L"« menu_item » est nul", std::logic_error);
        auto has_to_be_create = menu_item->IsChecked();
        auto view = menu_item->m_View;

        if(has_to_be_create)
        {
            if(!view->GetView())
            {
                auto* book = ViewsMenu::GetBook(view);
                auto* window = view->CreateView(book);

                MA_ASSERT(book, L"« book » est nul", std::logic_error);
                MA_ASSERT(window, L"« window » est nul", std::logic_error);

                AddPage(view, book, id);
            }
        }
        else
        {
            auto *window = view->GetView();
            if(window)
            {
                auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());
                MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);
                wxAuiNotebook* book = nullptr;

                auto books = {gui->GetCenterNoteBook(),
                              gui->GetBottomNotebook(),
                              gui->GetLeftNoteBook(),
                              gui->GetRightNoteBook()};

                for(auto it = books.begin(); it != books.end() && !book; ++it)
                {
                    book = *it;
                    MA_ASSERT(book, L"« book » est nul", std::logic_error);

                    auto page_index = book->FindPage(window);
                    if(page_index == wxNOT_FOUND)
                        book = nullptr;
                    else
                    {
                        book->DeletePage(page_index);
                        view->ViewDestroyed();
                    }
                }
            }
        }
    }

    void ViewsMenu::BeginObservation(const ObservablePtr &observable)
    {
        if(observable == m_RTTI)
            UpdateRTTIObservation();
        else if(observable == m_Views)
            UpdateViewsObservation();
    }

    void ViewsMenu::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        if(observable == m_RTTI)
        {
            if(data.HasActions(ma::Item::Key::Obs::ms_ConstructionEnd))
            {
                UpdateRTTIObservation();
            }
            else if(data.HasActions(ma::Item::Key::Obs::ms_RemoveItemEnd))
            {
                UpdateRTTIObservation();
            }
        }
        else if(observable == m_Views)
        {
            if(data.HasActions(ma::Item::Key::Obs::ms_AddItemEnd))
            {
                std::wcout << L"ma::Item::Key::Obs::ms_AddItemEnd" << std::endl;
            }
            else if(data.HasActions(ma::Item::Key::Obs::ms_ConstructionEnd))
            {
                UpdateViewsObservation();
            }
            else if(data.HasActions(ma::Item::Key::Obs::ms_RemoveItemEnd))
            {
                UpdateViewsObservation();
            }
        }
        else if(ma::ClassInfoManager::IsInherit(observable, ma::wx::View::GetwxViewClassName()))
        {
            if(data.HasActions(ma::Item::Key::Obs::ms_ConstructionEnd) ||
               data.HasActions(ma::Item::Key::Obs::ms_RemoveItemEnd))
            {
                auto view = std::dynamic_pointer_cast<ma::wx::View>(observable);
                MA_ASSERT(view, L"« view » est nul", std::invalid_argument);

                auto* window = view->GetView();

                if(window)
                {
                    // Trouvons la page parmi les feuillets et retirons la page sans détruire la fenêtre de la vue.
                    auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());
                    MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);
                    wxAuiNotebook* book = nullptr;

                    auto books = {gui->GetCenterNoteBook(),
                                  gui->GetBottomNotebook(),
                                  gui->GetLeftNoteBook(),
                                  gui->GetRightNoteBook()};

                    for(auto it = books.begin(); it != books.end() && !book; ++it)
                    {
                        book = *it;
                        MA_ASSERT(book, L"« book » est nul", std::logic_error);

                        auto page_index = book->FindPage(window);
                        if(page_index == wxNOT_FOUND)
                            book = nullptr;
                        else
                            book->RemovePage(page_index);
                    }

                    AddPage(view, book, -1);
                }
            }
        }
    }

    void ViewsMenu::EndObservation(const ObservablePtr &observable)
    {
        // todo Supprimer les menus.
        if(observable == m_RTTI)
        {
            for(auto menu_item : m_Menu3D->GetMenuItems())
                m_Menu3D->Destroy(menu_item);
        }
        else if(observable == m_Views)
        {
            for(auto menu_item : m_MenuOthers->GetMenuItems())
                m_Menu3D->Destroy(menu_item);
        }
    }

    const TypeInfo& ViewsMenu::GetEditorViewsMenuTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetEditorViewsMenuClassName(),
            L"Le contexte pour afficher le rendu de Cycles dans une texture.",
            {}
        };
        // clang-format on

        return info;
    }

    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ViewsMenu, Editor, Observer)
} // namespace ma::Editor




