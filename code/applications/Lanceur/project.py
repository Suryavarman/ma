#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#

import glob
import os

import wx
import datetime

from app import *
import language

_ = language._


class Project:
    ms_DefaultName = _(u"Nom du projet")

    def __init__(self):
        self.m_Name = self.ms_DefaultName
        self.m_EditorIndex = 0
        self.m_DirectoryPath = u""
        self.m_LastModificationDate = datetime.datetime(2023, 1, 1, 00, 00, 00, 0)
        self.m_LastModificationDateStr = self.m_LastModificationDate.strftime("%m/%d/%Y, %H:%M:%S")

    def set_editor(self, index: int):
        assert index >= 0, _(u"L'index de l'éditeur est négatif.")
        self.m_EditorIndex = index

    def set_directory(self, path: str):
        assert os.path.exists(path), _(f"Ce chemin « {path} » n'existe pas.")
        assert os.path.isdir(path), _(f"Ce chemin « {path} » ne représente pas un répertoire.")
        self.m_DirectoryPath = path
        self.update_modification_date()

        if len(self.m_Name) == self.ms_DefaultName:
            self.m_Name = os.path.basename(self.m_DirectoryPath)

    def update_modification_date(self):
        paths = glob.glob(os.path.join(self.m_DirectoryPath, "*"))
        self.m_LastModificationDate = datetime.datetime.fromtimestamp(os.path.getctime(self.m_DirectoryPath))

        for path in paths:
            path_date = datetime.datetime.fromtimestamp(os.path.getctime(path))
            if self.m_LastModificationDate < path_date:
                self.m_LastModificationDate = path_date

        self.m_LastModificationDateStr = self.m_LastModificationDate.strftime("%m/%d/%Y, %H:%M:%S")

    def get_datas(self) -> dict[str, str]:
        """ Défini la structure qui sauvegardera les données du projet. """
        return {"name": self.m_Name,
                "editor_index": self.m_EditorIndex,
                "path": self.m_DirectoryPath}

    def set_datas(self, datas: dict[str, str]):
        """ Charge les données de la structure de données data."""
        self.m_Name = datas["name"]
        self.m_EditorIndex = int(datas["editor_index"])
        self.m_DirectoryPath = datas["path"]


class ProjectGui(wx.Panel):

    def __init__(self, project: Project, editor_names: list[str], parent, window_id=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=wx.Size(670, 60),
                 style=wx.TAB_TRAVERSAL,
                 name=wx.EmptyString):
        """
        Interface pour définir le point d'entrée d'un projet.
        :param project: Le projet associé. Attention le répertoire du projet doit déjà être défini.
        :param editor_names: La liste des noms des éditeurs.
        :param parent: La fenêtre parente.
        :param window_id: L'identifiant de l'interface.
        :param pos: La position
        :param size: La taille
        :param style: Le style
        :param name: Le nom de l'interface
        """
        wx.Panel.__init__(self, parent, id=window_id, pos=pos, size=size, style=style, name=name)

        """ Le projet associé. """
        self.m_Project = project

        remove_bitmap_path = os.path.join(g_ma_image_directory, u"supprimer_24x24.png")

        sizer = wx.BoxSizer(wx.VERTICAL)

        sizer_top = wx.BoxSizer(wx.HORIZONTAL)

        sizer_left = wx.BoxSizer(wx.HORIZONTAL)

        self.m_NameTextCtrl = wx.TextCtrl(self, wx.ID_ANY, self.m_Project.m_Name, wx.DefaultPosition, wx.Size(-1, 35),
                                          0)
        self.m_NameTextCtrl.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
                    False, wx.EmptyString))
        self.m_NameTextCtrl.SetToolTip(_(u"Saisissez le nom du projet."))

        sizer_left.Add(self.m_NameTextCtrl, 1, wx.ALL | wx.EXPAND, 2)

        sizer_top.Add(sizer_left, 1, wx.EXPAND, 2)

        sizer_right = wx.BoxSizer(wx.HORIZONTAL)

        self.m_EditorChoice = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(200, 35), editor_names, 0)
        self.m_EditorChoice.SetSelection(self.m_Project.m_EditorIndex)
        self.m_EditorChoice.SetFont(
            wx.Font(10, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString))
        self.m_EditorChoice.SetToolTip(_(u"Nom de l'éditeur associé."))

        sizer_right.Add(self.m_EditorChoice, 1, wx.ALL, 2)

        self.m_OpenExplorerBitmapButton = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition,
                                                          wx.Size(40, 35), wx.BU_AUTODRAW | 0)

        self.m_OpenExplorerBitmapButton.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_GO_HOME, wx.ART_BUTTON))
        sizer_right.Add(self.m_OpenExplorerBitmapButton, 0, wx.ALL, 2)

        self.m_RemoveBitmapButton = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size(40, 35),
                                                    wx.BU_AUTODRAW | 0)

        self.m_RemoveBitmapButton.SetBitmap(wx.Bitmap(remove_bitmap_path, wx.BITMAP_TYPE_ANY))
        self.m_RemoveBitmapButton.SetToolTip(_(u"Retirer le projet de la liste."))

        sizer_right.Add(self.m_RemoveBitmapButton, 0, wx.ALL, 2)

        self.m_Run = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size(40, 35),
                                     wx.BU_AUTODRAW | 0)

        self.m_Run.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_EXECUTABLE_FILE, wx.ART_MENU))
        self.m_Run.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
                    False, wx.EmptyString))
        self.m_Run.SetToolTip(_(u"Ouvre l'éditeur Ma avec ce projet."))

        sizer_right.Add(self.m_Run, 0, wx.ALL, 2)

        sizer_top.Add(sizer_right, 0, wx.EXPAND, 2)

        sizer.Add(sizer_top, 0, wx.EXPAND, 5)

        sizer_footer = wx.BoxSizer(wx.HORIZONTAL)

        self.m_PathStaticText = wx.StaticText(self, wx.ID_ANY,
                                              self.m_Project.m_DirectoryPath,
                                              wx.DefaultPosition, wx.Size(-1, -1), wx.ALIGN_LEFT)
        self.m_PathStaticText.Wrap(-1)

        self.m_PathStaticText.SetFont(
            wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString))
        self.m_PathStaticText.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_GRAYTEXT))

        sizer_footer.Add(self.m_PathStaticText, 0, wx.ALL, 2)

        sizer_footer.Add((0, 0), 1, wx.EXPAND, 2)

        self.m_DateStaticText = wx.StaticText(self, wx.ID_ANY, self.m_Project.m_LastModificationDateStr,
                                              wx.DefaultPosition, wx.Size(130, -1), wx.ALIGN_CENTER_HORIZONTAL)
        self.m_DateStaticText.Wrap(-1)

        self.m_DateStaticText.SetFont(
            wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString))
        self.m_DateStaticText.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_GRAYTEXT))
        self.m_DateStaticText.SetToolTip(_(u"Date de la dernière modification."))

        sizer_footer.Add(self.m_DateStaticText, 0, wx.ALL, 2)

        self.m_MemorySizeStaticText = wx.StaticText(self, wx.ID_ANY, self.get_memory_size(), wx.DefaultPosition,
                                                    wx.Size(100, -1), wx.ALIGN_CENTER_HORIZONTAL)
        self.m_MemorySizeStaticText.Wrap(-1)

        self.m_MemorySizeStaticText.SetFont(
            wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString))
        self.m_MemorySizeStaticText.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_GRAYTEXT))

        sizer_footer.Add(self.m_MemorySizeStaticText, 0, wx.ALL, 2)

        sizer.Add(sizer_footer, 0, wx.EXPAND, 2)

        self.m_StaticLine = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        sizer.Add(self.m_StaticLine, 0, wx.EXPAND | wx.ALL, 2)

        self.SetSizer(sizer)
        self.Layout()

        # Connect Events
        self.m_NameTextCtrl.Bind(wx.EVT_TEXT, self.on_project_name)
        self.m_EditorChoice.Bind(wx.EVT_CHOICE, self.on_chosen_editor)
        self.m_OpenExplorerBitmapButton.Bind(wx.EVT_BUTTON, self.on_open_os_directory_explorer)

    def __del__(self):
        pass

    # Virtual event handlers, override them in your derived class
    def on_project_name(self, event: wx.CommandEvent):
        event.Skip()
        self.m_Project.m_Name = event.String

    def on_chosen_editor(self, event: wx.CommandEvent):
        event.Skip()
        self.m_Project.set_editor(event.GetInt())

    def on_open_os_directory_explorer(self, event: wx.CommandEvent):
        event.Skip()
        wx.LaunchDefaultApplication(os.path.abspath(self.m_Project.m_DirectoryPath))

    def set_projects_choices(self, editors_name: list[str] = []):
        # self.m_EditorChoice
        pass

    def get_memory_size(self):
        """
        https://pypi.org/project/humanize/
        :return:
        """
        memory_size = 0  # bytes
        if os.path.exists(self.m_Project.m_DirectoryPath):
            # https://stacklima.com/comment-obtenir-la-taille-du-dossier-en-utilisant-python/
            for ele in os.scandir(self.m_Project.m_DirectoryPath):
                if os.path.isfile(ele):
                    memory_size += os.stat(ele).st_size

        # https://stackoverflow.com/questions/40783029/os-stat-st-size-gives-me-incorrect-size-in-python
        # https://convertlive.com/u/convert/bytes/to/megabytes
        if (memory_size / 1e+9) > 0.001:
            memory_size = memory_size / 1e+9
            memory_size = str(memory_size) + " GB"
        else:
            # convert to MB
            memory_size = memory_size / 1e+6
            memory_size = str(memory_size) + " MB"

        # import humanize
        # >>> humanize.naturalsize(size)
        return memory_size

    def refresh_gui(self, editor_names: list[str]):
        """
        Rafraichie l'interface.
        cf: https://forums.wxwidgets.org/viewtopic.php?t=17892
        :param editor_names: La liste ordonnée des noms de chacun des éditeurs.
        :return:
        """

        # Bloque les évènements
        self.Freeze()

        self.m_NameTextCtrl.SetLabelText(self.m_Project.m_Name)
        self.m_DateStaticText.SetLabelText(self.m_Project.m_LastModificationDateStr)

        self.m_EditorChoice.SetItems(editor_names)
        self.m_EditorChoice.SetSelection(self.m_Project.m_EditorIndex)

        # Réactive les évènements
        self.Thaw()

