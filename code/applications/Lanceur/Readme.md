[![License](https://img.shields.io/badge/license-MIT-green)](https://framagit.org/Suryavarman/ma/-/blob/master/LICENSE)

![Lanceur de Ma](images/lanceur_969x256.png)

# Lanceur de Ma
Cette application écrite en python permet d'ouvrir et d'ajouter un projet Ma.

![Gui](images/gui_01.png)

# Feuille de route
## Premier cycle 8/13
- [x] ~~Créer une interface en python avec wxPython.~~
- [x] ~~Gestion des traductions de l'interface dans différentes langues.~~
- [x] ~~Création de la liste des tâches.~~
- [x] ~~L'interface doit permettre le choix d'instances de Ma.~~
- [x] ~~Sauvegarde de la configuration du lanceur.~~
- [x] ~~Afficher une liste des projets gérer avec l'instance de Ma qui lui est liée.~~
- [x] ~~Exécuter un éditeur indépendant du Lanceur.~~
- [ ] Charger un éditeur avec le projet associé.
- [ ] Configuration de Ogre3D (permettre le choix du répertoire des librairies d'Ogre3D).
- [x] ~~Permettre le choix d'une langue.~~
- [ ] Traduire l'application en FR, ES, RU, EN, US.
- [ ] Créer le script d'installation du lanceur de Ma.
- [ ] Créer le script de désinstallation d'un lanceur Ma. 
- [ ] Gestion de la mise à jour du lanceur.

## Deuxième cycle 0/3
- [ ] Le numéro de version doit inclure le numéro du commit.
- [ ] Gestion des mises à jour des éditeurs.
- [ ] Ajouter une version de l'éditeur Ma, depuis internet.





