#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#

import time

import os
import psutil

import language
_ = language.g_language.gettext


def file_is_lock(in_path: str) -> bool:
    """
    Teste si un fichier est utilisé par un autre processus.
    https://sourceforge.net/p/gdeps/mercurial/ci/default/tree/GDeps/gdeps/system.py
    :param in_path: Le chemin à tester
    :return: Vrais si le fichier est utilisé por un autre processus.
    """
    process_list = psutil.Process()

    open_files = os.path.normpath(str(process_list.open_files()))

    return open_files.find(os.path.normpath(in_path)) != -1


def wait_while_file_is_lock(in_path: str, im_waiting_time: int) -> bool:
    """
    Rend la main une fois que le fichier n'est plus utilisé par d'autres ressources ou que le temps soit écoulé.

    https://sourceforge.net/p/gdeps/mercurial/ci/default/tree/GDeps/gdeps/system.py#l394

        :param in_path: Le chemin du fichier à tester.
        :param im_waiting_time: Le temps pendant lequel à chaque seconde l'utilisation du fichier sera testée.
        :return: Vrais si le fichier n'est pas utilisé par un autre processus, sinon faux.
    """
    i = 0

    assert os.path.exists(in_path), _(u"Le fichier n'existe pas.")
    assert os.path.isfile(in_path), _(u"Le chemin ne pointe pas sur un fichier.")

    while i < im_waiting_time and file_is_lock(in_path):
        i += 1
        time.sleep(1)

    return file_is_lock(in_path)
