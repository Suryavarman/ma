#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#

import sys
import language


class Preferences:
    ms_KeyLanguage = "language"
    ms_KeyTerminal = "terminal"

    def __init__(self, in_language: str = "fr"):
        self._Language = ""
        self.Language = in_language

        self._TerminalCMDLinuxChoices = ["xterm -T $TITLE -e",
                                         "gnome-terminal --wait -t $TITLE -x",
                                         "konsole -e",
                                         "xfce4-terminal --disable-server -T $TITLE -x",
                                         "terminology -M -T $TITLE -e"]

        self._TerminalCMDWindowsChoices = [""]

        self._TerminalCMDMacChoices = [""]

        self._Terminal = ""
        self.Terminal = self.TerminalChoices[0]

    def get_datas(self) -> dict[str, str]:
        """ Défini la structure qui sauvegardera les préférences. """
        return {self.ms_KeyLanguage: self.Language,
                self.ms_KeyTerminal: self.Terminal}

    def set_datas(self, datas: dict[str, str]):
        """ Charge les données de la structure de données data."""
        if self.ms_KeyLanguage in datas:
            self.Language = datas[self.ms_KeyLanguage]

        if self.ms_KeyTerminal in datas:
            self.Terminal = datas[self.ms_KeyTerminal]

    @property
    def Language(self):
        return self._Language

    @Language.setter
    def Language(self, value):
        self._Language = value
        language.g_language.set_translation(self._Language)

    @property
    def Terminal(self):
        return self._Terminal

    @Terminal.setter
    def Terminal(self, value):
        self._Terminal = value

    @property
    def TerminalChoices(self) -> list[str]:
        """
        todo Tester la présence de xterm, gnome etc.
        :return:
        """
        if sys.platform.startswith('linux') or sys.platform.startswith('aix'):
            result = self._TerminalCMDLinuxChoices
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            result = self._TerminalCMDWindowsChoices
        elif sys.platform.startswith('darwin'):
            result = self._TerminalCMDMacChoices
        else:
            assert False, "La plateforme «" + sys.platform + "» n'est pas encore gérée."

        return result
