# -*- coding: utf-8 -*-
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#
# Erreurs:
# - window_decorations.css: No such file or directory
# Vous pouvez éditer le fichier  ~/.config/gtk-3.0/gtk.css et retirer la ligne incluant window_decorations.css
# @import 'window_decorations.css';
# https://9to5answer.com/quot-gtk-warning-theme-parsing-error-quot-for-no-longer-installed-extension
# notes:
# Pour trouver des icônes: https://www.onlinewebfonts.com/icon/search?q=preference
# import sys
# import subprocess
#
# import os
# import wx
import wx.xrc

# from app import *
from config import *
from editor import *
from project import *
from preferences import *
from system import *

import language

_ = language._


class LauncherGui(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=_(u"Lanceur de Ma") + f" <{g_version}>",
                           pos=wx.DefaultPosition, size=wx.Size(700, 600), style=wx.DEFAULT_DIALOG_STYLE)

        self.m_BitmapLogo = wx.Bitmap(g_ma_icon_path, wx.BITMAP_TYPE_ANY)
        self.m_Icon = wx.Icon(self.m_BitmapLogo)

        self.SetIcon(self.m_Icon)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        sizer_dialog = wx.BoxSizer(wx.VERTICAL)

        self.m_Notebook = wx.Notebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        notebook_image_size = wx.Size(16, 16)
        notebook_index = 0
        notebook_images = wx.ImageList(notebook_image_size.GetWidth(), notebook_image_size.GetHeight())
        self.m_Notebook.AssignImageList(notebook_images)
        self.m_ProjectsPanel = wx.Panel(self.m_Notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                        wx.TAB_TRAVERSAL)
        sizer_projects = wx.BoxSizer(wx.VERTICAL)

        sizer_projects_menu = wx.BoxSizer(wx.HORIZONTAL)

        sizer_projects_menu.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_AddProjectBitmapButton = wx.BitmapButton(self.m_ProjectsPanel, wx.ID_ANY, wx.NullBitmap,
                                                        wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | 0)

        self.m_AddProjectBitmapButton.SetBitmap(
            wx.Bitmap(os.path.join(g_ma_image_directory, u"ajouter_32x32.png"), wx.BITMAP_TYPE_ANY))
        self.m_AddProjectBitmapButton.SetToolTip(_(u"Ajouter un projet."))

        sizer_projects_menu.Add(self.m_AddProjectBitmapButton, 0, wx.ALL, 5)

        sizer_projects.Add(sizer_projects_menu, 0, wx.EXPAND, 5)

        self.m_ProjectsScrolledWindow = wx.ScrolledWindow(self.m_ProjectsPanel, wx.ID_ANY, wx.DefaultPosition,
                                                          wx.DefaultSize,
                                                          wx.HSCROLL | wx.VSCROLL)
        self.m_ProjectsScrolledWindow.SetScrollRate(5, 5)
        self.m_SizerProjectsScrollWindow = wx.BoxSizer(wx.VERTICAL)

        self.m_ProjectsStaticTextTitre = wx.StaticText(self.m_ProjectsScrolledWindow, wx.ID_ANY,
                                                       _(u"Liste des projets enregistrés"),
                                                       wx.DefaultPosition,
                                                       wx.DefaultSize,
                                                       wx.ALIGN_CENTER_HORIZONTAL)
        self.m_ProjectsStaticTextTitre.Wrap(-1)

        self.m_SizerProjectsScrollWindow.Add(self.m_ProjectsStaticTextTitre, 0, wx.ALL | wx.EXPAND, 5)

        self.m_ProjectsScrolledWindow.SetSizer(self.m_SizerProjectsScrollWindow)
        self.m_ProjectsScrolledWindow.Layout()
        self.m_SizerProjectsScrollWindow.Fit(self.m_ProjectsScrolledWindow)
        sizer_projects.Add(self.m_ProjectsScrolledWindow, 1, wx.EXPAND | wx.ALL, 5)

        self.m_ProjectsPanel.SetSizer(sizer_projects)
        self.m_ProjectsPanel.Layout()
        sizer_projects.Fit(self.m_ProjectsPanel)
        self.m_Notebook.AddPage(self.m_ProjectsPanel, _(u"Projets"), True)
        notebook_bitmap = wx.ArtProvider.GetBitmap(wx.ART_LIST_VIEW, wx.ART_MENU)
        if notebook_bitmap.IsOk():
            notebook_images.Add(notebook_bitmap)
            self.m_Notebook.SetPageImage(notebook_index, notebook_index)
            notebook_index += 1

        self.m_MaAppsPanel = wx.Panel(self.m_Notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        sizer_ma_apps = wx.BoxSizer(wx.VERTICAL)

        sizer_ma_apps_menu = wx.BoxSizer(wx.HORIZONTAL)

        sizer_ma_apps_menu.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_AddBitmapButton = wx.BitmapButton(self.m_MaAppsPanel, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition,
                                                 wx.DefaultSize, wx.BU_AUTODRAW | 0)

        self.m_AddBitmapButton.SetBitmap(wx.Bitmap(os.path.join(g_ma_image_directory, u"ajouter_32x32.png"),
                                                   wx.BITMAP_TYPE_ANY))
        sizer_ma_apps_menu.Add(self.m_AddBitmapButton, 0, wx.ALL, 5)

        sizer_ma_apps.Add(sizer_ma_apps_menu, 0, wx.EXPAND, 5)

        self.m_AppsScrolledWindow = wx.ScrolledWindow(self.m_MaAppsPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                      wx.VSCROLL)
        self.m_AppsScrolledWindow.SetScrollRate(5, 5)
        self.m_SizerEditorsScrollWindow = wx.BoxSizer(wx.VERTICAL)

        self.m_AppStaticTextTitre = wx.StaticText(self.m_AppsScrolledWindow, wx.ID_ANY,
                                                  _(u"Liste des éditeurs Ma enregistrés"),
                                                  wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL)
        self.m_AppStaticTextTitre.Wrap(-1)

        self.m_SizerEditorsScrollWindow.Add(self.m_AppStaticTextTitre, 0, wx.ALL | wx.EXPAND, 5)

        self.m_AppTextStaticLine = wx.StaticLine(self.m_AppsScrolledWindow, wx.ID_ANY, wx.DefaultPosition,
                                                 wx.DefaultSize, wx.LI_HORIZONTAL)

        self.m_SizerEditorsScrollWindow.Add(self.m_AppTextStaticLine, 0, wx.EXPAND | wx.ALL, 5)

        self.m_AppsScrolledWindow.SetSizer(self.m_SizerEditorsScrollWindow)
        self.m_AppsScrolledWindow.Layout()
        self.m_SizerEditorsScrollWindow.Fit(self.m_AppsScrolledWindow)
        sizer_ma_apps.Add(self.m_AppsScrolledWindow, 1, wx.EXPAND | wx.ALL, 5)

        self.m_MaAppsPanel.SetSizer(sizer_ma_apps)
        self.m_MaAppsPanel.Layout()
        sizer_ma_apps.Fit(self.m_MaAppsPanel)
        self.m_Notebook.AddPage(self.m_MaAppsPanel, _(u"Ma(s)"), False)
        notebook_bitmap = wx.Bitmap(os.path.join(g_ma_image_directory, u"ma_blanc_et_noir_256x256.png"),
                                    wx.BITMAP_TYPE_ANY)
        if notebook_bitmap.IsOk():
            notebook_images.Add(notebook_bitmap)
            self.m_Notebook.SetPageImage(notebook_index, notebook_index)
            notebook_index += 1

        self.m_PreferencesPanel = wx.Panel(self.m_Notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                           wx.TAB_TRAVERSAL)
        sizer_preferences = wx.BoxSizer(wx.VERTICAL)

        sizer_languages = wx.BoxSizer(wx.HORIZONTAL)

        self.m_LanguageSelectionStaticText = wx.StaticText(self.m_PreferencesPanel, wx.ID_ANY,
                                                           _(u"Langage de l'application"), wx.DefaultPosition,
                                                           wx.DefaultSize, 0)
        self.m_LanguageSelectionStaticText.Wrap(-1)

        sizer_languages.Add(self.m_LanguageSelectionStaticText, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        sizer_languages.Add((0, 0), 1, wx.EXPAND, 5)

        language_choices = language.g_language.get_languages()
        self.m_LanguageChoice = wx.Choice(self.m_PreferencesPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                          language_choices, 0)
        self.m_LanguageChoice.SetSelection(1)
        sizer_languages.Add(self.m_LanguageChoice, 0, wx.ALL, 5)

        sizer_preferences.Add(sizer_languages, 0, wx.EXPAND, 5)
        sizer_terminal = wx.BoxSizer(wx.HORIZONTAL)

        self.m_TerminalSelectionStaticText = wx.StaticText(self.m_PreferencesPanel, wx.ID_ANY,
                                                           _(u"Commande pour lancer un terminal"), wx.DefaultPosition,
                                                           wx.DefaultSize, 0)
        self.m_TerminalSelectionStaticText.Wrap(-1)

        sizer_terminal.Add(self.m_TerminalSelectionStaticText, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        # sizer_terminal.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_TerminalChoice = wx.Choice(self.m_PreferencesPanel, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, -1),
                                          [], 0)
        self.m_TerminalChoice.SetSelection(0)
        self.m_TerminalChoice.SetToolTip(_(u"Choisissez une commande qui sera exécutée pour lancer les éditeurs."))
        self.m_TerminalChoice.SetMinSize(wx.Size(250, -1))

        sizer_terminal.Add(self.m_TerminalChoice, 1, wx.ALL, 5)

        sizer_preferences.Add(sizer_terminal, 0, wx.EXPAND, 5)

        sizer_preferences.Add((0, 0), 1, wx.EXPAND, 5)

        sizer_maj = wx.BoxSizer(wx.HORIZONTAL)

        self.m_MajStaticText = wx.StaticText(self.m_PreferencesPanel, wx.ID_ANY, _(u"Mise à jour du lanceur"),
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_MajStaticText.Wrap(-1)

        sizer_maj.Add(self.m_MajStaticText, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        sizer_maj.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_MajBitmapButton = wx.BitmapButton(self.m_PreferencesPanel, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition,
                                                 wx.DefaultSize, wx.BU_AUTODRAW | 0)

        self.m_MajBitmapButton.SetBitmap(wx.Bitmap(os.path.join(g_ma_image_directory, u"maj_32x32.png"),
                                                   wx.BITMAP_TYPE_ANY))
        sizer_maj.Add(self.m_MajBitmapButton, 0, wx.ALL, 5)

        sizer_preferences.Add(sizer_maj, 0, wx.EXPAND, 5)

        self.m_PreferencesPanel.SetSizer(sizer_preferences)
        self.m_PreferencesPanel.Layout()
        sizer_preferences.Fit(self.m_PreferencesPanel)
        self.m_Notebook.AddPage(self.m_PreferencesPanel, _(u"Préférences"), False)
        notebook_bitmap = wx.Bitmap(os.path.join(g_ma_image_directory, u"preference_32x32.png"), wx.BITMAP_TYPE_ANY)
        if notebook_bitmap.IsOk():
            notebook_images.Add(notebook_bitmap)
            self.m_Notebook.SetPageImage(notebook_index, notebook_index)
            notebook_index += 1

        sizer_dialog.Add(self.m_Notebook, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(sizer_dialog)
        self.Layout()

        self.Centre(wx.BOTH)

    def __del__(self):
        pass

        # Virtual event handlers, override them in your derived class


class LauncherApp(wx.App):
    """
    # https://wiki.wxpython.org/DialogBoxes
    """

    def __init__(self, redirect=False, filename=None, useBestVisual=False, clearSigInt=True):
        self.m_EditorGuiEditors: dict[EditorGui, Editor] = {}
        self.m_EditorEditorGuis: dict[Editor, EditorGui] = {}
        self.m_EditorsOrdered: list[Editor] = []

        self.m_ProjectGuiProjects: dict[ProjectGui, Project] = {}
        self.m_ProjectProjectGuis: dict[Project, ProjectGui] = {}
        self.m_ProjectOrdered: list[Project] = []

        self.m_Launcher: LauncherGui = None
        self.m_Config: Config = None
        self.m_Preferences: Preferences = None
        wx.App.__init__(self, redirect=False, filename=None, useBestVisual=False, clearSigInt=True)

    def OnInit(self):
        self.m_Config = Config()
        self.m_Preferences = Preferences()
        self.load_preferences()
        self.m_Launcher = LauncherGui(parent=None)
        self.init_events()
        self.load()
        self.m_Launcher.ShowModal()
        self.save()
        self.m_Launcher.Destroy()
        self.m_Launcher = None
        return True

    def init_events(self):
        if self.m_Launcher is not None:
            self.m_Launcher.m_Notebook.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.on_page_changing)
            self.m_Launcher.m_AddProjectBitmapButton.Bind(wx.EVT_BUTTON, self.on_add_project)
            self.m_Launcher.m_AddBitmapButton.Bind(wx.EVT_BUTTON, self.on_add_editor)
            self.m_Launcher.m_LanguageChoice.Bind(wx.EVT_CHOICE, self.on_chosen_language)
            self.m_Launcher.m_TerminalChoice.Bind(wx.EVT_CHOICE, self.on_chosen_terminal)

    def on_rename_project(self, event):
        event.Skip()
        project_gui: ProjectGui = event.EventObject.GetParent()
        project = project_gui.m_Project
        # L'affection du nom est en double… Je m'assure que l'interface se met à jour avec le nouveau nom du projet.
        project.m_Name = event.String

        self.refresh_projects_gui()

    def on_page_changing(self, event):
        event.Skip()
        page_index = event.GetSelection()

        if page_index == 0:
            # onglet des projets
            self.refresh_projects_gui()
        elif page_index == 1:
            # onglet des éditeurs
            self.refresh_editors_gui()
        elif page_index == 2:
            # onglet des préférences
            self.refresh_preferences()

    def on_add_project(self, event: wx.CommandEvent):
        event.Skip()
        project = Project()

        dialog = wx.DirDialog(self.m_Launcher, _(u"Choisissez le dossier où est/sera sauvegardé le projet."))

        if dialog.ShowModal() == wx.ID_OK:
            project_path = dialog.Path
            if os.path.exists(project_path):
                project.set_directory(project_path)
                self.add_project(project)

    def add_project(self, project: Project):
        editor_names: list[str] = []
        for editor in self.m_EditorsOrdered:
            editor_names.append(editor.m_Name)

        project_gui = ProjectGui(project=project,
                                 editor_names=editor_names,
                                 parent=self.m_Launcher.m_ProjectsScrolledWindow)

        project_gui.m_RemoveBitmapButton.Bind(wx.EVT_BUTTON, self.on_remove_project)
        project_gui.m_NameTextCtrl.Bind(wx.EVT_TEXT, self.on_rename_project)
        project_gui.m_Run.Bind(wx.EVT_BUTTON, self.on_launch_editor)

        self.m_Launcher.m_SizerProjectsScrollWindow.Add(project_gui, 0, wx.EXPAND, 5)

        self.m_ProjectGuiProjects[project_gui] = project
        self.m_ProjectProjectGuis[project] = project_gui
        self.m_ProjectOrdered.append(project)

        self.refresh_projects_gui()

    def on_remove_project(self, event: wx.CommandEvent):
        project_gui: ProjectGui = event.EventObject.GetParent()

        if project_gui in self.m_ProjectGuiProjects:
            project = self.m_ProjectGuiProjects[project_gui]
            self.m_ProjectGuiProjects.pop(project_gui)
            self.m_ProjectProjectGuis.pop(project)
            self.m_ProjectOrdered.remove(project)
            project_gui.Destroy()

    def on_add_editor(self, event: wx.CommandEvent):
        event.Skip()
        editor = Editor(in_name="", in_editor_path="")

        self.add_editor(editor)

    def add_editor(self, editor):
        editor_gui = EditorGui(editor=editor, parent=self.m_Launcher.m_AppsScrolledWindow)
        editor_gui.m_Remove.Bind(wx.EVT_BUTTON, self.on_remove_editor)

        self.m_Launcher.m_SizerEditorsScrollWindow.Add(editor_gui, 0, wx.EXPAND, 5)

        self.m_EditorGuiEditors[editor_gui] = editor
        self.m_EditorEditorGuis[editor] = editor_gui
        self.m_EditorsOrdered.append(editor)

        self.refresh_editors_gui()

    def on_remove_editor(self, event: wx.CommandEvent):
        editor_gui: EditorGui = event.EventObject.GetParent()

        if editor_gui in self.m_EditorGuiEditors:
            editor = self.m_EditorGuiEditors[editor_gui]
            self.m_EditorGuiEditors.pop(editor_gui)
            self.m_EditorEditorGuis.pop(editor)
            self.m_EditorsOrdered.remove(editor)
            editor_gui.Destroy()

        self.refresh_editors_gui()

    def on_launch_editor(self, event: wx.CommandEvent):
        event.Skip()
        project_gui: ProjectGui = event.EventObject.GetParent()

        self.launch_editor(project_gui.m_Project)

    def refresh_editors_gui(self):
        size = self.m_Launcher.m_AppsScrolledWindow.GetBestVirtualSize()
        size.y = self.m_Launcher.m_AppStaticTextTitre.GetBestVirtualSize().y

        index = 0
        for editor in self.m_EditorsOrdered:
            assert editor in self.m_EditorEditorGuis, _(u"L'éditeur n'est pas présent dans m_EditorEditorGuis")
            editor_gui = self.m_EditorEditorGuis[editor]
            assert editor_gui in self.m_EditorGuiEditors, \
                _(u"L'interface graphique de l'éditeur n'est pas présente dans m_EditorGuiEditors")

            editor_gui.refresh_gui(index)

            index += 1
            size.y += editor_gui.GetBestVirtualSize().y

        self.m_Launcher.m_AppsScrolledWindow.SetVirtualSize(size)

        self.m_Launcher.m_AppsScrolledWindow.Layout()
        self.m_Launcher.Layout()

    def refresh_projects_gui(self):
        size = self.m_Launcher.m_ProjectsScrolledWindow.GetBestVirtualSize()
        size.y = self.m_Launcher.m_ProjectsStaticTextTitre.GetBestVirtualSize().y

        editor_names: list[str] = []
        for editor in self.m_EditorsOrdered:
            editor_names.append(editor.m_Name)

        for project in self.m_ProjectOrdered:
            assert project in self.m_ProjectProjectGuis, _(u"Le projet n'est pas présent dans m_ProjectProjectGuis")
            project_gui = self.m_ProjectProjectGuis[project]
            assert project_gui in self.m_ProjectGuiProjects, \
                _(u"L'interface graphique du projet n'est pas présente dans m_ProjectGuiProjects")

            project_gui.refresh_gui(editor_names=editor_names)

            size.y += project_gui.GetBestVirtualSize().y

        self.m_Launcher.m_ProjectsScrolledWindow.SetVirtualSize(size)

        self.m_Launcher.m_ProjectsScrolledWindow.Layout()
        self.m_Launcher.Layout()

    def on_chosen_language(self, event: wx.CommandEvent):

        index = event.GetInt()
        language_choice = self.m_Launcher.m_LanguageChoice.Items[index]
        self.m_Preferences.Language = language_choice

        message = _(u"""Pour que le changement de la langue soit pris en compte, l'application doit être redémarrée.\n 
Souhaitez-vous redémarrer l'application?
""")

        dialog = wx.MessageDialog(parent=self.m_Launcher,
                                  message=message,
                                  caption="Changement de langue",
                                  style=wx.YES_NO)

        dialog.SetYesNoLabels(_(u"Oui"), _("Non"))
        response = dialog.ShowModal()
        if response == wx.ID_YES:
            self.restart()
        else:
            event.Skip()

    def on_chosen_terminal(self, event: wx.CommandEvent):
        event.Skip()

        index = event.GetInt()
        terminal_choice = self.m_Launcher.m_TerminalChoice.Items[index]
        self.m_Preferences.Terminal = terminal_choice

    def refresh_preferences(self):
        choice = self.m_Launcher.m_LanguageChoice.FindString(self.m_Preferences.Language)
        if choice != wx.NOT_FOUND:
            self.m_Launcher.m_LanguageChoice.SetSelection(choice)

        self.m_Launcher.m_TerminalChoice.Items = self.m_Preferences.TerminalChoices
        choice = self.m_Launcher.m_TerminalChoice.FindString(self.m_Preferences.Terminal)
        if choice != wx.NOT_FOUND:
            self.m_Launcher.m_TerminalChoice.SetSelection(choice)

        self.m_Launcher.m_LanguageChoice.Layout()
        self.m_Launcher.m_TerminalChoice.Layout()
        self.m_Launcher.Layout()

    def load_preferences(self):
        """Permet de charger seulement les préférences"""
        self.m_Config.load()
        datas = self.m_Config.m_Datas

        if "preferences" in datas:
            preferences_datas = datas["preferences"]
            self.m_Preferences.set_datas(preferences_datas)

    def load(self):
        self.m_Config.load()
        datas = self.m_Config.m_Datas

        if "editors" in datas:
            editors_datas = datas["editors"]

            editors = []
            for i in range(len(editors_datas)):
                editors.append(Editor())

            for index, editor_datas in editors_datas.items():
                editor: Editor = editors[int(index)]
                editor.set_datas(editor_datas)
                self.add_editor(editor)

        # Il faut charger les projets à la suite des éditeurs.
        if "projects" in datas:
            projects_datas = datas["projects"]

            projects = []
            for i in range(len(projects_datas)):
                projects.append(Project())

            for index, projects_datas in projects_datas.items():
                project: Project = projects[int(index)]
                project.set_datas(projects_datas)
                self.add_project(project)

        if "preferences" in datas:
            preferences_datas = datas["preferences"]
            self.m_Preferences.set_datas(preferences_datas)

    def save(self):
        datas = {}
        # Les projets
        projects_data = {}

        index = 0
        for project in self.m_ProjectOrdered:
            projects_data[f"{index}"] = project.get_datas()
            index += 1

        datas["projects"] = projects_data

        # Les éditeurs
        editors_datas = {}

        index = 0
        for editor in self.m_EditorsOrdered:
            editors_datas[f"{index}"] = editor.get_datas()
            index += 1

        datas["editors"] = editors_datas

        # Les préférences
        datas["preferences"] = self.m_Preferences.get_datas()

        self.m_Config.m_Datas = datas
        self.m_Config.save()

    def restart(self):
        """ Redémarrage de l'application Lanceur"""
        self.save()

        message = _(u"L'application « Lanceur » va redémarrer.\nLes paramètres de lancement sont:\n")
        message += f"sys.executable............. : {sys.executable}\n"
        message += f"os.path.abspath(__file__).. : {os.path.abspath(__file__)}\n"
        message += f"sys.argv................... : {sys.argv}\n"

        print(message)

        os.execl(sys.executable, sys.executable, *sys.argv)

    def launch_editor(self, project: Project):
        """ Ouvre un projet avec l'éditeur qui lui est associé."""
        self.save()

        editor = self.m_EditorsOrdered[project.m_EditorIndex]

        message = _(u"Lancement de l'éditeur: ") + u" « " + editor.m_Name + u" » " + '\n'
        message += _(u"Ouverture du projet: ") + u" « " + project.m_Name + u" » " + '\n'
        message += _(u"La commande exécutée est: ")

        print(message)

        cmd = self.m_Preferences.Terminal
        cmd = cmd.replace("$TITLE", editor.m_Name.strip().replace(" ", "_"))
        cmd += u" " + editor.m_Path
        print(cmd)
        wx.Execute(cmd)


app = LauncherApp(redirect=0)
app.MainLoop()
