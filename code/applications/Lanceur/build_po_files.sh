# https://wiki.wxwidgets.org/Internationalization
# Exemple de script « .sh » pour générer un catalogue de fichier.
# Il recherche les chaînes de caractères entouré par « _() » dans tout les fichiers .py du répertoire courant « ./ ».
# Le fichier généré est appelé « lanceur.pot » et est généré dans le dossier « ./po »

SCRIPT=$(readlink -f "$0")
SCRIPT_DIRECTORY=$(dirname "$SCRIPT")
SOURCES_DIRECTORY="$SCRIPT_DIRECTORY"

PO_DIRECTORY="$SCRIPT_DIRECTORY/po"
if [ ! -d "$PO_DIRECTORY" ]; then
    mkdir "$PO_DIRECTORY"
fi

PY_FILE_LIST=$(find "$SOURCES_DIRECTORY" -name '*.py' -print)
xgettext -d lanceur -s --keyword=_ -p "$PO_DIRECTORY" -o lanceur.pot "$PY_FILE_LIST"

# Génération des fichiers « .mo »
LOCALE_DIRECTORY="$SCRIPT_DIRECTORY/locale"
if [ ! -d "$LOCALE_DIRECTORY" ]; then
    mkdir "$LOCALE_DIRECTORY"
fi

PO_FILE_LIST=$(find "$PO_DIRECTORY" -name '*_*.po' -print)
for PO_FILE in $PO_FILE_LIST
do
  LANG=$(basename -s .po "$PO_FILE")
  LANG_DIRECTORY="$LOCALE_DIRECTORY/$LANG"
  LC_DIRECTORY="$LANG_DIRECTORY/LC_MESSAGES"
  MO_OUTPUT_PATH="$LC_DIRECTORY/Lanceur.mo"

  if [ ! -d "$LANG_DIRECTORY" ]; then
    mkdir "$LANG_DIRECTORY"
  fi

  if [ ! -d "$LC_DIRECTORY" ]; then
    mkdir "$LC_DIRECTORY"
  fi

  echo "$MO_OUTPUT_PATH"
  msgfmt "$PO_FILE" -o "$MO_OUTPUT_PATH"

done