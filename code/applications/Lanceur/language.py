# -*- coding: utf-8 -*-
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#

import glob
import os
import gettext

from app import *

# Configuration des traductions.
# https://wiki.wxwidgets.org/Internationalization#Data_file_organization
g_locale_dir = os.path.join(g_script_directory, u"locale")
gettext.bindtextdomain(g_application_name, g_locale_dir)
gettext.textdomain(g_application_name)


class Language:

    def __init__(self):
        """
        https://wiki.wxwidgets.org/Internationalization#Data_file_organization
        """
        self.m_Translation = None

    def set_translation(self, language='fr'):
        self.m_Translation = gettext.translation(domain=g_application_name,
                                                 localedir=g_locale_dir,
                                                 languages=[language])
        self.m_Translation.install()

    def gettext(self, message: str):
        if self.m_Translation is None:
            return gettext.gettext(message)
        else:
            return self.m_Translation.gettext(message)

    def get_languages(self) -> list[str]:
        languages = glob.glob(os.path.join(g_locale_dir, "*_*"))

        result = []
        for language in languages:
            lanceur_mo_path = os.path.join(language, "LC_MESSAGES", "Lanceur.mo")
            if os.path.exists(language) and os.path.isdir(language) and \
                    os.path.exists(lanceur_mo_path) and os.path.isfile(lanceur_mo_path):
                result.append(os.path.basename(language))

        return result


g_language: Language = Language()
_ = g_language.gettext
