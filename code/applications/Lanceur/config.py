# -*- coding: utf-8 -*-
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#

import os
import wx
import json

from app import *


class Config:

    def __init__(self):
        # Accesseur aux chemins standards.
        self.m_StandardPaths = wx.StandardPaths.Get()

        # Chemin du dossier contenant les fichiers de configuration de la solution ma (éditeurs, lanceur…).
        self.m_Directory = os.path.join(self.m_StandardPaths.GetUserConfigDir(), u".ma")

        # Nom du dossier contenant les fichiers de configuration du lanceur.
        self.m_LauncherDirectory = os.path.join(self.m_Directory, u"lanceur")

        # Le fichier de configuration du lanceur.
        self.m_FilePath = os.path.join(self.m_LauncherDirectory, u"lanceur.json")

        if not os.path.exists(self.m_Directory):
            os.mkdir(self.m_Directory)

        assert os.path.exists(self.m_Directory), f"Le dossier « {self.m_Directory} n'a peu être créé."

        if not os.path.exists(self.m_LauncherDirectory):
            os.mkdir(self.m_LauncherDirectory)

        assert os.path.exists(self.m_LauncherDirectory), f"Le dossier « {self.m_LauncherDirectory} n'a peu être créé."

        self.m_Datas = {}

    def save(self):
        data_str = json.dumps(self.m_Datas)

        with open(self.m_FilePath, "w") as file:
            file.write(data_str)

    def load(self):
        data_str = ""

        if os.path.exists(self.m_FilePath):
            with open(self.m_FilePath, "r") as file:
                data_str = file.read()

            self.m_Datas = json.loads(data_str)
