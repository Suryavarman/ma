# -*- coding: utf-8 -*-
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#

import os

import wx
import wx.xrc

from app import *
import language
_ = language._


class Editor:
    def __init__(self, in_name="", in_editor_path=""):
        """
        Définition d'un éditeur
        :param in_name: Nom décrivant l'application.
        :param in_editor_path: Chemin de l'exécutable.
        """
        self.m_Name = in_name
        self.m_Path = in_editor_path

    @staticmethod
    def test_path(path):
        def is_exe(in_path):
            return os.path.exists(in_path) and os.access(in_path, os.X_OK) and os.path.isfile(in_path)

        # \todo exécuter l'application en ligne de commande. La commande l'interroge et de sa réponse dépendra la
        # la validité du chemin.
        return is_exe(path)

        # self.m_Remove.SetBitmap(wx.Bitmap(os.path.join(g_ma_image_directory, u"supprimer_32x32.png"),
        #                                   wx.BITMAP_TYPE_ANY))

    def get_datas(self) -> dict[str, str]:
        """ Défini la structure qui sauvegardera les données de l'éditeur. """
        return {"name": self.m_Name, "path": self.m_Path}

    def set_datas(self, datas: dict[str, str]):
        """ Charge les données de la structure de données data."""
        self.m_Name = datas["name"]
        self.m_Path = datas["path"]


class EditorGui(wx.Panel):

    def __init__(self, editor: Editor, parent, window_id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(670, 200),
                 style=wx.TAB_TRAVERSAL, name=wx.EmptyString):
        wx.Panel.__init__(self, parent, id=window_id, pos=pos, size=size, style=style, name=name)

        self.m_Editor = editor

        sizer = wx.BoxSizer(wx.VERTICAL)

        sizer_header = wx.BoxSizer(wx.HORIZONTAL)

        self.m_IndexAppStaticText = wx.StaticText(self, wx.ID_ANY, _(u"[XX]"), wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_IndexAppStaticText.Wrap(-1)

        sizer_header.Add(self.m_IndexAppStaticText, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_IdStaticLine = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.LI_HORIZONTAL | wx.LI_VERTICAL)
        sizer_header.Add(self.m_IdStaticLine, 0, wx.EXPAND | wx.ALL, 5)

        self.m_AppNameStaticText = wx.StaticText(self, wx.ID_ANY, _(u"Nom de l'éditeur :"), wx.DefaultPosition,
                                                 wx.DefaultSize, 0)
        self.m_AppNameStaticText.Wrap(-1)

        sizer_header.Add(self.m_AppNameStaticText, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_NameTextCtrl = wx.TextCtrl(self, wx.ID_ANY, self.m_Editor.m_Name, wx.DefaultPosition, wx.DefaultSize, 0)
        sizer_header.Add(self.m_NameTextCtrl, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_NameStaticLine = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                              wx.LI_HORIZONTAL | wx.LI_VERTICAL)
        sizer_header.Add(self.m_NameStaticLine, 0, wx.EXPAND | wx.ALL, 5)

        self.m_Remove = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size(-1, -1),
                                        wx.BU_AUTODRAW | 0)

        self.m_Remove.SetBitmap(wx.Bitmap(os.path.join(g_ma_image_directory, u"supprimer_32x32.png"),
                                          wx.BITMAP_TYPE_ANY))
        self.m_Remove.SetToolTip(_(u"Retirer cet éditeur de la liste."))

        sizer_header.Add(self.m_Remove, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        sizer.Add(sizer_header, 0, wx.EXPAND, 5)

        sizer_path = wx.BoxSizer(wx.HORIZONTAL)

        self.m_PathStaticText = wx.StaticText(self, wx.ID_ANY, _(u"Chemin de l'exécutable :"), wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_PathStaticText.Wrap(-1)

        sizer_path.Add(self.m_PathStaticText, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_ExecutbleFilePicker = wx.FilePickerCtrl(self, wx.ID_ANY, self.m_Editor.m_Path,
                                                       _(u"Sélectionner l'exécutable de l'éditeur Ma"), _(u"*"),
                                                       wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE)
        sizer_path.Add(self.m_ExecutbleFilePicker, 1, wx.ALL, 5)
        self.m_ExecutbleFilePicker.Update()

        self.m_OpenOsFileExplorerBitmapButton = wx.BitmapButton(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition,
                                                                wx.DefaultSize, wx.BU_AUTODRAW | 0)

        self.m_OpenOsFileExplorerBitmapButton.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_GO_HOME, wx.ART_BUTTON))
        sizer_path.Add(self.m_OpenOsFileExplorerBitmapButton, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        sizer.Add(sizer_path, 0, wx.EXPAND, 5)

        self.m_ModulesScrolledWindow = wx.ScrolledWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                         wx.HSCROLL | wx.VSCROLL)
        self.m_ModulesScrolledWindow.SetScrollRate(5, 5)
        sizer_modules = wx.GridSizer(1, 5, 0, 0)

        self.m_OgreBitmapButton = wx.BitmapButton(self.m_ModulesScrolledWindow, wx.ID_ANY, wx.NullBitmap,
                                                  wx.DefaultPosition, wx.Size(-1, -1), wx.BU_AUTODRAW | 0)

        self.m_OgreBitmapButton.SetBitmap(wx.Bitmap(os.path.join(g_ma_image_directory, u"ogre_64x64.png"),
                                                    wx.BITMAP_TYPE_ANY))
        self.m_OgreBitmapButton.SetToolTip(_(u"Options du module Ogre"))

        sizer_modules.Add(self.m_OgreBitmapButton, 0, wx.ALL, 0)

        self.m_ModulesScrolledWindow.SetSizer(sizer_modules)
        self.m_ModulesScrolledWindow.Layout()
        sizer_modules.Fit(self.m_ModulesScrolledWindow)
        sizer.Add(self.m_ModulesScrolledWindow, 1, wx.EXPAND | wx.ALL, 5)

        self.m_AppSeparator = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        sizer.Add(self.m_AppSeparator, 0, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(sizer)
        self.Layout()

        # Connect Events
        self.m_NameTextCtrl.Bind(wx.EVT_TEXT, self.on_editor_name)
        self.m_ExecutbleFilePicker.Bind(wx.EVT_FILEPICKER_CHANGED, self.on_file_changed)
        self.m_OpenOsFileExplorerBitmapButton.Bind(wx.EVT_BUTTON, self.on_open_os_file_explorer)
        self.m_OgreBitmapButton.Bind(wx.EVT_BUTTON, self.on_display_ogre_module)

    def __del__(self):
        pass

    # Virtual event handlers, override them in your derived class
    def on_editor_name(self, event: wx.CommandEvent):
        event.Skip()
        self.m_Editor.m_Name = event.String

    def on_file_changed(self, event: wx.FileDirPickerEvent):
        event.Skip()
        # dlg = wx.DirDialog(parent=self,
        #                    message=_(u"Choisissez l'exécutable d'un éditeur Ma."),
        #                    defaultPath=wx.StandardPaths.Get().GetExecutablePath(),
        #                    style=wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST)
        # if dlg.ShowModal() == wx.ID_OK:
        #     self.m_Editor.m_Path = dlg.GetPath()
        self.m_Editor.m_Path = event.GetPath()

    def on_open_os_file_explorer(self, event):
        event.Skip()
        wx.LaunchDefaultApplication(os.path.dirname(os.path.abspath(self.m_Editor.m_Path)))

    def on_display_ogre_module(self, event):
        event.Skip()

    def refresh_gui(self, index: int):
        """
        Rafraichie l'interface.
        cf: https://forums.wxwidgets.org/viewtopic.php?t=17892
        :param index: L'index de l'éditeur parmi les éditeurs répertoriés.
        :return:
        """
        # Bloque les évènements
        self.Freeze()

        self.m_IndexAppStaticText.SetLabelText(f"{index}")

        self.m_ExecutbleFilePicker.Path = self.m_Editor.m_Path
        self.m_NameTextCtrl.Name = self.m_Editor.m_Name

        # Réactive les évènements
        self.Thaw()
