# -*- coding: utf-8 -*-
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#

import os

# Le chemin de ce script.
g_script_directory = os.path.dirname(os.path.abspath(__file__))

# Le chemin du répertoire des images de l'application.
g_ma_image_directory = os.path.join(g_script_directory, u"images")

# Le chemin de l'icône du lanceur d'application.
g_ma_icon_path = os.path.join(g_ma_image_directory, u"ma_32x32.png")

# Nom de l'application
g_application_name = "Lanceur"

# Le numéro de version majeure correspond au cycle courant de la
# feuille de route. Ex : 0 pour 1er cycle, 1 pour le 2ème cycle.
# Remarque : La feuille de route est décrite dans le « lisez-moi » à la racine
# du projet.
g_version_major = 0

# Il correspond au nombre d'étapes accomplies durant ce cycle. Les étapes pour le lanceur ne sont pas encore définies.
g_version_minor = 8

#   Le statut de cette version « a », « rc » ou «» (vide).
# 	Le statut est défini ainsi :
# 	- « a » Pour « alpha », les étapes du cycle en cours ne sont pas finies.
# 	Sa présence indique aussi qu'un cycle est en cours.
# 	- « rc » Pour « Release Candidate » (version candidate à la diffusion),
# 	les étapes du cycle en cours sont terminées. La version en cours est
# 	alors peaufinée, corrigé et est préparée pour être livrée et étiquetée
# 	comme stable.
# 	- «» Vide indique que c'est une version publiée en cours d'amélioration.
# 	Le numéro de révision et de compilation servent ainsi à identifier la
# 	version diffusée.
g_status = 'a'

# Version de l'application de lancement de Ma
g_version = f"{g_version_major}.{g_version_minor}.{g_status}"
