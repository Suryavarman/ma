# utilisation:
# include(ma.cmake)
set (MA_GIT_MODULES_DIR "${PROJECT_SOURCE_DIR}/modules/git/gitmodules")
set (MA_MODULES_DIR "${PROJECT_SOURCE_DIR}/modules/ma")
set (MA_API_DIR "${PROJECT_SOURCE_DIR}/api/ma")
set (MA_DEPENDENCIES_DIR "${PROJECT_SOURCE_DIR}/../dependencies")

set (MA_DEPENDENCIES_CFG_PATH "${MA_DEPENDENCIES_DIR}/dependencies.cfg")

## Lecture et création des variables des dépendances.
file(STRINGS ${MA_DEPENDENCIES_CFG_PATH} MA_DEPENDENCIES_CFG_LINES)

macro(set_var _NAME _VALUE)
    set(${_NAME} "${_VALUE}")
endmacro()

foreach (_variableName ${MA_DEPENDENCIES_CFG_LINES})
    string(FIND "${_variableName}" "[" _FIND_GROUP)
    if(${_FIND_GROUP} EQUAL -1)
        string(FIND "${_variableName}" "=" _FIND_EQUAL)
        if(NOT ${_FIND_EQUAL} EQUAL -1)
            string(LENGTH "${_variableName}" _LENGTH)

            # La clef
            string(SUBSTRING "${_variableName}" 0 ${_FIND_EQUAL} _KEY)
            string(STRIP "${_KEY}" _KEY)
            string(TOUPPER "${_KEY}" _KEY)

            # La valeur
            math(EXPR _LENGTH "${_LENGTH} + 1" OUTPUT_FORMAT DECIMAL)
            string(SUBSTRING "${_variableName}" ${_FIND_EQUAL} ${_LENGTH} _VALUE)
            string(REPLACE "=" "" _VALUE "${_VALUE}")
            string(STRIP "${_VALUE}" _VALUE)

            set_var(${_KEY} ${_VALUE})
            #message("${_KEY}:${_VALUE}")
        endif()
    endif()
endforeach()