# wxWidgets
# Les valeurs qui seront utilisées.
# utilisation:
# include(wxWidgets.cmake)
#
# Attention:
# \todo Corriger ce bogue: Si votre système contient déjà les dll il de se passer l'erreur suivante:
#       Ex: /usr/lib/gcc/x86_64-mageia-linux/12/../../../../lib64/libwx_gtk3u_webview-3.2.so :
#       référence indéfinie vers « wxWindowBase::MakeModal(bool)@WXU_3.2 »
#       Tant que ce bogue n'est pas corrigé, vous pouvez désinstaller wx_gtk3u_webview de votre système.
set(WX_COMPILERS_OPTIONS)
set(WX_DEFINITION_OPTIONS WXUSINGDLL
        __WXGTK__
        wxUSE_UNICODE
        wxUSE_MENUS
        wxUSE_CMDLINE_PARSER
        wxUSE_VALIDATORS
        wxNO__T) # C'est un test à ne pas forcément mettre partout.
set(WX_COMPILER_DIRECTORIES)
set(WX_LINKER_LIBRARIES)
set(WX_LINKER_DIRECTORIES)

if(UNIX)
    set(WX_COMPILER_DIRECTORIES "${MA_WXPYTHON_PATH}/ext/wxWidgets/include"
                                "${MA_WXPYTHON_PATH}/build/wxbld/gtk3/lib/wx/include/gtk3-unicode-3.${MA_WX_MINOR_VERSION}"
                                "${MA_WXPYTHON_PATH}/wx/include"
                                "${MA_WXPYTHON_PATH}/sip/cpp"
                                "${MA_WXPYTHON_PATH}/sip/siplib")

    set(WX_LINKER_LIBRARIES "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_xrc-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_webview-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_html-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_adv-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_core-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu_xml-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu_net-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_aui-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_stc-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_richtext-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_gl-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_propgrid-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}")

    set(WX_LINKER_DIRECTORIES "${MA_PYTHONENV_SP_PATH}/wx")
endif()

if(APPLE)
    set(WX_COMPILERS_OPTIONS "MACOS_CLASSIC"
                             "__WXMAC_XCODE__=1"
                             "__WXOSX_COCOA__")

    set(WX_COMPILER_DIRECTORIES "${MA_WXPYTHON_PATH}/ext/wxWidgets/include"
                                "${MA_WXPYTHON_PATH}/build/wxbld/lib/wx/include/osx_cocoa-unicode-3.${MA_WX_MINOR_VERSION}"
                                "${MA_WXPYTHON_PATH}/wx/include"
                                "${MA_WXPYTHON_PATH}/sip/cpp"
                                "${MA_WXPYTHON_PATH}/sip/siplib")

    set(WX_LINKER_LIBRARIES "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu_net-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu_xml-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_adv-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_aui-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_core-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_gl-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_html-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_media-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_propgrid-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_ribbon-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_richtext-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_stc-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_webview-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_osx_cocoau_xrc-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}")

    set(WX_LINKER_DIRECTORIES "${MA_PYTHONENV_SP_PATH}/wx")
endif()

if(WIN32)
    set(WX_COMPILER_DIRECTORIES "${MA_PYTHON_INCLUDE_PATH}"
                                "${MA_WXPYTHON_PATH}/ext/wxWidgets/include"
                                "${MA_WXPYTHON_PATH}/ext/wxWidgets/include/msvc"
                                "${MA_WXPYTHON_PATH}/build/wxbld/gtk3/lib/wx/include/gtk3-unicode-3.${MA_WX_MINOR_VERSION}"
                                "${MA_WXPYTHON_PATH}/wx/include"
                                "${MA_WXPYTHON_PATH}/sip/cpp"
                                "${MA_WXPYTHON_PATH}/sip/siplib")

    set(WX_LINKER_LIBRARIES "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_xrc-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_webview-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_html-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_adv-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_core-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu_xml-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu_net-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_baseu-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_aui-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_stc-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_richtext-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}"
                            "${MA_PYTHONENV_SP_PATH}/wx/libwx_gtk3u_gl-3.${MA_WX_MINOR_VERSION}${MA_WX_SO_SUFFIX}")

    set(WX_LINKER_DIRECTORIES "${MA_PYTHONENV_SP_PATH}/wx"
            "${MA_PYTHON_DLL_PATH}")
endif()