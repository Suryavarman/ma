# utilisation:
# include(Intel.cmake)
string(FIND "${CMAKE_C_COMPILER}" "icx" _FIND_C_COMPILER)
if(NOT ${_FIND_C_COMPILER} EQUAL -1)
    find_package(IntelDPCPP REQUIRED)
    # \todo Pour Eigen il est très recommandé d'activer l'option
    # -inline-forceinline
endif()
