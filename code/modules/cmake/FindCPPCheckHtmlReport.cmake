# - try to find cppcheck-htmlreport tool
#
# Cache Variables:
# CPPCHECKHTMLREPORT_EXECUTABLE
#
# Non-cache variables you might use in your CMakeLists.txt:
# CPPCHECKHTMLREPORT_FOUND
# TODO:Refactor
file(TO_CMAKE_PATH "C:\\Program Files\\Cppcheck" WINDOWS_PATH)
file(TO_CMAKE_PATH "${CPPCHECKHTMLREPORT_ROOT_DIR}" CPPCHECKHTMLREPORT_ROOT_DIR)
set(CPPCHECKHTMLREPORT_ROOT_DIR
        "${CPPCHECKHTMLREPORT_ROOT_DIR}"
        CACHE
        PATH
        "Path to search for cppcheck-htmlreport")

mark_as_advanced(CPPCHECKHTMLREPORT_ROOT_DIR)

# cppcheck app bundles on Mac OS X are GUI, we want command line only
set(_oldappbundlesetting ${CMAKE_FIND_APPBUNDLE})
set(CMAKE_FIND_APPBUNDLE NEVER)

# If we have a custom path, look there first.
if(CPPCHECKHTMLREPORT_ROOT_DIR)
    find_program(CPPCHECKHTMLREPORT_EXECUTABLE
            NAMES
            cppcheck-htmlreport
            cli
            PATHS
            "${CPPCHECKHTMLREPORT_ROOT_DIR}"
            "${WINDOWS_PATH}"
            PATH_SUFFIXES
            cli
            NO_DEFAULT_PATH)
endif()

find_program(CPPCHECKHTMLREPORT_EXECUTABLE NAMES cppcheck-htmlreport
    PATHS "${WINDOWS_PATH}")

# Restore original setting for appbundle finding
set(CMAKE_FIND_APPBUNDLE ${_oldappbundlesetting})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CPPCheckHtmlReport
        DEFAULT_MSG
        CPPCHECKHTMLREPORT_EXECUTABLE
        )

if(CPPCHECKHTMLREPORT_FOUND OR CPPCHECKHTMLREPORT_MARK_AS_ADVANCED)
    mark_as_advanced(CPPCHECKHTMLREPORT_ROOT_DIR)
endif()

mark_as_advanced(CPPCHECKHTMLREPORT_EXECUTABLE)
