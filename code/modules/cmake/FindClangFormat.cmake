# - Try to find clang-format
# Once done this will define
# CLANG_FORMAT_FOUND - System has the clang-format executable.
# CLANG_FORMAT_EXECUTABLE - The clang-format executable.
# CLANG_FORMAT_VERSION - version of clang-format

if (EXISTS ${MA_CLANG_PATH})

    if(UNIX OR APPLE)
        set(CLANG_FORMAT_EXECUTABLE "${MA_CLANG_PATH}/clang-format")
    endif ()

    if(WIN32)
        set(CLANG_FORMAT_EXECUTABLE "${MA_CLANG_PATH}/clang-format.exe")
    endif()

    set(CLANG_FORMAT_FOUND ON)

else()

    string(REPLACE ":" ";" _PATH $ENV{PATH})
    foreach (p ${_PATH})
        file(GLOB cand ${p}/clang-format*)
        if (cand)
            set(CLANG_FORMAT_EXECUTABLE ${cand})
            set(CLANG_FORMAT_FOUND ON)
            break()
        else ()
            set(CLANG_FORMAT_FOUND OFF)
        endif ()

    endforeach ()

endif()

if(${CLANG_FORMAT_FOUND})
    execute_process(COMMAND ${CLANG_FORMAT_EXECUTABLE} -version OUTPUT_VARIABLE clang_out)
    string(REGEX MATCH .*\(version[^\n]*\)\n version ${clang_out})
    set(CLANG_FORMAT_VERSION ${CMAKE_MATCH_1})
endif()