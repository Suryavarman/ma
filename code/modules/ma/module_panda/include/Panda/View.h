/// \file Panda/View.h
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// /usr/share/panda3d/direct/wxwidgets/
/// https://discourse.panda3d.org/t/panda3d-and-wxwidgets/11310
/// https://discourse.panda3d.org/t/panda-in-windows-application/13823/3

#pragma once

#include <wx/glcanvas.h>
#include <ma/wx/wx.h>

#include <memory>

namespace ma::Panda
{
    class ViewSmile;

    /// \class View
    class View: public wxControl
    {
        public:
        /// Constructor - A new View must receive a parent window to
        /// which it can be attached
        ///    \param parent pointer to a parent window.
        ///    \param winid pointer to a parent window.
        ///    \param pos Window position. wxDefaultPosition indicates that
        ///           wxWidgets should generate a default position for the
        ///           window. If using the wxWindow class directly, supply
        ///           an actual position.
        ///    \param size Window size. wxDefaultSize indicates that
        ///           wxWidgets should generate a default size for the
        ///           window. If no suitable size can be found, the window
        ///           will be sized to 20x20 pixels so that the window is
        ///           visible but obviously not correctly sized.
        ///    \param style Window style. For generic window styles, please
        ///           see wxWindow.
        ///    \param name wxVarBaseItem name.
        View(wxWindow *parent,
             wxWindowID id = wxID_ANY,
             const wxPoint &pos = wxDefaultPosition,
             const wxSize &size = wxDefaultSize,
             long style = 0,
             const wxValidator &validator = wxDefaultValidator,
             const wxString &name = wxPanelNameStr);

        /// Constructeur par défaut
        View();

        ~View() override;

        /// Créer une nouvelle fenêtre de rendu avec le nom qui lui est attribué et assigne cette nouvelle fenêtre
        /// au pointeur de « RenderWindow ».
        ///  \param name Le nom de la fenêtre
        void setRenderWindow(const std::wstring &name);

        /// Pointeur opaque.
        std::unique_ptr<ViewSmile> m_ViewSmile;

        private:
        static const int ID_RENDER_TIMER;

        wxTimer *m_Timer;

        /// Temps en seconde entre chaque appel à dessiner le rendu.
        const double m_DeltaTimeCallFrame;

        /// Notifie Ogre de l'appel à l'évènement de redimensionnement de la fenêtre.
        /// \param event L'évènement de redimensionnement.
        void OnSize(wxSizeEvent &event);

        void OnRenderTimer(wxTimerEvent &event);

        void ToggleTimerRendering();

        /// Définitions des événements
        void InitEvents();
    };
} // namespace ma::Panda
