/// \file Panda/Entry.h
/// \author Pontier Pierre
/// \date 2023-11-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// \remarks Point d'entré du module.
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "Panda/Dll.h"
#include <filesystem>

// L'espace de nom est important sinon des fonctions d'autres modules pourraient être appelées à la place de celles de
// ce module.
namespace ma::Panda
{
    /// \param module_path Le chemin du dossier du module.
    extern "C" void MA_PANDA_DLL_EXPORT dllStartPlugin(const std::filesystem::path& module_path);
    extern "C" void MA_PANDA_DLL_EXPORT dllStopPlugin(void);
    extern "C" void MA_PANDA_DLL_EXPORT dllDeletePlugin(void);
}

