/// \file Panda/Panda.h
/// \author Pontier Pierre
/// \date 2023-11-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// \remarks Point d'entré du module.
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <ma/Platform.h>

#include <pandaFramework.h>
#include <pandaSystem.h>
#include <pandaNode.h>
#include <light.h>

#include <ma/Context.h>
#include <ma/engine/Node.h>
#include <ma/engine/Scene.h>
#include <ma/engine/Primitive.h>
#include <ma/engine/Camera.h>
#include <ma/engine/Light.h>

#include "Panda/Context.h"

/// \namespace Espace de nom pour le moteur de rendu Panda3D.
/// \see https://docs.panda3d.org/1.10/cpp/introduction/tutorial/loading-the-grassy-scenery#the-scene-graph
/// \see https://arsthaumaturgis.github.io/Panda3DTutorial.io/tutorial/tut_lesson01.html
/// Gestion de la boucle de rendu entre wxWidgets et Panda :
/// \see https://docs.panda3d.org/1.10/cpp/programming/tasks-and-events/main-loop
/// Exemple d'intégration de Panda avec wxWidgets :
/// \see https://github.com/panda3d/panda3d/blob/master/direct/src/wxwidgets/WxPandaWindow.py
/// \see https://github.com/panda3d/panda3d/blob/master/direct/src/wxwidgets/ViewPort.py
/// Dessins explicatifs de Panda3D :
/// \see https://docs.panda3d.org/1.10/cpp/more-resources/cheat-sheets
/// \see https://www.panda3d.org/download/noversion/BVWQuickReference.pdf
/// Bien pratique
/// \see  https://www.panda3d.org/download/noversion/QuickReference.pdf
namespace ma::Panda
{
        // RootSmile // ————————————————————————————————————————————————————————————————————————————————————————————————
        class RootSmile
        {
            public:
            /// \see https://docs.panda3d.org/1.10/cpp/introduction/tutorial/starting-panda3d
            ::PandaFramework m_Framework;

            /// Permet d'avoir un gardien qui sera partagé pour chaque fenêtre et GraphicsOutputs
            /// \see https://docs.panda3d.org/1.10/cpp/introduction/tutorial/loading-the-grassy-scenery#the-scene-graph.
            /// \remarks Il sera initialisé par la première fenêtre de créée.
            ::GraphicsStateGuardian *m_Guardian;

            RootSmile();
            virtual ~RootSmile();
        };

        // ViewSmile // ————————————————————————————————————————————————————————————————————————————————————————————————
        class ViewSmile
        {
            public:
            ::WindowFramework *m_RenderWindow;

            ViewSmile();
            virtual ~ViewSmile();
        };

        // Nodef // ————————————————————————————————————————————————————————————————————————————————————————————————————
        /// NodeSmile sera placé enfant du « ma::Panda::Item » héritant de « ma::Data ».
        /// NodeSmile met à jour m_PandaNode à partir de l'observation de du « NodefPtr » qu'observe
        /// « ma::Panda::ITEM ».
        class Nodef: public ma::Data, public ma::Observer
        {
            protected:
                WindowFramework* m_Window;
                PandaFramework* m_Framework;
                NodePath m_Render;

                virtual void SetObservation(const ma::ObservablePtr &observable);

                void OnEnable() override;
                void OnDisable() override;

            public:
                ma::Panda::ContextPtr m_PandaContext;
                ma::Engine::NodefPtr m_ItemNode;

                /// Permet le pointer sur l'objet du rendu. Une caméra, une lumière etc héritent de PandaNode.
                /// \see https://docs.panda3d.org/1.10/cpp/reference/panda3d.core.PandaNode#panda3d.core.PandaNode
                ::PandaNode *m_PandaNode;

                /// Permet de manipuler dans la scène l'objet qui est lié à ceux nœud.
                /// \see https://docs.panda3d.org/1.10/cpp/reference/panda3d.core.NodePath
                ::NodePath m_NodePath;

                explicit Nodef(const ma::Item::ConstructorParameters &in_params,
                               const ContextLinkPtr &link,
                               ma::ItemPtr client,
                               ::PandaNode *panda_node);

                Nodef() = delete;
                ~Nodef() override;

                /// Pour finaliser la construction.
                void EndConstruction() override;

//                /// EndConstruction va l'appeler.
//                /// C'est ici qu'il faut construire les objets.
//                /// A l'appel de cette fonction m_NodePath sera instancié.
//                /// \param panda_node Le nœud de l'objet.
//                virtual void BuildObject(::PandaNode *panda_node);

                void BeginObservation(const ObservablePtr &observable) override;
                void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
                void EndObservation(const ObservablePtr &) override;

                /// Charge la donnée dans le contexte.
                bool Load() override;

                /// Décharge la donnée du contexte.
                bool UnLoad() override;

                /// Par défaut le comportement est d'abord d'appeler Unload et si celui-ci renvoie vrai.
                bool Reload() override;
                M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, Panda)
        };

        // Scenef // ———————————————————————————————————————————————————————————————————————————————————————————————————
        class Scenef: public Nodef
        {
            public:
            ma::Engine::ScenefPtr m_ItemScene;

            explicit Scenef(const Item::ConstructorParameters &in_params,
                            const ContextLinkPtr &link,
                            ma::ItemPtr client);
            ~Scenef() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Scenef, Panda)
        };
        typedef std::shared_ptr<Scenef> ScenefPtr;
        M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, Panda)

        // Cubef // ————————————————————————————————————————————————————————————————————————————————————————————————————
        class Cubef: public Nodef
        {
            public:
            ma::Engine::CubefPtr m_ItemCube;

            // \see https://docs.panda3d.org/1.10/cpp/programming/internal-structures/procedural-generation/putting-geometry-in-scene-graph
            ::Geom *m_Geom;

            /// \see https://docs.panda3d.org/1.10/cpp/reference/panda3d.core.GeomNode#panda3d.core.GeomNode
            ::GeomNode *m_Geometry;

            explicit Cubef(const Item::ConstructorParameters &in_params,
                           const ContextLinkPtr &link,
                           ma::ItemPtr client);
            Cubef() = delete;
            ~Cubef() override;

            void EndConstruction() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cubef, Panda)
        };
        typedef std::shared_ptr<Cubef> CubefPtr;
        M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, Panda)

        // Cameraf // ——————————————————————————————————————————————————————————————————————————————————————————————————
        class Cameraf: public Nodef
        {
            protected:
            void SetObservation(const ma::ObservablePtr &observable) override;

            public:
            ma::Engine::CamerafPtr m_ItemCamera;

            ::Camera *m_Camera;

            /// \exception std::logic_error link->GetContext() renvoie null.
            /// \exception std::logic_error link->GetContext() n'est pas du type ma::Panda::Context.
            /// \exception std::logic_error  std::dynamic_pointer_cast<ma::Panda::Context>(link->GetContext())->m_View
            /// est nul.
            /// \exception std::runtime_error
            /// std::dynamic_pointer_cast<ma::Panda::Context>(link->GetContext())->m_View->m_ViewSmile est nul.
            /// \exception std::runtime_error
            /// std::dynamic_pointer_cast<ma::Panda::Context>(link->GetContext())->m_View->m_ViewSmile->m_RenderWindow
            /// est nul.
            explicit Cameraf(const Item::ConstructorParameters &in_params,
                             const ContextLinkPtr &link,
                             ma::ItemPtr client);
            Cameraf() = delete;
            ~Cameraf() override;

            void EndConstruction() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cameraf, Panda)
        };
        typedef std::shared_ptr<Cameraf> CamerafPtr;
        M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, Panda)

        // Lightf // ———————————————————————————————————————————————————————————————————————————————————————————————————
        class Lightf:
        public Nodef
        {
            protected:
            void SetObservation(const ma::ObservablePtr &observable) override;

            public:
            ma::Engine::LightfPtr m_ItemLight;

            /// \see https://docs.panda3d.org/1.10/cpp/programming/render-attributes/lighting
            PT(::PointLight) m_Light;

            explicit Lightf(const Item::ConstructorParameters &in_params,
                            const ContextLinkPtr &link,
                            ma::ItemPtr client);
            Lightf() = delete;
            ~Lightf() override;

            void EndConstruction() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Lightf, Panda)
        };
        typedef std::shared_ptr<Lightf> LightfPtr;
        M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, Panda)

} // namespace ma::Panda
