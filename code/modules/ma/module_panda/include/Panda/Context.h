/// \file Panda/Context.h
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <ma/Context.h>
#include <ma/Config.h>
#include <ma/engine/Scene.h>
#include <ma/engine/Primitive.h>
#include <ma/engine/Camera.h>
#include <ma/engine/Light.h>
#include <ma/engine/Context.h>

#include "Panda/View.h"
#include "Panda/Config.h"

namespace ma::Panda
{
    class RootSmile;

    class Root: public ma::Item
    {
        public:
        static const ma::Item::key_type &GetKeyRoot();

        explicit Root(const Item::ConstructorParameters &in_params);
        ~Root() override;

        std::unique_ptr<RootSmile> m_RootSmile;

        /// La configuration d'Panda..
        ma::Panda::ConfigGroupVarPtr m_ConfigGroup;

        /// À appeler avant la création de la fenêtre
        void CreateRoot();

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Root, Panda)
    };
    typedef std::shared_ptr<Root> RootPtr;

    class Context: public ma::Engine::Context, public ma::Observer
    {
        protected:
            void OnEnable() override;
            void OnDisable() override;

            void SetObservation(const ObservablePtr& observable);

        public:
            /// Chaque contexte créé une fenêtre. À la destruction du contexte celle-ci sera détruite.
            ma::Panda::View *m_View;

            /// Permet de visualiser les objets avec seulement leur arrêtes de visibles.
            /// \see ::WindowFramework::set_wireframe
            /// \see https://docs.panda3d.org/1.10/cpp/programming/using-cpp/window-framework
            ma::BoolVarPtr m_Wireframe;

            /// Permet d'afficher les deux côter des faces.
            /// \see ::WindowFramework::set_two_sided
            /// \see https://docs.panda3d.org/1.10/cpp/programming/using-cpp/window-framework
            ma::BoolVarPtr m_TwoSided;

            /// Permet d'afficher les faces inversées.
            /// \see ::WindowFramework::set_one_sided_reverse
            /// \see https://docs.panda3d.org/1.10/cpp/programming/using-cpp/window-framework
            ma::BoolVarPtr m_OneSidedReverse;

            /// Permet d'activer ou de désactiver l'illumination de la scène.
            /// \see ::WindowFramework::set_lighting
            /// \see https://docs.panda3d.org/1.10/cpp/programming/using-cpp/window-framework
            ma::BoolVarPtr m_Lighting;

            /// Permet d'activer l'éclairage par pixel?
            /// \see ::WindowFramework::set_perpixel
            /// \see https://docs.panda3d.org/1.10/cpp/programming/using-cpp/window-framework
            ma::BoolVarPtr m_PerPixel;

            explicit Context(const Item::ConstructorParameters &in_params);
            ~Context() override;
            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            wxWindow* GetView() override;

            void OnClosePage(wxAuiNotebookEvent &event);

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Context, Panda)
    };
    typedef std::shared_ptr<Context> ContextPtr;
} // namespace ma::Panda

namespace ma::Engine
{
    template std::shared_ptr<ma::Panda::Context> ma::Engine::CreateContext<ma::Panda::Context>();
}

M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Context, Panda)

