/// \file Dll.h
/// \author Pontier Pierre
/// \date 2023-11-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)

    #ifndef _BUILD_PANDA_STATIC_LIB
        #ifdef _BUILD_PANDA_DLL
            #define MA_PANDA_DLL_EXPORT __declspec(dllexport)
        #else
            #define MA_PANDA_DLL_EXPORT __declspec(dllimport)
        #endif
    #else
        #define MA_PANDA_DLL_EXPORT
    #endif

#else // linux && darwin

    #ifndef _BUILD_PANDA_STATIC_LIB
        #ifdef _BUILD_PANDA_DLL
            #define MA_PANDA_DLL_EXPORT __attribute__((__visibility__("default")))
        #else
            #define MA_PANDA_DLL_EXPORT
        #endif
    #else
        #define MA_PANDA_DLL_EXPORT
    #endif

#endif

// test
#ifdef MA_DLL_EXPORT
    #undef MA_DLL_EXPORT
#endif

#define MA_DLL_EXPORT MA_PANDA_DLL_EXPORT
