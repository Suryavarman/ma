/// \file Context.cpp
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <ma/wx/GuiAccess.h>
#include <ma/Root.h>

#include "Panda/Context.h"
#include "Panda/Panda.h"

namespace ma::Panda
{
    // Root //——————————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Item::key_type &Root::GetKeyRoot()
    {
        static const ma::Item::key_type key{Id().GetKey()};
        return key;
    }

    Root::Root(const Item::ConstructorParameters &in_params):
    ma::Item({in_params, GetKeyRoot(), true}), // note: il peut être sauvegardé
    m_RootSmile(new RootSmile()),
    m_ConfigGroup(AddReadOnlyMemberVar<ma::Panda::ConfigGroupVar>(L"m_ConfigGroup"))
    {
        auto config_item = ma::root()->m_Config;
        if(auto group = config_item->GetGroup(L"panda"))
            SetVarFromValue<ma::Panda::ConfigGroupVar>(m_ConfigGroup->GetKey(), group.value());
    }

    Root::~Root()
    {
        m_RootSmile->m_Framework.close_framework();
    }

    const TypeInfo& Root::GetPandaRootTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetPandaRootClassName(),
            L"Le nœud racine pour le moteur graphique Panda",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    void Root::CreateRoot()
    {
        try
        {
            m_RootSmile->m_Framework.open_framework();
        }
        catch(const std::exception &e)
        {
            MA_ASSERT(false,
                      L"L'appel à open_framework ::Panda::Root a échoué: \n" + ma::to_wstring(e.what()),
                      std::invalid_argument);
        }
    }

    // Context //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Context::Context(const Item::ConstructorParameters &in_params):
    ma::Engine::Context(in_params, L"Panda"),
    m_View{nullptr},
    m_Wireframe{},
    m_TwoSided{},
    m_OneSidedReverse{},
    m_Lighting{},
    m_PerPixel{}
    {}

    Context::~Context() = default;

    void Context::EndConstruction()
    {
        ma::Context::EndConstruction();

        AddDataMaker<ma::Panda::ScenefDataMaker>();
        AddDataMaker<ma::Panda::CubefDataMaker>();
        AddDataMaker<ma::Panda::CamerafDataMaker>();
        AddDataMaker<ma::Panda::LightfDataMaker>();

        auto render = m_View && m_View->m_ViewSmile->m_RenderWindow ? m_View->m_ViewSmile->m_RenderWindow : nullptr;

        if(render)
        {
            render->set_perpixel(true);
            render->set_lighting(true);
        }

        m_Wireframe = AddMemberVar<BoolVar>(L"m_Wireframe", render && render->get_wireframe());
        m_TwoSided = AddMemberVar<BoolVar>(L"m_TwoSided", render && render->get_two_sided());
        m_OneSidedReverse = AddMemberVar<BoolVar>(L"m_OneSidedReverse", render && render->get_one_sided_reverse());
        m_Lighting = AddMemberVar<BoolVar>(L"m_Lighting", render && render->get_lighting());
        m_PerPixel = AddMemberVar<BoolVar>(L"m_PerPixel", render && render->get_perpixel());

        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_Wireframe);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_TwoSided);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_OneSidedReverse);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_Lighting);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_PerPixel);
        }
    }

    wxWindow* Context::GetView()
    {
        return m_View;
    }

    void Context::OnClosePage(wxAuiNotebookEvent &event)
    {
        if(m_View)
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            if(m_View == center_book->GetPage(event.GetSelection()))
            {
                // Pour éviter d'appeler de nouveau la destruction de m_View.
                m_View = nullptr;
                if(auto parent = GetParentOpt())
                    parent.value()->RemoveChild(GetKey());
            }
        }
    }

    void Context::OnEnable()
    {
        ma::Observer::OnEnable();
        ma::Context::OnEnable();

        // Nous commençons par créer la racine de Panda :
        ma::Panda::RootPtr root;
        const auto &key_root = ma::Panda::Root::GetKeyRoot();

        if(!ma::Item::HasItem(key_root))
        {
            root = ma::Item::CreateItem<ma::Panda::Root>();
            root->CreateRoot();
        }

        //auto& framework = root->m_RootSmile->m_Framework;

        if(ma::Item::HasItem(ma::Item::Key::GetGuiAccess()))
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            m_View = new ma::Panda::View(center_book);
            const auto name = L"PandaView: " + GetKey();
            center_book->AddPage(m_View, name);

            m_View->setRenderWindow(name);
            center_book->Bind(wxEVT_AUINOTEBOOK_PAGE_CLOSE, &Context::OnClosePage, this);
        }
    }

    void Context::OnDisable()
    {
        ma::Observer::OnDisable();
        ma::Context::OnDisable();

        if(m_View)
        {
            auto view = m_View;
            m_View = nullptr;
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            const auto page_index = center_book->GetPageIndex(view);
            center_book->DeletePage(page_index);
        }

        RemoveChildren();
    }

    void Context::SetObservation(const ObservablePtr& observable)
    {
        auto render = m_View && m_View->m_ViewSmile->m_RenderWindow ? m_View->m_ViewSmile->m_RenderWindow : nullptr;

        if(render)
        {
            if(observable == m_Wireframe)
                render->set_wireframe(m_Wireframe->Get());

            else if(observable == m_TwoSided)
                render->set_two_sided(m_TwoSided->Get());

            else if(observable == m_OneSidedReverse)
                render->set_one_sided_reverse(m_OneSidedReverse->Get());

            else if(observable == m_Lighting)
                render->set_lighting(m_Lighting->Get());

            else if(observable == m_PerPixel)
                render->set_perpixel(m_PerPixel->Get());
        }
    }

    void Context::BeginObservation(const ObservablePtr &observable)
    {
        SetObservation(observable);
    }

    void Context::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        SetObservation(observable);
    }

    void Context::EndObservation(const ObservablePtr &observable)
    {}

    const TypeInfo& Context::GetPandaContextTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetPandaContextClassName(),
            L"Le contexte pour le moteur graphique Panda",
            {}
        };
        // clang-format on

        return info;
    }

} // namespace ma::Panda

M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Root, Panda, Item)

M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Context, Panda, ma::Engine::Context::GetEngineContextClassHierarchy())
M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Context, Panda)
