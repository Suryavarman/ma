/// \file Panda.cpp
/// \author Pontier Pierre
/// \date 2023-11-30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Panda/Panda.h"

#include <geomTriangles.h>
#include <ambientLight.h>
#include <directionalLight.h>
#include <pointLight.h>
#include <spotlight.h>

namespace ma::Panda
{
    std::string GetNewName(const ma::ItemPtr& item, const std::wstring &default_base_name)
    {
        auto node_name = default_base_name;

        if(item->HasVar(Item::Key::Var::GetName()))
            node_name = item->GetVar(Item::Key::Var::GetName())->toString();

        if(node_name.empty())
            node_name = L"node";

        node_name += L"_" + item->GetKey();

        return ma::to_string(node_name);
    }

    // RootSmile // ————————————————————————————————————————————————————————————————————————————————————————————————————
    RootSmile::RootSmile():
    m_Framework{},
    m_Guardian{nullptr}
    {}

    RootSmile::~RootSmile() = default;

    // ViewSmile // ————————————————————————————————————————————————————————————————————————————————————————————————————
    ViewSmile::ViewSmile():
    m_RenderWindow(nullptr)
    {}

    ViewSmile::~ViewSmile() = default;

    // Nodef // ———————————————————————————————————————————————————————————————————————————————————————————————————
    Nodef::Nodef(const ma::Item::ConstructorParameters &in_params,
                 const ContextLinkPtr &link, ma::ItemPtr client,
                 ::PandaNode *panda_node):
    ma::Data::Data(in_params, link, client),
    ma::Observer(),
    m_Window{nullptr},
    m_Framework{nullptr},
    m_Render{},
    m_PandaContext(std::dynamic_pointer_cast<ma::Panda::Context>(link->GetContext())),
    m_ItemNode(std::dynamic_pointer_cast<ma::Engine::Nodef>(client)),
    m_PandaNode{panda_node},
    m_NodePath{}
    {
        MA_ASSERT(m_PandaContext,
                  L"m_PandaContext est nul.",
                  std::invalid_argument);

        MA_ASSERT(m_ItemNode,
                  L"m_ItemNode est nul.",
                  std::invalid_argument);

        MA_ASSERT(m_PandaContext->m_View,
                  L"m_PandaContext->m_View est nul.",
                  std::invalid_argument);

        MA_ASSERT(m_PandaContext->m_View->m_ViewSmile,
                  L"m_PandaContext->m_View->m_ViewSmile est nul.",
                  std::invalid_argument);

        m_Window = m_PandaContext->m_View->m_ViewSmile->m_RenderWindow;

        MA_ASSERT(m_Window,
                  L"m_Window est nul.",
                  std::invalid_argument);

        m_Render = m_Window->get_render();

        m_Framework = m_Window->get_panda_framework();

        MA_ASSERT(m_Framework,
                  L"m_Framework est nul.",
                  std::invalid_argument);

        MA_ASSERT(m_PandaNode,
                  L"m_PandaNode est nul.",
                  std::invalid_argument);

        m_NodePath = m_Render.attach_new_node(m_PandaNode);

        MA_ASSERT(!m_NodePath.is_empty(),
                  L"m_NodePath est vide.",
                  std::invalid_argument);
    }

    Nodef::~Nodef()
    {
        if(m_PandaNode)
        {
            MA_ASSERT(m_NodePath, L"m_NodePath est nul.", std::invalid_argument);

            // https://docs.panda3d.org/1.10/cpp/reference/panda3d.core.NodePath#_CPPv4N8NodePath11remove_nodeEP6Thread
            m_NodePath.remove_node();

//            // Mais comme c'est nul.
//            auto parents = m_PandaNode->get_parents();
//
//            for(auto i = 0; i < parents.size(); i++)
//            {
//                auto *parent = parents[i];
//                parent->remove_child(m_PandaNode);
//            }

            // Nous allons faire confiance au compteur de référence.
            // Pour voir si Panda détruit bien les objets, il faut s'assurer que dans PandaNode
            // pgraph_cat.is_debug() soit activé.
            m_PandaNode = nullptr;
        }
    }

    void Nodef::SetObservation(const ma::ObservablePtr &observable)
    {
        // Test
        MA_ASSERT(IsEnable(), L"Ne devrait pas être appelé si le nœud est inactif.");

        {
            if(m_ItemNode->m_Position == observable || m_ItemNode->m_Quaternion == observable ||
               m_ItemNode->m_Scale == observable)
            {
                const auto &position = m_ItemNode->m_Position->Get();
                const auto &quaternion = m_ItemNode->m_Quaternion->Get();
                const auto &scale = m_ItemNode->m_Scale->Get();

                // LQuaternion panda_quaternion(quaternion.w(), quaternion.x(), quaternion.y(), quaternion.z());

                // \todo voir si l'utilisation de set_pos_quat_scale serait pas mieux.
                m_NodePath.set_pos(position.x(), position.y(), position.z());
                m_NodePath.set_quat({quaternion.w(), quaternion.x(), quaternion.y(), quaternion.z()});
                m_NodePath.set_scale(scale.x(), scale.y(), scale.z());
            }
            else if(m_ItemNode->m_Euler == observable)
            {
                const auto &quaternion = m_ItemNode->m_Quaternion->Get();
                m_NodePath.set_quat({quaternion.w(), quaternion.x(), quaternion.y(), quaternion.z()});
            }
        }
    }

    void Nodef::EndConstruction()
    {
        MA_ASSERT(m_PandaContext, L"m_PandaContext est nul.", std::invalid_argument);

        MA_ASSERT(m_ItemNode, L"m_ItemScene est nul.", std::invalid_argument);

        MA_ASSERT(
            ma::Item::HasItem(m_ItemNode->GetParentKey()), L"m_ItemNode n'a pas de parent.", std::invalid_argument);

        auto panda_item_parent = ma::Item::GetItem(GetParentKey());

        // Si le parent est un nœud ce pointeur ne sera pas nul.
        auto panda_node_parent = std::dynamic_pointer_cast<ma::Panda::Nodef>(panda_item_parent);

        if(panda_node_parent)
        {
            MA_ASSERT(panda_node_parent->m_PandaNode, L"scene_parent->m_PandaNode est nul.", std::logic_error);

            m_NodePath.reparent_to(panda_node_parent->m_NodePath);
        }

        // Cette fonction déclenchera l'évènement OnEnable qui va appeler Load.
        ma::Item::EndConstruction();

        if(ma::Item::HasItem(Key::GetObservationManager()))
        {
            auto observer_manager = ma::Item::GetItem<ma::ObservationManager>(Key::GetObservationManager());
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Position);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Quaternion);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Euler);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Scale);
        }
        else
        {
            MA_WARNING(false,
                       L"Le gestionnaire d'observations n'ai pas disponible. "
                       L"Aucun item n'est lié à la clef qui lui est attitrée: « " +
                           Key::GetObservationManager() + L" ». Ce nœud ne pourra pas observer les variables: \n" +
                           L" - m_Position \n - m_Quaternion \n - m_Euler \n - m_Scale");
        }
    }

    void Nodef::BeginObservation(const ObservablePtr &observable)
    {
        SetObservation(observable);
    }

    void Nodef::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        SetObservation(observable);
    }

    void Nodef::EndObservation(const ObservablePtr &observable)
    {
        m_PandaContext.reset();
    }

    void Nodef::OnEnable()
    {
        Observer::OnEnable();
        Data::OnEnable();
    }

    void Nodef::OnDisable()
    {
        Observer::OnDisable();
        Data::OnDisable();
    }

    bool Nodef::Load()
    {
        bool result = Data::Load();

        MA_ASSERT(!m_NodePath.is_empty(), L"m_NodePath est vide.", std::logic_error);

        if(m_NodePath.is_hidden())
            m_NodePath.show();

        return result;
    }

    bool Nodef::UnLoad()
    {
        if(!m_NodePath.is_empty())
        {
            if(!m_NodePath.is_hidden())
                m_NodePath.hide();
        }

        return Data::UnLoad();
    }

    bool Nodef::Reload()
    {
        return Data::Reload();
    }

    const TypeInfo&Nodef::GetPandaNodefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetPandaNodefClassName(),
            L"Nodef sera placé enfant du « ma::Panda::ITEM » héritant de « ma::Data ».\n"
            L"Nodef met à jour m_OgreNode à partir de l'observation de du « NodefPtr » "
            L"qu'observe\n « ma::Panda::ITEM »..\n",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Scenef //————————————————————————————————————————————————————————————————————————————————————————————————————————
    Scenef::Scenef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client, new GeomNode(GetNewName(client, L"scene"))),
    m_ItemScene(std::dynamic_pointer_cast<ma::Engine::Scenef>(client))
    {
        MA_ASSERT(m_ItemScene, L"m_ItemScene est nul.", std::invalid_argument);
    }

    Scenef::~Scenef() = default;

    const TypeInfo& Scenef::GetPandaScenefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetPandaScenefClassName(),
            L"Implémentation de ma::Scenef pour Panda.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cubef //—————————————————————————————————————————————————————————————————————————————————————————————————————————
    Cubef::Cubef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client, new GeomNode(GetNewName(client, L"cube"))),
    m_ItemCube(std::dynamic_pointer_cast<ma::Engine::Cubef>(client)),
    m_Geom(nullptr),
    m_Geometry(dynamic_cast<GeomNode*>(m_PandaNode)) // c'est nul de faire ça.
    {
        MA_ASSERT(m_Geometry, L"m_Geometry est nul.", std::invalid_argument);
    }

    Cubef::~Cubef() = default;

    // You can't normalize inline so this is a helper function
    template<typename... Args>
    ::LVector3 normalized(const Args &...args)
    {
        auto vector = ::LVector3(args...);
        vector.normalize();
        return vector;
    }

    Geom* makeCube(float size = 1.0f)
    {
        // https://docs.panda3d.org/1.10/python/programming/internal-structures/procedural-generation/creating-vertex-data
        // https://github.com/panda3d/panda3d-docs/blob/1.10/programming/internal-structures/procedural-generation/predefined-vertex-formats.rst
        // https://programtalk.com/python-more-examples/panda3d.core.GeomVertexFormat.getV3n3cpt2/
        // pos | normal | rgba | uv
        auto format = ::GeomVertexFormat::get_v3n3c4t2(); // direxctx style -> get_v3n3cpt2();
        PT(GeomVertexData) vdata;
        vdata = new ::GeomVertexData("box", format, ::Geom::UH_static);

        // Une ligne correspond à un index. Il faudra donc répéter des points pour avoir différentes normales, couleurs
        // et coordonnées de texture selon la face.
        // Il faut donc 4 vertices par faces soit 6 x 4 = 24 lignes à créer.
        vdata->set_num_rows(24u);

        auto vertices = ::GeomVertexWriter(vdata, "vertex");
        auto normals = ::GeomVertexWriter(vdata, "normal");
        auto colours = ::GeomVertexWriter(vdata, "color");
        auto texcoords = ::GeomVertexWriter(vdata, "texcoord");

        // Le cube doit avoir une dimension de size. Il faut donc diviser par deux sa taille pour obtenir les valeurs
        // des coordonnées de ses sommets.
        auto half_size = size * 0.5f;

        // Positions des sommets
        const ::LVector3 V0(-half_size, -half_size, -half_size),
                         V1(-half_size,  half_size, -half_size),
                         V2( half_size,  half_size, -half_size),
                         V3( half_size, -half_size, -half_size),
                         V4(-half_size, -half_size,  half_size),
                         V5(-half_size,  half_size,  half_size),
                         V6( half_size,  half_size,  half_size),
                         V7( half_size, -half_size,  half_size);

        // Coordonnées des normales (il y a une normale par face)
        const ::LVector3 Nxp( 1,  0,  0),
                         Nxn(-1,  0,  0),
                         Nyp( 0,  1,  0),
                         Nyn( 0, -1,  0),
                         Nzp( 0,  0,  1),
                         Nzn( 0,  0, -1);

        // Couleurs de chaque face
        const ::LVector4 Cxp(1.0, 0.0, 0.0, 1.0),
                         Cxn(0.0, 1.0, 0.0, 1.0),
                         Cyp(0.0, 0.0, 1.0, 1.0),
                         Cyn(1.0, 0.0, 1.0, 1.0),
                         Czp(0.0, 1.0, 1.0, 1.0),
                         Czn(1.0, 1.0, 0.0, 1.0);

        // Chaque face est recouverte par l'entièreté de la texture.
        const ::LVector2 T0(0.0, 1.0),
                         T1(0.0, 0.0),
                         T2(1.0, 0.0),
                         T3(1.0, 1.0);

        // Face -Z
        {
            // 0
            vertices.add_data3(V0);
            normals.add_data3(Nzn);
            colours.add_data4(Czn);
            texcoords.add_data2(T0);

            // 1
            vertices.add_data3(V1);
            normals.add_data3(Nzn);
            colours.add_data4(Czn);
            texcoords.add_data2(T1);

            // 2
            vertices.add_data3(V2);
            normals.add_data3(Nzn);
            colours.add_data4(Czn);
            texcoords.add_data2(T2);

            // 3
            vertices.add_data3(V3);
            normals.add_data3(Nzn);
            colours.add_data4(Czn);
            texcoords.add_data2(T3);
        }

        // Face +Z
        {
            // 4
            vertices.add_data3(V4);
            normals.add_data3(Nzp);
            colours.add_data4(Czp);
            texcoords.add_data2(T0);

            // 5
            vertices.add_data3(V7);
            normals.add_data3(Nzp);
            colours.add_data4(Czp);
            texcoords.add_data2(T3);

            // 6
            vertices.add_data3(V6);
            normals.add_data3(Nzp);
            colours.add_data4(Czp);
            texcoords.add_data2(T2);

            // 7
            vertices.add_data3(V5);
            normals.add_data3(Nzp);
            colours.add_data4(Czp);
            texcoords.add_data2(T1);
        }

        // Face +Y
        {
            // 8
            vertices.add_data3(V1);
            normals.add_data3(Nyp);
            colours.add_data4(Cyp);
            texcoords.add_data2(T0);

            // 9
            vertices.add_data3(V5);
            normals.add_data3(Nyp);
            colours.add_data4(Cyp);
            texcoords.add_data2(T1);

            // 10
            vertices.add_data3(V6);
            normals.add_data3(Nyp);
            colours.add_data4(Cyp);
            texcoords.add_data2(T2);

            // 11
            vertices.add_data3(V2);
            normals.add_data3(Nyp);
            colours.add_data4(Cyp);
            texcoords.add_data2(T3);
        }

        // Face -Y
        {
            // 12
            vertices.add_data3(V0);
            normals.add_data3(Nyn);
            colours.add_data4(Cyn);
            texcoords.add_data2(T0);

            // 13
            vertices.add_data3(V3);
            normals.add_data3(Nyn);
            colours.add_data4(Cyn);
            texcoords.add_data2(T1);

            // 14
            vertices.add_data3(V7);
            normals.add_data3(Nyn);
            colours.add_data4(Cyn);
            texcoords.add_data2(T2);

            // 15
            vertices.add_data3(V4);
            normals.add_data3(Nyn);
            colours.add_data4(Cyn);
            texcoords.add_data2(T3);
        }

        // Face +X
        {
            // 16
            vertices.add_data3(V3);
            normals.add_data3(Nxp);
            colours.add_data4(Cxp);
            texcoords.add_data2(T0);

            // 17
            vertices.add_data3(V2);
            normals.add_data3(Nxp);
            colours.add_data4(Cxp);
            texcoords.add_data2(T1);

            // 18
            vertices.add_data3(V6);
            normals.add_data3(Nxp);
            colours.add_data4(Cxp);
            texcoords.add_data2(T2);

            // 19
            vertices.add_data3(V7);
            normals.add_data3(Nxp);
            colours.add_data4(Cxp);
            texcoords.add_data2(T3);
        }

        // Face -X
        {
            // 20
            vertices.add_data3(V4);
            normals.add_data3(Nxn);
            colours.add_data4(Cxn);
            texcoords.add_data2(T0);

            // 21
            vertices.add_data3(V5);
            normals.add_data3(Nxn);
            colours.add_data4(Cxn);
            texcoords.add_data2(T1);

            // 22
            vertices.add_data3(V1);
            normals.add_data3(Nxn);
            colours.add_data4(Cxn);
            texcoords.add_data2(T2);

            // 23
            vertices.add_data3(V0);
            normals.add_data3(Nxn);
            colours.add_data4(Cxn);
            texcoords.add_data2(T3);
        }

        PT(GeomTriangles) tris;
        tris = new ::GeomTriangles(::GeomEnums::UsageHint::UH_static);

        auto quad = [&](int v0, int v1, int v2, int v3)
        {
            tris->add_vertices(v0, v1, v2);
            tris->add_vertices(v0, v2, v3);
            tris->close_primitive();
        };

        for(int i = 0; i < 24; i += 4)
            quad(i, i + 1, i + 2, i + 3);

        auto* square = new ::Geom(vdata);
        square->add_primitive(tris);
        return square;
    }

    // https://discourse.panda3d.org/t/draw-a-box-sphere-tube-teapot/4506/5
    void Cubef::EndConstruction()
    {
        Nodef::EndConstruction();

        m_Geom = makeCube();
        m_Geometry->add_geom(m_Geom);
    }

    const TypeInfo& Cubef::GetPandaCubefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetPandaCubefClassName(),
            L"Implémentation de ma::Cubef pour Panda.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cameraf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Cameraf::Cameraf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    // On prend la caméra qui a été créée automatiquement lors de la création de la fenêtre.
    Nodef::Nodef(in_params, link, client, std::dynamic_pointer_cast<ma::Panda::Context>(link->GetContext())->m_View->m_ViewSmile->m_RenderWindow->get_camera(0)), //std::dynamic_pointer_cast<ma::Panda::Context>(link->GetContext())->m_View->m_ViewSmile->m_RenderWindow->get_camera_group().get_top_node()),
    m_ItemCamera(std::dynamic_pointer_cast<ma::Engine::Cameraf>(client)),
    m_Camera(dynamic_cast<Camera*>(m_PandaNode))
    {}

    Cameraf::~Cameraf() = default;

    void Cameraf::EndConstruction()
    {
        Nodef::EndConstruction();

        // \todo Pour faire comme pour la partie d'Ogre, il faudra retirer cette ligne plus tard.
        m_NodePath.look_at(LVector3(0, 0, 0));

        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_BackgroundColour);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Far);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Near);
            // observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Ratio);
        }
    }

    void Cameraf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);

        if(m_ItemCamera->m_BackgroundColour == observable)
        {
            const auto &colour = m_ItemCamera->m_BackgroundColour->Get();

            m_Window->get_display_region_3d()->set_clear_color(LColor(static_cast<float>(colour.Red()) / 255.f,
                                                                      static_cast<float>(colour.Green()) / 255.f,
                                                                      static_cast<float>(colour.Blue()) / 255.f,
                                                                      1.0f));
        }
        else
        {
            auto *lens = m_Camera->get_lens();

            if(lens)
            {
                if(m_ItemCamera->m_Far == observable)
                {
                    const auto far = m_ItemCamera->m_Far->Get();
                    lens->set_far(far);
                }
                else if(m_ItemCamera->m_Near == observable)
                {
                    const auto near = m_ItemCamera->m_Near->Get();
                    lens->set_near(near);
                }
//                else if(m_ItemCamera->m_Ratio == observable)
//                {
//                    const auto ratio = m_ItemCamera->m_Ratio->Get();
//                    lens->set_aspect_ratio(ratio);
//                }
            }
        }
    }

    const TypeInfo& Cameraf::GetPandaCamerafTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetPandaCamerafClassName(),
            L"Implémentation de ma::Engine::Cameraf pour Panda.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Lightf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Lightf::Lightf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client, new PointLight(GetNewName(client, L"lumière"))),
    m_ItemLight(std::dynamic_pointer_cast<ma::Engine::Lightf>(client)),
    m_Light{dynamic_cast<PointLight*>(m_PandaNode)}
    {
        MA_ASSERT(m_Light, L"m_Light est nul.", std::invalid_argument);
    }

    Lightf::~Lightf() = default;

    void Lightf::EndConstruction()
    {
        Nodef::EndConstruction();

        MA_ASSERT(m_ItemLight, L"m_ItemLight est nul.", std::invalid_argument);

        m_Render.set_light(m_NodePath);
        //m_Render.clear_light(m_NodePath); // à mettre lors de la désactivation de la lumière.

        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemLight->m_Intensity);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemLight->m_Colour);
        }
    }

    void Lightf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);

        if(m_ItemLight->m_Colour == observable)
        {
            const auto &wx_colour = m_ItemLight->m_Colour->Get();
            const auto panda_colour = LColor(static_cast<float>(wx_colour.Red()) / 255.f,
                                             static_cast<float>(wx_colour.Green()) / 255.f,
                                             static_cast<float>(wx_colour.Blue()) / 255.f,
                                             1.0f);

            m_Light->set_color(panda_colour);
            m_Light->set_specular_color(panda_colour);
        }
        else if(m_ItemLight->m_Intensity == observable)
        {
            const auto intensity = m_ItemLight->m_Intensity->Get();
            // https://docs.panda3d.org/1.10/cpp/programming/render-attributes/lighting
            m_Light->set_attenuation(LVecBase3(intensity, 0, 1));
        }
    }

    const ma::TypeInfo& Lightf::GetPandaLightfTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetPandaLightfClassName(),
            L"Implémentation de ma::Engine::Lightf pour Panda.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

} // namespace ma::Panda

M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, Panda, Data, Observer)
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Scenef, Panda, ma::Panda::Nodef::GetPandaNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cubef, Panda, ma::Panda::Nodef::GetPandaNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cameraf, Panda, ma::Panda::Nodef::GetPandaNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Lightf, Panda, ma::Panda::Nodef::GetPandaNodefClassHierarchy())

M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, Panda, ma::Engine::Scenef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, Panda, ma::Engine::Cubef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, Panda, ma::Engine::Cameraf)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, Panda, ma::Engine::Lightf)
