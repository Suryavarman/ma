/// \file sample.cpp
/// \author Pontier Pierre
/// \date 2023-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <ma/Ma.h>
#include "sample.h"

// en-tête
namespace ma::SampleCpp
{
    class Sample : public ma::Item
    {
        public:
            using ma::Item::Item;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Sample, SampleCpp)
    };
    typedef std::shared_ptr<Sample> SamplePtr;
}
M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Sample, SampleCpp)

// corps
namespace ma::SampleCpp
{
    static bool g_Allocate{false};
    static constexpr auto g_ItemKey{L"944f485b-ca26-40bf-8034-76569de9cabb"};
    static Item::key_type g_MakerKey;

    const TypeInfo& Sample::GetSampleCppSampleTypeInfo()
    {
        // clang-format off
        static const TypeInfo result
        {
            GetSampleCppSampleClassName(),
            L"Classe de démonstration créer via le module « sample_cpp ».",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        // src/sample.cpp
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return result;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Sample, SampleCpp, Item)
    M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Sample, SampleCpp)

    extern "C" void MA_DLL_SAMPLE_CPP_EXPORT dllStartPlugin(const std::filesystem::path& module_path)
    {
        if(g_Allocate)
        {
            dllStopPlugin();
            dllDeletePlugin();
        }

        if(!Item::HasItem(g_MakerKey))
        {
            ma::Item::CreateParameters params{ma::Item::Key::GetRTTI()};
            g_MakerKey = ma::Item::CreateItem<SampleMaker>(params)->GetKey();
        }

        if(!Item::HasItem(g_ItemKey))
            ma::Item::CreateItem<Sample>({Item::Key::GetRoot(), true, true, g_ItemKey});

        g_Allocate = true;
    }

    extern "C" void MA_DLL_SAMPLE_CPP_EXPORT dllStopPlugin(void)
    {
        if(g_Allocate)
        {
            if(Item::HasItem(g_MakerKey))
                Item::EraseItem(g_MakerKey);

            if(Item::HasItem(g_ItemKey))
                Item::EraseItem(g_ItemKey);
        }
    }

    extern "C" void MA_DLL_SAMPLE_CPP_EXPORT dllDeletePlugin(void)
    {
        if(g_Allocate)
        {
            // On s'assure que toutes les ressources du plugin sont bien désallouées.
            // Remarques: La question qu'il faut se poser c'est, doit-on laisser le temps à la poubelle de supprimer les
            // éléments qui ne servent plus à rien ou les libérer directement dans dllStopPlugin.
            if(auto opt = Item::HasItemOpt<Garbage>(Item::Key::GetGarbage()))
            {
                GarbagePtr garbage = opt.value();

                if(garbage->HasTrash(g_MakerKey))
                    garbage->RemoveTrash(g_MakerKey);

                if(garbage->HasTrash(g_ItemKey))
                    garbage->RemoveTrash(g_ItemKey);
            }

            g_Allocate = false;
        }
    }
}