/// \file sample_cpp/sample.h
/// \author Pontier Pierre
/// \date 2023-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "Dll.h"
#include <filesystem>

// L'espace de nom est important sinon des fonctions d'autres modules pourraient être appelées à la place de celles de
// ce module.
namespace ma::SampleCpp
{
    extern "C" void MA_DLL_SAMPLE_CPP_EXPORT dllStartPlugin(const std::filesystem::path& module_path);
    extern "C" void MA_DLL_SAMPLE_CPP_EXPORT dllStopPlugin(void);
    extern "C" void MA_DLL_SAMPLE_CPP_EXPORT dllDeletePlugin(void);
}
