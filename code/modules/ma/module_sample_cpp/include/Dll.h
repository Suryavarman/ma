/// \file Dll.h
/// \author Pontier Pierre
/// \date 2023-11-12
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// \remarks à placer après l'inclusion des en-têtes de ma.
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)

    #ifndef _BUILD_SAMPLE_STATIC_CPP_LIB
        #ifdef _BUILD_SAMPLE_CPP_DLL
            #define MA_DLL_SAMPLE_CPP_EXPORT __declspec(dllexport)
        #else
            #define MA_DLL_SAMPLE_CPP_EXPORT __declspec(dllimport)
        #endif
    #else
        #define MA_DLL_SAMPLE_CPP_EXPORT
    #endif

#else // linux && darwin

    #ifndef _BUILD_SAMPLE_STATIC_CPP_LIB
        #ifdef _BUILD_SAMPLE_CPP_DLL
            // https://gcc.gnu.org/onlinedocs/gcc-4.0.0/gcc/Function-Attributes.html
            #define MA_DLL_SAMPLE_CPP_EXPORT __attribute__((__visibility__("default")))
        #else
            #define MA_DLL_SAMPLE_CPP_EXPORT
        #endif
    #else
        #define MA_DLL_SAMPLE_CPP_EXPORT
    #endif

#endif


