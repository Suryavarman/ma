/// \file wx.py.tpp
/// \author Pontier Pierre
/// \date 2019/07/30
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

template <typename T>
pybind11::handle ma::py::wxCast(const T* src)
{
    //py::print("pybind11::handle ma::py::wxCast(const T* src)");
    // wxASSERT(src);
    if(!!src) // Sans le !! des NULL passe.
    {
        // As always, first grab the GIL
        wxPyBlock_t blocked = ma::py::wxPyBeginBlockThreads();

        /*
        auto *class_info = src->GetClassInfo();
        wxASSERT(class_info);

        auto *class_name = class_info->GetClassName();
        wxASSERT(class_name);

        wxString type_name(class_name);
        /*/
        wxString type_name(src->GetClassInfo()->GetClassName());
        //*/

        PyObject* obj = wxPyConstructObject(const_cast<T*>(src), type_name, false);
        // Finally, after all Python stuff is done, release the GIL
        ma::py::wxPyEndBlockThreads(blocked);

        wxASSERT(obj != nullptr);
        return obj;
    }
    else
        return {};

}

template <typename T>
pybind11::handle ma::py::wxCast(const T* src, const wxString &inTypeName)
{
    //py::print("pybind11::handle ma::py::wxCast(const T* src, const wxString &inTypeName)");
    //wxASSERT(src);
    if(!!src) // Sans le !! des NULL passe.
    {
        // As always, first grab the GIL
        wxPyBlock_t blocked = ma::py::wxPyBeginBlockThreads();

        // La nouvelle instance sera gérée par python. C'est pour ça que le
        // paramètre setThisOwn est à vrais.
        PyObject* obj = wxPyConstructObject(new T(*src), inTypeName, true);

        // Finally, after all Python stuff is done, release the GIL
        ma::py::wxPyEndBlockThreads(blocked);

        wxASSERT(obj != nullptr);
        return obj;
    }
    else return {};
}

template <typename T>
T* ma::py::wxLoad(pybind11::handle src, const wxString& inTypeName)
{
    ::py::print("T* ma::py::wxLoad(pybind11::handle src, const wxString& inTypeName)");
    wxASSERT(src);

    // As always, first grab the GIL
    wxPyBlock_t blocked = ma::py::wxPyBeginBlockThreads();

    /* Extract PyObject from handle */
    PyObject *source = src.ptr();
    T* obj = nullptr;
    bool success = wxPyConvertWrappedPtr(source, (void**) &obj, inTypeName);

    // Finally, after all Python stuff is done, release the GIL
    ma::py::wxPyEndBlockThreads(blocked);

    wxASSERT_MSG(success, wxT("Returned object was not a ") + inTypeName);
    return obj;
}
