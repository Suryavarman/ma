/// \file Item.py.h
/// \author Pontier Pierre
/// \date 2019-12-09
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <pybind11/pybind11.h>
#include <ma/Item.h>

namespace py = pybind11;

namespace ma::py::Item
{
    typedef ::py::class_<ma::Item, ma::ItemPtr, ma::Observable> value_type;
    value_type SetModule(::py::module &in_module);
}
