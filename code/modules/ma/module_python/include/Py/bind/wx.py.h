/// \file wx.py.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \remarks doit être inclus avant les en-têtes de wxWidgets à cause de la macro _() qui est utilisé dans pybind11 et
/// wxWidgets.
///

#pragma once

#include <pybind11/pybind11.h>
//#include <pybind11/stl.h>
//#include <pybind11/embed.h>

// wxWidget va définir l'expression _() avec wxTranslation si WXINTL_NO_GETTEXT_MACRO n'est pas défini.
#ifndef WXINTL_NO_GETTEXT_MACRO
    #define WXINTL_NO_GETTEXT_MACRO
#endif // WXINTL_NO_GETTEXT_MACRO

// pybind11 va définir l'expression _() si elle n'est pas déjà définie
// template <typename Type> constexpr descr<1, Type> _() { return {'%'}; }

#include <pybind11/operators.h>
#include <sip.h>
#include <wxPython/wxpy_api.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <wx/aui/aui.h>
#include <wx/treectrl.h>

// https://pybind11.readthedocs.io/en/stable/advanced/cast/custom.html
// https://pybind11.readthedocs.io/en/stable/advanced/pycpp/object.html#accessing-python-libraries-from-c
// https://www.openflipper.org/media/Documentation/latest/a02744_source.html
// PYBIND11_TYPE_CASTER(M_IN_TYPE, M_IN_STR_PY_TYPE_NAME);
// _wxPyAPI : https://github.com/wxWidgets/Phoenix/blob/master/wx/include/wxPython/wxpy_api.h
// https://docs.python.org/3.9/c-api/index.html

# define M_WX_PY_CASTER(M_IN_TYPE, M_IN_STR_PY_TYPE_NAME) \
namespace pybind11 { namespace detail { \
 \
    template <> struct type_caster<M_IN_TYPE> \
    { \
    public: \
        /** \
         * This macro establishes the name 'M_IN_STR_PY_TYPE_NAME' in \
         * function signatures and declares a local variable \
         * 'value' of type M_IN_TYPE \
         */ \
    protected: \
        M_IN_TYPE* value; \
    public: \
        static constexpr auto name = _(M_IN_STR_PY_TYPE_NAME); \
        template <typename T_, enable_if_t<std::is_same<M_IN_TYPE, remove_cv_t<T_>>::value, int> = 0> \
        static handle cast(T_ *src, return_value_policy policy, handle parent) \
        { \
            if(!src) \
                return none().release(); \
            else \
                return cast(src, policy, parent); \
        } \
        operator M_IN_TYPE*() { return value; } \
        operator M_IN_TYPE&() { return *value; } \
        operator M_IN_TYPE&&() && { return std::move(*value); } \
        template <typename T_> using cast_op_type = pybind11::detail::movable_cast_op_type<T_>; \
 \
        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a M_IN_TYPE \
         * instance or return false upon failure. The second argument \
         * indicates whether implicit conversions should be applied. \
         */ \
        bool load(handle src, bool) \
        { \
            const wxString type_name = wxString::FromUTF8(#M_IN_TYPE); \
            value = ma::py::wxLoad<M_IN_TYPE>(src, type_name); \
            return value != nullptr; \
        } \
 \
        /** \
         * Conversion part 2 (C+    + -> Python): convert an inty instance into \
         * a Python object. The second and third arguments are used to \
         * indicate the return value policy and parent object (for \
         * ``return_value_policy::reference_internal``) and are generally \
         * ignored by implicit casters. \
         */ \
        static handle cast(M_IN_TYPE *src, return_value_policy /* policy */, handle /* parent */) \
        { \
            return ma::py::wxCast(src); \
        } \
    }; \
}} // namespace pybind11::detail

# define M_WX_PY_CASTER_NO_CLASSINFO(M_IN_TYPE, M_IN_STR_PY_TYPE_NAME) \
class M_IN_TYPE##Test : public M_IN_TYPE \
{ \
    public: \
        using M_IN_TYPE::M_IN_TYPE; \
        M_IN_TYPE##Test(const M_IN_TYPE & value): \
        M_IN_TYPE(value) \
        {} \
 \
        virtual ~M_IN_TYPE##Test() \
        { \
            std::cout << "Destructeur de" << #M_IN_TYPE << "Test appelé" << std::endl; \
        } \
}; \
 \
namespace pybind11 { namespace detail { \
 \
    template <> struct type_caster<M_IN_TYPE> \
    { \
    public: \
        /** \
         * This macro establishes the name 'M_IN_STR_PY_TYPE_NAME' in \
         * function signatures and declares a local variable \
         * 'value' of type M_IN_TYPE \
         */ \
 \
    protected: \
        M_IN_TYPE value; \
    public: \
        static constexpr auto name = _(M_IN_STR_PY_TYPE_NAME); \
        template <typename T_, enable_if_t<std::is_same<M_IN_TYPE, remove_cv_t<T_>>::value, int> = 0> \
        static handle cast(const T_ &src, return_value_policy policy, handle parent) { \
            if (policy == return_value_policy::take_ownership) { \
                auto h = cast(std::move(*src), policy, parent); delete src; return h; \
            } else { \
                return cast(*src, policy, parent); \
            } \
        } \
        operator M_IN_TYPE*() { return &value; } \
        operator M_IN_TYPE&() { return value; } \
        operator M_IN_TYPE&&() && { return std::move(value); } \
        template <typename T_> using cast_op_type = pybind11::detail::movable_cast_op_type<T_>; \
 \
        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a M_IN_TYPE \
         * instance or return false upon failure. The second argument \
         * indicates whether implicit conversions should be applied. \
         */ \
        bool load(handle src, bool) \
        { \
            const wxString type_name = wxString::FromUTF8(#M_IN_TYPE); \
            M_IN_TYPE* value_ptr = ma::py::wxLoad<M_IN_TYPE>(src, type_name); \
            if(value_ptr) \
            { \
                value = *value_ptr; \
                return true; \
            } \
            else \
                return false;  \
        } \
 \
        /** \
         * Conversion part 2 (C++ -> Python): convert an inty instance into \
         * a Python object. The second and third arguments are used to \
         * indicate the return value policy and parent object (for \
         * ``return_value_policy::reference_internal``) and are generally \
         * ignored by implicit casters. \
         */ \
        static handle cast(const M_IN_TYPE& src, return_value_policy /* policy */, handle /* parent */) \
        { \
            /* auto* ptr_src = new M_IN_TYPE##Test(src); */ \
            /* return ma::py::wxCast(ptr_src, wxString::FromUTF8(#M_IN_TYPE)); */ \
            return ma::py::wxCast(&src, wxString::FromUTF8(#M_IN_TYPE)); \
        } \
    }; \
}} // namespace pybind11::detail

namespace py = pybind11;

namespace ma::py
{
    /// \brief Permet de construire l'objet de wxPython qui enrobera l'objet de
    ///         wxWidget. Cette fonction est à utiliser pour définir les modules
    ///         que vous souhaitez exportez vers python avec pybind11.
    ///
    /// \param[in] src Objet source de wxWidgets qui possède son enrobage en
    ///             python grâce à wxPython.\n
    ///             Exemple si `T = wxFrame`:\n
    /// >               wxFrame* frame = GetFrame();\n
    /// >               wxCast(frame);\n
    ///
    /// \return L'objet qui décrit l'enrobage de la source en python.
    /// \remarks src doit être un objet qui hérite de wxObject afin d'avoir le wxClassInfo.
    /// \see [La documentation de pybind11 qui traite de cette fonctionnalité.](https://pybind11.readthedocs.io/en/stable/advanced/cast/custom.html)
    ///
    template <typename T>
    pybind11::handle wxCast(const T* src);

    pybind11::handle wxCast(const wxColour* src);

    /// \brief Permet de construire l'objet de wxPython qui enrobera l'objet de
    ///         wxWidget. Cette fonction est à utiliser pour définir les modules
    ///         que vous souhaitez exportez vers python avec pybind11.
    ///
    /// \param[in] src Objet source de wxWidgets qui possède son enrobage en
    ///             python grâce à wxPython.\n
    ///             Exemple si `T = wxSize`:\n
    /// >               wxSize windowSize = GetSize();\n
    /// >               wxCast(windowSize, wxT("wxSize"));\n
    ///
    /// \return L'objet qui décrit l'enrobage de la source en python.
    /// \remarks src
    /// \see [La documentation de pybind11 qui traite de cette fonctionnalité.](https://pybind11.readthedocs.io/en/stable/advanced/cast/custom.html)
    /// \see L'implémentation de PyObject* i_wxVariant_out_helper(const wxVariant& value)
    template <typename T>
    pybind11::handle wxCast(const T* src, const wxString &inTypeName);

    /// \brief Permet de récupérer l'objet wxWidget enrobé par wxPython.
    ///
    /// \param[in] src L'ojet python représenté par pybind11
    /// \param[in] inTypeName La dénomination du type de la source.\n
    ///             Exemple si `T = wxFrame` alors `inTypename = wxT("wxFrame")`
    /// \return Renvoie l'objet de wxWidget représenté par wxPython.
    ///
    template <typename T>
    T* wxLoad(pybind11::handle src, const wxString& inTypeName);

    /// \brief Permet de récupérer la wxString enrobé par wxPython.
    ///
    /// \param[in] src L'objet string python
    ///
    /// \return Renvoie la représentation c++ de la chaîne de caractères python.
    wxString wxLoad(pybind11::handle src);

    /// \brief Permet de définir la valeur par défaut d'un « Validator » avant l'initialisation de wxWidgets.
    const wxValidator& GetDefaultValidator();

    /// \brief permet de déboguer et d'utiliser pybind11
    inline wxPyAPI* wxPyGetAPIPtr()
    {
        static wxPyAPI* wxPyAPIPtr = nullptr;

        if(wxPyAPIPtr == nullptr)
        {
            PyGILState_STATE state = PyGILState_Ensure();

            PyObject* globals = PyDict_New();
            #if PY_MAJOR_VERSION >= 3
                PyObject* builtins = PyImport_ImportModule("builtins");
            #else
                PyObject* builtins = PyImport_ImportModule("__builtin__");
            #endif

            if(PyErr_Occurred()) { PyErr_Print(); }

            wxASSERT(builtins);
            if(builtins)
            {
                // Cette fonction plante si builtins est null https://bugs.python.org/issue5627
                PyDict_SetItemString(globals, "__builtins__", builtins);
                Py_DECREF(builtins);
            }

            wxPyAPIPtr = static_cast<wxPyAPI*>(PyCapsule_Import("wx._wxPyAPI", 0));

            if(PyErr_Occurred())
                PyErr_Print();

            wxASSERT_MSG(wxPyAPIPtr != nullptr, wxT("wxPyAPIPtr is null!!!"));
            PyGILState_Release(state);
        }

        return wxPyAPIPtr;
    }

    inline wxPyBlock_t wxPyBeginBlockThreads()
    { return ma::py::wxPyGetAPIPtr()->p_wxPyBeginBlockThreads(); }

    inline void wxPyEndBlockThreads(wxPyBlock_t blocked)
    { ma::py::wxPyGetAPIPtr()->p_wxPyEndBlockThreads(blocked); }
}

#include "Py/bind/wx.py.tpp"

M_WX_PY_CASTER_NO_CLASSINFO(wxSize, "wx.Size")
M_WX_PY_CASTER_NO_CLASSINFO(wxPoint, "wx.Point")
M_WX_PY_CASTER_NO_CLASSINFO(wxBoxSizer, "wx.BoxSizer")
M_WX_PY_CASTER_NO_CLASSINFO(wxColour, "wx.Colour")
M_WX_PY_CASTER_NO_CLASSINFO(wxDateTime, "wx.DateTime")
M_WX_PY_CASTER_NO_CLASSINFO(wxBitmap, "wx.Bitmap")
M_WX_PY_CASTER_NO_CLASSINFO(wxImage, "wx.Image")
M_WX_PY_CASTER_NO_CLASSINFO(wxIcon, "wx.Icon")

M_WX_PY_CASTER(wxWindow, "wx.Window")
M_WX_PY_CASTER(wxValidator, "wx.Validator")
M_WX_PY_CASTER(wxControl, "wx.Control")
M_WX_PY_CASTER(wxAuiNotebook, "wx._aui.AuiNotebook")
M_WX_PY_CASTER(wxFrame, "wx.Frame")
M_WX_PY_CASTER(wxPanel, "wx.Panel")
M_WX_PY_CASTER(wxTreeCtrl, "wx.TreeCtrl")
M_WX_PY_CASTER(wxBitmapButton, "wx.BitmapButton")
M_WX_PY_CASTER(wxTextCtrl, "wx.TextCtrl")
M_WX_PY_CASTER(wxMenuBar, "wx.MenuBar")
