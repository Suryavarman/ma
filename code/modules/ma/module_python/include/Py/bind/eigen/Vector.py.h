/// \file Vector.py.h
/// \author Pontier Pierre
/// \date 2022-04-10
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <pybind11/pybind11.h>

#include <Py/bind/Item.py.h>

namespace py = pybind11;

namespace ma::py::Eigen::Vector
{
    void SetModule(::py::module &in_ma_module, ::py::module &in_ma_eigen_module, ma::py::Item::value_type& in_class_item);
}
