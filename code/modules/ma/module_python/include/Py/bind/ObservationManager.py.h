/// \file ObservationManager.py.h
/// \author Pontier Pierre
/// \date 2020-02-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace ma::py
{
    namespace ObservationManager
    {
        void SetModule(::py::module &in_module);
    }
}
