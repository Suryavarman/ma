/// \file Py/Py.h
/// \author Pontier Pierre
/// \date 2023-07-09
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Gestion des module écrit en Python3.
///

#pragma once

/// L'espace de nom de « ma::py » de Ma.
/// Ce module créé un module python capable d'utiliser en partie l'api de Ma.
/// Le module ainsi créé se nomme « ma ».
/// \code {.py}
/// import ma
/// root = ma.Item.fs_GetRoot()
/// \endcode
/// \brief Exporte l'api de ma dans un module Python nommé « ma ».
namespace ma::py
{}
