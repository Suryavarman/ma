/// \file Py/Module.h
/// \author Pontier Pierre
/// \date 2023-12-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Gestion des module écrit en Python3.
///

#pragma once

#include <ma/Ma.h>

namespace ma::py
{
    /// Gestion des modules écrit en python 3.
    /// Le module doit contenir les fonctions suivantes:
    /// \code{.py}
    /// import ma
    /// def py_start_plugin():
    ///     pass
    ///
    /// def py_stop_plugin():
    ///     pass
    /// \endcode
    class ModuleMaker: public ma::ModuleMaker
    {
        public:
            /// « .py »
            /// \exception std::domain_error La plateforme n'est pas géré, aucune extension n'est retournée.
            static const std::initializer_list<std::wstring> &GetExtensions();

            explicit ModuleMaker(const Item::ConstructorParameters &in_params);
            ModuleMaker() = delete;
            ~ModuleMaker() override;

            DataMakerPtr Clone(const Item::key_type& key) const override;

            DataPtr CreateData(ContextLinkPtr &link, ma::ItemPtr client, const CreateParameters &params) const override;

            const TypeInfo &GetFactoryTypeInfo() const override;
            static std::wstring GetDataClassName();
            static std::wstring GetClientClassName();

            /// \return Si le dossier du module contient un fichier python du même nom avec l'extension : \n
            /// - « .py »
            std::optional<std::filesystem::path>
            Match(const std::filesystem::directory_entry &dir_entry) const override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ModuleMaker, py)
    };
    typedef std::shared_ptr<ModuleMaker> ModuleMakerPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(ModuleMaker, py)

    /// Module pour charger dynamiquement un script python
    class Module: public ma::Module
    {
        protected:
            /// Raccourcis pour accéder au gestionnaire d'observation.
            ma::ObservationManagerPtr m_ObservationManager;

            /// Raccourcis pour accéder au registre des types.
            ma::ClassInfoManagerPtr m_ClassInfoManager;

            /// Chemin du fichier d'entré.
            /// Cela permet d'établir le chemin une fois.
            std::filesystem::path m_PluginPath;

            py::ModuleMakerPtr m_Maker;

        public:
            /// Représente le fichier qui sera charger. \n
            /// Le fichier d'entré doit être nommé ainsi: \n
            /// {prefix lib}{prexfix module}_{nom du dossier parent sans le préfixe « module_ »} \n
            /// Exemples: \n
            /// module_toto —> libmodule_toto.so \n
            /// moduuli_toto2 —> moduuli_toto.dll
            ma::ResourceVarPtr m_EntryPoint;

            explicit Module(const Item::ConstructorParameters &in_params,
                            const ContextLinkPtr &link,
                            ItemPtr client,
                            py::ModuleMakerPtr maker);
            Module() = delete;
            ~Module() override;

            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            bool Load() override;
            bool UnLoad() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Module, py)
    };
    typedef std::shared_ptr<Module> ModulePtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Module, py)

} // namespace ma::py
