/// \file Py/Config.h
/// \author Pontier Pierre
/// \date 2023-12-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

// #include <sipAPI_core.h>

#include <Python.h>

#include <wx/app.h>
#include <wx/fileconf.h>
#include <memory>
#include <filesystem>

#include <sip.h>
#include <wxPython/wxpy_api.h>
#include <ma/Ma.h>

namespace ma::py
{
    ///  [python]
    ///  path=/home/toto/blabla/Ma/dependencies/venv
    ///  paths=/home/toto/Ma/dependencies/venv/lib/python311.zip:/home/toto/Ma/dependencies/venv/lib/python3.11
    class ConfigGroup : public ma::ConfigGroup
    {
        private:
            PyThreadState *m_MainTState;
            PyObject *m_PyApp;

            /// \todo régler ce doublon avec m_VenvFolderPath
            std::filesystem::path m_PythonEnvPath;

            void CreatePyApp();

            /// \brief Initialise l'interpréteur python et « wx.PyApp ».
            /// \return Vrais si l'initialisation s'est bien déroulée, sinon faux.
            bool InitPython();

            bool GetConfigPythonEnvPath(std::filesystem::path &out_value) const;

            bool GetConfigPythonEnvPaths(std::vector <std::filesystem::path> &out_value) const;

            bool GetConfigGroupEntry(std::wstring &value,
                                     const std::wstring &key) const;

        public:
            struct Key
            {
                static const ma::Item::key_type& GetConfigGroup();
            };

            /// Le chemin du dossier de l'environnement virtuel Python.
            /// path=/home/toto/blabla/Ma/dependencies/venv
            ma::DirectoryVarPtr m_VenvFolderPath;

            /// Les chemins de recherche de python
            /// paths=/home/toto/Ma/dependencies/venv/lib/python311.zip:/home/toto/Ma/dependencies/venv/lib/python3.11
            ma::StringHashSetVarPtr m_Paths;

            /// \brief Constructeur de Project.
            /// \param params Les paramètres de construction d'un item.
            explicit ConfigGroup(const Item::ConstructorParameters &params, const std::filesystem::path& module);

            ConfigGroup() = delete;

            /// \brief Destructeur de Project
            ~ConfigGroup() override;

            /// \brief Pour appeler InitPython(). InitPython a besoin d'accéder au parent.
            void EndConstruction() override;
    };

    typedef std::shared_ptr <ConfigGroup> ConfigGroupPtr;
}

