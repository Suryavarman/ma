/// \file Ogre/Entry.h
/// \author Pontier Pierre
/// \date 2023-12-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \remarks Point d'entré du module.
///

#pragma once

#include "Py/Dll.h"
#include <filesystem>

// L'espace de nom est important sinon des fonctions d'autres modules pourraient être appelées à la place de celles de
// ce module.
namespace ma::py
{
    /// \param module_path Le chemin du dossier du module.
    extern "C" void MA_PY_DLL_EXPORT dllStartPlugin(const std::filesystem::path& module_path);
    extern "C" void MA_PY_DLL_EXPORT dllStopPlugin(void);
    extern "C" void MA_PY_DLL_EXPORT dllDeletePlugin(void);
}
