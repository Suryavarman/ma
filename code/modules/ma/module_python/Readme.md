[![License](https://img.shields.io/badge/license-MIT-green)](https://framagit.org/Suryavarman/ma/-/blob/master/LICENSE)

![Lanceur de Ma](images/ma_py_969x256.png)

# ```import ma```

Ce module python permet d'accéder à l'api C++ de Ma.

```python
>>> import ma
>>> item = ma.Item.fs_CreateItem()
>>> var = item.AddBooleanVar("m_Var", True)
>>> var
<ma.BoolVar( 'm_Var' )>
>>> var.Get()
True
```

Il permet aussi d'accéder à l'interface wxWidgets via wxPython:
```python
import wx
import ma

menu_bar = ma.wx.get_menu_bar()

menu = wx.Menu()
menu.Append(wx.ID_ABOUT, "A propos", "Test Py MenuBar Ajouter un menu.")

menu_bar.Append(menu, "PyMenu")
```

# Feuille de route
## Premier cycle 3/6
- [x] ~~Créer un module python permettant l'accès à l'API via pybind11.~~
- [x] ~~Permettre via le même module python de « Ma », d'accéder à l'interface graphique de wxWidgets via wxPython.~~ 
- [x] ~~Permettre de compiler avec des compilateurs différents.~~
- [x] ~~Créer un fichier [pyproject.toml](https://packaging.python.org/en/latest/tutorials/packaging-projects/) et compiler le module avec wheel.~~
- [ ] Gérer l'ajout d'un ou plusieurs scripts python à un Item.
- [ ] Gérer l'intégration du module « Ma » à Pycharm. Cela comprend aussi le débogage.

## Deuxième cycle 0/7
- [ ] Le numéro de version doit inclure le numéro du commit.
- [ ] Placer les dossiers et fichiers de constructions dans un dossier « .build ».
- [ ] Rendre transparent l'accès aux variables de type « API::Var » dans une classe d'Item. 
- [ ] Générer la documentation. 
- [ ] Gérer et générer les traductions.
- [ ] Gérer et générer les traductions de la documentation.
- [ ] Ne plus utiliser SetupTools pour compiler le module. Utiliser la commande `python -m build`





