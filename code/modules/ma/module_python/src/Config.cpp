/// \file Config.cpp
/// \author Pontier Pierre
/// \date 2023-12-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/Config.h"
#include "Py/ConfigPaths.h"

// #ifdef Py_PYCORECONFIG_H
#if(PY_MAJOR_VERSION >= 3 && PY_MINOR_VERSION >= 11) // Py_PYCORECONFIG_H
// #if MA_COMPILER != MA_COMPILER_CLANG
#define USE_PYCONFIG
// #define USE_PRE_CONFIG
// #define USE_PY_CONFIG_BACKWARD_COMPATIBILY

#ifdef USE_PY_CONFIG_BACKWARD_COMPATIBILY
// https://github.com/python/cpython/blob/b07f546ea3a574bc3016fb023c157c65a47f4849/Python/pathconfig.c
        #define TMP_DEPRECATED Py_DEPRECATED
        #undef Py_DEPRECATED
        #define Py_DEPRECATED
void Py_SetPath_2(const wchar_t *path)
{
    Py_SetPath(path);
}

void Py_SetPythonHome_2(const wchar_t *home)
{
    Py_SetPythonHome(home);
}
        #undef Py_DEPRECATED
        #define Py_DEPRECATED TMP_DEPRECATED
        #undef TMP_DEPRECATED
#endif
// #endif
#endif

#include <pybind11/embed.h>

namespace py = pybind11;

const std::wstring print_line = L"===============================================";

namespace ma::py
{
    // Python //————————————————————————————————————————————————————————————————————————————————————————————————————————————
    ConfigGroup::ConfigGroup(const Item::ConstructorParameters &params, const std::filesystem::path& module):
    ma::ConfigGroup({params, Key::GetConfigGroup()}, L"python"),
    m_MainTState(nullptr),
    m_PyApp(nullptr),
    m_PythonEnvPath(MA_PYTHONENV_PATH),
    m_VenvFolderPath(AddMemberVar<ma::DirectoryVar>(L"path", MA_PYTHONENV_PATH)),
    m_Paths(AddMemberVar<ma::StringHashSetVar>(L"paths"))
    {}

    void ConfigGroup::EndConstruction()
    {
        Item::EndConstruction();
        MA_ASSERT(InitPython(), L"Initialisation du module python a échouée.");
    }

    ConfigGroup::~ConfigGroup()
    {
        if (m_PyApp)
            Py_DECREF(m_PyApp);

        // Restore the thread state and tell Python to clean up after itself.
        // wxPython will do its own cleanup as part of that process.  This is done
        // in OnExit instead of ~App because OnExit is only called if OnInit is
        // successful.
        wxPyEndAllowThreads(m_MainTState);
        Py_Finalize();
    }

    const ma::Item::key_type& ConfigGroup::Key::GetConfigGroup()
    {
        static const auto key = L"4e72e7d7-82a1-4267-bdcd-6738b650be96"s;
        return key;
    }

    bool ConfigGroup::GetConfigGroupEntry(std::wstring &value, const std::wstring &key) const
    {
        // ne pas utiliser GetParentOpt LoadConfig a été appelé avant
        if (const auto config = GetParentOpt<ma::ConfigItem>())
        {
            const auto &datas = config.value()->GetConfig().m_Datas;
            const std::wstring group = m_Name->toString();
            const auto it_find_group = datas.find(group);

            bool result = it_find_group != datas.end();
            if (result)
            {
                const auto it_find_key = it_find_group->second.find(key);
                result = it_find_key != it_find_group->second.end();

                if (result)
                    value = it_find_key->second;
                else
                {
                    const auto message = L"Le fichier de configuration ne contient pas la valeur référencé par ce " \
                                         L"groupe et cette clef: [" + group + L"][" + key + L"]";
                    MA_WARNING(false, message);
                }
            }
            else
            {
                const auto message = L"Le fichier de configuration ne contient pas ce groupe [" + group + L"][" + key +
                                     L"]";
                MA_WARNING(false, message);
            }

            return result;
        }
        else
            return false;
    }

    bool ConfigGroup::GetConfigPythonEnvPath(std::filesystem::path &out_value) const
    {
        std::wstring value;
        const auto result = GetConfigGroupEntry(value, m_VenvFolderPath->GetKey());
        if (result)
            out_value = value;
        return result;
    }

    bool ConfigGroup::GetConfigPythonEnvPaths(std::vector<std::filesystem::path> &out_value) const
    {
        std::wstring value;
        const auto result = GetConfigGroupEntry(value, m_Paths->GetKey());
        if (result)
        {
            for (const auto &path_str: ma::Split(value, L':'))
                out_value.emplace_back(path_str);
        }
        return result;
    }

// https://docs.python.org/3.7/library/gc.html
// https://docs.wxpython.org/wx.App.html
// https://stackoverflow.com/questions/4675728/redirect-stdout-to-a-file-in-python
    char *python_code = "\
# in case of wxPhoenix we need to create a wxApp first and store it\n\
# to prevent removal by garbage collector\n\
import wx\n\
import gc\n\
from contextlib import redirect_stdout\n\
gc.disable()\n\
with open('wx_app_creation_log.txt', 'w') as f:\n\
    with redirect_stdout(f):\n\
        print('Début de la création wx.App')\n\
theApp = wx.App(redirect=True, filename='wx_app_creation_log.txt')\n\
with open('wx_app_creation_log.txt', 'w') as f:\n\
    with redirect_stdout(f):\n\
        print('Début de la création wx.App')\n\
gc.enable()\n\
";

    void ConfigGroup::CreatePyApp()
    {
        // As always, first grab the GIL
        wxPyBlock_t blocked = wxPyBeginBlockThreads();

        PyObject * globals = PyDict_New();
#if PY_MAJOR_VERSION >= 3
        PyObject * builtins = PyImport_ImportModule("builtins");
#else
        PyObject *builtins = PyImport_ImportModule("__builtin__");
#endif

        wxASSERT(builtins);
        if (builtins)
        {
            // crash if builtins is null https://bugs.python.org/issue5627
            PyDict_SetItemString(globals, "__builtins__", builtins);
            Py_DECREF(builtins);
        }

        // Ne peut être appelé pour tous les scripts. Car ça plante quand nous n'en avons pas besoin. Peut-être faut-il le
        // créé sans que le « garbage collector » pose un problème.

        // wx.App = wxPyApp
        m_PyApp = PyRun_String(python_code, Py_file_input, globals, globals);
        if (!m_PyApp)
            PyErr_Print(); // Was there an exception?

        // Finally, after all Python stuff is done, release the GIL
        wxPyEndBlockThreads(blocked);
    }

    bool ConfigGroup::InitPython()
    {
        std::wcout << L"===============================================" << '\n'
                   << _(L"Début de l'initialisation de wxPython.") << '\n'
                   << std::endl;

        if (const auto config = GetParentOpt<ma::ConfigItem>())
        {
            // const auto& config_app = config.value()->GetConfig();
            // Initialize Python
            // https://github.com/pyenv/pyenv/issues/65
            // env PYTHON_CONFIGURE_OPTS="--enable-shared" LD_LIBRARY_PATH=~/.pyenv/versions/2.7.5/lib/ pyenv install -k 2.7.5
            // https://www.developpez.net/forums/i1575075/autres-langages/python-zope/general-python/probleme-import-python/
            // https://docs.python.org/3/using/cmdline.html#envvar-PYTHONHOME
            // https://bugs.python.org/issue34725
            // pyvenv-3.5 --system-site-packages --copies venv
            // si ça ne fonctionne pas essayer ça
            // urpmi python-virtualenv
            // virtualenv --python=/usr/bin/python3 --system-site-packages venv

            m_PythonEnvPath = m_VenvFolderPath->toString();

            std::wcout << L"Python Env Path....... : " << m_PythonEnvPath << std::endl;

#if !defined(USE_PYCONFIG)
            Py_SetPythonHome(const_cast<wchar_t *>(m_PythonEnvPath.wc_str()));
#elif defined(USE_PY_CONFIG_BACKWARD_COMPATIBILY)
            Py_SetPythonHome_2(const_cast<wchar_t *>(m_PythonEnvPath.wc_str()));
#else

#ifdef USE_PRE_CONFIG
            PyPreConfig py_pre_config;
            {
                PyPreConfig_InitIsolatedConfig(&py_pre_config);
                py_pre_config.utf8_mode = 1;
                const auto status = Py_PreInitialize(&py_pre_config);

                if(PyStatus_Exception(status))
                {
                    wxASSERT_MSG(false, _("Impossible d'initialiser la pré-configuration de python "));
                    return false;
                }
            }
#endif

            PyConfig py_config;
            {
                // PyConfig_InitIsolatedConfig(&py_config);
                PyConfig_InitPythonConfig(&py_config);
            }

            // PYTHONHOME
            // Modifie l'emplacement des bibliothèques standards de Python. Par
            // défaut, les bibliothèques sont recherchées dans
            // préfixe/lib/pythonversion et préfixe_exec/lib/pythonversion où
            // préfixe et préfixe_exec sont des répertoires qui dépendent de
            // l'installation (leur valeur par défaut étant /usr/local).
            // Quand PYTHONHOME est défini à un simple répertoire, sa valeur
            // remplace à la fois préfixe et préfixe_exec.
            // Pour spécifier des valeurs différentes à ces variables, définissez
            // PYTHONHOME à préfixe : préfixe_exec.

            // {
            //     // https://docs.python.org/3/c-api/init_config.html#c.pyconfig.home
            //     const auto home = m_PythonEnvPath.wstring();
            //     const auto status = PyConfig_SetString(&py_config, &py_config.home, home.c_str());
            //     if(PyStatus_Exception(status))
            //     {
            //         PyConfig_Clear(&py_config);
            //         wxASSERT_MSG(false,
            //                      _(L"Impossible d'appliquer le chemin du dossier python : ") + home);
            //         return false;
            //     }
            //     py_config._init_main = 0;
            // }

#endif

            std::wcout << L"Python BuildInfo...... : " << ma::to_wstring(Py_GetBuildInfo()) << std::endl;

            const auto py_version = ma::to_wstring(Py_GetVersion());

            wxStringTokenizer tokenizer(py_version, ".");

            wxASSERT_MSG(tokenizer.HasMoreTokens(),
                         _(L"La version de python ne contient pas de version mineur : ") + py_version);
            const auto major = tokenizer.GetNextToken().ToStdWstring();

            wxASSERT_MSG(tokenizer.HasMoreTokens(),
                         _(L"La version de python ne contient pas de version de révision(micro) : ") + py_version);
            const auto minor = tokenizer.GetNextToken().ToStdWstring();

// \todo générer en fonction des versions de python
// https://docs.python.org/3/c-api/init.html#c.Py_SetPath
#ifdef __UNIX__
            const auto delimiter = L":"s;
#else
            const auto delimiter = L";"s;
#endif // __UNIX__

            const std::wstring major_minor = major + L"." + minor;
            std::wcout << _(L"La version de python.. : ") << major_minor << std::endl;

#if defined(USE_PYCONFIG) and !defined(USE_PY_CONFIG_BACKWARD_COMPATIBILY)
            {
                const auto python_executable_path = m_PythonEnvPath / L"bin"s / (L"python"s + major_minor);

                // Python ProgramFullPath : /usr/bin/python3
                // Python ProgramName : python3

                // Traceback (most recent call last):
                // File "<string>", line 4, in <module>
                //   _ZN3API4Item8GetCountEv
                // ImportError:
                // /home/gandi/Working/Ma/dependencies/venv/lib/python3.11/site-packages/ma.cpython-311-x86_64-linux-gnu.so:
                // undefined symbol: _ZN3API4Item8GetCountEv
                {
                    // const auto status = PyConfig_SetBytesString(&py_config, &py_config.program_name,
                    // python_executable_path.c_str()); const auto status = PyConfig_SetBytesString(&py_config,
                    // &py_config.program_name, L"python3"); const auto status = PyConfig_SetString(&py_config,
                    // &py_config.program_name, L"python3");
                    const auto status =
                        PyConfig_SetString(&py_config, &py_config.program_name, python_executable_path.wstring().c_str());
                    if(PyStatus_Exception(status))
                    {
                        PyConfig_Clear(&py_config);
                        wxASSERT_MSG(false,
                                     _(L"Impossible de nommer le nom de l'application python : ") +
                                     python_executable_path.wstring());

                        return false;
                    }
                    else
                    {
                        std::wcout << L"py_config.program_name : " << py_config.program_name << std::endl;
                    }
                }

                {
                    // Valeur renvoyée par Pycharm quand il est lié à un environnement
                    // virtuel.
                    // sys.executable
                    //'/home/gandi/Working/Ma/dependencies/venv/bin/python3.11'
                    const auto status =  PyConfig_SetString(&py_config,
                                                                 &py_config.executable,
                                                                      python_executable_path.wstring().c_str());

                    // const auto status = PyConfig_SetString(&py_config, &py_config.executable, L"");
                    if(PyStatus_Exception(status))
                    {
                        PyConfig_Clear(&py_config);

                        MA_ASSERT(false,
                                  L"Impossible de nommer le chemin de l'application python : " +
                                  python_executable_path.wstring());

                        return false;
                    }
                    else
                    {
                        std::wcout << L"py_config.executable.. : " << py_config.executable << std::endl;
                    }
                }

                {
                    // Valeur renvoyée quand ça fonctionne :
                    // >>> sys.exec_prefix
                    //''
                    // Ceci est bizarre sur Pycharm voilà ce qui est renvoyé :
                    // >>> sys.prefix
                    // '/home/gandi/Working/Ma/dependencies/venv'
                    //
                    const auto status = PyConfig_SetString(&py_config,
                                                                &py_config.prefix,
                                                                     m_PythonEnvPath.wstring().c_str());

                    // const auto status = PyConfig_SetString(&py_config, &py_config.prefix, L"");
                    if(PyStatus_Exception(status))
                    {
                        PyConfig_Clear(&py_config);

                        MA_ASSERT(false,
                                  L"Impossible de nommer le prefix de l'application python : " + m_PythonEnvPath.wstring());

                        return false;
                    }
                    else
                    {
                        std::wcout << L"py_config.prefix...... : " << m_PythonEnvPath << std::endl;
                    }
                }
            }

#endif

            std::vector<std::filesystem::path> py_paths;

            // Imaginons que je souhaite éditer mon fichier .ma/gui/config.cfg et que je me
            // trompe dans l'un des chemins. Alors, il faut pouvoir dire que l'un des
            // chemins est faux et que les chemins par défaut seront utilisés. Et
            // surtout ne pas écraser le fichier de configuration, pour ne pas embêter
            // l'utilisateur qui essaie de modifier ces chemins.
            bool py_paths_dont_overwrite_config_paths = false;

            // Si un seul de ces dossiers et fichiers n'existe pas alors les chemins par
            // défaut seront utilisés.
            // if(GetConfigPythonEnvPaths(py_paths))
            {
                std::vector<std::filesystem::path> paths_dont_exists;

                for (const auto &it: ma::Split(m_Paths->toString(), L':'))
                {
                    const auto path = std::filesystem::path(it);
                    if (!std::filesystem::exists(path) && !std::filesystem::is_directory(path))
                        paths_dont_exists.push_back(path);
                }

                if (!paths_dont_exists.empty())
                {
                    std::wcout << L'\n' << _(L"Attention le fichier de configuration") << L" «"
                               << config.value()->m_ConfigFilePath->toString()
                               << L"» " << _(L"contient des chemins qui n'existent pas:") << std::endl;

                    for (const auto &path: paths_dont_exists)
                        std::wcout << path << std::endl;

                    py_paths.clear();
                    py_paths_dont_overwrite_config_paths = true;
                }
            }

            if (py_paths.empty())
            {
                std::wcout << L'\n' << _(L"Le fichier de configuration: ")
                           << config.value()->m_ConfigFilePath->toString()
                           << _(L"» contient au moins un chemin « paths » invalide.") << L'\n'
                           << _(L"Les valeurs par défaut seront donc utilisées.") << std::endl;

                const std::vector<std::filesystem::path> paths = {
                        // \todo faire un teste de la plateforme si c'est Fedora ou SuSE c'est lib64 sinon c'est probable que ça
                        //      soit lib:
                        //      Il faudrait peut être interroger: sys.platlibdir
                        //      https://docs.python.org/fr/3/library/sys.html#sys.platlibdir
                        m_PythonEnvPath / L"lib64" / (L"python" + major + minor + L".zip"),
                        m_PythonEnvPath / L"lib64" / (L"python" + major_minor),
                        m_PythonEnvPath / L"lib64" / (L"python" + major_minor) / L"plat-linux",
                        m_PythonEnvPath / L"lib64" / (L"python" + major_minor) / L"lib-dynload",
                        m_PythonEnvPath / L"lib" / (L"python" + major_minor) / L"site-packages",
                        m_PythonEnvPath / L"lib" / (L"python" + major + minor + L".zip"),
                        m_PythonEnvPath / L"lib" / (L"python" + major_minor),
                        m_PythonEnvPath / L"lib" / (L"python" + major_minor) / L"plat-linux",
                        m_PythonEnvPath / L"lib" / (L"python" + major_minor) / L"lib-dynload"};

#ifdef USE_PYCONFIG
                // py_config.module_search_paths_set = 1;
#endif

                for (const auto &path: paths)
                {
                    const auto status = PyWideStringList_Append(&py_config.module_search_paths, path.wstring().c_str());
                    if (PyStatus_Exception(status))
                    {
                        PyConfig_Clear(&py_config);

                        wxASSERT_MSG(false,
                                     _(L"Impossible d'ajouter ce chemin de recherche à python : ") + path.wstring());

                        return false;
                    }
                }
            }

            // Exemple de ce qui fonctionne sous Mageia 8:
            // /home/toto/ma/dependencies/venv/lib64/python311.zip
            // /home/toto/ma/dependencies/venv/lib64/python3.11
            // /home/toto/ma/dependencies/venv/lib64/python3.11/plat-linux
            // /home/toto/ma/dependencies/venv/lib64/python3.11/lib-dynload
            // /home/toto/ma/dependencies/venv/lib/python3.11/site-packages
            // /home/toto/ma/dependencies/venv/lib/python311.zip
            // /home/toto/ma/dependencies/venv/lib/python3.11
            // /home/toto/ma/dependencies/venv/lib/python3.11/plat-linux
            // /home/toto/ma/dependencies/venv/lib/python3.11/lib-dynload
            // python programfullpath :
            // python programname :
            // python version : 3.11.0 (main, nov 23 2022, 00:11:48) [gcc 10.4.0]
            // python prefix :
            // python execution prefix :
            // python py_getpythonhome: /home/toto/ma/dependencies/venv

            std::wstring py_paths_str;
            for (const auto &it: py_paths)
                py_paths_str += it.wstring() + delimiter;

#if !defined(USE_PYCONFIG)
            // https://docs.python.org/3/c-api/init.html#c.Py_SetPath
            Py_SetPath(const_cast<wchar_t *>(py_paths_str.wc_str()));
#elif defined(USE_PY_CONFIG_BACKWARD_COMPATIBILY)
            Py_SetPath_2(const_cast<wchar_t *>(py_paths_str.wc_str()));
#else
            {
                // py_config.pythonpath_env = const_cast<wchar_t*>(py_paths_str.wc_str());
                // codec_search_path
                // wxString encoding("utf-8");
                // py_config.filesystem_encoding = const_cast<wchar_t*>(encoding.wc_str());

                const auto status = Py_InitializeFromConfig(&py_config);
                if(PyStatus_Exception(status))
                {
                     // Capture the current Python error
                    PyObject *ptype, *pvalue, *ptraceback;
                    PyErr_Fetch(&ptype, &pvalue, &ptraceback);

                    // Convert the Python error to a string
                    PyObject* str_exc_type = PyObject_Str(ptype);
                    PyObject* str_exc_value = PyObject_Str(pvalue);
                    const auto err_type = ma::to_wstring(PyUnicode_AsUTF8(str_exc_type));
                    const auto err_msg = ma::to_wstring(PyUnicode_AsUTF8(str_exc_value));

                    // Cleanup
                    Py_DECREF(ptype);
                    Py_DECREF(pvalue);
                    Py_DECREF(ptraceback);
                    Py_DECREF(str_exc_type);
                    Py_DECREF(str_exc_value);

                    PyConfig_Clear(&py_config);

                    MA_ASSERT(false,
                              L"Impossible d'initialiser la configuration de python.\n"
                              L"L'erreur est la suivante: « " + err_msg + L" ».\n"
                              L"Le type de l'erreur est le suivant: « " + err_type + L" ».\n",
                              std::invalid_argument);

                    return false;
                }
                else
                {
                    // Double dés-allocation
                    PyConfig_Clear(&py_config);
                }
            }
#endif

            std::wcout << L'\n' << _(L"Les chemins de Python : ") << std::endl;

            {
                const auto paths = ma::Split(ma::to_string(Py_GetPath()), ':');
                for (const auto &path: paths)
                    std::wcout << path << std::endl;
            }

            const auto py_program_full_path = Py_GetProgramFullPath();
            const auto py_program_name = Py_GetProgramName();
            const auto py_prefix = Py_GetPrefix();
            const auto py_exec_prefix = Py_GetExecPrefix();
            const auto py_home = Py_GetPythonHome();

            std::wcout << L'\n' << L"Python ProgramFullPath. : " << py_program_full_path << L'\n'
                       << L"Python ProgramName..... : " << py_program_name << L'\n' << L"Python Version......... : "
                       << py_version << L'\n' << L"Python prefix.......... : " << py_prefix << L'\n'
                       << L"Python execution prefix : " << py_exec_prefix << L'\n' << L"Python Py_GetPythonHome : "
                       << py_home << std::endl;

#if !defined(USE_PYCONFIG) || defined(USE_PY_CONFIG_BACKWARD_COMPATIBILY)
            Py_Initialize();
            std::wcout << L"Py_Initialize" << std::endl;

            std::wcout << L"===============================================" << L'\n' << L"Python Paths : "
                       << std::endl;

            {
                auto paths = ma::Split(Py_GetPath(), ':');
                for (auto path: paths)
                    std::wcout << path << std::endl;
            }

            std::wcout << L"Python ProgramFullPath. : " << Py_GetProgramFullPath() << L'\n'
                       << L"Python ProgramName..... : "
                       << Py_GetProgramName() << L'\n' << L"Python Version......... : " << py_version << L'\n'
                       << L"Python prefix.......... : " << Py_GetPrefix() << L'\n' << L"Python execution prefix : "
                       << Py_GetExecPrefix() << L'\n' << L"Python Py_GetPythonHome : " << Py_GetPythonHome()
                       << std::endl;

#endif
            std::wcout << print_line << std::endl;

#if(PY_MAJOR_VERSION <= 3 && PY_MINOR_VERSION < 9)
            // Ne fait plus rien depuis la version 3.7 et a été déprécié depuis la
            // version 3.9.
            // https://docs.python.org/3/c-api/init.html
            PyEval_InitThreads();
#endif
            // Load the wxPython core API.  Imports the wx._core_ module and sets a
            // local pointer to a function table located there.  The pointer is used
            // internally by the rest of the API functions.
            if (!wxPyGetAPIPtr())
            {
                std::set<std::wstring> modules;
                PyObject* sys_modules = PySys_GetObject("modules");
                PyObject* keys = PyDict_Keys(sys_modules);
                bool wx_installed{false};

                for (Py_ssize_t i=0; i < PyList_Size(keys); i++)
                {
                     PyObject *key = PyList_GetItem(keys, i);
                     const auto module_name = ma::to_wstring(PyUnicode_AsUTF8(key));
                     if(!wx_installed)
                        wx_installed = module_name.size() >= 2u && module_name[0u] == L'w' && module_name[1u] == L'x';

                     modules.insert(module_name);
                }

                const bool wx_listed = modules.count(L"wx") > 0;

                std::wstring message;
                message = wx_installed ? L"Le module « wx » est bien installé" : L"Le module « wx » n'est pas installé";
                message += L" et ";
                message += wx_listed ? L"est bien présent" : L"n'est pas présent";
                message += L" dans la liste des modules de cet environnement python.";

                wxLogError(_(L"Erreur lors du chargement du module python « wx »! ") + message);

                PyErr_Print();
                Py_Finalize();
                return false;
            }

            // Save the current Python thread state and release the
            // Global Interpreter Lock.
            m_MainTState = wxPyBeginAllowThreads();

            CreatePyApp();

            std::wcout << L"py_paths_str" << py_paths_str << std::endl;

            auto paths = m_Paths->Get();
            paths.clear();

            m_PythonEnvPath = py_prefix;

            MA_ASSERT(std::filesystem::exists(m_PythonEnvPath),
                      L"m_PythonEnvPath n'existe pas « " + m_PythonEnvPath.wstring() + L" ».");

            m_VenvFolderPath->fromString(m_PythonEnvPath.wstring());
            if (!py_paths_dont_overwrite_config_paths)
                for (const auto &it: py_paths)
                    paths.insert(it.wstring());
        }
        std::wcout << _(L"Fin de l'initialisation de wxPython.") << '\n' << print_line << std::endl;

        return true;
    }
} // namespace ma::py