/// \file Module.cpp
/// \author Pontier Pierre
/// \date 2023-12-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <Python.h>
//#include <pybind11/embed.h>
#include <sip.h>
#include <wxPython/wxpy_api.h>
#include <sstream>

#include "Py/Module.h"

std::wstring GetErrorMessageDeprecated()
{
     // Capture the current Python error
    PyObject *ptype, *pvalue, *ptraceback;
    PyErr_Fetch(&ptype, &pvalue, &ptraceback);

    // Convert the Python error to a string
    PyObject* str_exc_type = PyObject_Str(ptype);
    PyObject* str_exc_value = PyObject_Str(pvalue);
    const auto err_type = ma::to_wstring(PyUnicode_AsUTF8(str_exc_type));
    const auto err_msg = ma::to_wstring(PyUnicode_AsUTF8(str_exc_value));

    std::wstringstream ss_trace_back;

    if(ptraceback)
    {
        PyObject *tracebackModule = PyImport_ImportModule("traceback");
        PyObject *tracebackDict = PyModule_GetDict(tracebackModule);
        PyObject *formatExceptionFunc = PyDict_GetItemString(tracebackDict, "format_exception");

        if(formatExceptionFunc && PyCallable_Check(formatExceptionFunc))
        {
            PyObject *args = PyTuple_Pack(3, ptype, pvalue, ptraceback);
            PyObject *output = PyObject_CallObject(formatExceptionFunc, args);

            if(output)
            {
                // Convert the output to a string for printing
                PyObject* outStr = PyObject_Repr(output);
                ss_trace_back << PyUnicode_AsUTF8(outStr) << L"\n";
            }
        }
    }

    PyErr_Restore(ptype, pvalue, ptraceback);

    // Cleanup
    Py_DECREF(ptype);
    Py_DECREF(pvalue);
    Py_DECREF(ptraceback);
    Py_DECREF(str_exc_type);
    Py_DECREF(str_exc_value);

    return L"L'erreur est la suivante: « " + err_msg + L" ».\n"
           L"Le type de l'erreur est le suivant: « " + err_type + L" ».\n" + ss_trace_back.str();
}

// À faire quand la fonction PyErr_GetRaisedException sera opérationnelle. Python 3.12 ?
std::wstring GetErrorMessageNew()
{
    // https://docs.python.org/fr/3/c-api/exceptions.html#c.PyErr_GetRaisedException
//    PyObject *type, *value, *traceback;
//    int ret;
//
//    ret = PyErr_GetRaisedException(&type, &value, &traceback);
    return {};
}

void CallPyFunction(const std::wstring &module_name, const std::filesystem::path &module_path, const std::wstring& function_name)
{
    if(std::filesystem::exists(module_path))
    {
        //  = PyRun_String(import_module_str.c_str(), Py_file_input, globals, globals);
        PyObject* sys_paths = PySys_GetObject("path");
        const auto parent_path = module_path.parent_path();
        auto* py_name = PyUnicode_DecodeFSDefault(ma::to_string(module_name).c_str());
        auto* py_path = PyUnicode_DecodeFSDefault(parent_path.c_str());

        bool find = false;
        for (Py_ssize_t i{PyList_Size(sys_paths)}; i-- && !find;)
        {
             auto sys_path_str = ma::to_wstring(PyUnicode_AsUTF8(PyList_GetItem(sys_paths, i)));
             const std::filesystem::path sys_path{sys_path_str};
             find = parent_path == sys_path;
        }

        if(!find)
            PyList_Append(sys_paths, py_path);

        PyObject* py_module = PyImport_Import(py_name);

        Py_DECREF(py_name);
        Py_DECREF(py_path);

        // https://docs.python.org/3.11/extending/embedding.html#pure-embedding
        if (py_module != nullptr)
        {
            const auto func_name = ma::to_string(function_name);
            auto* func = PyObject_GetAttrString(py_module, func_name.c_str());

            if (func && PyCallable_Check(func))
            {
                auto* value = PyObject_CallObject(func, nullptr);
                if (value != nullptr)
                {
                    printf("Result of call: %ld\n", PyLong_AsLong(value));
                    Py_DECREF(value);
                }
                else
                {
                    MA_WARNING(false, L"L'appel à la fonction " + ma::to_wstring(func_name) +
                                      L" à échoué." + GetErrorMessageDeprecated());
                }
            }
            else
            {
                 MA_WARNING(false, L"Impossible de trouver la fonction " + ma::to_wstring(func_name) +
                                   L".\n" + GetErrorMessageDeprecated());
            }
            Py_XDECREF(func);
            Py_DECREF(py_module);
        }
        else
        {
            MA_WARNING(false, L"Le module " + module_path.wstring() + L" n'a peu être chargé.\n" +
                              GetErrorMessageDeprecated());
        }
    }
}

void LoadwxPython(const wxString& inScriptPath)
{
    // As always, first grab the GIL
    wxPyBlock_t blocked = wxPyBeginBlockThreads();
    {   // Portée pour s'assurer que les object statique soient bien supprimés.

        /*
        auto py_wx_module = py::module::import("wx");
        auto py_wx_aui_module = py::module::import("wx.aui");
        /*/
        PyObject* py_wx_module = PyImport_ImportModule("wx");
        PyObject* py_wx_aui_module = PyImport_ImportModule("wx.aui");
        //*/

        // avant l'appel à wxPyEndBlockThreads
        PyObject* globals = PyDict_New();
#if PY_MAJOR_VERSION >= 3
        PyObject* builtins = PyImport_ImportModule("builtins");
#else
        PyObject* builtins = PyImport_ImportModule("__builtin__");
#endif

        wxASSERT(builtins);
        if(builtins)
        {
            // Cette fonction plante si builtins est null https://bugs.python.org/issue5627
            PyDict_SetItemString(globals, "__builtins__", builtins);
            Py_DECREF(builtins);
        }

        PyObject* py_module = nullptr;

        // Execute the code to make the make_window function
        auto path = inScriptPath.ToStdString();
        std::ifstream file(path);
        if(file)
        {
            std::stringstream buffer;
            buffer << file.rdbuf();
            file.close();

            py_module = PyRun_String(buffer.str().c_str(), Py_file_input, globals, globals);
        }

        auto decref = [](PyObject* inPyObj)
        {
            if(inPyObj)
                Py_DECREF(inPyObj);
            else
                PyErr_Print(); // Was there an exception?
        };

        decref(py_module);
        decref(py_wx_module);
        decref(py_wx_aui_module);
        decref(globals);

        // Finally, after all Python stuff is done, release the GIL
    }
    wxPyEndBlockThreads(blocked);
}

namespace ma::py
{
    // Module //———————————————————————————————————————————————————————————————————————————————————————————————————
    Module::Module(const Item::ConstructorParameters &in_params,
                   const ContextLinkPtr &link,
                   ItemPtr client,
                   ma::py::ModuleMakerPtr maker):
    ma::Module(in_params, link, client, maker),
    m_ObservationManager(GetItem<ObservationManager>(Item::Key::GetObservationManager())),
    m_ClassInfoManager(GetItem<ClassInfoManager>(Item::Key::GetClassInfoManager())),
    m_PluginPath(),
    m_Maker(maker)
    {
        MA_ASSERT(m_Maker, L"m_Maker est nul.", std::logic_error);
    }

    Module::~Module()
    {
        // \todo Si le module est charger le décharger.
    }

    void Module::EndConstruction()
    {
        m_EntryPoint = AddReadOnlyMemberVar<ResourceVar>(L"m_EntryPoint");

        auto &folder = m_Folder->GetItem();
        const std::filesystem::directory_entry folder_path{folder->m_Path->Get()};
        if(auto const opt = m_Maker->Match(folder_path))
        {
            m_PluginPath = opt.value();
        }
        else
        {
            const auto &path = folder_path.path();
            MA_ASSERT(false,
                      L"Le dossier « " + path.wstring() + L" » ne contient aucun fichier d'entré valide"
                          L" portant le  nom « " + path.stem().wstring() + L" ». ",
                      std::logic_error);
        }

        // On va observer le dossier.
        // Dès que celui-ci possède le fichier du point d'entrée, nous relancerons Load().
        // \todo Load sera donc lancé deux fois inutilement. Veiller à ce qu'il soit lancé une seul fois.
        m_ObservationManager->AddObservation<ma::ItemObserverVar, ma::ItemVar>(this, folder);

        ma::Module::EndConstruction();
    }

    void Module::BeginObservation(const ObservablePtr &observable)
    {
        ma::Module::BeginObservation(observable);

        const auto &folder = m_Folder->GetItem();
        if(folder == observable)
        {
            const auto &resources = folder->GetResources();
            for(auto && [filename, resource]: resources)
            {
                if(resource && resource->m_Path->Get() == m_PluginPath)
                {
                    SetVarFromValue<ResourceVar>(m_EntryPoint->GetKey(), resource);
                    OnEnable();
                }
            }
        }
    }

    void Module::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        ma::Module::UpdateObservation(observable, data);

        const auto &folder = m_Folder->GetItem();
        if(folder == observable)
        {
            const auto &actions = data.GetOrdered();
            for(const auto &action : actions)
            {
                // La construction n'est pas fini, la ressource ne sera pas observable.
                // Nous attendrons donc l'appel à ms_ConstructionEnd
                if(action.first != ma::Item::Key::Obs::ms_AddItemBegin &&
                   action.first != ma::Item::Key::Obs::ms_AddItemEnd &&
                   action.first != ma::Item::Key::Obs::ms_ConstructionEnd)
                {
                    if(auto opt = folder->HasItemOpt<Resource>(action.second, SearchMod::LOCAL))
                    {
                        auto resource = opt.value();
                        if(resource && resource->m_Path->Get() == m_PluginPath)
                        {
                            if(action.first == ma::Folder::Key::Obs::ms_ConstructionEnd)
                            {
                                SetVarFromValue<ResourceVar>(m_EntryPoint->GetKey(), resource);

                                // Deuxième appel à Load mais cette fois-ci les variables pointes sur des éléments.
                                OnEnable();
                            }
                            else if(action.first == ma::Item::Key::Obs::ms_RemoveItemBegin)
                            {
                                OnDisable();
                            }
                        }
                    }
                }
            }
        }
    }

    void Module::EndObservation(const ObservablePtr &observable)
    {
        ma::Module::EndObservation(observable);
    }

    bool Module::Load()
    {
        MA_ASSERT(m_EntryPoint, L"m_EntryPoint est nul.", std::logic_error);

        const auto resource = m_EntryPoint->GetItem();

        // Le dossier est observé, dès que celui-ci se voit ajouter ses ressources, alors m_EntryPoint pointera sur la
        // ressource et cette fonction Load sera appelée.
        if(resource && ma::Module::Load())
        {
            const auto &plugin_name = m_Name->toString();

            const std::filesystem::path path = resource->m_Path->toString();

            // https://docs.python.org/3/c-api/init_config.html#c.PyConfig.module_search_paths_set

            // \todo Charger le module.

            // As always, first grab the GIL
            wxPyBlock_t blocked = wxPyBeginBlockThreads();
            {   // Portée pour s'assurer que les object statique soient bien supprimés.

                PyObject* py_wx_module = PyImport_ImportModule("wx");
                PyObject* py_wx_aui_module = PyImport_ImportModule("wx.aui");

                // avant l'appel à wxPyEndBlockThreads
                PyObject* globals = PyDict_New();
        #if PY_MAJOR_VERSION >= 3
                PyObject* builtins = PyImport_ImportModule("builtins");
        #else
                PyObject* builtins = PyImport_ImportModule("__builtin__");
        #endif

                wxASSERT(builtins);
                if(builtins)
                {
                    // Cette fonction plante si builtins est null https://bugs.python.org/issue5627
                    PyDict_SetItemString(globals, "__builtins__", builtins);
                    Py_DECREF(builtins);
                }

                CallPyFunction(plugin_name, path, L"py_start_plugin");

                auto decref = [](PyObject* inPyObj)
                {
                    if(inPyObj)
                        Py_DECREF(inPyObj);
                    else
                        PyErr_Print(); // Was there an exception?
                };

                decref(py_wx_module);
                decref(py_wx_aui_module);
                decref(globals);

                // Finally, after all Python stuff is done, release the GIL
            }
            wxPyEndBlockThreads(blocked);

        }

        return false;
    }

    bool Module::UnLoad()
    {
        MA_ASSERT(m_EntryPoint, L"m_EntryPoint est nul.", std::logic_error);

        const auto resource = m_EntryPoint->GetItem();

        // Le dossier est observé, dès que celui-ci se voit ajouter ses ressources, alors m_EntryPoint pointera sur la
        // ressource et cette fonction Load sera appelée.
        if(resource && ma::Module::UnLoad())
        {
            const auto &plugin_name = m_Name->toString();

            const std::filesystem::path path = resource->m_Path->toString();

            // https://docs.python.org/3/c-api/init_config.html#c.PyConfig.module_search_paths_set

            // \todo Charger le module.

            // As always, first grab the GIL
            wxPyBlock_t blocked = wxPyBeginBlockThreads();
            {   // Portée pour s'assurer que les object statique soient bien supprimés.

                PyObject* py_wx_module = PyImport_ImportModule("wx");
                PyObject* py_wx_aui_module = PyImport_ImportModule("wx.aui");

                // avant l'appel à wxPyEndBlockThreads
                PyObject* globals = PyDict_New();
        #if PY_MAJOR_VERSION >= 3
                PyObject* builtins = PyImport_ImportModule("builtins");
        #else
                PyObject* builtins = PyImport_ImportModule("__builtin__");
        #endif

                wxASSERT(builtins);
                if(builtins)
                {
                    // Cette fonction plante si builtins est null https://bugs.python.org/issue5627
                    PyDict_SetItemString(globals, "__builtins__", builtins);
                    Py_DECREF(builtins);
                }

                CallPyFunction(plugin_name, path, L"py_stop_plugin");

                if(std::filesystem::exists(path))
                {
                    PyObject* sys_paths = PySys_GetObject("path");
                    const auto parent_path = path.parent_path();

                    bool find = false;
                    for (Py_ssize_t i{PyList_Size(sys_paths)}; i-- && !find;)
                    {
                         auto* py_path = PyUnicode_AsUTF8(PyList_GetItem(sys_paths, i));
                         const std::filesystem::path sys_path = ma::to_wstring(py_path);
                         find = parent_path == sys_path;

                         // Si on trouve le chemin, on le supprime.
                         if(find)
                             PyList_SetSlice(sys_paths, i, i + 1u, nullptr);
                         Py_DECREF(py_path);
                    }
                }

                auto decref = [](PyObject* inPyObj)
                {
                    if(inPyObj)
                        Py_DECREF(inPyObj);
                    else
                        MA_WARNING(false, L"inPyObj est null.\n" + GetErrorMessageDeprecated());
                };

                decref(py_wx_module);
                decref(py_wx_aui_module);
                decref(globals);

                // Finally, after all Python stuff is done, release the GIL
            }
            wxPyEndBlockThreads(blocked);

        }

        return false;
    }

    const TypeInfo &Module::GetpyModuleTypeInfo()
    {
        const auto module_typeinfo = ma::Module::GetModuleTypeInfo();
        // clang-format off
        static const TypeInfo info =
        {
            GetpyModuleClassName(),
            L"La classe « ma::py::Module » permet de charger et décharger un module écrit en python3.",
            module_typeinfo.MergeDatas(
            {
                {
                    TypeInfo::Key::GetWhiteListParentsData(),
                    {
                        ModuleManager::GetModuleManagerClassName()
                    }
                },
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            })
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Module, py, Module)

    // ModuleCMaker // —————————————————————————————————————————————————————————————————————————————————————————————————
    ModuleMaker::ModuleMaker(const Item::ConstructorParameters &in_params):
    ma::ModuleMaker(in_params, GetDataClassName(), GetClientClassName())
    {}

    ModuleMaker::~ModuleMaker() = default;

    DataMakerPtr ModuleMaker::Clone(const Item::key_type& key) const
    {
        return ma::Item::CreateItem<ModuleMaker>({GetParentKey(), true, m_CanBeDeleteOnLoad, key});
    }

    const std::initializer_list<std::wstring> &ModuleMaker::GetExtensions()
    {
        static const auto result = {L".py"s};
        return result;
    }

    std::optional<std::filesystem::path> ModuleMaker::Match(const std::filesystem::directory_entry &dir_entry) const
    {
        if(auto opt = m_ModuleManager->IsValidModuleFolderName(dir_entry))
        {
            // const auto &module_name = opt.value();
            const auto &folder_path = dir_entry.path();
            const auto folder_name = folder_path.stem().wstring();

            const auto plateforme_extensions = GetExtensions();

            // Chemin le plus probable
            auto plugin_path = folder_path / (folder_name + *plateforme_extensions.begin());

            if(!std::filesystem::exists(plugin_path))
            {
                for(auto it = plateforme_extensions.begin();
                    it != plateforme_extensions.end() && !std::filesystem::exists(plugin_path);
                    ++it)
                {
                    plugin_path = folder_path / (folder_name + *it);
                }
            }

            if(std::filesystem::exists(plugin_path))
                return plugin_path;
        }

        return {};
    }

    ma::DataPtr ModuleMaker::CreateData(ContextLinkPtr &link,
                                        ma::ItemPtr client,
                                        const ma::Item::CreateParameters &params) const
    {
        auto folder = std::dynamic_pointer_cast<ma::Folder>(client);
        MA_ASSERT(folder, L"folder est nul.", std::invalid_argument);

        const std::filesystem::directory_entry dir_entry{folder->m_Path->Get()};

        MA_ASSERT(m_ModuleManager->IsValidModuleFolderName(dir_entry).has_value(),
                  L"Ce dossier ne possède pas un nom valide. Normalement cela à lieu en amont via la fonction Match.",
                  std::logic_error);

        return ma::Item::CreateItem<Module>(params, link, folder, GetPtr<ModuleMaker>());
    }

    const ma::TypeInfo &ModuleMaker::GetFactoryTypeInfo() const
    {
        return Module::GetpyModuleTypeInfo();
    }

    std::wstring ModuleMaker::GetDataClassName()
    {
        return Module::GetpyModuleClassName();
    }

    std::wstring ModuleMaker::GetClientClassName()
    {
        return ma::Folder::GetFolderClassName();
    }

    const TypeInfo &ModuleMaker::GetpyModuleMakerTypeInfo()
    {
        const auto maker_typeinfo = ModuleMaker::GetModuleMakerTypeInfo();
        // clang-format off
        static const TypeInfo info =
        {
            GetpyModuleMakerClassName(),
            L"La classe « ma::py::ModuleMaker » permet de créer un élément « ma::py::Module », si le dossier est um "
            L"module avec un fichier « NOM_DU_DOSSIER.py ».",
            maker_typeinfo.MergeDatas(
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            })
        };
        // clang-format on

        return info;
    }
    M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ModuleMaker, py, ModuleMaker)
} // namespace ma::py
