/// \file Id.py.cpp
/// \author Pontier Pierre
/// \date 2019-12-09
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/Id.py.h"

#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::Id::SetModule(::py::module &in_module)
{
    ::py::class_<ma::Id>(in_module, "Id")
        .def(::py::init<>())
        .def(::py::init<const std::wstring &>())
        .def("GetKey", &ma::Id::GetKey)
        .def("IsValid", static_cast<bool (ma::Id::*)() const>(&ma::Id::IsValid))
        .def("__repr__",
            [](const ma::Id &id)
            {
                return std::wstring(L"<ma.Id( '") + id.GetKey() + std::wstring(L"' )>");
            })
        .def_static("fs_GetStrLenght", [](std::wstring::size_type){return ma::Id::GetStrLength();})
        .def_static("fs_IsUnique", [](const ma::Id::key_type& key){return ma::Id::IsUnique(key);})
        .def_static("fs_ClearKeys", [](){return ma::Id::ClearKeys();})
        /// Le préfixe fs_ (fonction statique) est là pour éviter ça :
        /// «ImportError: overloading a method with both static and instance
        /// methods is not supported; compile in debug mode for more details»
        /// https://github.com/pybind/pybind11/commit/d355f2fcca2afc852aef40f96bfd40ee0b4712b0
        .def_static("fs_IsValid", static_cast<bool (*)(const ma::Id::key_type&)>(&ma::Id::IsValid), ::py::arg("key"));
}

