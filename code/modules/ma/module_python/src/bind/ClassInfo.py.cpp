/// \file ClassInfo.py.cpp
/// \author Pontier Pierre
/// \date 2020-02-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/ClassInfo.py.h"

#include <pybind11/operators.h>
#include "ma/Ma.h"
#include <sstream>

using namespace pybind11::literals;

void ma::py::ClassInfo::SetModule(::py::module &in_module)
{
    ::py::class_<ma::ClassInfo, ma::ClassInfoPtr, ma::Item>(in_module, "ClassInfo")
        .def_static("fs_GetClassInfoClassName", &ma::ClassInfo::GetClassInfoClassName)
        .def_readwrite("m_Name", &ma::ClassInfo::m_Name)
        .def("GetTypeInfo", &ma::ClassInfo::GetTypeInfo)
        .def("__repr__",
            [](const ma::ClassInfoPtr &cls_info)
            {
                MA_ASSERT(cls_info,
                          L"cls_info est nulle.",
                          L"ma::py::ClassInfo::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ClassInfo( '") + cls_info->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::ClassInfoManager, ma::ClassInfoManagerPtr, ma::Item>(in_module, "ClassInfoManager")
        .def_static("fs_GetClassInfoManagerClassName", &ma::ClassInfoManager::GetClassInfoManagerClassName)
        .def_static("fs_IsInherit", static_cast<bool (*)(const ma::ObservablePtr&, const std::wstring&)>(&ma::ClassInfoManager::IsInherit), ::py::arg("observable"), ::py::arg("class_name"))
        .def("BuildHierarchies", &ma::ClassInfoManager::BuildHierarchies)
        .def("GetTypeInfo", &ma::ClassInfoManager::GetTypeInfo)
        .def("__repr__",
            [](const ma::ClassInfoManagerPtr &cls_mgr)
            {
                MA_ASSERT(cls_mgr,
                          L"cls_mgr est nulle.",
                          L"ma::py::ClassInfoManager::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ClassInfoManager( '") + cls_mgr->GetKey() + std::wstring(L"' )>");
            });
}

