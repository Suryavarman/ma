/// \file Quaternion.py.cpp
/// \author Pontier Pierre
/// \date 2022/04/14
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#define EIGEN_VAR_QUATERNION_PY(in_M_quaternion_type) \
::py::class_<ma::Eigen::in_M_quaternion_type##Var, ma::Eigen::in_M_quaternion_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_quaternion_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_quaternion_type##Var>(key); \
    }), ::py::arg("item_key"), ::py::arg("key")) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_quaternion_type& in_quaternion) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_quaternion_type##Var>(key, in_quaternion); \
    }), ::py::arg("item_key"), ::py::arg("key"), ::py::arg("quaternion")) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::Eigen::in_M_quaternion_type::Scalar w, ma::Eigen::in_M_quaternion_type::Scalar x, ma::Eigen::in_M_quaternion_type::Scalar y, ma::Eigen::in_M_quaternion_type::Scalar z) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_quaternion_type##Var>(key, ma::Eigen::in_M_quaternion_type(w, x, y, z)); \
    }), ::py::arg("item_key"), ::py::arg("key"), ::py::arg("w"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z")) \
    .def("toString", &ma::Eigen::in_M_quaternion_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_quaternion_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_quaternion_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_quaternion_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_quaternion_type##VarPtr self, const ma::Eigen::in_M_quaternion_type& in_quaternion) \
        { \
            self->Set(in_quaternion); \
        }, ::py::arg("quaternion")) \
    .def("Set", \
        [](ma::Eigen::in_M_quaternion_type##VarPtr self, ma::Eigen::in_M_quaternion_type::Scalar w, ma::Eigen::in_M_quaternion_type::Scalar x, ma::Eigen::in_M_quaternion_type::Scalar y, ma::Eigen::in_M_quaternion_type::Scalar z) \
        { \
            ma::Eigen::in_M_quaternion_type quat{w, x, y, z}; \
            self->Set(quat); \
        }, ::py::arg("w"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z")) \
    .def("Get", \
        [](ma::Eigen::in_M_quaternion_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_quaternion_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_quaternion_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Quaternion::"s + ma::to_wstring(#in_M_quaternion_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_quaternion_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_quaternion_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_quaternion_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_quaternion_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_quaternion_type::Scalar w, ma::Eigen::in_M_quaternion_type::Scalar x, ma::Eigen::in_M_quaternion_type::Scalar y, ma::Eigen::in_M_quaternion_type::Scalar z) \
        { \
            return self->AddVar<ma::Eigen::in_M_quaternion_type##Var>(key, ma::Eigen::in_M_quaternion_type(w, x, y, z)); \
        }, ::py::arg("key"), ::py::arg("w"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z")) \
    .def("AddEigen" #in_M_quaternion_type "Var", static_cast<ma::Eigen::in_M_quaternion_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_quaternion_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_quaternion_type##Var>), ::py::arg("key"), ::py::arg("quaternion")) \
    .def("AddReadOnlyEigen" #in_M_quaternion_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_quaternion_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_quaternion_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_quaternion_type::Scalar w, ma::Eigen::in_M_quaternion_type::Scalar x, ma::Eigen::in_M_quaternion_type::Scalar y, ma::Eigen::in_M_quaternion_type::Scalar z) \
        { \
            return self->AddReadOnlyVar<ma::Eigen::in_M_quaternion_type##Var>(key, ma::Eigen::in_M_quaternion_type(w, x, y, z)); \
        }, ::py::arg("key"), ::py::arg("w"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z")) \
    .def("AddReadOnlyEigen" #in_M_quaternion_type "Var", static_cast<ma::Eigen::in_M_quaternion_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_quaternion_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_quaternion_type##Var>), ::py::arg("key"), ::py::arg("quaternion"));

// l'ordre est important, sinon l'Eigen du système est pris en compte et non
// celui du dossier « dependencies »
#include "Py/bind/wx.py.h"
#include "ma/Ma.h"
#include "Py/bind/eigen/Quaternion.py.h"

#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <sstream>

using namespace pybind11::literals;

void ma::py::Eigen::Quaternion::SetModule(::py::module &in_ma_module, ::py::module &in_ma_eigen_module, ma::py::Item::value_type& in_class_item)
{
    //EIGEN_VAR_QUATERNION_PY(Quaternioni)
    //EIGEN_VAR_QUATERNION_PY(Quaternionu)
    //EIGEN_VAR_QUATERNION_PY(Quaternionl)
    //EIGEN_VAR_QUATERNION_PY(Quaternionll)
    EIGEN_VAR_QUATERNION_PY(Quaternionf)
    EIGEN_VAR_QUATERNION_PY(Quaterniond)
    EIGEN_VAR_QUATERNION_PY(Quaternionld)
}

