/// \file Vector.py.cpp
/// \author Pontier Pierre
/// \date 2022/04/14
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#define EIGEN_VAR_VECTOR2D_PY(in_M_vector_type) \
::py::class_<ma::Eigen::in_M_vector_type##Var, ma::Eigen::in_M_vector_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_vector_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_vector_type& in_vector) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key, in_vector); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y)); \
    })) \
    .def("toString", &ma::Eigen::in_M_vector_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_vector_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_vector_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_vector_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_vector_type##VarPtr self, const ma::Eigen::in_M_vector_type& in_vector) \
        { \
            self->Set(in_vector); \
        }, ::py::arg("vector")) \
    .def("Set", \
        [](ma::Eigen::in_M_vector_type##VarPtr self, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y) \
        { \
            self->Set({x, y}); \
        }, ::py::arg("x"), ::py::arg("y")) \
    .def("Get", \
        [](ma::Eigen::in_M_vector_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_vector_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_vector_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Vector::"s + ma::to_wstring(#in_M_vector_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_vector_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y)); \
        }, ::py::arg("key"), ::py::arg("x"), ::py::arg("y")) \
    .def("AddEigen" #in_M_vector_type "Var", static_cast<ma::Eigen::in_M_vector_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_vector_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_vector_type##Var>), ::py::arg("key"), ::py::arg("vector")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y) \
        { \
            return self->AddReadOnlyVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y)); \
        }, ::py::arg("key"), ::py::arg("x"), ::py::arg("y")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", static_cast<ma::Eigen::in_M_vector_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_vector_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_vector_type##Var>), ::py::arg("key"), ::py::arg("vector"));

#define EIGEN_VAR_VECTOR3D_PY(in_M_vector_type) \
::py::class_<ma::Eigen::in_M_vector_type##Var, ma::Eigen::in_M_vector_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_vector_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_vector_type& in_vector) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key, in_vector); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y, z)); \
    })) \
    .def("toString", &ma::Eigen::in_M_vector_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_vector_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_vector_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_vector_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_vector_type##VarPtr self, const ma::Eigen::in_M_vector_type& in_vector) \
        { \
            self->Set(in_vector); \
        }, ::py::arg("vector")) \
    .def("Set", \
        [](ma::Eigen::in_M_vector_type##VarPtr self, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z) \
        { \
            self->Set({x, y, z}); \
        }, ::py::arg("x"), ::py::arg("y"), ::py::arg("z")) \
    .def("Get", \
        [](ma::Eigen::in_M_vector_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_vector_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_vector_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Vector::"s + ma::to_wstring(#in_M_vector_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_vector_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y, z)); \
        }, ::py::arg("key"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z")) \
    .def("AddEigen" #in_M_vector_type "Var", static_cast<ma::Eigen::in_M_vector_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_vector_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_vector_type##Var>), ::py::arg("key"), ::py::arg("vector")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z) \
        { \
            return self->AddReadOnlyVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y, z)); \
        }, ::py::arg("key"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", static_cast<ma::Eigen::in_M_vector_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_vector_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_vector_type##Var>), ::py::arg("key"), ::py::arg("vector"));

#define EIGEN_VAR_VECTOR4D_PY(in_M_vector_type) \
::py::class_<ma::Eigen::in_M_vector_type##Var, ma::Eigen::in_M_vector_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_vector_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_vector_type& in_vector) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key, in_vector); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z, ma::Eigen::in_M_vector_type::Scalar w) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y, z, w)); \
    })) \
    .def("toString", &ma::Eigen::in_M_vector_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_vector_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_vector_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_vector_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_vector_type##VarPtr self, const ma::Eigen::in_M_vector_type& in_vector) \
        { \
            self->Set(in_vector); \
        }, ::py::arg("vector")) \
    .def("Set", \
        [](ma::Eigen::in_M_vector_type##VarPtr self, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z, ma::Eigen::in_M_vector_type::Scalar w) \
        { \
            self->Set({x, y, z, w}); \
        }, ::py::arg("x"), ::py::arg("y"), ::py::arg("z"), ::py::arg("w")) \
    .def("Get", \
        [](ma::Eigen::in_M_vector_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_vector_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_vector_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Vector::"s + ma::to_wstring(#in_M_vector_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_vector_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z, ma::Eigen::in_M_vector_type::Scalar w) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y, z, w)); \
        }, ::py::arg("key"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z"), ::py::arg("w")) \
    .def("AddEigen" #in_M_vector_type "Var", static_cast<ma::Eigen::in_M_vector_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_vector_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_vector_type##Var>), ::py::arg("key"), ::py::arg("vector")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_vector_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, ma::Eigen::in_M_vector_type::Scalar x, ma::Eigen::in_M_vector_type::Scalar y, ma::Eigen::in_M_vector_type::Scalar z, ma::Eigen::in_M_vector_type::Scalar w) \
        { \
            return self->AddReadOnlyVar<ma::Eigen::in_M_vector_type##Var>(key, ma::Eigen::in_M_vector_type(x, y, z, w)); \
        }, ::py::arg("key"), ::py::arg("x"), ::py::arg("y"), ::py::arg("z"), ::py::arg("w")) \
    .def("AddReadOnlyEigen" #in_M_vector_type "Var", static_cast<ma::Eigen::in_M_vector_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_vector_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_vector_type##Var>), ::py::arg("key"), ::py::arg("vector"));

// l'ordre est important, sinon l'Eigen du système est pris en compte et non
// celui du dossier « dependencies »
#include "Py/bind/wx.py.h"
#include "ma/Ma.h"
#include "Py/bind/eigen/Vector.py.h"

#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <sstream>

using namespace pybind11::literals;

void ma::py::Eigen::Vector::SetModule(::py::module &in_ma_module, ::py::module &in_ma_eigen_module, ma::py::Item::value_type& in_class_item)
{
    // ne fonctionne pas, essayer de déclarer un tableau numpy
    //
    // typedef ::Eigen::Matrix<int, 1, 2> Vector2i
    // Vector2i  numpy.ndarray[numpy.int32[1,2]]
    //
    // Vector2u  numpy.ndarray[numpy.uint32[1,2]]
    // Vector2l  numpy.ndarray[numpy.int64[1,2]]
    // Vector2ll numpy.ndarray[numpy.int64[1,2]]
    // Vector2f  numpy.ndarray[numpy.float32[1,2]]
    // Vector2d  numpy.ndarray[numpy.float64[1,2]]
    // Vector2ld numpy.ndarray[numpy.longdouble[1,2]]
    //
    // Vector3i
    // Vector3u
    // Vector3l
    // Vector3ll
    // Vector3f
    // Vector3d
    // Vector3ld
    //
    // Vector4i
    // Vector4u
    // Vector4l
    // Vector4ll
    // Vector4f
    // Vector4d
    // Vector4ld
    //
    // Vector2i = typing.NewType("Vector2i", numpy.ndarray[int[1,2]])
    // Traceback (most recent call last):
    // File "<input>", line 1, in <module>
    // TypeError: 'type' object is not subscriptable
    //::py::class_<ma::Eigen::Vector2i>(in_ma_eigen_module, "Vector2i");
    //::py::class_<ma::Eigen::Vector2u>(in_ma_eigen_module, "Vector2u");

    EIGEN_VAR_VECTOR2D_PY(Vector2i)
    EIGEN_VAR_VECTOR2D_PY(Vector2u)
    EIGEN_VAR_VECTOR2D_PY(Vector2l)
    EIGEN_VAR_VECTOR2D_PY(Vector2ll)
    EIGEN_VAR_VECTOR2D_PY(Vector2f)
    EIGEN_VAR_VECTOR2D_PY(Vector2d)
    EIGEN_VAR_VECTOR2D_PY(Vector2ld)

    EIGEN_VAR_VECTOR3D_PY(Vector3i)
    EIGEN_VAR_VECTOR3D_PY(Vector3u)
    EIGEN_VAR_VECTOR3D_PY(Vector3l)
    EIGEN_VAR_VECTOR3D_PY(Vector3ll)
    EIGEN_VAR_VECTOR3D_PY(Vector3f)
    EIGEN_VAR_VECTOR3D_PY(Vector3d)
    EIGEN_VAR_VECTOR3D_PY(Vector3ld)

    EIGEN_VAR_VECTOR4D_PY(Vector4i)
    EIGEN_VAR_VECTOR4D_PY(Vector4u)
    EIGEN_VAR_VECTOR4D_PY(Vector4l)
    EIGEN_VAR_VECTOR4D_PY(Vector4ll)
    EIGEN_VAR_VECTOR4D_PY(Vector4f)
    EIGEN_VAR_VECTOR4D_PY(Vector4d)
    EIGEN_VAR_VECTOR4D_PY(Vector4ld)
}

