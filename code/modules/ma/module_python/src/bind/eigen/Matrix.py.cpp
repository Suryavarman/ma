/// \file Matrix.py.cpp
/// \author Pontier Pierre
/// \date 2022/04/14
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#define EIGEN_VAR_MATRIX_2x2_PY(in_M_matrix_type) \
::py::class_<ma::Eigen::in_M_matrix_type##Var, ma::Eigen::in_M_matrix_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_matrix_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_matrix_type& in_matrix) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_matrix_type##Var>(key, in_matrix); \
    })) \
    .def("toString", &ma::Eigen::in_M_matrix_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_matrix_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_matrix_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_matrix_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_matrix_type##VarPtr self, const ma::Eigen::in_M_matrix_type& in_matrix) \
        { \
            self->Set(in_matrix); \
        }, ::py::arg("matrix")) \
    .def("Get", \
        [](ma::Eigen::in_M_matrix_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_matrix_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_matrix_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Matrix::"s + ma::to_wstring(#in_M_matrix_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_matrix_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_matrix_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_matrix_type "Var", static_cast<ma::Eigen::in_M_matrix_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_matrix_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_matrix_type##Var>), ::py::arg("key"), ::py::arg("matrix")) \
    .def("AddReadOnlyEigen" #in_M_matrix_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_matrix_type "Var", static_cast<ma::Eigen::in_M_matrix_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_matrix_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_matrix_type##Var>), ::py::arg("key"), ::py::arg("matrix"));

#define EIGEN_VAR_MATRIX_3x3_PY(in_M_matrix_type) \
::py::class_<ma::Eigen::in_M_matrix_type##Var, ma::Eigen::in_M_matrix_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_matrix_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_matrix_type& in_matrix) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_matrix_type##Var>(key, in_matrix); \
    })) \
    .def("toString", &ma::Eigen::in_M_matrix_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_matrix_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_matrix_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_matrix_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_matrix_type##VarPtr self, const ma::Eigen::in_M_matrix_type& in_matrix) \
        { \
            self->Set(in_matrix); \
        }, ::py::arg("matrix")) \
    .def("Get", \
        [](ma::Eigen::in_M_matrix_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_matrix_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_matrix_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Matrix::"s + ma::to_wstring(#in_M_matrix_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_matrix_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_matrix_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_matrix_type "Var", static_cast<ma::Eigen::in_M_matrix_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_matrix_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_matrix_type##Var>), ::py::arg("key"), ::py::arg("matrix")) \
    .def("AddReadOnlyEigen" #in_M_matrix_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_matrix_type "Var", static_cast<ma::Eigen::in_M_matrix_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_matrix_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_matrix_type##Var>), ::py::arg("key"), ::py::arg("matrix"));

#define EIGEN_VAR_MATRIX_4x4_PY(in_M_matrix_type) \
::py::class_<ma::Eigen::in_M_matrix_type##Var, ma::Eigen::in_M_matrix_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_matrix_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
    })) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_matrix_type& in_matrix) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_matrix_type##Var>(key, in_matrix); \
    })) \
    .def("toString", &ma::Eigen::in_M_matrix_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_matrix_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_matrix_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_matrix_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_matrix_type##VarPtr self, const ma::Eigen::in_M_matrix_type& in_matrix) \
        { \
            self->Set(in_matrix); \
        }, ::py::arg("matrix")) \
    .def("Get", \
        [](ma::Eigen::in_M_matrix_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_matrix_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_matrix_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Matrix::"s + ma::to_wstring(#in_M_matrix_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_matrix_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_matrix_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_matrix_type "Var", static_cast<ma::Eigen::in_M_matrix_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_matrix_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_matrix_type##Var>), ::py::arg("key"), ::py::arg("matrix")) \
    .def("AddReadOnlyEigen" #in_M_matrix_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_matrix_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_matrix_type "Var", static_cast<ma::Eigen::in_M_matrix_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_matrix_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_matrix_type##Var>), ::py::arg("key"), ::py::arg("matrix"));

// l'ordre est important, sinon l'Eigen du système est pris en compte et non
// celui du dossier « dependencies »
#include "Py/bind/wx.py.h"
#include "ma/Ma.h"
#include "Py/bind/eigen/Matrix.py.h"

#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <sstream>

using namespace pybind11::literals;

void ma::py::Eigen::Matrix::SetModule(::py::module &in_ma_module, ::py::module &in_ma_eigen_module, ma::py::Item::value_type& in_class_item)
{
    EIGEN_VAR_MATRIX_2x2_PY(Matrix22i)
    EIGEN_VAR_MATRIX_2x2_PY(Matrix22u)
    EIGEN_VAR_MATRIX_2x2_PY(Matrix22l)
    EIGEN_VAR_MATRIX_2x2_PY(Matrix22ll)
    EIGEN_VAR_MATRIX_2x2_PY(Matrix22f)
    EIGEN_VAR_MATRIX_2x2_PY(Matrix22d)
    EIGEN_VAR_MATRIX_2x2_PY(Matrix22ld)

    EIGEN_VAR_MATRIX_3x3_PY(Matrix33i)
    EIGEN_VAR_MATRIX_3x3_PY(Matrix33u)
    EIGEN_VAR_MATRIX_3x3_PY(Matrix33l)
    EIGEN_VAR_MATRIX_3x3_PY(Matrix33ll)
    EIGEN_VAR_MATRIX_3x3_PY(Matrix33f)
    EIGEN_VAR_MATRIX_3x3_PY(Matrix33d)
    EIGEN_VAR_MATRIX_3x3_PY(Matrix33ld)

    EIGEN_VAR_MATRIX_4x4_PY(Matrix44i)
    EIGEN_VAR_MATRIX_4x4_PY(Matrix44u)
    EIGEN_VAR_MATRIX_4x4_PY(Matrix44l)
    EIGEN_VAR_MATRIX_4x4_PY(Matrix44ll)
    EIGEN_VAR_MATRIX_4x4_PY(Matrix44f)
    EIGEN_VAR_MATRIX_4x4_PY(Matrix44d)
    EIGEN_VAR_MATRIX_4x4_PY(Matrix44ld)
}

