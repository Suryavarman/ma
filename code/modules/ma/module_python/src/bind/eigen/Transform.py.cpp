/// \file Transform.py.cpp
/// \author Pontier Pierre
/// \date 2022/04/20
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///

#define EIGEN_VAR_TRANSFORM_PY(in_M_transform_type) \
::py::class_<ma::Eigen::in_M_transform_type>(in_ma_eigen_module, #in_M_transform_type) \
    .def(::py::init([]() \
    { \
        auto result = ma::Eigen::in_M_transform_type{}; \
        result.setIdentity(); \
        return result; \
    })) \
    .def(::py::init([](const ma::Eigen::in_M_transform_type::MatrixType& in_matrix) \
    { \
        return ma::Eigen::in_M_transform_type{in_matrix}; \
    })) \
    .def("MakeAffine", &ma::Eigen::in_M_transform_type::makeAffine) \
    .def("matrix", [](const ma::Eigen::in_M_transform_type& self) \
    { \
        return self.matrix(); \
    }) \
    .def("rotate", [](ma::Eigen::in_M_transform_type& self, const ::Eigen::Quaternion<ma::Eigen::in_M_transform_type::Scalar>& quaternion) \
    { \
        return self.rotate(quaternion); \
    }, ::py::arg("quaternion")) \
    .def("scale", [](ma::Eigen::in_M_transform_type& self, const ma::Eigen::in_M_transform_type::Scalar& value) \
    { \
        return self.scale(value); \
    }, ::py::arg("value")) \
    .def("setIdentity", &ma::Eigen::in_M_transform_type::setIdentity) \
    .def("__repr__", [](const ma::Eigen::in_M_transform_type& self) \
    { \
        return L"<ma.eigen."s + ma::to_wstring(#in_M_transform_type) + L"( '"s + ma::toString(self.matrix()) + L"' )>"s; \
    }); \
 \
::py::class_<ma::Eigen::in_M_transform_type##Var, ma::Eigen::in_M_transform_type##VarPtr, ma::Var>(in_ma_eigen_module, #in_M_transform_type "Var") \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_transform_type##Var>(key); \
    }), ::py::arg("item_key"), ::py::arg("key")) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_transform_type& in_transform) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_transform_type##Var>(key, in_transform); \
    }), ::py::arg("item_key"), ::py::arg("key"), ::py::arg("transform")) \
    .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::Eigen::in_M_transform_type::MatrixType& matrix) \
    { \
        auto item = ma::Item::GetItem(item_key); \
        return item->AddVar<ma::Eigen::in_M_transform_type##Var>(key, ma::Eigen::in_M_transform_type(matrix)); \
    }), ::py::arg("item_key"), ::py::arg("key"), ::py::arg("matrix")) \
    .def("toString", &ma::Eigen::in_M_transform_type##Var::toString) \
    .def("fromString", &ma::Eigen::in_M_transform_type##Var::fromString) \
    .def("IsValidString", &ma::Eigen::in_M_transform_type##Var::IsValidString) \
    .def("Reset", &ma::Eigen::in_M_transform_type##Var::Reset) \
    .def("Set", \
        [](ma::Eigen::in_M_transform_type##VarPtr self, const ma::Eigen::in_M_transform_type& in_transform) \
        { \
            self->Set(in_transform); \
        }, ::py::arg("transform")) \
    .def("Set", \
        [](ma::Eigen::in_M_transform_type##VarPtr self, const ma::Eigen::in_M_transform_type::MatrixType& matrix) \
        { \
            ma::Eigen::in_M_transform_type transform{matrix}; \
            self->Set(transform); \
        }, ::py::arg("matrix")) \
    .def("Get", \
        [](ma::Eigen::in_M_transform_type##VarPtr self) \
        { \
            return self->Get(); \
        }) \
    .def("GetTypeInfo", &ma::Eigen::in_M_transform_type##Var::GetTypeInfo) \
    .def("__repr__", \
        [](const ma::Eigen::in_M_transform_type##VarPtr &var) \
        { \
            MA_ASSERT(var, \
                      L"var est nulle.", \
                      L"ma::py::Eigen::Transform::"s + ma::to_wstring(#in_M_transform_type) + L"Var::__repr__"s, \
                      std::invalid_argument); \
 \
            return L"<ma.eigen."s + ma::to_wstring(#in_M_transform_type) + L"Var( '"s + var->GetKey() + std::wstring(L"' )>"); \
        }); \
 \
in_class_item \
    .def("AddEigen" #in_M_transform_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_transform_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddEigen" #in_M_transform_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, const ma::Eigen::in_M_transform_type::MatrixType& matrix) \
        { \
            return self->AddVar<ma::Eigen::in_M_transform_type##Var>(key, ma::Eigen::in_M_transform_type(matrix)); \
        }, ::py::arg("key"), ::py::arg("matrix")) \
    .def("AddEigen" #in_M_transform_type "Var", static_cast<ma::Eigen::in_M_transform_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_transform_type&)>(&ma::Item::AddVar<ma::Eigen::in_M_transform_type##Var>), ::py::arg("key"), ::py::arg("transform")) \
    .def("AddReadOnlyEigen" #in_M_transform_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key) \
        { \
            return self->AddVar<ma::Eigen::in_M_transform_type##Var>(key); \
        }, ::py::arg("key")) \
    .def("AddReadOnlyEigen" #in_M_transform_type "Var", \
        [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, const ma::Eigen::in_M_transform_type::MatrixType& matrix) \
        { \
            return self->AddReadOnlyVar<ma::Eigen::in_M_transform_type##Var>(key, ma::Eigen::in_M_transform_type(matrix)); \
        }, ::py::arg("key"), ::py::arg("matrix")) \
    .def("AddReadOnlyEigen" #in_M_transform_type "Var", static_cast<ma::Eigen::in_M_transform_type##VarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::Eigen::in_M_transform_type&)>(&ma::Item::AddReadOnlyVar<ma::Eigen::in_M_transform_type##Var>), ::py::arg("key"), ::py::arg("transform"));

// l'ordre est important, sinon l'Eigen du système est pris en compte et non
// celui du dossier « dependencies »
#include "Py/bind/wx.py.h"
#include "ma/Ma.h"
#include "Py/bind/eigen/Transform.py.h"

#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <sstream>

using namespace pybind11::literals;

void ma::py::Eigen::Transform::SetModule(::py::module &in_ma_module, ::py::module &in_ma_eigen_module, ma::py::Item::value_type& in_class_item)
{
    /*
    ::py::class_<ma::Eigen::Transformi>(in_ma_eigen_module, "Transformi")
        .def(::py::init([]()
        {
            return ma::Eigen::Transformi{};
        }))
        .def(::py::init([](const ma::Eigen::Transformi::MatrixType& in_matrix)
        {
            return ma::Eigen::Transformi{in_matrix};
        }))
        .def("MakeAffine", &ma::Eigen::Transformi::makeAffine)
        .def("matrix", []()
        {
            return ma::root()->wx->GetToolBar();

        }, ::py::return_value_policy::reference, "Renvoie une copie de la matrice.")
        .def("matrix", [](const ma::Eigen::Transformi& self)
        {
            return self.matrix();
        })
        .def("rotate", [](ma::Eigen::Transformi& self, const ma::Eigen::Quaternioni& quaternion)
        {
            return self.rotate(quaternion);
        }, ::py::arg("quaternion"))
        .def("scale", [](ma::Eigen::Transformi& self, const ma::Eigen::Transformi::Scalar& value)
        {
            return self.scale(value);
        }, ::py::arg("value"))
        .def("setIdentity", &ma::Eigen::Transformi::setIdentity)
        .def("__repr__", [](const ma::Eigen::Transformi& self)
        {
            return L"<ma.eigen.Transformi( '"s + ma::toString(self.matrix()) + L"' )>"s;
        });
    //*/

    //EIGEN_VAR_TRANSFORM_PY(Transformi)
    //EIGEN_VAR_TRANSFORM_PY(Transformu)
    //EIGEN_VAR_TRANSFORM_PY(Transforml)
    //EIGEN_VAR_TRANSFORM_PY(Transformll)
    EIGEN_VAR_TRANSFORM_PY(Transformf)
    EIGEN_VAR_TRANSFORM_PY(Transformd)
    EIGEN_VAR_TRANSFORM_PY(Transformld)
}

