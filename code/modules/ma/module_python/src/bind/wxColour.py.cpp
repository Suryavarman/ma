/// \file wxColour.py.cpp
/// \author Pontier Pierre
/// \date 2020/11/04
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/wx.py.h"
#include "Py/bind/wxColour.py.h"

#include <pybind11/operators.h>
#include <sip.h>
#include <wxPython/wxpy_api.h>

#include <ma/Ma.h>

#include <sstream>

using namespace pybind11::literals;


void ma::py::wxPyColour::SetModule(::py::module &in_module, ::py::module &in_ma_wx_module, ma::py::Item::value_type& inClassItem)
{
    auto py_wx_module = ::py::module::import("wx");

    ::py::class_<ma::wxColourVar, ma::wxColourVarPtr, ma::Var>(in_module, "wxColourVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::wxColourVar>(key);
        }))
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = wxALPHA_OPAQUE)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::wxColourVar>(key, red, green, blue, alpha);
        }))
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const wxColour& value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::wxColourVar>(key, value);
        }))
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, unsigned long colRGB)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::wxColourVar>(key, colRGB);
        }))
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const std::string &colourName)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::wxColourVar>(key, wxString(colourName));
        }))
        .def("toString", &ma::wxColourVar::toString)
        .def("fromString", &ma::wxColourVar::fromString)
        .def("IsValidString", &ma::wxColourVar::IsValidString)
        .def("Reset", &ma::wxColourVar::Reset)
        .def("Set",
            [](ma::wxColourVarPtr self, unsigned int red, unsigned int green, unsigned int blue, float alpha)
            {
                self->Set(red, green, blue, alpha);
            }, ::py::arg("red"), ::py::arg("green"), ::py::arg("blue"), ::py::arg("alpha") = 1.f)
        .def("Set",
            [](ma::wxColourVarPtr self, unsigned long colRGB)
            {
                self->Set(colRGB);
            }, ::py::arg("colRGB"))
        .def("Set",
            [](ma::wxColourVarPtr self, const std::string &color_name)
            {
                self->Set(color_name);
            }, ::py::arg("color_name"))
        .def("Set",
            [](ma::wxColourVarPtr self, const wxColour& colour)
            {
                self->Set(colour);
            }, ::py::arg("colour"))
        .def("Get",
            [](ma::wxColourVarPtr self)
            {
                return self->Get();
            })
        .def("GetTypeInfo", &ma::wxColourVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::wxColourVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::wxPyColour::wxColourVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.wxColourVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    inClassItem
        .def("AddwxColour",
            [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key)
            {
                return self->AddVar<ma::wxColourVar>(key, *wxWHITE);
            }, ::py::arg("key"))
        .def("AddwxColour",
            [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, unsigned char red, unsigned char green, unsigned char blue, float alpha)
            {
                return self->AddVar<ma::wxColourVar>(key, red, green, blue, static_cast<unsigned char>(alpha * 255.f));
            }, ::py::arg("key"), ::py::arg("red"), ::py::arg("green"), ::py::arg("blue"), ::py::arg("alpha"))
        .def("AddwxColour", static_cast<ma::wxColourVarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const wxColour&)>(&ma::Item::AddVar<ma::wxColourVar>), ::py::arg("key"), ::py::arg("value"))
//        .def("AddwxColour",
//            [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, unsigned long colRGB)
//            {
//                return self->AddVar<ma::wxColourVar>(key, colRGB);
//            }, ::py::arg("key"), ::py::arg("colRGB"))
        .def("AddwxColour", static_cast<ma::wxColourVarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const std::wstring&)>(&ma::Item::AddVar<ma::wxColourVar>), ::py::arg("key"), ::py::arg("colourName"))

        .def("AddReadOnlywxColour",
            [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key)
            {
                return self->AddVar<ma::wxColourVar>(key, *wxWHITE);
            }, ::py::arg("key"))
        .def("AddReadOnlywxColour",
            [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
            {
                return self->AddReadOnlyVar<ma::wxColourVar>(key, red, green, blue, alpha);
            }, ::py::arg("key"), ::py::arg("red"), ::py::arg("green"), ::py::arg("blue"), ::py::arg("alpha"))
        .def("AddReadOnlywxColour", static_cast<ma::wxColourVarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const wxColour&)>(&ma::Item::AddReadOnlyVar<ma::wxColourVar>), ::py::arg("key"), ::py::arg("value"))
//        .def("AddReadOnlywxColour",
//            [](const ma::ItemPtr &self, const ma::VarsMap::key_type& key, unsigned long colRGB)
//            {
//                return self->AddReadOnlyVar<ma::wxColourVar>(key, colRGB);
//            }, ::py::arg("key"), ::py::arg("colRGB"))
        .def("AddReadOnlywxColour", static_cast<ma::wxColourVarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const std::wstring&)>(&ma::Item::AddReadOnlyVar<ma::wxColourVar>), ::py::arg("key"), ::py::arg("colourName"));


    auto wx_python_module = ::py::module::import("wx");

    ::py::class_<ma::wx::ColourPanel, std::unique_ptr<ma::wx::ColourPanel, ::py::nodelete>>(in_ma_wx_module, "VarwxColour")
        .def(::py::init<>
             ([](ma::wxColourVarPtr var, ::py::object inWindow)
             {
                 wxWindow *window = ma::py::wxLoad<wxWindow>(inWindow.release(), "wxWindow");
                 wxASSERT(window);

                 return new ma::wx::ColourPanel(var, window);
             }),
             ::py::arg("var"), ::py::arg("window"))

        .def("getParentClass",
             [](const ma::wx::ColourPanel& self)
             {
                return static_cast<wxPanel*>(const_cast<ma::wx::ColourPanel*>(&self));
             },
             ::py::call_guard<::py::gil_scoped_release>(),
             ::py::return_value_policy::reference,
             "Retourne la classe wxPanel parente de ma::wxVarwxColour.");
}

