/// \file wxTreeItems.py.cpp
/// \author Pontier Pierre
/// \date 2019/12/09
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/wx.py.h"
#include "Py/bind/wxTreeItems.py.h"


#include <pybind11/operators.h>
#include <sip.h>
#include <wxPython/wxpy_api.h>

#include <ma/Ma.h>

#include <sstream>

using namespace pybind11::literals;

void ma::py::wxTreeItems::SetModule(::py::module &in_module)
{
    auto py_wx_module = ::py::module::import("wx");

    ::py::class_<ma::wx::TreeItems, std::unique_ptr<ma::wx::TreeItems, ::py::nodelete>>(in_module, "TreeItems")
        .def(::py::init<>
             ([](::py::object inWindow)
             {
                 wxWindow *window = ma::py::wxLoad<wxWindow>(inWindow.release(), "wxWindow");
                 wxASSERT(window);

                 return new ma::wx::TreeItems(window);
             }),
             ::py::arg("inWindow"))

        .def("getParentClass",
             [](const ma::wx::TreeItems& self)
             {
                return static_cast<wxPanel*>(const_cast<ma::wx::TreeItems*>(&self));
             },
             ::py::call_guard<::py::gil_scoped_release>(),
             ::py::return_value_policy::reference,
             "Retourne la classe wxPanel parente de ma::wx::TreeItems.");
}
