/// \file Pybind11Modules.cpp
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <pybind11/operators.h>
#include <ma/Version.h>
#include <sstream>

#include "Py/bind/Info.py.h"
#include "Py/bind/Id.py.h"
#include "Py/bind/Observer.py.h"
#include "Py/bind/Item.py.h"
#include "Py/bind/Var.py.h"
#include "Py/bind/ClassInfo.py.h"
#include "Py/bind/Module.py.h"
#include "Py/bind/ObservationManager.py.h"
#include "Py/bind/RTTI.py.h"
#include "Py/bind/Root.py.h"
#include "Py/bind/wx/View.py.h"
#include "Py/bind/wxColour.py.h"
#include "Py/bind/wxAccess.py.h"
#include "Py/bind/wxTreeItems.py.h"
#include "Py/bind/eigen/Matrix.py.h"
#include "Py/bind/eigen/Quaternion.py.h"
#include "Py/bind/eigen/Transform.py.h"
#include "Py/bind/eigen/Vector.py.h"

namespace py = pybind11;
using namespace pybind11::literals;

PYBIND11_MODULE(ma, ma_module)
{
    ma_module.doc() = R"pbdoc(
        L'api de Maçon de l'espace est accessible via « import ma ».
        C'est un module qui permet de manipuler l'éditeur graphique de
        «Maçon de l'espace» via python et wxPython.
    )pbdoc";

    ::py::module ma_wx_module = ma_module.def_submodule("wx", "Accès l'interface graphique de wxWidgets.");
    ma_wx_module.doc() = R"pbdoc(
        ma.wx sous module. plugin
        -----------------------

        .. currentmodule:: ma.wx

        .. autosummary::
           :toctree: _generate
    )pbdoc";

    ::py::module ma_eigen_module = ma_module.def_submodule("eigen", "Accès aux objets mathématiques");
    ma_eigen_module.doc() = R"pbdoc(
        ma.eigen sous module. plugin
        -----------------------

        .. currentmodule:: ma.eigen

        .. autosummary::
           :toctree: _generate
    )pbdoc";

    ma::py::wxAccess::SetModule(ma_wx_module);
    ma::py::wxTreeItems::SetModule(ma_wx_module);
    ma::py::Id::SetModule(ma_module);
    ma::py::Info::SetModule(ma_module);
    ma::py::Observer::SetModule(ma_module);

    auto py_item = ma::py::Item::SetModule(ma_module);
    ma::py::Var::SetModule(ma_module, py_item);
    ma::py::wxPyColour::SetModule(ma_module, ma_wx_module, py_item);
    ma::py::ClassInfo::SetModule(ma_module);
    //ma::py::Module::SetModule(ma_module); // Plante
    ma::py::ObservationManager::SetModule(ma_module);
    ma::py::RTTI::SetModule(ma_module);
    ma::py::Root::SetModule(ma_module);
    ma::py::wx::View::SetModule(ma_wx_module);

    ma::py::Eigen::Matrix::SetModule(ma_module, ma_eigen_module, py_item);
    ma::py::Eigen::Quaternion::SetModule(ma_module, ma_eigen_module, py_item);
    ma::py::Eigen::Transform::SetModule(ma_module, ma_eigen_module, py_item);
    ma::py::Eigen::Vector::SetModule(ma_module, ma_eigen_module, py_item);

    ma_module.attr("__version__") = MA_VERSION_STRING;
}



