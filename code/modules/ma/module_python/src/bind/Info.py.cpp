/// \file Info.py.cpp
/// \author Pontier Pierre
/// \date 2020-02-18
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/Info.py.h"

#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::Info::SetModule(::py::module &in_module)
{
    ::py::class_<ma::TypeInfo>(in_module, "TypeInfo")
        .def(::py::init<>())
        .def_static("fs_GetTypeInfo", &ma::TypeInfo::GetTypeInfo)
        .def_readwrite("m_ClassName", &ma::TypeInfo::m_ClassName)
        .def_readwrite("m_Description", &ma::TypeInfo::m_Description)
        .def_readwrite("m_Data", &ma::TypeInfo::m_Data)
        .def("__repr__",
            [](const ma::TypeInfo &info)
            {
                return std::string("<ma.TypeInfo( '") + info.m_ClassName + std::string("' )>");
            });
        //.def_static("AddClassHierarchies", [](const ma::TypeInfo::key_type& key){return ma::Id::IsUnique();})
        //.def_static("GetClassHierarchies", [](std::string::size_type){return ma::Id::GetStrLength();})
}

