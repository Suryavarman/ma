/// \file wx.py.cpp
/// \author Pontier Pierre
/// \date 2019-08-02
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/wx.py.h"

wxString ma::py::wxLoad(pybind11::handle src)
{
    wxASSERT(src);

    // As always, first grab the GIL
    wxPyBlock_t blocked = ma::py::wxPyBeginBlockThreads();

    /* Extract PyObject from handle */
    PyObject *source = src.ptr();
    wxString obj = Py2wxString(source);

    // Finally, after all Python stuff is done, release the GIL
    ma::py::wxPyEndBlockThreads(blocked);

    return obj;
}

const wxValidator& ma::py::GetDefaultValidator()
{
    static const wxValidator default_validatior;
    return default_validatior;
}
