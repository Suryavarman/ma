/// \file Module.py.cpp
/// \author Pontier Pierre
/// \date 2023-11-15
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/Module.py.h"

#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::Module::SetModule(::py::module &in_module)
{
    class PyModuleMaker : public ma::ModuleMaker
    {
        public:
            /* Inherit the constructors */
            using ma::ModuleMaker::ModuleMaker;

            /* Trampoline (need one for each virtual function) */
            std::optional<std::filesystem::path> Match(const std::filesystem::directory_entry &dir_entry) const override
            {
                PYBIND11_OVERLOAD_PURE(
                    std::optional<std::filesystem::path>, /* Return type */
                    ma::ModuleMaker,                      /* Parent class */
                    Match                                 /* Name of function in C++ (must match Python name) */
                );
            }
    };

    class ModuleMakerPublicist : public ma::ModuleMaker
    {
        public:
            using ma::ModuleMaker::Match;
    };

    /// \see https://stackoverflow.com/questions/797771/python-protected-attributes
    auto py_maker = ::py::class_<ma::ModuleMaker, PyModuleMaker, ma::ModuleMakerPtr, ma::DataMaker>(in_module, "ModuleMaker")
                        .def("_Match", &ModuleMakerPublicist::Match)
                        .def("__repr__",
                            [](const ma::ModuleMakerPtr& maker)
                            {
                                MA_ASSERT(maker,
                                          L"maker est nul.",
                                          L"ma::py::Module::ModuleMaker::__repr__",
                                          std::invalid_argument);

                                return L"<ma.ModuleMaker( '"s + maker->GetKey() + L"' )>"s;
                            });
}
