/// \file Var.py.cpp
/// \author Pontier Pierre
/// \date 2020-02-18
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/Var.py.h"

#include <pybind11/operators.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <pybind11/functional.h>
#include <pybind11/chrono.h>
#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::Var::SetModule(::py::module &in_module, ma::py::Item::value_type& in_class_item)
{
    // https://pybind11.readthedocs.io/en/master/advanced/classes.html#operator-overloading
    // https://pybind11.readthedocs.io/en/stable/advanced/smart_ptrs.html
    class PyVar : public ma::Var
    {
        public:
            /* Inherit the constructors */
            using ma::Var::Var;

            /* Trampoline (need one for each virtual function) */
            std::wstring toString() const override
            {
                PYBIND11_OVERLOAD_PURE(
                    std::wstring, /* Return type */
                    ma::Var,    /* Parent class */
                    toString     /* Name of function in C++ (must match Python name) */
                );

//                PYBIND11_OVERLOAD_PURE_NAME(
//                  std::string, // Return type (ret_type)
//                  ma::Var,      // Parent class (cname)
//                  toString,    // Name of function in C++ (name)
//                  "__str__",   // Name of method in Python (fn)
//                );
            }

            void fromString(const std::wstring& value) override
            {
                PYBIND11_OVERLOAD_PURE(
                    void,       /* Return type */
                    ma::Var,   /* Parent class */
                    fromString, /* Name of function in C++ (must match Python name) */
                    value       /* Argument(s) */
                );
            }

            bool IsValidString(const std::wstring& value) const override
            {
                PYBIND11_OVERLOAD_PURE(
                    bool,          /* Return type */
                    ma::Var,      /* Parent class */
                    IsValidString, /* Name of function in C++ (must match Python name) */
                    value          /* Argument(s) */
                );
            }

            void Reset() override
            {
                PYBIND11_OVERLOAD_PURE(
                    void,     /* Return type */
                    ma::Var, /* Parent class */
                    Reset     /* Name of function in C++ (must match Python name) */
                );
            }
    };

    auto py_var = ::py::class_<ma::Var, PyVar, ma::VarPtr>(in_module, "Var")
        .def_readonly_static("ms_KeyGUI", &ma::Var::ms_KeyGUI)
        .def_readonly_static("ms_Sep", &ma::Var::ms_Sep)
        .def_readonly("m_Key", &ma::Var::m_Key)
        .def_readonly("m_Dynamic", &ma::Var::m_Dynamic)
        .def_readonly("m_ReadOnly", &ma::Var::m_ReadOnly)
        .def_static("fs_GetKeyValue", &ma::Var::GetKeyValue)
        .def("GetKey", &ma::Var::GetKey)
        .def("GetItemKey", &ma::Var::GetItemKey)
        .def("GetTypeInfo", &ma::Var::GetTypeInfo)
        .def("GetIInfo", &ma::Var::GetIInfo)
        .def("GetIInfoValue", &ma::Var::GetIInfoValue)
        .def("InsertIInfo", &ma::Var::InsertIInfo)
        .def("InsertIInfoKeyValue", &ma::Var::InsertIInfoKeyValue)
        .def("HasIInfoKey", &ma::Var::HasIInfoKey)
        .def("RemoveIInfoKeyValue", &ma::Var::RemoveIInfoKeyValue)
        .def("GetIInfoCount", &ma::Var::GetIInfoCount)
        .def("IsObservable", &ma::Var::IsObservable)
        .def("__repr__",
            [](const ma::VarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::Var::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.Var( '") + var->GetKey() + std::wstring(L"' )>");
            });

    auto key = ::py::class_<ma::Var::Key>(py_var, "Key");

    auto obs = ::py::class_<ma::Var::Key::Obs>(key, "Obs")
                   .def_readonly_static("ms_BeginSetValue", &ma::Var::Key::Obs::ms_BeginSetValue)
                   .def_readonly_static("ms_EndSetValue", &ma::Var::Key::Obs::ms_EndSetValue)
                   .def_readonly_static("ms_BeginSetIInfo", &ma::Var::Key::Obs::ms_BeginSetIInfo)
                   .def_readonly_static("ms_EndSetIInfo", &ma::Var::Key::Obs::ms_EndSetIInfo)
                   .def_readonly_static("ms_BeginSetIInfoValue", &ma::Var::Key::Obs::ms_BeginSetIInfoValue)
                   .def_readonly_static("ms_EndSetIInfoValue", &ma::Var::Key::Obs::ms_EndSetIInfoValue)
                   .def_readonly_static("ms_BeginAddIInfo", &ma::Var::Key::Obs::ms_BeginAddIInfo)
                   .def_readonly_static("ms_EndAddIInfo", &ma::Var::Key::Obs::ms_EndAddIInfo)
                   .def_readonly_static("ms_BeginRemoveIInfo", &ma::Var::Key::Obs::ms_BeginRemoveIInfo)
                   .def_readonly_static("ms_EndRemoveIInfo", &ma::Var::Key::Obs::ms_EndRemoveIInfo);

    auto info = ::py::class_<ma::Var::Key::IInfo>(key, "IInfo")
                   .def_readonly_static("ms_Doc", &ma::Var::Key::IInfo::ms_Doc);

    ::py::class_<ma::DefineVar, ma::DefineVarPtr, ma::Var>(in_module, "DefineVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::DefineVar>(key);
        }))
        .def("toString", &ma::DefineVar::toString)
        .def("fromString", &ma::DefineVar::fromString)
        .def("IsValidString", &ma::DefineVar::IsValidString)
        .def("Reset", &ma::DefineVar::Reset)
        .def("GetTypeInfo", &ma::DefineVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::DefineVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::DefineVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.DefineVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::BoolVar, ma::BoolVarPtr, ma::Var>(in_module, "BoolVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, bool value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::BoolVar>(key, value);
        }))
        .def("toString", &ma::BoolVar::toString)
        .def("fromString", &ma::BoolVar::fromString)
        .def("IsValidString", &ma::BoolVar::IsValidString)
        .def("Reset", &ma::BoolVar::Reset)
        .def("Set", &ma::BoolVar::Set)
        .def("Get", &ma::BoolVar::Get)
        .def("GetTypeInfo", &ma::BoolVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::BoolVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::BoolVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.BoolVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::IntVar, ma::IntVarPtr, ma::Var>(in_module, "IntVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::IntVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::IntVar>(key, value);
        }))
        .def("toString", &ma::IntVar::toString)
        .def("fromString", &ma::IntVar::fromString)
        .def("IsValidString", &ma::IntVar::IsValidString)
        .def("Reset", &ma::IntVar::Reset)
        .def("Set", &ma::IntVar::Set)
        .def("Get", &ma::IntVar::Get)
        .def("GetTypeInfo", &ma::IntVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::IntVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::IntVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.IntVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::LongVar, ma::LongVarPtr, ma::Var>(in_module, "LongVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::LongVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::LongVar>(key, value);
        }))
        .def("toString", &ma::LongVar::toString)
        .def("fromString", &ma::LongVar::fromString)
        .def("IsValidString", &ma::LongVar::IsValidString)
        .def("Reset", &ma::LongVar::Reset)
        .def("Set", &ma::LongVar::Set)
        .def("Get", &ma::LongVar::Get)
        .def("GetTypeInfo", &ma::LongVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::LongVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::LongVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.LongVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::LongLongVar, ma::LongLongVarPtr, ma::Var>(in_module, "LongLongVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::LongLongVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::LongLongVar>(key, value);
        }))
        .def("toString", &ma::LongLongVar::toString)
        .def("fromString", &ma::LongLongVar::fromString)
        .def("IsValidString", &ma::LongLongVar::IsValidString)
        .def("Reset", &ma::LongLongVar::Reset)
        .def("Set", &ma::LongLongVar::Set)
        .def("Get", &ma::LongLongVar::Get)
        .def("GetTypeInfo", &ma::LongLongVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::LongLongVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::LongLongVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.LongLongVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::UVar, ma::UVarPtr, ma::Var>(in_module, "UVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::UVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::UVar>(key, value);
        }))
        .def("toString", &ma::UVar::toString)
        .def("fromString", &ma::UVar::fromString)
        .def("IsValidString", &ma::UVar::IsValidString)
        .def("Reset", &ma::UVar::Reset)
        .def("Set", &ma::UVar::Set)
        .def("Get", &ma::UVar::Get)
        .def("GetTypeInfo", &ma::UVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::UVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::UVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.UVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::ULongVar, ma::ULongVarPtr, ma::Var>(in_module, "ULongVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::ULongVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::ULongVar>(key, value);
        }))
        .def("toString", &ma::ULongVar::toString)
        .def("fromString", &ma::ULongVar::fromString)
        .def("IsValidString", &ma::ULongVar::IsValidString)
        .def("Reset", &ma::ULongVar::Reset)
        .def("Set", &ma::ULongVar::Set)
        .def("Get", &ma::ULongVar::Get)
        .def("GetTypeInfo", &ma::ULongVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::ULongVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::ULongVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ULongVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::ULongLongVar, ma::ULongLongVarPtr, ma::Var>(in_module, "ULongLongVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::ULongLongVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::ULongLongVar>(key, value);
        }))
        .def("toString", &ma::ULongLongVar::toString)
        .def("fromString", &ma::ULongLongVar::fromString)
        .def("IsValidString", &ma::ULongLongVar::IsValidString)
        .def("Reset", &ma::ULongLongVar::Reset)
        .def("Set", &ma::ULongLongVar::Set)
        .def("Get", &ma::ULongLongVar::Get)
        .def("GetTypeInfo", &ma::ULongLongVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::ULongLongVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::ULongLongVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ULongLongVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

//    ::py::class_<ma::wxULongLongVar, ma::wxULongLongVarPtr, ma::Var>(in_module, "wxULongLongVar")
//        .def(py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::wxULongLongVar::value_type value)
//        {
//            auto item = ma::Item::GetItem(item_key);
//            return item->AddVar<ma::wxULongLongVar>(key, value);
//        }))
//        .def("toString", &ma::wxULongLongVar::toString)
//        .def("fromString", &ma::wxULongLongVar::fromString)
//        .def("IsValidString", &ma::wxULongLongVar::IsValidString)
//        .def("Reset", &ma::wxULongLongVar::Reset)
//        .def("Set", &ma::wxULongLongVar::Set)
//        .def("Get", &ma::wxULongLongVar::Get)
//        .def("GetTypeInfo", &ma::wxULongLongVar::GetTypeInfo)
//        .def("__repr__",
//            [](const ma::ULongLongVarPtr &var)
//            {
//                MA_ASSERT(var,
//                          L"var est nulle.",
//                          L"ma::py::Var::wxULongLongVar::__repr__",
//                          std::invalid_argument);
//
//                return std::wstring(L"<ma.wxULongLongVar( '") + var->GetKey() + std::wstring(L"' )>");
//            });

    ::py::class_<ma::FloatVar, ma::FloatVarPtr, ma::Var>(in_module, "FloatVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::FloatVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::FloatVar>(key, value);
        }))
        .def("toString", &ma::FloatVar::toString)
        .def("fromString", &ma::FloatVar::fromString)
        .def("IsValidString", &ma::FloatVar::IsValidString)
        .def("Reset", &ma::FloatVar::Reset)
        .def("Set", &ma::FloatVar::Set)
        .def("Get", &ma::FloatVar::Get)
        .def("GetTypeInfo", &ma::FloatVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::FloatVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::FloatVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.FloatVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::DoubleVar, ma::DoubleVarPtr, ma::Var>(in_module, "DoubleVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::DoubleVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::DoubleVar>(key, value);
        }))
        .def("toString", &ma::DoubleVar::toString)
        .def("fromString", &ma::DoubleVar::fromString)
        .def("IsValidString", &ma::DoubleVar::IsValidString)
        .def("Reset", &ma::DoubleVar::Reset)
        .def("Set", &ma::DoubleVar::Set)
        .def("Get", &ma::DoubleVar::Get)
        .def("GetTypeInfo", &ma::DoubleVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::DoubleVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::DoubleVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.DoubleVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::LongDoubleVar, ma::LongDoubleVarPtr, ma::Var>(in_module, "LongDoubleVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, ma::LongDoubleVar::value_type value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::LongDoubleVar>(key, value);
        }))
        .def("toString", &ma::LongDoubleVar::toString)
        .def("fromString", &ma::LongDoubleVar::fromString)
        .def("IsValidString", &ma::LongDoubleVar::IsValidString)
        .def("Reset", &ma::LongDoubleVar::Reset)
        .def("Set", &ma::LongDoubleVar::Set)
        .def("Get", &ma::LongDoubleVar::Get)
        .def("GetTypeInfo", &ma::LongDoubleVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::LongDoubleVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::LongDoubleVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.LongDoubleVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::StringHashSetVar, ma::StringHashSetVarPtr, ma::Var>(in_module, "StringHashSetVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::StringHashSetVar::value_type& value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::StringHashSetVar>(key, value);
        }))
        .def("toString", &ma::StringHashSetVar::toString)
        .def("fromString", &ma::StringHashSetVar::fromString)
        .def("IsValidString", &ma::StringHashSetVar::IsValidString)
        .def("Reset", &ma::StringHashSetVar::Reset)
        .def("Set", &ma::StringHashSetVar::Set)
        .def("Get", &ma::StringHashSetVar::Get)
        .def("GetTypeInfo", &ma::StringHashSetVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::StringHashSetVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::StringHashSetVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.StringHashSetVar( '") + var->GetKey() + std::wstring(L"' )>");
            });


    ::py::class_<ma::StringVar, ma::StringVarPtr, ma::Var>(in_module, "StringVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const std::wstring& value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::StringVar>(key, value);
        }))
        .def("toString", &ma::StringVar::toString)
        .def("fromString", &ma::StringVar::fromString)
        .def("IsValidString", &ma::StringVar::IsValidString)
        .def("Reset", &ma::StringVar::Reset)
        .def("Set", &ma::StringVar::Set)
        .def("Get", &ma::StringVar::Get)
        .def("GetTypeInfo", &ma::StringVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::StringVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::StringVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.StringVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::PathVar, ma::PathVarPtr, ma::Var>(in_module, "PathVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const std::wstring& value)
        {
            auto item = ma::Item::GetItem(item_key);

            const std::filesystem::path path{value};

            return item->AddVar<ma::PathVar>(key, path);
        }))
        .def("toString", &ma::PathVar::toString)
        .def("fromString", &ma::PathVar::fromString)
        .def("IsValidString", &ma::PathVar::IsValidString)
        .def("Reset", &ma::PathVar::Reset)
        .def("GetTypeInfo", &ma::PathVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::PathVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::PathVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.PathVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::DirectoryVar, ma::DirectoryVarPtr, ma::Var>(in_module, "DirectoryVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const std::string& value)
        {
            auto item = ma::Item::GetItem(item_key);
            const std::filesystem::path path{value};
            return item->AddVar<ma::DirectoryVar>(key, path);
        }))
        .def("toString", &ma::DirectoryVar::toString)
        .def("fromString", &ma::DirectoryVar::fromString)
        .def("IsValidString", &ma::DirectoryVar::IsValidString)
        .def("Reset", &ma::DirectoryVar::Reset)
        .def("GetTypeInfo", &ma::DirectoryVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::DirectoryVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::DirectoryVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.DirectoryVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::FilePathVar, ma::FilePathVarPtr, ma::Var>(in_module, "FilePathVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const std::wstring& value)
        {
            auto item = ma::Item::GetItem(item_key);
            const std::filesystem::path path{value};
            return item->AddVar<ma::FilePathVar>(key, path);
        }))
        .def("toString", &ma::FilePathVar::toString)
        .def("fromString", &ma::FilePathVar::fromString)
        .def("IsValidString", &ma::FilePathVar::IsValidString)
        .def("Reset", &ma::FilePathVar::Reset)
        .def("GetTypeInfo", &ma::FilePathVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::FilePathVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::FilePathVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.FilePathVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    class PyObservableVar : public ma::ObservableVar
    {
        public:
            /* Inherit the constructors */
            using ma::ObservableVar::ObservableVar;

            /* Trampoline (need one for each virtual function) */
            void Set(const ma::ObservablePtr& observable) override
            {
                PYBIND11_OVERLOAD_PURE(
                    void,               /* Return type */
                    ma::ObservableVar, /* Parent class */
                    Set                 /* Name of function in C++ (must match Python name) */
                    observable          /* Argument(s) */
                );
            }

            ma::ObservablePtr Get() const override
            {
                PYBIND11_OVERLOAD_PURE(
                    ma::ObservablePtr, /* Return type */
                    ma::ObservableVar, /* Parent class */
                    Get,                /* Name of function in C++ (must match Python name) */
                );
            }

            virtual ma::ObservableVar& operator = (const ma::ObservablePtr&) override
            {
                MA_ASSERT(false,
                          L"Ne peut être utilisé via python.");

                return *this;
            }
    };

    ::py::class_<ma::ObservableVar, PyObservableVar, ma::ObservableVarPtr, ma::Var>(in_module, "ObservableVar")
        .def("GetTypeInfo", &ma::Var::GetTypeInfo)
        .def("__repr__",
            [](const ma::ObservableVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::ObservableVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ObservableVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::ItemVar, ma::ItemVarPtr, ma::ObservableVar>(in_module, "ItemVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::ItemPtr& value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::ItemVar>(key, value);
        }))
        .def("toString", &ma::ItemVar::toString)
        .def("fromString", &ma::ItemVar::fromString)
        .def("IsValidString", &ma::ItemVar::IsValidString)
        .def("Reset", &ma::ItemVar::Reset)
        .def("SetItem", &ma::ItemVar::SetItem)
        .def("GetItem", &ma::ItemVar::GetItem)
        .def("Get", &ma::ItemVar::Get)
        .def("GetTypeInfo", &ma::ItemVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::ItemVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::ItemVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ItemVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::VarVar, ma::VarVarPtr, ma::ObservableVar>(in_module, "VarVar")
        .def(::py::init([](ma::Item::key_type item_key, const ma::Var::key_type& key, const ma::VarVarPtr& value)
        {
            auto item = ma::Item::GetItem(item_key);
            return item->AddVar<ma::VarVar>(key, value);
        }))
        .def("toString", &ma::VarVar::toString)
        .def("fromString", &ma::VarVar::fromString)
        .def("IsValidString", &ma::VarVar::IsValidString)
        .def("Reset", &ma::VarVar::Reset)
        .def("Set", &ma::VarVar::Set)
        .def("Get", &ma::VarVar::Get)
        .def("GetTypeInfo", &ma::VarVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::VarVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::VarVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.VarVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::ObserverVar, ma::ObserverVarPtr, ma::Var>(in_module, "ObserverVar")
        .def("Set", &ma::ObserverVar::Set)
        .def("Get", &ma::ObserverVar::Get)
        .def("GetTypeInfo", &ma::ObserverVar::GetTypeInfo)
        .def("__repr__",
            [](const ma::ObserverVarPtr &var)
            {
                MA_ASSERT(var,
                          L"var est nulle.",
                          L"ma::py::Var::ObserverVar::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ObserverVar( '") + var->GetKey() + std::wstring(L"' )>");
            });

//    ::py::class_<ma::wxPanelItemVar, ma::wxPanelItemVarPtr, ma::ObserverVar>(in_module, "wxPanelItemVar")
//        .def("toString", &ma::wxPanelItemVar::toString)
//        .def("fromString", &ma::wxPanelItemVar::fromString)
//        .def("IsValidString", &ma::wxPanelItemVar::IsValidString)
//        .def("Reset", &ma::wxPanelItemVar::Reset)
//        .def("GetTypeInfo", &ma::wxPanelItemVar::GetTypeInfo)
//        .def("__repr__",
//            [](const ma::wxPanelItemVarPtr &var)
//            {
//                MA_ASSERT(var,
//                          "var est nulle.",
//                          "ma::py::Var::wxPanelItemVarPtr::__repr__",
//                          std::invalid_argument);
//
//                return std::string("<ma.wxPanelItemVarPtr( '") + var->GetKey() + std::string("' )>");
//            });
    in_class_item
        .def("AddDefineVar"               , static_cast<ma::DefineVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&)>                                         (&ma::Item::AddVar<ma::DefineVar>)               , ::py::arg("key"))
        .def("AddStringVar"               , static_cast<ma::StringVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&, const ma::StringVar::value_type&)>       (&ma::Item::AddVar<ma::StringVar>)               , ::py::arg("key"), ::py::arg("value"))
        .def("AddBooleanVar"              , static_cast<ma::BoolVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&, const ma::BoolVar::value_type&)>         (&ma::Item::AddVar<ma::BoolVar>)                 , ::py::arg("key"), ::py::arg("value"))
        .def("AddIntVar"                  , static_cast<ma::IntVarPtr           (ma::Item::*)(const ma::VarsMap::key_type&, const ma::IntVar::value_type&)>          (&ma::Item::AddVar<ma::IntVar>)                  , ::py::arg("key"), ::py::arg("value"))
        .def("AddLongVar"                 , static_cast<ma::LongVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&, const ma::LongVar::value_type&)>         (&ma::Item::AddVar<ma::LongVar>)                 , ::py::arg("key"), ::py::arg("value"))
        .def("AddLongLongVar"             , static_cast<ma::LongLongVarPtr      (ma::Item::*)(const ma::VarsMap::key_type&, const ma::LongLongVar::value_type&)>     (&ma::Item::AddVar<ma::LongLongVar>)             , ::py::arg("key"), ::py::arg("value"))
        .def("AddUVar"                    , static_cast<ma::UVarPtr             (ma::Item::*)(const ma::VarsMap::key_type&, const ma::UVar::value_type&)>            (&ma::Item::AddVar<ma::UVar>)                    , ::py::arg("key"), ::py::arg("value"))
        .def("AddULongVar"                , static_cast<ma::ULongVarPtr         (ma::Item::*)(const ma::VarsMap::key_type&, const ma::ULongVar::value_type&)>        (&ma::Item::AddVar<ma::ULongVar>)                , ::py::arg("key"), ::py::arg("value"))
        .def("AddULongLongVar"            , static_cast<ma::ULongLongVarPtr     (ma::Item::*)(const ma::VarsMap::key_type&, const ma::ULongLongVar::value_type&)>    (&ma::Item::AddVar<ma::ULongLongVar>)            , ::py::arg("key"), ::py::arg("value"))
        .def("AddFloatVar"                , static_cast<ma::FloatVarPtr         (ma::Item::*)(const ma::VarsMap::key_type&, const ma::FloatVar::value_type&)>        (&ma::Item::AddVar<ma::FloatVar>)                , ::py::arg("key"), ::py::arg("value"))
        .def("AddDoubleVar"               , static_cast<ma::DoubleVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&, const ma::DoubleVar::value_type&)>       (&ma::Item::AddVar<ma::DoubleVar>)               , ::py::arg("key"), ::py::arg("value"))
        .def("AddLongDoubleVar"           , static_cast<ma::LongDoubleVarPtr    (ma::Item::*)(const ma::VarsMap::key_type&, const ma::LongDoubleVar::value_type&)>   (&ma::Item::AddVar<ma::LongDoubleVar>)           , ::py::arg("key"), ::py::arg("value"))
        .def("AddStringHashSetVar"        , static_cast<ma::StringHashSetVarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::StringHashSetVar::value_type&)>(&ma::Item::AddVar<ma::StringHashSetVar>)        , ::py::arg("key"), ::py::arg("value"))
        .def("AddItemVar"                 , static_cast<ma::ItemVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&, const ma::ItemVar::ptr_type&)>           (&ma::Item::AddVar<ma::ItemVar>)                 , ::py::arg("key"), ::py::arg("value"))
        .def("AddVarVar"                  , static_cast<ma::VarVarPtr           (ma::Item::*)(const ma::VarsMap::key_type&, const ma::VarVar::ptr_type&)>            (&ma::Item::AddVar<ma::VarVar>)                  , ::py::arg("key"), ::py::arg("value"))

        .def("AddReadOnlyDefineVar"       , static_cast<ma::DefineVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&)>                                         (&ma::Item::AddReadOnlyVar<ma::DefineVar>)       , ::py::arg("key"))
        .def("AddReadOnlyStringVar"       , static_cast<ma::StringVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&, const ma::StringVar::value_type&)>       (&ma::Item::AddReadOnlyVar<ma::StringVar>)       , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyBooleanVar"      , static_cast<ma::BoolVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&, const ma::BoolVar::value_type&)>         (&ma::Item::AddReadOnlyVar<ma::BoolVar>)         , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyIntVar"          , static_cast<ma::IntVarPtr           (ma::Item::*)(const ma::VarsMap::key_type&, const ma::IntVar::value_type&)>          (&ma::Item::AddReadOnlyVar<ma::IntVar>)          , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyLongVar"         , static_cast<ma::LongVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&, const ma::LongVar::value_type&)>         (&ma::Item::AddReadOnlyVar<ma::LongVar>)         , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyLongLongVar"     , static_cast<ma::LongLongVarPtr      (ma::Item::*)(const ma::VarsMap::key_type&, const ma::LongLongVar::value_type&)>     (&ma::Item::AddReadOnlyVar<ma::LongLongVar>)     , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyUVar"            , static_cast<ma::UVarPtr             (ma::Item::*)(const ma::VarsMap::key_type&, const ma::UVar::value_type&)>            (&ma::Item::AddReadOnlyVar<ma::UVar>)            , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyULongVar"        , static_cast<ma::ULongVarPtr         (ma::Item::*)(const ma::VarsMap::key_type&, const ma::ULongVar::value_type&)>        (&ma::Item::AddReadOnlyVar<ma::ULongVar>)        , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyULongLongVar"    , static_cast<ma::ULongLongVarPtr     (ma::Item::*)(const ma::VarsMap::key_type&, const ma::ULongLongVar::value_type&)>    (&ma::Item::AddReadOnlyVar<ma::ULongLongVar>)    , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyFloatVar"        , static_cast<ma::FloatVarPtr         (ma::Item::*)(const ma::VarsMap::key_type&, const ma::FloatVar::value_type&)>        (&ma::Item::AddReadOnlyVar<ma::FloatVar>)        , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyDoubleVar"       , static_cast<ma::DoubleVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&, const ma::DoubleVar::value_type&)>       (&ma::Item::AddReadOnlyVar<ma::DoubleVar>)       , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyLongDoubleVar"   , static_cast<ma::LongDoubleVarPtr    (ma::Item::*)(const ma::VarsMap::key_type&, const ma::LongDoubleVar::value_type&)>   (&ma::Item::AddReadOnlyVar<ma::LongDoubleVar>)   , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyStringHashSetVar", static_cast<ma::StringHashSetVarPtr (ma::Item::*)(const ma::VarsMap::key_type&, const ma::StringHashSetVar::value_type&)>(&ma::Item::AddReadOnlyVar<ma::StringHashSetVar>), ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyItemVar"         , static_cast<ma::ItemVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&, const ma::ItemVar::ptr_type&)>           (&ma::Item::AddReadOnlyVar<ma::ItemVar>)         , ::py::arg("key"), ::py::arg("value"))
        .def("AddReadOnlyVarVar"          , static_cast<ma::VarVarPtr           (ma::Item::*)(const ma::VarsMap::key_type&, const ma::VarVar::ptr_type&)>            (&ma::Item::AddReadOnlyVar<ma::VarVar>)          , ::py::arg("key"), ::py::arg("value"))

        .def("GetDefineVar"               , static_cast<ma::DefineVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::DefineVar>)               , ::py::arg("key"))
        .def("GetStringVar"               , static_cast<ma::StringVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::StringVar>)               , ::py::arg("key"))
        .def("GetBooleanVar"              , static_cast<ma::BoolVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::BoolVar>)                 , ::py::arg("key"))
        .def("GetIntVar"                  , static_cast<ma::IntVarPtr           (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::IntVar>)                  , ::py::arg("key"))
        .def("GetLongVar"                 , static_cast<ma::LongVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::LongVar>)                 , ::py::arg("key"))
        .def("GetLongLongVar"             , static_cast<ma::LongLongVarPtr      (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::LongLongVar>)             , ::py::arg("key"))
        .def("GetUVar"                    , static_cast<ma::UVarPtr             (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::UVar>)                    , ::py::arg("key"))
        .def("GetULongVar"                , static_cast<ma::ULongVarPtr         (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::ULongVar>)                , ::py::arg("key"))
        .def("GetULongLongVar"            , static_cast<ma::ULongLongVarPtr     (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::ULongLongVar>)            , ::py::arg("key"))
        .def("GetFloatVar"                , static_cast<ma::FloatVarPtr         (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::FloatVar>)                , ::py::arg("key"))
        .def("GetDoubleVar"               , static_cast<ma::DoubleVarPtr        (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::DoubleVar>)               , ::py::arg("key"))
        .def("GetLongDoubleVar"           , static_cast<ma::LongDoubleVarPtr    (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::LongDoubleVar>)           , ::py::arg("key"))
        .def("GetStringHashSetVar"        , static_cast<ma::StringHashSetVarPtr (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::StringHashSetVar>)        , ::py::arg("key"))
        .def("GetItemVar"                 , static_cast<ma::ItemVarPtr          (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::ItemVar>)                 , ::py::arg("key"))
        .def("GetVarVar"                  , static_cast<ma::VarVarPtr           (ma::Item::*)(const ma::VarsMap::key_type&) const>                                   (&ma::Item::GetVar<ma::VarVar>)                  , ::py::arg("key"));
}

