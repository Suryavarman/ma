/// \file ObservationManager.py.cpp
/// \author Pontier Pierre
/// \date 2020-02-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MITconst
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/ObservationManager.py.h"
#include "Py/bind/Utils.py.h"

#include <pybind11/operators.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::ObservationManager::SetModule(::py::module &in_module)
{
    ::py::bind_map<ma::Observations>(in_module, L"Observations");

    ::py::class_<ma::ObservationManager, ma::ObservationManagerPtr, ma::Item>(in_module, "ObservationManager")
        .def_static("fs_GetObservationManagerClassName", &ma::ObservationManager::GetObservationManagerClassName)
        .def("HasObservation", static_cast<bool (ma::ObservationManager::*)(ma::ObserverPtr, ma::ObservablePtr) const>(&ma::ObservationManager::HasObservation), ::py::arg("observer"), ::py::arg("observable"))
        .def("HasObservable", static_cast<bool (ma::ObservationManager::*)(ma::ObservablePtr) const>(&ma::ObservationManager::HasObservable), ::py::arg("observable"))
        .def("HasObserver", static_cast<bool (ma::ObservationManager::*)(ma::ObserverPtr) const>(&ma::ObservationManager::HasObserver), ::py::arg("observer"))
        .def("GetObservation", static_cast<ma::ObservationPtr (ma::ObservationManager::*)(ma::ObserverPtr, ma::ObservablePtr) const>(&ma::ObservationManager::GetObservation), ::py::arg("observer"), ::py::arg("observable"))
        .def("GetObservations", static_cast<ma::Observations (ma::ObservationManager::*)(ma::ObserverPtr) const>(&ma::ObservationManager::GetObservations), ::py::arg("observer"))
        .def("GetObservations", static_cast<ma::Observations (ma::ObservationManager::*)(ma::ObservablePtr) const>(&ma::ObservationManager::GetObservations), ::py::arg("observable"))
        .def("UpdateObservations", static_cast<void (ma::ObservationManager::*)(ma::ObservablePtr, const ma::Observable::Data&)>(&ma::ObservationManager::UpdateObservations), ::py::arg("observable"), ::py::arg("data"))
        .def("GetTypeInfo", &ma::ObservationManager::GetTypeInfo)
        .def("__repr__",
            [](const ma::ObservationManagerPtr &obs_mgr)
            {
                MA_ASSERT(obs_mgr,
                          L"obs_mgr est nulle.",
                          L"ma::py::ObservationManager::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.ObservationManager( '") + obs_mgr->GetKey() + std::wstring(L"' )>");
            });

    ::py::class_<ma::Observation, ma::ObservationPtr, ma::Item>(in_module, "Observation")
        .def_static("fs_GetObservationClassName", &ma::Observation::GetObservationClassName)
        .def_readwrite("m_ObservableVar", &ma::Observation::m_ObservableVar)
        .def_readwrite("m_ObserverVar", &ma::Observation::m_ObserverVar)
        .def("GetTypeInfo", &ma::Observation::GetTypeInfo)
        .def("__repr__",
            [](const ma::ObservationPtr &obs)
            {
                MA_ASSERT(obs,
                          L"obs est nulle.",
                          L"ma::py::ObservationManager::Observation::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.Observation( '") + obs->GetKey() + std::wstring(L"' )>");
            });
}

