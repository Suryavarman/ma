/// \file RTTI.py.cpp
/// \author Pontier Pierre
/// \date 2020-02-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/RTTI.py.h"

#include <pybind11/operators.h>
#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::RTTI::SetModule(::py::module &in_module)
{
    ::py::class_<ma::RTTI, ma::RTTIPtr, ma::Item>(in_module, "RTTI")
        .def("GetTypeInfo", &ma::RTTI::GetTypeInfo)
        .def("__repr__",
            [](const ma::RTTIPtr &rtti)
            {
                MA_ASSERT(rtti,
                          L"rtti est nulle.",
                          L"ma::py::RTTI::RTTI::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.RTTI( '") + rtti->GetKey() + std::wstring(L"' )>");
            });
}

