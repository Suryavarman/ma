/// \file Item.py.cpp
/// \author Pontier Pierre
/// \date 2019-12-09
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/Item.py.h"
#include "Py/bind/wx.py.h"
#include "Py/bind/Utils.py.h"

#include <pybind11/operators.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

ma::py::Item::value_type ma::py::Item::SetModule(::py::module &in_module)
{
    // https://pybind11.readthedocs.io/en/master/advanced/classes.html#operator-overloading
    // https://pybind11.readthedocs.io/en/stable/advanced/smart_ptrs.html

    ::py::bind_map<ma::ItemsMap>(in_module, L"ItemsMap");
    //::py::bind_deque<ma::ItemsOrder>(in_module, "ItemsOrder");

    auto py_item = ma::py::Item::value_type(in_module, "Item")
        .def(::py::init([]()
                        {
                            return ma::Item::CreateItem<ma::Item>();
                        }))

        .def(::py::init([](ma::Item::key_type parent_key)
                        {
                            return ma::Item::CreateItem<ma::Item>({std::move(parent_key)});
                        }))

        .def_static("fs_GetItemClassName", &ma::Item::GetItemClassName)

        .def_static("fs_CreateItem",
                    [](ma::Item::key_type parent_key)
                    {
                        return ma::Item::CreateItem<ma::Item>({std::move(parent_key)});
                    },
                    ::py::arg("parent_key") = ma::Item::Key::GetRoot())

        .def_static("fs_CreateRoot",
                    [](const std::wstring& config_path)
                    {
                        return ma::Item::CreateRoot<ma::Root>(config_path.empty() ? std::filesystem::current_path() / L"config.cfg" : config_path);
                    })

        .def_static("fs_EraseRoot", &ma::Item::EraseRoot)
        .def_static("fs_GetRoot", static_cast<ma::ItemPtr (*)()>(&ma::Item::GetRoot))

        .def_static("fs_EraseItem",
                    [](const ma::Item::key_type& key)
                    {
                        ma::Item::EraseItem(key);
                    }, ::py::arg("key"))

        .def_static("fs_EraseItem",
                    [](ma::ItemPtr item)
                    {
                        ma::Item::EraseItem(item);
                    }, ::py::arg("item"))

        .def_static("fs_GetItems", static_cast<ma::ItemsMap (*)()>(&ma::Item::GetItems))
        .def_static("fs_GetItemsPostOrder", static_cast<ma::ItemsOrder (*)()>(&ma::Item::GetItemsPostOrder))

        .def_static("fs_GetItem",
                    static_cast<ma::ItemPtr (*)(const ma::Item::key_type&)>(&ma::Item::GetItem),
                    ::py::arg("key"))

        .def_static("fs_HasItem",
                    static_cast<bool (*)(const ma::Item::key_type&)>(&ma::Item::HasItem),
                    ::py::arg("key"))

        .def_static("fs_GetCount", static_cast<ma::ItemsMap::size_type (*)()>(&ma::Item::GetCount))

        .def("GetItem",
             static_cast<ma::ItemPtr (ma::Item::*)(const ma::Item::key_type&, ma::Item::SearchMod) const>(&ma::Item::GetItem),
             ::py::arg("key"), ::py::arg("search_mod"))

        .def("HasItem",
             static_cast<bool (ma::Item::*)(const ma::Item::key_type&, ma::Item::SearchMod) const>(&ma::Item::HasItem),
             ::py::arg("key"), ::py::arg("search_mod"))

        .def("GetCount",
             static_cast<ma::ItemsMap::size_type (ma::Item::*)(ma::Item::SearchMod) const>(&ma::Item::GetCount),
             ::py::arg("search_mod"))

        .def("GetItems",
             static_cast<ma::ItemsMap (ma::Item::*)(ma::Item::SearchMod) const>(&ma::Item::GetItems),
             ::py::arg("search_mod"))

        .def("GetItemsPostOrder",
             static_cast<ma::ItemsOrder (ma::Item::*)(ma::Item::SearchMod) const>(&ma::Item::GetItemsPostOrder),
             ::py::arg("search_mod"))

        .def("GetKey", &ma::Item::GetKey)

        .def("GetParentKey", &ma::Item::GetParentKey)

        .def("AddChild",
             static_cast<void (ma::Item::*)(const ma::Item::key_type&)>(&ma::Item::AddChild),
             ::py::arg("key"))

        .def("AddChild",
             static_cast<void (ma::Item::*)(ma::ItemPtr)>(&ma::Item::AddChild),
             ::py::arg("item"))

        .def("RemoveChild",
             static_cast<void (ma::Item::*)(const ma::Item::key_type&)>(&ma::Item::RemoveChild),
             ::py::arg("key"))

        .def("RemoveChild",
             static_cast<void (ma::Item::*)(ma::ItemPtr)>(&ma::Item::RemoveChild),
             ::py::arg("item"))

        .def("Remove",
             static_cast<void (ma::Item::*)()>(&ma::Item::Remove),
             "Raccourcis d'écriture qui permet de retirer l'item de son parent."
             "L'exception std::invalid_argument est levée si le parent n'est pas dans ms_Map ou si l'item n'a pas de "
             "parent.")

        .def("AcceptToAddChild",
             static_cast<bool (ma::Item::*)(const ma::ItemPtr&) const>(&ma::Item::AcceptToAddChild),
             ::py::arg("item"))

        .def("AcceptToRemoveChild",
             static_cast<bool (ma::Item::*)(const ma::Item::key_type&) const>(&ma::Item::AcceptToRemoveChild),
             ::py::arg("key"))

        .def("EraseVar",
             static_cast<void (ma::Item::*)(const ma::Var::key_type&)>(&ma::Item::EraseVar),
             ::py::arg("key"))

        .def("GetVar",
             static_cast<ma::VarPtr (ma::Item::*)(const ma::VarsMap::key_type&) const>(&ma::Item::GetVar),
             ::py::arg("key"))

        .def("HasVar",
             static_cast<bool (ma::Item::*)(const ma::VarsMap::key_type&) const>(&ma::Item::HasVar),
             ::py::arg("key"))

        .def("GetVars",
             static_cast<ma::VarsMap (ma::Item::*)() const>(&ma::Item::GetVars))

        .def("GetTypeInfo", &ma::Item::GetTypeInfo)

        .def("IsObservable", &ma::Item::IsObservable)

        .def("IsEnable",
             [](const ma::ItemPtr &self)
             {
                self->IsEnable();
             }) // La fonction n'est pas IsEnable virtuelle.

        .def("__repr__",
            [](const ma::ItemPtr &self)
            {
                MA_ASSERT(self,
                          L"self est nulle.",
                          L"ma::py::Item::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.Item( '") + self->GetKey() + std::wstring(L"' )>");
            });

    ::py::enum_<ma::Item::SearchMod>(py_item, "SearchMod")
        .value("LOCAL", ma::Item::SearchMod::LOCAL)
        .value("RECURSIVE", ma::Item::SearchMod::RECURSIVE)
        .value("GLOBAL", ma::Item::SearchMod::GLOBAL)
        .export_values();

    auto key = ::py::class_<ma::Item::Key>(py_item, "Key")
        .def_static("GetNoParent",
                    &ma::Item::Key::GetNoParent,
                    "Si un item se retrouve sans parents, l'id de son parent sera celui-ci."
                    "Hormis l'item racine, un item sans parents est un item qui n'est pas enregistré dans ms_Map."
                    "Un item sans parents doit être considéré comme geler."
                    "Les opérations telles que GetItem, utilisent ms_Map pour retrouver les items.")

        .def_static("GetRoot", &ma::Item::Key::GetRoot,
                    "Tout item actif hérite de root. « Root » est item possédant l'id « ms_KeyRoot »."
                    "Tout item qui n'est pas enfant de root, n'est pas présent dans « ms_Map ».")

        .def_static("GetObservationManager",
                    &ma::Item::Key::GetObservationManager,
                    "Clef du gestionnaire des observations")

        .def_static("GetClassInfoManager",
                    &ma::Item::Key::GetClassInfoManager,
                    "Clef du gestionnaire des informations sur la hiérarchie des classes.")

        .def_static("GetRTTI",
                    &ma::Item::Key::GetRTTI,
                    "Clef du gestionnaire de fabriques d'items, de variables…")

        .def_static("GetResourceManager",
                    &ma::Item::Key::GetResourceManager,
                    "Clef du gestionnaire de ressources.")

        .def_static("GetContextManager",
                    &ma::Item::Key::GetContextManager,
                    "Clef du gestionnaire de contextes.")

        .def_static("GetGuiAccess",
                    &ma::Item::Key::GetGuiAccess,
                    "Clef pour accéder aux éléments standard de l'interface graphique.")

        .def_static("GetwxParameterManager",
                    &ma::Item::Key::GetGuiAccess,
                    "Clef du gestionnaire des paramètres d'affichage de l'interface wxWidgets.")

        .def_static("GetGarbage",
                    &ma::Item::Key::GetGarbage,
                    "Clef de la poubelle à items.")

        .def_static("GetConfig",
                    &ma::Item::Key::GetConfig,
                    "La clef du fichier de configuration de l'application.")

        .def_static("GetModuleManager",
                    &ma::Item::Key::GetModuleManager,
                    "Clef du gestionnaire de modules.");

    auto var = ::py::class_<ma::Item::Key::Var>(key, "Var")
        .def_static("GetName",
                    &ma::Item::Key::Var::GetName,
                    "Clef utilisée par défaut pour nommer l'instance d'un item."
                    "La convention est que la variable est du type « ma.StringVar »."
                    "Par défaut cette variable n'est pas présente.")

        .def_static("GetDoc",
                    &ma::Item::Key::Var::GetDoc,
                    "Clef utilisée par défaut pour décrire l'instance d'un item."
                    "La convention est que la variable est du type « ma.StringVar »."
                    "Par défaut cette variable n'est pas présente.")

        .def_static("GetSavePath",
                    &ma::Item::Key::Var::GetSavePath,
                    "Clef utiliser pour nommer la variable m_SavePath.")

        .def_static("GetLastSavePath",
                    &ma::Item::Key::Var::GetLastSavePath,
                    "Clef utiliser pour nommer la variable m_LastSavePath.")

        .def_static("GetCustomSavePath",
                    &ma::Item::Key::Var::GetCustomSavePath,
                    "Clef utiliser pour nommer la variable m_CustomSavePath.");

    auto obs = ::py::class_<ma::Item::Key::Obs>(key, "Obs")
        .def_readonly_static("ms_RemoveItemBegin", &ma::Item::Key::Obs::ms_RemoveItemBegin)
        .def_readonly_static("ms_RemoveItemEnd", &ma::Item::Key::Obs::ms_RemoveItemEnd)
        .def_readonly_static("ms_AddVar", &ma::Item::Key::Obs::ms_AddVar)
        .def_readonly_static("ms_RemoveVarBegin", &ma::Item::Key::Obs::ms_RemoveVarBegin)
        .def_readonly_static("ms_RemoveVarEnd", &ma::Item::Key::Obs::ms_RemoveVarEnd)
        .def_readonly_static("ms_ConstructionEnd", &ma::Item::Key::Obs::ms_ConstructionEnd);

    return py_item;
}

