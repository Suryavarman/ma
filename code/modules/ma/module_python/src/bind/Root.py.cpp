/// \file Root.py.cpp
/// \author Pontier Pierre
/// \date 2020-02-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/Root.py.h"
#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::Root::SetModule(::py::module &in_module)
{
    ::py::class_<ma::Root, ma::RootPtr, ma::Item>(in_module, "Root")
        .def_static("fs_GetKeyCurrentVarProperty", &ma::Root::GetKeyCurrentVarProperty)
        .def_readwrite("m_ClassInfoManager", &ma::Root::m_ClassInfoManager)
        .def_readwrite("m_wxGuiAccess", &ma::Root::m_wxGuiAccess)
        .def_readwrite("m_Rtti", &ma::Root::m_RttiManager)
        .def_readwrite("m_Views", &ma::Root::m_Views)
        .def_readwrite("m_ObservationManager", &ma::Root::m_ObservationManager)
        .def_readwrite("m_Current", &ma::Root::m_Current)
        .def("GetTypeInfo", &ma::Root::GetTypeInfo)
        .def("__repr__",
            [](const ma::RootPtr &root)
            {
                MA_ASSERT(root,
                          L"root est nulle.",
                          L"ma::py::Root::Root::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.Root( '") + root->GetKey() + std::wstring(L"' )>");
            });

    in_module.def("root", &ma::Item::GetRoot<ma::Root>);
}

