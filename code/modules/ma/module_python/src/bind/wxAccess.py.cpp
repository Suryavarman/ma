/// \file wxAccess.py.cpp
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

// wxWidget va nedéfinir l'expression _() avec wxTranslation si WXINTL_NO_GETTEXT_MACRO n'est pas défini.
#ifndef WXINTL_NO_GETTEXT_MACRO
    #define WXINTL_NO_GETTEXT_MACRO
#endif // WXINTL_NO_GETTEXT_MACRO

#include "Py/bind/wxAccess.py.h"
#include "Py/bind/wx.py.h"

#include <ma/Ma.h>

void ma::py::wxAccess::SetModule(::py::module &in_module)
{
    ::py::object py_wxModule = ::py::module::import("wx");

    in_module.def("get_menu_bar", []()
    {
        return ma::root()->m_wxGuiAccess->GetMenuBar();

    }, ::py::return_value_policy::reference, "Return the menu bar.");

    in_module.def("get_tool_bar", []()
    {
        return ma::root()->m_wxGuiAccess->GetToolBar();

    }, ::py::return_value_policy::reference, "Return the aui tool bar.");

    in_module.def("get_bottom", []()
    {
        return ma::root()->m_wxGuiAccess->GetBottomNotebook();

    }, ::py::return_value_policy::reference, "Return the bottom aui note book.");

    in_module.def("get_left", []()
    {
        return ma::root()->m_wxGuiAccess->GetLeftNoteBook();

    }, ::py::return_value_policy::reference, "Return the left aui note book.");

    in_module.def("get_right", []()
    {
        return ma::root()->m_wxGuiAccess->GetRightNoteBook();

    }, ::py::return_value_policy::reference, "Return the right aui note book.");

    in_module.def("get_center", []()
    {
        return ma::root()->m_wxGuiAccess->GetCenterNoteBook();

    }, ::py::return_value_policy::reference, "Return the center aui note book.");

    in_module.def("get_mother_frame", []()
    {
        return ma::root()->m_wxGuiAccess->GetMotherFrame();

    }, ::py::return_value_policy::reference, "Return the mother frame.");

    in_module.def("SendTextToClipboard", [](const std::wstring& str)
    {
        return ma::root()->m_wxGuiAccess->SendTextToClipboard(str);
    }
    , ::py::return_value_policy::reference, "Return the mother frame."
    , ::py::arg("str"));
}

