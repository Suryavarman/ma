/// \file View.py.cpp
/// \author Pontier Pierre
/// \date 2024-01-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/wx.py.h"
#include "Py/bind/wx/View.py.h"

#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include <pybind11/operators.h>
#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

namespace ma::wx
{
    class PyView : public ma::wx::View
    {
        public:
            explicit PyView(const Item::ConstructorParameters &in_params, const ma::Var::key_type &name):
            ma::wx::View(in_params, name)
            {}

            PyView() = delete;
            ~PyView() override = default;

            void EndConstruction() override
            {
                ma::wx::View::EndConstruction();
            }

//            void SetWindowView(wxWindow* window)
//            {
//                m_WindowView = window;
//            }
//
//            wxWindow* GetWindowView() const
//            {
//                return m_WindowView;
//            }

            /* Trampoline (need one for each virtual function) */
            wxWindow *BuildWindow(wxWindow* parent) override
            {
                // PYBIND11_OVERLOAD_PURE_NAME → pour renommer BuildWindow em _BuildWindow et ainsi indiquer que le
                // membre est protégé.

                PYBIND11_OVERLOAD_PURE(
                    wxWindow*,    /* Return type */
                    ma::wx::View, /* Parent class */
                    BuildWindow,  /* Name of function in C++ (must match Python name) */
                    parent        /* Argument(s) */
                );

//                pybind11::gil_scoped_acquire gil;
//                pybind11::function override = pybind11::get_override(static_cast<const ma::wx::View*>(this), "BuildWindow");
//
//                if(override)
//                {
//                    auto o = override(parent);
//                    if(pybind11::detail::cast_is_temporary_value_reference<wxWindow*>::value)
//                    {
//                        static pybind11::detail::override_caster_t<wxWindow*>caster;
//                        return pybind11::detail::cast_ref<wxWindow*>(std::move(o),caster);
//                    }
//
//                    return pybind11::detail::cast_safe<wxWindow*>(std::move(o));
//                }
//                pybind11::pybind11_fail("Tried to call pure virtual function \"" "ma::wx::View" "::" "BuildWindow" "\"");
            }

//            void DestroyWindow() override
//            {
//                PYBIND11_OVERLOAD_PURE(
//                    void,          /* Return type */
//                    ma::wx::View,  /* Parent class */
//                    DestroyWindow  /* Name of function in C++ (must match Python name) */
//                );
//            }

            wxWindow* GetView() const override
            {
                PYBIND11_OVERRIDE(
                    wxWindow*,     /* Return type */
                    ma::wx::View,  /* Parent class */
                    GetView        /* Name of function in C++ (must match Python name) */
                );
            }

            ma::wx::View::tags_type GetTags() const override
            {
                PYBIND11_OVERRIDE(
                    ma::wx::View::tags_type, /* Return type */
                    ma::wx::View,            /* Parent class */
                    GetTags                  /* Name of function in C++ (must match Python name) */
                );
            }

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(PyView, wx)
    };
    typedef std::shared_ptr<PyView> PyViewPtr;

    const TypeInfo& PyView::GetwxPyViewTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetwxPyViewClassName(),
            L"Trampoline pour exporter en python la classe ma::wx::View.",
            {}
        };
        // clang-format on
        return info;
    }

    M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(PyView, wx, ma::wx::View::GetwxViewClassHierarchy())
}

void ma::py::wx::View::SetModule(::py::module &in_ma_wx_module)
{
    #define MA_PY_DEF_CLASS(in_M_module, in_M_Class, in_M_Namespace, in_M_ParentClassWithNameSpace) \
        ::py::class_<ma::in_M_Namespace::in_M_Class, ma::in_M_Namespace::in_M_Class##Ptr, in_M_ParentClassWithNameSpace>(in_M_module, #in_M_Class, ma::to_string(ma::in_M_Namespace::in_M_Class::Get##in_M_Namespace##in_M_Class##TypeInfo().m_Description).c_str()) \
        .def(::py::init([](const ma::Item::key_type& parent_key) \
         { \
            return ma::Item::CreateItem<ma::in_M_Namespace::in_M_Class>({parent_key}); \
         }))                                                                                        \
         .def("__repr__", [](const ma::in_M_Namespace::in_M_Class##Ptr &tag) \
         { \
                return "<ma."s + #in_M_Namespace + "."s + #in_M_Class + "( '"s + tag->GetKey() + "' )>"s; \
         })

    auto tag = MA_PY_DEF_CLASS(in_ma_wx_module, Tag, wx, ma::Item);

    auto position = MA_PY_DEF_CLASS(tag     , Position, wx, ma::wx::Tag);
    auto bottom   = MA_PY_DEF_CLASS(position, Bottom  , wx, ma::wx::Position);
    auto top      = MA_PY_DEF_CLASS(position, Top     , wx, ma::wx::Position);
    auto right    = MA_PY_DEF_CLASS(position, Right   , wx, ma::wx::Position);
    auto left     = MA_PY_DEF_CLASS(position, Left    , wx, ma::wx::Position);
    auto center   = MA_PY_DEF_CLASS(position, Center  , wx, ma::wx::Position);

    auto axis = MA_PY_DEF_CLASS(tag , Axis, wx, ma::wx::Tag);
    auto x    = MA_PY_DEF_CLASS(axis,    X, wx, ma::wx::Axis);
    auto y    = MA_PY_DEF_CLASS(axis,    Y, wx, ma::wx::Axis);
    auto xy   = MA_PY_DEF_CLASS(axis,   XY, wx, ma::wx::Axis);

    auto type       = MA_PY_DEF_CLASS(      tag,       Type, wx, ma::wx::Tag);
    auto text       = MA_PY_DEF_CLASS(     type,       Text, wx, ma::wx::Type);
    auto widgets    = MA_PY_DEF_CLASS(     type,    Widgets, wx, ma::wx::Type);
    auto rendering  = MA_PY_DEF_CLASS(     type,  Rendering, wx, ma::wx::Type);
    auto realTime   = MA_PY_DEF_CLASS(rendering,   RealTime, wx, ma::wx::Rendering);
    auto precompute = MA_PY_DEF_CLASS(rendering, Precompute, wx, ma::wx::Rendering);

    class ViewPublicist : public ma::wx::View
    {
        public:
            using ma::wx::View::m_WindowID;
            using ma::wx::View::BuildWindow;
    };

    auto py_wx_module = ::py::module::import("wx");

    // \todo faire dériver aussi View de ma::Observer. Il faudra pour cela trouver la solution à cette erreur:
    //       Message: Le module module_py_shell/module_py_shell.py n'a peu être chargé.
    //       L'erreur est la suivante: « generic_type: type "View" has a non-default holder type while its base
    //       "ma::Observer" does not ».
    //       Le type de l'erreur est le suivant: « <class 'ImportError'> ».
    ::py::class_<ma::wx::View, ma::wx::PyView, ma::wx::ViewPtr, ma::Item>(in_ma_wx_module,
                                                                          "View",
                                                                          "Permet de créer et détruire un type de vue.")

        .def(::py::init([](const ma::Item::key_type& parent_key, const ma::Var::key_type &name)
             {
                return ma::Item::CreateItem<ma::wx::PyView>({ma::wx::Views::Key::Get()}, name);
             }))

        .def_readwrite("_WindowView", &ViewPublicist::m_WindowID)
        .def("BuildWindow", [](std::shared_ptr<ViewPublicist> self, ::py::object inWindow)
             {
                 wxWindow *window = ma::py::wxLoad<wxWindow>(inWindow.release(), "wxWindow");
                 wxASSERT(window);

                 return self->BuildWindow(window);
            },
            ::py::arg("window"), ::py::return_value_policy::reference)

        .def_readwrite("m_Display", &ma::wx::View::m_Display)
        .def_readonly("m_Name", &ma::wx::View::m_Name)

        .def("CreateView",
             &ma::wx::View::CreateView,
             ::py::return_value_policy::reference,
             "Renvoie la fenêtre nouvellement créée associée à cette vue.")

        .def("ViewDestroyed",
             &ma::wx::View::ViewDestroyed,
             "Permet de notifier de la destruction de la fenêtre de la vue.")

        .def("GetView",[](const std::shared_ptr<ma::wx::View>& self)
            {
                return self->GetView();
            },
            ::py::return_value_policy::reference,
            "Renvoie la fenêtre associée à cette vue. Peut être nul si aucune fenêtre ne lui est associées.")

        .def("GetTags", &ma::wx::View::GetTags, "Renvoie Renvoie la liste des étiquettes associées à cette item.")
        .def("GetTypeInfo", &ma::wx::View::GetTypeInfo)
        .def("__repr__",
            [](const ma::wx::ViewPtr &view)
            {
                MA_ASSERT(view,
                          L"view est nul.",
                          L"ma::py::wx::View::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.wx.View( '") + view->GetKey() + std::wstring(L"' )>");
            });

    // ::py::bind_deque<ma::wx::Views::views_type>(py_views, L"views_type");
    auto py_views = ::py::class_<ma::wx::Views, ma::wx::ViewsPtr, ma::Item>(in_ma_wx_module,
                                                                            "Views",
                                                                            "Conteneur de vues.")

        .def("Get", &ma::wx::Views::GetViews)
        .def("GetTags", &ma::wx::Views::GetTags)
        .def("GetTypeInfo", &ma::wx::Views::GetTypeInfo)
        .def("__repr__",
            [](const ma::wx::ViewsPtr &views)
            {
                MA_ASSERT(views,
                          L"views est nul.",
                          L"ma::py::wx::Views::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.wx.Views( '") + views->GetKey() + std::wstring(L"' )>");
            });

    auto key = ::py::class_<ma::wx::Views::Key>(py_views, "Key")
        .def_static("Get",
                    &ma::wx::Views::Key::Get,
                    "Clef unique associé à « ma::wx::Views »");

    std::wcout << "Module « ma.wx.View » chargé." << std::endl;
}
