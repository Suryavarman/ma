/// \file Observer.py.cpp
/// \author Pontier Pierre
/// \date 2020-02-19
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Py/bind/Observer.py.h"

#include <pybind11/operators.h>
#include <ma/Ma.h>
#include <sstream>

using namespace pybind11::literals;

void ma::py::Observer::SetModule(::py::module &in_module)
{
    class PyObservable : public ma::Observable
    {
        public:
            /* Inherit the constructors */
            using ma::Observable::Observable;

            /* Trampoline (need one for each virtual function) */
            ma::Observable::key_type GetKey() const override
            {
                PYBIND11_OVERLOAD_PURE(
                    ma::Observable::key_type, /* Return type */
                    ma::Observable,           /* Parent class */
                    GetKey                     /* Name of function in C++ (must match Python name) */
                );
            }

            ma::ObservablePtr GetPtr() const override
            {
                PYBIND11_OVERLOAD_PURE(
                    ma::ObservablePtr, /* Return type */
                    ma::Observable,    /* Parent class */
                    GetPtr              /* Name of function in C++ (must match Python name) */
                );
            }

    };

    class ObservablePublicist : public ma::Observable
    {
        public:
            using ma::Observable::GetPtr;
            using ma::Observable::SetNeedUpdate;
            using ma::Observable::EndConstruction;
    };

    /// \see https://stackoverflow.com/questions/797771/python-protected-attributes
    auto py_observable = ::py::class_<ma::Observable, PyObservable, ma::ObservablePtr>(in_module, "Observable")
                        .def("GetKey", &ma::Observable::GetKey)
                        .def("UpdateObservations", &ma::Observable::UpdateObservations)
                        .def("BeginBatch", &ma::Observable::BeginBatch)
                        .def("EndBatch", &ma::Observable::EndBatch)
                        .def("GetBatchState", &ma::Observable::GetBatchState)
                        .def("SetObserve", &ma::Observable::SetObserve)
                        .def("GetEndConstruction", &ma::Observable::GetEndConstruction)
                        .def("GetTypeInfo", &ma::Observable::GetTypeInfo)
                        .def("_GetPtr", &ObservablePublicist::GetPtr)
                        .def("_SetNeedUpdate", &ObservablePublicist::SetNeedUpdate)
                        .def("_EndConstruction", &ObservablePublicist::EndConstruction)
                        .def("__repr__",
                            [](const ma::ObservablePtr &observable)
                            {
                                MA_ASSERT(observable,
                                          L"observable est nulle.",
                                          L"ma::py::Observer::Observable::__repr__",
                                          std::invalid_argument);

                                return std::wstring(L"<ma.Observable( '") + observable->GetKey() + std::wstring(L"' )>");
                            });

    ::py::class_<ma::Observable::Data>(py_observable, "Data")
        .def(::py::init<std::initializer_list<ma::Observable::DataOrdered::value_type>>())
        .def(::py::init<const ma::Observable::DataOrdered&>())
        .def("GetOrdered", &ma::Observable::Data::GetOrdered)
        .def("GetUnordered", &ma::Observable::Data::GetUnordered)
        .def("HasActions", &ma::Observable::Data::HasActions)
        .def("GetLastAction", &ma::Observable::Data::GetLastAction)
        .def("GetFirstAction", &ma::Observable::Data::GetFirstAction)
        .def("GetActions", &ma::Observable::Data::GetActions)
        .def("GetCount", &ma::Observable::Data::GetCount)
        .def("SetAction", &ma::Observable::Data::SetAction)
        .def("Clear", &ma::Observable::Data::Clear)
        .def("GetSize", &ma::Observable::Data::GetSize)
        .def("Merge", &ma::Observable::Data::Merge)
        .def("__repr__",
            [](const ma::Observable::Data &data)
            {
                return std::wstring(L"<ma.Observable.Data( '") + data.toString() + std::wstring(L"' )>");
            });

    class PyObserver : public ma::Observer
    {
        public:
            /* Inherit the constructors */
            using ma::Observer::Observer;

            /// Trampoline (need one for each virtual function)
            void BeginObservation(const ma::ObservablePtr& observable) override
            {
                PYBIND11_OVERLOAD_PURE(
                    void,             // Return type
                    ma::Observer,     // Parent class
                    BeginObservation, // Name of function in C++ (must match Python name)
                    observable        // Argument(s) (...)
                );
            }

            /// Trampoline (need one for each virtual function)
            void UpdateObservation(const ma::ObservablePtr& observable, const ma::Observable::Data& data) override
            {
                PYBIND11_OVERLOAD_PURE(
                    void,              // Return type
                    ma::Observer,      // Parent class
                    UpdateObservation, // Name of function in C++ (must match Python name)
                    observable, data   // Argument(s) (...)
                );
            }

            /// Trampoline (need one for each virtual function)
            void EndObservation(const ma::ObservablePtr& observable) override
            {
                PYBIND11_OVERLOAD_PURE(
                    void,           // Return type
                    ma::Observer,  // Parent class
                    EndObservation, // Name of function in C++ (must match Python name)
                    observable      // Argument(s) (...)
                );
            }
    };

    ::py::class_<ma::Observer>(in_module, "Observer")
        .def("BeginObservation", &ma::Observer::BeginObservation)
        .def("UpdateObservation", &ma::Observer::UpdateObservation)
        .def("EndObservation", &ma::Observer::EndObservation)
        .def("GetDestroyCall", &ma::Observer::GetDestroyCall)
        .def("GetInstanceInfo", &ma::Observer::GetInstanceInfo)
        .def("GetTypeInfo", &ma::Observer::GetTypeInfo)
        .def("__repr__",
            [](const ma::ObserverPtr &observer)
            {
                MA_ASSERT(observer,
                          L"observer est nulle.",
                          L"ma::py::Observer::Observer::__repr__",
                          std::invalid_argument);

                return std::wstring(L"<ma.Observer( '") + observer->GetTypeInfo().m_ClassName + std::wstring(L"' )>");
            });
}

