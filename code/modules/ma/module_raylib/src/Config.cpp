/// \file Config.cpp
/// \author Pontier Pierre
/// \date 2023-12-3
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Raylib/Config.h"

namespace ma::Raylib
{
    const ConfigGroup::key_type ConfigGroup::ms_Key(L"9051f961-1760-43da-a2e5-27ed77bc75ef");

    // ConfigCycle //———————————————————————————————————————————————————————————————————————————————————————————————
    ConfigGroup::ConfigGroup(const Item::ConstructorParameters &params, const std::filesystem::path& module):
    ma::ConfigGroup(params, L"panda")
    {}

    const TypeInfo& ConfigGroup::GetPandaConfigGroupTypeInfo()
    {
        static const TypeInfo info =
        {
            GetPandaConfigGroupClassName(),
            L"Le groupe de données qui sont enregistrés dans le fichier de configuration l'application.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };

        return info;
    }

    M_CPP_ITEM_TYPE_VAR_WITH_NAMESPACE(ConfigGroup, Raylib)

} // namespace ma::Raylib
M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ConfigGroup, Raylib, ConfigGroup)
