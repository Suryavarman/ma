/// \file View.cpp
/// \author Pontier Pierre
/// \date 2019-07-25
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

// PLATFORM Flag 	Platform/OS 	OpenGL version 	Notes
// PLATFORM_DESKTOP 	Windows 	OpenGL ES 2.0 	Support through two possible ways:
// - WGL_EXT_create_context_es2_profile extension
// - ANGLE project
// PLATFORM_DESKTOP 	Linux 	    OpenGL ES 2.0 	Support through:
// - GLX_EXT_create_context_es2_profile extension
//
// Je pense qu'il doit être possible de créer une fenêtre glfw avec pour parent la vue.
// https://www.glfw.org/docs/latest/group__window.html#ga3555a418df92ad53f917597fe2f64aeb



#include "nativeWindowHandle.h"

#if MA_PLATFORM == MA_PLATFORM_LINUX
    #include <pre_x11_include.h>
    #include <gtk/gtk.h>
    // https://stackoverflow.com/questions/14788439/getting-x11-window-handle-from-gtkwidget
    #include <gdk/gdkx.h>
    #include <post_x11_include.h>
#elif MA_PLATFORM == MA_PLATFORM_APPLE
//
//    #include <gdk/gdkquartz.h>
//    #include <gtk/gtk.h>
//    #include <wx/defs.h>
//
#endif // MA_PLATFORM

#include "Raylib/View.h"

#include <ma/Macros.h>
#include <ma/Platform.h>
#include <ma/Prerequisites.h>

#include "Raylib/Raylib.h"


namespace ma::Raylib
{
    const int View::ID_RENDER_TIMER = wxNewId();

    View::View(wxWindow *parent,
               wxWindowID id,
               const wxPoint &pos,
               const wxSize &size,
               long style,
               const wxValidator &validator,
               const wxString &name):
    wxControl(parent, id, pos, size, style, validator, name),
    m_ViewSmile(new ViewSmile()),
    m_Timer(new wxTimer(this, ID_RENDER_TIMER)),
    m_DeltaTimeCallFrame(1. / 60.)
    {
        InitEvents();
    }

    View::View():
    wxControl(),
    m_ViewSmile(new ViewSmile()),
    m_Timer(new wxTimer(this, ID_RENDER_TIMER)),
    m_DeltaTimeCallFrame(1. / 60.)
    {
        InitEvents();
    }

    View::~View()
    {
        if(m_Timer->IsRunning())
            m_Timer->Stop();

        if(m_ViewSmile->m_RenderWindow)
        {
            try
            {
                auto root = ma::Item::GetItem<Root>(Root::GetKeyRoot());
                auto& framework = root->m_RootSmile->m_Framework;

                framework.close_window(m_ViewSmile->m_RenderWindow);
                m_ViewSmile->m_RenderWindow = nullptr;
            }
            catch(const std::exception &e)
            {
                MA_ASSERT(false,
                          L"La destruction d'une ::Panda::RenderWindow a échouée: \n"s + ma::to_wstring(e.what()),
                          std::runtime_error);
            }
        }
    }

    void View::InitEvents()
    {
        Bind(wxEVT_SIZE, &View::OnSize, this);
        // Bind(wxEVT_PAINT, &View::OnPaint, this); // Produces flickers and runs too fast!
        // Bind(wxEVT_ERASE_BACKGROUND, &View::OnEraseBackground);
        Bind(wxEVT_TIMER, &View::OnRenderTimer, this);
        ToggleTimerRendering();
        m_Timer->Start(std::floor(m_DeltaTimeCallFrame * 1000.));
    }

    void View::setRenderWindow(const std::wstring &name)
    {
        int width, height;
        GetSize(&width, &height);

        auto root = ma::Item::GetItem<Root>(Root::GetKeyRoot());
        auto& framework = root->m_RootSmile->m_Framework;

        // https://discourse.panda3d.org/t/wxpython-hosting-a-panda3d-display/2240/25

#if MA_PLATFORM == MA_PLATFORM_WIN32
        auto handle = ::NativeWindowHandle::make_win((HWND)GetHandle()));
#elif defined(__WXGTK__) // GTK3
    #if defined(GDK_WINDOWING_X11)
        GtkWidget *widget = GetHandle();
        gtk_widget_realize(widget); // Mandatory. Otherwise, a segfault happens.
        GdkWindow *gdk_window = gtk_widget_get_window(widget);
        auto wid = GDK_WINDOW_XID(gdk_window); // Window is a typedef for XID, which is a typedef for unsigned int
        auto handle = ::NativeWindowHandle::make_int(wid);
    #elif defined(GDK_WINDOWING_QUARTZ)
        // GdkQuartzDisplay *display = GDK_QUARTZ_DISPLAY(gdk_window_get_display(gdk_window));
        // GdkDisplay *display = gdk_window_get_display(gdk_window);
        // std::string displayStr = display->gdk_display_get_name()
        // handleStream << displayStr;
        handle = ""; // handleStream.str();
        #error Has to be implemented
    #else
        #error Not supported on this platform.
    #endif
#else
    #error Not supported on this platform.
#endif

        ::WindowProperties properties;
        framework.get_default_window_props(properties);
        properties.set_parent_window(handle);
        properties.set_size(width, height);
        properties.set_origin(0, 0);

        try
        {
            // https://docs.panda3d.org/1.10/cpp/programming/rendering-process/introducing-graphics-classes
            // https://docs.panda3d.org/1.10/cpp/programming/render-to-texture/low-level-render-to-texture
            auto flags = GraphicsPipe::BF_require_window;
            auto* render_window = framework.open_window(properties, flags, framework.get_default_pipe(), root->m_RootSmile->m_Guardian);

            m_ViewSmile->m_RenderWindow = render_window;
            // Pour aider à déboguer.
            //render_window->set_wireframe(true);
            //render_window->set_two_sided(true);
            //render_window->set_lighting(true);

            if(!root->m_RootSmile->m_Guardian)
                root->m_RootSmile->m_Guardian = render_window->get_graphics_output()->get_gsg();
        }
        catch(const std::exception &e)
        {
            MA_ASSERT(false,
                      L"La création d'une ::Panda::RenderWindow a échouée: \n"s + ma::to_wstring(e.what()),
                      std::runtime_error);
        }
    }

    void View::OnSize(wxSizeEvent &event)
    {
        if(m_ViewSmile->m_RenderWindow)
        {
            static int width;
            static int height;
            GetSize(&width, &height);
            // ::WindowProperties properties;
            // properties.set_size(width, height);
            //m_ViewSmile->m_RenderWindow->request_properties(properties);
            m_ViewSmile->m_RenderWindow->adjust_dimensions();
        }
    }

    void View::OnRenderTimer(wxTimerEvent &event)
    {
        if(m_ViewSmile->m_RenderWindow)
        {
            Thread *current_thread = Thread::get_current_thread();
            m_ViewSmile->m_RenderWindow->get_panda_framework()->do_frame(current_thread);
        }
    }

    void View::ToggleTimerRendering()
    {
        if(m_Timer->IsRunning())
            m_Timer->Stop();
        else
            m_Timer->Start(std::floor(m_DeltaTimeCallFrame * 1000.));
    }
} // namespace ma::Raylib