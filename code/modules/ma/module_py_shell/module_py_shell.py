import wx
import wx.aui
from wx.py import shell, version
import ma
import images


class ShellPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, style=wx.SUNKEN_BORDER)

        intro = 'Bienvenu sur PyCrust %s - La moins mauvaise (falbkiest?!) des consoles python.' % version.VERSION

        # https://wiki.wxpython.org/PyCrust
        # http://cvs.wxwindows.org/viewcvs.cgi/
        self.shell = shell.Shell(self, -1, introText=intro)

        self.shell.setStatusText = self.SetStatusText

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.shell, 1, wx.EXPAND | wx.BOTTOM | wx.LEFT | wx.RIGHT, 10)

        self.SetSizer(sizer)

        self.mother_frame = ma.wx.get_mother_frame()
        # Override the shell so that status messages go to the status bar.
        self.shell.setStatusText = self.SetStatusText
        self.shell.autoSaveHistory = True

        self.shell.SetFocus()

    def OnClose(self, event):
        """Event handler for closing."""
        # This isn't working the way I want, but I'll leave it for now.
        if self.shell.waiting:
            if event.CanVeto():
                event.Veto(True)
        else:
            self.shell.destroy()

    def SetStatusText(self, text):
        self.mother_frame.SetStatusText(text)


class PyShell(ma.wx.View):
    instance = None

    def __init__(self, parent_key: str):
        ma.wx.View.__init__(self, parent_key, "PyShell")
        self.AddStringVar(ma.Item.Key.Var.GetDoc(), u"Console Python")
        key = self.GetKey()
        ma.wx.Tag.Position.Bottom(key)
        ma.wx.Tag.Axis.Y(key)
        ma.wx.Tag.Type.Text(key)

    def BuildWindow(self, parent: wx.Window):
        return ShellPanel(parent)


def py_start_plugin():
    if PyShell.instance is None:
        shell = PyShell(ma.wx.Views.Key.Get())
        PyShell.instance = shell

        # notebook = ma.wx.get_bottom()
        # ShellPanel.instance = ShellPanel(notebook)
        # notebook.AddPage(ShellPanel.instance,
        #                  "Console Python",
        #                  False,
        #                  wx.Bitmap(images.py_crust_16_png_path, wx.BITMAP_TYPE_ANY))

        # ajouter le menu console celui-ci doit gérer cette instance.


def py_stop_plugin():
    if PyShell.instance is not None:
        # todo nettoyer les variables locales etc

        ma.root().m_Views.RemoveChild(PyShell.instance.GetKey())
        PyShell.instance = None

        # notebook = ma.wx.get_bottom()
        # index = notebook.FindPage(ShellPanel.instance)
        # assert index != wx.NOT_FOUND, "L'instance de la console ne correspond à aucun onglet."
        # notebook.DeletePage(index)
        # ShellPanel.instance = None
