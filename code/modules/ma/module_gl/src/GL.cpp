/// \file gl/GL.cpp
/// \author Pontier Pierre
/// \date 2023-05-28
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "gl/GL.h"

namespace ma::gl
{
    std::string GetNewName(ma::ItemPtr item, const std::wstring &default_base_name)
    {
        auto node_name = default_base_name;

        if(item->HasVar(Item::Key::Var::GetName()))
            node_name = item->GetVar(Item::Key::Var::GetName())->toString();

        if(node_name.empty())
            node_name = L"node";

        node_name += L"_" + item->GetKey();

        return ma::to_string(node_name);
    }

    // ContextSmile // —————————————————————————————————————————————————————————————————————————————————————————————————
    ContextSmile::ContextSmile():
    m_Camera{}
    //m_Context{nullptr}
    {}

    ContextSmile::~ContextSmile() = default;

    // Nodef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    Nodef::Nodef(const ma::Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    ma::Data::Data(in_params, link, client),
    ma::Observer(),
    m_GLContext(std::dynamic_pointer_cast<ma::gl::Context>(link->GetContext())),
    m_ItemNode(std::dynamic_pointer_cast<ma::Engine::Nodef>(client))
    {}

    Nodef::~Nodef() = default;

    void Nodef::EndConstruction()
    {
        //MA_ASSERT(m_GLContext, L"m_GLContext est nul.", std::logic_error);
        //MA_ASSERT(m_GLContext->m_ContextSmile, L"m_GLContext->m_ContextSmile est nul.", std::logic_error);
        MA_ASSERT(m_ItemNode, L"m_ItemScene est nul.", std::logic_error);
        MA_ASSERT(ma::Item::HasItem(m_ItemNode->GetParentKey()), L"m_ItemNode n'a pas de parent.", std::logic_error);

        auto gl_item_parent = ma::Item::GetItem(GetParentKey());

        // Cette fonction déclenchera l'évènement OnEnable qui va appeler Load et qui aura besoin de m_Node
        ma::Item::EndConstruction();

        if(ma::Item::HasItem(Key::GetObservationManager()))
        {
            auto observer_manager = ma::Item::GetItem<ma::ObservationManager>(Key::GetObservationManager());
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Position);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Quaternion);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Euler);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Scale);
        }
        else
        {
            MA_WARNING(false,
                       L"Le gestionnaire d'observations n'ai pas disponible. \n"
                       L"Aucun item n'est lié à la clef qui lui est attitrée: « " +
                           Key::GetObservationManager() + L" ». Ce nœud ne pourra pas observer les variables: \n" +
                           L" - m_Position \n - m_Quaternion \n - m_Euler \n - m_Scale");
        }
    }

    void Nodef::SetObservation(const ma::ObservablePtr &observable)
    {
        // Test
        MA_ASSERT(IsEnable(), L"Ne devrait pas être appelé si le nœud est inactif.");

//        if(m_ItemNode->m_Position == observable || m_ItemNode->m_Quaternion == observable ||
//           m_ItemNode->m_Scale == observable || m_ItemNode->m_Euler)
//        {
//            const auto &eigen_transformation = m_ItemNode->m_Transform->Get();
//
//            //*
//            MA_ASSERT(eigen_transformation.cols() * eigen_transformation.rows() >= 12,
//                      L"Le nombre d'éléments est inférieur à celui requis.",
//                      std::logic_error);
//
//            auto *data = eigen_transformation.data();
//
//            m_Transform.x.x = *data++;
//            m_Transform.x.y = *data++;
//            m_Transform.x.z = *data++;
//            m_Transform.x.w = *data++;
//            m_Transform.y.x = *data++;
//            m_Transform.y.y = *data++;
//            m_Transform.y.z = *data++;
//            m_Transform.y.w = *data++;
//            m_Transform.z.x = *data++;
//            m_Transform.z.y = *data++;
//            m_Transform.z.z = *data++;
//            m_Transform.z.w = *data;
//        }

        m_GLContext->GetView()->Refresh();

        if(m_ItemNode->m_Position == observable || m_ItemNode->m_Quaternion == observable ||
           m_ItemNode->m_Scale == observable || m_ItemNode->m_Euler == observable)
        {
            const auto &position = m_ItemNode->m_Position->Get();
            const auto &euler = m_ItemNode->m_Euler->Get();
            const auto &scale = m_ItemNode->m_Scale->Get();
        }
    }

    void Nodef::BeginObservation(const ObservablePtr &observable)
    {
        SetObservation(observable);
    }

    void Nodef::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        SetObservation(observable);
    }

    void Nodef::EndObservation(const ObservablePtr &observable)
    {
        // m_GLContext.reset();
    }

    void Nodef::Display()
    {}

    void Nodef::OnEnable()
    {
        Observer::OnEnable();
        Data::OnEnable();
    }

    void Nodef::OnDisable()
    {
        Observer::OnDisable();
        Data::OnDisable();
    }

    bool Nodef::Load()
    {
        return Data::Load();
    }

    bool Nodef::UnLoad()
    {
        // m_GLContext.reset();

        return Data::UnLoad();
    }

    bool Nodef::Reload()
    {
        return Data::Reload();
    }

    const TypeInfo& Nodef::GetglNodefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetglNodefClassName(),
            L"Nodef sera placé enfant du « ma::gl::ITEM » héritant de « ma::Data ».\n"
            L"Nodef met à jour m_Node à partir de l'observation du « NodefPtr » "
            L"qu'observe\n « ma::gl::ITEM »..\n",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Scenef //————————————————————————————————————————————————————————————————————————————————————————————————————————
    Scenef::Scenef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemScene(std::dynamic_pointer_cast<ma::Engine::Scenef>(client))
    {
        MA_ASSERT(m_ItemScene, L"m_ItemScene est nul.", std::invalid_argument);
    }

    Scenef::~Scenef() = default;

    void Scenef::Display()
    {}

    const TypeInfo& Scenef::GetglScenefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetglScenefClassName(),
            L"Implémentation de ma::Scenef pour Cycles.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cubef //—————————————————————————————————————————————————————————————————————————————————————————————————————————
    Cubef::Cubef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemCube(std::dynamic_pointer_cast<ma::Engine::Cubef>(client))
    {}

    Cubef::~Cubef() = default;

    void Cubef::Display()
    {}

    const TypeInfo& Cubef::GetglCubefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetglCubefClassName(),
            L"Implémentation de ma::Cubef pour Cycle.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cameraf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Cameraf::Cameraf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemCamera(std::dynamic_pointer_cast<ma::Engine::Cameraf>(client))
    {}

    Cameraf::~Cameraf() = default;

    void Cameraf::EndConstruction()
    {
        Nodef::EndConstruction();

        MA_ASSERT(m_ItemCamera, L"m_ItemCamera est nul.", std::logic_error);

        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_BackgroundColour);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Far);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Near);
        }
    }

    void Cameraf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);
        // this->m_Link->GetItem();
        m_GLContext->m_ContextSmile->m_Camera = m_ItemCamera;
    }

    void Cameraf::Display()
    {}

    const TypeInfo& Cameraf::GetglCamerafTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetglCamerafClassName(),
            L"Implémentation de ma::Engine::Cameraf pour Cycles.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Lightf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Lightf::Lightf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemLight(std::dynamic_pointer_cast<ma::Engine::Lightf>(client))
    {}

    Lightf::~Lightf() = default;

    void Lightf::EndConstruction()
    {
        Nodef::EndConstruction();

        MA_ASSERT(m_ItemLight, L"m_ItemLight est nul.", std::invalid_argument);
    }

    void Lightf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);
    }

    void Lightf::Display()
    {}

    const TypeInfo& Lightf::GetglLightfTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetglLightfClassName(),
            L"Implémentation de ma::Engine::Lightf pour OpnGL.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

} // namespace ma::Cycles

M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, gl, Data, Observer)
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Scenef , gl, ma::gl::Nodef::GetglNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cubef  , gl, ma::gl::Nodef::GetglNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cameraf, gl, ma::gl::Nodef::GetglNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Lightf , gl, ma::gl::Nodef::GetglNodefClassHierarchy())

M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, gl, ma::Engine::Scenef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, gl, ma::Engine::Cubef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, gl, ma::Engine::Cameraf)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, gl, ma::Engine::Lightf)
