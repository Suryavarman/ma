/// \file gl/View.cpp
/// \author Pontier Pierre
/// \date 2023-12-04
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// https://wiki.wxwidgets.org/WxGLCanvas
///

#include <memory>
#include <utility>

#include "gl/View.h"
#include "gl/GL.h"

#if !wxUSE_GLCANVAS
    #error "OpenGL required: set wxUSE_GLCANVAS to 1 and rebuild the library"
#else
    #if MA_PLATFORM == MA_PLATFORM_LINUX
        #include <EGL/egl.h>
        #include <GL/glu.h>
    #endif
#endif

static void CheckGLError()
{
    GLenum errLast = GL_NO_ERROR;

    for ( ;; )
    {
        GLenum err = glGetError();
        if ( err == GL_NO_ERROR )
            return;

        // normally the error is reset by the call to glGetError() but if
        // glGetError() itself returns an error, we risk looping forever here
        // so check that we get a different error than the last time
        if ( err == errLast )
        {
            wxLogError(wxT("OpenGL error state couldn't be reset."));
            return;
        }

        errLast = err;

        wxLogError(wxT("OpenGL error %d"), err);
    }
}

namespace ma::gl
{
    wxString glGetwxString(GLenum name)
    {
        const GLubyte *v = glGetString(name);
        if (v == nullptr)
        {
            // The error is not important. It is GL_INVALID_ENUM.
            // We just want to clear the error stack.
            glGetError();

            return {};
        }

        return {(const char *) v};
    }

    // GLContext //—————————————————————————————————————————————————————————————————————————————————————————————————————
    wxGLContext::wxGLContext(wxGLCanvas *canvas,
                             const wxGLContext *other,
                             const wxGLContextAttrs *ctxAttrs) :
    ::wxGLContext(canvas, other, ctxAttrs)
    {}

    wxGLContext::~wxGLContext() = default;

    void wxGLContext::Init()
    {
        // set up the parameters we want to use
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_TEXTURE_2D);

        // add slightly more light, the default lighting is rather dark
        GLfloat ambient[] = {0.5, 0.5, 0.5, 0.5};
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);

        // set viewing projection
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-0.5f, 0.5f, -0.5f, 0.5f, 1.0f, 3.0f);

        // create the textures to use for cube sides: they will be reused by all
        // canvases (which is probably not critical in the case of simple textures
        // we use here but could be really important for a real application where
        // each texture could take many megabytes)
        // Créé une texture pour le plan. ELle sera réutilisée pour tout les wxCanvas
        glGenTextures(1u, &m_Texture);

        glBindTexture(GL_TEXTURE_2D, m_Texture);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

//            const wxImage img(DrawDice(256, i + 1));
//
//            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
//            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.GetWidth(), img.GetHeight(),
//                         0, GL_RGB, GL_UNSIGNED_BYTE, img.GetData());


        CheckGLError();
    }

    void wxGLContext::DrawPlane()
    {
        glTranslatef(0.0f, 0.0f, -2.0f);

        //glBindTexture(GL_TEXTURE_2D, m_Texture);
        glBegin(GL_QUADS);
        {
            glNormal3f(0.0f, 0.0f, 1.0f);
            glTexCoord2f(0, 0);
            glVertex3f(0.5f, 0.5f, 0.5f);
            glTexCoord2f(1, 0);
            glVertex3f(-0.5f, 0.5f, 0.5f);
            glTexCoord2f(1, 1);
            glVertex3f(-0.5f, -0.5f, 0.5f);
            glTexCoord2f(0, 1);
            glVertex3f(0.5f, -0.5f, 0.5f);
        }
        glEnd();
    }

    // View //——————————————————————————————————————————————————————————————————————————————————————————————————————
    // const int View::ID_RENDER_TIMER = wxNewId();

    const wxGLAttributes &GetAttributes()
    {
        static wxGLAttributes attributes;
        attributes.PlatformDefaults().EndList(); // DoubleBuffer().Depth(32).EndList();
        return attributes;
    }

    View::View(wxWindow *parent, ContextSmilePtr context) :
    // With perspective OpenGL graphics, the wxFULL_REPAINT_ON_RESIZE style
    // flag should always be set, because even making the canvas smaller should
    // be followed by a paint event that updates the entire canvas with new
    // viewport settings.
    wxGLCanvas(parent,
               GetAttributes(),
               wxID_ANY,
               parent->GetPosition(),
               parent->GetSize(),
               wxFULL_REPAINT_ON_RESIZE | wxTRANSPARENT_WINDOW),
    m_GLContext(nullptr),
    m_ContextSmile(std::move(context))
    {
        InitEvents();
        InitContext();
    }

    View::~View()
    {}

#if MA_PLATFORM == MA_PLATFORM_LINUX
    void eglDisplayError()
    {
        auto error = eglGetError();

        switch (error)
        {
            case EGL_SUCCESS:
                std::wcout << L"La dernière fonction a été exécutée sans erreur." << std::endl;
                break;

            case EGL_NOT_INITIALIZED:
                std::wcout << L"EGL n'est pas initialisé, ou n'a pas peu être initialisé, pour la connexion de l'EGL "
                              L"qui a été spécifié." << std::endl;
                break;

            case EGL_BAD_ACCESS:
                std::wcout << L"EGL ne peut accéder la ressource demandée, par exemple un contexte est lié à un autre "
                              L"« thread »." << std::endl;
                break;

            case EGL_BAD_ALLOC:
                std::wcout << L"EGL n'a pas réussi à allouer la ressource pour l'opération demandée." << std::endl;
                break;

            case EGL_BAD_ATTRIBUTE:
                std::wcout << L"Un attribut ou une valeur d'attribut non reconnu fait parti de la liste d'attributs."
                           << std::endl;
                break;

            case EGL_BAD_CONTEXT:
                std::wcout << L"Le contexte passé en paramètre ne représente pas un contexte valide de rendu EGL."
                           << std::endl;
                break;

            case EGL_BAD_CONFIG:
                std::wcout << L"Un argument EGLConfig ne nomme pas une configuration EGL frame buffer valide."
                           << std::endl;
                break;

            case EGL_BAD_CURRENT_SURFACE:
                std::wcout << L"La surface courante du thread de la vidéo, du pixel buffer ou du « pixmap », "
                              L"n'est plus valide. " << std::endl;
                break;

            case EGL_BAD_DISPLAY:
                std::wcout << L"An EGLDisplay argument does not name a valid EGL display connection." << std::endl;
                break;

            case EGL_BAD_SURFACE:
                std::wcout << L"An EGLSurface argument does not name a valid surface (window, pixel buffer or pixmap)"
                              L" configured for GL rendering." << std::endl;
                break;

            case EGL_BAD_MATCH:
                std::wcout << L"Arguments are inconsistent (for example, a valid context requires buffers not supplied"
                              L" by a valid surface)." << std::endl;
                break;

            case EGL_BAD_PARAMETER:
                std::wcout << L"Au moins un des arguments n'est pas valide." << std::endl;
                break;

            case EGL_BAD_NATIVE_PIXMAP:
                std::wcout << L"A NativePixmapType argument does not refer to a valid native pixmap." << std::endl;
                break;

            case EGL_BAD_NATIVE_WINDOW:
                std::wcout << L"A NativeWindowType argument does not refer to a valid native window." << std::endl;
                break;

            case EGL_CONTEXT_LOST:
                std::wcout << L"A power management event has occurred. The application must destroy all contexts and"
                              L" reinitialise OpenGL ES state and objects to continue rendering." << std::endl;
                break;

            default:
                std::wcout << L"Erreur Egl inconnue. Son code est le suivant: " << error << std::endl;
        }
    }
#endif // MA_PLATFORM == MA_PLATFORM_LINUX

    void View::InitContext()
    {
#if MA_PLATFORM == MA_PLATFORM_LINUX
        // Permet de connaitre l'erreur
        if (!GetEGLConfig())
        {
            const auto &wx_attrsList = GetAttributes();
            const int *attrsList = wx_attrsList.GetGLAttrs();

            EGLDisplay dpy = GetDisplay();

            auto *config = new EGLConfig;
            int returned;

            // https://registry.khronos.org/EGL/sdk/docs/man/html/eglChooseConfig.xhtml
            // Use the first good match
            if (!eglChooseConfig(dpy, attrsList, config, 1, &returned)) {
                eglDisplayError();

                delete config;
                config = nullptr;
            }

            bool result = InitVisual(wx_attrsList);
            if (!result)
                eglDisplayError();

            MA_WARNING(result, L"L'initialisation des attributs d'OpenGL a échouée.");
        }
#endif

        // Explicitly create a new rendering context instance for this canvas.
        wxGLContextAttrs ctxAttrs;

        // An impossible context, just to test IsOk()
        ctxAttrs.PlatformDefaults().EndList();
        m_GLContext = std::make_unique<wxGLContext>(this, nullptr, &ctxAttrs);

        if (!m_GLContext->IsOK())
        {
            MA_WARNING(false,
                       L"Échec de la création d'un contexte opengl avec les paramètres par défauts.");

            ctxAttrs.Reset();

            ctxAttrs.PlatformDefaults().CoreProfile().OGLVersion(3, 2).EndList();

            m_GLContext = std::make_unique<wxGLContext>(this, nullptr, &ctxAttrs);
        }

        MA_WARNING(m_GLContext->IsOK(), L"m_GLContext->IsOK() == false.");
        if (m_GLContext->IsOK())
            m_GLContext->Init();
    }

    void View::InitEvents()
    {
        Bind(wxEVT_SIZE, &View::OnSize, this);
        //Bind(wxEVT_KEY_DOWN, &View::OnKeyDown, this);
        Bind(wxEVT_PAINT, &View::OnPaint, this);
    }

    void View::OnPaint(wxPaintEvent &event)
    {
        if (m_GLContext && !m_GLContext->IsOK())
        {
            m_GLContext.reset();
            InitContext();
        }

        if (m_GLContext)
        {
            SetCurrent(*m_GLContext);
            if (!IsShown())
                return;

            wxPaintDC dc(this);

            // Set the OpenGL viewport according to the client size of this canvas.
            // This is done here rather than in a wxSizeEvent handler because our
            // OpenGL rendering context (and thus viewport setting) is used with
            // multiple canvases: If we updated the viewport in the wxSizeEvent
            // handler, changing the size of one canvas causes a viewport setting that
            // is wrong when next another canvas is repainted.
            const wxSize ClientSize = GetClientSize();

            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LEQUAL);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            auto camera = m_ContextSmile->m_Camera;
            if(camera)
            {
                const auto& colour = camera->m_BackgroundColour->Get();
                glClearColor(static_cast<GLfloat>(colour.Red()) / 255.f,
                             static_cast<GLfloat>(colour.Green()) / 255.f,
                             static_cast<GLfloat>(colour.Blue()) / 255.f,
                             static_cast<GLfloat>(colour.Alpha()) / 255.f);
            }
            else
                glClearColor((GLfloat) 0.15, (GLfloat) 0.15, 0.0, (GLfloat) 1.0); // Dark, but not black.

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glClearDepth(1.0f);    // Depth Buffer Setup
            glEnable(GL_DEPTH_TEST); // Enables Depth Testing
            glDepthFunc(GL_LEQUAL); // The Type Of Depth Testing To Do
            glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

            glEnable(GL_COLOR_MATERIAL);

            glViewport(0, 0, ClientSize.x, ClientSize.y);
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();

            const float ratio_w_h = static_cast<float>(ClientSize.x) / static_cast<float>(ClientSize.y);
            const float fovy = 45.0f;

            if(camera)
            {
                const auto near = camera->m_Near->Get();
                const auto far = camera->m_Far->Get();
                gluPerspective(fovy, ratio_w_h, near, far);
            }
            else
                gluPerspective(fovy, ratio_w_h, 0.1, 200.0);

            glMatrixMode(GL_MODELVIEW);

            glLoadIdentity();
            m_GLContext->DrawPlane();
            glFlush();

            CheckGLError();

            SwapBuffers();
        }
    }

    void View::OnSize(wxSizeEvent &event)
    {
        Refresh();
    }

    void View::OnKeyDown(wxKeyEvent &event)
    {
        switch (event.GetKeyCode())
        {
            default:
                event.Skip();
        }
    }
}