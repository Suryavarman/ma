/// \file gl/Context.cpp
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <ma/wx/GuiAccess.h>

#include "gl/Context.h"
#include "gl/GL.h"

namespace ma::gl
{
    // Context //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Context::Context(const Item::ConstructorParameters &in_params):
    ma::Engine::Context(in_params, L"Opengl"),
    m_View(nullptr),
    m_ContextSmile(new ContextSmile())
    {}

    Context::~Context() = default;

    void Context::EndConstruction()
    {
        ma::Context::EndConstruction();

        AddDataMaker<ma::gl::ScenefDataMaker>();
        AddDataMaker<ma::gl::CubefDataMaker>();
        AddDataMaker<ma::gl::CamerafDataMaker>();
        AddDataMaker<ma::gl::LightfDataMaker>();
    }

    wxWindow* Context::GetView()
    {
        return m_View;
    }

    void Context::OnClosePage(wxAuiNotebookEvent &event)
    {
        if(m_View)
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            if(m_View == center_book->GetPage(event.GetSelection()))
            {
                // Pour éviter d'appeler de nouveau la destruction de m_View.
                m_View = nullptr;
                if(auto parent = GetParentOpt())
                    parent.value()->RemoveChild(GetKey());
            }
        }
    }

    void Context::OnEnable()
    {
        ma::Context::OnEnable();

        if(ma::Item::HasItem(ma::Item::Key::GetGuiAccess()))
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();

            m_View = new ma::gl::View(center_book, m_ContextSmile);

            const auto name = L"OpenGLView: " + GetKey();
            center_book->AddPage(m_View, name, true);

            center_book->Bind(wxEVT_AUINOTEBOOK_PAGE_CLOSE, &Context::OnClosePage, this);
        }
    }

    void Context::OnDisable()
    {
        ma::Context::OnDisable();

        if(m_View)
        {
            auto view = m_View;
            m_View = nullptr;
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            const auto page_index = center_book->GetPageIndex(view);
            center_book->DeletePage(page_index);
        }

        RemoveChildren();
    }

    const TypeInfo& Context::GetglContextTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetglContextClassName(),
            L"Le contexte pour afficher le rendu OpenGL.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

} // namespace ma::gl
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Context, gl, ma::Engine::Context::GetEngineContextClassHierarchy())
M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Context, gl)