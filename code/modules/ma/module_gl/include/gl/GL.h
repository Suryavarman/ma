/// \file gl/GL.h
/// \author Pontier Pierre
/// \date 2023-12-04
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \remarks Point d'entré du module.
/// \see Liens en vrac :
/// https://stackoverflow.com/questions/69201843/how-do-you-do-3d-rendering-in-gtk-using-opengl
///

#pragma once

#include <ma/Ma.h>
#include "gl/Context.h"

namespace ma::gl
{
    // ContextSmile // —————————————————————————————————————————————————————————————————————————————————————————————————
    class ContextSmile
    {
        public:
            //  /// Permet d'avoir un seul context pour ce module et ainsi de partager les ressources.
            //  ma::gl::wxGLContext* m_GLContext;

            /// La caméra injectera sa transformation ici lors de ses mises à jour.
            std::shared_ptr<ma::Engine::Cameraf> m_Camera;

            ContextSmile();
            virtual ~ContextSmile();
    };

    // Nodef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    class Nodef: public ma::Data, public ma::Observer
    {
        protected:
            virtual void SetObservation(const ma::ObservablePtr &observable);

            void OnEnable() override;
            void OnDisable() override;

        public:
            ma::gl::ContextPtr m_GLContext;
            ma::Engine::NodefPtr m_ItemNode;

            explicit Nodef(const ma::Item::ConstructorParameters &in_params,
                           const ContextLinkPtr &link,
                           ma::ItemPtr client);
            Nodef() = delete;
            ~Nodef() override;

            /// Pour finaliser la construction.
            void EndConstruction() override;

            void BeginObservation(const ObservablePtr &observable) override;
            void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
            void EndObservation(const ObservablePtr &) override;

            /// Charge la donnée dans le contexte.
            bool Load() override;

            /// Décharge la donnée du contexte.
            bool UnLoad() override;

            /// Par défaut le comportement est d'abord d'appeler Unload et si celui-ci renvoie vrai.
            bool Reload() override;

            /// Affiche l'objet et renvoie sa transformation dans le repère monde.
            virtual void Display();

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, gl)
    };
    typedef std::shared_ptr<Nodef> NodefPtr;

    // Scenef // ———————————————————————————————————————————————————————————————————————————————————————————————————————
    class Scenef: public Nodef
    {
        public:
            ma::Engine::ScenefPtr m_ItemScene;

            explicit Scenef(const Item::ConstructorParameters &in_params,
                            const ContextLinkPtr &link,
                            ma::ItemPtr client);
            ~Scenef() override;

            void Display() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Scenef, gl)
    };
    typedef std::shared_ptr<Scenef> ScenefPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, gl)

    // Cubef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    class Cubef: public Nodef
    {
        public:
            ma::Engine::CubefPtr m_ItemCube;

            explicit Cubef(const Item::ConstructorParameters &in_params,
                           const ContextLinkPtr &link,
                           ma::ItemPtr client);
            Cubef() = delete;
            ~Cubef() override;

            void Display() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cubef, gl)
    };
    typedef std::shared_ptr<Cubef> CubefPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, gl)

    // Cameraf // ——————————————————————————————————————————————————————————————————————————————————————————————————————
    class Cameraf: public Nodef
    {
        protected:
            void SetObservation(const ma::ObservablePtr &observable) override;

        public:
            ma::Engine::CamerafPtr m_ItemCamera;

            explicit Cameraf(const Item::ConstructorParameters &in_params,
                             const ContextLinkPtr &link,
                             ma::ItemPtr client);
            Cameraf() = delete;
            ~Cameraf() override;

            void EndConstruction() override;

            void Display() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cameraf, gl)
    };
    typedef std::shared_ptr<Cameraf> CamerafPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, gl)

    // Lightf // ———————————————————————————————————————————————————————————————————————————————————————————————————————
    class Lightf: public Nodef
    {
        protected:
            void SetObservation(const ma::ObservablePtr &observable) override;

        public:
            ma::Engine::LightfPtr m_ItemLight;

            explicit Lightf(const Item::ConstructorParameters &in_params,
                            const ContextLinkPtr &link,
                            ma::ItemPtr client);
            Lightf() = delete;
            ~Lightf() override;

            void EndConstruction() override;

            void Display() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Lightf, gl)
    };
    typedef std::shared_ptr<Lightf> LightfPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, gl)

} // namespace ma::gl
