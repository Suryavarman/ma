/// \file gl/View.h
/// \author Pontier Pierre
/// \date 2023-12-04
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <memory>

#include <wx/glcanvas.h>
#include <ma/Ma.h>
#include <ma/wx/wx.h>

// https://github.com/wxWidgets/wxWidgets/blob/WX_3_0_BRANCH/samples/opengl/cube/cube.h

namespace ma::gl
{
    class ContextSmile;
    typedef std::shared_ptr<ContextSmile> ContextSmilePtr;

    class wxGLContext : public ::wxGLContext
    {
        public:
            explicit wxGLContext(wxGLCanvas *win,
                                 const wxGLContext *other = nullptr,
                                 const wxGLContextAttrs *ctxAttrs = nullptr);

            wxGLContext() = delete;
            ~wxGLContext() override;

            void Init();

            /// Rend un plan sur lequel est plaquée la texture du rendu.
            static void DrawPlane();

        private:
            /// L'identifiant de la texture où sera enregistré le rendu.
            GLuint m_Texture;
    };

    class View: public wxGLCanvas
    {
        public:
            View() = delete;
            explicit View(wxWindow *parent, ContextSmilePtr context);
            ~View() override;

            void InitContext();
            void InitEvents();
        private:
            std::unique_ptr<ma::gl::wxGLContext> m_GLContext;
            std::shared_ptr<ContextSmile> m_ContextSmile;

            void OnPaint(wxPaintEvent &event);
            void OnKeyDown(wxKeyEvent& event);
            void OnSize(wxSizeEvent &event);
    };

} // namespace ma::gl
