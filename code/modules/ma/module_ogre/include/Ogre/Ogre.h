/// \file Ogre/Ogre.h
/// \author Pontier Pierre
/// \date 2022-07-12
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \remarks À inclure dans le corps des classes de l'interface à Ogre.
/// Ce fichier ne doit pas être inclus dans les en-têtes du module Ogre. Seulement dans les fichiers .cpp.
/// Sert aussi à la définition des pointeurs opaques.
///

#pragma once

#include <ma/Platform.h>
#include <thread>

#if MA_COMPILER == MA_COMPILER_CLANG
    // https://clang.llvm.org/docs/UsersManual.html#controlling-diagnostics-via-pragmas
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Winconsistent-missing-override"
#endif // MA_COMPILER_CLANG

#include <Ogre.h>
#include <OgrePrerequisites.h>
#include <OgreOverlaySystem.h>

#if MA_COMPILER == MA_COMPILER_CLANG
    #pragma clang diagnostic pop
#endif // MA_COMPILER_CLANG

#include <ma/Context.h>
#include <ma/engine/Node.h>
#include <ma/engine/Scene.h>
#include <ma/engine/Primitive.h>
#include <ma/engine/Camera.h>
#include <ma/engine/Light.h>

#include "Ogre/Context.h"

// https://forums.ogre3d.org/viewtopic.php?p=334608
// http://wiki.ogre3d.org/Integrating+Ogre+into+QT5

#if MA_PLATFORM == MA_PLATFORM_LINUX

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wparentheses"
    #include <gtk/gtk.h>
    #pragma GCC diagnostic pop
    // https://apple.stackexchange.com/questions/205404/how-can-i-get-gdk-x11-3-0-on-os-x
    #include <gdk/gdkx.h>

    // Il vous faudra
    // gtk3
    // glib
    // pango
    // cairo
    // gdk-pixbuf
    // atk

#elif MA_PLATFORM == MA_PLATFORM_APPLE

    //#include <gdk/gdkquartz.h>
    //#include <gtk/gtk.h>
    //#include <wx/defs.h>

#endif // MA_PLATFORM

namespace ma::Ogre
{
    // RootSmile // ————————————————————————————————————————————————————————————————————————————————————————————————————
    class RootSmile
    {
        public:
        ::Ogre::Root *m_Root;
        ::Ogre::OverlaySystem *m_OverlaySystem;

        RootSmile();
        virtual ~RootSmile();
    };

    // ViewSmile // ————————————————————————————————————————————————————————————————————————————————————————————————————
    class ViewSmile
    {
        public:
        ::Ogre::RenderWindow *m_RenderWindow;

        ViewSmile();
        virtual ~ViewSmile();
    };

    // Nodef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    /// NodeSmile sera placé enfant du « ma::Ogre::Item » héritant de « ma::Data ».
    /// NodeSmile met à jour m_OgreNode à partir de l'observation de du « NodefPtr » qu'observe
    /// « ma::Ogre::ITEM ».
    class Nodef:
    public ma::Data,
        public ma::Observer
    {
        protected:
        virtual void SetObservation(const ma::ObservablePtr &observable);

        std::wstring GetSceneManagerName() const;

        ::Ogre::Root *GetOgreRoot() const;

        /// \brief Renvoie le gestionnaire de la scène d'Ogre associée à ce nœud.
        /// \param create_if_not_exist Si vrais, le gestionnaire de scène d'Ogre n'existe pas, il sera donc créé.
        /// \remarks Le gestionnaire de scène d'Ogre est créé par le nœud de tête lié au contexte.
        /// \exception std::logic_error Le gestionnaire de scène est nul et create_if_not_exist == false.
        ::Ogre::SceneManager *GetOgreSceneManager(bool create_if_not_exist = false) const;

        void OnEnable() override;
        void OnDisable() override;

        public:
        ma::Ogre::ContextPtr m_OgreContext;
        ma::Engine::NodefPtr m_ItemNode;
        ::Ogre::SceneNode *m_OgreNode;

        explicit Nodef(const ma::Item::ConstructorParameters &in_params,
                       const ContextLinkPtr &link,
                       ma::ItemPtr client);
        Nodef() = delete;
        ~Nodef() override;

        /// Pour finaliser la construction.
        void EndConstruction() override;

        void BeginObservation(const ObservablePtr &observable) override;
        void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
        void EndObservation(const ObservablePtr &) override;

        /// Charge la donnée dans le contexte.
        bool Load() override;

        /// Décharge la donnée du contexte.
        bool UnLoad() override;

        /// Par défaut le comportement est d'abord d'appeler Unload et si celui-ci renvoie vrai.
        bool Reload() override;
        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, Ogre)
    };

    // Scenef // ———————————————————————————————————————————————————————————————————————————————————————————————————————
    class Scenef:
    public Nodef
    {
        public:
        ma::Engine::ScenefPtr m_ItemScene;

        explicit Scenef(const Item::ConstructorParameters &in_params,
                        const ContextLinkPtr &link,
                        ma::ItemPtr client);
        ~Scenef() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Scenef, Ogre)
    };
    typedef std::shared_ptr<Scenef> ScenefPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, Ogre)

    // Cubef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    class Cubef:
    public Nodef
    {
        public:
        ma::Engine::CubefPtr m_ItemCube;
        ::Ogre::ManualObject *m_Entity;

        explicit Cubef(const Item::ConstructorParameters &in_params,
                       const ContextLinkPtr &link,
                       ma::ItemPtr client);
        Cubef() = delete;
        ~Cubef() override;

        void EndConstruction() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cubef, Ogre)
    };
    typedef std::shared_ptr<Cubef> CubefPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, Ogre)

    // Cameraf // ——————————————————————————————————————————————————————————————————————————————————————————————————————
    class Cameraf:
    public Nodef
    {
        protected:
        void SetObservation(const ma::ObservablePtr &observable) override;

        public:
        ma::Engine::CamerafPtr m_ItemCamera;

        /// \todo Gérer le type selon le type de la scène :
        ///       - ::Ogre::OctreeCamera
        ///       - ::Ogre::PCZCamera
        ::Ogre::Camera *m_Camera;

        /// La vue qui lie la caméra et la fenêtre de rendu.
        ::Ogre::Viewport *m_Viewport;

        /// \exception std::logic_error link->GetContext() renvoie null.
        /// \exception std::logic_error link->GetContext() n'est pas du type ma::Ogre::Context.
        /// \exception std::logic_error  std::dynamic_pointer_cast<ma::Ogre::Context>(link->GetContext())->m_View
        /// est nul.
        /// \exception std::runtime_error
        /// std::dynamic_pointer_cast<ma::Ogre::Context>(link->GetContext())->m_View->m_ViewSmile est nul.
        /// \exception std::runtime_error
        /// std::dynamic_pointer_cast<ma::Ogre::Context>(link->GetContext())->m_View->m_ViewSmile->m_RenderWindow
        /// est nul.
        explicit Cameraf(const Item::ConstructorParameters &in_params,
                         const ContextLinkPtr &link,
                         ma::ItemPtr client);
        Cameraf() = delete;
        ~Cameraf() override;

        void EndConstruction() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cameraf, Ogre)
    };
    typedef std::shared_ptr<Cameraf> CamerafPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, Ogre)

    // Lightf // ———————————————————————————————————————————————————————————————————————————————————————————————————————
    class Lightf:
    public Nodef
    {
        protected:
        void SetObservation(const ma::ObservablePtr &observable) override;

        public:
        ma::Engine::LightfPtr m_ItemLight;

        //::Ogre::Light::LightTypes m_Type;
        ::Ogre::Light *m_Light;

        explicit Lightf(const Item::ConstructorParameters &in_params,
                        const ContextLinkPtr &link,
                        ma::ItemPtr client);
        Lightf() = delete;
        ~Lightf() override;

        void EndConstruction() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Lightf, Ogre)
    };
    typedef std::shared_ptr<Lightf> LightfPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, Ogre)
} // namespace ma::Ogre

