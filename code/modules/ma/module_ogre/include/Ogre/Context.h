/// \file ogre/Context.h
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <ma/Context.h>
#include <ma/Config.h>
#include <ma/engine/Scene.h>
#include <ma/engine/Primitive.h>
#include <ma/engine/Camera.h>
#include <ma/engine/Light.h>
#include <ma/engine/Context.h>

#include "Ogre/View.h"
#include "Ogre/Config.h"

namespace ma::Ogre
{
    class RootSmile;

    class Root:
    public ma::Item
    {
        public:
        static const ma::Item::key_type &GetKeyRoot();

        explicit Root(const Item::ConstructorParameters &in_params);
        ~Root() override;

        std::unique_ptr<RootSmile> m_RootSmile;

        /// La configuration d'Ogre.
        /// C'est ici que sont définis les chemins vers plugin.cfg, ogre.cfg, etc.
        ma::Ogre::ConfigGroupVarPtr m_ConfigGroup;

        /// Choix du système de rendu.
        ma::EnumerationVarPtr m_RenderSystems;

        /// À appeler avant la création de la fenêtre
        void CreateRoot();

        /// À appeler après la création de la fenêtre
        /// \remarks Cette fonction n'est pas utilisée. Elle ne sert peut-être à rien.
        void InitRoot();

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Root, Ogre)
    };
    typedef std::shared_ptr<Root> RootPtr;

    class Context: public ma::Engine::Context
    {
        protected:
            void OnEnable() override;
            void OnDisable() override;

        public:
            /// Chaque contexte créé une fenêtre. À la destruction du contexte celle-ci sera détruite.
            ma::Ogre::View *m_View;

            explicit Context(const Item::ConstructorParameters &in_params);
            ~Context() override;
            void EndConstruction() override;

            wxWindow* GetView() override;

            void OnClosePage(wxAuiNotebookEvent &event);

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Context, Ogre)
    };
    typedef std::shared_ptr<Context> ContextPtr;

} // namespace ma::Ogre

namespace ma::Engine
{
    template std::shared_ptr<ma::Ogre::Context> ma::Engine::CreateContext<ma::Ogre::Context>();
}

M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Context, Ogre)
