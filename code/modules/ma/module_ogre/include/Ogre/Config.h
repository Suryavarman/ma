/// \file Ogre/Config.h
/// \author Pontier Pierre
/// \date 19-03-2023
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <ma/Item.h>
#include <ma/Variable.h>
#include <ma/wx/Variable.h>
#include <ma/Config.h>

namespace ma
{
    namespace Ogre
    {
        ///  [ogre]
        ///  plugin_cfg_path=
        ///  config_cfg_path=
        ///  log_path=
        ///  resources_cfg_path=
        class ConfigGroup:
        public ma::ConfigGroup
        {
            public:
            static const key_type ms_Key;

            /// plugin_cfg_path
            ma::FilePathVarPtr m_PluginCfgPath;

            /// config_cfg_path
            ma::FilePathVarPtr m_ConfigCfgPath;

            /// log_path
            ma::FilePathVarPtr m_LogPath;

            /// resources_cfg_path
            ma::DirectoryVarPtr m_ResourcesDirectory;

            /// \brief Constructeur de ConfigGroup.
            /// \param params Les paramètres de construction d'un item.
            /// \param module Le chemin du dossier contenant le module.
            explicit ConfigGroup(const Item::ConstructorParameters &params, const std::filesystem::path& module);
            ConfigGroup() = delete;

            /// \brief Destructeur de ConfigGroup
            ~ConfigGroup() override = default;
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ConfigGroup, Ogre)
        };
        typedef std::shared_ptr<ma::Ogre::ConfigGroup> ConfigGroupPtr;
    } // namespace Ogre

    /// Permet de faire référence exclusivement à un ConfigGroup dans une variable.
    M_HEADER_ITEM_TYPE_VAR_WITH_NAMESPACE(ConfigGroup, Ogre)
} // namespace API

// M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(ConfigGroupVar, Ogre)

