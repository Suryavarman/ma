/// \file ogre/View.h
/// \author Pontier Pierre
/// \date 2018-12-27
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \todo gérer différentes version du rendu :
///       https://registry.khronos.org/OpenGL/extensions/EXT/GLX_EXT_create_context_es2_profile.txt

#pragma once

#include <wx/glcanvas.h>
#include <ma/wx/wx.h>

#include <memory>

namespace ma::Ogre
{
    class ViewSmile;

    /// \class View
    /// \brief
    /// The OgreView class
    ///     originals authors:
    ///     * jabaa : http://wiki.ogre3d.org/tiki-user_information.php?userId=1434
    ///     * jacmoe : http://wiki.ogre3d.org/tiki-user_information.php?userId=2
    /// \see http://wiki.ogre3d.org/WxOgreView
    ///
    /// The View class provides an Ogre render window and a function to
    /// get the window handle. You can create and assign a render window
    /// with the setRenderWindow method.
    class View: public wxControl
    {
        public:
        /// Constructor - A new View must receive a parent window to
        /// which it can be attached
        ///    \param parent pointer to a parent window.
        ///    \param winid pointer to a parent window.
        ///    \param pos Window position. wxDefaultPosition indicates that
        ///           wxWidgets should generate a default position for the
        ///           window. If using the wxWindow class directly, supply
        ///           an actual position.
        ///    \param size Window size. wxDefaultSize indicates that
        ///           wxWidgets should generate a default size for the
        ///           window. If no suitable size can be found, the window
        ///           will be sized to 20x20 pixels so that the window is
        ///           visible but obviously not correctly sized.
        ///    \param style Window style. For generic window styles, please
        ///           see wxWindow.
        ///    \param name wxVarBaseItem name.
        View(wxWindow *parent,
             wxWindowID id = wxID_ANY,
             const wxPoint &pos = wxDefaultPosition,
             const wxSize &size = wxDefaultSize,
             long style = 0,
             const wxValidator &validator = wxDefaultValidator,
             const wxString &name = wxPanelNameStr);

        /// Constructeur par défaut
        View();

        ~View() override;

        /// Créer une nouvelle fenêtre de rendu avec le nom qui lui est attribué et assigne cette nouvelle fenêtre
        /// au pointeur de « RenderWindow ».
        ///  \param name Le nom de la fenêtre
        void setRenderWindow(const std::wstring &name);

        /// \return L'identifiant (handle) de la fenêtre utilisée pour créer la fenêtre de rendu.
        std::string getWindowHandle();

        /// Pointeur opaque.
        std::unique_ptr<ViewSmile> m_ViewSmile;

        private:
        static const int ID_RENDER_TIMER;

        wxTimer *m_Timer;

        /// Temps en seconde entre chaque appel à dessiner le rendu.
        const double m_DeltaTimeCallFrame;

        /// Notifie Ogre de l'appel à l'évènement de redimensionnement de la fenêtre.
        /// \param event L'évènement de redimensionnement.
        void OnSize(wxSizeEvent &event);

        void OnRenderTimer(wxTimerEvent &event);

        void ToggleTimerRendering();

        /// Définitions des événements
        void InitEvents();
    };
} // namespace ma::Ogre

