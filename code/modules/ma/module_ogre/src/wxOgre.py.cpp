/// \file wxOgre.py.cpp
/// \author Pontier Pierre
/// \date 2019/08/01
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "../python/wx.py.h"
#include "Ogre/wxOgre.py.h"

#include <pybind11/operators.h>
#include <sip.h>
#include <wxPython/wxpy_api.h>

#include <ma/Ma.h>

#include <sstream>

using namespace pybind11::literals;

//class PywxOgreView : public API::wx::OgreView {
//public:
//    using API::wx::OgreView::OgreView; // Inherit constructors
//    void setWindowHandle(const std::string &name) override { PYBIND11_OVERLOAD(void, OgreView, setRenderWindow, std::string); }
//    std::string getWindowHandle() override { PYBIND11_OVERLOAD(std::string, OgreView, getWindowHandle); }
//};

void ma::Py::wxOgre::SetModule(py::module &in_ma_wx_module)
{
    // https://pybind11.readthedocs.io/en/master/advanced/classes.html#operator-overloading
    // https://pybind11.readthedocs.io/en/master/advanced/classes.html
    // https://pybind11.readthedocs.io/en/stable/advanced/misc.html?highlight=init#generating-documentation-using-sphinx

    //https://pybind11.readthedocs.io/en/stable/advanced/pycpp/object.html


    // https://pybind11.readthedocs.io/en/master/advanced/misc.html?highlight=inheritance#partitioning-code-over-multiple-extension-modules

//    py::object py_type_wxWindow = static_cast<py::object>(py_wx_module.attr("Window"));

//    py::handle py_DefaultPosition = static_cast<py::object>(py_wx_module.attr("DefaultPosition"));
//    py::handle py_DefaultSize = static_cast<py::object>(py_wx_module.attr("DefaultSize"));
//    py::handle py_DefaultValidator = static_cast<py::object>(py_wx_module.attr("DefaultValidator"));
//    py::print(py_DefaultPosition);
//    py::print(py_DefaultSize);
//    py::print(py_DefaultValidator);


    auto py_wx_module = py::module::import("wx");

    py::object py_type_wxControl = static_cast<py::object>(py_wx_module.attr("Control"));
    py::print(py_type_wxControl);

//    py::print(py::globals());

//    for(auto it = builtins2.begin(); it != builtins2.end(); it++)
//    {
//        py::print(*it);
//    }

    //if(builtins.contains(id) && py::isinstance<py::capsule>(builtins[id]))
    //{
        //std::cout << builtins << std::endl;
        //internals** internals_pp = static_cast<internals **>(capsule(builtins[id]));

    //}

//    std::cout << "py_type_wxControl.ptr(): " << !!py_type_wxControl.ptr() << std::endl;
//
//    auto test = py::detail::all_type_info_get_cache(py_type_wxControl.ptr()->ob_type);
//    std::cout << "py::all_type_info_get_cache: " << test.second << std::endl;
//
//    py::detail::type_record record;
//    /*
//    py::detail::process_attributes<py::object>::init(py_type_wxControl, &record);
//    /*/
//    record.bases.append(py_type_wxControl.ptr()->ob_type);
//    //*/
//
////    template <>
////    struct process_attribute<py::object> : process_attribute_default<handle> {
////        static void init(const handle &h, type_record *r)
////        {
////            r->bases.append(h);
////        }
////    };
//
//    auto &bases = py::detail::all_type_info((PyTypeObject *) record.bases[0].ptr());
//    if (bases.size() == 0)
//        std::cout << "bases.size() == 0" << std::endl;
//    else
//        if (bases.size() > 1)
//            std::cout << "bases.size() > 1" << std::endl;
//        else
//            std::cout << "bases.size() == 1 C'est bien" << std::endl;
//    //*
//    auto parent_tinfo = py::detail::get_type_info((PyTypeObject *) record.bases[0].ptr());
//    /*/
//    auto parent_tinfo = py::detail::get_type_info(record.bases[0].ptr()->ob_type);
//    //*/
//    std::cout << "parent_tinfo is null: " << !parent_tinfo << std::endl;

    py::class_<ma::Ogre::View, std::unique_ptr<API::Ogre::View, py::nodelete>>(in_ma_wx_module, "OgreView")//, py_type_wxControl)  // , py::multiple_inheritance() ?
        .def(py::init<>())
        .def(py::init<>
        (
             // (py::handle inWindow, wxWindowID inId, const wxPoint& inPos, const wxSize& inSize, long inStyle, py::handle inValidator, const std::string &inName)
             // (wxWindow* inWindow, wxWindowID inId, const wxPoint& inPos, const wxSize& inSize, long inStyle, const wxValidator& inValidator, const std::string &inName)
             [] (py::object inWindow)//, wxWindowID inId)//, const wxPoint& inPos, const wxSize& inSize, long inStyle/*, const wxValidator& inValidator*/, const std::string &inName)
             {

                 wxWindow *window = ma::Py::wxLoad<wxWindow>(inWindow.release(), "wxWindow");
                 wxASSERT(window);

//                 wxPoint *window_pos = ma::Py::wxLoad<wxPoint>(inPos, "wxPoint");
//                 wxASSERT(window_pos);
//
//                 wxSize *window_size = ma::Py::wxLoad<wxSize>(inSize, "wxSize");
//                 wxASSERT(window_size);
//
//                 wxValidator *validator = ma::Py::wxLoad<wxValidator>(inValidator, "wxValidator");
//                 wxASSERT(validator);

//                 wxString name(inName);

                 //return new API::Ogre::View(window, inId, *window_pos, *window_size, inStyle, *validator, name);
                 return new ma::Ogre::View(window);//, inId);//, inPos, inSize, inStyle, wxDefaultValidator, name);
             }
        ), py::arg("inWindow")//, py::return_value_policy::reference, "Return an OgreView."
//         , py::arg_v("inId", wxID_ANY, "wx.ID_ANY")
//         , py::arg_v("inPos", wxDefaultPosition, "wx.DefaultPosition")
//         , py::arg_v("inSize", wxDefaultSize, "wx.DefaultSize")
//         , py::arg("inStyle") = 0
         //, py::arg_v("inValidator", const_cast<wxValidator*>(&wxDefaultValidator), "wx.DefaultValidator") // crash !!
//         , py::arg_v("inName", wxPanelNameStr, "wx.PanelNameStr") // https://github.com/patrikhuber/eos/issues/256
        )
        .def("setRenderWindow", &ma::Ogre::View::setRenderWindow)
        .def("getWindowHandle", &ma::Ogre::View::getWindowHandle)
        //.def("getParentClass", [](const API::Ogre::View& self){return ma::Py::wxCast(static_cast<wxControl*>(const_cast<API::Ogre::View*>(&self)));}, py::call_guard<py::gil_scoped_release>(), py::return_value_policy::reference, "Retourne la classe parente de OgreView.")
        .def("getParentClass",
             [](const ma::Ogre::View& self)
             {
                return static_cast<wxControl*>(const_cast<API::Ogre::View*>(&self));
             },
             py::call_guard<py::gil_scoped_release>(),
             py::return_value_policy::reference,
             "Retourne la classe parente de OgreView.")
        ;
}
