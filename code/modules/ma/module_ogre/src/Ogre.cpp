/// \file Ogre/Ogre.cpp
/// \author Pontier Pierre
/// \date 2022-07-12
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Ogre/Ogre.h"

namespace ma::Ogre
{
    // RootSmile // ————————————————————————————————————————————————————————————————————————————————————————————————————
    RootSmile::RootSmile(): m_Root(nullptr), m_OverlaySystem(nullptr)
    {}

    RootSmile::~RootSmile() = default;

    // ViewSmile // ————————————————————————————————————————————————————————————————————————————————————————————————————
    ViewSmile::ViewSmile(): m_RenderWindow(nullptr)
    {}

    ViewSmile::~ViewSmile() = default;

    std::string GetNewName(ma::ItemPtr item, const std::wstring &default_base_name)
    {
        auto node_name = default_base_name;

        if(item->HasVar(Item::Key::Var::GetName()))
            node_name = item->GetVar(Item::Key::Var::GetName())->toString();

        if(node_name.empty())
            node_name = L"node";

        node_name += L"_" + item->GetKey();

        return ma::to_string(node_name);
    }

    // Nodef // ———————————————————————————————————————————————————————————————————————————————————————————————————
    Nodef::Nodef(const ma::Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    ma::Data::Data(in_params, link, client),
    ma::Observer(),
    m_OgreContext(std::dynamic_pointer_cast<ma::Ogre::Context>(link->GetContext())),
    m_ItemNode(std::dynamic_pointer_cast<ma::Engine::Nodef>(client)),
    m_OgreNode(nullptr)
    {}

    Nodef::~Nodef()
    {
        if(m_OgreNode)
        {
            // MA_ASSERT(m_OgreNode, L"m_OgreNode est nul.", std::invalid_argument);

            auto *ogre_scene_parent = m_OgreNode->getParentSceneNode();

            if(ogre_scene_parent)
            {
                ogre_scene_parent->removeAndDestroyChild(m_OgreNode);
                MA_MSG(L"ogre_scene_parent n'est pas nul.");
            }
            else
                MA_MSG(L"ogre_scene_parent est nul.");

            auto *ogre_root = GetOgreRoot();
            auto *ogre_scene_manager = GetOgreSceneManager();
            ogre_root->destroySceneManager(ogre_scene_manager);
        }
    }

    void Nodef::EndConstruction()
    {
        // Ne peut être défini dans le constructeur, car GetOgreSceneManager a besoin gue la hiérarchie des items soit
        // réalisé pour ce nœud.
        MA_ASSERT(m_OgreContext, L"m_OgreContext est nul.", std::invalid_argument);

        MA_ASSERT(m_ItemNode, L"m_ItemScene est nul.", std::invalid_argument);

        MA_ASSERT(
            ma::Item::HasItem(m_ItemNode->GetParentKey()), L"m_ItemNode n'a pas de parent.", std::invalid_argument);

        auto ogre_item_parent = ma::Item::GetItem(GetParentKey());

        // Si le parent est un nœud ce pointeur ne sera pas nul.
        auto ogre_node_parent = std::dynamic_pointer_cast<ma::Ogre::Nodef>(ogre_item_parent);

        if(ogre_node_parent)
        {
            MA_ASSERT(ogre_node_parent->m_OgreNode, L"scene_parent->m_OgreNode est nul.", std::logic_error);

            m_OgreNode = ogre_node_parent->m_OgreNode->createChildSceneNode(GetNewName(m_ItemNode, L"node"));
        }
        else
        {
            const bool create_if_not_exist = true;
            auto *ogre_scene_manager = GetOgreSceneManager(create_if_not_exist);

            m_OgreNode = ogre_scene_manager->getRootSceneNode();
        }

        MA_ASSERT(m_OgreNode, L"m_OgreNode est nul.", std::invalid_argument);

        // Cette fonction déclenchera l'évènement OnEnable qui va appeler Load et qui aura besoin de m_OgreNode
        ma::Item::EndConstruction();

        if(ma::Item::HasItem(Key::GetObservationManager()))
        {
            auto observer_manager = ma::Item::GetItem<ma::ObservationManager>(Key::GetObservationManager());
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Position);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Quaternion);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Euler);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Scale);
        }
        else
        {
            MA_WARNING(false,
                       L"Le gestionnaire d'observations n'ai pas disponible. "
                       L"Aucun item n'est lié à la clef qui lui est attitrée: « " +
                           Key::GetObservationManager() + L" ». Ce nœud ne pourra pas observer les variables: \n" +
                           L" - m_Position \n - m_Quaternion \n - m_Euler \n - m_Scale");
        }
    }

    void Nodef::SetObservation(const ma::ObservablePtr &observable)
    {
        // Test
        MA_ASSERT(IsEnable(), L"Ne devrait pas être appelé si le nœud est inactif.");

        // Si le nœud n'est pas actif. Il ne faut pas mettre à jour Ogre. Les instances d'Ogre risquent d'être
        // invalident.
        //        if(IsEnable())
        {
            if(m_ItemNode->m_Position == observable || m_ItemNode->m_Quaternion == observable ||
               m_ItemNode->m_Scale == observable)
            {
                const auto &position = m_ItemNode->m_Position->Get();
                const auto &quaternion = m_ItemNode->m_Quaternion->Get();
                const auto &scale = m_ItemNode->m_Scale->Get();

                m_OgreNode->setPosition(position.x(), position.y(), position.z());
                m_OgreNode->setOrientation(quaternion.w(), quaternion.x(), quaternion.y(), quaternion.z());
                m_OgreNode->setScale(scale.x(), scale.y(), scale.z());
            }
            else if(m_ItemNode->m_Euler == observable)
            {
                const auto &quaternion = m_ItemNode->m_Quaternion->Get();
                m_OgreNode->setOrientation(quaternion.w(), quaternion.x(), quaternion.y(), quaternion.z());
            }
        }
        // else
        // {
        //     MA_MSG(L"IsEnable()");
        // }

        // std::wstring message;
        // message += L"Nom: " + to_wstring(m_OgreNode->getName()) + L" / ";
        // message += L"isInSceneGraph: " + toString(m_OgreNode->isInSceneGraph()) + L" / ";
        // message += L"Nb d'enfants: " + toString(m_OgreNode->getChildren().size()) + L" / ";
        // message += L"Nom du gestionnaire de scène: " + to_wstring(m_OgreNode->getCreator()->getName());
        //
        // MA_MSG(message);

        // m_OgreNode->isInSceneGraph()
        // m_OgreNode->getName()
        // m_OgreNode->getShowBoundingBox()
        // m_OgreNode->getChildren().size()
        // m_OgreNode->getOrientation()
        // m_OgreNode->getPosition()
        // m_OgreNode->getScale()
        // m_OgreNode->getParentSceneNode()->getName()
    }

    void Nodef::BeginObservation(const ObservablePtr &observable)
    {
        SetObservation(observable);
    }

    void Nodef::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        SetObservation(observable);
    }

    void Nodef::EndObservation(const ObservablePtr &observable)
    {
        m_OgreContext.reset();
    }

    void Nodef::OnEnable()
    {
        Observer::OnEnable();
        Data::OnEnable();
    }

    void Nodef::OnDisable()
    {
        Observer::OnDisable();
        Data::OnDisable();
    }

    bool Nodef::Load()
    {
        bool result = Data::Load();

        MA_ASSERT(m_OgreNode, L"m_OgreNode est nul.", std::logic_error);

        const auto cascade = true;
        m_OgreNode->setVisible(true, cascade);

        return result;
    }

    bool Nodef::UnLoad()
    {
        if(m_OgreNode)
        {
            // const auto cascade = true;
            // m_OgreNode->setVisible(false, cascade);

            // m_OgreNode->detachAllObjects();

            // m_OgreNode->removeAndDestroyAllChildren();
            auto *ogre_scene_manager = GetOgreSceneManager();
            if(m_OgreNode == ogre_scene_manager->getRootSceneNode())
            {
                auto *ogre_root = GetOgreRoot();
                ogre_root->destroySceneManager(ogre_scene_manager);
                typedef ma::MatchType<Nodef> node_match_type;

                auto children =
                    ma::Find<node_match_type>::GetDeque(GetKey(), node_match_type(), ma::Item::SearchMod::RECURSIVE);
                for(const auto& child : children)
                    child->m_OgreNode = nullptr;

                m_OgreNode = nullptr;
            }
        }

        m_OgreContext.reset();

        return Data::UnLoad();
    }

    bool Nodef::Reload()
    {
        return Data::Reload();
    }

    ::Ogre::Root *Nodef::GetOgreRoot() const
    {
        auto item_root = Item::GetItem<Ogre::Root>(ma::Ogre::Root::GetKeyRoot());
        MA_ASSERT(item_root->m_RootSmile, L"« item_root->m_RootSmile » est nul.", std::logic_error);
        MA_ASSERT(item_root->m_RootSmile->m_Root, L"« item_root->m_RootSmile->m_Root » est nul.", std::logic_error);

        return item_root->m_RootSmile->m_Root;
    }

    std::wstring Nodef::GetSceneManagerName() const
    {
        if(m_Link->Get()->GetKey() == GetParentKey())
            return GetKey();
        else
        {
            auto parent = GetParent<Nodef>();
            MA_ASSERT(parent, L"Le parent est nul.");
            MA_ASSERT(parent.get() != this, L"Le parent est lui même.");
            return GetParent<Nodef>()->GetSceneManagerName();
        }
    }

    ::Ogre::SceneManager *Nodef::GetOgreSceneManager(bool create_if_not_exist) const
    {
        auto *ogre_root = GetOgreRoot();

        ::Ogre::SceneManager *ogre_scene_manager = nullptr;
        const auto scene_manager_name = ma::to_string(GetSceneManagerName());
        if(ogre_root->hasSceneManager(scene_manager_name))
        {
            ogre_scene_manager = ogre_root->getSceneManager(scene_manager_name);
        }
        else
        {
            MA_ASSERT(create_if_not_exist,
                      L"Le gestionnaire de scène pour ce nœud n'est pas autorisé à être créé.",
                      std::invalid_argument);
            try
            {
                ogre_scene_manager = ogre_root->createSceneManager(::Ogre::SMT_DEFAULT, scene_manager_name);
            }
            catch(const std::exception &e)
            {
                MA_ASSERT(false,
                          L"L'appel à ogre_root->createSceneManager() a échoué: \n" + ma::to_wstring(e.what()),
                          std::runtime_error);
            }
        }

        MA_ASSERT(ogre_scene_manager, L"Le gestionnaire de scène d'Ogre est nul.", std::logic_error);
        return ogre_scene_manager;
    }

    const TypeInfo&Nodef::GetOgreNodefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetOgreNodefClassName(),
            L"Nodef sera placé enfant du « ma::Ogre::ITEM » héritant de « ma::Data ».\n"
            L"Nodef met à jour m_OgreNode à partir de l'observation de du « NodefPtr » "
            L"qu'observe\n « ma::Ogre::ITEM »..\n",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Scenef //————————————————————————————————————————————————————————————————————————————————————————————————————————
    // https://ogrecave.github.io/ogre/api/latest/tut__first_scene.html
    Scenef::Scenef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemScene(std::dynamic_pointer_cast<ma::Engine::Scenef>(client))
    {
        MA_ASSERT(m_ItemScene, L"m_ItemScene est nul.", std::invalid_argument);
    }

    Scenef::~Scenef() = default;

    const TypeInfo& Scenef::GetOgreScenefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetOgreScenefClassName(),
            L"Implémentation de ma::Scenef pour Ogre.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cubef //—————————————————————————————————————————————————————————————————————————————————————————————————————————
    Cubef::Cubef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemCube(std::dynamic_pointer_cast<ma::Engine::Cubef>(client)),
    m_Entity(nullptr)
    {}

    Cubef::~Cubef() = default;

    // https://ogrecave.github.io/ogre/api/latest/manual-mesh-creation.html
    // https://cpp.hotexamples.com/site/file?hash=0x562429eda77396a942b3896c6cfdb4608f3f96dbb70e3cbf2943567d6863bca8
    // https://cpp.hotexamples.com/examples/ogre/ManualObject/-/cpp-manualobject-class-examples.html
    void Cubef::EndConstruction()
    {
        Nodef::EndConstruction();

        auto *scene_manager = GetOgreSceneManager();

        MA_ASSERT(m_ItemCube, L"m_ItemCube est nul.", std::invalid_argument);

        const auto size = 1.0f;
        const auto half_size = size * 0.5f;

        auto& material_manager = ::Ogre::MaterialManager::getSingleton();
        auto& resource_group_manager = ::Ogre::ResourceGroupManager::getSingleton();

        static const auto cube_mat_name = "default_cube"s;
        static const auto group_mat_name = "ma"s;

        if(!resource_group_manager.resourceGroupExists(group_mat_name))
            resource_group_manager.createResourceGroup(group_mat_name);

        auto pair = material_manager.createOrRetrieve(cube_mat_name, group_mat_name, true);
        ::Ogre::MaterialPtr mat{std::dynamic_pointer_cast<::Ogre::Material>(pair.first)};
        if(pair.second)
        {
            auto *pass = mat->getTechnique(0)->getPass(0);
            pass->setLightingEnabled(false);
        }

        ::Ogre::ManualObject* man = scene_manager->createManualObject(GetNewName(m_ItemCube, L"cube"));

        man->begin(mat, ::Ogre::RenderOperation::OT_TRIANGLE_LIST);

        const ::Ogre::Vector3 V0(-half_size, -half_size, -half_size),
                              V1(-half_size,  half_size, -half_size),
                              V2( half_size,  half_size, -half_size),
                              V3( half_size, -half_size, -half_size),
                              V4(-half_size, -half_size,  half_size),
                              V5(-half_size,  half_size,  half_size),
                              V6( half_size,  half_size,  half_size),
                              V7( half_size, -half_size,  half_size);

        // Coordonnées des normales (il y a une normale par face)
        const ::Ogre::Vector3 Nxp( 1,  0,  0),
                              Nxn(-1,  0,  0),
                              Nyp( 0,  1,  0),
                              Nyn( 0, -1,  0),
                              Nzp( 0,  0,  1),
                              Nzn( 0,  0, -1);

        // Couleurs de chaque face
        const ::Ogre::ColourValue Cxp(1.0, 0.0, 0.0, 1.0),
                                  Cxn(0.0, 1.0, 0.0, 1.0),
                                  Cyp(0.0, 0.0, 1.0, 1.0),
                                  Cyn(1.0, 0.0, 1.0, 1.0),
                                  Czp(0.0, 1.0, 1.0, 1.0),
                                  Czn(1.0, 1.0, 0.0, 1.0);

        // Chaque face est recouverte par l'entièreté de la texture.
        const ::Ogre::Vector2 T0(0.0, 1.0),
                              T1(0.0, 0.0),
                              T2(1.0, 0.0),
                              T3(1.0, 1.0);

        // Face -Z
        {
            // 0
            man->position(V0);
            man->normal(Nzn);
            man->colour(Czn);
            man->textureCoord(T0);

            // 1
            man->position(V1);
            man->normal(Nzn);
            man->colour(Czn);
            man->textureCoord(T1);

            // 2
            man->position(V2);
            man->normal(Nzn);
            man->colour(Czn);
            man->textureCoord(T2);

            // 3
            man->position(V3);
            man->normal(Nzn);
            man->colour(Czn);
            man->textureCoord(T3);
        }

        // Face +Z
        {
            // 4
            man->position(V4);
            man->normal(Nzp);
            man->colour(Czp);
            man->textureCoord(T0);

            // 5
            man->position(V7);
            man->normal(Nzp);
            man->colour(Czp);
            man->textureCoord(T3);

            // 6
            man->position(V6);
            man->normal(Nzp);
            man->colour(Czp);
            man->textureCoord(T2);

            // 7
            man->position(V5);
            man->normal(Nzp);
            man->colour(Czp);
            man->textureCoord(T1);
        }

        // Face +Y
        {
            // 8
            man->position(V1);
            man->normal(Nyp);
            man->colour(Cyp);
            man->textureCoord(T0);

            // 9
            man->position(V5);
            man->normal(Nyp);
            man->colour(Cyp);
            man->textureCoord(T1);

            // 10
            man->position(V6);
            man->normal(Nyp);
            man->colour(Cyp);
            man->textureCoord(T2);

            // 11
            man->position(V2);
            man->normal(Nyp);
            man->colour(Cyp);
            man->textureCoord(T3);
        }

        // Face -Y
        {
            // 12
            man->position(V0);
            man->normal(Nyn);
            man->colour(Cyn);
            man->textureCoord(T0);

            // 13
            man->position(V3);
            man->normal(Nyn);
            man->colour(Cyn);
            man->textureCoord(T1);

            // 14
            man->position(V7);
            man->normal(Nyn);
            man->colour(Cyn);
            man->textureCoord(T2);

            // 15
            man->position(V4);
            man->normal(Nyn);
            man->colour(Cyn);
            man->textureCoord(T3);
        }

        // Face +X
        {
            // 16
            man->position(V3);
            man->normal(Nxp);
            man->colour(Cxp);
            man->textureCoord(T0);

            // 17
            man->position(V2);
            man->normal(Nxp);
            man->colour(Cxp);
            man->textureCoord(T1);

            // 18
            man->position(V6);
            man->normal(Nxp);
            man->colour(Cxp);
            man->textureCoord(T2);

            // 19
            man->position(V7);
            man->normal(Nxp);
            man->colour(Cxp);
            man->textureCoord(T3);
        }

        // Face -X
        {
            // 20
            man->position(V4);
            man->normal(Nxn);
            man->colour(Cxn);
            man->textureCoord(T0);

            // 21
            man->position(V5);
            man->normal(Nxn);
            man->colour(Cxn);
            man->textureCoord(T1);

            // 22
            man->position(V1);
            man->normal(Nxn);
            man->colour(Cxn);
            man->textureCoord(T2);

            // 23
            man->position(V0);
            man->normal(Nxn);
            man->colour(Cxn);
            man->textureCoord(T3);
        }

        for(auto i = 0u; i < 24u; i+=4u)
             man->quad(i, i + 1u, i + 2u, i + 3u);

        man->end();

        // Convertion vers entité ?
        // https://forums.ogre3d.org/viewtopic.php?t=78388
        m_Entity = man;

        m_OgreNode->attachObject(m_Entity);
    }

    const TypeInfo& Cubef::GetOgreCubefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetOgreCubefClassName(),
            L"Implémentation de ma::Cubef pour Ogre.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cameraf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Cameraf::Cameraf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemCamera(std::dynamic_pointer_cast<ma::Engine::Cameraf>(client)),
    m_Camera(nullptr),
    m_Viewport(nullptr)
    {}

    Cameraf::~Cameraf() = default;

    void Cameraf::EndConstruction()
    {
        Nodef::EndConstruction();

        MA_ASSERT(m_ItemCamera, L"m_ItemCamera est nul.", std::invalid_argument);
        auto *scene_manager = GetOgreSceneManager();

        m_Camera = scene_manager->createCamera(GetNewName(m_ItemCamera, L"camera"));
        m_Camera->setAutoAspectRatio(true);

        m_OgreNode->attachObject(m_Camera);

        /// \todo Voir s'il faut retirer cette ligne.
        m_OgreNode->lookAt(::Ogre::Vector3(0, 0, 0), ::Ogre::Node::TransformSpace::TS_WORLD);

        auto context = m_Link->GetItem()->GetContext();
        MA_ASSERT(context, L"context est nul.", std::logic_error);

        auto ogre_context = std::dynamic_pointer_cast<ma::Ogre::Context>(context);
        MA_ASSERT(ogre_context, L"ogre_context n'est pas du type ma::Ogre::Context", std::logic_error);

        auto *view = ogre_context->m_View;
        MA_ASSERT(view, L"m_View est nul", std::logic_error);

        MA_ASSERT(view->m_ViewSmile, L"m_View->m_ViewSmile est nul", std::logic_error);

        auto *render_window = view->m_ViewSmile->m_RenderWindow;
        MA_ASSERT(render_window, L"render_window est nul.", std::runtime_error);

        int ZOrder = 0;
        float left = 0.0f;
        float top = 0.0f;
        float width = 1.0f;
        float height = 1.0f;
        m_Viewport = render_window->addViewport(m_Camera, ZOrder, left, top, width, height);

        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_BackgroundColour);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Far);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Near);
            // observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Ratio);
        }
    }

    void Cameraf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);

        if(m_ItemCamera->m_BackgroundColour == observable)
        {
            const auto &colour = m_ItemCamera->m_BackgroundColour->Get();
            m_Viewport->setBackgroundColour(
                ::Ogre::ColourValue(static_cast<float>(colour.Red()) / 255.f,
                                    static_cast<float>(colour.Green()) / 255.f,
                                    static_cast<float>(colour.Blue()) / 255.f));
        }
        else if(m_ItemCamera->m_Far == observable)
        {
            const auto far = m_ItemCamera->m_Far->Get();
            m_Camera->setFarClipDistance(far);
        }
        else if(m_ItemCamera->m_Near == observable)
        {
            const auto near = m_ItemCamera->m_Near->Get();
            m_Camera->setNearClipDistance(near);
        }
//        else if(m_ItemCamera->m_Ratio == observable)
//        {
//            const auto ratio = m_ItemCamera->m_Ratio->Get();
//            m_Camera->setAutoAspectRatio(true);
//            // m_Camera->setAspectRatio(ratio); // \todo Il y a t'il un sens avec « AutoAspectRatio » à vrai?
//        }
    }

    const TypeInfo& Cameraf::GetOgreCamerafTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetOgreCamerafClassName(),
            L"Implémentation de ma::Engine::Cameraf pour Ogre.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Lightf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Lightf::Lightf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemLight(std::dynamic_pointer_cast<ma::Engine::Lightf>(client)),
    m_Light(nullptr)
    {}

    Lightf::~Lightf() = default;

    void Lightf::EndConstruction()
    {
        Nodef::EndConstruction();

        MA_ASSERT(m_ItemLight, L"m_ItemLight est nul.", std::invalid_argument);

        auto *scene_manager = GetOgreSceneManager();

        m_Light = scene_manager->createLight(GetNewName(m_ItemLight, L"light"));

        m_Light->setType(::Ogre::Light::LT_POINT);

        m_OgreNode->attachObject(m_Light);

        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemLight->m_Intensity);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemLight->m_Colour);
        }
    }

    void Lightf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);

        if(m_ItemLight->m_Colour == observable)
        {
            const auto &wx_colour = m_ItemLight->m_Colour->Get();
            const auto ogre_colour =
                ::Ogre::ColourValue(static_cast<float>(wx_colour.Red()) / 255.f,
                                    static_cast<float>(wx_colour.Green()) / 255.f,
                                    static_cast<float>(wx_colour.Blue()) / 255.f);
            m_Light->setDiffuseColour(ogre_colour);
            m_Light->setSpecularColour(ogre_colour);
        }
        else if(m_ItemLight->m_Intensity == observable)
        {
            const auto intensity = m_ItemLight->m_Intensity->Get();
            m_Light->setPowerScale(intensity);
        }
    }

    const ma::TypeInfo& Lightf::GetOgreLightfTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetOgreLightfClassName(),
            L"Implémentation de ma::Engine::Lightf pour Ogre.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

} // namespace ma::Ogre

M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, Ogre, Data, Observer)
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Scenef, Ogre, ma::Ogre::Nodef::GetOgreNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cubef, Ogre, ma::Ogre::Nodef::GetOgreNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cameraf, Ogre, ma::Ogre::Nodef::GetOgreNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Lightf, Ogre, ma::Ogre::Nodef::GetOgreNodefClassHierarchy())

M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, Ogre, ma::Engine::Scenef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, Ogre, ma::Engine::Cubef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, Ogre, ma::Engine::Cameraf)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, Ogre, ma::Engine::Lightf)