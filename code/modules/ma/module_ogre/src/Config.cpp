/// \file Config.cpp
/// \author Pontier Pierre
/// \date 19-03-2023
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Ogre/Config.h"
#include "Ogre/ConfigPaths.h"

namespace ma
{
    namespace Ogre
    {
        // ConfigGroup //———————————————————————————————————————————————————————————————————————————————————————————————
        const ConfigGroup::key_type ConfigGroup::ms_Key(L"f9d61275-92c4-4b1e-a0eb-7130bb8406f9");

        std::filesystem::path get_default_config_path(const std::filesystem::path& module)
        {
            // if (char* val = getenv("OGRE_CONFIG_DIR"))
            //{
            //  Ogre::String configDir = Ogre::StringUtil::standardisePath(val);
            // wxFileName configFileName(wxStandardPaths::Get().GetUserConfigDir() + wxFileName::GetPathSeparator() +
            // ".ma_gui"); configFileName.SetExt("conf");
            // https://en.cppreference.com/w/cpp/filesystem/create_directory
            // Save();

            //        std::filesystem::path ogre_directory = std::filesystem::temp_directory_path() / L"ogre";
            //
            //        if(!std::filesystem::exists(ogre_directory))
            //            std::filesystem::create_directories(ogre_directory);
            //
            //        // MA_WARNING(false, L"A implémenter")
            //        // return GetFolderPath() / L"ogre.cfg";
            //        return ogre_directory / L"ogre.cfg";
            return module / MA_OGRE_CONFIG_CFG_PATH;
        }

        std::filesystem::path get_default_plugin_path(const std::filesystem::path& module)
        {
            // if(!Ogre::FileSystemLayer::fileExists(plugins_path))
            //{
            //    pluginsPath = ::Ogre::FileSystemLayer::resolveBundlePath(OGRE_CONFIG_DIR "/plugins.cfg");
            //}

            // std::wcout << "MA_OGRE_PLUGIN_CFG: " << MA_OGRE_PLUGIN_CFG_PATH << std::endl;
            return module / MA_OGRE_PLUGIN_CFG_PATH;
        }

        std::filesystem::path get_default_resources_path(const std::filesystem::path& module)
        {
            return module / MA_OGRE_RESOURCES_CFG_PATH;
        }

        std::filesystem::path get_default_log_path(const std::filesystem::path& module)
        {
            return module / MA_OGRE_LOG_PATH;
        }

        ConfigGroup::ConfigGroup(const Item::ConstructorParameters &params, const std::filesystem::path& module):
        ma::ConfigGroup(params, L"ogre"),
        m_PluginCfgPath(AddMemberVar<ma::FilePathVar>(L"plugin_cfg_path", get_default_plugin_path(module).wstring())),
        m_ConfigCfgPath(AddMemberVar<ma::FilePathVar>(L"config_cfg_path", get_default_config_path(module).wstring())),
        m_LogPath(AddMemberVar<ma::FilePathVar>(L"log_path", get_default_log_path(module).wstring())),
        m_ResourcesDirectory(AddMemberVar<ma::DirectoryVar>(L"resources_cfg_path", get_default_resources_path(module).wstring()))
        {
            MA_WARNING(m_PluginCfgPath->Exist(), L"Le fichier « " + m_PluginCfgPath->toString() + L" » n'existe pas.");

            MA_WARNING(std::filesystem::exists(m_ResourcesDirectory->Get()),
                       L"Le fichier « " + m_ResourcesDirectory->toString() + L" » n'existe pas.");

        }

        const TypeInfo& ConfigGroup::GetOgreConfigGroupTypeInfo()
        {
            // clang-format off
            static const TypeInfo info =
            {
                GetOgreConfigGroupClassName(),
                L"Le groupe de données qui sont enregistrés dans le fichier de configuration l'application.",
                {
                    {
                        ma::TypeInfo::Key::GetDependenciesData(),
                        {
                            ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                        }
                    }
                }
            };
            // clang-format on

            return info;
        }

    } // namespace Ogre
    M_CPP_ITEM_TYPE_VAR_WITH_NAMESPACE(ConfigGroup, Ogre)
} // namespace API

M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ConfigGroup, Ogre, ConfigGroup)
