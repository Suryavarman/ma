/// \file View.cpp
/// \author Pontier Pierre
/// \date 2019-07-25
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// Création du contexte, de la fenêtre et gestion des évènements via Qt:
/// https://github.com/OGRECave/ogre/blob/master/Components/Bites/src/OgreApplicationContextQt.cpp

#include <ma/Macros.h>
#include <ma/Platform.h>
#include <ma/Prerequisites.h>

#include "Ogre/Ogre.h"

namespace ma::Ogre
{
    const int View::ID_RENDER_TIMER = wxNewId();

    View::View(wxWindow *parent,
               wxWindowID id,
               const wxPoint &pos,
               const wxSize &size,
               long style,
               const wxValidator &validator,
               const wxString &name):
    wxControl(parent, id, pos, size, style, validator, name),
    m_ViewSmile(new ViewSmile()),
    m_Timer(new wxTimer(this, ID_RENDER_TIMER)),
    m_DeltaTimeCallFrame(1. / 60.)
    {
        InitEvents();
    }

    View::View():
    wxControl(),
    m_ViewSmile(new ViewSmile()),
    m_Timer(new wxTimer(this, ID_RENDER_TIMER)),
    m_DeltaTimeCallFrame(1. / 60.)
    {
        InitEvents();
    }

    View::~View()
    {
        if(m_Timer->IsRunning())
            m_Timer->Stop();

        if(m_ViewSmile->m_RenderWindow)
        {
            try
            {
                auto &root = ::Ogre::Root::getSingleton();
                root.destroyRenderTarget(m_ViewSmile->m_RenderWindow);
                m_ViewSmile->m_RenderWindow = nullptr;

                // Construction d'une vue puis destruction, ensuite une construction d'une vue :
                // mGLInitialised est à vrais
                // Le contexte courant reste donc à nul
                // RenderSystem::shutdown(); Est la seule fonction qui remet mGLInitialised

                // Ça serait bien de retrouver toutes les fenêtres de rendu gérées par la racine d'Ogre et si celles-ci
                // sont à zéro, nous appelons l'arrêt d'Ogre via root.shutdown().
                // root.shutdown();
            }
            catch(const std::exception &e)
            {
                MA_ASSERT(false,
                          L"La destruction d'une ::Ogre::RenderWindow a échouée: \n"s + ma::to_wstring(e.what()),
                          std::runtime_error);
            }
        }
    }

    void View::InitEvents()
    {
        Bind(wxEVT_SIZE, &View::OnSize, this);
        // Bind(wxEVT_PAINT, &View::OnPaint, this); // Produces flickers and runs too fast!
        // Bind(wxEVT_ERASE_BACKGROUND, &View::OnEraseBackground);
        Bind(wxEVT_TIMER, &View::OnRenderTimer, this);
        ToggleTimerRendering();
        m_Timer->Start(std::floor(m_DeltaTimeCallFrame * 1000.));
    }

    std::string View::getWindowHandle()
    {
        std::string handle;

#if MA_PLATFORM == MA_PLATFORM_WIN32
        handle = ::Ogre::StringConverter::toString((size_t)((HWND)GetHandle()));
#elif defined(__WXGTK__) // GTK3
        // todo: Someone test this. you might to use "parentWindowHandle" if this does not work.
        //       Ogre 1.2 + Linux + GLX platform wants a string of the format display:screen:window,
        //       which has variable types ulong:uint:ulong.

    #if defined(GDK_WINDOWING_X11)
        GtkWidget *widget = GetHandle();
        gtk_widget_realize(widget); // Mandatory. Otherwise, a segfault happens.
        std::stringstream handleStream;
        GdkWindow *gdk_window = gtk_widget_get_window(widget);

        Display *display = GDK_WINDOW_XDISPLAY(gdk_window);
        Window wid = GDK_WINDOW_XID(gdk_window); // Window is a typedef for XID, which is a typedef for unsigned int
        /* Get the right display (DisplayString() returns ":display.screen") */
        // DisplayString est une fonction du serveur d'affichage X11. Elle se situe dans le fichier X11/Xlib.h
        std::string displayStr = DisplayString(display);
        displayStr = displayStr.substr(1, (displayStr.find('.', 0) - 1));
        /* Put all together */
        handleStream << displayStr << ':' << DefaultScreen(display) << ':' << wid;
        handle = handleStream.str();
    #elif defined(GDK_WINDOWING_QUARTZ)
        // GdkQuartzDisplay *display = GDK_QUARTZ_DISPLAY(gdk_window_get_display(gdk_window));
        // GdkDisplay *display = gdk_window_get_display(gdk_window);
        // std::string displayStr = display->gdk_display_get_name()
        // handleStream << displayStr;
        handle = ""; // handleStream.str();
    #endif

#elif defined(__WXOSX_COCOA__)
        handle = "";
#else
    #error Not supported on this platform.
#endif

        return handle;
    }

    // https://forums.ogre3d.org/viewtopic.php?t=62805
    void View::setRenderWindow(const std::wstring &name)
    {
        int width, height;
        GetSize(&width, &height);
        ::Ogre::NameValuePairList params;
        params["externalWindowHandle"] = getWindowHandle();

#if MA_PLATFORM == MA_PLATFORM_APPLE
        params["macAPI"] = "cocoa";
        params["macAPICocoaUseNSView"] = "true";
#endif

        try
        {
            auto &root = ::Ogre::Root::getSingleton();

            const bool full_screen = false;
            auto *render_window = root.createRenderWindow(ma::to_string(name), width, height, full_screen, &params);
            //            auto* render_system = root.getRenderSystem();// initialiseContext
            //
            //            MA_ASSERT(render_system,
            //                      L"Le système de rendu courant d'Ogre est nul.",
            //                      std::runtime_error);
            // render_system->_initRenderTargets();
            // render_system->initialiseContext(render_window);

            //
            //    std::wcout << L"render_window->isActive()" << render_window->isActive() << std::endl;
            //    std::wcout << L"render_window->isHidden()" << render_window->isHidden() << std::endl;
            //    std::wcout << L"render_window->isAutoUpdated()" << render_window->isAutoUpdated() << std::endl;
            m_ViewSmile->m_RenderWindow = render_window;
        }
        catch(const std::exception &e)
        {
            MA_ASSERT(false,
                      L"La création d'une ::Ogre::RenderWindow a échouée: \n"s + ma::to_wstring(e.what()),
                      std::runtime_error);
        }
    }

    void View::OnSize(wxSizeEvent &event)
    {
        if(m_ViewSmile->m_RenderWindow)
        {
            static int width;
            static int height;
            GetSize(&width, &height);
            m_ViewSmile->m_RenderWindow->resize(width, height);
            // Letting Ogre know the window has been resized;
            m_ViewSmile->m_RenderWindow->windowMovedOrResized();
        }
    }

    void View::OnRenderTimer(wxTimerEvent &event)
    {
        if(m_ViewSmile->m_RenderWindow)
        {
            ::Ogre::Root::getSingleton().renderOneFrame();
        }
    }

    void View::ToggleTimerRendering()
    {
        if(m_Timer->IsRunning())
            m_Timer->Stop();
        else
            m_Timer->Start(std::floor(m_DeltaTimeCallFrame * 1000.));
    }
} // namespace ma::Ogre