/// \file Context.cpp
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.

// Création du Root:
// https://github.com/OGRECave/ogre/blob/master/Components/Bites/src/OgreApplicationContextBase.cpp
//
// wxWidgets Ogre:
// https://wiki.ogre3d.org/Embedding+OGRE+in+wxWidgets+2.8
// https://wiki.ogre3d.org/wxWidgets
// https://wiki.ogre3d.org/WxOgreRenderWindow+for+Eihort

#include <ma/wx/GuiAccess.h>
#include <ma/Root.h>

#include "Ogre/Context.h"
#include "Ogre/Ogre.h"

namespace ma::Ogre
{
    // Root //——————————————————————————————————————————————————————————————————————————————————————————————————————————
    const ma::Item::key_type &Root::GetKeyRoot()
    {
        static const ma::Item::key_type key{Id().GetKey()};
        return key;
    }

    Root::Root(const Item::ConstructorParameters &in_params):
    ma::Item({in_params, GetKeyRoot(), true}), // note: il peut être sauvegardé
    m_RootSmile(new RootSmile()),
    m_ConfigGroup(AddReadOnlyMemberVar<ma::Ogre::ConfigGroupVar>(L"m_ConfigGroup")),
    m_RenderSystems(AddReadOnlyMemberVar<ma::EnumerationVar>(L"m_RenderSystems"))
    {
        auto config_item = ma::root()->m_Config;
        if(auto group = config_item->GetGroup(L"ogre"))
            SetVarFromValue<ma::Ogre::ConfigGroupVar>(m_ConfigGroup->GetKey(), group.value());

        // À sa création: il va se sauvegarder et ajouter plugin.cfg,
        // config.cfg …
    }

    Root::~Root() = default;

    const TypeInfo& Root::GetOgreRootTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetOgreRootClassName(),
            L"Le nœud racine pour le moteur graphique Ogre",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    void Root::CreateRoot()
    {
        MA_ASSERT(!m_RootSmile->m_Root, L"La racine « m_Root » doit être nulle.", std::logic_error);

        MA_ASSERT(!m_RootSmile->m_OverlaySystem,
                  L"Le système d'interface graphique d'ogre « m_OverlaySystem » doit être nulle.",
                  std::logic_error);

        MA_ASSERT(m_ConfigGroup->Get(), L"m_ConfigGroup est nul.", std::logic_error);

        const auto &group = m_ConfigGroup->GetItem();

        const std::filesystem::path plugins_path = group->m_PluginCfgPath->toString();
        const std::filesystem::path config_path = group->m_ConfigCfgPath->toString();
        const std::filesystem::path log_path = group->m_LogPath->toString();
        const std::filesystem::path resources_path = group->m_ResourcesDirectory->toString();

        std::wcout << L"Création de la racine d'Ogre.\n";
        std::wcout << L"Les chemins des fichiers de configuration sont:\n";
        std::wcout << L"plugins_path... : [" << std::filesystem::exists(plugins_path) << L"]" << plugins_path << '\n';
        std::wcout << L"config_path.... : [" << std::filesystem::exists(config_path) << L"]" << config_path << '\n';
        std::wcout << L"log_path....... : [" << std::filesystem::exists(log_path) << L"]" << log_path << '\n';
        std::wcout << L"resources_path. : [" << std::filesystem::exists(resources_path) << L"]" << resources_path
                   << '\n';
        std::wcout << std::endl;

        try
        {
            m_RootSmile->m_Root = OGRE_NEW ::Ogre::Root(plugins_path.string(), config_path.string(), log_path.string());
        }
        catch(const std::exception &e)
        {
            MA_ASSERT(false,
                      L"L'appel à OGRE_NEW ::Ogre::Root a échoué: \n" + ma::to_wstring(e.what()),
                      std::invalid_argument);
        }

        ::Ogre::Root *ogre_root = m_RootSmile->m_Root;

        // Test
        ::Ogre::LogManager::getSingleton().getDefaultLog()->setDebugOutputEnabled(false);

        // https://ogrecave.github.io/ogre/api/1.12/class_ogre_1_1_render_system.html#a606200a27a8adebfa62b3e388a49cf39
        // Default 	Description
        // Full Screen 	false 	Window full-screen flag
        // VSync 	true 	"vsync" in _createRenderWindow
        // VSync Interval 	1 	"vsyncInterval" in _createRenderWindow
        // sRGB Gamma Conversion 	false 	"gamma" in _createRenderWindow
        // FSAA 	0 	concatenation of "FSAA" and "FSAAHint" as in _createRenderWindow
        // Video Mode 	- 	Window resolution
        // Display Frequency 	- 	"displayFrequency" in _createRenderWindow
        // Content Scaling Factor 	1.0 	"contentScalingFactor" in _createRenderWindow

        // https://github.com/OGRECave/ogre/blob/master/Components/Bites/src/OgreApplicationContextBase.cpp

        const ::Ogre::RenderSystemList &renderers = ogre_root->getAvailableRenderers();

        MA_ASSERT(!renderers.empty(), L"Il n'y aucun système de rendu disponible pour Ogre.");

        ma::EnumerationVar::value_type values;

        auto index = 0u;
        for(auto &renderer : renderers)
        {
            values[ma::to_wstring(renderer->getName())] = index;
            index++;
        }

        SetVarFromValue<ma::EnumerationVar>(m_RenderSystems->GetKey(), values);

        ::Ogre::RenderSystem *renderer = renderers[0];

        MA_ASSERT(renderer, L"renderer est nul.");

        ogre_root->setRenderSystem(renderer);

        renderer->setConfigOption("Full Screen", "false");
        renderer->setConfigOption("VSync", "false");

        try
        {
            m_RootSmile->m_OverlaySystem = OGRE_NEW ::Ogre::OverlaySystem();
        }
        catch(const std::exception &e)
        {
            MA_ASSERT(false,
                      L"L'appel à OGRE_NEW ::Ogre::OverlaySystem a échoué: \n" + ma::to_wstring(e.what()),
                      std::invalid_argument);
        }

        ogre_root->saveConfig();

        // At this point there are no render systems, so we
        // can try to load in the previous configuration
        // if(false) // !ogre_root->restoreConfig())
        // {
        //     // https://www.ogre3d.org/docs/api/1.9/class_ogre_1_1_config_dialog.html#details
        //     // That failed so we have to show the Dialog
        //
        //     MA_ASSERT(!ogre_root->showConfigDialog(nullptr), L"Aucun système de rendu n'a été choisi.");
        // }
    }

    void Root::InitRoot()
    {
        ::Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

        auto &resource_manager = ::Ogre::ResourceGroupManager::getSingleton();

        const auto &group = m_ConfigGroup->GetItem();
        const std::filesystem::path resources_path = group->m_ResourcesDirectory->toString();

        ma::Config config_ressources{resources_path};
        config_ressources.Load();

        //*
        for(const auto &section : config_ressources.m_Datas)
            for(const auto &it : section.second)
                resource_manager.addResourceLocation(
                    ma::to_string(it.second), ma::to_string(it.first), ma::to_string(section.first));
        /*/
        ::Ogre::ConfigFile cf;
        cf.load("/home/gandi/Travail/ma/dependencies/Ogre/build/bin/resources2.cfg"s);

        auto &sections = cf.getSettingsBySection();

        for(const auto &section : sections)
            for(const auto &it : section.second)
                resource_manager.addResourceLocation(it.second, it.first, section.first);
        */

        resource_manager.initialiseAllResourceGroups();

        m_RootSmile->m_OverlaySystem = OGRE_NEW ::Ogre::OverlaySystem();
    }

    // Context //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Context::Context(const Item::ConstructorParameters &in_params):
    ma::Engine::Context(in_params, L"Ogre"),
    m_View(nullptr)
    {}

    Context::~Context() = default;

    void Context::EndConstruction()
    {
        ma::Context::EndConstruction();

        AddDataMaker<ma::Ogre::ScenefDataMaker>();
        AddDataMaker<ma::Ogre::CubefDataMaker>();
        AddDataMaker<ma::Ogre::CamerafDataMaker>();
        AddDataMaker<ma::Ogre::LightfDataMaker>();
    }

    wxWindow* Context::GetView()
    {
        return m_View;
    }

    void Context::OnClosePage(wxAuiNotebookEvent &event)
    {
        if(m_View)
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            if(m_View == center_book->GetPage(event.GetSelection()))
            {
                // Pour éviter d'appeler de nouveau la destruction de m_View.
                m_View = nullptr;
                if(auto parent = GetParentOpt())
                    parent.value()->RemoveChild(GetKey());
            }
        }
    }

    void Context::OnEnable()
    {
        ma::Context::OnEnable();

        // Nous commençons par créer la racine d'ogre :
        ma::Ogre::RootPtr root;
        const auto &key_root = ma::Ogre::Root::GetKeyRoot();

        const bool initialize_ogre_root = !ma::Item::HasItem(key_root);
        if(!initialize_ogre_root)
        {
            root = ma::Item::GetItem<ma::Ogre::Root>(key_root);
            MA_ASSERT(root->m_RootSmile->m_Root, L"La racine « m_Root » est nulle.", std::logic_error);

            MA_ASSERT(root->m_RootSmile->m_OverlaySystem,
                      L"Le système d'interface graphique d'ogre « m_OverlaySystem » est nul.",
                      std::logic_error);
        }
        else
        {
            root = ma::Item::CreateItem<ma::Ogre::Root>();
            root->CreateRoot();
        }

        ::Ogre::Root *ogre_root = root->m_RootSmile->m_Root;

        if(ma::Item::HasItem(ma::Item::Key::GetGuiAccess()))
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            m_View = new ma::Ogre::View(center_book);
            const auto name = L"OgreView: " + GetKey();
            center_book->AddPage(m_View, name);

            if(initialize_ogre_root || !ogre_root->isInitialised())
            {
                const auto auto_create_window = false;
                ogre_root->initialise(auto_create_window);
            }

            m_View->setRenderWindow(name);
            center_book->Bind(wxEVT_AUINOTEBOOK_PAGE_CLOSE, &Context::OnClosePage, this);
        }

        std::wstring message;

//        auto scene_managers = ogre_root->getSceneManagers();
//        message += L"Nombre de gestionnaire de scènes: " + toString(scene_managers.size());
//
//        for(auto &&[name, scene_manager] : scene_managers)
//        {
//            message += L"Nom: " + to_wstring(name) + L"\n";
//        }
//
//        MA_MSG(message);
    }

    void Context::OnDisable()
    {
        ma::Context::OnDisable();

        if(m_View)
        {
            auto view = m_View;
            m_View = nullptr;
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            const auto page_index = center_book->GetPageIndex(view);
            center_book->DeletePage(page_index);
        }

        RemoveChildren();
    }

    const TypeInfo& Context::GetOgreContextTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetOgreContextClassName(),
            L"Le contexte pour le moteur graphique Ogre",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

} // namespace ma::Ogre

M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Root, Ogre, Item)

M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Context, Ogre, ma::Engine::Context::GetEngineContextClassHierarchy())
M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Context, Ogre)
