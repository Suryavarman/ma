/// \file Cycles/Cycle.h
/// \author Pontier Pierre
/// \date 2022-07-12
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// \remarks À inclure dans le corps des classes de l'interface à Cycle.
/// Ce fichier ne doit pas être inclus dans API.h. Il sert aussi à la définition des pointeurs opaques.
/// La documentation de Cycles: https://wiki.blender.org/wiki/Source/Render/Cycles
/// L'espace de nom ccl pour Cycles est défini par la macro CCL_NAMESPACE_BEGIN et CCL_NAMESPACE_END dans CMakeLists.txt.
/// Exemple de projet utilisant Cycle :
/// - https://bitbucket.org/gremigi/qt-cycles/src/master/
/// - https://bitbucket.org/gremigi/qt-cycles/src/master/cycles/CyclesWidgetImpl.cpp <—————
/// - https://projects.blender.org/blender/blender/src/branch/main/intern/cycles/CMakeLists.txt
/// - Cycle4D
/// - Gaffer https://devtalk.blender.org/t/cycles-for-gaffer/390/5
///          https://github.com/boberfly/GafferCycles
/// - Rhino https://github.com/mcneel/RhinoCycles
/// - https://github.com/mcneel/CCSycles
///

#pragma once

#include <scene/background.h>
#include <scene/camera.h>
#include <scene/film.h>
#include <scene/integrator.h>
#include <scene/light.h>
#include <scene/mesh.h>
#include <scene/object.h>
#include <scene/osl.h>
#include <scene/scene.h>
#include <session/session.h>

#include <ma/Context.h>
#include <ma/engine/Node.h>
#include <ma/engine/Scene.h>
#include <ma/engine/Primitive.h>
#include <ma/engine/Camera.h>
#include <ma/engine/Light.h>

#include "Cycles/Context.h"

namespace ma::Cycles
{
    // ContextSmile // —————————————————————————————————————————————————————————————————————————————————————————————————
    class ContextSmile
    {
        public:
        ccl::Session m_Session;

        ContextSmile();
        virtual ~ContextSmile();
    };

    // Nodef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    /// NodeSmile sera placé enfant du « ma::Cycles::Item » héritant de « ma::Data ».
    /// NodeSmile met à jour m_CycleNode à partir de l'observation de du « NodefPtr » qu'observe
    /// « ma::Cycle::ITEM ».
    class Nodef: public ma::Data, public ma::Observer
    {
        protected:
        virtual void SetObservation(const ma::ObservablePtr &observable);

        void OnEnable() override;
        void OnDisable() override;

        /// Est appelé par EndConstruction.
        /// Cette fonction appellera m_Scene->create_node<T>.
        virtual void CreateNode() = 0;

        /// Est appelé par le destructeur.
        /// Cette fonction appellera m_Scene->delete_node<T>.
        virtual void DestroyNode() = 0;

        public:
        ma::Cycles::ContextPtr m_CyclesContext;
        ma::Engine::NodefPtr m_ItemNode;

        /// Accès rapide à l'instance de m_CycleContext->m_ContextSmile->m_Session.m_Scene;
        ccl::Scene *m_Scene;

        /// Le nœud de Cycle. Il est créé via CreateNode.
        /// Il peut être nul si m_NodeOwner ne l'est pas et inversement.
        ccl::Node *m_Node;

        /// Certain objet de Cycles n'ont pas de nœud, car ils sont propriétaires de nœuds.
        /// La scène par exemple est propriétaire.
        /// Donc si l'objet de Cycles hérite d'un nœud m_NodeOwner sera nul et m_Node pointera sur une instance d'un
        /// nœud.
        /// Si l'objet de Cycles n'hérite pas de la classe « ccl::Node » alors m_NodeOwner pointera sur l'instance de
        /// cet objet.
        ccl::NodeOwner *m_NodeOwner;

        /// La transformation qui sera utilisée pour éditer la position, l'orientation et l'échelle des Nœuds.
        ccl::Transform m_Transform;

        explicit Nodef(const ma::Item::ConstructorParameters &in_params,
                       const ContextLinkPtr &link,
                       ma::ItemPtr client);
        Nodef() = delete;
        ~Nodef() override;

        /// Pour finaliser la construction.
        void EndConstruction() override;

        void BeginObservation(const ObservablePtr &observable) override;
        void UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data) override;
        void EndObservation(const ObservablePtr &) override;

        /// Charge la donnée dans le contexte.
        bool Load() override;

        /// Décharge la donnée du contexte.
        bool UnLoad() override;

        /// Par défaut le comportement est d'abord d'appeler Unload et si celui-ci renvoie vrai.
        bool Reload() override;
        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, Cycles)
    };

    // Scenef // ———————————————————————————————————————————————————————————————————————————————————————————————————————
    class Scenef: public Nodef
    {
        protected:
        /// Est appelé par EndConstruction.
        /// Cette fonction appellera m_Scene->create_node<T>.
        void CreateNode() override;

        /// Est appelé par le destructeur.
        /// Cette fonction appellera m_Scene->delete_node<T>.
        void DestroyNode() override;

        public:
        ma::Engine::ScenefPtr m_ItemScene;
        ccl::Scene* m_Scene;

        explicit Scenef(const Item::ConstructorParameters &in_params,
                        const ContextLinkPtr &link,
                        ma::ItemPtr client);
        ~Scenef() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Scenef, Cycles)
    };
    typedef std::shared_ptr<Scenef> ScenefPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, Cycles)

    // Cubef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    class Cubef: public Nodef
    {
        protected:
        /// Est appelé par EndConstruction.
        /// Cette fonction appellera m_Scene->create_node<T>.
        void CreateNode() override;

        /// Est appelé par le destructeur.
        /// Cette fonction appellera m_Scene->delete_node<T>.
        void DestroyNode() override;

        public:
        ma::Engine::CubefPtr m_ItemCube;
        ccl::Mesh *m_Mesh;
        ccl::Object *m_Object;

        explicit Cubef(const Item::ConstructorParameters &in_params,
                       const ContextLinkPtr &link,
                       ma::ItemPtr client);
        Cubef() = delete;
        ~Cubef() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cubef, Cycles)
    };
    typedef std::shared_ptr<Cubef> CubefPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, Cycles)

    // Cameraf // ——————————————————————————————————————————————————————————————————————————————————————————————————————
    class Cameraf: public Nodef
    {
        protected:
        /// Est appelé par EndConstruction.
        /// Cette fonction appellera m_Scene->create_node<T>.
        void CreateNode() override;

        /// Est appelé par le destructeur.
        /// Cette fonction appellera m_Scene->delete_node<T>.
        void DestroyNode() override;

        void SetObservation(const ma::ObservablePtr &observable) override;

        public:
        ma::Engine::CamerafPtr m_ItemCamera;

        ccl::Camera *m_Camera;

        /// \exception std::logic_error link->GetContext() renvoie null.
        /// \exception std::logic_error link->GetContext() n'est pas du type ma::Cycles::Context.
        /// \exception std::logic_error  std::dynamic_pointer_cast<ma::Cycles::Context>(link->GetContext())->m_View
        /// est nul.
        explicit Cameraf(const Item::ConstructorParameters &in_params,
                         const ContextLinkPtr &link,
                         ma::ItemPtr client);
        Cameraf() = delete;
        ~Cameraf() override;

        void EndConstruction() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Cameraf, Cycles)
    };
    typedef std::shared_ptr<Cameraf> CamerafPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, Cycles)

    // Lightf // ———————————————————————————————————————————————————————————————————————————————————————————————————————
    class Lightf: public Nodef
    {
        protected:
        /// Est appelé par EndConstruction.
        /// Cette fonction appellera m_Scene->create_node<T>.
        void CreateNode() override;

        /// Est appelé par le destructeur.
        /// Cette fonction appellera m_Scene->delete_node<T>.
        void DestroyNode() override;

        void SetObservation(const ma::ObservablePtr &observable) override;

        public:
        ma::Engine::LightfPtr m_ItemLight;

        ccl::Light *m_Light;

        explicit Lightf(const Item::ConstructorParameters &in_params,
                        const ContextLinkPtr &link,
                        ma::ItemPtr client);
        Lightf() = delete;
        ~Lightf() override;

        void EndConstruction() override;

        M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Lightf, Cycles)
    };
    typedef std::shared_ptr<Lightf> LightfPtr;
    M_HEADER_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, Cycles)

} // namespace ma::Cycles
