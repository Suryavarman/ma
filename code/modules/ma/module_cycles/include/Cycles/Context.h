/// \file Cycles/Context.h
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include "ma/Context.h"
#include "ma/Config.h"
#include "ma/engine/Scene.h"
#include "ma/engine/Primitive.h"
#include "ma/engine/Camera.h"
#include "ma/engine/Light.h"
#include "ma/engine/Context.h"

#include "Cycles/View.h"

#include <wx/aui/auibook.h>

namespace ma::Cycles
{
    class ContextSmile;

    class Context: public ma::Engine::Context
    {
        protected:
            void OnEnable() override;
            void OnDisable() override;

            /// Chaque contexte créé une fenêtre. À la destruction du contexte celle-ci sera détruite.
            ma::Cycles::View *m_View;

        public:
            std::unique_ptr<ContextSmile> m_ContextSmile;

            explicit Context(const Item::ConstructorParameters &in_params);
            Context() = delete;
            ~Context() override;
            void EndConstruction() override;

            void OnClosePage(wxAuiNotebookEvent &event);

            wxWindow* GetView() override;

            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(Context, Cycles)
    };
    typedef std::shared_ptr<Context> ContextPtr;

} // namespace ma::Cycle

namespace ma::Engine
{
    template std::shared_ptr<ma::Cycles::Context> ma::Engine::CreateContext<ma::Cycles::Context>();
}

M_HEADER_MAKERITEM_TYPE_WITH_NAMESPACE(Context, Cycles)
