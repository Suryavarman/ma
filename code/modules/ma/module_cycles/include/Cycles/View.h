/// \file Cycles/View.h
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///
/// https://github.com/wxWidgets/wxWidgets/blob/WX_3_0_BRANCH/samples/opengl/cube/cube.h
///

#pragma once

#include <memory>

#include <wx/glcanvas.h>

#include <ma/Ma.h>



namespace ma::Cycles
{
    class wxGLContext : public ::wxGLContext
    {
        public:
            explicit wxGLContext(wxGLCanvas *canvas,
                                 const wxGLContext *other = nullptr,
                                 const wxGLContextAttrs *ctxAttrs = nullptr);

            wxGLContext() = delete;
            ~wxGLContext() override;

            void Init();

            /// Rend un plan sur lequel est plaquée la texture du rendu.
            void DrawPlane();

            private:
            /// L'identifiant de la texture où sera enregistré le rendu.
            GLuint m_Texture;
    };

    class View: public wxGLCanvas
    {
        public:
            View() = delete;
            explicit View(wxWindow *parent);
            ~View() override;
            std::unique_ptr<ma::Cycles::wxGLContext> m_Context;

        private:
            void OnPaint(wxPaintEvent &event);
            void OnKeyDown(wxKeyEvent& event);
            void InitEvents();
            void InitContext();
    };

} // namespace ma::Cycles
