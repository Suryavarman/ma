/// \file Cycles/Config.h
/// \author Pontier Pierre
/// \date 23-11-2023
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#pragma once

#include <ma/Item.h>
#include <ma/Variable.h>
#include <ma/wx/Variable.h>
#include <ma/Config.h>

namespace ma
{
    namespace Cycles
    {
        ///  [cycles]
        class ConfigGroup:
        public ma::ConfigGroup
        {
            public:
            static const key_type ms_Key;

            /// \brief Constructeur de ConfigGroup.
            /// \param params Les paramètres de construction d'un item.
            /// \param module Le chemin du dossier contenant le module.
            explicit ConfigGroup(const Item::ConstructorParameters &params, const std::filesystem::path& module);
            ConfigGroup() = delete;

            /// \brief Destructeur de ConfigGroup
            ~ConfigGroup() override = default;
            M_HEADER_CLASSHIERARCHY_WITH_NAMESPACE(ConfigGroup, Cycles)
        };
        typedef std::shared_ptr<ma::Cycles::ConfigGroup> ConfigGroupPtr;
    } // namespace Cycles

    /// Permet de faire référence exclusivement à un ConfigGroup dans une variable.
    M_HEADER_ITEM_TYPE_VAR_WITH_NAMESPACE(ConfigGroup, Cycles)
} // namespace API

// M_HEADER_MAKERVAR_TYPE_WITH_NAMESPACE(ConfigGroupVar, Cycles)

