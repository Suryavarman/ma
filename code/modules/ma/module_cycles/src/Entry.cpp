/// \file Entry.cpp
/// \author Pontier Pierre
/// \date 2023-11-07
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Cycles/Entry.h"
#include "Cycles/Config.h"
#include "Cycles/Context.h"

namespace ma::Cycles
{
    static bool g_Allocate = false;
    static Item::key_type g_ConfigKey{};
    static Item::key_type g_ContextMakerKey{};

    void SetupRTTI()
    {
        ma::Item::CreateParameters params{Item::Key::GetRTTI()};

        g_ContextMakerKey = ma::Item::CreateItem<ma::Cycles::ContextMaker>(params)->GetKey();
    }

    void CleanRTTI()
    {
        ma::Item::EraseItem(g_ContextMakerKey);
    }

    extern "C" void MA_CYCLES_DLL_EXPORT dllStartPlugin(const std::filesystem::path& module_path)
    {
        if(g_Allocate)
        {
            dllStopPlugin();
            dllDeletePlugin();
        }

        if(!Item::HasItem(g_ConfigKey))
        {
            auto config = Item::GetOrCreateItem<ma::Cycles::ConfigGroup>({
                                                                            Item::Key::GetConfig(),
                                                                            false,
                                                                            true,
                                                                            ma::Cycles::ConfigGroup::ms_Key
                                                                         },
                                                                         module_path);
            g_ConfigKey = config->GetKey();

            config->LoadConfig();

            config->EndConstruction();
        }

        SetupRTTI();

        g_Allocate = true;
    }

    extern "C" void MA_CYCLES_DLL_EXPORT dllStopPlugin(void)
    {
        if(g_Allocate)
        {
            if(Item::HasItem(g_ConfigKey))
                Item::EraseItem(g_ConfigKey);

            CleanRTTI();
        }
    }

    extern "C" void MA_CYCLES_DLL_EXPORT dllDeletePlugin(void)
    {
        if(g_Allocate)
        {
            // On s'assure que toutes les ressources du plugin sont bien désallouées.
            // Remarques: La question qu'il faut se poser c'est, doit-on laisser le temps à la poubelle de supprimer les
            // éléments qui ne servent plus à rien ou les libérer directement dans dllStopPlugin.
            if(auto opt = Item::HasItemOpt<Garbage>(Item::Key::GetGarbage()))
            {
                GarbagePtr garbage = opt.value();

                if(garbage->HasTrash(g_ConfigKey))
                    garbage->RemoveTrash(g_ConfigKey);
            }

            g_Allocate = false;
        }
    }
}