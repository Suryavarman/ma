/// \file Config.cpp
/// \author Pontier Pierre
/// \date 19-03-2023
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Cycles/Config.h"

namespace ma
{
    namespace Cycles
    {
        const ConfigGroup::key_type ConfigGroup::ms_Key(L"96667b64-4118-4859-869c-8972be3697b2");

        // ConfigCycle //———————————————————————————————————————————————————————————————————————————————————————————————
        ConfigGroup::ConfigGroup(const Item::ConstructorParameters &params, const std::filesystem::path& module):
        ma::ConfigGroup(params, L"cycles")
        {}

        const TypeInfo& ConfigGroup::GetCyclesConfigGroupTypeInfo()
        {
            // clang-format off
            static const TypeInfo info =
            {
                GetCyclesConfigGroupClassName(),
                L"Le groupe de données qui sont enregistrés dans le fichier de configuration l'application.",
                {
                    {
                        ma::TypeInfo::Key::GetDependenciesData(),
                        {
                            ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                        }
                    }
                }
            };
            // clang-format on
            return info;
        }

    } // namespace Cycles
    M_CPP_ITEM_TYPE_VAR_WITH_NAMESPACE(ConfigGroup, Cycles)

} // namespace API
M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(ConfigGroup, Cycles, ConfigGroup)
