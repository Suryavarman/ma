/// \file Cycles/Context.cpp
/// \author Pontier Pierre
/// \date 2021-02-13
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include <ma/wx/GuiAccess.h>

#include "Cycles/Context.h"
#include "Cycles/Cycles.h"

namespace ma::Cycles
{
    // Context //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Context::Context(const Item::ConstructorParameters &in_params):
    ma::Engine::Context(in_params, L"Cycles"),
    m_View(nullptr),
    m_ContextSmile(new ContextSmile())
    {}

    Context::~Context() = default;

    void Context::EndConstruction()
    {
        ma::Context::EndConstruction();

        AddDataMaker<ma::Cycles::ScenefDataMaker>();
        AddDataMaker<ma::Cycles::CubefDataMaker>();
        AddDataMaker<ma::Cycles::CamerafDataMaker>();
        AddDataMaker<ma::Cycles::LightfDataMaker>();
    }

    wxWindow* Context::GetView()
    {
        return m_View;
    }

    void Context::OnClosePage(wxAuiNotebookEvent &event)
    {
        if(m_View)
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            if(m_View == center_book->GetPage(event.GetSelection()))
            {
                // Pour éviter d'appeler de nouveau la destruction de m_View.
                m_View = nullptr;
                if(auto parent = GetParentOpt())
                    parent.value()->RemoveChild(GetKey());
            }
        }
    }

    void Context::OnEnable()
    {
        ma::Context::OnEnable();

        if(ma::Item::HasItem(ma::Item::Key::GetGuiAccess()))
        {
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();

            m_View = new ma::Cycles::View(center_book);

            const auto name = L"CyclesView: " + GetKey();
            center_book->AddPage(m_View, name);

            center_book->Bind(wxEVT_AUINOTEBOOK_PAGE_CLOSE, &Context::OnClosePage, this);
        }
    }

    void Context::OnDisable()
    {
        ma::Context::OnDisable();

        if(m_View)
        {
            auto view = m_View;
            m_View = nullptr;
            auto gui = ma::Item::GetItem<ma::wx::GuiAccess>(ma::Item::Key::GetGuiAccess());

            MA_ASSERT(gui, L"wxGuiAccess est nul.", std::logic_error);

            auto *center_book = gui->GetCenterNoteBook();
            const auto page_index = center_book->GetPageIndex(view);
            center_book->DeletePage(page_index);
        }

        RemoveChildren();
    }

    const TypeInfo& Context::GetCyclesContextTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetCyclesContextClassName(),
            L"Le contexte pour afficher le rendu de Cycles dans une texture.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

} // namespace ma::Cycles
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Context, Cycles, ma::Engine::Context::GetEngineContextClassHierarchy())
M_CPP_MAKERITEM_TYPE_WITH_NAMESPACE(Context, Cycles)