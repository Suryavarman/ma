/// \file Cycles/Cycle.cpp
/// \author Pontier Pierre
/// \date 2023-05-28
/// \copyright Pierre Pontier (https://www.suryavarman.fr)
/// \license MIT
///
/// Droit d'auteur (c) 2018-2024.
/// Sous license MIT voir le fichier LICENSE à la racine du dépôt.
///
/// Copyright (c) 2018-2024.
/// Licensed to MIT see the LICENSE file in the repository root location.
///

#include "Cycles/Cycles.h"

namespace ma::Cycles
{
    std::string GetNewName(ma::ItemPtr item, const std::wstring &default_base_name)
    {
        auto node_name = default_base_name;

        if(item->HasVar(Item::Key::Var::GetName()))
            node_name = item->GetVar(Item::Key::Var::GetName())->toString();

        if(node_name.empty())
            node_name = L"node";

        node_name += L"_" + item->GetKey();

        return ma::to_string(node_name);
    }

    // ContextSmile // —————————————————————————————————————————————————————————————————————————————————————————————————
    ContextSmile::ContextSmile():
    m_Session(ccl::SessionParams(), ccl::SceneParams())
    {}

    ContextSmile::~ContextSmile() = default;

    // Nodef // ————————————————————————————————————————————————————————————————————————————————————————————————————————
    Nodef::Nodef(const ma::Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    ma::Data::Data(in_params, link, client),
    ma::Observer(),
    m_CyclesContext(std::dynamic_pointer_cast<ma::Cycles::Context>(link->GetContext())),
    m_ItemNode(std::dynamic_pointer_cast<ma::Engine::Nodef>(client)),
    m_Scene(nullptr),
    m_Node(nullptr),
    m_NodeOwner(nullptr),
    m_Transform(ccl::transform_identity())
    {}

    // Devrait être appelé par UnLoad, DestroyNodes étant virtuelle, elle ne doit pas être appelée dans ce
    // destructeur.
    // MA_ASSERT(m_Scene, L"m_Scene est nul.", std::logic_error);
    // MA_ASSERT(m_Node, L"m_Node est nul.", std::logic_error);
    //
    // DestroyNode();
    // m_Node = nullptr;
    Nodef::~Nodef() = default;

    void Nodef::EndConstruction()
    {
        MA_ASSERT(m_CyclesContext, L"m_CycleContext est nul.", std::logic_error);
        MA_ASSERT(m_CyclesContext->m_ContextSmile, L"m_CycleContext->m_ContextSmile est nul.", std::logic_error);
        MA_ASSERT(m_ItemNode, L"m_ItemScene est nul.", std::logic_error);
        MA_ASSERT(
            ma::Item::HasItem(m_ItemNode->GetParentKey()), L"m_ItemNode n'a pas de parent.", std::logic_error);

        auto& session = m_CyclesContext->m_ContextSmile->m_Session;

        m_Scene = session.scene;

        auto cycle_item_parent = ma::Item::GetItem(GetParentKey());

        // Si le parent est un nœud ce pointeur ne sera pas nul.
        // auto cycle_node_parent = std::dynamic_pointer_cast<ma::Cycles::Nodef>(cycle_item_parent);
        CreateNode();

        if(!m_NodeOwner)
        {
            MA_ASSERT(m_Node, L"m_Node est nul. CreateNode doit allouer m_Node.", std::logic_error);
            m_Node->name = GetNewName(m_ItemNode, L"node");
        }
        else
            MA_ASSERT(!m_Node, L"m_Node n'est pas nul alors que m_NodeOwner ne l'ai pas.", std::logic_error);

        // Cette fonction déclenchera l'évènement OnEnable qui va appeler Load et qui aura besoin de m_Node
        ma::Item::EndConstruction();

        if(ma::Item::HasItem(Key::GetObservationManager()))
        {
            auto observer_manager = ma::Item::GetItem<ma::ObservationManager>(Key::GetObservationManager());
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Position);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Quaternion);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Euler);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemNode->m_Scale);
        }
        else
        {
            MA_WARNING(false,
                       L"Le gestionnaire d'observations n'ai pas disponible. "
                       L"Aucun item n'est lié à la clef qui lui est attitrée: « " +
                           Key::GetObservationManager() + L" ». Ce nœud ne pourra pas observer les variables: \n" +
                           L" - m_Position \n - m_Quaternion \n - m_Euler \n - m_Scale");
        }
    }

    void Nodef::SetObservation(const ma::ObservablePtr &observable)
    {
        // Test
        MA_ASSERT(IsEnable(), L"Ne devrait pas être appelé si le nœud est inactif.");

//        if(m_ItemNode->m_Position == observable || m_ItemNode->m_Quaternion == observable ||
//           m_ItemNode->m_Scale == observable || m_ItemNode->m_Euler)
//        {
//            const auto &eigen_transformation = m_ItemNode->m_Transform->Get();
//
//            //*
//            MA_ASSERT(eigen_transformation.cols() * eigen_transformation.rows() >= 12,
//                      L"Le nombre d'éléments est inférieur à celui requis.",
//                      std::logic_error);
//
//            auto *data = eigen_transformation.data();
//
//            m_Transform.x.x = *data++;
//            m_Transform.x.y = *data++;
//            m_Transform.x.z = *data++;
//            m_Transform.x.w = *data++;
//            m_Transform.y.x = *data++;
//            m_Transform.y.y = *data++;
//            m_Transform.y.z = *data++;
//            m_Transform.y.w = *data++;
//            m_Transform.z.x = *data++;
//            m_Transform.z.y = *data++;
//            m_Transform.z.z = *data++;
//            m_Transform.z.w = *data;
//        }

        if(m_ItemNode->m_Position == observable || m_ItemNode->m_Quaternion == observable ||
           m_ItemNode->m_Scale == observable || m_ItemNode->m_Euler == observable)
        {
            const auto &position = m_ItemNode->m_Position->Get();
            const auto &euler = m_ItemNode->m_Euler->Get();
            const auto &scale = m_ItemNode->m_Scale->Get();

            const auto cycle_scale = ccl::transform_scale(scale.x(), scale.y(), scale.z());
            const auto cycle_position = ccl::transform_translate(position.x(), position.y(), position.z());
            const auto cycle_rotation = ccl::euler_to_transform(ccl::make_float3(euler.x(), euler.y(), euler.z()));

            m_Transform = cycle_scale * cycle_position * cycle_rotation;
        }
    }

    void Nodef::BeginObservation(const ObservablePtr &observable)
    {
        SetObservation(observable);
    }

    void Nodef::UpdateObservation(const ObservablePtr &observable, const ma::Observable::Data &data)
    {
        SetObservation(observable);
    }

    void Nodef::EndObservation(const ObservablePtr &observable)
    {
        m_CyclesContext.reset();
    }

    void Nodef::OnEnable()
    {
        Observer::OnEnable();
        Data::OnEnable();
    }

    void Nodef::OnDisable()
    {
        Observer::OnDisable();
        Data::OnDisable();
    }

    bool Nodef::Load()
    {
        bool result = Data::Load();

        if(m_NodeOwner)
            MA_ASSERT(!m_Node, L"m_Node n'est pas nul alors que m_NodeOwner ne l'est pas.", std::logic_error);
        else
            MA_ASSERT(m_Node, L"m_Node est nul.", std::logic_error);

        // \todo EndConstruction ne devrait-il pas appeler Load() ?

        // const auto cascade = true;
        // m_Node->setVisible(true, cascade);

        return result;
    }

    bool Nodef::UnLoad()
    {
        if(m_Node)
        {
            DestroyNode();

            // MA_ASSERT(!m_Node,
            //           L"m_Node est nul. CreateNode doit désallouer m_Node et le mettre à nul.",
            //           std::logic_error);
         }

        m_CyclesContext.reset();

        return Data::UnLoad();
    }

    bool Nodef::Reload()
    {
        return Data::Reload();
    }

    const TypeInfo& Nodef::GetCyclesNodefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetCyclesNodefClassName(),
            L"Nodef sera placé enfant du « ma::Cycles::ITEM » héritant de « ma::Data ».\n"
            L"Nodef met à jour m_Node à partir de l'observation du « NodefPtr » "
            L"qu'observe\n « ma::Cycles::ITEM »..\n",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Scenef //————————————————————————————————————————————————————————————————————————————————————————————————————————
    Scenef::Scenef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemScene(std::dynamic_pointer_cast<ma::Engine::Scenef>(client))
    {
        MA_ASSERT(m_ItemScene, L"m_ItemScene est nul.", std::invalid_argument);
    }

    Scenef::~Scenef() = default;

    void Scenef::CreateNode()
    {
        m_Node = nullptr;
        m_NodeOwner = m_Scene;
    }

    void Scenef::DestroyNode()
    {}

    const TypeInfo& Scenef::GetCyclesScenefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetCyclesScenefClassName(),
            L"Implémentation de ma::Scenef pour Cycles.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cubef //—————————————————————————————————————————————————————————————————————————————————————————————————————————
    Cubef::Cubef(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemCube(std::dynamic_pointer_cast<ma::Engine::Cubef>(client)),
    m_Mesh(nullptr),
    m_Object(nullptr)
    {}

    Cubef::~Cubef() = default;

    void Cubef::CreateNode()
    {
        m_Mesh = new ccl::Mesh();
        m_Scene->geometry.push_back(m_Mesh);

        m_Object = new ccl::Object();
        m_Object->set_geometry(m_Mesh);
        m_Object->set_tfm(m_Transform);
        m_Scene->objects.push_back(m_Object);

        m_Node = m_Object;
    }

    void Cubef::DestroyNode()
    {
        MA_ASSERT(m_Mesh,
                  L"m_Mesh est nul.",
                  std::logic_error);

        // geometry
        {
            const auto it_find = std::find(m_Scene->geometry.begin(), m_Scene->geometry.end(), m_Mesh);
            if(it_find != std::end(m_Scene->geometry))
                m_Scene->geometry.erase(it_find);
        }

        // object
        {
            const auto it_find = std::find(m_Scene->objects.begin(), m_Scene->objects.end(), m_Object);
            if(it_find != std::end(m_Scene->objects))
                m_Scene->objects.erase(it_find);
        }
    }

    const TypeInfo& Cubef::GetCyclesCubefTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetCyclesCubefClassName(),
            L"Implémentation de ma::Cubef pour Cycle.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on
        return info;
    }

    // Cameraf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Cameraf::Cameraf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemCamera(std::dynamic_pointer_cast<ma::Engine::Cameraf>(client)),
    m_Camera(nullptr)
    {}

    Cameraf::~Cameraf() = default;

    void Cameraf::EndConstruction()
    {
        Nodef::EndConstruction();

        MA_ASSERT(m_ItemCamera, L"m_ItemCamera est nul.", std::logic_error);

//        C'est fait dans create Node
//        MA_ASSERT(m_Scene->camera, L"m_Scene->camera est nul.", std::runtime_error);
//
//        m_Camera = m_Scene->camera;

//        auto context = m_Link->GetItem()->GetContext();
//        MA_ASSERT(context, L"context est nul.", std::logic_error);
//
//        auto cycle_context = std::dynamic_pointer_cast<ma::Cycles::Context>(context);
//        MA_ASSERT(cycle_context, L"cycle_context n'est pas du type ma::Cycles::Context", std::logic_error);
//
//        auto *view = cycle_context->m_View;
//        MA_ASSERT(view, L"m_View est nul", std::logic_error);
//
//        MA_ASSERT(view->m_ViewSmile, L"m_View->m_ViewSmile est nul", std::logic_error);
//
//        auto *render_window = view->m_ViewSmile->m_RenderWindow;
//        MA_ASSERT(render_window, L"render_window est nul.", std::runtime_error);
//
//        int ZOrder = 0;
//        float left = 0.0f;
//        float top = 0.0f;
//        float width = 1.0f;
//        float height = 1.0f;
//        m_Viewport = render_window->addViewport(m_Camera, ZOrder, left, top, width, height);

        if(auto opt = ma::Item::HasItemOpt<ma::ObservationManager>(Key::GetObservationManager()))
        {
            auto &observer_manager = opt.value();
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_BackgroundColour);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Far);
            observer_manager->AddObservation<ma::ItemObserverVar, ma::VarVar>(this, m_ItemCamera->m_Near);
        }
    }

    void Cameraf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);
    }

    void Cameraf::CreateNode()
    {
        m_Camera = m_Scene->camera;
        m_Node = m_Camera;
    }

    void Cameraf::DestroyNode()
    {}

    const TypeInfo& Cameraf::GetCyclesCamerafTypeInfo()
    {
        // clang-format off
        static const TypeInfo info =
        {
            GetCyclesCamerafClassName(),
            L"Implémentation de ma::Engine::Cameraf pour Cycles.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

    // Lightf //———————————————————————————————————————————————————————————————————————————————————————————————————————
    Lightf::Lightf(const Item::ConstructorParameters &in_params, const ContextLinkPtr &link, ma::ItemPtr client):
    Nodef::Nodef(in_params, link, client),
    m_ItemLight(std::dynamic_pointer_cast<ma::Engine::Lightf>(client)),
    m_Light(nullptr)
    {}

    Lightf::~Lightf() = default;

    void Lightf::EndConstruction()
    {
        Nodef::EndConstruction();

        MA_ASSERT(m_ItemLight, L"m_ItemLight est nul.", std::invalid_argument);
    }

    void Lightf::CreateNode()
    {
        m_Light = new ccl::Light();
        auto *shader = new ccl::Shader();
        m_Scene->shaders.push_back(shader);
        m_Scene->lights.push_back(m_Light);

        m_Node = m_Light;
    }

    void Lightf::DestroyNode()
    {}

    void Lightf::SetObservation(const ma::ObservablePtr &observable)
    {
        Nodef::SetObservation(observable);
    }

    const TypeInfo& Lightf::GetCyclesLightfTypeInfo()
    {
        // clang-format off
        static const ma::TypeInfo info =
        {
            GetCyclesLightfClassName(),
            L"Implémentation de ma::Engine::Lightf pour Cycles.",
            {
                {
                    ma::TypeInfo::Key::GetDependenciesData(),
                    {
                        ma::Module::GetGuid(std::filesystem::path{M_FILE_NAME}.parent_path())
                    }
                }
            }
        };
        // clang-format on

        return info;
    }

} // namespace ma::Cycles

M_CPP_CLASSHIERARCHY_WITH_NAMESPACE(Nodef, Cycles, Data, Observer)
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Scenef, Cycles, ma::Cycles::Nodef::GetCyclesNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cubef, Cycles, ma::Cycles::Nodef::GetCyclesNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Cameraf, Cycles, ma::Cycles::Nodef::GetCyclesNodefClassHierarchy())
M_CPP_CLASSHIERARCHY2_WITH_NAMESPACE(Lightf, Cycles, ma::Cycles::Nodef::GetCyclesNodefClassHierarchy())

M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Scenef, Cycles, ma::Engine::Scenef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cubef, Cycles, ma::Engine::Cubef)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Cameraf, Cycles, ma::Engine::Cameraf)
M_CPP_MAKERDATA_TYPE_WITH_NAMESPACE(Lightf, Cycles, ma::Engine::Lightf)
