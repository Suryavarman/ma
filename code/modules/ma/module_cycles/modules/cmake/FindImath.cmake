# - Find IMATH library
# Find the native IMATH includes and library
# This module defines
#  IMATH_INCLUDE_DIRS, where to find ImathMath.h, Set when
#                            IMATH_INCLUDE_DIR is found.
#  IMATH_LIBRARIES, libraries to link against to use IMATH.
#  IMATH_ROOT_DIR, The base directory to search for IMATH.
#                        This can also be an environment variable.
#  IMATH_FOUND, If false, do not try to use IMATH.
#
# also defined, but not for general use are
#  IMATH_LIBRARY, where to find the IMATH library.

# If IMATH_ROOT_DIR was defined in the environment, use it.
IF(NOT IMATH_ROOT_DIR AND NOT $ENV{IMATH_ROOT_DIR} STREQUAL "")
    SET(IMATH_ROOT_DIR $ENV{IMATH_ROOT_DIR})
ENDIF()

SET(_imath_SEARCH_DIRS
        ${IMATH_ROOT_DIR}
        /opt/lib/imath
        )

FIND_PATH(IMATH_INCLUDE_DIR
        NAMES
        Imath/ImathMath.h
        HINTS
        ${_imath_SEARCH_DIRS}
        PATH_SUFFIXES
        include
        )

FIND_LIBRARY(IMATH_LIBRARY
        NAMES
        Imath
        HINTS
        ${_imath_SEARCH_DIRS}
        PATH_SUFFIXES
        lib64 lib
        )

# handle the QUIETLY and REQUIRED arguments and set IMATH_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Imath DEFAULT_MSG
        IMATH_LIBRARY IMATH_INCLUDE_DIR)

IF(IMATH_FOUND)
    SET(IMATH_LIBRARIES ${IMATH_LIBRARY})
    SET(IMATH_INCLUDE_DIRS ${IMATH_INCLUDE_DIR})
ENDIF()

MARK_AS_ADVANCED(
        IMATH_INCLUDE_DIR
        IMATH_LIBRARY
)

UNSET(_imath_SEARCH_DIRS)
