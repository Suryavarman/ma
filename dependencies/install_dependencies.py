#!python3
# Copyright 20019-2020 Maçon de l'espace. All Rights Reserved.
# Licensed to MIT see LICENSE
# Pour utiliser install_dependencies il vous faudra un environnement python avec les dépendances suivantes:
# - psutil
# - wget
# - requests

import os
import ma_dep

import argparse

arg_parser = argparse.ArgumentParser(description='Définition des chemins pour compiler les dépendances de Ma.')

arg_parser.add_argument('--auto',
                        help="Si défini, alors les dépendances seront téléchargées et mises à jour avec la version "
                             "recommandée.",
                        action='store_true')

arg_parser.add_argument('--no_colour',
                        help="Si défini, le flux sortant ne sera pas coloré. "
                             "Ce qui est utile pour sauvegarder dans un fichier log.",
                        action='store_true')

arg_parser.add_argument('--venv_path',
                        help="Le chemin de l'environnement virtuel à créé. Par défaut le chemin est en locale 'venv'.",
                        type=str,
                        default="venv")
help_sr = "A partir de quel source souhaitez vous installer wxPython:" + os.linesep

help_sr += "--wxp_python_from " + str(ma_dep.InstallationFrom.undefined.name) + \
           ": C'est la valeur par défaut. Au fil de l'installation des questions vous seront posées." + os.linesep

help_sr += "--wxp_python_from " + str(ma_dep.InstallationFrom.git.name) + \
           ": L'installation se fera à partir du dépôt git suivant: " + os.linesep + ma_dep.InstallwxPython.ms_GitUrl + \
           os.linesep + " le tag utilisé sera: " + os.linesep + ma_dep.InstallwxPython.ms_Tag + os.linesep

help_sr += "--wx_python_from " + str(ma_dep.InstallationFrom.own_archive.name) + \
           ": Placez votre archive wxPython dans le dossier Ma/dependencies. Elle doit être au format «" + \
           ma_dep.InstallwxPython.ms_SnapShotArchiveGlobPattern + "». Pour trouver des archives wxPython rendez-vous à cette " \
                                                                  "adresse: " + os.linesep + "(snapshot-builds): https://wxpython.org/Phoenix/snapshot-builds/" + os.linesep

help_sr += "--wx_python_from " + str(ma_dep.InstallationFrom.default_archive.name) + ": L'archive sera téléchargée à " \
                                                                                     "partir de l'url suivante: " + os.linesep + ma_dep.InstallwxPython.ms_ArchiveUrl

arg_parser.add_argument('--wx_python_from',
                        help=help_sr,
                        type=str,
                        default=ma_dep.InstallationFrom.undefined.name)

args = arg_parser.parse_args()

auto = args.auto is not None and args.auto is True
ma_dep.Colours.enable = not (args.no_colour is not None and args.no_colour is True)

print(f"""
Début de l'installation des dépendances du projet et de ses modules.
 {ma_dep.cl_key("Ma")}      : {ma_dep.cl_url("https://framagit.org/Suryavarman/ma/")}
 {ma_dep.cl_key("modules")} : {ma_dep.cl_url("https://framagit.org/Suryavarman/ma/-/tree/master/code/modules/ma")}
""")

ma_dep.InstallLinux().go()

install_py_env = ma_dep.InstallPyEnv(venv_path=os.path.abspath(args.venv_path), auto=auto)
install_py_env.go()

ma_dep.InstallCMake(auto=auto).go()  # doit être installé après avoir créé le venv.

ma_dep.InstallClang(auto=auto).go()

ma_dep.Installpybind11(auto=auto).go()

ma_dep.InstallOgre(auto=auto).go()

ma_dep.InstallCycles(auto=auto, py_env_dep=install_py_env).go()

ma_dep.InstallCriterion(auto=auto).go()

ma_dep.InstallEigen(auto=auto).go()

ma_dep.InstallPanda(auto=auto).go()

ma_dep.InstallRaylib(auto=auto).go()

install_wx = ma_dep.InstallwxPython(installation_from=ma_dep.InstallationFrom[args.wx_python_from],
                                    auto=auto,
                                    py_env_dep=install_py_env)

install_wx.go()

# Les dépendances pour Lanceur.py
ma_dep.Install.install_py_modules(install_wx, ["psutil"])

print("Fin de l'installation des dépendances.")
