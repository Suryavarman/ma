#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import glob
import os
from pathlib import Path
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.
import ma_dep


# Sur Mageia il vous faudra installer lib64pugixml-devel


class InstallOgre(ma_dep.Install):
    ms_UseLastTag = True  # si vrais ms_GitTag sera ignoré, le dernier tag du dépôt sera utilisé.
    ms_GitTag = "v14.2.3"
    ms_GitUrl = 'https://github.com/OGRECave/ogre.git'
    ms_BackupArchive = f'https://codeload.github.com/OGRECave/ogre/zip/refs/tags/%TAG%'
    ms_GitVCPKG_Tag = "2023.04.15"
    ms_vcpkg_url = 'https://github.com/Microsoft/vcpkg.git'
    ms_GitVCPKG2_Tag = "2020.11"
    ms_vcpkg2_Url = 'https://github.com/IRCAD-IHU/vcpkg.git'

    # ms_vcpkg2_Branch = 'ogre-limit-dependencies' # cette branche n'existe plus
    # xcode-select --install
    # brew install gcc
    # git clone https://github.com/Microsoft/vcpkg.git
    # cd vcpkg
    # ./bootstrap-vcpkg.sh
    # ./vcpkg integrate install
    # vcpkg install ogre

    # CMake projects should use: "-DCMAKE_TOOLCHAIN_FILE=/Users/gandi/Working/vcpkg/scripts/buildsystems/vcpkg.cmake"

    def __init__(self, auto=False):
        super().__init__(name='Ogre')
        self.m_vcpkg_dir = os.path.join(self.m_Dir, "vcpkg")
        #self.m_vcpkg2_Dir = os.path.join(self.m_Dir, "vcpkg2")
        self.auto = auto

        if self.m_Result.m_Done:
            if InstallOgre.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallOgre.ms_GitTag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
Attention le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.
{ma_dep.cl_index('4)')} Utiliser ce dossier existant et poursuivre la compilation de {self.get_terminal_name()}."""

            if os.path.exists(self.m_vcpkg_dir):
                message += f"""
{ma_dep.cl_index('5)')} Utiliser le répertoire vcpkg existant et recompiler {self.get_terminal_name()} à partir de lui.
{ma_dep.cl_index('6)')} Supprimer le dossier vcpkg et compiler {self.get_terminal_name()} à partir d'une version récente de vcpkg."""
            else:
                message += f"""
{ma_dep.cl_index('5)')} Utiliser le projet vcpkg pour compiler {self.get_terminal_name()}."""

            message += f"""
Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')} ):"""

            if self.auto:
                nb = 1
            else:
                nb = input(message)

            try:

                number = int(nb)
                if 0 < number <= 6:
                    if number == 1:
                        self.remove_install_folder()
                        self.git_clone_sources()

                    if number == 1 or number == 3 or number == 4:
                        self.build(False if number == 4 else True)

                    if number == 6 and os.path.exists(self.m_vcpkg_dir):
                        self.remove_folder(self.m_vcpkg_dir)

                    if number == 5 or number == 6:
                        if not os.path.exists(self.m_vcpkg_dir):
                            self.git_clone_sources(use_vcpkg_package=True)

                        self.vcpkg_build()
                else:
                    raise ValueError("Vous n'avez pas rentré le bon numéro.")

            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")
        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self, use_vcpkg_package: bool = False):
        if use_vcpkg_package:
            # ma_dep.Install.git_clone(self,
            #                          url=self.ms_vcpkg2_Url,
            #                          update_submodule=True,
            #                          tags=self.ms_GitVCPKG2_Tag,
            #                          manual_dir=self.m_vcpkg2_Dir)
            ma_dep.Install.git_clone(self,
                                     url=self.ms_vcpkg_url,
                                     update_submodule=True,
                                     tag=self.ms_GitVCPKG_Tag,
                                     manual_dir=self.m_vcpkg_dir,
                                     use_last_tag=self.ms_UseLastTag)
        else:
            ma_dep.Install.git_clone(self,
                                     url=self.ms_GitUrl,
                                     update_submodule=True,
                                     tag=self.ms_GitTag,
                                     use_last_tag=self.ms_UseLastTag,
                                     archive_backup=self.ms_BackupArchive)

    def build(self, remove_build_dir=True):
        # https://ogrecave.github.io/ogre/api/latest/building-ogre.html
        os.chdir(self.m_Dir)
        build_path = os.path.join(self.m_Dir, "build")

        if os.path.exists(build_path) and remove_build_dir:
            self.remove_folder(build_path)

        if not os.path.exists(build_path):
            os.mkdir(build_path)

        os.chdir(build_path)

        if sys.platform.startswith('linux'):
            self.build_linux()
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.build_windows()
        elif sys.platform.startswith('darwin'):
            self.build_mac()
        else:
            assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

    @staticmethod
    def build_linux():
        options = "-DOGRE_CONFIG_THREAD_PROVIDER=std"

        ma_dep.call(f'cmake {options} ..')
        ma_dep.call('make -j ' + str(ma_dep.thread_count()))

    def build_mac(self):
        message = f"""
{ma_dep.cl_index('1)')} Installer les dépendances via Homebrew
{ma_dep.cl_index('2)')}  Sauter l'étape de l'installation des dépendances.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')} ):
"""
        if self.auto:
            nb = 1
        else:
            nb = input(message)

        try:
            number = int(nb)
            if 0 < number <= 2:
                if number == 1:
                    ma_dep.call('brew install assimp')
                    ma_dep.call('brew install brotli')
                    ma_dep.call('brew install freeimage')
                    ma_dep.call('brew install freetype')
                    ma_dep.call('brew install jasper')
                    ma_dep.call('brew install jxrlib')
                    ma_dep.call('brew install lcms')
                    ma_dep.call('brew install xz')  # c'est pour liblzma
                    ma_dep.call('brew install libraw')
                    ma_dep.call('brew install minizip')
                    ma_dep.call('brew install openexr')
                    ma_dep.call('brew install openjpeg')
                    ma_dep.call('brew install pugixml')
                    ma_dep.call('brew install rapidjson')
                    ma_dep.call('brew install sdl2')
                    ma_dep.call('brew install libtiff')
                    ma_dep.call('brew install gdk-pixbuf')

                    ma_dep.call('brew upgrade assimp')
                    ma_dep.call('brew upgrade brotli')
                    ma_dep.call('brew upgrade freeimage')
                    ma_dep.call('brew upgrade freetype')
                    ma_dep.call('brew upgrade jasper')
                    ma_dep.call('brew upgrade jxrlib')
                    ma_dep.call('brew upgrade lcms')
                    ma_dep.call('brew upgrade xz')  # c'est pour liblzma
                    ma_dep.call('brew upgrade libraw')
                    ma_dep.call('brew upgrade minizip')
                    ma_dep.call('brew upgrade openexr')
                    ma_dep.call('brew upgrade openjpeg')
                    ma_dep.call('brew upgrade pugixml')
                    ma_dep.call('brew upgrade rapidjson')
                    ma_dep.call('brew upgrade sdl2')
                    ma_dep.call('brew upgrade libtiff')
                    ma_dep.call('brew upgrade gdk-pixbuf')

                    ma_dep.call('brew install gtk+3')
                    ma_dep.call('brew upgrade gtk+3')
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        except ValueError:
            print("La chaîne de caractère saisie ne correspond pas à un numéro.")

        # ios ma_dep.call('cmake -DCMAKE_TOOLCHAIN_FILE=CMake/toolchain/ios.toolchain.xcode.cmake -DIOS_PLATFORM=SIMULATOR ..')
        # OgrePlatform.h:183:13: fatal error: 'Availability.h' file not found
        #   include "Availability.h"
        # Pour éviter cette erreur j'utilise gcc à la place de clang
        # il est donc nécessaire d'utiliser homebrew ou autre pour installer gcc.
        # % brew --prefix
        # /usr/local

        options = "-DOGRE_BUILD_RENDERSYSTEM_METAL=1 "
        options += "-DOGRE_INSTALL_SAMPLES=0 "
        options += "-DOGRE_BUILD_COMPONENT_CSHARP=0 "
        options += "-DOGRE_BUILD_COMPONENT_PYTHON=0 "
        options += "-DOGRE_BUILD_COMPONENT_BITES=1 "
        options += "-DOGRE_BUILD_PLUGIN_ASSIMP=0 "
        options += "-DOGRE_ENABLE_PRECOMPILED_HEADERS=0 "
        # compiler = "-DCMAKE_C_COMPILER=/usr/bin/gcc -DCMAKE_CXX_COMPILER=/usr/bin/g++"
        # compiler = "-DCMAKE_C_COMPILER=/usr/local/bin/gcc-10 -DCMAKE_CXX_COMPILER=/usr/local/bin/g++-10"
        compiler = ""
        ma_dep.call('cmake ' + compiler + ' ' + options + '..')
        ma_dep.call('cmake --build . --clean-first --config Release -j ' + str(ma_dep.thread_count()))
        # ma_dep.call('make --build -j ' + str(ma_dep.thread_count()))

        # Objective-C was disabled in PCH file but is currently enabled
        # https://github.com/cocos2d/cocos2d-x/issues/20607
        # https://forums.unrealengine.com/development-discussion/c-gameplay-programming/1730504-c-17-was-disabled-in-pch-file-but-is-currently-enabled
        # Le chemin de Ogre.dynlib :
        # […]/dependencies/Ogre/Codeblocks/lib/macosx/Ogre.framework/Versions/1.12.10/Ogre (le fichier est sans extension
        # Les raccourcis étant:
        # /Users/gandi/Working/Ma/dependencies/Ogre/Codeblocks/lib/macosx/Ogre.framework/Ogre
        # /Users/gandi/Working/Ma/dependencies/Ogre/Codeblocks/lib/macosx/RenderSystem_Metal.framework/RenderSystem_Metal
        # /Users/gandi/Working/Ma/dependencies/Ogre/Codeblocks/lib/macosx/OgreTerrain.framework/OgreTerrain
        # etc.

        # Il faut maintenant renommer les lib leur rajouter le .dynlib
        # /Users/gandi/Working/Ma/dependencies/Ogre/build/lib/macosx
        # lib_path = os.path.normpath(os.path.join(self.m_Dir, "build" + os.sep + "lib" + os.sep + "macosx"))
        #
        # def add_extension(name_framework):
        #     src = os.path.normpath(os.path.join(lib_path, name_framework + ".framework" + os.sep + name_framework))
        #     real_src = Path(src).resolve().absolute().as_posix()
        #     extensions = [".dylib", ".so"]
        #     for extension in extensions:
        #         if len(real_src) > 7 and real_src[-7:] != extension:
        #             if not os.path.exists(src + extension):
        #                 os.symlink(real_src, src + extension)
        #
        #             if not os.path.exists(real_src + extension):
        #                 os.symlink(real_src, real_src + extension)
        #
        # src_names = [f[:-len('.framework')] for f in os.listdir(lib_path) if os.path.isdir(os.path.join(lib_path, f)) and ('.framework' in f)]
        # for src_name in src_names:
        #     add_extension(name_framework=src_name)

    @staticmethod
    def build_windows():
        # https://stackoverflow.com/questions/54581717/adding-boost-to-cmake-project
        # https://sourceforge.net/projects/boost/files/boost-binaries/1.74.0/
        ma_dep.call('cmake ..')
        ma_dep.call('cmake --build . --clean-first --config Release -j ' + str(ma_dep.thread_count()))

    def vcpkg_build(self):
        os.chdir(self.m_vcpkg_dir)

        ma_dep.call('./bootstrap-vcpkg.sh -disableMetrics')
        ma_dep.call('./vcpkg integrate install --disable-metrics')

        use_vcpkg = True
        if use_vcpkg:
            ma_dep.call('./vcpkg install ogre --disable-metrics')
        else:
            ma_dep.call('./vcpkg install assimp --disable-metrics')
            ma_dep.call('./vcpkg install brotli --disable-metrics')
            ma_dep.call('./vcpkg install freeimage --disable-metrics')
            ma_dep.call('./vcpkg install freetype --disable-metrics')
            ma_dep.call('./vcpkg install imgui --disable-metrics')
            ma_dep.call('./vcpkg install jasper --disable-metrics')
            ma_dep.call('./vcpkg install jxrlib --disable-metrics')
            ma_dep.call('./vcpkg install kubazip --disable-metrics')
            ma_dep.call('./vcpkg install lcms --disable-metrics')
            ma_dep.call('./vcpkg install liblzma --disable-metrics')
            ma_dep.call('./vcpkg install libraw --disable-metrics')
            ma_dep.call('./vcpkg install libwepp --disable-metrics')
            ma_dep.call('./vcpkg install minizip --disable-metrics')
            ma_dep.call('./vcpkg install openexr --disable-metrics')
            ma_dep.call('./vcpkg install opengl --disable-metrics')
            ma_dep.call('./vcpkg install openjpeg --disable-metrics')
            ma_dep.call('./vcpkg install poly2tri --disable-metrics')
            ma_dep.call('./vcpkg install polyclipping --disable-metrics')
            ma_dep.call('./vcpkg install pugixml --disable-metrics')
            ma_dep.call('./vcpkg install rapidjson --disable-metrics')
            ma_dep.call('./vcpkg install sdl2 --disable-metrics')
            ma_dep.call('./vcpkg install stb --disable-metrics')
            ma_dep.call('./vcpkg install tiff --disable-metrics')
            ma_dep.call('./vcpkg install utcpp --disable-metrics')
            ma_dep.call('./vcpkg install zziplib --disable-metrics')
            os.chdir(self.m_Dir)
            build_path = os.path.join(self.m_Dir, "build")

            if os.path.exists(build_path):
                self.remove_folder(build_path)

            if not os.path.exists(build_path):
                os.mkdir(build_path)

            os.chdir(build_path)

            options = "-DOGRE_BUILD_RENDERSYSTEM_METAL=1 "
            options += "-DOGRE_BUILD_SAMPLE=0 "
            options += "-DOGRE_BUILD_COMPONENT_CSHARP=0 "
            options += "-DOGRE_BUILD_COMPONENT_BITES=0 "
            options += '-DCMAKE_TOOLCHAIN_FILE=' + '"' + os.path.join(self.m_vcpkg_dir,
                                                                      'scripts/buildsystems/vcpkg.cmake') + '" '

            # compiler = "-DCMAKE_C_COMPILER=/usr/bin/gcc -DCMAKE_CXX_COMPILER=/usr/bin/g++"
            compiler = ""

            ma_dep.call('cmake ' + compiler + ' ' + options + '..')
            ma_dep.call('cmake --build . --clean-first --config Release -j ' + str(ma_dep.thread_count()))

    def init_config(self):
        super().init_config()

        build_path = os.path.join(self.m_Dir, "build")

        bin_path = f"{build_path}/bin"
        lib_path = f"{build_path}/lib"
        build_include_path = f"{build_path}/include"
        include_path = os.path.join(self.m_Dir, "OgreMain", "include")
        include_components_path = os.path.join(self.m_Dir, "Components")
        input_plugin_cfg_path = f"{bin_path}/plugins.cfg"
        output_plugin_cfg_path = f"{bin_path}/ma_plugins.cfg"
        output_resources_cfg_path = f"{bin_path}/resources.cfg"

        with open(input_plugin_cfg_path) as input_file, open(output_plugin_cfg_path, "w") as output_file:
            for line in input_file:
                if "PluginFolder" in line:
                    path = line.split("=")[1]
                    relative_path = os.path.normpath(os.path.relpath(path, f"{build_path}/bin")).replace("\n", "")
                    output_file.write(f'PluginFolder={relative_path}\n')
                else:
                    output_file.write(line)

        group_name = "ogre"

        key_include_path = ma_dep.GroupKeyDefaultValue(group_name, "MA_OGRE_INCLUDE_PATH", include_path)

        key_build_include_path = ma_dep.GroupKeyDefaultValue(group_name,
                                                             "MA_OGRE_BUILD_INCLUDE_PATH",
                                                             build_include_path)

        key_include_components_path = ma_dep.GroupKeyDefaultValue(group_name,
                                                                  "MA_OGRE_COMPONENTS_INCLUDE_PATH",
                                                                  include_components_path)

        key_bin_path = ma_dep.GroupKeyDefaultValue(group_name, "MA_OGRE_BIN_PATH", bin_path)

        key_lib_path = ma_dep.GroupKeyDefaultValue(group_name,
                                                   "MA_OGRE_BUILD_LIB_PATH",
                                                   lib_path)

        key_plugin_cfg_path = ma_dep.GroupKeyDefaultValue(group_name,
                                                          "MA_OGRE_PLUGIN_CFG_PATH",
                                                          output_plugin_cfg_path)

        key_resources_cfg_path = ma_dep.GroupKeyDefaultValue(group_name,
                                                             "MA_OGRE_RESOURCES_CFG_PATH",
                                                             output_resources_cfg_path)

        self.m_Config.set_value(key_bin_path, bin_path)
        self.m_Config.set_value(key_lib_path, lib_path)
        self.m_Config.set_value(key_include_path, include_path)
        self.m_Config.set_value(key_build_include_path, build_include_path)
        self.m_Config.set_value(key_plugin_cfg_path, output_plugin_cfg_path)
        self.m_Config.set_value(key_resources_cfg_path, output_resources_cfg_path)
        self.m_Config.set_value(key_include_components_path, include_components_path)


if __name__ == '__main__':
    InstallOgre().go()
