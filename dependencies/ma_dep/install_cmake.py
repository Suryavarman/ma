#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#
# Permet d'avoir la dernière version de CMake. Certaines dépendances nécessite une version de CMake que le système n'a
# pas par défaut.

import os
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallCMake(ma_dep.Install):
    ms_UseLastTag = True  # si vrais le dernier tag du dépôt sera utilisé.
    ms_GitUrl = 'https://github.com/Kitware/CMake.git'
    ms_BackupArchive = f'https://codeload.github.com/Kitware/CMake/zip/refs/tags/%TAG%'

    def __init__(self, auto=False):
        super().__init__(name='CMake')
        self.auto = auto

        if self.m_Result.m_Done:
            current_cwd = os.getcwd()
            os.chdir(self.m_Dir)

            try:
                self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
            except KeyError:
                print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

            os.chdir(current_cwd)

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
{ma_dep.cl_warning("Attention")} le dossier {ma_dep.cl_path(self.m_Dir)} existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}) : 
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()

                if number == 1 or number == 3:
                    self.build(remove_build_dir=number == 3)
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_GitUrl,
                                 update_submodule=True,
                                 tag="",
                                 use_last_tag=self.ms_UseLastTag,
                                 archive_backup=self.ms_BackupArchive)

    def build(self, remove_build_dir=True):
        os.chdir(self.m_Dir)
        build_path = os.path.join(self.m_Dir, "build")

        if os.path.exists(build_path) and remove_build_dir:
            self.remove_folder(build_path)

        if not os.path.exists(build_path):
            os.mkdir(build_path)

        os.chdir(build_path)
        ma_dep.call('cmake -DCMAKE_BUILD_TYPE=Release ..')
        ma_dep.call(f'cmake --build . --clean-first --config Release -j {ma_dep.thread_count()}')

        self.setup_os_path()

        print("Installation de CMake terminée.")

    def get_cmake_executable_path(self):
        if sys.platform.startswith('linux') or sys.platform.startswith('darwin') or sys.platform.startswith('aix'):
            return os.path.join(self.m_Dir, "build", "bin", "cmake")
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            return os.path.join(self.m_Dir, "build", "bin", "cmake.exe")
        else:
            assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

    def setup_os_path(self):
        self.m_Environ.ms_Vars["PATH"] = self.get_cmake_executable_path() + ":" + self.m_Environ.ms_Vars["PATH"]


if __name__ == '__main__':
    InstallCMake().go()

