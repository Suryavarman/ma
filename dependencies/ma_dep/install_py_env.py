#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import os
import time
import shutil
import wget
# pour télécharger l'archive python peut ne pas être installer par défaut:
# pip install wget
# pip3 install wget
#
# On Mac OSX brew install wget
# git clone https://github.com/python/cpython
import glob
import re
import sys

import platform

using_distro = False
try:
    import distro

    using_distro = True
except ImportError:
    pass

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep

yes = ['y', 'o', 'oui', 'yes', 'si', 'ja']
# no = ['n', 'non', 'no', 'nein']

# https://codeload.github.com/python/cpython/zip/refs/tags/v3.12.1

python_versions = ["3.5.10",
                   "3.6.13",
                   "3.7.17",
                   "3.8.19",
                   "3.9.19",
                   "3.10.14",
                   "3.11.8",
                   "3.12.2"]
# Pour python 3.12 l'erreur suivante apparait :
# pip_path[0] IndexError : list index out of range
# https://github.com/python/cpython/issues/89230
# Il faut installer readline-devel qui devrait installer aussi curse.


def create_sym_link(target_path, sym_link_path):
    if os.path.exists(target_path) and not os.path.exists(sym_link_path):
        os.symlink(target_path, sym_link_path)


class InstallPyEnv:

    def __init__(self, venv_path="", auto=False):
        self.dependencies_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.auto = auto
        self.responses = ""
        for word in ["y", "o", "oui", "yes", "si", "ja"]:
            if not self.responses:
                self.responses += ma_dep.cl_key(word)
            else:
                self.responses += " / " + ma_dep.cl_key(word)

        if venv_path:
            self.venv_path = os.path.join(self.dependencies_dir, "venv")
        else:
            self.venv_path = venv_path

        """ Le fichier qui sauvegardera l'état de l'installation de l'environnement virtuel."""
        self.m_Result = ma_dep.Result(os.path.join(self.dependencies_dir, "ma_venv_result.txt"))

        """ Permet aux modules qui dépendent de cette instance de savoir si il doit se recompiler."""
        self.m_HasBeenRebuild = not self.m_Result.m_Done

    def __del__(self):
        self.m_Result.write()

    def go(self, call_by_go=False):
        if not call_by_go:
            print(ma_dep.cl_project_separator("Installation de l'environnement virtuel de", "Python3"))

        if self.m_Result.m_Done:
            print(f"\n{ma_dep.cl_info('Information')}: La précédente installation de cet environnement virtuel avait "
                  f"réussie.")
            if self.auto:
                return

        if os.path.exists(self.venv_path):
            dependencies_path = os.path.dirname(self.venv_path)
            paths = ""
            for directory in glob.glob(os.path.join(dependencies_path, "Python-*")):
                paths += directory + os.linesep

            # https://regex101.com/r/T61J5a/1
            regex = r"Python-(\d+.\d+.\d+)$"

            # matches = re.finditer(regex, paths, re.MULTILINE)
            py_versions = re.findall(regex, paths, re.MULTILINE)

            message = f"""
{ma_dep.cl_warning("Attention")} le dossier {ma_dep.cl_path(self.venv_path)} existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Le supprimer et repartir d'une version neuve de « {ma_dep.cl_key('Python')} ». {ma_dep.cl_info("Pensez à relancer l'IDE C++ et son projet")}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
"""
            if len(py_versions) > 0:
                message += (f"{ma_dep.cl_index('3)')} Utiliser les dossiers « {ma_dep.cl_key('Python')} » existant et recompiler "
                            f"« {ma_dep.cl_key('Python')} ».")

            print(message)

            if self.auto:
                nb = 1
            else:
                nb = input(f"Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}): ")
            number = 2

            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:

                if number == 1:
                    self.m_HasBeenRebuild = True
                    self.remove_env_folder()
                    self.install_python()
                    self.install_setuptools()

                if number == 2:
                    print(f"Vous avez choisi d'utiliser le répertoire {ma_dep.cl_path(self.venv_path)}"
                          f" comme environnement « {ma_dep.cl_key('Python')} ».")

                if number == 3 and len(py_versions) > 0:
                    self.m_HasBeenRebuild = True
                    print("Choisissez le dossier que vous souhaitez recompiler.")

                    i = 1
                    for py_version in py_versions:
                        print(ma_dep.cl_index(str(i) + ") ") + ma_dep.cl_key(py_version))
                        i += 1

                    if self.auto:
                        nb = len(py_versions)
                    else:
                        nb = input(f"Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}): ")

                    try:
                        number = int(nb)
                    except ValueError:
                        print("La chaîne de caractère saisie ne correspond pas à un numéro.")

                    if 0 < number <= len(py_versions):
                        python_build_version = py_versions[number - 1]
                        python_path = os.path.join(dependencies_path, "Python-" + python_build_version)
                        major, minor, micro = python_build_version.split(".")
                        self.rebuild(python_path=python_path,
                                     python_major=major,
                                     python_minor=minor)
                        self.install_setuptools()
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")
        else:
            self.install_python()
            self.install_setuptools()

        # print(f"Fin de la création de l'environnement « {ma_dep.cl_key('python3')} ».")
        self.m_Result.m_Done = True
        self.m_Result.write()

    def remove_env_folder(self):
        if os.path.exists(self.venv_path):
            if sys.platform != "win32":
                shutil.rmtree(self.venv_path)
            else:
                # ma_dep.call('rm -rf "' + self.venv_path + '"')
                wait_time = 5
                i = 0

                while i < wait_time and ma_dep.folder_has_files_lock(self.venv_path):
                    i += 1
                    time.sleep(1)

                ma_dep.call('rmdir /S /Q "' + self.venv_path + '"')

                if os.path.exists(self.venv_path):
                    i = 0
                    while i < wait_time and ma_dep.folder_has_files_lock(self.venv_path):
                        i += 1
                        time.sleep(1)

            assert not os.path.exists(self.venv_path), (f"Le dossier {ma_dep.cl_path(self.venv_path)} existe "
                                                        f"toujours il aurait dû être supprimé.")

        os.mkdir(self.venv_path)

    def install_setuptools(self):
        if sys.platform.startswith('linux'):
            self.install_setuptools_linux()
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.install_setuptools_windows()
        elif sys.platform.startswith('darwin'):
            self.install_setuptools_darwin()
        else:
            assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

    def install_setuptools_windows(self):
        pip_path = os.path.normpath(os.path.join(self.venv_path, "Scripts/pip"))
        # L'accès à pip est refusé. Mais de toute manière get-pip.py a été sûrement appelé auparavant
        # ma_dep.call('"' + pip_path + '" install --upgrade pip')

        ma_dep.call('"' + pip_path + '" install setuptools --upgrade')

    def install_setuptools_linux(self):
        ma_dep.call(self.venv_path + "/bin/pip install --upgrade pip")
        ma_dep.call(self.venv_path + "/bin/pip install setuptools --upgrade")

    def install_setuptools_darwin(self):
        ma_dep.call(self.venv_path + "/bin/pip install --upgrade pip")
        ma_dep.call(self.venv_path + "/bin/pip install setuptools --upgrade")

    def install_python(self):

        py_versions_str = ""
        index = 0
        for python_version in python_versions:
            if index < len(python_versions) - 1:
                py_versions_str += ma_dep.cl_index(python_version) + ", "
            else:
                py_versions_str += ma_dep.cl_index(python_version) + "."
            index += 1

        message = f"""

{ma_dep.cl_b(ma_dep.colour_key)}{ma_dep.cl_bold("Maçon de l'espace")}{ma_dep.cl_e()} nécessite d'utiliser un environnement « {ma_dep.cl_key('Python')} ». 
Vous pouvez choisir l'une de ces options:"
      {ma_dep.cl_index('-')} Installer un environnement virtuel à partir d'une version de « {ma_dep.cl_key('Python')} » déjà installer.
      {ma_dep.cl_index('-')} Installer une version de « {ma_dep.cl_key('Python')} » à partir des sources officielles. 
        Elle sera compiler et installer localement. Les versions disponibles sont: {py_versions_str}
        
      Que vous choisissiez d'installer un environnement virtuel ou de compiler une version à partir des sources, le 
      répertoire d'installation de l'environnement sera : "
      {ma_dep.cl_path(self.venv_path)}
"""
        print(message)

        if sys.platform == 'win32':
            print(f"Attention: Sous Windows 7 veuillez utiliser une version de « {ma_dep.cl_key('Python')} » inférieur à la "
                  f"{ma_dep.cl_index('3.9')}.")

        print(f"{ma_dep.cl_index('1)')} Créer un environnement virtuel.")
        i = 2
        for py_version in python_versions:
            index_str = f'{i})'
            if i < len(python_versions) + 1:
                print(f"{ma_dep.cl_index(index_str)} Compiler les sources de {ma_dep.cl_key('Python')} {ma_dep.cl_index(py_version)}.")
            else:
                print(f"{ma_dep.cl_index(index_str)} Compiler les sources de {ma_dep.cl_key('Python')} {ma_dep.cl_index(py_version)}. {ma_dep.cl_key('(défaut)')}")
            i += 1

        if self.auto:
            nb = len(python_versions) + 1
        else:
            nb = input(f"Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1)')}: ")

        try:
            number = int(nb)
        except ValueError:
            print("Invalid number")

        install_virtual_env = False
        python_build_version = ''

        if 0 < number <= len(python_versions) + 1:

            if number == 1:
                install_virtual_env = True
                print("Vous avez choisi d'installer un environment virtuel python.")
            else:
                install_virtual_env = False
                python_build_version = python_versions[number - 2]

                assert python_build_version != '', " python_build_version ne devrai pas être vide. Il est donc" \
                                                   f" impossible de définir la version de {ma_dep.cl_key('Python')}" \
                                                   " que vous souhaitez installer."

                print(f"Vous avez choisi de compiler {ma_dep.cl_key('Python')} {ma_dep.cl_index(python_build_version)} à partir des sources.")
        else:
            raise ValueError("Vous avez n'avez pas entré le bon numéro.")

        if install_virtual_env:
            python_path = ma_dep.python_version_choice()
            ma_dep.define_python_env(python_path, self.venv_path)
            ma_dep.make_python_env(python_path, self.venv_path)
        else:
            if os.path.exists(self.venv_path):
                shutil.rmtree(self.venv_path)

            assert not os.path.exists(self.venv_path)

            os.mkdir(self.venv_path)
            if sys.platform == 'linux' or sys.platform == 'darwin':
                url = "https://www.python.org/ftp/python/" + \
                      python_build_version + \
                      "/Python-" + \
                      python_build_version + \
                      ".tgz"

                download_path = os.path.dirname(self.venv_path) + "/Python-" + python_build_version + ".tgz"
            elif sys.platform == 'win32':
                extension = ".tgz"  # ".tar.xz"

                url = "https://www.python.org/ftp/python/" + \
                      python_build_version + \
                      "/Python-" + \
                      python_build_version + \
                      extension

                download_path = os.path.dirname(self.venv_path) + "/Python-" + python_build_version + extension
            else:
                assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

            extract_path = os.path.dirname(self.venv_path) + "/Python-" + python_build_version

            response = True

            if os.path.exists(extract_path):
                print(f"Le dossier d'extraction de l'archive existe déjà {ma_dep.cl_path(extract_path)}. "
                      f"Souhaitez vous le supprimer pour repartir de l'archive?")

                if self.auto:
                    response = "y"
                else:
                    response = input(f"Inscrivez votre réponse (Ex: {ma_dep.cl_index(self.responses)}): ") in yes

            if not os.path.exists(extract_path) or response:

                response = True
                if os.path.exists(download_path):
                    print(f"L'archive {ma_dep.cl_path(download_path)} existe déjà. "
                          f"Souhaitez vous la supprimer pour la télécharger à nouveau?")

                    if self.auto:
                        reponse = "y"
                    else:
                        response = input(f"Inscrivez votre réponse (Ex: {ma_dep.cl_index(self.responses)}): ") in yes

                if not os.path.exists(download_path) or response:
                    if os.path.exists(download_path):
                        os.remove(download_path)
                    assert not os.path.exists(download_path)

                    print(f"Téléchargement de l'archive {ma_dep.cl_path(os.path.basename(download_path))}.")

                    wget.download(url, download_path)
                    assert os.path.exists(download_path)
                    print("Téléchargement terminer.")

                if os.path.exists(extract_path):
                    shutil.rmtree(extract_path)
                assert not os.path.exists(extract_path)

                print(f"Extraction de l'archive {ma_dep.cl_path(download_path)} dans le dossier "
                      f"{ma_dep.cl_path(extract_path)}.")
                shutil.unpack_archive(download_path, os.path.dirname(extract_path))

            version_numbers = python_build_version.split('.')
            self.rebuild(python_path=extract_path,
                         python_major=version_numbers[0],
                         python_minor=version_numbers[1])

    def rebuild(self, python_path, python_major, python_minor):

        if sys.platform == 'linux':
            self.rebuild_linux(python_path, python_major, python_minor)
        elif sys.platform == 'win32':
            self.rebuild_windows(python_path)
        elif sys.platform == 'darwin':
            self.rebuild_darwin(python_path, python_major, python_minor)
        else:
            assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

    def rebuild_windows(self, python_path):
        # https://stackoverflow.com/questions/32127524/how-to-install-and-use-make-in-windows
        # https://cyrille.rossant.net/create-a-standalone-windows-installer-for-your-python-application/
        # https://winpython.github.io/
        # https://docs.python.org/2.3/inst/alt-install-windows.html
        # https://sebsauvage.net/python/mingw.html
        # https://docs.python.org/3.9/distutils/builtdist.html?highlight=alternate%20installation
        # https://packaging.python.org/tutorials/installing-packages/
        # modifier les options d'installation de python à partir de l'installateur
        # https://docs.python.org/3/using/windows.html
        # https://docs.python.org/3/using/windows.html
        # ex python-3.9.0.exe /quiet InstallAllUsers=1 PrependPath=1 Include_test=0
        # https://github.com/python/cpython/blob/main/PCbuild/readme.txt

        ma_dep.init_visual()

        current_dir = os.path.normpath(os.path.join(python_path, "PCbuild"))
        os.chdir(current_dir)
        ma_dep.call('clean.bat')

        print(f"Souhaitez vous effectuez la batterie de tests unitaires sur cette nouvelle installation de "
              f"{ma_dep.cl_key('Python')}?")
        print(f"Remarque: Cela peut être s'avérer très long. (bloque sur le {ma_dep.cl_key('test_gdb')})")

        if self.auto:
            response = "n"
        else:
            response = input(f"Inscrivez votre réponse (Ex: {self.responses}): ") in yes

        if ma_dep.is_32bits_os():
            out_dir = os.path.normpath(os.path.join(current_dir, "win32"))
            ma_dep.call('build.bat -t CleanAll')
            ma_dep.call('build.bat -c Release -p Win32')
            if response:
                ma_dep.call('rt.bat -q')

            ma_dep.make_python_env(out_dir, self.venv_path)

        elif ma_dep.is_64bits_os():
            out_dir = os.path.normpath(os.path.join(current_dir, "amd64"))
            ma_dep.call('build.bat -t CleanAll')
            ma_dep.call('build.bat -c Release -p x64')
            if response:
                ma_dep.call('rt.bat -q -x64')

            ma_dep.make_python_env(out_dir, self.venv_path)

        # ma_dep.copy(out_dir, self.venv_path)
        # activate_out_path = os.path.normpath(os.path.join(out_dir, "activate.bat"))
        # python_out_path = os.path.normpath(os.path.join(out_dir, "python"))
        # ma_dep.call('"' + activate_out_path + '"')

        # print("Téléchargement du script get-pip.py.")
        # url = "https://bootstrap.pypa.io/get-pip.py"
        # get_pip_path = os.path.normpath(os.path.join(out_dir, "get-pip.py"))
        # if sys.platform == 'darwin':
        #     print(r'Si vous obtenez une erreur de certification avec la commande wget.download, vous aurez peut être,'
        #           r'à exécuter cette commande avant tout «sudo /Applications/Python\ 3.x/Install\ Certificates.command»'
        #           r'x étant la version de python que vous utiliser pour installer les dépendances.')
        #
        # wget.download(url, get_pip_path)
        # assert os.path.exists(get_pip_path)
        # print("Téléchargement terminer.")
        # os.chdir(out_dir)
        # ma_dep.call('"' + python_out_path + '" "' + get_pip_path + '"')
        # ma_dep.call('"' + python_out_path + '" -m venv --system-site-packages --copies "' + self.venv_path + '"')

        # python_bat_path = os.path.normpath(os.path.join(python_path, "python"))
        # setup_path = os.path.normpath(os.path.join(python_path, "setup.py"))
        # os.chdir(python_bat_path)
        # create_sym_link(target_path=setup_path,
        #                 sym_link_path=out_dir)
        # ma_dep.call('"' + python_bat_path + '" setup.py install --prefix="' + self.venv_path + '"')
        # ma_dep.call('"' + python_bat_path + '" "' + setup_path + '" install --prefix="' + self.venv_path + '"')

    def rebuild_linux(self, python_path, python_major, python_minor):
        os.chdir(python_path)
        ma_dep.call('./configure --prefix="' + self.venv_path + '" --enable-shared LDFLAGS=-Wl,-rpath="' +
                    self.venv_path + '/lib" --enable-optimizations')

        ma_dep.call('make -j ' + str(ma_dep.thread_count()))
        ma_dep.call('make altinstall')

        python_version_short = python_major + '.' + python_minor

        abi_flag = "m" if os.path.exists(self.venv_path + "/bin/python" + python_version_short + "m") else ""

        create_sym_link(target_path=self.venv_path + "/bin/python" + python_version_short + abi_flag,
                        sym_link_path=self.venv_path + "/bin/python" + python_major)

        # Pour l'installation de wxWidgets via wxPython. Cette compilation utilise « python » et non python3 ou python2.
        # Certaines OS comme Ubuntu obligent à faire un choix en supprimant le raccourci python.
        create_sym_link(target_path=self.venv_path + "/bin/python" + python_version_short + abi_flag,
                        sym_link_path=self.venv_path + "/bin/python")

        create_sym_link(target_path=self.venv_path + "/bin/pip" + python_version_short,
                        sym_link_path=self.venv_path + "/bin/pip" + python_major)

        create_sym_link(target_path=self.venv_path + "/bin/pip" + python_version_short,
                        sym_link_path=self.venv_path + "/bin/pip")

        create_sym_link(target_path=self.venv_path + "/bin/python" + python_version_short + abi_flag + "-config",
                        sym_link_path=self.venv_path + "/bin/python" + python_version_short + "-config")

        create_sym_link(target_path=self.venv_path + "/bin/python" + python_version_short + abi_flag + "-config",
                        sym_link_path=self.venv_path + "/bin/python" + python_major + "-config")


        ma_dep.define_python_env(self.venv_path + "/bin/python" + python_version_short + abi_flag, self.venv_path)

        # build python
        # https://stackoverflow.com/questions/38772946/what-exactly-does-configure-enable-shared-do-during-python-altinstall

        # ./python: symbol lookup error: ./python: undefined symbol: __gcov_indirect_call
        # https://bugs.python.org/issue29712 -> bien supprimer le dossier d'installation avant de configuré et construire python.

        # ./configure --prefix=/home/gandi/Working/Ma/dependencies/python3.7 --enable-shared LDFLAGS=-Wl,-rpath=/home/gandi/Working/Ma/dependencies/python3.7/lib --enable-optimizations
        # make -j 16
        # make altinstall
        # ln -sf /home/gandi/Working/Ma/dependencies/python3.7/bin/python3.7m /home/gandi/Working/Ma/dependencies/python3.7/bin/python3
        # ln -sf /home/gandi/Working/Ma/dependencies/python3.7/bin/pip3.7 /home/gandi/Working/Ma/dependencies/python3.7/bin/pip3
        # ln -sf /home/gandi/Working/Ma/dependencies/python3.7/bin/pip3.7 /home/gandi/Working/Ma/dependencies/python3.7/bin/pip
        # ln -sf /home/gandi/Working/Ma/dependencies/python3.7/bin/python3.7m-config /home/gandi/Working/Ma/dependencies/python3.7/bin/python3.7-config
        # ln -sf /home/gandi/Working/Ma/dependencies/python3.7/bin/python3.7m-config /home/gandi/Working/Ma/dependencies/python3.7/bin/python3-config

    def rebuild_darwin(self, python_path, python_major, python_minor):
        os.chdir(python_path)
        ma_dep.call('./configure --prefix="' + self.venv_path + '" --enable-shared LDFLAGS=-Wl,-rpath="' +
                    self.venv_path + '/lib" --enable-optimizations --with-openssl-rpath=auto '
                                     '--with-ssl-default-suites=openssl')

        ma_dep.call('make -j ' + str(ma_dep.thread_count()))
        ma_dep.call('make altinstall')

        python_version_short = python_major + '.' + python_minor

        abi_flag = "m" if os.path.exists(self.venv_path + "/bin/python" + python_version_short + "m") else ""

        create_sym_link(target_path=self.venv_path + "/bin/python" + python_version_short + abi_flag,
                        sym_link_path=self.venv_path + "/bin/python" + python_major)

        create_sym_link(target_path=self.venv_path + "/bin/pip" + python_version_short,
                        sym_link_path=self.venv_path + "/bin/pip" + python_major)

        create_sym_link(target_path=self.venv_path + "/bin/pip" + python_version_short,
                        sym_link_path=self.venv_path + "/bin/pip")

        create_sym_link(target_path=self.venv_path + "/bin/python" + python_version_short + abi_flag + "-config",
                        sym_link_path=self.venv_path + "/bin/python" + python_version_short + "-config")

        create_sym_link(target_path=self.venv_path + "/bin/python" + python_version_short + abi_flag + "-config",
                        sym_link_path=self.venv_path + "/bin/python" + python_major + "-config")

        ma_dep.define_python_env(self.venv_path + "/bin/python" + python_version_short + abi_flag, self.venv_path)


if __name__ == '__main__':
    import argparse

    arg_parser = argparse.ArgumentParser(description='Définition des chemins pour compiler Ma.')
    arg_parser.add_argument('--venv_path', help=f"Le chemin de l'environnement virtuel à créé. Par défaut "
                                                f"le chemin est en locale {ma_dep.cl_path('venv')}.")

    args = arg_parser.parse_args()

    installer = InstallPyEnv(venv_path=os.path.abspath(args.venv_path))
    installer.install_python()
