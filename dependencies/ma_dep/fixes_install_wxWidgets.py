#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import glob
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class FixWxWidgets:

    @staticmethod
    def fix(wx_python_path):
        # Si vous avez cette erreur lors de l'import du module python wx :
        # ImportError: /home/gandi/Working/Ma/dependencies/venv/lib/python3.5/site-packages/wx/_core.cpython-35m-x86_64-linux-gnu.so: symbol _ZN19wxUIActionSimulator10MouseClickEi, version WXU_3.1 not defined in file libwx_gtk3u_core-3.1.so.3 with link time reference
        #
        # Ou cette erreur:
        # Lors de la compilation de wxPython:
        # [118/902] Compiling sip/cpp/sip_corewxClientData.cpp
        # ../../../../sip/cpp/sip_corewxUIActionSimulator.cpp: Dans la fonction « PyObject* meth_wxUIActionSimulator_MouseMove(PyObject*, PyObject*, #PyObject*) »:
        # ../../../../sip/cpp/sip_corewxUIActionSimulator.cpp:31:12: erreur: « ::wxUIActionSimulator » n'a pas été déclaré; vouliez-vous employer #« sipName_wxUIActionSimulator »?
        #   31 |          ::wxUIActionSimulator *sipCpp;
        #      |            ^~~~~~~~~~~~~~~~~~~
        # /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets
        # setup.h.in
        # /*
        #    Use XTest extension to implement wxUIActionSimulator?
        #
        #    Default is 1, it is set to 0 if the necessary headers/libraries are not
        #    found by configure.
        #
        #    Recommended setting: 1, wxUIActionSimulator won't work in wxGTK3 without it.
        #  */
        # #define wxUSE_XTEST 0
        # see wxPython/ext/wxWidgets/build/cmake/init.cmake and search UIACTIONSIMULATOR
        setup_folder = os.path.join(wx_python_path, "ext", "wxWidgets")

        setup_in_file_path = ma_dep.get_path(os.path.join(setup_folder, "setup.h.in"))
        ma_dep.replace_str(setup_in_file_path, "#define wxUSE_XTEST 0", "#define wxUSE_XTEST 1")

        setup_vms_file_path = ma_dep.get_path(os.path.join(setup_folder, "setup.h_vms"))

        # https://docs.wxwidgets.org/stable/classwx_web_view.html
        # mageia installer lib64webkitgtk
        ma_dep.replace_str(setup_vms_file_path, "#define wxUSE_WEBVIEW 0", "#define wxUSE_WEBVIEW 1")
        ma_dep.replace_str(setup_vms_file_path, "#define wxUSE_WEBVIEW_WEBKIT2 0", "#define wxUSE_WEBVIEW_WEBKIT2 1")

        if sys.platform.startswith('linux'):
            # checking for XTST... configure: WARNING: XTest not found, disabling wxUIActionSimulator
            # you have to install XTest
            # Mageia 6
            # lib64xtst-devel

            # checking for WEBKIT... configure: WARNING: webkit2gtk not found, falling back to webkitgtk
            # On mageia6 cmake cannot find it!

            # checking for python... /home/gandi/Working/Ma/dependencies/venv/bin/python3
            # checking for WEBKIT... configure: WARNING: webkit2gtk not found, falling back to webkitgtk
            # checking for WEBKIT... configure: WARNING: webkitgtk not found.
            # configure: WARNING: WebKit not available, disabling wxWebView

            webview2gtk_paths = []
            webview2gtk_directories = ['/usr/lib/', '/usr/lib64', '/usr/lib/x86_64-linux-gnu']

            # trouver les versions de webview2gtk installer
            for path in webview2gtk_directories:
                print(path)
                # /usr/lib64/libwebkit2gtk-4.0.so.37.37.6
                glob_results = glob.glob(path + '/libwebkit2gtk*')
                if len(glob_results):
                    for glob_result in glob_results:
                        webview2gtk_paths.append((path, glob_result))

#             pkg_config_directories = ['/usr/lib64/pkgconfig/']
#             # /usr/lib64/pkgconfig/webkit2gtk-4.1.pc
#             webkit_pc_pattern = 'webkit2gtk-4.*.pc'
#             webkit_pc_path = ''
#             for path in pkg_config_directories:
#                 glob_results = glob.glob(os.path.join(path, webkit_pc_pattern))
#                 if len(glob_results):
#                     for glob_result in glob_results:
#                         if not webkit_pc_path:
#                             webkit_pc_path = glob_result
#                         else:
#                             last_minor = int(webkit_pc_path.split('.')[-2])
#                             current_minor = int(glob_result.split('.')[-2])
#                             if current_minor > last_minor:
#                                 webkit_pc_path = glob_result
#
#             assert webkit_pc_path,  f"""
# Aucun {webkit_pc_pattern} dans pkgconfig n'a été trouvé.
# La version de webkit2gtk par défaut est la 4.0
# Veuillez installer :
# Sur ubuntu: libwebkit2gtk-4.*-dev
# Sur mageia: lib64webkit2gtk4.*-devel
# """

            # configure_path = os.path.join(wx_python_path, 'ext', 'wxWidgets', 'configure')
            #
            # assert os.path.exists(configure_path)

            # ma_dep.replace_str(configure_path, old_code, new_code)


            # # urpmi lib64webkit2gtk4.0-devel
            # # On prend la première version et ont lui colle à côté un raccourci
            # sym_link_name = "libwebkit2gtk-4.0.so"
            #
            # # Truc à essayer:
            # # détecter la version du libwebkit2gtk et renommer dans configure.in webkit2gtk-4.0 en webkit2gtk-4.1
            # # sinon installer la version 4.0 et non la version 4.1 de webkit...
            #
            # # sur Mageia7 il faut installer lib64webkit2-devel
            # assert len(webview2gtk_paths), "aucune version de " + sym_link_name + "n'a été trouvée."
            #
            # # Ça parrait un peu fort en café de modifier ainsi une librairie système...
            # if len(webview2gtk_paths):
            #     tuple_dir_path = webview2gtk_paths[0]
            #     sym_link_path = tuple_dir_path[0] + '/' + sym_link_name
            #     if not (os.path.exists(sym_link_path) or os.path.islink(sym_link_path)):
            #
            #         assert os.getuid() == 0, \
            #             'Pour créer le raccourci, il faut être en mode administrateur. Veuillez effectuer' \
            #             ' la commande suivante : ln -s "' + tuple_dir_path[1] + '" "' + sym_link_path + '"'
            #
            #         os.symlink(tuple_dir_path[1], sym_link_path)
            #         print("sym link créé: " + sym_link_path + " -> " + tuple_dir_path[1])

            # Bogue dans le dossier temporaire de waf import requests
            # File "/tmp/pip-req-build-_4jvize2/build.py", line 615, in getTool
            # import requests
            # ModuleNotFoundError: No module named 'requests'
            ma_config = ma_dep.MaConfig()
            py_env_spp_path = ma_config.get_value(ma_dep.MaConfig.ms_Key_PythonEnvSPPath)

            assert os.path.exists(py_env_spp_path), f"Le dossier site-package de l'environnement python n'existe pas: " \
                                                    f"{py_env_spp_path}"

            build_file_path = ma_dep.get_path(wx_python_path + os.sep + "build.py")
            old_code = "import requests"

            # attention à l'indentation
            new_code = f"""         
            # exemple: [...]/Ma/dependencies/venv_installation/lib/python3.10/site-packages/
            sys.path.append("{py_env_spp_path}")
            import requests
"""
            ma_dep.replace_str(build_file_path, old_code, new_code)


if __name__ == '__main__':
    FixWxWidgets().fix(os.path.join(ma_dep.dependency_dir, "wxPython"))
