#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import argparse
import configparser
import os

if __name__ == '__main__':
    dependency_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/dependencies.cfg'

    config = configparser.ConfigParser()
    config.read(dependency_path)

    parser = argparse.ArgumentParser(description="Permet d'accéder au fichier configuration dependencies.cfg via ligne"
                                                 " commande.")

    parser.add_argument('--key', help="La clef de la valeur.")
    parser.add_argument('--group', help="Le groupe de la pair clef/valeur.")

    args = parser.parse_args()

    config.has_option(args.group, args.key) if print(config[args.group][args.key]) else print("")
