#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#
# Une erreur possible sous Windows est la non présence de des outils pour générer les messages de wxWidgets.
# Exemple: WARNING: msgfmt and/or make commands not found, message catalogs not
#          rebuilt. Please install gettext and associated tools.
# Pour y remédier il faut installer gettext.
# Pour se faire:
# 1- Il faut installer https://mlocati.github.io/articles/gettext-iconv-windows.html choisir la version statique.
# 2- Mettre à jour les chemin d'environnement. Control Panel > System > Advanced > Environment Variables
# 3- Ajouter le chemin C:\Program Files\gettext-iconv\bin à la variable Path
# https://stackoverflow.com/questions/38806553/how-to-install-gnu-gettext-0-15-on-windows-so-i-can-produce-po-mo-files-in/45574890#45574890
#
# Il faudra vous assuré d'utiliser le même compilateur que celui qui aura été utilisé pour compiler python.
# Sous windows 10 si vous utilisez visual 2015, 2017 ou 2019 (ou 2022?) vous pouvez activer celui que vous désirez u
# tiliser en exécutant le vcvarsx86_amd64.bat
# Ex: C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsx86_amd64
# Note: 2015 (msvc 14.0)
#
# Les erreurs possibles:
#
# Bogue dans le dossier temporaire de waf import requests
# File "/tmp/pip-req-build-_4jvize2/build.py", line 615, in getTool
# import requests
# ModuleNotFoundError: No module named 'requests'
# Solution:
# Normalement ce bogue est contourné dans le script fixes_install_wxWidgets.py
# 1/ Essayer d'installer dans l'environnement système le module requests:
# sur Mageia: ’urpmi python3-requests’
# Cela peut ne pas être suffisant/la solution. Vous devrez alors installer vous même doxygen et veiller à la présence de
# la variable d'environnement DOXYGEN
# « Unable to download https://wxpython.org/Phoenix/tools/doxygen-1.8.8-linux.bz2
# Set DOXYGEN in the environment to use a local build of doxygen instead »
#
# Bogue dans
#       File ".../Ma/dependencies/wxPython/build.py", line 615, in getTool
#         import requests
#     ModuleNotFoundError: No module named 'requests'
#
# Solution:
# L'environnement avec lequel vous appelez le script ne possède pas le module requests
#
# Bogue Compiling sip/cpp/sip_corewxRearrangeCtrl.cpp
#       durant l'étape GIMPLE: vrp
#       Dans le fichier inclus depuis /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/toplevel.h:361,
#                        depuis /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/wx.h:41,
#                        depuis ../../../../wx/include/wxPython/wxpy_api.h:41,
#                        depuis ../../../../sip/cpp/sipAPI_core.h:20845,
#                        depuis ../../../../sip/cpp/sip_corewxRefCounter.cpp:10:
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/gtk/toplevel.h: Dans la fonction membre « virtual void wxTopLevelWindowGTK::SetLabel(const wxString&) »:
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/gtk/toplevel.h:74:18: erreur interne du compilateur: Erreur de segmentation
#          74 |     virtual void SetLabel(const wxString& label) wxOVERRIDE { SetTitle( label ); }
#             |                  ^~~~~~~~
#       Veuillez soumettre un rapport d’anomalies complet,
#       avec la sortie du préprocesseur si nécessaire.
#       Voir <https://bugs.mageia.org/> pour les instructions.
#
#       Waf: Leaving directory `/tmp/pip-req-build-gbnd7he5/build/waf/3.1/gtk3'
#       Build failed
#        -> task in '_core' failed with exit status 1 (run with -v to display more information)
#       Command '"/home/gandi/Working/Ma/dependencies/venv/bin/python3" /tmp/pip-req-build-gbnd7he5/bin/waf-2.0.19 --wx_config=/tmp/pip-req-build-gbnd7he5/build/wxbld/gtk3/wx-config --gtk3 --python="/home/gandi/Working/Ma/dependencies/venv/bin/python3" --out=build/waf/3.1/gtk3 configure build ' failed with exit code 1.
#       Finished command: build_py (1m20.430s)
#       Finished command: build (3m22.309s)
#       Command '"/home/gandi/Working/Ma/dependencies/venv/bin/python3" -u build.py build' failed with exit code 1.
#       ----------------------------------------
#       ERROR: Failed building wheel for wxPython
#       Running setup.py clean for wxPython
#     Failed to build wxPython
# Solution:
# supprimer le dossier wxPython et relancer install_wxpython.py.
#
# Bogue compilation:
# Dans le fichier inclus depuis /usr/include/c++/10/string:55,
#                      depuis ../../../../ext/wxWidgets/include/wx/stringimpl.h:66,
#                      depuis ../../../../ext/wxWidgets/include/wx/unichar.h:15,
#                      depuis ../../../../ext/wxWidgets/include/wx/strvararg.h:19,
#                      depuis ../../../../ext/wxWidgets/include/wx/string.h:37,
#                      depuis ../../../../ext/wxWidgets/include/wx/memory.h:15,
#                      depuis ../../../../ext/wxWidgets/include/wx/object.h:19,
#                      depuis ../../../../ext/wxWidgets/include/wx/wx.h:15,
#                      depuis ../../../../wx/include/wxPython/wxpy_api.h:41,
#                      depuis ../../../../sip/cpp/sipAPI_core.h:20845,
#                      depuis ../../../../sip/cpp/sip_corewxSVGBitmapEmbedHandler.cpp:10:
#     /usr/include/c++/10/bits/basic_string.h: Dans l'instanciation de « std::__cxx11::basic_string<_CharT, _Traits, _Alloc>::basic_string(std::__cxx11::basic_string<_CharT, _Traits, _Alloc>&&) [with _CharT = char16_t; _Traits = std::char_traits<char16_t>; _Alloc = std::allocator<char16_t>] » :
#     /usr/include/c++/10/bits/basic_string.h:6917:49:   requis depuis ici
#     /usr/include/c++/10/bits/basic_string.h:558:28: erreur interne du compilateur: Erreur de segmentation
#       558 |          _S_local_capacity + 1);
#           |          ~~~~~~~~~~~~~~~~~~^~~
# Solution:
# supprimer le dossier wxPython et relancer install_wxpython.py.
# Bogue:
#  ../../../../sip/cpp/sip_corewxChoice.cpp: Dans la fonction membre « void sipwxChoice::sipProtectVirt_DoSetSize(bool, int, int, int, int, int) »:
#       ../../../../sip/cpp/sip_corewxChoice.cpp:3299:2: erreur interne du compilateur: Erreur de segmentation
#        3299 | };
#             |  ^
#       Veuillez soumettre un rapport d’anomalies complet,
#       avec la sortie du préprocesseur si nécessaire.
#       Voir <https://bugs.mageia.org/> pour les instructions.
#
#       Dans le fichier inclus depuis /home/gandi/Working/Ma/dependencies/venv/include/python3.10/unicodeobject.h:1046,
#                        depuis /home/gandi/Working/Ma/dependencies/venv/include/python3.10/Python.h:83,
#                        depuis ../../../../sip/siplib/sip.h:32,
#                        depuis ../../../../sip/cpp/sipAPI_core.h:13,
#                        depuis ../../../../sip/cpp/sip_corewxString.cpp:10:
#       ../../../../sip/cpp/sip_corewxString.cpp: Dans la fonction « int convertTo_wxString(PyObject*, void**, int*, PyObject*) »:
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:451:75: attention: « Py_ssize_t _PyUnicode_get_wstr_length(PyObject*) » est obsolète [-Wdeprecated-declarations]
#         451 | #define PyUnicode_WSTR_LENGTH(op) _PyUnicode_get_wstr_length((PyObject*)op)
#             |                                                                           ^
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:261:7: note: dans l'expansion de la macro « PyUnicode_WSTR_LENGTH »
#         261 |       PyUnicode_WSTR_LENGTH(op) :                    \
#             |       ^~~~~~~~~~~~~~~~~~~~~
#       ../../../../sip/cpp/sip_corewxString.cpp:74:22: note: dans l'expansion de la macro « PyUnicode_GET_SIZE »
#          74 |         size_t len = PyUnicode_GET_SIZE(uni);
#             |                      ^~~~~~~~~~~~~~~~~~
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:446:26: note: déclaré ici
#         446 | static inline Py_ssize_t _PyUnicode_get_wstr_length(PyObject *op) {
#             |                          ^~~~~~~~~~~~~~~~~~~~~~~~~~
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:262:52: attention: « Py_UNICODE* PyUnicode_AsUnicode(PyObject*) » est obsolète [-Wdeprecated-declarations]
#         262 |       ((void)PyUnicode_AsUnicode(_PyObject_CAST(op)),\
#             |                                                    ^
#       ../../../../sip/cpp/sip_corewxString.cpp:74:22: note: dans l'expansion de la macro « PyUnicode_GET_SIZE »
#          74 |         size_t len = PyUnicode_GET_SIZE(uni);
#             |                      ^~~~~~~~~~~~~~~~~~
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:580:45: note: déclaré ici
#         580 | Py_DEPRECATED(3.3) PyAPI_FUNC(Py_UNICODE *) PyUnicode_AsUnicode(
#             |                                             ^~~~~~~~~~~~~~~~~~~
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:451:75: attention: « Py_ssize_t _PyUnicode_get_wstr_length(PyObject*) » est obsolète [-Wdeprecated-declarations]
#         451 | #define PyUnicode_WSTR_LENGTH(op) _PyUnicode_get_wstr_length((PyObject*)op)
#             |                                                                           ^
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:264:8: note: dans l'expansion de la macro « PyUnicode_WSTR_LENGTH »
#         264 |        PyUnicode_WSTR_LENGTH(op)))
#             |        ^~~~~~~~~~~~~~~~~~~~~
#       ../../../../sip/cpp/sip_corewxString.cpp:74:22: note: dans l'expansion de la macro « PyUnicode_GET_SIZE »
#          74 |         size_t len = PyUnicode_GET_SIZE(uni);
#             |                      ^~~~~~~~~~~~~~~~~~
#       /home/gandi/Working/Ma/dependencies/venv/include/python3.10/cpython/unicodeobject.h:446:26: note: déclaré ici
#         446 | static inline Py_ssize_t _PyUnicode_get_wstr_length(PyObject *op) {
#             |                          ^~~~~~~~~~~~~~~~~~~~~~~~~~
#
#       ../../../../sip/cpp/sip_corewxIcon.cpp: Dans la fonction « PyObject* meth_wxIcon_SetDepth(PyObject*, PyObject*, PyObject*) »:
#       ../../../../sip/cpp/sip_corewxIcon.cpp:347:35: attention: « virtual void wxBitmap::SetDepth(int) » est obsolète [-Wdeprecated-declarations]
#         347 |             sipCpp->SetDepth(depth);
#             |                                   ^
#       Dans le fichier inclus depuis /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/wx.h:14,
#                        depuis ../../../../wx/include/wxPython/wxpy_api.h:41,
#                        depuis ../../../../sip/cpp/sipAPI_core.h:20845,
#                        depuis ../../../../sip/cpp/sip_corewxIcon.cpp:10:
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/gtk/bitmap.h:127:23: note: déclaré ici
#         127 |     wxDEPRECATED(void SetDepth( int depth ) wxOVERRIDE);
#             |                       ^~~~~~~~
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/defs.h:618:43: note: dans la définition de la macro « wxDEPRECATED »
#         618 | #define wxDEPRECATED(x) wxDEPRECATED_DECL x
#             |                                           ^
#       ../../../../sip/cpp/sip_corewxIcon.cpp: Dans la fonction « PyObject* meth_wxIcon_SetHeight(PyObject*, PyObject*, PyObject*) »:
#       ../../../../sip/cpp/sip_corewxIcon.cpp:386:37: attention: « virtual void wxBitmap::SetHeight(int) » est obsolète [-Wdeprecated-declarations]
#         386 |             sipCpp->SetHeight(height);
#             |                                     ^
#       Dans le fichier inclus depuis /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/wx.h:14,
#                        depuis ../../../../wx/include/wxPython/wxpy_api.h:41,
#                        depuis ../../../../sip/cpp/sipAPI_core.h:20845,
#                        depuis ../../../../sip/cpp/sip_corewxIcon.cpp:10:
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/gtk/bitmap.h:125:23: note: déclaré ici
#         125 |     wxDEPRECATED(void SetHeight( int height ) wxOVERRIDE);
#             |                       ^~~~~~~~~
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/defs.h:618:43: note: dans la définition de la macro « wxDEPRECATED »
#         618 | #define wxDEPRECATED(x) wxDEPRECATED_DECL x
#             |                                           ^
#       ../../../../sip/cpp/sip_corewxIcon.cpp: Dans la fonction « PyObject* meth_wxIcon_SetWidth(PyObject*, PyObject*, PyObject*) »:
#       ../../../../sip/cpp/sip_corewxIcon.cpp:425:35: attention: « virtual void wxBitmap::SetWidth(int) » est obsolète [-Wdeprecated-declarations]
#         425 |             sipCpp->SetWidth(width);
#             |                                   ^
#       Dans le fichier inclus depuis /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/wx.h:14,
#                        depuis ../../../../wx/include/wxPython/wxpy_api.h:41,
#                        depuis ../../../../sip/cpp/sipAPI_core.h:20845,
#                        depuis ../../../../sip/cpp/sip_corewxIcon.cpp:10:
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/gtk/bitmap.h:126:23: note: déclaré ici
#         126 |     wxDEPRECATED(void SetWidth( int width ) wxOVERRIDE);
#             |                       ^~~~~~~~
#       /home/gandi/Working/Ma/dependencies/wxPython/ext/wxWidgets/include/wx/defs.h:618:43: note: dans la définition de la macro « wxDEPRECATED »
#         618 | #define wxDEPRECATED(x) wxDEPRECATED_DECL x
#             |                                           ^
#
#       Waf: Leaving directory `/tmp/pip-req-build-e20qc8bb/build/waf/3.1/gtk3'
#       Build failed
#        -> task in '_core' failed with exit status 1 (run with -v to display more information)
#       Command '"/home/gandi/Working/Ma/dependencies/venv/bin/python3.10" /tmp/pip-req-build-e20qc8bb/bin/waf-2.0.19 --wx_config=/tmp/pip-req-build-e20qc8bb/build/wxbld/gtk3/wx-config --gtk3 --python="/home/gandi/Working/Ma/dependencies/venv/bin/python3.10" --out=build/waf/3.1/gtk3 configure build ' failed with exit code 1.
#       Finished command: build_py (0m38.842s)
#       Finished command: build (2m34.625s)
#       Command '"/home/gandi/Working/Ma/dependencies/venv/bin/python3.10" -u build.py build' failed with exit code 1.
#       ----------------------------------------
#       ERROR: Failed building wheel for wxPython
# Solution:
# J'ai pas. Mais wxPython s'installe malgré cette erreur…

import os
import glob
import wget
import shutil
import sys
from enum import Enum

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallationFrom(Enum):
    undefined = 0
    git = 1
    own_archive = 2
    default_archive = 3


class InstallwxPython(ma_dep.Install):
    # Ne compile pas car e paterne «/home/.../Ma/dependencies/wxPython/build/wxbld/gtk3/lib/wx/include/gtk3*/wx/setup.h»
    # ne retourne aucun résultat.
    # No module named 'attrdict'
    ms_UseLastTag = False  # si vrais ms_Tag sera ignoré, le dernier tag du dépôt sera utilisé.

    ms_Tag = "wxPython-4.2.1"
    ####################################################################################################################
    # ATTENTION!!!
    # >>>>> NE PAS OUBLIER DE METTRE À JOUR dependencies.cfg <<<<<
    ####################################################################################################################

    ms_GitUrl = 'https://github.com/wxWidgets/Phoenix.git'
    ms_ArchiveUrl = f"https://codeload.github.com/wxWidgets/Phoenix/zip/refs/tags/wxPython-{ms_Tag}"

    # Le nom de l'archive qui sera télécharger par défaut si aucune archive n'a été trouvée dans le dossier des
    # dépendances.
    ms_ArchiveName = f"Phoenix-wxPython-{ms_Tag}.zip"
    ms_ArchiveGlobPattern = "Phoenix-wxPython-*.zip"

    ms_SnapShotUrl = "https://wxpython.org/Phoenix/snapshot-builds/"
    ms_SnapShotArchiveGlobPattern = "wxPython-*.tar.gz"
    ms_Debug = True

    def __init__(self, installation_from=InstallationFrom.undefined, auto=False, py_env_dep: ma_dep.InstallPyEnv = None):
        super().__init__(name='wxPython')

        self.m_PythonSPPath = os.path.normpath(self.m_Config.get_value(ma_dep.MaConfig.ms_Key_PythonEnvSPPath))
        self.m_InstallationFrom = installation_from
        self.auto = auto

        self.m_InstallPyEnv: ma_dep.InstallPyEnv = py_env_dep

        if sys.platform.startswith('linux') or sys.platform.startswith('darwin') or sys.platform.startswith('aix'):
            bin_folder_name = "bin"
            # python_app_name = "python3"
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            bin_folder_name = "Scripts"
            # python_app_name = "python"
        else:
            assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

        self.m_Environ.ms_Vars["PATH"] = os.path.join(self.m_PythonPath, bin_folder_name) + ":" + self.m_Environ.ms_Vars["PATH"]

        if py_env_dep is not None:
            self.m_Result.m_Done = self.m_Result.m_Done and not py_env_dep.m_HasBeenRebuild

        if self.m_Result.m_Done:
            if InstallwxPython.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")
                    
                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallwxPython.ms_Tag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        dir_exist = os.path.exists(self.m_Dir)

        number = 3

        if dir_exist:
            message = f"""
{ma_dep.cl_warning("Attention")} le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez vous: 
{ma_dep.cl_index('1)')} Effacer le dossier et repartir d'une nouvelle solution. 
  {ma_dep.cl_warning(f"—> Choisissez cette option, si l'environnement {ma_dep.cl_bold('Python')} a été modifier ou mis à jour ou pour mettre à jour {ma_dep.cl_bold(self.m_Name)}")}.         
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.   
{ma_dep.cl_index('3)')} Recompiler la solution.
{ma_dep.cl_index('4)')} Réinstaller les modules {ma_dep.cl_bold("Python")} et Recompiler la solution.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            try:
                number = int(nb)
            except ValueError:
                print("Numéro invalide")

        if not dir_exist or number == 1:
            self.remove_install_folder()
            self.install_py_modules()

            if not self.m_InstallationFrom == InstallationFrom.git and \
                    (self.m_InstallationFrom == InstallationFrom.default_archive or
                     self.m_InstallationFrom == InstallationFrom.own_archive or
                     self.archive_or_git()):
                self.install_from_archive()
            else:
                self.install_from_git()

        if number == 4:
            self.install_py_modules()

        if number != 2:
            self.build()

        self.go_end()

    """Retourne vrais si l'utilisateur souhaites installer wxPython à partir d'une archive."""

    def archive_or_git(self):
        message = f"""
Installer wxPython à partir:   
1) du dépôt git le tag étant celui-ci: {ma_dep.cl_key(self.ms_Tag)}     
2) d'une archive (vous aurez le choix d'utiliser votre archive ou une par défaut)    

Inscrivez le numéro correspondant à votre choix (Ex: 1):
"""

        if self.auto:
            nb = 1
        else:
            nb = input(message)

        number = 1
        try:
            number = int(nb)
        except ValueError:
            print("La chaîne de caractères saisie ne correspond pas à un numéro.")

        if 0 < number <= 2:

            if number == 1:
                print(f"Vous avez choisi d'installer {ma_dep.cl_key('wxPython')} à partir du dépôt git.")
                return False

            if number == 2:
                print(f"Vous avez choisi d'installer {ma_dep.cl_key('wxPython')} à partir d'une archive.")
                return True
        else:
            raise ValueError("Vous avez n'avez pas entré le bon numéro.")

    def install_py_modules(self):
        # wxPython 4.2.1 build failed with cython 3.0.0 and sip 6.7.10
        # https://github.com/wxWidgets/Phoenix/issues/2439
        modules = 'attrdict3 numpy pillow six appdirs setuptools wheel twine sphinx requests pytest ' \
                  'pytest-xdist pytest-forked pytest-timeout pathlib2 sip==6.7.9 wget cython==0.29.36 doc2dash ' \
                  'beautifulsoup4 requests[security]'

        if sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            modules += ' python-gettext'  # Finalement, ça ne sert à rien.

        ma_dep.Install.install_py_modules(self, modules)

    def install_from_archive(self):

        if self.m_InstallationFrom != InstallationFrom.default_archive and self.m_InstallationFrom != InstallationFrom.own_archive:
            message = f"""
Avant de continuer vous avez la possibilité de télécharger vous même l'archive python que vous désirez et de la placer
dans le dossier « {ma_dep.cl_path(self.m_DependenciesDir)} ». 
L'archive doit être au format suivant « {ma_dep.cl_path(self.ms_SnapShotArchiveGlobPattern)} » et peut être télécharger
 via ce lien : « {ma_dep.cl_url(self.ms_SnapShotUrl)} ».

Une fois fait vous pouvez passer à la suite en pressant une touche du clavier. Si aucune archive n'est trouvée l'archive
 suivante sera téléchargée : 
 « {ma_dep.cl_url(self.ms_ArchiveUrl)} »
Si il y a plusieurs archives, un choix vous sera proposé.

Presser une touche pour continuer.
"""
            if not self.auto:
                input(message)

        # archive_finding = glob.glob(self.ms_ArchiveGlobPattern)
        archive_finding = glob.glob(self.ms_SnapShotArchiveGlobPattern)

        if len(archive_finding) == 0:
            archive_path = os.path.join(self.m_DependenciesDir, self.ms_ArchiveName)

            if os.path.exists(archive_path):
                os.remove(archive_path)
            assert not os.path.exists(archive_path)

            print(f"Téléchargement de l'archive « {ma_dep.cl_path(self.ms_ArchiveName)} ».")
            wget.download(self.ms_ArchiveUrl, archive_path)
            assert os.path.exists(archive_path)
            print("Téléchargement terminer.")

        elif len(archive_finding) == 1:
            archive_path = archive_finding[0]
        else:
            print("Plusieurs archives on été trouvées. Veuillez en choisir une pour être extraite et compilée.")
            i = 1
            for it_archive_path in archive_finding:
                print(f"{i}) {it_archive_path}")
                i += 1

            if self.auto:
                nb = 1
            else:
                nb = input("Inscrivez le numéro correspondant à votre choix (Ex: 1):")

            number = len(archive_finding) - 1
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractère saisie ne correspond pas à un numéro.")

            if 1 < number < len(archive_finding) + 1:
                archive_path = archive_finding[number - 1]
            else:
                raise ValueError("Vous avez n'avez pas rentré le bon numéro.")

        print(f"Extraction de l'archive « {ma_dep.cl_path(archive_path)} » dans le dossier " + self.m_Dir)

        shutil.unpack_archive(filename=archive_path,
                              extract_dir=self.m_Dir)

        basename = os.path.basename(archive_path)
        basename_split = basename.split(".")

        if len(basename_split) > 2 and basename_split[-2] == "tar" and basename_split[-1] == "gz":
            name = basename[:-len(".tar.gz")]
        else:
            name = basename[:- len(basename_split[-1]) - 1]

        extract_folder = os.path.join(self.m_Dir, name)
        shutil.move(extract_folder, self.m_Dir)

    def install_from_git(self):
        ma_dep.Install.git_clone(self, self.ms_GitUrl, True, self.ms_Tag,
                                 use_last_tag=self.ms_UseLastTag,
                                 archive_backup=self.ms_ArchiveUrl)

    """@todo optimiser cette compilation…"""

    def build(self):
        if sys.platform.startswith('linux'):
            self.build_linux()
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.build_windows()
        elif sys.platform.startswith('darwin'):
            self.build_darwin()

    def build_linux(self):
        ma_dep.FixWxWidgets().fix(self.m_Dir)

        os.chdir(self.m_Dir)

        # https://docs.wxwidgets.org/latest/plat_gtk_install.html
        if 'WEBKIT_LIBS' not in os.environ or not os.path.exists(os.environ['WEBKIT_LIBS']):
            webview2gtk_path = ''
            webview2gtk_directories = ['/usr/lib/', '/usr/lib64', '/usr/lib/x86_64-linux-gnu']

            # trouver les versions de webview2gtk installer
            for path in webview2gtk_directories:
                # /usr/lib64/libwebkit2gtk-4.0.so.37.37.6
                glob_results = glob.glob(os.path.join(path, 'libwebkit2gtk*'))
                if len(glob_results):
                    for glob_result in glob_results:
                        if not webview2gtk_path:
                            webview2gtk_path = glob_result
                        else:
                            last_minor = int(webview2gtk_path.split('.so')[0].split('.')[-1])
                            current_minor = int(glob_result.split('.so')[0].split('.')[-1])
                            if current_minor > last_minor:
                                webview2gtk_path = glob_result

            assert webview2gtk_path, f"""
Aucune librairie webkit2gtk n'a été trouvée.
La version de webkit2gtk par défaut est la 4.0 
Veuillez installer :
Sur Ubuntu: libwebkit2gtk-4.0-dev (n'est pas présente par défaut, c'est la 4.1 et ça ne fonctionne pas.)
Sur Debian: libwebkit2gtk-4.0-dev
Sur Mageia: lib64webkit2gtk4.0-devel
Sur Fedora: webkit2gtk3-devel
"""
            # WEBKIT_CFLAGS  C compiler flags for WEBKIT, overriding pkg-config
            # WEBKIT_LIBS    linker flags for WEBKIT, overriding pkg-config
            os.environ['WEBKIT_LIBS'] = webview2gtk_path
            ma_dep.Environ.ms_Vars['WEBKIT_LIBS'] = webview2gtk_path
        else:
            ma_dep.Environ.ms_Vars['WEBKIT_LIBS'] = os.environ['WEBKIT_LIBS']

        # Test #
        ma_config = ma_dep.MaConfig()
        py_env_app_path = ma_config.get_value(ma_dep.MaConfig.ms_Key_PythonPath)
        assert os.path.exists(py_env_app_path), (f"Le chemin de l'application python n'existe pas: "
                                                 f"{ma_dep.cl_path(py_env_app_path)}")
        self.m_PythonBinPath = py_env_app_path

        if self.ms_Debug:
            target = "--dev --debug"
        else:
            target = "--release"

        ma_dep.call(self.m_PythonBinPath + ' build.py cleanall')
        # Pour forcer à générer les fichiers du dossier sip.
        ma_dep.call(self.m_PythonBinPath + f' build.py '
                                           f'dox etg --nodoc sip build --force_config --gtk3 {target} --nodoc -v '
                                           f'--python="{self.m_PythonBinPath}" --jobs={ma_dep.thread_count()}')

        # Pour compiler normalement…
        ma_dep.call(self.m_PythonBinPath + f' build.py build_wx etg --force_config --gtk3 {target} --nodoc -v '
                                           f'--python="{self.m_PythonBinPath}" --jobs={ma_dep.thread_count()}')

        os.chdir(self.m_DependenciesDir)
        ma_dep.FixWxPython().fix(self.m_Dir)

        os.chdir(self.m_Dir)
        ma_dep.call(self.m_PythonBinPath + f' build.py --python="{self.m_PythonBinPath}" '
                                           f'dox doxhtml touch sip build_py --force_config --nodoc -v --keep --gtk3 '
                                           f'{target} --extra_waf="--pythonarchdir={self.m_PythonSPPath} '
                                           f'--pythondir={self.m_PythonBinPath}" --jobs={ma_dep.thread_count()}')

        os.chdir(self.m_PythonPath)

        # https://stackoverflow.com/questions/52866645/could-not-fetch-url-https-pypi-python-org-simple-xlrd
        # ma_dep.call('"' + self.m_PythonBinPath + '" "' + self.m_PipBinPath + '" install --upgrade --trusted-host=pypi.python.org --trusted-host=pypi.org --trusted-host=files.pythonhosted.org "' + self.m_Dir + '"')
        ma_dep.call('"' + self.m_PythonBinPath + '" "' + self.m_PipBinPath + '" install --upgrade "' + self.m_Dir + '"')
        # ma_dep.call('"' + self.m_PythonBinPath + '" setup.py install')



        os.chdir(self.m_DependenciesDir)

    def build_windows(self):
        # https://stackoverflow.com/questions/8125968/how-to-build-wxpython-trunk-with-mingw-on-windows
        # https://wiki.wxpython.org/BuildWxPythonOnWindows
        # https://web.archive.org/web/20071013073037/http://www.mingw.org/cms/node/17
        # Mingw : https://github.com/wxWidgets/Phoenix/issues/1582
        # Mingw : https://bitbucket.org/ustsv/testpkg_msys2/src/master/mingw-w64-wxpython-phoenix-git/

        ma_dep.init_visual()

        ma_dep.FixWxWidgets().fix(self.m_Dir)

        os.chdir(self.m_Dir)

        # \todo --x64 ajouter cette option pour forcer à compiler en 64bits sur windows

        ma_dep.call(self.m_PythonBinPath + ' build.py cleanall')
        # Pour forcer à généer les fichiers du dossier sip.
        ma_dep.call(self.m_PythonBinPath +
                    f' build.py --dump_waf_log --regenerate_sysconfig --nodoc sip build --force_config --gtk3 '
                    f'--release --nodoc -v --python="{self.m_PythonBinPath}" --jobs={ma_dep.thread_count()}')

        # Pour compiler normalement…
        ma_dep.call(self.m_PythonBinPath +
                    f' build.py build_wx --dump_waf_log --regenerate_sysconfig --force_config --gtk3 --release '
                    f'--nodoc -v --python="{self.m_PythonBinPath}" --jobs={ma_dep.thread_count()}')

        os.chdir(self.m_DependenciesDir)
        ma_dep.FixWxPython().fix(self.m_Dir)

        os.chdir(self.m_Dir)
        #  --jom
        ma_dep.call(self.m_PythonBinPath +
                    f' build.py --dump_waf_log --regenerate_sysconfig --python="{self.m_PythonBinPath}" touch sip '
                    f'build_py --force_config --nodoc -v --keep --gtk3 --release '
                    f'--extra_waf="--pythonarchdir={self.m_PythonSPPath} --pythondir={self.m_PythonBinPath}" '
                    f'--jobs={ma_dep.thread_count()}')

        os.chdir(self.m_PythonPath)

        # https://stackoverflow.com/questions/52866645/could-not-fetch-url-https-pypi-python-org-simple-xlrd
        # ma_dep.call('"' + self.m_PythonBinPath + '" "' + self.m_PipBinPath + '" install --upgrade --trusted-host=pypi.python.org --trusted-host=pypi.org --trusted-host=files.pythonhosted.org "' + self.m_Dir + '"')
        ma_dep.call(f'"{self.m_PythonBinPath}" "{self.m_PipBinPath}" install --upgrade "{self.m_Dir}"')
        # ma_dep.call('"' + self.m_PythonBinPath + '" setup.py install')

        os.chdir(self.m_DependenciesDir)

    def build_darwin(self):
        """Construction de la solution wxPython sur OSX.
            .. see: https://wiki.wxpython.org/wxPythonOSX%20Building%20from%20source
            --mac_universal_binary
            --mac_framework
            --mac_arch=

            -mmacosx-version-min=10.9 ?
        """
        # ma_dep.FixWxWidgets().fix(self.m_Dir)

        os.chdir(self.m_Dir)

        ma_dep.call(self.m_PythonBinPath + ' build.py cleanall')
        # Pour forcer à généer les fichiers du dossier sip.
        ma_dep.call(self.m_PythonBinPath +
                    f' build.py dox etg --nodoc sip build --force_config --gtk3 --release --nodoc --mac_arch=x86_64 '
                    f'-v --python="{self.m_PythonBinPath}" --jobs={ma_dep.thread_count()}')

        # Pour compiler normalement…
        ma_dep.call(self.m_PythonBinPath +
                    f' build.py build_wx etg --force_config --gtk3 --release --nodoc --mac_arch=x86_64 -v '
                    f'--python="{self.m_PythonBinPath}" --jobs={ma_dep.thread_count()}')

        os.chdir(self.m_DependenciesDir)
        # ma_dep.FixWxPython().fix(self.m_Dir)

        os.chdir(self.m_Dir)
        ma_dep.call(self.m_PythonBinPath +
                    f' build.py --python="{self.m_PythonBinPath}" dox doxhtml touch sip build_py --force_config '
                    f'--nodoc -v --keep --gtk3 --release --extra_waf="--pythonarchdir={self.m_PythonSPPath} '
                    f'--pythondir={self.m_PythonBinPath}" --mac_arch=x86_64 --jobs={ma_dep.thread_count()}')

        os.chdir(self.m_PythonPath)

        # https://stackoverflow.com/questions/52866645/could-not-fetch-url-https-pypi-python-org-simple-xlrd
        # ma_dep.call('"' + self.m_PythonBinPath + '" "' + self.m_PipBinPath + '" install --upgrade --trusted-host=pypi.python.org --trusted-host=pypi.org --trusted-host=files.pythonhosted.org "' + self.m_Dir + '"')
        ma_dep.call(f'"{self.m_PythonBinPath}" "{self.m_PipBinPath}" install --upgrade "{self.m_Dir}"')
        # ma_dep.call('"' + self.m_PythonBinPath + '" setup.py install')

        os.chdir(self.m_DependenciesDir)

    def init_config(self):
        super().init_config()

        group_wx = "wx"

        wxpython_path = os.path.join(self.m_Dir, '/wxPython')
        key_wxpython_path = ma_dep.GroupKeyDefaultValue(group_wx, 'MA_WXPYTHON_PATH', wxpython_path)
        key_wx_minor_version = ma_dep.GroupKeyDefaultValue(group_wx, 'MA_WX_MINOR_VERSION', '0')
        key_wx_so_suffix = ma_dep.GroupKeyDefaultValue(group_wx, 'MA_WX_SO_SUFFIX', '.so.0')

        wx_venv_path = os.path.join(self.m_PythonSPPath, 'wx')

        paths = glob.glob(os.path.join(wx_venv_path, "libwx_baseu-*"))

        if len(paths):
            lib_wx_base_path = os.path.basename(paths[0])

            # libwx_baseu-3.2.so.0 ou libwx_baseu-3.2.so.0.2.1
            base, suffix = lib_wx_base_path.split('-')

            if suffix.count('.') == 3:
                # 3.2.so.0
                major, minor, extension, extension_num = suffix.split('.')
            elif suffix.count('.') == 5:
                # 3.2.so.0.2.1
                major, minor, extension, extension_num, extension_num_2, extension_num_3 = suffix.split('.')
            else:
                raise ValueError(f"La librairie {ma_dep.cl_path(lib_wx_base_path)} n'est pas formaté comme prévu.")

            self.m_Config.set_value(key_wx_minor_version, minor)  # '0'
            self.m_Config.set_value(key_wx_so_suffix, f'.{extension}.{extension_num}')  # '.so.0'


if __name__ == '__main__':
    InstallwxPython().go()
