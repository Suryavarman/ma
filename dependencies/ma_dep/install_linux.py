#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import os
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallLinux(ma_dep.Install):
    def __init__(self):
        super().__init__(name='Linux')

    def go(self):
        self.go_begin()
        self.init_config()
        self.go_end()

    def init_config(self):
        super().init_config()

        group_name = "linux"

        def set_value(key, default_path):
            key_path = ma_dep.GroupKeyDefaultValue(group_name, key, default_path)
            self.m_Config.set_value(key_path, default_path)

        # gtk-3.0
        set_value('MA_LINUX_GTK3_INCLUDE_PATH', '/usr/include/gtk-3.0')

        # glib-2.0
        result = ma_dep.call('pkg-config --cflags glib-2.0')
        glib_directories = result[1][0].strip().split(" ")

        assert len(glib_directories) == 2, (f"pkg-config --cflags glib-2.0 ne renvoie pas deux chemins: "
                                            f"{glib_directories}")

        default_glib2_path = glib_directories[0].split("I")[1]
        default_glib2_config_path = glib_directories[1].split("I")[1]

        set_value('MA_LINUX_GLIB_INCLUDE_PATH', default_glib2_path)
        set_value('MA_LINUX_GLIB_CONFIG_INCLUDE_PATH', default_glib2_config_path)

        # pango
        set_value('MA_LINUX_PANGO_INCLUDE_PATH', '/usr/include/pango-1.0')

        # cairo
        set_value('MA_LINUX_CAIRO_INCLUDE_PATH', '/usr/include/cairo')

        # gdkpixbuf
        set_value('MA_LINUX_GDKPIXBUF_INCLUDE_PATH', '/usr/include/gdk-pixbuf-2.0')

        # atk
        set_value('MA_LINUX_ATK_INCLUDE_PATH', '/usr/include/atk-1.0')

        # harfbuzz
        set_value('MA_LINUX_HARFBUZZ_INCLUDE_PATH', '/usr/include/harfbuzz')


if __name__ == '__main__':
    InstallLinux().go()
