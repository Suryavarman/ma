#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#
# https://github.com/OGRECave/ogre-next
# Pour compiler ogre-next:
# https://ogrecave.github.io/ogre-next/api/latest/_setting_up_ogre.html
# Dépendances :
# - rapidjson

import glob
import os
from pathlib import Path
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.
import ma_dep


class InstallOgreNext(ma_dep.Install):
    ms_UseLastTag = True  # si vrais ms_GitTag sera ignoré, le dernier tag du dépôt sera utilisé.
    ms_GitTag = "v2.3.3"
    ms_GitUrl = 'https://github.com/OGRECave/ogre-next'
    ms_BackupArchive = f'https://codeload.github.com/OGRECave/ogre-next/zip/refs/tags/%TAG%'

    ms_DepsGitTag = "bin-releases"
    ms_DepsGitUrl = "https://github.com/OGRECave/ogre-next-deps"
    ms_DepsBackupArchive = f'https://codeload.github.com/OGRECave/ogre-next-deps/zip/refs/tags/{ms_DepsGitTag}'

    # git clone --recurse-submodules --shallow-submodules https://github.com/OGRECave/ogre-next-deps

    def __init__(self, auto=False):
        super().__init__(name='OgreNext')
        self.m_DepsDir = os.path.join(self.m_Dir, "dependencies")
        self.auto = auto

        if self.m_Result.m_Done:
            if InstallOgreNext.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallOgreNext.ms_GitTag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
Attention le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
            nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()

                if number == 1 or number == 3:
                    self.build(remove_build_dir=number == 3)
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_GitUrl,
                                 update_submodule=True,
                                 tag=self.ms_GitTag,
                                 use_last_tag=self.ms_UseLastTag)

        ma_dep.Install.git_clone(self,
                                 url=self.ms_DepsGitUrl,
                                 update_submodule=True,
                                 tag=self.ms_DepsGitTag,
                                 manual_dir=self.m_DepsDir,
                                 archive_backup=self.ms_BackupArchive)

    def build(self, remove_build_dir=True):
        if sys.platform.startswith('linux'):
            self.build_linux(remove_build_dir)
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.build_windows(remove_build_dir)
        elif sys.platform.startswith('darwin'):
            self.build_darwin(remove_build_dir)

        print(f"Installation de {self.get_terminal_name()} terminée.")

    def build_linux(self, remove_build_dir=True):
        os.chdir(self.m_Dir)

        build_debug_path = os.path.join(self.m_Dir, "built_debug")
        build_release_path = os.path.join(self.m_Dir, "build_release")

        if os.path.exists(build_debug_path) and remove_build_dir:
            self.remove_folder(build_debug_path)

        if os.path.exists(build_release_path) and remove_build_dir:
            self.remove_folder(build_release_path)

        if not os.path.exists(build_debug_path):
            os.mkdir(build_debug_path)

        if not os.path.exists(build_release_path):
            os.mkdir(build_release_path)

        os.chdir(build_debug_path)
        ma_dep.call(f'cmake -DOGRE_BUILD_SAMPLES2=1 -DOGRE_CONFIG_THREAD_PROVIDER=0 -DOGRE_CONFIG_THREADS=0 -DCMAKE_BUILD_TYPE=Debug {self.m_Dir}')
        ma_dep.call('make -j ' + str(ma_dep.thread_count()))

        os.chdir(build_release_path)
        ma_dep.call(f'cmake -DOGRE_BUILD_SAMPLES2=1 -DOGRE_CONFIG_THREAD_PROVIDER=0 -DOGRE_CONFIG_THREADS=0 -DCMAKE_BUILD_TYPE=Release {self.m_Dir}')
        ma_dep.call('make -j ' + str(ma_dep.thread_count()))

        self.create_config_files(build_debug_path, build_release_path)

    def build_darwin(self, remove_build_dir=True):
        pass

    def build_windows(self, remove_build_dir=True):
        pass

    def init_config(self):
        super().init_config()
        # bin_path = f"{build_path}/bin"
        # input_plugin_cfg_path = f"{bin_path}/plugins.cfg"
        # output_plugin_cfg_path = f"{bin_path}/ma_plugins.cfg"
        # output_resources_cfg_path = f"{bin_path}/resources.cfg"
        #
        # with open(input_plugin_cfg_path) as input_file, open(output_plugin_cfg_path, "w") as output_file:
        #     for line in input_file:
        #         if "PluginFolder" in line:
        #             path = line.split("=")[1]
        #             relative_path = os.path.normpath(os.path.relpath(path, f"{build_path}/bin")).replace("\n", "")
        #             output_file.write(f'PluginFolder={relative_path}\n')
        #         else:
        #             output_file.write(line)
        #
        # key_bin_path = ma_dep.GroupKeyDefaultValue(self.m_Config.ms_GroupOgre,
        #                                           "MA_OGRENEXT_BIN_PATH",
        #                                            bin_path)
        #
        # key_plugin_cfg_path = ma_dep.GroupKeyDefaultValue(self.m_Config.ms_GroupOgre,
        #                                                   "MA_OGRENEXT_PLUGIN_CFG_PATH",
        #                                                   output_plugin_cfg_path)
        #
        # key_resources_cfg_path = ma_dep.GroupKeyDefaultValue(self.m_Config.ms_GroupOgre,
        #                                                     "MA_OGRENEXT_RESOURCES_CFG_PATH",
        #                                                      output_resources_cfg_path)
        #
        # self.m_Config.set_value(key_bin_path, bin_path)
        # self.m_Config.set_value(key_plugin_cfg_path, output_plugin_cfg_path)
        # self.m_Config.set_value(key_resources_cfg_path, output_resources_cfg_path)
        pass


if __name__ == '__main__':
    InstallOgreNext().go()
