#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

# svn checkout https://svn.code.sf.net/p/irrlicht/code/trunk irrlicht-code

import os
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallIrrlicht(ma_dep.Install):
    ms_Version = '1.8'
    ms_SvnUrl = f'https://sourceforge.net/p/irrlicht/code/HEAD/tree/branches/releases/{ms_Version}'

    def __init__(self, auto=False):
        super().__init__(name='Irrlicht')
        self.auto = auto

        if self.m_Result.m_Done:
            self.m_Result.m_Done = self.m_Result.m_Version == InstallIrrlicht.ms_SvnUrl

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
Attention le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
            nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()

                if number == 1 or number == 3:
                    self.build(remove_build_dir=number == 3)
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.svn_clone(self,
                                 url=self.ms_SvnUrl)

    def build(self, remove_build_dir=True):
        if sys.platform.startswith('linux'):
            self.build_linux(remove_build_dir)
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.build_windows(remove_build_dir)
        elif sys.platform.startswith('darwin'):
            self.build_darwin(remove_build_dir)

        print(f"Installation de « {self.m_Name} » terminée.")

    def build_windows(self, remove_build_dir=True):
        # https://sourceforge.net/p/irrlicht/code/HEAD/tree/branches/releases/1.8/bin/Win64-VisualStudio/
        os.chdir(os.path.join(self.m_Dir, "source"))

        # BuildAllExamples_*.sln

    def build_linux(self, remove_build_dir=True):
        # https://sourceforge.net/p/irrlicht/code/HEAD/tree/branches/releases/1.8/bin/Linux/
        os.chdir(os.path.join(self.m_Dir, "source"))

        ma_dep.call(f'make')

    def build_darwin(self, remove_build_dir=True):
        # https://sourceforge.net/p/irrlicht/code/HEAD/tree/branches/releases/1.8/bin/MacOSX/
        os.chdir(os.path.join(self.m_Dir, "source"))


if __name__ == '__main__':
    InstallIrrlicht().go()
