#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import os
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallClang(ma_dep.Install):
    ms_UseLastTag = True  # si vrais le dernier tag du dépôt sera utilisé.
    ms_GitTag = 'llvmorg-17.0.6'
    ms_GitCommitHash = 'c280bed85'
    ms_GitUrl = 'https://github.com/llvm/llvm-project.git'
    ms_GitDepth = 0  # 1 # si 1 le commit hash n'est pas téléchargé. # 0 est lent

    def __init__(self, auto=False):
        super().__init__(name='Clang')
        self.auto = auto

        if self.m_Result.m_Done:
            if InstallClang.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallClang.ms_GitCommitHash

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
{ma_dep.cl_warning("Attention")} le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()

                if number == 1 or number == 3:
                    self.build(remove_build_dir=number == 3)
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self):
        if self.ms_UseLastTag:
            ma_dep.Install.git_clone(self,
                                     url=self.ms_GitUrl,
                                     update_submodule=True,
                                     tag=self.ms_GitTag,
                                     use_last_tag=self.ms_UseLastTag)
        else:
            ma_dep.Install.git_clone(self,
                                     url=self.ms_GitUrl,
                                     update_submodule=True,
                                     # tag=self.ms_GitTag,
                                     # use_last_tag=self.ms_UseLastTag,
                                     depth=self.ms_GitDepth,
                                     commit_hash=self.ms_GitCommitHash)

    def build(self, remove_build_dir=True):
        os.chdir(self.m_Dir)
        build_path = os.path.join(self.m_Dir, "build")

        if os.path.exists(build_path) and remove_build_dir:
            self.remove_folder(build_path)

        if not os.path.exists(build_path):
            os.mkdir(build_path)

        os.chdir(build_path)
        ma_dep.call('cmake -DLLVM_ENABLE_PROJECTS=clang -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ../llvm')
        ma_dep.call('make -j ' + str(ma_dep.thread_count()))
        # ma_dep.call('make check-clang')

        bin_path = os.path.join(build_path, 'bin')

    def init_config(self):
        super().init_config()

        built_path = os.path.join(self.m_Dir, "built")
        built_debug_path = os.path.join(self.m_Dir, "built_debug")
        build_path = os.path.join(self.m_Dir, "build")

        group_name = "clang"

        bin_path = os.path.join(group_name, 'Clang', 'build', 'bin')
        key_bin_path = ma_dep.GroupKeyDefaultValue(group_name, "MA_CLANG_PATH", bin_path)
        self.m_Config.set_value(key_bin_path, bin_path)


if __name__ == '__main__':
    InstallClang().go()
