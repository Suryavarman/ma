#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import os
import sys
import ma_dep


def parse_log(log_path, deps=None):
    """
    Permet de parcourir et trier la liste des dépendances.
    :param log_path: Le chemin du fichier contenant la liste des dépendances.
    :param deps: La variable « deps » contient [le nom de la librairie sans l'extension] = [path_1, path_2, ...]
    """
    assert os.path.exists(log_path), f"Le fichier {log_path} n'existe pas."
    assert deps is not None, "La variable deps ne doit pas être nulle."

    with open(log_path) as file:
        for line in file.readlines():
            # "	libGLdispatch.so.0 => /lib64/libGLdispatch.so.0 (0x00007f112b1ba000)"
            split_result = line.split("=>")
            # [" libGLdispatch.so.0 ", " /lib64/libGLdispatch.so.0 (0x00007f112b1ba000)"]
            if len(split_result) == 2:
                name = split_result[0].split('.')[0].strip().lstrip("lib")
                path = split_result[1].split('(')[0].strip()

                if name not in deps:
                    deps[name] = []

                other_paths = deps[name]
                same_file = False
                for other_path in other_paths:
                    same_file = os.path.samefile(other_path, path)

                if not same_file:
                    deps[name].append(path)


def main(args):
    assert os.path.exists(args.exe_a_path), "Le chemin de la première application ne fonctionne pas."
    assert os.path.exists(args.exe_b_path), "Le chemin de la deuxième application ne fonctionne pas."

    deps_compare_folder_name = "compare_dependencies"
    log_directory = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", deps_compare_folder_name))
    print(log_directory)
    if not os.path.exists(log_directory):
        os.mkdir(log_directory)

    log_a = os.path.join(log_directory, os.path.basename(args.exe_a_path).split('.')[0] + ".txt")
    log_b = os.path.join(log_directory, os.path.basename(args.exe_b_path).split('.')[0] + ".txt")

    if sys.platform.startswith('linux'):
        ma_dep.call(f'ldd -v "{args.exe_a_path}" > {log_a}')
        ma_dep.call(f'ldd -v "{args.exe_b_path}" > {log_b}')
        pass
    elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
        assert False, "N'est pas implémentée."
    elif sys.platform.startswith('darwin'):
        assert False, "N'est pas implémentée."

    deps = {}
    parse_log(log_a, deps)
    parse_log(log_b, deps)

    for name, paths in deps.items():
        if len(paths) > 1:
            print(f"{name}: {paths}")
        # else:
        #     print(f"{name}: unique")


if __name__ == '__main__':
    import argparse
    arg_parser = argparse.ArgumentParser(description='Définition des chemins des deux exécutables pour comparer leurs '
                                                     'dépendances.')
    arg_parser.add_argument('--exe_a_path',
                            help="Le chemin du premier exécutable.",
                            type=str,
                            required=True)

    arg_parser.add_argument('--exe_b_path',
                            help="Le chemin du deuxième exécutable.",
                            type=str,
                            required=True)

    main(arg_parser.parse_args())
