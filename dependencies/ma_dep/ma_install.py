#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import glob
import os
import sys
import shutil
import time
import re
import ma_dep
import wget


class Result:
    key_done = "done"
    key_version = "version"

    def __init__(self, path: str):
        self.m_Path = path
        self.m_Done = False
        self.m_Version = ""

        if os.path.exists(self.m_Path):
            key_values = {}
            with open(self.m_Path, "r") as file:
                for line in file.readlines():
                    if "=" in line:
                        key, value = line.split("=")
                        key = key.strip()
                        value = value.strip()
                        key_values[key] = value

            if Result.key_done in key_values:
                self.m_Done = key_values[Result.key_done] == "True"

            if Result.key_version in key_values:
                self.m_Version = key_values[Result.key_version]

        else:
            self.write()

    def write(self):
        if os.path.exists(os.path.dirname(self.m_Path)):
            key_values = {Result.key_done: str(self.m_Done),
                          Result.key_version: str(self.m_Version)}

            with open(self.m_Path, "w") as file:
                for key, value in key_values.items():
                    file.write(f"{key} = {value}\n")
        else:
            print(ma_dep.cl_warning("Attention") +
                  ": le fichier ne peut être créé, car son dossier parent n'existe pas. " + ma_dep.cl_path(self.m_Path))


class Install:
    """Nombre maximal pour tenter de cloner un dépôt."""
    ms_MaxCloneCounterTry = 10

    def __init__(self, name):
        print("\n" + ma_dep.cl_project_separator(f"Installation de", name))

        """Permet d'avoir des variables d'environnements uniques et modifiables, car Environ.ms_Vars est statique."""
        self.m_Environ = ma_dep.Environ()

        """ Le nom de la dépendance, elle servira aussi à créer le dossier où seront placées les ressources propres à
        cette dépendance.
        """
        self.m_Name = name

        """Le chemin du dossier contenant les dépendances à préparer pour compiler Ma"""
        self.m_DependenciesDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        """ Le chemin des sources de Ma."""
        self.m_SourcesDir = os.path.join(os.path.dirname(self.m_DependenciesDir), "code")

        """ Le chemin du dossiers contentant les sous-modules git."""
        self.m_SubModulesGitDir = os.path.join(self.m_SourcesDir, "modules", "git", "gitmodules")

        """ Le chemin du dossiers contentant les modules de Ma."""
        self.m_ModulesMaDir = os.path.join(self.m_SourcesDir, "modules", "ma")

        """ Une instance du fichier de configuration où sont répertorié les chemins qui serviront à retrouver
            les dépendances. 
        """
        self.m_Config = ma_dep.MaConfig()

        """Le dossier où sera ou est déjà installer de l'environnement python."""
        self.m_PythonPath = os.path.normpath(self.m_Config.get_value(ma_dep.MaConfig.ms_Key_PythonEnvPath))

        if sys.platform.startswith('linux') or sys.platform.startswith('darwin') or sys.platform.startswith('aix'):
            bin_path = os.path.join(self.m_PythonPath, 'bin')
            python_path = glob.glob(os.path.join(bin_path, "python*"))
            pip_path = glob.glob(os.path.join(bin_path, "pip*"))
            self.m_PythonBinPath = python_path[0]
            self.m_PipBinPath = pip_path[0]
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.m_PythonBinPath = os.path.normpath(
                os.path.join(os.path.join(self.m_PythonPath, 'Scripts'), 'python.exe'))
            self.m_PipBinPath = os.path.normpath(os.path.join(os.path.join(self.m_PythonPath, 'Scripts'), 'pip3.exe'))

        """ Le dossier de la dépendance où sont placés les ressources propre à celle-ci."""
        self.m_Dir = os.path.normpath(os.path.join(self.m_DependenciesDir, self.m_Name))

        """ Le fichier qui sauvegardera l'état de la compilation de cette dépendance."""
        self.m_Result = Result(os.path.join(self.m_Dir, "ma_dep_result.txt"))

        """ Permet aux modules qui dépendent de cette instance de savoir si il doit se recompiler."""
        self.m_HasBeenRebuild = not self.m_Result.m_Done

        message = f"""
Liste des répertoires utilisés pour la compilation de {self.get_terminal_name()}:
Dépendances . . . . . . . . . . . : {ma_dep.cl_path(self.m_DependenciesDir)}
Environnement de travail python . : {ma_dep.cl_path(self.m_PythonPath)}
Installation de {self.get_terminal_name()} . . . . : {ma_dep.cl_path(self.m_Dir)}
"""
        print(message)

    def __del__(self):
        self.m_Result.write()

    def install_py_modules(self, modules, use_current_python=False):
        """
        Installe un ou plusieurs modules python.
        :param modules:
        :param use_current_python: Si vrais, il installera le module dans le python qui exécute ce script.
        :return:
        """
        # https://stackoverflow.com/questions/10667960/python-requests-throwing-sslerror
        # https://docs.python.org/3/library/ssl.html
        # Caused by SSLError(SSLError(161, '[SSL: LIBRARY_HAS_NO_CIPHERS] library has no ciphers
        # https://stackoverflow.com/questions/38015537/python-requests-exceptions-sslerror-dh-key-too-small
        # https://stackoverflow.com/questions/25981703/pip-install-fails-with-connection-error-ssl-certificate-verify-failed-certi
        # https://pip.pypa.io/en/stable/reference/pip/#general-options
        # https://ask.xiaolee.net/questions/1024029

        if use_current_python:
            python3_path = sys.executable
            bin_folder_path = os.path.dirname(python3_path)
            pip_path = os.path.join(bin_folder_path, "pip")
        else:
            if sys.platform.startswith('linux') or sys.platform.startswith('darwin') or sys.platform.startswith('aix'):
                bin_folder_name = "bin"
                # python_app_name = "python3"
            elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
                bin_folder_name = "Scripts"
                # python_app_name = "python"
            else:
                assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

            # python3_path = os.path.join(self.m_PythonPath, bin_folder_name, python_app_name)
            pip_path = os.path.join(self.m_PythonPath, bin_folder_name, "pip")
            # setup_path = os.path.join(self.m_PythonPath, "setup.py")
            # get_pip_path = os.path.join(self.m_PythonPath, 'get-pip.py')

        # curl https://bootstrap.pypa.io/get-pip.py -o oself.m_PythonPath, get-pip.py
        # ma_dep.call('curl https://bootstrap.pypa.io/get-pip.py -o "' +
        #             os.path.join(self.m_PythonPath, 'get-pip.py') + '"')

        # ma_dep.call('"' + python3_path + '" "' + get_pip_path + '" --no-index --find-links=' + os.path.join(self.m_PythonPath, "bin"))

        # ma_dep.call('"' + pip_path+ '" --cert /home/suryavarman/Working/Ma/dependencies/ma_dep/cacert.pem install linkchecker')
        # ma_dep.call('"' + python3_path + '" install --upgrade --trusted-host=pypi.python.org --trusted-host=pypi.org --trusted-host=files.2pythonhosted.org ' + modules)

        if type(modules) is list or type(modules) is tuple:
            for module in modules:
                ma_dep.call(f'"{pip_path}" install --trusted-host=pypi.python.org --trusted-host=pypi.org '
                            f'--trusted-host=files.pythonhosted.org --upgrade {module}')
        else:
            ma_dep.call(f'"{pip_path}" install --trusted-host=pypi.python.org --trusted-host=pypi.org '
                        f'--trusted-host=files.pythonhosted.org --upgrade {modules}')

        # ma_dep.call('"' + pip_path + '" install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org --upgrade ' + modules)
        # ma_dep.call('"' + pip_path + '" install --upgrade --trusted-host=pypi.python.org --trusted-host=pypi.org --trusted-host=files.pythonhosted.org ' + modules)
        # ma_dep.call('"' + pip_path + '" install --upgrade ' + modules)
        # ma_dep.call('"' + pip_path + '" install --trusted-host pypi.org --trusted-host files.pythonhosted.org pip setuptools')
        # ma_dep.call('"' + pip_path + '" install --upgrade ' + modules)
        # ma_dep.call('"' + os.path.join(self.m_PythonPath, "bin" + os.sep + "easy_install-3.7") + '" ' + modules)
        # ma_dep.call('"' + pip_path + '" install --upgrade config --global http.sslVerify false' + modules)

    def git_clone(self,
                  url,
                  update_submodule=True,
                  tag='',
                  quiet=False,
                  counter_try=10,
                  manual_dir="",
                  branche="",
                  commit_hash="",
                  use_last_tag=False,
                  depth=0,
                  archive_backup=""):
        """
        Clone un dépôt en utilisant git.
        :param url: L'adresse du dépôt.
        Il faut de préférence utilisé la version « https: » du lien et non la version « git: ».
        :param update_submodule: Si vrais les sous-modules seront téléchargé et mis à jour.
        :param tag: L'étiquette de la version sera télécharger.
        :param quiet:
        :param counter_try:
        :param manual_dir:
        :param branche: La branche qui sera télécharger.
        :param commit_hash: Si renseigné ce numéro de commit sera utilisé.
        :param use_last_tag: Si sa valeur est à « vrais », alors tags ne sera pas utilisé.
        La dernière version étiquetée qui ne soit pas une préversion sera utilisée.
        :param depth: Si « depth » est supérieur à 0 (par exemple 1), alors la commande depth de git sera utilisée et
        permettra ainsi d'accélérer le téléchargement des sources.
        :param archive_backup: Si spécifier l'url de l'archive de secours sera utilisée au cas où le clonage échoue.
        :return:
        """
        # https://stackoverflow.com/questions/3954852/how-to-complete-a-git-clone-for-a-big-project-on-an-unstable-connection
        # https://stackoverflow.com/questions/1778088/how-do-i-clone-a-single-branch-in-git
        directory = manual_dir if manual_dir != "" else self.m_Dir
        assert not os.path.exists(directory), f"Le dossier {ma_dep.cl_path(directory)} existe déjà."

        ma_dep.call('git config --local core.compression 0')
        ma_dep.call('git config --local https.postBuffer 524288000')
        ma_dep.call('git config --local http.maxRequestBuffer 100M')

        current_dir = os.getcwd()

        # git clone --depth <Number> <repository> --branch <branch name> --single-branch
        branch_arg = branche
        if tag or use_last_tag:
            if use_last_tag:

                if not os.path.exists(directory):
                    os.mkdir(directory)

                os.chdir(directory)
                ma_dep.call(f'git init')
                ma_dep.call(f'git remote add origin {url}')

                last_tags = Install.git_last_tags()

                os.chdir(current_dir)
                self.remove_folder(directory)

                # Il n'y a pas besoin de tags/ devant {last_tags[-1]} tags/ est nécessaire pour la commande checkout.
                branch_arg = f"--branch {last_tags[-1]} --single-branch"
                archive_backup = archive_backup.replace("%TAG%", last_tags[-1])
                self.m_Result.m_Version = last_tags[-1]
            else:
                # Il n'y a pas besoin de tags/ devant {last_tags[-1]} tags/ est nécessaire pour la commande checkout.
                branch_arg = f"--branch {tag} --single-branch"
                archive_backup = archive_backup.replace("%TAG%", tag)
                self.m_Result.m_Version = tag
        elif branche:
            branch_arg = f"--branch {branche} --single-branch"
            archive_backup = archive_backup.replace("%BRANCH%", branche)
            self.m_Result.m_Version = branche

        if depth < 1:
            depth = 1

        result = ma_dep.call(f'git clone {url} --depth={depth} {branch_arg} "{directory}"')

        #  C'est au moment du clonage du dépôt que le dossier parent, du fichier de résultats, est créé.
        self.m_Result.write()

        # if os.path.exists(directory):
        #     os.chdir(directory)
        #     # risque d'échoué avec une mauvaise connexion
        #     # todo Puisque que la branche désirée est présente, est-ce utile?
        #     ma_dep.call(f'git fetch --unshallow')
        #     ma_dep.call(f'git pull --all')
        #     os.chdir(current_dir)

        def no_error():
            if len(result) > 0:
                error_tags = ["erreur",  # français
                              "error",  # anglais, espagnole
                              "fehler",  # allemand
                              "грешка",  # bulgare
                              "错误",  # chinois
                              "오류",  # coréen
                              "fejl",  # danois
                              "fallo",  # espagnole
                              "viga",  # estonien
                              "virhe",  # finnois
                              "σφάλμα", "λάθος",  # grec
                              "hiba",  # hongrois
                              "kesalahan", "salah",  # indonésiens
                              "errore", "errori",  # italien
                              "エラー", "誤り", "ミス", "間違い",  # japonais
                              "kļūda",  # letton
                              "klaida",  # lituanien
                              "fout",  # néerlandais
                              "feil",  # norvégiens
                              "błąd",  # polonais
                              "błąd",  # polonais
                              "erro", "erros", "engano",  # portugais
                              "eroare",  # roumain
                              "ошибка",  # russe
                              "chyba", "chybu",  # slovaque, tchèque
                              "napaka",  # slovène
                              "napaka",  # slovène
                              "fel",  # suédois
                              "hata",  # turc
                              "помилка", "похибка"]  # ukrainien

                for error_message in result[1]:
                    for error_tag in error_tags:
                        if error_tag in error_message:
                            return False
            return True

        if not archive_backup:
            assert no_error(), "git clone a échoué."
        else:
            if not no_error():
                if not os.path.exists(self.m_Dir):
                    os.mkdir(self.m_Dir)
                print(f"""
La commande {ma_dep.cl_key('git clone')} à échouée."
Téléchargement de l'archive de secours en cours:
{ma_dep.cl_path(archive_backup)}""")

                # https://stackoverflow.com/questions/58125279/python-wget-module-doesnt-show-progress-bar
                def bar_progress(current, total, width=80):
                    progress_message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
                    # Don't use print() as it will print in new line every time.
                    # sys.stdout.write("\r" + progress_message)
                    sys.stdout.flush()
                    return progress_message

                archive_path = wget.download(url=archive_backup, out=self.m_Dir, bar=bar_progress)
                print("")
                assert os.path.exists(archive_path)

                basename = os.path.basename(archive_path)
                basename_split = basename.split(".")

                if len(basename_split) > 2 and basename_split[-2] == "tar" and basename_split[-1] == "gz":
                    name = basename[:-len(".tar.gz")]
                else:
                    name = basename[:- len(basename_split[-1]) - 1]
                extract_folder = os.path.join(self.m_Dir, name)

                shutil.unpack_archive(filename=archive_path, extract_dir=self.m_Dir)
                assert os.path.exists(extract_folder)

                shutil.copytree(extract_folder, self.m_Dir, dirs_exist_ok=True)
                shutil.rmtree(extract_folder)

                os.chdir(current_dir)
                return

        if not os.path.exists(directory):

            if quiet and counter_try > 0:
                message = f"""
Une erreur est survenue de dossier {ma_dep.cl_path(directory)} n'existe pas.
Nous retentons l'appel dans une seconde.
Appel {counter_try} / {self.ms_MaxCloneCounterTry}
"""
                print(message)
                time.sleep(1.0)
                Install.git_clone(self, url, update_submodule, tag, quiet, counter_try - 1)
            else:
                message = f"""
Une erreur est survenue de dossier {ma_dep.cl_path(directory)} n'existe pas.
La commande de clonage a échouée.     
Souhaitez-vous réessayer? 
{ma_dep.cl_index("1)")} {ma_dep.cl_key("Oui")}   
{ma_dep.cl_index("2)")} {ma_dep.cl_key("Non")}    

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index("1")} ):    
"""
                nb = input(message)
                number = 2

                try:
                    number = int(nb)
                except ValueError:
                    print("La chaîne de caractère saisie ne correspond pas à un numéro.")

                if 0 < number <= 2:

                    if number == 1:
                        Install.git_clone(self, url, update_submodule, tag, quiet, counter_try)
                else:
                    raise ValueError("Vous avez n'avez pas rentré le bon numéro.")

        os.chdir(directory)

        if not tag and commit_hash:
            print(f"commit hash: {commit_hash}")
            ma_dep.call(f'git reset --hard {commit_hash}')
            ma_dep.call(f'git clean -df')
            self.m_Result.m_Version = commit_hash

        if update_submodule:
            # doit être exécuté après avoir pointé sur tag sinon on obtient cette erreur :
            # https://github.com/wxWidgets/Phoenix/issues/558
            ma_dep.call('git submodule update --init --recursive --single-branch --depth=1')

        #  Au cas où on re-sauvegarde ici.
        self.m_Result.write()

        os.chdir(current_dir)

    def hg_clone(self, url, quiet=False, counter_try=10, manual_dir=""):
        directory = manual_dir if manual_dir != "" else self.m_Dir
        cmd = 'hg clone ' + url + ' "' + directory + '"'
        ma_dep.call(cmd)

        self.m_Result.m_Version = url

        if not os.path.exists(directory):

            if quiet and counter_try > 0:
                message = f"""
Une erreur est survenue de dossier {ma_dep.cl_path(directory)} n'existe pas.
Nous retentons l'appel dans une seconde.

Appel {counter_try} / {self.ms_MaxCloneCounterTry} 
"""
                print(message)
                time.sleep(1.0)
                Install.hg_clone(self, url, quiet, counter_try - 1)
            else:
                message = f"""
Une erreur est survenue de dossier {ma_dep.cl_path(directory)} n'existe pas.
La commande « {ma_dep.cl_key(cmd)} » a échoué."

Souhaitez-vous réessayer?
{ma_dep.cl_index("1)")} {ma_dep.cl_key("Oui")}   
{ma_dep.cl_index("2)")} {ma_dep.cl_key("Non")}    

Inscrivez le numéro correspondant à votre choix (Ex: 1):
"""

                nb = input(message)
                number = 2
                try:
                    number = int(nb)
                except ValueError:
                    print("La chaîne de caractère saisie ne correspond pas à un numéro.")

                if 0 < number <= 2:

                    if number == 1:
                        Install.hg_clone(self, url, quiet)
                else:
                    raise ValueError("Vous avez n'avez pas rentré le bon numéro.")

        #  C'est au moment du clonage du dépôt que le dossier parent, du fichier de résultats, est créé.
        self.m_Result.write()

    def get_terminal_name(self):
        return f"« {ma_dep.cl_key(self.m_Name)} »"

    def init_config(self):
        pass

    def go_begin(self):
        self.m_HasBeenRebuild = True

    def go_end(self):

        self.init_config()

        # print(f"Fin de création de {self.get_terminal_name()}.")
        self.m_Result.m_Done = True
        self.m_Result.write()

    def remove_install_folder(self):
        if os.path.exists(self.m_Dir):
            if os.path.isdir(self.m_Dir):
                if sys.platform.startswith('linux') or sys.platform.startswith('darwin') or sys.platform.startswith(
                        'aix'):
                    shutil.rmtree(self.m_Dir)
                elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
                    ma_dep.rmdir(self.m_Dir)
                else:
                    assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."
            else:
                os.remove(self.m_Dir)

        assert not os.path.exists(self.m_Dir), f"Le dossier {ma_dep.cl_path(self.m_Dir)} aurait dû être supprimé."

    @staticmethod
    def remove_folder(directory):
        if os.path.exists(directory):
            if sys.platform.startswith('linux') or sys.platform.startswith('darwin') or sys.platform.startswith('aix'):
                shutil.rmtree(directory)
            elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
                ma_dep.rmdir(directory)
            else:
                assert False, f"La plateforme« {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

        assert not os.path.exists(directory), f"Le dossier {ma_dep.cl_path(directory)} aurait dû être supprimé."

    @staticmethod
    def git_tags(remote=True):
        """
        :param remote: Si vrais les étiquettes seront celles du dépôt distant.
                       Si faux, elles seront celles du dépôt local.
        :return: Renvoie la liste der versions étiquetées.
        Attention doit être executé dans le dossier d'un dépôt git.
        Le dépôt minimum : (pas besoin de tout télécharger)
        git init
        git remote add origin <repo_url>
        """
        assert os.path.exists(".git"), "Doit être executé dans le dossier d'un dépôt git."

        if remote:
            cmd = "git ls-remote --tags origin"  # | grep -Eo 'v[0-9.]+$' "
            return_code, tags = ma_dep.call(in_command=cmd, in_print_output=False, in_print_cmd=False)

            result = []

            for tag in tags:
                if "^{}" not in tag:
                    tag_split = tag.split("/")
                    result.append(tag_split[-1])

            return result
        else:
            cmd = "git tag"
            return_code, tags = ma_dep.call(in_command=cmd, in_print_output=False, in_print_cmd=False)
            return tags

    @staticmethod
    def git_last_tags(remote=True):
        """
        :param remote: Si vrais les étiquettes seront celles du dépôt distant.
                       Si faux, elles seront celles du dépôt local.
        :return: Renvoie la dernière version étiquetée.
        Les versions qui contiennent d'autres lettres que la première sont exclues.
        Exemple :
        v2.4.2 est inclue.
        v2.4.2-rc est exclue.
        """
        tags = Install.git_tags(remote)
        major = 0
        minor = 0
        micro = 0

        regex = r"[0-9]{1,3}\.[0-9]{1,3}\.\w*"

        def is_int(number_str):
            result = True
            try:
                int(number_str)
            except ValueError:
                result = False
            return result

        tag_names = {}  # dict[tuple[int, int, int], str]

        for tag in tags:
            matches = re.finditer(regex, tag, re.MULTILINE)

            for matchNum, match in enumerate(matches, start=1):
                version = match.group()
                versions = version.split(".")

                # Nous ne gardons que les versions n'ayant qu'un entier et non des lettres
                # Exemple : v0rc3.
                if is_int(versions[2]):
                    tmp_major, tmp_minor, tmp_micro = int(versions[0]), int(versions[1]), int(versions[2])

                    if tmp_major != major or tmp_minor != minor or tmp_micro != micro:
                        tag_names[(tmp_major, tmp_minor, tmp_micro)] = tag

                        if tmp_major > major or \
                                tmp_major == major and tmp_minor > minor or \
                                tmp_major == major and tmp_minor == minor and tmp_micro > micro:
                            major = tmp_major
                            minor = tmp_minor
                            micro = tmp_micro

        return major, minor, micro, tag_names[(major, minor, micro)]

# # tests
# if __name__ == '__main__':
#     # tags = Install.git_tags()
#
#     print(Install.git_last_tags())
