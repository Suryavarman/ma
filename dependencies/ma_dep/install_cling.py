#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import os

if __name__ == "__main__":
    import sys
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallCling(ma_dep.Install):
    """
    https://rawcdn.githack.com/root-project/cling/d59d27ad61f2f3a78cd46e652cd9fb8adb893565/www/build.html
    git clone http://root.cern.ch/git/llvm.git src
    cd src
    git checkout cling-patches
    cd tools
    git clone http://root.cern.ch/git/cling.git
    git clone http://root.cern.ch/git/clang.git
    cd clang
    git checkout cling-patches

    mkdir obj
    cd obj
    cmake -DCMAKE_INSTALL_PREFIX=[Install Path] ..\src
    cmake --build . --config [Release/Debug] --target cling
    """
    ms_LLVM_GitUrl = 'http://root.cern.ch/git/llvm.git'
    ms_Cling_GitUrl = 'http://root.cern.ch/git/cling.git'
    ms_CLANG_GitUrl = 'http://root.cern.ch/git/clang.git'
    ms_Branch = "cling-patches"

    def __init__(self, auto=False):
        super().__init__(name='Cling')
        self.auto = auto

        if self.m_Result.m_Done:
            current_cwd = os.getcwd()
            os.chdir(self.m_Dir)

            try:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallCling.ms_Branch
            except KeyError:
                print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

            os.chdir(current_cwd)

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        message = f"""
Souhaitez-vous:
1) Installer {self.get_terminal_name()} via Homebrew (seulement sur MacOS).
2) Installer {self.get_terminal_name()} à partir du dépôt git officiel.

Remarques:
Pour compiler criterion il vous faudra meson et ninja

Inscrivez le numéro correspondant à votre choix (Ex: 1):
"""
        nb = input(message)

        try:
            number = int(nb)
        except ValueError:
            print("Nombre invalide")

        if 0 < number <= 2:
            if number == 1:
                ma_dep.call("brew install snaipe/soft/criterion")

            elif number == 2:
                if os.path.exists(self.m_Dir):

                    message = f"""
{ma_dep.cl_warning("Attention")} le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
1) Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
2) Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.
3) Utiliser ce dossier existant et passer à l'installation des autres dépendances.

Inscrivez le numéro correspondant à votre choix (Ex: 1):
"""
                    nb = input(message)

                    try:
                        number = int(nb)
                    except ValueError:
                        print("La chaîne de caractère saisie ne correspond pas à un numéro.")

                    if 0 < number <= 3:
                        if number == 1:
                            self.remove_install_folder()
                            self.git_clone_sources()

                        if number == 1 or number == 2:
                            self.build()
                    else:
                        raise ValueError("Vous n'avez pas rentré le bon numéro.")
                else:
                    self.git_clone_sources()
                    self.build()
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_Cling_GitUrl,
                                 update_submodule=True,
                                 branche=self.ms_Branch)
        self.m_Result.write()

    def build(self):
        # build_dir = os.path.join(self.m_Dir, 'build')
        # if not os.path.exists(build_dir):
        #     os.mkdir(build_dir)
        # os.chdir(build_dir)
        # ma_dep.call('cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DI18N=OFF ..')
        # ma_dep.call('cmake --build .')
        os.chdir(self.m_Dir)
        ma_dep.call('meson build')
        ma_dep.call('ninja -C build')

        print("Installation de Critérion terminé.")


if __name__ == '__main__':
    InstallCling().go()
