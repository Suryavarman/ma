#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
import math

import ma_dep

colour_background = (30, 31, 34)
colour_text = (60, 190, 196)
colour_title = (199, 125, 187)
colour_index = (207, 142, 109)
colour_str = (90, 171, 115)
colour_link = (86, 168, 245)

colour_info = colour_index
colour_warning = colour_title
colour_error = (207, 81, 78)

colour_url = colour_link
colour_key = colour_title
colour_path = colour_link

colour_separator = colour_text
colour_separator_size = 90

colour_operator = colour_text


class Colours:
    enable = True


def cl_b(in_colour_foreground: tuple[int, int, int], in_colour_background: tuple[int, int, int] = None) -> str:
    """
    Colour_begin
    Génère la chaîne de caractères pour coloriser un texte dans un terminal.
    Seul le texte sera coloré et non le fond.
    :param in_colour_foreground: Couleur du texte.
    :param in_colour_background: Couleur de l'arrière-plan.
    :return: La chaîne de caractère ex : \033[38;2;<r>;<g>;<b>m
    """
    if not Colours.enable:
        return ""
    else:
        if in_colour_background is None:
            return f"\033[38;2;{in_colour_foreground[0]};{in_colour_foreground[1]};{in_colour_foreground[2]}m"
        else:
            return f"\033[38;2;{in_colour_foreground[0]};{in_colour_foreground[1]};{in_colour_foreground[2]};48;2;{in_colour_background[0]};{in_colour_background[1]};{in_colour_background[2]}m"


def cl_e():
    """
    Colour_end
    Clôture une couleur.
    :return:
    """
    if Colours.enable:
        return "\033[0m"
    else:
        return ""


def cl_url(url: str) -> str:
    if Colours.enable:
        return cl_b(colour_url) + url + cl_e()
    else:
        return url


def cl_key(key: str) -> str:
    if Colours.enable:
        return cl_b(colour_key) + key + cl_e()
    else:
        return key


def cl_path(path: str) -> str:
    if Colours.enable:
        return cl_b(colour_operator) + cl_b(colour_path) + path + cl_b(colour_operator) + cl_e()
    else:
        return "« " + path + " »"


def cl_index(index: str) -> str:
    if Colours.enable:
        return cl_b(colour_index) + index + cl_e()
    else:
        return index


def cl_block(block: str) -> str:
    if Colours.enable:
        return cl_b(colour_text) + block + cl_e()
    else:
        return block


def cl_lerp(colour_a: tuple[int, int, int], colour_b: tuple[int, int, int], weight: float) -> tuple[int, int, int]:
    return (math.ceil(colour_a[0] + weight * (colour_b[0] - colour_a[0])),
            math.ceil(colour_a[1] + weight * (colour_b[1] - colour_a[1])),
            math.ceil(colour_a[2] + weight * (colour_b[2] - colour_a[2])))


def cl_project_separator(text: str, project_name: str) -> str:
    project_name_str = " " + text + " " + project_name

    if Colours.enable:
        colour_a = colour_url  # (0, 119, 255)
        colour_b = colour_background  # (11, 251, 199)
        texte_colour = colour_background
        project_name_colour = colour_background

        steps = colour_separator_size
        ramp = []
        for index in range(steps):
            ramp.append(cl_lerp(colour_a, colour_b, index / steps))

        ramp_str = ""
        index = 0
        for colour in ramp:
            if index < len(project_name_str) - len(project_name):
                ramp_str += f"{cl_b(texte_colour, colour)}{project_name_str[index]}{cl_e()}"
            elif index < len(project_name_str):
                ramp_str += f"\033[1m{cl_b(project_name_colour, colour)}{project_name_str[index]}{cl_e()}"
            else:
                ramp_str += f"{cl_b(texte_colour, colour)} {cl_e()}"
            index += 1
        return ramp_str
    else:
        steps = colour_separator_size
        top_separator    = "   ┌─"
        middle_separator = "───┤ "
        bottom_separator = "   └─"

        project_name_str = f"{project_name_str}"
        for index in range(steps):
            if index < len(project_name_str):
                top_separator    += "─"
                middle_separator += project_name_str[index]
                bottom_separator += "─"
            elif index < len(project_name_str) + 1:
                top_separator    += "─"
                middle_separator += " "
                bottom_separator += "─"
            elif index < len(project_name_str) + 2:
                top_separator    += "┐"
                middle_separator += "├"
                bottom_separator += "┘"
            else:
                top_separator    += " "
                middle_separator += "─"
                bottom_separator += " "

        return f"""
{top_separator}        
{middle_separator}        
{bottom_separator}        
"""


def cl_command_separator() -> str:
    if Colours.enable:
        colour_a = colour_separator
        colour_b = colour_background

        steps = colour_separator_size
        ramp = []
        for index in range(steps):
            ramp.append(cl_lerp(colour_a, colour_b, index / steps))

        ramp_str = ""
        for colour in ramp:
            ramp_str += cl_b(colour) + "—" + cl_e()

        return ramp_str
    else:
        steps = colour_separator_size
        separator = ""
        for index in range(steps):
            separator += "—"
        return separator


def cl_info(text: str) -> str:
    if Colours.enable:
        return cl_b(ma_dep.colour_info) + text + cl_e()
    else:
        return text


def cl_warning(text: str) -> str:
    if Colours.enable:
        return cl_b(ma_dep.colour_warning) + text + cl_e()
    else:
        return text


def cl_error(text: str) -> str:
    if Colours.enable:
        return cl_b(ma_dep.colour_warning) + text + cl_e()
    else:
        return text


def cl_bold(text: str) -> str:
    if Colours.enable:
        return "\033[1m" + text + "\033[21;24m"
    else:
        return text
