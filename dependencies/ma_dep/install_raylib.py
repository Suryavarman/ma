#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import os
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallRaylib(ma_dep.Install):
    ms_UseLastTag = True  # si vrais le dernier tag du dépôt sera utilisé.
    ms_GitTag = '5.0'
    ms_GitUrl = 'https://github.com/raysan5/raylib'

    # https://github.com/robloach/raylib-cpp
    # https://github.com/hbiblia/gtk-raylib

    def __init__(self, auto=False):
        super().__init__(name='Raylib')
        self.auto = auto

        if self.m_Result.m_Done:
            if InstallRaylib.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallRaylib.ms_GitTag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
{ma_dep.cl_warning("Attention")} le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')} ):
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()

                if number == 1 or number == 3:
                    self.build(remove_build_dir=number == 3)
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_GitUrl,
                                 update_submodule=True,
                                 tag=self.ms_GitTag,
                                 use_last_tag=self.ms_UseLastTag)

    def build(self, remove_build_dir=True):
        if sys.platform.startswith('linux'):
            self.build_linux(remove_build_dir)
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.build_windows(remove_build_dir)
        elif sys.platform.startswith('darwin'):
            self.build_darwin(remove_build_dir)

        print(f"Installation de « {self.m_Name} » terminée.")

    def build_windows(self, remove_build_dir=True):
        # https://github.com/raysan5/raylib/wiki/Working-on-Windows
        os.chdir(self.m_Dir)

    def build_linux(self, remove_build_dir=True):
        # https://github.com/raysan5/raylib/wiki/Working-on-GNU-Linux

        build_path = os.path.join(self.m_Dir, "build")

        if os.path.exists(build_path) and remove_build_dir:
            self.remove_folder(build_path)

        if not os.path.exists(build_path):
            os.mkdir(build_path)

        os.chdir(build_path)

        ma_dep.call("cmake -DBUILD_SHARED_LIBS=ON ..")
        ma_dep.call(f'make -j {ma_dep.thread_count()}')

    def build_darwin(self, remove_build_dir=True):
        # https://github.com/raysan5/raylib/wiki/Working-on-macOS
        os.chdir(self.m_Dir)

    def init_config(self):
        super().init_config()

        group = "raylib"

        bin_path = os.path.join(self.m_Dir, "build", "raylib")
        key_bin_path = ma_dep.GroupKeyDefaultValue(group, "MA_RAYLIB_BIN_PATH", bin_path)
        self.m_Config.set_value(key_bin_path, bin_path)


if __name__ == '__main__':
    InstallRaylib().go()
