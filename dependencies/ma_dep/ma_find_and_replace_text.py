#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import glob
import fileinput


def get_path(in_glob_pattern):
    glob_results = glob.glob(in_glob_pattern)
    assert len(glob_results) != 0, "Le paterne «" + in_glob_pattern + "» ne retourne aucun résultat."
    return glob_results[0]


def replace_str(in_filepath, str_target, str_replacement):
    with fileinput.FileInput(in_filepath, inplace=True, backup='.bak') as file:
        for line in file:
            print(line.replace(str_target, str_replacement), end='')
