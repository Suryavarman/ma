#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import os
import sys
import psutil

if __name__ == "__main__":
    import sys
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class Installpybind11(ma_dep.Install):
    ms_UseLastTag = True  # si vrais ms_GitTag sera ignoré, le dernier tag du dépôt sera utilisé.
    ms_GitTag = "v2.11.1"
    ms_GitUrl = 'https://github.com/pybind/pybind11.git'

    def __init__(self, auto=False, py_env_dep: ma_dep.InstallPyEnv = None):
        super().__init__(name='pybind11')
        self.auto = auto
        self.m_InstallPyEnv: ma_dep.InstallPyEnv = py_env_dep

        if py_env_dep is not None:
            self.m_Result.m_Done = self.m_Result.m_Done and not py_env_dep.m_HasBeenRebuild

        if self.m_Result.m_Done:
            if Installpybind11.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == Installpybind11.ms_GitTag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        def rebuild_all():
            self.remove_install_folder()
            self.git_clone_sources()
            self.setup_py_modules()
            self.build()

        if os.path.exists(self.m_Dir):
            message = f"""
{ma_dep.cl_warning("Attention")} le dossier {ma_dep.cl_path(self.m_Dir)} existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')} ):
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 2:
                if number == 1:
                    rebuild_all()
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            rebuild_all()

        self.go_end()

    def setup_py_modules(self):
        # Le module build est installé, pour compiler le module python de ma via « python -m build ».
        ma_dep.Install.install_py_modules(self, ["pytest", "build"])

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_GitUrl,
                                 update_submodule=True,
                                 tag=self.ms_GitTag,
                                 use_last_tag=self.ms_UseLastTag)

    def build(self):
        os.chdir(self.m_Dir)
        os.mkdir("build")
        os.chdir(os.path.join(self.m_Dir, "build"))

        # par défaut MinSizeRel https://cmake.org/cmake/help/v3.2/manual/cmake.1.html

        if sys.platform.startswith('linux') or sys.platform.startswith('darwin') or sys.platform.startswith('aix'):

            ma_dep.call('cmake .. -DPYTHON_EXECUTABLE="' + os.path.join(self.m_PythonPath, 'bin' + os.sep + 'python3') + '" -DDOWNLOAD_CATCH=1 -DCMAKE_BUILD_TYPE=Debug')
            ma_dep.call('make -j ' + str(ma_dep.thread_count()))

        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):

            if sys.platform.startswith('win32'):
                ma_dep.init_visual()

            ma_dep.call('cmake .. -A x64 -DPYTHON_EXECUTABLE="' + os.path.join(self.m_PythonPath, 'Scripts' + os.sep + 'python3') + '" -DDOWNLOAD_CATCH=1 -DCMAKE_BUILD_TYPE=Release')
            ma_dep.call('cmake --build . --clean-first --target check --config Release -- -j ' + str(ma_dep.thread_count()))

        else:
            assert False, f"La plateforme « {ma_dep.cl_key(sys.platform)} » n'est pas encore gérée."

        print(f"Installation du module pybind11 dans l'environnement python suivant: {ma_dep.cl_path(self.m_PythonPath)}.")
        pip_path = os.path.join(self.m_PythonPath, 'bin' + os.sep + 'pip3')
        ma_dep.call(f'"{pip_path}"  install "{self.m_Dir}/"')

    def init_config(self):
        super().init_config()

        group_pybind11 = "pybind11"

        pybind11_path = self.m_Dir
        key_pybind11_path = ma_dep.GroupKeyDefaultValue(group_pybind11, 'MA_PYBIND11_PATH', pybind11_path)

        self.m_Config.set_value(key_pybind11_path, pybind11_path)


if __name__ == '__main__':
    Installpybind11().go()
