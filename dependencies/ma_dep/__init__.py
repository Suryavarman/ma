#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.
#
#  @package ma_dep
#  Module python pour télécharger et compiler les dépendances du projet Ma.

__author__ = 'Suryavarman (https://www.suryavarman.fr/)'

__all__ = ["ma_python",
           "ma_config",
           "ma_find_and_replace_text",
           "ma_colours",
           "install_linux",
           "install_cmake",
           "install_clang",
           "install_py_env",
           "install_pybind11",
           "install_ogre",
           "install_cycles",
           "install_luxcore",
           "install_yafaray",
           "install_panda",
           "install_raylib",
           "install_criterion",
           "install_wxpython",
           "fixes_install_wxWidgets",
           "fixes_install_wxPython"]


# Cœur
from ma_dep.ma_config import *
from ma_dep.ma_os import *
from ma_dep.ma_python import *
from ma_dep.ma_find_and_replace_text import *
from ma_dep.ma_install import *
from ma_dep.ma_colours import *

# Installation des dépendances
from ma_dep.install_linux import *
from ma_dep.install_cmake import *
from ma_dep.install_clang import *
from ma_dep.install_py_env import *
from ma_dep.install_pybind11 import *
from ma_dep.install_ogre import *
from ma_dep.install_cycles import *
from ma_dep.install_panda import *
from ma_dep.install_luxcore import *
from ma_dep.install_yafaray import *
from ma_dep.install_raylib import *
from ma_dep.install_criterion import *
from ma_dep.install_eigen import *
from ma_dep.install_wxpython import *
from ma_dep.fixes_install_wxWidgets import *
from ma_dep.fixes_install_wxPython import *
