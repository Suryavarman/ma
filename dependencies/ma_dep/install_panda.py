#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import os
import sys
import wget

# les dépendances:
# build-essential pkg-config fakeroot python3-dev libpng-dev libjpeg-dev libtiff-dev zlib1g-dev libssl-dev libx11-dev
# libgl1-mesa-dev libxrandr-dev libxxf86dga-dev libxcursor-dev bison flex libfreetype6-dev libvorbis-dev
# libopenal-dev libode-dev libbullet-dev nvidia-cg-toolkit libgtk-3-dev libassimp-dev libopenexr-dev
# libeigen3-dev <——— mieux vaut utiliser celui compiler dans les dépendances de Ma.
# https://github.com/panda3d/panda3d/blob/master/README.md
# Sur linux: Ne pas oublier d'installer Bison

# Bogues :
#  compilation en débogue de Panda3D avec gcc13 sur Manjaro : https://github.com/panda3d/panda3d/issues/1580

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallPanda(ma_dep.Install):
    ms_UseLastTag = True  # si vrais le dernier tag du dépôt sera utilisé.
    ms_Version = '1.10.13'
    ms_GitTag = f'v{ms_Version}'
    ms_GitUrl = 'https://github.com/panda3d/panda3d'
    ms_GitDepth = 1  # 0 par défaut. Si 1 le commit hash n'est pas téléchargé le téléchargement sera donc plus rapide.
    ms_BackupArchive = "https://codeload.github.com/panda3d/panda3d/zip/refs/tags/%TAG%"
    ms_CommitHash = ""  # "c6c7509"

    # https://www.panda3d.org/download/panda3d-1.10.13/panda3d-1.10.13-tools-win64.zip
    # https://www.panda3d.org/download/panda3d-1.10.13/panda3d-1.10.13-tools-mac.tar.gz

    def __init__(self, auto=False):
        super().__init__(name='Panda')
        self.auto = auto

        if self.m_Result.m_Done:
            if InstallPanda.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallPanda.ms_GitTag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
Attention le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}. 
    -> Au préalable sur linux assurez-vous d'avoir installer Bison.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()

                if number == 1 or number == 3:
                    self.build(remove_build_dir=number == 3)
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_GitUrl,
                                 update_submodule=True,
                                 tag=self.ms_GitTag,
                                 use_last_tag=self.ms_UseLastTag,
                                 depth=self.ms_GitDepth,
                                 archive_backup=self.ms_BackupArchive,
                                 commit_hash=self.ms_CommitHash)

    def build(self, remove_build_dir=True):
        if sys.platform.startswith('linux'):
            self.build_linux(remove_build_dir)
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            self.build_windows(remove_build_dir)
        elif sys.platform.startswith('darwin'):
            self.build_darwin(remove_build_dir)

        print(f"Installation de « {self.m_Name} » terminée.")

    def build_windows(self, remove_build_dir=True):
        # https://github.com/panda3d/panda3d/blob/master/README.md
        os.chdir(self.m_Dir)
        ma_dep.call(
            f'makepanda\makepanda.bat --everything --installer --msvc-version=14.3 --windows-sdk=10 --no-eigen --threads={ma_dep.thread_count()}')

    def build_linux(self, remove_build_dir=True):
        # https://github.com/panda3d/panda3d/blob/master/README.md
        os.chdir(self.m_Dir)

        built_path = os.path.join(self.m_Dir, "built")
        built_debug_path = os.path.join(self.m_Dir, "built_debug")
        build_path = os.path.join(self.m_Dir, "build")

        if os.path.exists(built_path) and remove_build_dir:
            self.remove_folder(built_path)

        if os.path.exists(built_debug_path) and remove_build_dir:
            self.remove_folder(built_debug_path)

        if os.path.exists(build_path) and remove_build_dir:
            self.remove_folder(build_path)

        # \todo trouver la dépendence pour avoir l'audio ?
        # \todo inclure Eigen.
        python_path = os.path.join(self.m_PythonPath, 'bin' + os.sep + 'python3')

        # Supprime le dossier de sortie ce qui est déjà fait en amont.
        # ma_dep.call(f'{python_path} makepanda/makepanda.py --clean')

        # release
        ma_dep.call(
            f'{python_path} makepanda/makepanda.py --everything --installer --no-egl --no-gles --no-gles2 --no-opencv --threads={ma_dep.thread_count()} --optimize=3')

        # debug
        ma_dep.call(
            f'{python_path} makepanda/makepanda.py --everything --installer --no-egl --no-gles --no-gles2 --no-opencv --threads={ma_dep.thread_count()} --optimize=1 --outputdir=built_debug')

    def build_darwin(self, remove_build_dir=True):
        # https://github.com/panda3d/panda3d/blob/master/README.md
        os.chdir(self.m_Dir)
        dep_archive_name = f"panda3d-{self.ms_Version}-tools-mac"
        dependencies_url = f"https://www.panda3d.org/download/panda3d-{self.ms_Version}/{dep_archive_name}.tar.gz"

        archive_path = os.path.join(self.m_Dir, dep_archive_name + ".tar.gz")

        if os.path.exists(archive_path):
            os.remove(archive_path)
        assert not os.path.exists(archive_path)

        print(f"Téléchargement de l'archive {ma_dep.cl_path(dep_archive_name)}.")
        wget.download(dependencies_url, archive_path)
        assert os.path.exists(archive_path)
        print("Téléchargement terminer.")

        ma_dep.call('python makepanda/makepanda.py --everything --installer')

    def init_config(self):
        super().init_config()

        built_path = os.path.join(self.m_Dir, "built")
        built_debug_path = os.path.join(self.m_Dir, "built_debug")
        build_path = os.path.join(self.m_Dir, "build")

        group_panda = "panda"
        conf = self.m_Config

        # lib
        lib_path = os.path.join(built_path, 'lib')
        key_panda_lib_path = ma_dep.GroupKeyDefaultValue(group_panda, "MA_PANDA_LIB_PATH", lib_path)
        conf.set_value(key_panda_lib_path, lib_path)

        lib_debug_path = os.path.join(built_debug_path, 'lib')
        key_panda_lib_debug_path = ma_dep.GroupKeyDefaultValue(group_panda, "MA_PANDA_LIB_DEBUG_PATH", lib_debug_path)
        conf.set_value(key_panda_lib_debug_path, lib_debug_path)

        # include
        include_path = os.path.join(built_path, 'include')
        key_panda_include_path = ma_dep.GroupKeyDefaultValue(group_panda, "MA_PANDA_INCLUDE_PATH", include_path)
        conf.set_value(key_panda_include_path, include_path)

        include_debug_path = os.path.join(built_debug_path, 'include')
        key_panda_include_debug_path = ma_dep.GroupKeyDefaultValue(group_panda,
                                                                   "MA_PANDA_INCLUDE_DEBUG_PATH",
                                                                   include_debug_path)
        conf.set_value(key_panda_include_debug_path, include_debug_path)


if __name__ == '__main__':
    InstallPanda().go()
