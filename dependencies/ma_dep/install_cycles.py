#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import glob
import os
import sys

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallCycles(ma_dep.Install):
    # https://projects.blender.org/blender/cycles
    ms_UseLastTag = False  # si vrais ms_Tag sera ignoré, le dernier tag du dépôt sera utilisé.

    # ms_Tag = "v4.0.2"
    ms_GitCommitHash = "2d4573dbae53f4850f7e35b679556d5d64ad1f3b"

    # Ça serait peut-être plus simple d'utiliser cette branche ?
    # https://projects.blender.org/boberfly/blender/src/branch/cycles_shared_lib
    # Elle provient de cette requête : https://projects.blender.org/blender/cycles/pulls/1
    ms_GitUrl = 'https://projects.blender.org/blender/cycles.git'
    ms_BackupArchive = "https://projects.blender.org/blender/cycles/archive/%TAG%.zip"

    # fix / attente de l'acceptation de la requête de fusion.
    # ms_GitUrl = 'https://projects.blender.org/Pierre-Pontier/cycles.git'
    # ms_Branch = "fix/cmake_policy_cmp0144_is_not_know"
    ms_Tag = ""

    # ms_GitUrl = 'https://projects.blender.org/Pierre-Pontier/cycles.git'
    # ms_Branch = 'fix/node_set_enum_sfinae'
    # ms_Tag = ""

    # ms_OpenImageIO_GitUrl = "https://github.com/OpenImageIO/oiio.git"
    # ms_OpenImageIO_GitTag = "v2.4.11.1"
    #
    # ms_OpenTBB_GitUrl = "https://github.com/oneapi-src/oneTBB"
    # ms_OpenTBB_GitTag = "v2021.9.0"
    #
    # # Dépend de :
    # # - Boost
    # # - Intel TBB
    # # - OpenSubDiv ?
    # ms_USD_GitUrl = "https://github.com/PixarAnimationStudios/USD"
    # ms_USD_GitTag = "v23.05"

    # Pour USD, mais celui-ci télécharge lui-même ses dépendances.
    # ms_OpenSubDiv_GitUrl = "https://github.com/PixarAnimationStudios/OpenSubdiv.git"
    # ms_OpenSubDiv_GitTag = "v3_5_0"
    # Laisser le choix de compiler boost ou d'utiliser celui du système ?

    def __init__(self, auto=False, py_env_dep: ma_dep.InstallPyEnv = None):
        super().__init__(name='Cycles')
        self.auto = auto
        self.m_InstallPyEnv: ma_dep.InstallPyEnv = py_env_dep

        cycle_dependencies = os.path.join(self.m_ModulesMaDir, "module_cycles", "dependencies")
        if not os.path.exists(cycle_dependencies):
            os.makedirs(cycle_dependencies, exist_ok=True)

        self.m_Dir = os.path.join(cycle_dependencies, "cycles")

        # self.m_CycleDependenciesDir = os.path.join(self.m_Dir, "dependencies")

        # self.m_OpenImageIO_Dir = os.path.join(self.m_CycleDependenciesDir, "oiio")
        # self.m_OpenTBB_Dir = os.path.join(self.m_CycleDependenciesDir, "tbb")
        # self.m_USD_Dir = os.path.join(self.m_CycleDependenciesDir, "usd")
        # self.m_USD_Build_Dir = os.path.join(self.m_USD_Dir, "build")

        self.m_Result = ma_dep.Result(os.path.join(self.m_Dir, "ma_dep_result.txt"))

        if py_env_dep is not None:
            self.m_Result.m_Done = self.m_Result.m_Done and not py_env_dep.m_HasBeenRebuild

        if self.m_Result.m_Done:
            if InstallCycles.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = (self.m_Result.m_Version == InstallCycles.ms_Tag or
                                        self.m_Result.m_Version == InstallCycles.ms_GitCommitHash)

    def init_config(self):
        super().init_config()

        conf = self.m_Config
        conf.ms_GroupCycles = "cycles"
        conf.ms_Key_cycles_include_path = ma_dep.GroupKeyDefaultValue(conf.ms_GroupCycles,
                                                                      'MA_CYCLES_INCLUDE_PATH',
                                                                      os.path.join(self.m_Dir, "src"))

        conf.ms_Key_cycles_lib_directory = ma_dep.GroupKeyDefaultValue(conf.ms_GroupCycles,
                                                                       'MA_CYCLES_LIB_DIRECTORY',
                                                                       os.path.join(self.m_Dir, "build", "lib"))

        conf.ms_Key_cycles_third_party_path = ma_dep.GroupKeyDefaultValue(conf.ms_GroupCycles,
                                                                          'MA_CYCLES_THIRD_PARTY_DIRECTORY',
                                                                          os.path.join(self.m_Dir, "src"))

        # old_extern_lib_path = os.path.abspath(os.path.join(self.m_Dir, "..", "lib"))
        extern_lib_path = os.path.abspath(os.path.join(self.m_Dir, "lib"))

        assert os.path.exists(extern_lib_path), f"Le dossier {extern_lib_path} n'existe pas."

        if sys.platform.startswith('linux'):
            extern_lib_path = os.path.join(extern_lib_path, "linux_x64")
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            # todo gérer l'arm
            arm = False
            if arm:
                extern_lib_path = os.path.join(extern_lib_path, "windows_arm64")
            else:
                extern_lib_path = os.path.join(extern_lib_path, "windows_x64")
        elif sys.platform.startswith('darwin'):
            # todo gérer l'arm
            arm = False
            if arm:
                extern_lib_path = os.path.join(extern_lib_path, "macos_arm")
            else:
                extern_lib_path = os.path.join(extern_lib_path, "macos_x64")
        else:
            assert False, "La plateforme «" + sys.platform + "» n'est pas encore gérée."

        print(extern_lib_path)

        conf.ms_Key_cycles_ext_lib_directory = ma_dep.GroupKeyDefaultValue(conf.ms_GroupCycles,
                                                                           'MA_CYCLES_LIB_EXTERN_DIRECTORY',
                                                                           extern_lib_path)

        conf.ms_Keys.append(conf.ms_Key_cycles_include_path)
        conf.ms_Keys.append(conf.ms_Key_cycles_lib_directory)
        conf.ms_Keys.append(conf.ms_Key_cycles_third_party_path)
        conf.ms_Keys.append(conf.ms_Key_cycles_ext_lib_directory)

        conf.set_value(conf.ms_Key_cycles_include_path, conf.ms_Key_cycles_include_path.m_DefaultValue)
        conf.set_value(conf.ms_Key_cycles_lib_directory, conf.ms_Key_cycles_lib_directory.m_DefaultValue)
        conf.set_value(conf.ms_Key_cycles_third_party_path, conf.ms_Key_cycles_third_party_path.m_DefaultValue)
        conf.set_value(conf.ms_Key_cycles_ext_lib_directory, conf.ms_Key_cycles_ext_lib_directory.m_DefaultValue)

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):
            message = f"""
{ma_dep.cl_warning("Attention")} le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractères saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()
                    # self.git_clone_dependencies()

                if number == 1 or number == 3:
                    self.build()
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        else:
            # Il faut d'abord créer le dossier des sources pour y créer ensuite le dossier des dépendances.
            self.git_clone_sources()
            # self.git_clone_dependencies()
            self.build()

        # Nous copions le code dans le dossier du module cycle."
        # shutil.copytree(self.m_Dir, os.path.join(self.m_ModulesMaDir, "cycle", "cycles"))
        # shutil.copytree(os.path.join(self.m_Dir, "..", "lib"), os.path.join(self.m_ModulesMaDir, "cycle", "lib"))

        self.go_end()

    # def git_clone_dependencies(self):
    #     if not os.path.exists(self.m_CycleDependenciesDir):
    #         os.mkdir(self.m_CycleDependenciesDir)
    #
    #     ma_dep.Install.git_clone(self,
    #                              url=self.ms_OpenImageIO_GitUrl,
    #                              manual_dir=self.m_OpenImageIO_Dir,
    #                              update_submodule=True,
    #                              tags=self.ms_OpenImageIO_GitTag,
    #                              use_last_tag=self.ms_UseLastTag)
    #
    #     ma_dep.Install.git_clone(self,
    #                              url=self.ms_OpenTBB_GitUrl,
    #                              manual_dir=self.m_OpenTBB_Dir,
    #                              update_submodule=True,
    #                              tags=self.ms_OpenTBB_GitTag,
    #                              use_last_tag=self.ms_UseLastTag)
    #
    #     ma_dep.Install.git_clone(self,
    #                              url=self.ms_USD_GitUrl,
    #                              manual_dir=self.m_USD_Dir,
    #                              update_submodule=True,
    #                              tags=self.ms_USD_GitTag,
    #                              use_last_tag=self.ms_UseLastTag)

    def git_clone_sources(self):
        if self.ms_Tag != "":
            ma_dep.Install.git_clone(self,
                                     url=self.ms_GitUrl,
                                     update_submodule=True,
                                     tag=self.ms_Tag,
                                     # branche=self.ms_Branch,
                                     use_last_tag=self.ms_UseLastTag,
                                     archive_backup=self.ms_BackupArchive)
        elif self.ms_GitCommitHash != "":
            ma_dep.Install.git_clone(self,
                                     url=self.ms_GitUrl,
                                     update_submodule=True,
                                     commit_hash=self.ms_GitCommitHash)
        else:
            ma_dep.Install.git_clone(self,
                                     url=self.ms_GitUrl,
                                     update_submodule=True,
                                     branche=self.ms_Branch,
                                     use_last_tag=self.ms_UseLastTag,
                                     archive_backup=self.ms_BackupArchive)

    def build(self):
        # ma_dep.Install.install_py_modules(self, ["pyside6", "PyOpenGL", "jinja2"], use_current_python=True)
        #
        # os.chdir(self.m_USD_Dir)
        # ma_dep.call(sys.executable + " " + os.path.join(self.m_USD_Dir, "build_scripts", "build_usd.py") + " " +
        #             self.m_USD_Build_Dir)
        #

        os.chdir(self.m_Dir)
        # Could NOT find PythonLibsUnix (missing: PYTHON_LIBRARY PYTHON_LIBPATH
        # PYTHON_INCLUDE_DIR PYTHON_INCLUDE_CONFIG_DIR)
        # https://stackoverflow.com/questions/61101987/cmake-error-could-not-find-pythonlibsunix
        # ma_dep.call('cmake -B ./build -DPXR_ROOT="' + self.m_USD_Build_Dir + '"')
        # ma_dep.call('cmake -B ./build')
        ma_dep.call('make update')
        ma_dep.call('make')

        print("Installation de Cycle terminée.")


if __name__ == '__main__':
    # cmake_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "CMake", "build", "bin")
    # cmake_path = os.path.join(cmake_dir, "cmake")
    #
    # ma_dep.Environ.ms_Vars["PATH"] = cmake_dir + ":" + ma_dep.Environ.ms_Vars["PATH"]

    # print(ma_dep.Environ.ms_Vars["PYTHON_LIBRARY"])
    # print(ma_dep.Environ.ms_Vars["PYTHON_LIBPATH"])
    # print(ma_dep.Environ.ms_Vars["PYTHON_INCLUDE_DIR"])
    # print(ma_dep.Environ.ms_Vars["PYTHON_INCLUDE_CONFIG_DIR"])

    # for path in sys.path:
    #     ma_dep.Environ.ms_Vars["PYTHONPATH"] = path + ":" + ma_dep.Environ.ms_Vars["PYTHONPATH"]
    # print("PYTHONPATH : " + ma_dep.Environ.ms_Vars["PYTHONPATH"])
    #
    # ma_dep.call('python3 -c "import sys;print(sys.path)"')
    # ma_dep.call("echo $PATH")
    # ma_dep.call("cmake --version")
    # ma_dep.call("whereis cmake")
    InstallCycles().go()
