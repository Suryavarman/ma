#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import os
import sys
import time
import psutil
import warnings
import ma_dep


class Environ:
    ms_Vars = os.environ.copy()


def thread_count():
    # https://docs.python.org/3/library/multiprocessing.html#multiprocessing.cpu_count
    # https://stackoverflow.com/questions/1006289/how-to-find-out-the-number-of-cpus-using-python
    if sys.platform.startswith('linux') or sys.platform.startswith('aix'):
        return len(os.sched_getaffinity(0))
    elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
        return len(psutil.Process().cpu_affinity())
    elif sys.platform.startswith('darwin'):
        return os.cpu_count()
    else:
        assert False, "La plateforme «" + sys.platform + "» n'est pas encore gérée."


def is_32bits_os():
    return sys.maxsize == 2 ** 31 - 1


def is_64bits_os():
    return sys.maxsize != 2 ** 31 - 1


def get_paths(directory):
    """Recursive function to list every files and directories inside inDirectory
        :return: List of un-ignoring paths
        :rtype: array of str
    """

    paths = [os.path.normpath(directory)]
    directories = os.listdir(paths[0])

    for aDirectory in directories:

        path = os.path.normpath(directory + r'\\' + aDirectory)

        if os.path.isdir(path):
            paths += get_paths(path)
        else:
            paths.append(path)

    return paths


def file_is_lock(in_path):
    """ Teste si le fichier est utilisé par un autre processus.
        :param in_path: the directory file
        :type in_path: str

        :return: True si de fichier est utilisé par un autre processus
        :rtype: bool
    """
    process_list = psutil.Process()
    open_file_list = os.path.normpath(str(process_list.open_files()))

    return open_file_list.find(os.path.normpath(in_path)) != -1


def folder_has_files_lock(in_path):
    """ Test if every files inside the folder are using by another process
        :param in_path: the directory file
        :type in_path: str

        :return: True if the folder has a locked file by an other process.
        :rtype: bool
    """
    paths = get_paths(in_path)
    process_list = psutil.Process()
    open_file_list = os.path.normpath(str(process_list.open_files()))

    for file_path in paths:
        if os.path.isfile(file_path) and open_file_list.find(os.path.normpath(file_path)) != -1:
            return True
    return False


def file_is_lock(path):
    """ Test if the file is using by another process
        :param path: the directory file
        :type path: str
    """
    process_list = psutil.Process()
    open_file_list = os.path.normpath(str(process_list.open_files()))

    return open_file_list.find(os.path.normpath(path)) != -1


def wait_while_path_is_lock(path, wait_time):
    """ During the inWaiTime seconds will wait the unblocking of inPath.

        :param path: The directory or the file that should be test.
        :type path: str

        :param wait_time: While the inWaitTime timer has not expired we will check every seconds if the folder has
                           been unlock. The minimum value is 1s
        :type wait_time: int [1: MAX_INT]
    """
    i = 0

    is_a_directory = os.path.exists(path) and os.path.isdir(path)
    is_a_file = os.path.exists(path) and os.path.isfile(path)

    if is_a_directory:
        while i < wait_time and folder_has_files_lock(path):
            i += 1
            time.sleep(1)
            print('wait copy folder')
    elif is_a_file:
        while i < wait_time and file_is_lock(path):
            i += 1
            time.sleep(1)
            print('wait copy file')

    result = (not folder_has_files_lock(path)) if is_a_directory else not file_is_lock(path) if is_a_file else True

    if not result:
        if is_a_directory:
            warnings.warn('After ' + str(wait_time) + ' seconds the directory stay blocked.')
        elif is_a_file:
            warnings.warn('After ' + str(wait_time) + ' seconds the file stay blocked.')
        else:
            warnings.warn('The path ' + path + 'is not a file or a folder and after ' + wait_time + ' seconds is stay'
                                                                                                    ' blocked.')


def copy(source, destination):
    """
        Permet de copier un dossier avec windows.

        :param source: Le chemin du dossier qui sera copié
        :type source: str

        :param destination: Le chemin de destination de la copie
        :type destination: str

        .. note:
            `XCOPY <http://www.computerhope.com/xcopyhlp.htm>`_
            `COPY <https://www.computerhope.com/copyhlp.htm>`_
    """

    if os.path.isfile(source):
        cmd = 'copy "' + source + '" "' + destination + '" /Y /V'
    else:
        cmd = 'xcopy "' + source + '" "' + destination + '" /H /K /Y /E /I'

    ma_dep.call(cmd)


def rmdir(directory, wait_unlock=False, wait_time=10):
    """ Use dos to remove the directory.
        :param directory: The directory to be remove
        :type directory: str

        :param wait_unlock: if it's True we will wait inWaitTime seconds the inSource unlock to remove the directory. After
                             this time in any case we will try to remove. Every seconds we will test if the folder has
                             been unlock.
        :type wait_unlock: bool

        :param wait_time: While the inWaitTime timer has not expired we will check every seconds if the folder has
                           been unlock. The minimum value is 1s
        :type wait_time: int [1: MAX_INT]
    """
    if os.path.exists(directory):
        remove_folder_command = 'rmdir /S /Q ' + directory

        if wait_unlock:
            wait_while_path_is_lock(directory, wait_time)

        return ma_dep.call(remove_folder_command)
    else:
        return 0, []


def init_visual():
    vs_2019_bat_path = "C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Auxiliary/Build/vcvarsx86_amd64.bat"
    vs_2017_bat_path = "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Auxiliary/Build/vcvarsx86_amd64.bat"
    if os.path.exists(vs_2019_bat_path):
        ma_dep.call(vs_2019_bat_path)

    if os.path.exists(vs_2017_bat_path):
        ma_dep.call(vs_2019_bat_path)
