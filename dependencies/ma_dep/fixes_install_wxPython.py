#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep
from shutil import copyfile


class FixWxPython:

    @staticmethod
    def fix(wx_python_path):
        if sys.platform.startswith('linux'):
            # /home/gandi/Working/Ma/dependencies/wxPython/build/wxbld/gtk3/lib/wx/include/
            #
            # find gtk3-unicode-3.1
            # /wx
            # open setup.h
            # replace «#define wxUSE_UIACTIONSIMULATOR 0» by «#define wxUSE_UIACTIONSIMULATOR 1»
            setup_file_path = ma_dep.get_path(wx_python_path + "/build/wxbld/gtk3/lib/wx/include/gtk3*/wx/setup.h")
            ma_dep.replace_str(setup_file_path, "#define wxUSE_UIACTIONSIMULATOR 0", "#define wxUSE_UIACTIONSIMULATOR 1")

            # Je ne sais pas pourquoi sur certains PC wxUSE_AUTOID_MANAGEMENT est après la compilation.
            ma_dep.replace_str(setup_file_path, "#define wxUSE_AUTOID_MANAGEMENT 1", "#define wxUSE_AUTOID_MANAGEMENT 0")

            # tests_makefile_file_path = ma_dep.get_path(ma_dep.dependency_dir + "/wxPython/build/wxbld/gtk3/tests/Makefile")
            # ma_dep.replace_str(tests_makefile_file_path, "TEST_CXXFLAGS =", "TEST_CXXFLAGS = -fPIC")

            # python_config_file_in_path = ma_dep.get_path(ma_dep.dependency_dir + "/wxPython/build/waf/3.7/gtk3/")
            # python_config_file_out_path = ma_dep.get_path(ma_dep.dependency_dir + "/wxPython/build/wxbld/gtk3/wx-config")
            # copyfile(python_config_file_in_path, python_config_file_out_path)
            #
            # ma_dep.replace_str(tests_makefile_file_path, "TEST_CXXFLAGS =", "TEST_CXXFLAGS = -fPIC")
            # PYTHON_CONFIG

            # python_config_file_sh_path = ma_dep.get_path(ma_dep.dependency_dir + "/Python-3.7.4/python-config")
            # ma_dep.replace_str(python_config_file_sh_path, 'OPT="-DNDEBUG -g -fwrapv -O3 -Wall"', 'OPT="-DNDEBUG -g -fwrapv -O3 -Wall -fPIC"')
            #
            # python_config_file_py_path = ma_dep.get_path(ma_dep.dependency_dir + "/Python-3.7.4/python-config.py")
            # ma_dep.replace_str(python_config_file_py_path, "print(' '.join(flags))", "print(' '.join(flags) + '-fPIC')")
            #
            # python_config_gnu_file_py_path = ma_dep.get_path(ma_dep.dependency_dir + "/python3.7/lib/python3.7/config-3.7m-x86_64-linux-gnu/python-config.py")
            # ma_dep.replace_str(python_config_gnu_file_py_path, "print(' '.join(flags))", "print(' '.join(flags) + '-fPIC')")
            #
            # python_config_bin_file_py_path = ma_dep.get_path(ma_dep.dependency_dir + "/python3.7/bin/python3.7m-config")
            # ma_dep.replace_str(python_config_gnu_file_py_path, 'echo "$INCDIR $PLATINCDIR $BASECFLAGS $CFLAGS $OPT"', 'echo "$INCDIR $PLATINCDIR $BASECFLAGS $CFLAGS $OPT -fPIC"')

            # créer un sim link qui ./python3.7/bin/python3-config qui pointe vers ./python3.7/bin/python3.7m-config
        elif sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
            setup_cfg = ma_dep.get_path(wx_python_path + "/setup.cfg")
            ma_dep.replace_str(setup_cfg, "license-file", "license_file")
        elif sys.platform.startswith('darwin'):
            pass


if __name__ == '__main__':
    FixWxPython().fix(os.path.join(ma_dep.dependency_dir, "wxPython"))
