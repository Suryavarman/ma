#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import os

if __name__ == "__main__":
    import sys
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallEigen(ma_dep.Install):
    ms_UseLastTag = True  # si vrais ms_Tag sera ignoré, le dernier tag du dépôt sera utilisé.
    ms_Tag = "3.4.1"
    ms_GitUrl = 'https://gitlab.com/libeigen/eigen.git'
    ms_BackupArchive = "https://gitlab.com/libeigen/eigen/-/archive/%TAG%/eigen-%TAG%.zip"

    def __init__(self, auto=False):
        super().__init__(name='Eigen')
        self.auto = auto

        if self.m_Result.m_Done:
            if InstallEigen.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallEigen.ms_Tag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        if os.path.exists(self.m_Dir):

            message = f"""
{ma_dep.cl_warning("Attention")} le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
            if self.auto:
                nb = 1
            else:
                nb = input(message)

            number = 3

            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractère saisie ne correspond pas à un numéro.")

            if 0 < number <= 3:
                if number == 1:
                    self.remove_install_folder()
                    self.git_clone_sources()

                if number == 1 or number == 3:
                    self.build()
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")
        else:
            self.git_clone_sources()
            self.build()

        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_GitUrl,
                                 update_submodule=True,
                                 tag=self.ms_Tag,
                                 use_last_tag=self.ms_UseLastTag,
                                 archive_backup=self.ms_BackupArchive)

    def build(self):
        os.chdir(self.m_Dir)
        print("Installation de Eigen terminé.")

    def init_config(self):
        super().init_config()

        group_eigen = "eigen"
        eigen_path = self.m_Dir
        key_eigen_path = ma_dep.GroupKeyDefaultValue(group_eigen, 'MA_EIGEN_PATH', eigen_path)

        self.m_Config.set_value(key_eigen_path, eigen_path)


if __name__ == '__main__':
    InstallEigen().go()
