#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import os
import sys
import shutil
import subprocess
import ma_dep


def python_version_choice():
    # https://stackoverflow.com/questions/53312590/how-to-check-all-the-installed-python-versions-on-windows
    # which python3.6m, python3.7m etc

    python_versions = ['3.5', '3.6', '3.7', '3.8', '3.9', '3.10', '3.11']
    abi_flags = ['m', '']

    python_paths = []
    python_paths_search = []  # en cas d'échec, nous afficherons la liste des chemins testés.

    # trouver les versions de python qui sont installées.
    paths = os.environ['PATH'].split(os.pathsep)

    for path in paths:
        for version in python_versions:
            for abi_flag in abi_flags:
                python_dir = path + '/python' + version + abi_flag
                python_include_dir = '/usr/include/python' + version + abi_flag  # @todo faire une fonction générique
                # https://stackoverflow.com/questions/35071192/how-to-find-out-where-the-python-include-directory-is

                python_paths_search.append(python_dir)
                if os.path.exists(python_dir) and os.path.isdir(python_include_dir):
                    python_paths.append(python_dir)

    if len(python_paths) == 0:
        message = """
Aucune version valide de python n'a été trouvée.
Voici la liste des chemins qui ont été recherchés.
"""

        for path in python_paths_search:
            message += path + os.linesep

        raise ValueError(message)
    else:
        print("Choisissez la version de python avec laquelle «Maçon de l'espace» fonctionnera.")
        i = 1

        for python_path in python_paths:
            print(str(i) + ") " + python_path)
            i += 1

        nb = input("Inscrivez le numéro correspondant à votre choix (Ex: 1):")

        number = len(python_paths)
        try:
            number = int(nb)
        except ValueError:
            print("La chaîne de caractère saisie ne correspond pas à un numéro.")

        if 0 < number <= len(python_paths):
            print("Vous avez choisi: " + python_paths[number - 1])
            return python_paths[number - 1]
        else:
            raise ValueError("Vous avez n'avez pas rentré le bon numéro.")


def define_python_env(in_python_path, in_venv_path):
    """ Attention, en fonction du chemin de l'exécutable python, il ne doit pas être contenu dans un environnement
    virtuel, definePythonEnv va mettre à jour le groupe ms_GroupPython et le groupe ms_GroupPythonVenv.

    in_python_path (str): Chemin de l'exécutable python, attention il ne doit pas être contenu dans un environnement
    virtuel. Ou pas!!!!

    in_venv_path (str): Chemin du futur environment python ou existant. L'existence de l'environment virtuel n'est pas
    obligatoire. Les variables seront déterminé à partir d'in_python_path.
    """
    config = ma_dep.MaConfig()
    config.set_value(config.ms_Key_PythonPath, in_python_path)
    config.set_value(config.ms_Key_PythonEnvPath, in_venv_path)

    print("cfg path: " + config.ms_Filename)

    cmd = f"""
{in_python_path} -c "#!python3

import sysconfig
import sys
import os
import configparser

py_version_short = sysconfig.get_config_var('py_version_short')

config = configparser.ConfigParser()
config.read('{config.ms_Filename}')

if not config.has_section('{config.ms_Key_PythonEnvPath.m_Group}'):
    config.add_section('{config.ms_Key_PythonEnvPath.m_Group}')
    
if config.has_option('{config.ms_Key_PythonEnvPath.m_Group}', '{config.ms_Key_PythonEnvPath.m_Key}'):
    venv_path = config['{config.ms_Key_PythonEnvPath.m_Group}']['{config.ms_Key_PythonEnvPath.m_Key}']
else:
    venv_path = '{config.ms_Key_PythonEnvPath.m_DefaultValue}'

sp_path = venv_path + '/lib64/python' + py_version_short + '/site-packages' 
if not os.path.exists(sp_path):
    sp_path = venv_path + '/lib/python' + py_version_short + '/site-packages'

config.set('{config.ms_Key_PythonEnvSPPath.m_Group}', 
           '{config.ms_Key_PythonEnvSPPath.m_Key}', 
           sp_path)
           
config.set('{config.ms_Key_PythonEnvIncludePath.m_Group}', 
           '{config.ms_Key_PythonEnvIncludePath.m_Key}', 
           venv_path + '/include/python' + py_version_short + sys.abiflags)
           
config.set('{config.ms_Key_PythonDllPath.m_Group}', 
           '{config.ms_Key_PythonDllPath.m_Key}',
            venv_path + '/lib')
            
config.set('{config.ms_Key_PythonDllName.m_Group}', 
           '{config.ms_Key_PythonDllName.m_Key}', 
           'libpython' + py_version_short + sys.abiflags + '.so')
           
with open('{config.ms_Filename}', 'w') as configfile: 
    config.write(configfile)
"
"""

    # cmd = in_python_path + ' -c "'
    # cmd += "#!python3" + os.linesep
    # cmd += "" + os.linesep
    # cmd += "import sysconfig" + os.linesep
    # cmd += "import sys" + os.linesep
    # cmd += "import os" + os.linesep
    # cmd += "import configparser" + os.linesep
    # cmd += "py_version_short = sysconfig.get_config_var('py_version_short')" + os.linesep
    # cmd += "config = configparser.ConfigParser()" + os.linesep
    # cmd += "config.read('" + config.ms_Filename + "')" + os.linesep
    # cmd += "if not config.has_section('" + config.ms_Key_PythonEnvPath.m_Group + "'):" + os.linesep
    # cmd += "    config.add_section('" + config.ms_Key_PythonEnvPath.m_Group + "')" + os.linesep
    # cmd += "if config.has_option('" + config.ms_Key_PythonEnvPath.m_Group + "', '" + config.ms_Key_PythonEnvPath.m_Key + "'):" + os.linesep
    # cmd += "    venv_path = config['" + config.ms_Key_PythonEnvPath.m_Group + "']['" + config.ms_Key_PythonEnvPath.m_Key + "']" + os.linesep
    # cmd += "else:" + os.linesep
    # cmd += "    venv_path = '" + config.ms_Key_PythonEnvPath.m_DefaultValue + "'" + os.linesep
    # cmd += "sp_path = venv_path + '/lib64/python' + py_version_short + '/site-packages'" + os.linesep
    # cmd += "if not os.path.exists(sp_path):" + os.linesep
    # cmd += "    sp_path = venv_path + '/lib/python' + py_version_short + '/site-packages'" + os.linesep
    # cmd += "config.set('" + config.ms_Key_PythonEnvSPPath.m_Group + "', '" + config.ms_Key_PythonEnvSPPath.m_Key + "', sp_path)" + os.linesep
    # cmd += "config.set('" + config.ms_Key_PythonEnvIncludePath.m_Group + "', '" + config.ms_Key_PythonEnvIncludePath.m_Key + "', venv_path + '/include/python' + py_version_short + sys.abiflags)" + os.linesep
    # cmd += "config.set('" + config.ms_Key_PythonDllPath.m_Group + "', '" + config.ms_Key_PythonDllPath.m_Key + "', venv_path + '/lib')" + os.linesep
    # cmd += "config.set('" + config.ms_Key_PythonDllName.m_Group + "', '" + config.ms_Key_PythonDllName.m_Key + "', 'libpython' + py_version_short + sys.abiflags + '.so')" + os.linesep
    # cmd += "with open('" + config.ms_Filename + "', 'w') as configfile:" + os.linesep
    # cmd += "    config.write(configfile)" + os.linesep
    # cmd += '"'
    call(cmd)


def make_python_env(in_python_path, in_venv_path):
    # todo utiliser des lignes de commande multi-plateformes
    # https://stackoverflow.com/questions/1811691/running-an-outside-program-executable-in-python

    if sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
        python_path = os.path.normpath(os.path.join(in_python_path, "python.exe"))
        assert os.path.exists(python_path), "Le dossier python n'existe pas: " + python_path
        venv_path = os.path.normpath(in_venv_path)
        print("Début de la génération de l'environnement python à l'emplacement: «" + venv_path + "».")

        cmd = '"' + python_path + '"' + " -m venv " + '"' + venv_path + '"'
        call(cmd, False)
    else:
        if not shutil.which("virtualenv"):
            message = """
Il vous faut installer virtualenv.
1) Veuillez installer virtualenv via le système de paquets et ensuite validez l'option pour relancer la création de 
l'environnement virtuel. (FORTEMENT CONSEILLÉ)
2) Laissez pip3 installer virtualenv dans $HOME/.local/. Cela n'ai pas conseillé car sur Mageia7 virtualenv n'arrive pas
 à installer l'environnement virtuel. Il ne trouve pas certains modules.
3) Quitter l'installateur.

Inscrivez le numéro correspondant à votre choix (Ex: 1):
"""
            nb = input(message)

            number = 3
            try:
                number = int(nb)
            except ValueError:
                print("La chaîne de caractère saisie ne correspond pas à un numéro.")

            if number == 0:
                make_python_env(in_python_path, in_venv_path)
                return
            elif number == 1:
                pip_path = os.path.normpath(os.path.join(in_python_path, "pip3"))
                cmd = "if ! " + pip_path + " install --user virtualenv" + os.linesep
                cmd += "then" + os.linesep

                if sys.platform.startswith('win32') or sys.platform.startswith('cygwin'):
                    cmd += ' echo -e "La commande «"'+ pip_path + '"» install --user virtualenv» a échouée. Peut être que vous devriez installer python3pip"' + os.linesep
                else:
                    cmd += ' echo -e "\033[1;31mLa commande « "'+ pip_path + '" install --user virtualenv» a échouée. Peut être que vous devriez installer python3pip. \033[0m"' + os.linesep

                cmd += " exit 3" + os.linesep
                cmd += "fi" + os.linesep
                call(cmd, False)
            elif number == 3:
                raise FileExistsError("virtualenv veuillez l'installez via votre système de paquets.")
            else:
                raise ValueError("Vous avez n'avez pas rentré le bon numéro.")

        # https://python-guide-pt-br.readthedocs.io/fr/latest/dev/virtualenvs.html
        # https://virtualenv.pypa.io/en/latest/
        # --system-site-packages --no-site-packages --relocatable
        # https://github.com/pypa/virtualenv/issues/1069
        # create a SYMLINK to $HOME/.local/lib64/python3.6/lib-dynload/ in $HOME/.local/lib64/python3.6/.
        cmd = ""
        cmd += 'if ! virtualenv --python="' + in_python_path + '" --clear --download --verbose --always-copy --no-wheel "' + in_venv_path + '"' + os.linesep
        cmd += "then" + os.linesep
        cmd += ' echo -e "\033[1;31mLa commande «virtualenv --python=' + in_python_path + ' --clear --relocatable --download --verbose --always-copy --no-wheel ' + in_venv_path + '» a échouée.\033[0m"' + os.linesep
        cmd += " exit 3" + os.linesep
        cmd += "fi" + os.linesep
        call(cmd, False)

        assert os.path.isdir(in_venv_path + "/include"), \
               in_python_path + " est une installation de python qui ne permet pas d'avoir le dossier d'en-têtes, " \
               "car le dossier " + in_venv_path + "/include n'existe pas."


def call(in_command, in_print_output=True, in_print_cmd=True, in_env=ma_dep.Environ.ms_Vars):  # os.environ.copy()):
    """ Le code provient d'ici: https://sourceforge.net/p/gdeps/mercurial/ci/default/tree/GDeps/gdeps/system.py
    :param in_command:
    :type in_command: str

    :param in_print_output:
    :type in_print_output: bool

    :param in_print_cmd:
    :type in_print_cmd: bool

    :param in_env:
    :type in_env: dict[str, str]

    :return: return_code, return_std_out_lines: Le premier est le code d'erreur de retour et le deuxième un tableau
    composé des lignes de texte généré par la commande.
    :rtype: [int, list[str]]

    :todo: bogue rien ne (semble?) se passer si in_print_output et in_print_cmd sont à faux.
    """
    if in_print_cmd:
        print(ma_dep.cl_command_separator())
        print("Commande : ")
        print(ma_dep.cl_block("{"))
        print("    " + in_command)
        print(ma_dep.cl_block("}"))
        print("")

    if in_print_output:
        print("Sortie : ")
        print(ma_dep.cl_block("{"))

    return_code = -1
    return_std_out_lines = []

    def popen(in_process):
        lines = []

        for line in in_process.stdout:
            if in_print_output:
                print("    " + line, end='')
            lines.append(line[:-1])  # nous supprimons le retour chariot de fin de ligne

        if in_process.returncode is not None and in_process.returncode != 0:
            print("    returned non-zero exit status", in_process.returncode, file=sys.stderr)

        return in_process.returncode, lines

    try:
        if in_env:
            with subprocess.Popen(in_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,  bufsize=1,
                                  universal_newlines=True, shell=True, env=in_env) as process:
                return_code, return_std_out_lines = popen(process)
        else:
            with subprocess.Popen(in_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1,
                                  universal_newlines=True, shell=True) as process:
                return_code, return_std_out_lines = popen(process)

    except OSError as aError:
        print("    Execution failed:", aError, file=sys.stderr)

    if in_print_output:
        print(ma_dep.cl_block("}"))
        print("")
        print(ma_dep.cl_command_separator())

    return return_code, return_std_out_lines




