#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.

import os

if __name__ == "__main__":
    import sys

    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # Dans le cas d'un appel direct à ce script, le module ma_dep ne pourra pas être trouvé.

import ma_dep


class InstallCriterion(ma_dep.Install):
    ms_UseLastTag = True  # si vrais ms_Tag sera ignoré, le dernier tag du dépôt sera utilisé.
    ms_Tag = "v2.4.2"
    ms_GitUrl = 'https://github.com/Snaipe/Criterion'

    def __init__(self, auto=False):
        super().__init__(name='Criterion')
        self.auto = auto

        if self.m_Result.m_Done:
            if InstallCriterion.ms_UseLastTag:
                current_cwd = os.getcwd()
                os.chdir(self.m_Dir)

                try:
                    self.m_Result.m_Done = self.m_Result.m_Version == self.git_last_tags()[-1]
                except KeyError:
                    print(f"{ma_dep.cl_warning('Attention')} il est impossible d'obtenir les étiquettes de ce projet.")

                os.chdir(current_cwd)
            else:
                self.m_Result.m_Done = self.m_Result.m_Version == InstallCriterion.ms_Tag

    def go(self):
        if self.m_Result.m_Done:
            print(f"{ma_dep.cl_info('Information')}: la précédente compilation a réussie.")
            if self.auto:
                return

        self.go_begin()

        message = f"""
Souhaitez-vous:
{ma_dep.cl_index('1)')} Installer {self.get_terminal_name()} via {ma_dep.cl_bold('Homebrew')} {ma_dep.cl_warning("(seulement sur MacOS)")}.
{ma_dep.cl_index('2)')} Installer {self.get_terminal_name()} à partir du dépôt git officiel.
   {ma_dep.cl_warning(f"-> N'oubliez pas d'installer {ma_dep.cl_bold('meson')} avant d'installer {ma_dep.cl_bold(self.m_Name)}. En option vous pouvez installer {ma_dep.cl_bold('libgit2')}")}. 

Remarques:
Pour compiler criterion il vous faudra {ma_dep.cl_bold('meson')} et {ma_dep.cl_bold('ninja')}

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
        if self.auto:
            nb = 2
        else:
            nb = input(message)

        number = 2
        try:
            number = int(nb)
        except ValueError:
            print("Nombre invalide")

        if 0 < number <= 2:
            if number == 1:
                ma_dep.call("brew install snaipe/soft/criterion")

            elif number == 2:
                if os.path.exists(self.m_Dir):

                    message = f"""
{ma_dep.cl_warning("Attention")} le dossier « {ma_dep.cl_path(self.m_Dir)} » existe déjà.
Souhaitez-vous:
{ma_dep.cl_index('1)')} Supprimer le dossier et partir d'une version neuve de {self.get_terminal_name()}.
{ma_dep.cl_index('2)')} Utiliser ce dossier existant et passer à l'installation des autres dépendances.
{ma_dep.cl_index('3)')} Utiliser ce dossier existant et recompiler {self.get_terminal_name()}.

Inscrivez le numéro correspondant à votre choix (Ex: {ma_dep.cl_index('1')}):
"""
                    if self.auto:
                        nb = 1
                    else:
                        nb = input(message)

                    number = 3
                    try:
                        number = int(nb)
                    except ValueError:
                        print("La chaîne de caractère saisie ne correspond pas à un numéro.")

                    if 0 < number <= 3:
                        if number == 1:
                            self.remove_install_folder()
                            self.git_clone_sources()

                        if number == 1 or number == 3:
                            self.build()
                    else:
                        raise ValueError("Vous n'avez pas rentré le bon numéro.")
                else:
                    self.git_clone_sources()
                    self.build()
            else:
                raise ValueError("Vous n'avez pas rentré le bon numéro.")

        self.init_config()
        self.go_end()

    def git_clone_sources(self):
        ma_dep.Install.git_clone(self,
                                 url=self.ms_GitUrl,
                                 update_submodule=True,
                                 tag=self.ms_Tag,
                                 use_last_tag=self.ms_UseLastTag)

    def build(self):
        # build_dir = os.path.join(self.m_Dir, 'build')
        # if not os.path.exists(build_dir):
        #     os.mkdir(build_dir)
        # os.chdir(build_dir)
        # ma_dep.call('cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DI18N=OFF ..')
        # ma_dep.call('cmake --build .')
        os.chdir(self.m_Dir)
        ma_dep.call('meson build')
        ma_dep.call('ninja -C build')

    def init_config(self):
        super().init_config()

        group_name = "criterion"

        # Le chemin du répertoire de Criterion.
        criterion_path = self.m_Dir
        key_criterion_path = ma_dep.GroupKeyDefaultValue(group_name,
                                                         'MA_CRITERION_PATH',
                                                         criterion_path)

        # Le chemin du répertoire des librairies aux formats « .dll » et « .so » de Criterion.
        criterion_dll_path = os.path.join(criterion_path, 'build', 'src')
        key_criterion_dll_path = ma_dep.GroupKeyDefaultValue(group_name,
                                                             'MA_CRITERION_DLL_PATH',
                                                             criterion_dll_path)

        self.m_Config.set_value(key_criterion_path, criterion_path)
        self.m_Config.set_value(key_criterion_dll_path, criterion_dll_path)


if __name__ == '__main__':
    InstallCriterion().go()
