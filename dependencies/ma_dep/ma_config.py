#!python3
#  Droit d'auteur (c) 2018-2024.
#  Sous license MIT voir le fichier LICENSE à la racine du dépôt.
#
#  Copyright (c) 2018-2024.
#  Licensed to MIT see the LICENSE file in the repository root location.


import configparser
import os

import ma_dep

dependency_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class GroupKeyDefaultValue:
    def __init__(self, in_group, in_key, in_default_value):
        self.m_Group = in_group
        self.m_Key = in_key
        self.m_DefaultValue = in_default_value


class MaConfig:
    """
    Définition des chemins pour compiler le projet « Maçon de l'espace ».

    ms_Filename (str): Nom et extension du fichier de configuration. Vous pourrez l'éditer comme bon vous semble.
    Ce fichier est utilisé par l'ide pour déterminer les chemins des dépendances.

    ms_GroupPython (str) : Clef du groupe concernant l'installation de python3 sur le système. C'est cette version de
    python qui servira à créer l'environnement virtuel.
    ms_Key_PythonPath (GroupKeyDefaultValue) : Le chemin de l'exécutable de python3. La version minimale requise est la
    3.6 (la 3.5 est tout de même proposée, mais n'a pas été testée).
    ms_Key_PythonDllPath (GroupKeyDefaultValue) : Le chemin du dossier contenant la librairie dynamique de python3.
    Exemple : /usr/lib

    ms_GroupPythonVenv (str) : Clef du groupe concernant l'installation de l'environnement virtuel de la version python3
    défini dans la variable ms_Key_PythonPath.
    ms_Key_PythonEnvPath (GroupKeyDefaultValue) : Le chemin du répertoire de l'environnement python.
    ms_Key_PythonEnvSPPath (GroupKeyDefaultValue) : Le chemin du répertoire « site-packages » dans l'environnement
    python.
    ms_Key_PythonEnvIncludePath (GroupKeyDefaultValue) : Le chemin du répertoire « include/pythonx.yz » dans
    l'environnement python.

    ms_Keys (array(str)) : La liste des GroupKeyDefaultValue.
    """

    ms_Filename = dependency_dir + '/dependencies.cfg'

    ms_GroupPython = "python"
    ms_Key_PythonPath = GroupKeyDefaultValue(ms_GroupPython, 'MA_PYTHON_PATH', '/usr/bin/python3.8')
    ms_Key_PythonDllPath = GroupKeyDefaultValue(ms_GroupPython, 'MA_PYTHON_DLL_PATH', '/usr/lib/')
    ms_Key_PythonDllName = GroupKeyDefaultValue(ms_GroupPython, 'MA_PYTHON_DLL_NAME', 'python3.8m')

    ms_GroupPythonVenv = "venv"
    ms_Key_PythonEnvPath = GroupKeyDefaultValue(ms_GroupPythonVenv, 'MA_PYTHONENV_PATH', dependency_dir + '/venv')
    ms_Key_PythonEnvSPPath = GroupKeyDefaultValue(ms_GroupPythonVenv, 'MA_PYTHONENV_SP_PATH', dependency_dir +
                                                  '/venv/lib/python3.6/site-packages/')
    ms_Key_PythonEnvIncludePath = GroupKeyDefaultValue(ms_GroupPythonVenv, 'MA_PYTHONENV_INCLUDE_PATH', dependency_dir +
                                                       '/venv/include/python3.6m')

    ms_Keys = [ms_Key_PythonPath,
               ms_Key_PythonDllPath,
               ms_Key_PythonDllName,
               ms_Key_PythonEnvPath,
               ms_Key_PythonEnvSPPath,
               ms_Key_PythonEnvIncludePath]

    def __init__(self):
        self.m_Config = configparser.ConfigParser()
        self.read()

        for key_pair in MaConfig.ms_Keys:
            if not self.m_Config.has_option(key_pair.m_Group, key_pair.m_Key):
                self.set_value(key_pair, key_pair.m_DefaultValue)

        self.write()

    def get_value(self, in_group_key_default_value):
        """ Retourne la valeur correspondante du fichier de configuration. Si la pair [groupe][clef] n'est pas présente
        dans le fichier de configuration, la valeur par défaut associée sera retournée.

        in_group_key_default_value (GroupKeyDefaultValue) : Pair de clefs permettant d'accéder aux valeurs stockées dans
        le fichier de configuration.
        """
        key_pair = in_group_key_default_value

        if self.m_Config.has_option(key_pair.m_Group, key_pair.m_Key):
            return self.m_Config[key_pair.m_Group][key_pair.m_Key]
        else:
            print("Aucune option existe pour le couple {clef: valeur} suivant : {" + key_pair.m_Group + ": " +
                  key_pair.m_Key + "}")
            return key_pair.m_DefaultValue

    def set_value(self, in_group_key_default_value, in_value):
        """ Assigne à la paire [groupe:clef] sa nouvelle valeur et sauvegarde la nouvelle configuration dans le fichier
            MaDependencies.ms_Filename.

        in_group_key_default_value (GroupKeyDefaultValue): Pair de clefs permettant d'accéder aux valeurs stockées dans
        le fichier de configuration.

        in_value (str): Valeur de la paire [groupe:clef].
        """
        key_pair = in_group_key_default_value

        if not self.m_Config.has_section(key_pair.m_Group):
            self.m_Config.add_section(key_pair.m_Group)

        self.m_Config.set(key_pair.m_Group, key_pair.m_Key, in_value)

        self.write()

    def write(self):
        with open(MaConfig.ms_Filename, 'w') as configfile:
            self.m_Config.write(configfile)

    def read(self):
        self.m_Config.read(self.ms_Filename)
