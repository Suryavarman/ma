// Copyright 20019-2020 Maçon de l'espace. All Rights Reserved.
// Licensed to MIT see LICENSE

pipeline
{
    agent
    {
        node
        {
            label 'linux'
        }
    }

    options
    {
        gitLabConnection('framagit_groupe_ma')
        gitlabBuilds(builds: ['infos', 'deps', 'linux_gcc', 'linux_clang', 'unit_tests']) // 'linux_intel',

        // Le compte à rebours débute juste après l'allocation d'un agent.
        timeout(time: 5, unit: 'HOURS')
    }

    stages
    {
         // Les informations qui seront publiés peut importe l'état des compilations.
        stage("infos")
        {
            steps
            {
                script
                {
                    updateGitlabCommitStatus name: 'infos', state: 'running'
                    echo "env.gitlabSourceBranch: ${env.gitlabSourceBranch}"
                    echo "env.gitlabMergeRequestId: ${env.gitlabMergeRequestId}"

                    if(env.gitlabMergeRequestId)
                    {
                        // Cette commande ne peut être faite que lors de la création d'une requête de fusion.
                        // Il faut donc qu'elle soit exécutée à tout pris.
                        addGitLabMRComment comment: "Liste des artefacts:\n ${JOB_URL}lastSuccessfulBuild/artifact/"
                    }
                }
            }
            post
            {
                failure
                {
                    updateGitlabCommitStatus name: 'infos', state: 'failed'
                }
                success
                {
                    // archiveArtifacts artifacts: 'build_gcc/*Linux.tar.gz', fingerprint: true
                    updateGitlabCommitStatus name: 'infos', state: 'success'
                }
                aborted
                {
                    updateGitlabCommitStatus name: 'infos', state: 'canceled'
                }
            }
        }

        // prérequis :
        // git
        // subversion (svn)
        // sur Linux: build-essential, sur BSD: os-generic-userland-devtools
        // cmake
        // ninja
        // python3.xx-venv
        // zlib (dev)
        // ssl (dev)
        // meson et libgit2 pour criterion
        // bison pour panda3d
        // les version dev des librairies: xrandr, xinerama, xcursor et xi pour raylib ex: libxi-dev
        // libgtk-3-dev, doxygen, graphviz pour wxPython
        // doxygen pour générer la documentation de Ma
        // XTest pour wxUIActionSimulator (libtxtst-devel)
        // webkit pour compiler wxWebView Debian: libwebkit2gtk-4.0-dev
        //                                Fedora: webkit2gtk3-devel
        //                                Mageia: lib64webkit2gtk4.0-devel
        //                                Ubuntu: libwebkit2gtk-4.0-dev Il faut ajouter le dépôt jammy:
        //                                        sudo nano /etc/apt/sources.list
        //                                        deb http://fr.archive.ubuntu.com/ubuntu/ jammy main
        //                                        sudo apt update
        //                                        sudo apt install libwebkit2gtk-4.0-dev
        //
        //
        // Optionnel :
        // git-lfs pour Cycles https://git-lfs.com/ (cf: ensure_git_lfs dans src/cmake/make_update.py )
        //
        //
        // Les erreurs:
        // 1/ Pour l'installation de python 3.12 l'erreur suivante peu apparaitre :
        //    pip_path[0] IndexError: list index out of range
        //    https://github.com/python/cpython/issues/89230
        //    Il faut installer readline-devel qui devrait installer aussi curse.
        //
        // 2/ Si il y a cette erreur no module named _bz2 found :
        //    Installez ces lib:
        //    sudo apt-get install zlib1g-dev libffi-dev libssl-dev libbz2-dev libreadline-dev libsqlite3-dev
        //         liblzma-dev
        //    cf: https://www.pythonpool.com/fixed-modulenotfounderror-no-module-named-_bz2/
        //
        // 3/ hb.h not found : Il faut installer la librairie HarfBuzz, si elle l'ai c'est que le dependencies.cfg est
        //    mal configuré : HarfBuzz
        //
        // 4/ dot not found : Doxygen a besoin graphviz
        //
        // 5/ oidn Ne pas installer openimagedenoise-devel sinon l'en-tête oidn.h du système sera utiliser
        //    denoiser_oidn_gpu.cpp:134:27: erreur: « oidnGetNumPhysicalDevices » n'a pas été déclaré dans cette portée.
        //
        // Corrections des bogues des dépendances:
        // Gérer les forks: https://statnmap.com/fr/2019-05-12-garder-a-jour-fork-github-gitlab/
        //
        // Projet de tests:
        // https://framagit.org/ma_groupe/jenkins_pipeline_parallel_serial/-/blob/main/Jenkinsfile
        stage("deps")
        {
            steps
            {
                updateGitlabCommitStatus name: 'deps', state: 'running'
                echo "Téléchargement et compilations des dépendances."
                echo "Le fichier /dependencies/dependencies.cfg va être généré ou mis à jour."
                echo "Via CMake le code de « ma » se liera à ces dépendances grâce à ce fichier de configuration."
                sh "git submodule update --init --recursive"

                dir('dependencies')
                {
                    sh "python3 -m venv venv_install"
                    sh "venv_install/bin/pip install -r requirements.txt"
                    sh "venv_install/bin/python3 install_dependencies.py --wx_python_from git --auto --no_colour"
                }
            }
            post
            {
                failure
                {
                    updateGitlabCommitStatus name: 'deps', state: 'failed'
                }
                success
                {
                    updateGitlabCommitStatus name: 'deps', state: 'success'
                }
                aborted
                {
                    updateGitlabCommitStatus name: 'deps', state: 'canceled'
                }
            }
        }

        stage("gcc")
        {
            steps
            {
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE')
                {
                    echo "Construction des binaires avec le compilateur gcc."

                    dir('code')
                    {
                        updateGitlabCommitStatus name: 'linux_gcc', state: 'running'
                        sh '''
if [ -e linux_gcc ]; then
    rm -r linux_gcc
fi
'''
                        sh "mkdir -p linux_gcc"
                        dir('linux_gcc')
                        {
                            sh '''
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_MAKE_PROGRAM=ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -G Ninja ../
'''
                            sh '''
cmake --build . --target MaApi_UnitTests
'''
                            sh '''
cmake --build . --target MaEditor
'''
                       }
                    }
               }
            }
            post
            {
                failure
                {
                    updateGitlabCommitStatus name: 'linux_gcc', state: 'failed'
                }
                success
                {
                    updateGitlabCommitStatus name: 'linux_gcc', state: 'success'
                }
                aborted
                {
                    updateGitlabCommitStatus name: 'linux_gcc', state: 'canceled'
                }
            }
        }

        stage("clang")
        {
            steps
            {
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE')
                {
                    echo "Construction des binaires avec le compilateur clang."

                    dir('code')
                    {
                        updateGitlabCommitStatus name: 'linux_clang', state: 'running'
                        sh '''
if [ -e linux_clang ]; then
    rm -r linux_clang
fi
'''
                        sh "mkdir -p linux_clang"
                        dir('linux_clang')
                        {
                            sh '''
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_MAKE_PROGRAM=ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -G Ninja ../
'''
                            sh '''
cmake --build . --target MaApi_UnitTests
'''
                            sh '''
cmake --build . --target MaEditor
'''
                        }
                    }
               }
            }
            post
            {
                failure
                {
                    updateGitlabCommitStatus name: 'linux_clang', state: 'failed'
                }
                success
                {
                    updateGitlabCommitStatus name: 'linux_clang', state: 'success'
                }
                aborted
                {
                    updateGitlabCommitStatus name: 'linux_clang', state: 'canceled'
                }
            }
        }

//         stage("intel")
//         {
//             steps
//             {
//                 catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE')
//                 {
//                     echo "Construction des binaires avec le compilateur intel."
//
//                     dir('code')
//                     {
//                         updateGitlabCommitStatus name: 'linux_intel', state: 'running'
//                         // sh "git clean -dxf"
//                         sh '''
// if [ -e linux_intel ]; then
//     rm -r linux_intel
// fi
// '''
//                         sh "mkdir -p linux_intel"
//                         dir('linux_intel')
//                         {
// // Ajouter cette dans votre .bashrc pour activer les variables d'environnement du compilateur intel (icx et icpx).
// // . /opt/intel/oneapi/setvars.sh --force intel64
//                             sh '''
// . ~/.bashrc
// . /opt/intel/oneapi/setvars.sh --force intel64
// icpx --version
// cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_MAKE_PROGRAM=ninja -DCMAKE_C_COMPILER=icx -DCMAKE_CXX_COMPILER==icpx -G Ninja ../
// '''
//                             sh '''
// cmake --build . --target MaApi_UnitTests
// '''
//                             sh '''
// cmake --build . --target MaEditor
// '''
//                         }
//                     }
//                }
//             }
//             post
//             {
//                 failure
//                 {
//                     updateGitlabCommitStatus name: 'linux_intel', state: 'failed'
//                 }
//                 success
//                 {
//                     updateGitlabCommitStatus name: 'linux_intel', state: 'success'
//                 }
//                 aborted
//                 {
//                     updateGitlabCommitStatus name: 'linux_intel', state: 'canceled'
//                 }
//             }
//         }

        stage("Unit tests")
        {
            steps
            {
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE')
                {
                    echo "Test unitaires."

                    dir('code/linux_gcc/api/ma')
                    {
                        updateGitlabCommitStatus name: 'unit_tests', state: 'running'
                        sh '''
./MaApi_UnitTests --ascii --xml="tests.xml"
'''
                    }
                }
            }
            post
            {
                failure
                {
                    updateGitlabCommitStatus name: 'unit_tests', state: 'failed'
                }
                success
                {
                    updateGitlabCommitStatus name: 'unit_tests', state: 'success'
                }
                aborted
                {
                    updateGitlabCommitStatus name: 'unit_tests', state: 'canceled'
                }
            }
        }
    }
    post
    {
        always {
            junit testResults: 'code/linux_gcc/api/ma/tests.xml', skipPublishingChecks: true
        }
    }
}
